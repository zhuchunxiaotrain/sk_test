package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;

/**
 * Created by ZhuChunXiao on 2017/3/29.
 * 资产报废
 */
@Entity
@Table(name="ec_assetscrap")
public class AssetScrap extends BaseFlowEntity {
    /**
     * 资产使用登记表
     */
    private AssetUse assetUse;
    /**
     * 报废原因
     */
    private Dict scrapReason;
    /**
     * 是否上报中心
     */
    private boolean leaderExam;

    @ManyToOne(fetch = FetchType.LAZY)
    public AssetUse getAssetUse() {
        return assetUse;
    }

    public void setAssetUse(AssetUse assetUse) {
        this.assetUse = assetUse;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getScrapReason() {
        return scrapReason;
    }

    public void setScrapReason(Dict scrapReason) {
        this.scrapReason = scrapReason;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isLeaderExam() {
        return leaderExam;
    }

    public void setLeaderExam(boolean leaderExam) {
        this.leaderExam = leaderExam;
    }
}
