package com.joint.core.entity.manage;

import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 * 加油明细
 */
@Entity
@Table(name="ec_oilquery")
public class OilQuery extends BaseFlowEntity {
    /**
     * 当前里程数
     */
    private int kilometre;
    /**
     * 加油时间
     */
    private Date oilDate;
    /**
     * 加油型号
     */
    private String type;
    /**
     * 加油金额
     */
    private Double oilMoney;
    /**
     * 当前累计加油金额
     */
    private Double oilMoneyNum;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 车辆配置表
     */
    private CarConfig carConfig;
    /**
     * 车辆信息对象
     */
    private Oil oil;

    public int getKilometre() {
        return kilometre;
    }

    public void setKilometre(int kilometre) {
        this.kilometre = kilometre;
    }

    public Date getOilDate() {
        return oilDate;
    }

    public void setOilDate(Date oilDate) {
        this.oilDate = oilDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getOilMoney() {
        return oilMoney;
    }

    public void setOilMoney(Double oilMoney) {
        this.oilMoney = oilMoney;
    }

    public Double getOilMoneyNum() {
        return oilMoneyNum;
    }

    public void setOilMoneyNum(Double oilMoneyNum) {
        this.oilMoneyNum = oilMoneyNum;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Oil getOil() {
        return oil;
    }

    public void setOil(Oil oil) {
        this.oil = oil;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public CarConfig getCarConfig() {
        return carConfig;
    }

    public void setCarConfig(CarConfig carConfig) {
        this.carConfig = carConfig;
    }
}
