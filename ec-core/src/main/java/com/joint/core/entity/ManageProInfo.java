package com.joint.core.entity;

import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;

/**
 * Created by dqf on 2015/8/25.
 */
@Entity
@Table(name="ec_manageproinfo")
public class ManageProInfo extends BaseFlowEntity {



    /**
     * 项目信息编号
     */
    private String projectNo;

    /**
     * 建设单位地址
     */
    private String buildAddress;

    /**
     * 项目概况
     */
    private String projectInfo;

    /**
     * 项目计划投资额（万元）
     */
    private double numInventMoney;

    /**
     * 资金来源
     */
    private String moneyFrom;

    /**
     * 招标信息来源
     */
    private String bidFrom;

    /**
     * 备注
     */
    private String remark;

    /**
     * 下一步审批人的Name的集合，用逗号分隔
     */
    private String nextStepApproversName;

    /**
     * 下一步审批人的id的集合，用逗号分隔
     */
    private String nextStepApproversId;







    public String getProjectNo() {
        return projectNo;
    }

    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getBuildAddress() {
        return buildAddress;
    }

    public void setBuildAddress(String buildAddress) {
        this.buildAddress = buildAddress;
    }

    public String getProjectInfo() {
        return projectInfo;
    }

    public void setProjectInfo(String projectInfo) {
        this.projectInfo = projectInfo;
    }

    public double getNumInventMoney() {
        return numInventMoney;
    }

    public void setNumInventMoney(double numInventMoney) {
        this.numInventMoney = numInventMoney;
    }

    public String getMoneyFrom() {
        return moneyFrom;
    }

    public void setMoneyFrom(String moneyFrom) {
        this.moneyFrom = moneyFrom;
    }

    public String getBidFrom() {
        return bidFrom;
    }

    public void setBidFrom(String bidFrom) {
        this.bidFrom = bidFrom;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Column(length = 510)
    public String getNextStepApproversId() {
        return nextStepApproversId;
    }

    public void setNextStepApproversId(String nextStepApproversId) {
        this.nextStepApproversId = nextStepApproversId;
    }

    public String getNextStepApproversName() {
        return nextStepApproversName;
    }

    public void setNextStepApproversName(String nextStepApproversName) {
        this.nextStepApproversName = nextStepApproversName;
    }
}
