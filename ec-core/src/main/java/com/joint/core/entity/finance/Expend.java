package com.joint.core.entity.finance;

import com.joint.base.parent.BaseFlowEntity;
import com.joint.core.entity.manage.Purchase;
import com.joint.core.entity.manage.Reception;

import javax.persistence.*;

/**
 * Created by ZhuChunXiao on 2017/4/1.
 */
@Entity
@Table(name="ec_expend")
public class Expend extends BaseFlowEntity {
    private static final long serialVersionUID = 5668453794738260692L;

    /**
     * 支出类别 1采购支出 2接待支出 3日常支出 4来往支付
     */
    private String type;
    /**
     * 采购申请单
     */
    private Purchase purchase;
    /**
     * 接待申请单
     */
    private Reception reception;
    /**
     * 支出内容
     */
    private String content;
    /**
     * 合计
     */
    private String num;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Reception getReception() {
        return reception;
    }

    public void setReception(Reception reception) {
        this.reception = reception;
    }

    @Column(length = 16777216)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
}
