package com.joint.core.entity.manage;

import com.joint.base.entity.Department;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/1/12.
 * 日程安排
 */
@Entity
@Table(name="ec_schedule")
public class Schedule extends BaseFlowEntity {


    private static final long serialVersionUID = -5274608668946356788L;
    /**
     * 日程类型
     */
    private String type;

    /**
     * 主题
     */
    private String topic;

    /**
     * 开始日期(时间)
     */
    private Date beginDate;

    /**
     * 结束日期(时间)
     */
    private Date endDate;

    /**
     * 参与人员
     */
    private List<Users> joinUsers;

    /**
     * 是否公开 1公开 0不公开
     */
    private boolean open;


    /**
     * 公开部门
     */
    private List<Department> openDepartment;

    /**
     * 公开个人
     */
    private List<Users> openUsers;


    /**
     * 公开范围
     */
    private String object;

    /**
     * 具体内容
     */
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_schedule_joinUsers", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "joinUsersId"))
    public List<Users> getJoinUsers() {
        return joinUsers;
    }

    public void setJoinUsers(List<Users> joinUsers) {
        this.joinUsers = joinUsers;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_schedule_openUsers", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "openUsersId"))
    public List<Users> getOpenUsers() {
        return openUsers;
    }

    public void setOpenUsers(List<Users> openUsers) {
        this.openUsers = openUsers;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_schedule_openDepartment", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "openDepartmentId"))
    public List<Department> getOpenDepartment() {
        return openDepartment;
    }

    public void setOpenDepartment(List<Department> openDepartment) {
        this.openDepartment = openDepartment;
    }
}
