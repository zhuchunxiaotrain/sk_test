package com.joint.core.entity;

import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_trainrecord")
public class TrainRecord extends BaseFlowEntity {

    /**
     * 培训类型
     */
    private String type;

    /**
     * 人员
     */
    private Users users;

    /**
     * 培训周期
     */
    private Date startDate;
    private Date endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
