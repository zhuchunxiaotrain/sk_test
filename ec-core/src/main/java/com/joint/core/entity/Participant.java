package com.joint.core.entity;

import com.joint.base.entity.Department;
import com.joint.base.entity.Post;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_participant")
public class Participant extends BaseFlowEntity {

    /**
     * 参与人部门
     * @return
     */
    private Department department;

    /**
     * 参与人
     * @return
     */
    private List<Users> participantUsers;

    /**
     * 培训实体
     */
    private Train train;

    @ManyToOne(fetch = FetchType.LAZY)
    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_participant_users", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="usersId"))
    public List<Users> getParticipantUsers() {
        return participantUsers;
    }

    public void setParticipantUsers(List<Users> participantUsers) {
        this.participantUsers = participantUsers;
    }
}
