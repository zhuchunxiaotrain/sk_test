package com.joint.core.entity;

import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.jws.soap.SOAPBinding;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_records")
public class Records extends BaseFlowEntity {

    /**
     * 奖惩类型
     */
    private String types;

    /**
     * 奖惩时间
     */
    private Date rpDate;

    /**
     * 相关附件
     */
    private Set<FileManage> accessories;
    private String accessoriesText;

    /**
     *  员工基本信息
     */
    private Employees employees;

    /**
     *
     * @return
     */
    private Users users;

    @OneToOne
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public Date getRpDate() {
        return rpDate;
    }

    public void setRpDate(Date rpDate) {
        this.rpDate = rpDate;
    }

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(name="ec_records_accessories",joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="accessoryId"))
    @OrderBy("name asc")
    public Set<FileManage> getAccessories() {
        return accessories;
    }

    public void setAccessories(Set<FileManage> accessories) {
        this.accessories = accessories;
    }

    public String getAccessoriesText() {
        return accessoriesText;
    }

    public void setAccessoriesText(String accessoriesText) {
        this.accessoriesText = accessoriesText;
    }
}
