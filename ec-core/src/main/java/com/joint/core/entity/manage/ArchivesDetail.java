package com.joint.core.entity.manage;

import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@Entity
@Table(name="ec_archivesdetail")
public class ArchivesDetail extends BaseFlowEntity {
    /**
     * 相关附件
     */
    private List<FileManage> file;
    /**
     * 架
     */
    private String frame;
    /**
     * 柜
     */
    private String cabinet;
    /**
     * 层
     */
    private String layer;
    /**
     * 提交日期
     */
    private Date date;
    /**
     * 档案对象
     */
    private Archives archives;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_archivesdetail_file", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    public String getFrame() {
        return frame;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }

    public String getCabinet() {
        return cabinet;
    }

    public void setCabinet(String cabinet) {
        this.cabinet = cabinet;
    }

    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Archives getArchives() {
        return archives;
    }

    public void setArchives(Archives archives) {
        this.archives = archives;
    }
}
