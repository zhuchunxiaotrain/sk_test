package com.joint.core.entity;

import com.joint.base.entity.Department;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 党费缴纳
 * Created by Administrator on 2017/1/5.
 */
@Entity
@Table(name = "sk_moneyavg")
public class MoneyAvg extends BaseFlowEntity {


    private static final long serialVersionUID = -8248205252131570909L;

    /**
     * 所有支部在这本年度所有月份的总金额
     */
    private BigDecimal allDepartMoney;
    /**
     * 总计党员人数
     */
    private Integer allPartyHuman;
    /**
     * 各个支部分摊详情
     */
    private List<MoneyAvgDetail> moneyAvgDetailList;

    /**
     * 总计分摊金额
     * @return
     */
    private Double allPartyAvgMoney;

    public BigDecimal getAllDepartMoney() {
        return allDepartMoney;
    }

    public void setAllDepartMoney(BigDecimal allDepartMoney) {
        this.allDepartMoney = allDepartMoney;
    }

    public Integer getAllPartyHuman() {
        return allPartyHuman;
    }

    public void setAllPartyHuman(Integer allPartyHuman) {
        this.allPartyHuman = allPartyHuman;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_moneyavg_moneyavgdetaillist",joinColumns =@JoinColumn(name = "id"),inverseJoinColumns = @JoinColumn(name = "moneyAvgDetailListId"))
    @OrderBy("id asc ")
    public List<MoneyAvgDetail> getMoneyAvgDetailList() {
        return moneyAvgDetailList;
    }

    public void setMoneyAvgDetailList(List<MoneyAvgDetail> moneyAvgDetailList) {
        this.moneyAvgDetailList = moneyAvgDetailList;
    }

    public Double getAllPartyAvgMoney() {
        return allPartyAvgMoney;
    }

    public void setAllPartyAvgMoney(Double allPartyAvgMoney) {
        this.allPartyAvgMoney = allPartyAvgMoney;
    }


}
