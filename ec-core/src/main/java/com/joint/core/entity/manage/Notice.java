package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/1/12.
 * 公告通知
 */
@Entity
@Table(name="ec_notice")
public class Notice extends BaseFlowEntity {


    private static final long serialVersionUID = -8277818538544161664L;
    /**
     * 通知主题
     */
    private String topic;

    /**
     * 是否置顶 1置顶 0不置顶
     */
    private boolean top;

    /**
     * 公告类型
     */
    private Dict type;

    /**
     * 发布部门
     */
    private Department department;

    /**
     * 通知对象
     * 1全体员工  2部门 3个人
     */
    private String object;

    /**
     * 通知部门
     */
    private List<Department> noticeDepartment;

    /**
     * 通知个人
     */
    private List<Users> noticeUsers;

    /**
     * 内容
     */
    private String content;

    /**
     * 附件上传
     * @return
     */
    private List<FileManage> file;

    /**
     * 是否具有有效期
     */
    private boolean hasValid;

    /**
     * 有效期
     */
    private Date validDate;

    /**
     * 是否失效
     */
    private boolean invalid;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_notice_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isTop() {
        return top;
    }

    public void setTop(boolean top) {
        this.top = top;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getType() {
        return type;
    }

    public void setType(Dict type) {
        this.type = type;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_notice_noticeUsers", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "noticeUsersId"))
    public List<Users> getNoticeUsers() {
        return noticeUsers;
    }

    public void setNoticeUsers(List<Users> noticeUsers) {
        this.noticeUsers = noticeUsers;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_notice_noticeDepartment", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "noticeDepartmentId"))
    public List<Department> getNoticeDepartment() {
        return noticeDepartment;
    }

    public void setNoticeDepartment(List<Department> noticeDepartment) {
        this.noticeDepartment = noticeDepartment;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getValidDate() {
        return validDate;
    }

    public void setValidDate(Date validDate) {
        this.validDate = validDate;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isHasValid() {
        return hasValid;
    }

    public void setHasValid(boolean hasValid) {
        this.hasValid = hasValid;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }
}
