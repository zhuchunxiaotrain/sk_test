package com.joint.core.entity;

import com.joint.base.entity.Department;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 党费缴纳
 * Created by Administrator on 2017/1/5.
 */
@Entity
@Table(name = "sk_moneyManagerdetail")
public class MoneyManagerDetail extends BaseFlowEntity {


    private static final long serialVersionUID = 3071174524605080081L;

    /**
     * 党费登记表
     */
    private MoneyManager moneyManager;

    /**
     *员工对象
     */
    private Employees  employees;

    /**
     * 本月实缴党费
     */
    private  BigDecimal monthPayDues;

    @ManyToOne(fetch = FetchType.LAZY)
    public MoneyManager getMoneyManager() {
        return moneyManager;
    }

    public void setMoneyManager(MoneyManager moneyManager) {
        this.moneyManager = moneyManager;
    }

    public BigDecimal getMonthPayDues() {
        return monthPayDues;
    }

    public void setMonthPayDues(BigDecimal monthPayDues) {
        this.monthPayDues = monthPayDues;
    }
    @ManyToOne
    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }
}
