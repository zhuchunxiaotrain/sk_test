package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Role;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 干部选拔公告
 * Created by 9Joint on 2016/12/26.
 */
@Entity
@Table(name="sk_selectionnotice")
public class SelectionNotice extends BaseFlowEntity{


    private static final long serialVersionUID = -6208258115975325862L;
    /**
     * 岗位
     */
    private Dict post;
    /**
     * 要求
     */
    private String requires;
    /**
     * 报名周期
     */
    private Date selectDate;
    /**
     * 竞聘方案附件
     */
    private List<FileManage> makePlan;
    /**
     * 竞聘方案文本
     */
    private String makePlanText;
    /**
     * 审核小组
     */
    private Role role;
    /**
     * 备注
     */
    private String remark;

    /**
     * 干部选拔结束标记
     * @return
     */
    private String selectionFinished;

    /**
     * 报名完成标志
     * @return
     */
     private String applyFinished;

    /**
     * 报名申请者
     * @return
     */
    private Users applyUser;


    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getPost() {
        return post;
    }

    public void setPost(Dict post) {
        this.post = post;
    }

    public String getRequires() {
        return requires;
    }

    public void setRequires(String requires) {
        this.requires = requires;
    }

    public Date getSelectDate() {
        return selectDate;
    }

    public void setSelectDate(Date selectDate) {
        this.selectDate = selectDate;
    }
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_selectionnotice_markplan",joinColumns = @JoinColumn(name = "id"),inverseJoinColumns =@JoinColumn (name ="makePlanId"))
    @OrderBy("id desc ")
    public List<FileManage> getMakePlan() {
        return makePlan;
    }

    public void setMakePlan(List<FileManage> makePlan) {
        this.makePlan = makePlan;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @ManyToOne
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
    public String getMakePlanText() {
        return makePlanText;
    }

    public void setMakePlanText(String makePlanText) {
        this.makePlanText = makePlanText;
    }

    public String getSelectionFinished() {
        return selectionFinished;
    }

    public void setSelectionFinished(String selectionFinished) {
        this.selectionFinished = selectionFinished;
    }

    public String getApplyFinished() {
        return applyFinished;
    }

    public void setApplyFinished(String applyFinished) {
        this.applyFinished = applyFinished;
    }
    @ManyToOne
    public Users getApplyUser() {
        return applyUser;
    }

    public void setApplyUser(Users applyUser) {
        this.applyUser = applyUser;
    }
}
