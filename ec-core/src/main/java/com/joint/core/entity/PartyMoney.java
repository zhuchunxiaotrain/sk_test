package com.joint.core.entity;

import com.joint.base.entity.Department;
import com.joint.base.entity.Power;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/2/8.
 */
@Entity
@Table(name = "sk_partymoney")
public class PartyMoney extends BaseFlowEntity {

    private static final long serialVersionUID = -6384145414289536477L;
    /**
     * 缴纳党费时间
     */
    private Date payDate;
    /**
     * 缴纳的年份
     */
    private String payDateYear;
    /**
     * 缴纳的月份
     */
    private String  payDateMonth;
    /**
     * 是否是党员
     */
    private String isParty;
    /**
     * 用户
     */
    private Users users;
    /**
     * 用户所属于的部门
     */
    private Department department;

    /**
     * 每个月应缴党费
     */
    private BigDecimal everyPay;

    /**
     * 党费详情
     * @return
     *//*
    private List<PartyMoneyDetail> partyMoneyDetailList;*/

    public String getIsParty() {
        return isParty;
    }

    public void setIsParty(String isParty) {
        this.isParty = isParty;
    }
    @ManyToOne
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
    @ManyToOne
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }



    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }
    /*@ManyToMany
    @JoinTable(name = "sk_partymoney_partymoneydetaillist", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "partyMoneyDetailListId"))
    @OrderBy("id asc ")
    public List<PartyMoneyDetail> getPartyMoneyDetailList() {
        return partyMoneyDetailList;
    }

    public void setPartyMoneyDetailList(List<PartyMoneyDetail> partyMoneyDetailList) {
        this.partyMoneyDetailList = partyMoneyDetailList;
    }*/

    public BigDecimal getEveryPay() {
        return everyPay;
    }

    public void setEveryPay(BigDecimal everyPay) {
        this.everyPay = everyPay;
    }

    public String getPayDateYear() {
        return payDateYear;
    }

    public void setPayDateYear(String payDateYear) {
        this.payDateYear = payDateYear;
    }

    public String getPayDateMonth() {
        return payDateMonth;
    }

    public void setPayDateMonth(String payDateMonth) {
        this.payDateMonth = payDateMonth;
    }
}
