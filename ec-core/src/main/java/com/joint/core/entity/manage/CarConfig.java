package com.joint.core.entity.manage;

import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
@Entity
@Table(name="ec_carconfig")
public class CarConfig extends BaseFlowEntity {
    /**
     * 车辆品牌
     */
    private String brand;
    /**
     * 车辆型号
     */
    private String type;
    /**
     * 车辆号牌
     */
    private String carNo;
    /**
     * 当前里程数
     */
    private int kilometre;
    /**
     * 加油时间
     */
    private Date oilDate;
    /**
     * 加油金额
     */
    private Double oilMoney;
    /**
     * 当前累计加油金额
     */
    private Double oilMoneyNum;
    /**
     * 备注
     */
    private String remarks;


    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }

    public int getKilometre() {
        return kilometre;
    }

    public void setKilometre(int kilometre) {
        this.kilometre = kilometre;
    }

    public Date getOilDate() {
        return oilDate;
    }

    public void setOilDate(Date oilDate) {
        this.oilDate = oilDate;
    }

    public Double getOilMoney() {
        return oilMoney;
    }

    public void setOilMoney(Double oilMoney) {
        this.oilMoney = oilMoney;
    }

    public Double getOilMoneyNum() {
        return oilMoneyNum;
    }

    public void setOilMoneyNum(Double oilMoneyNum) {
        this.oilMoneyNum = oilMoneyNum;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
