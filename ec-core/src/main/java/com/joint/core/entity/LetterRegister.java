package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/1/5.
 */
@Entity
@Table(name = "sk_letterregister")
public class LetterRegister extends BaseFlowEntity {


    private static final long serialVersionUID = 6068008544566488639L;
    /**
     * 年份
     */
    private String year;
    /**
     * 月份
     */
    private String month;

    /**
     * 信访类型
     */
    private Dict letterType;
    /**
     *信访开始日期
     */
    private Date letterStartDate;
    /**
     * 信访结束日期
     */
    private Date letterEndDate;
    /**
     * 信访件处理结果
     */
    private BigDecimal workSummary;

    /**
     * 信访件处理评价
     */
    private String letterEvaluate;
    @ManyToOne
    public Dict getLetterType() {
        return letterType;
    }

    public void setLetterType(Dict letterType) {
        this.letterType = letterType;
    }

    public BigDecimal getWorkSummary() {
        return workSummary;
    }

    public void setWorkSummary(BigDecimal workSummary) {

        this.workSummary = workSummary;
    }

    public Date getLetterStartDate() {
        return letterStartDate;
    }

    public void setLetterStartDate(Date letterStartDate) {
        this.letterStartDate = letterStartDate;
    }

    public Date getLetterEndDate() {
        return letterEndDate;
    }

    public void setLetterEndDate(Date letterEndDate) {
        this.letterEndDate = letterEndDate;
    }

    public String getLetterEvaluate() {
        return letterEvaluate;
    }

    public void setLetterEvaluate(String letterEvaluate) {
        this.letterEvaluate = letterEvaluate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }


}
