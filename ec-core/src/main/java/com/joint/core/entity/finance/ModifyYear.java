package com.joint.core.entity.finance;

import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/7.
 */
@Entity
@Table(name="ec_modifyyear")
public class ModifyYear extends BaseFlowEntity {
    /**
     * 当前年份
     */
    private String year;
    /**
     * 原全年预算报表
     */
    private CalculationYear calculationYear;
    /**
     * 全年预算报表
     */
    private List<FileManage> newBudget;
    /**
     * 是否失效
     */
    private boolean invalid;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @OneToOne(fetch = FetchType.LAZY)
    public CalculationYear getCalculationYear() {
        return calculationYear;
    }

    public void setCalculationYear(CalculationYear calculationYear) {
        this.calculationYear = calculationYear;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_modifyyear_newbudget", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="newbudgetId"))
    public List<FileManage> getNewBudget() {
        return newBudget;
    }

    public void setNewBudget(List<FileManage> newBudget) {
        this.newBudget = newBudget;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }
}
