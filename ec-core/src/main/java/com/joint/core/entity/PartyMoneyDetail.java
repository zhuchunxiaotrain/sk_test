package com.joint.core.entity;

import com.joint.base.entity.Department;
import com.joint.base.entity.Power;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Administrator on 2017/2/8.
 */
@Entity
@Table(name = "sk_partymoneydetail")
public class PartyMoneyDetail extends BaseFlowEntity {


    private static final long serialVersionUID = -4306213980373621061L;
    /**
     * 用户所属于的职权
     */
    private Power power;
    /**
     * 每个月应缴党费
     */
    private BigDecimal everyPay;

    public BigDecimal getEveryPay() {
        return everyPay;
    }

    public void setEveryPay(BigDecimal everyPay) {

        this.everyPay = everyPay;
    }

    @ManyToOne
    public Power getPower() {
        return power;
    }

    public void setPower(Power power) {
        this.power = power;
    }
}
