package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Administrator on 2017/1/5.
 */
@Entity
@Table(name = "sk_annualbudgetdetail")
public class AnnualBudgetDetail extends BaseFlowEntity {

    private static final long serialVersionUID = 656653932992154586L;
    /**
     * 年度预算申请对象
     */
    private AnnualBudget annualBudget;
    /**
    * 年度预算科目
     */
    private Dict annualBudgetItem;
    /**
     * 科目预算金额
     */
    private Double budgetMoney;
    /**科目已用金额
     *
     */
    private Double usedMoney;
    /**
     * 科目剩余金额
     */
    private Double enableMoney;
    /**
     * 备注
     */
    private  String remark;
    @OneToOne
    public Dict getAnnualBudgetItem() {
        return annualBudgetItem;
    }

    public void setAnnualBudgetItem(Dict annualBudgetItem) {
        this.annualBudgetItem = annualBudgetItem;
    }

    public Double getBudgetMoney() {
        return budgetMoney;
    }

    public void setBudgetMoney(Double budgetMoney) {
        this.budgetMoney = budgetMoney;
    }

    public Double getUsedMoney() {
        return usedMoney;
    }

    public void setUsedMoney(Double usedMoney) {
        this.usedMoney = usedMoney;
    }

    public Double getEnableMoney() {
        return enableMoney;
    }

    public void setEnableMoney(Double enableMoney) {
        this.enableMoney = enableMoney;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public AnnualBudget getAnnualBudget() {
        return annualBudget;
    }

    public void setAnnualBudget(AnnualBudget annualBudget) {
        this.annualBudget = annualBudget;
    }
}
