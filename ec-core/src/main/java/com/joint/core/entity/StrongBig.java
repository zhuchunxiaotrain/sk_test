package com.joint.core.entity;

import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;
import com.joint.core.entity.manage.MeetingUse;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Entity
@Table(name="sk_strongbig")
public class StrongBig extends BaseFlowEntity {


    private static final long serialVersionUID = -1420693423849153956L;
    /**
     * 年份
     */
    private String year;
    /**
     * 月份
     */
    private String month;
    /**
     * 登记事宜名称
     */
    private String registerName;
    /**
     *三重一大会议类型
     */
    private String type;
    /**
     * 是否需要提交中心审核
     */
    private String isCheck;
    /**
     * 备案
     */
    private String strongBigRecord;
    /**
     * 结论
     */
    private String conclusion;
    /**
     * 通知对象
     */
    private List<Users> tellUsers;
    /**
     * 相应会议记录
     */
    private List<MeetingUse> meetingUse;
    /**
     * 三中一大附件
     */
    private List<FileManage> attchment;
    /**
     *三中一大文本
     */
    private String attchmentText;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStrongBigRecord() {
        return strongBigRecord;
    }

    public void setStrongBigRecord(String strongBigRecord) {
        this.strongBigRecord = strongBigRecord;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }



    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_strongbig_tellusers",joinColumns = @JoinColumn(name = "id"),inverseJoinColumns =@JoinColumn (name ="tellUsersId"))
    @OrderBy("id desc ")
    public List<Users> getTellUsers() {
        return tellUsers;
    }

    public void setTellUsers(List<Users> tellUsers) {
        this.tellUsers = tellUsers;
    }



    public String getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(String isCheck) {
        this.isCheck = isCheck;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_strongbig_attchment",joinColumns = @JoinColumn(name = "id"),inverseJoinColumns =@JoinColumn (name ="attchmentId"))
    @OrderBy("id desc ")
    public List<FileManage> getAttchment() {
        return attchment;
    }

    public void setAttchment(List<FileManage> attchment) {
        this.attchment = attchment;
    }

    public String getAttchmentText() {
        return attchmentText;
    }

    public void setAttchmentText(String attchmentText) {
        this.attchmentText = attchmentText;
    }

    public String getRegisterName() {
        return registerName;
    }

    public void setRegisterName(String registerName) {
        this.registerName = registerName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_strongbig_meetinguse",joinColumns = @JoinColumn(name = "id"),inverseJoinColumns =@JoinColumn (name ="meetingUseId"))
    @OrderBy("id desc ")
    public List<MeetingUse> getMeetingUse() {
        return meetingUse;
    }

    public void setMeetingUse(List<MeetingUse> meetingUse) {
        this.meetingUse = meetingUse;
    }
}
