package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/1/12.
 * 资料登记表
 */
@Entity
@Table(name="ec_registe")
public class Registe extends BaseFlowEntity {


    private static final long serialVersionUID = 3916771229002243082L;
    /**
     * 资料类别
     */
    private Dict type;
    /**
     * 资料说明
     */
    private String dataExplain;

    /**
     * 资料名称
     */
    private String dataName;
    /**
     * 附件上传
     * @return
     */
    private List<FileManage> file;

    /**
     * 是否失效
     */
    private boolean invalid;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_registe_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getType() {
        return type;
    }

    public void setType(Dict type) {
        this.type = type;
    }

    public String getDataExplain() {
        return dataExplain;
    }

    public void setDataExplain(String dataExplain) {
        this.dataExplain = dataExplain;
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }
}
