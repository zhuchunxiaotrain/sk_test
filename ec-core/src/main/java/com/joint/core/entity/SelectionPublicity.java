package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 *结果公示
 * Created by 9Joint on 2016/12/26.
 */
@Entity
@Table(name="sk_selectionpublicity")
public class SelectionPublicity extends BaseFlowEntity{

    private static final long serialVersionUID = -2588686268154104697L;
    /**
     * 竞聘演讲对象
     */
    private SelectionSpeech selectionSpeech;
    /**
     * 公告对象
     */
    private SelectionNotice selectionNotice;
    /**
     * 结果公示人员
     */
    private Users applyUsers;
    /**
     * 结果公示
     */
    private List<FileManage> attchment;
    /**
     * 结果公示文本
     */
    private String attchmentText;
    /**
     * 党总支人事决策会议
     */
    private List<FileManage> meetingDecisionAttchment;
    /**
     * 党总支人事决策会议文本
     */
    private String meetingDecisionAttchmentText;
    /**
     * 公示开始日期
     */
    private Date publicStartDate;
    /**
     * 公示结束日期
     */
    private Date publicEndDate;
    /**
     * 公示结果
     * 0:不通过，1：通过
     */
    private String result;
    @OneToOne
    public SelectionSpeech getSelectionSpeech() {
        return selectionSpeech;
    }

    public void setSelectionSpeech(SelectionSpeech selectionSpeech) {
        this.selectionSpeech = selectionSpeech;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_selectionpublicity_attchment",joinColumns = @JoinColumn(name = "id"),inverseJoinColumns =@JoinColumn (name ="attchmentId"))
    @OrderBy("id desc ")
    public List<FileManage> getAttchment() {
        return attchment;
    }

    public void setAttchment(List<FileManage> attchment) {
        this.attchment = attchment;
    }


    public String getAttchmentText() {
        return attchmentText;
    }

    public void setAttchmentText(String attchmentText) {
        this.attchmentText = attchmentText;
    }

    public Date getPublicStartDate() {
        return publicStartDate;
    }

    public void setPublicStartDate(Date publicStartDate) {
        this.publicStartDate = publicStartDate;
    }

    public Date getPublicEndDate() {
        return publicEndDate;
    }

    public void setPublicEndDate(Date publicEndDate) {
        this.publicEndDate = publicEndDate;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_selectionpublicity_meetingdecisionattchment",joinColumns = @JoinColumn(name = "id"),inverseJoinColumns =@JoinColumn (name ="meetingDecisionAttchmentId"))
    @OrderBy("id desc ")
    public List<FileManage> getMeetingDecisionAttchment() {
        return meetingDecisionAttchment;
    }

    public void setMeetingDecisionAttchment(List<FileManage> meetingDecisionAttchment) {
        this.meetingDecisionAttchment = meetingDecisionAttchment;
    }

    public String getMeetingDecisionAttchmentText() {
        return meetingDecisionAttchmentText;
    }

    public void setMeetingDecisionAttchmentText(String meetingDecisionAttchmentText) {
        this.meetingDecisionAttchmentText = meetingDecisionAttchmentText;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
    @ManyToOne
    public SelectionNotice getSelectionNotice() {
        return selectionNotice;
    }

    public void setSelectionNotice(SelectionNotice selectionNotice) {
        this.selectionNotice = selectionNotice;
    }

    @ManyToOne
    public Users getApplyUsers() {
        return applyUsers;
    }

    public void setApplyUsers(Users applyUsers) {
        this.applyUsers = applyUsers;
    }
}
