package com.joint.core.entity;

import com.fz.us.base.bean.BaseEnum;
import com.joint.base.entity.Department;
import com.joint.base.entity.Post;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;

/**
 * Created by root on 16/12/29.
 */
@Entity
@Table(name="ec_employment")
public class Employment extends BaseFlowEntity {

    /**
     * 用工性质
     */
    private String  nature;

    /**
     * 录用岗位
     */
    private Post post;

    /**
     * 性别(0男 1女)
     */
    private BaseEnum.SexEnum sex;

    /**
     * 员工工号
     */
    private String staffid;

    /**
     * ID
     */
    private String cardID;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 应聘部门
     */
    private Department department;

    /**
     * 备注
     */
     private String remark;

    /**
     *面试
     * @return
     */
    private Manage manage;

    /**
     * 人员
     * @return
     */
    private Employees employees;

    /**
     * 学历
     * @return
     */
    private String edu;


    @ManyToOne(fetch = FetchType.LAZY)
    public Manage getManage() {
        return manage;
    }

    public void setManage(Manage manage) {
        this.manage = manage;
    }


    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    @Column
    @Enumerated(EnumType.STRING)
    public BaseEnum.SexEnum getSex() {
        return sex;
    }

    public void setSex(BaseEnum.SexEnum sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    @OneToOne(fetch = FetchType.LAZY)
    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }
}
