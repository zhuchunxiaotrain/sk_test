package com.joint.core.entity.manage;

import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
@Entity
@Table(name="ec_qualificationsdict")
public class QualificationsDict extends BaseFlowEntity {
    private static final long serialVersionUID = -3373547070410431329L;
}
