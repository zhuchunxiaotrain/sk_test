package com.joint.core.entity;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Post;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_employees")
public class Employees extends BaseFlowEntity {

    /**
     * 人员
     */
    private Users users;

    /**
     * 用人性质
     */
    private String  nature;

    /**
     * 合同开始日期
     */
    private Date startDate;

    /**
     * 合同结束日期
     */
    private Date endDate;

    /**
     * 试用开始时间
     */
    private Date trialStart;

    /**
     * 试用结束时间
     */
    private Date trialEnd;

    /**
     * 续签合同开始时间
     */
    private Date renewalStart;

    /**
     * 续签合同结束时间
     */
    private Date renewalEnd;

    /**
     * 续签薪资
     */
    private BigDecimal renewMoney;

    /**
     * 下一步审批人的Name的集合，用逗号分隔
     */
    private String nextStepApproversName;

    /**
     * 下一步审批人的id的集合，用逗号分隔
     */
    private String nextStepApproversId;

    /**
     * 所属部门
     */
    private Department dep;

    /**
     * 岗位
     */
    private Post empPost;

    /**
     * 基本薪资
     */
    private BigDecimal money;

    /**
     * 性别
     */
    private BaseEnum.SexEnum sex;

    /**
     * 性别
     */
    private String gender;

    /**
     * 年龄
     */
    private String age;

    /**
     * 籍贯
     */
    private String nativePlace;

    /**
     * 民族
     */
    private String nation;

    /**
     * 出生日期
     */
    private Date birthDate;
    private String mon;

    /**
     * 户口所在地
     */
    private String houseLocation;

    /**
     * 居住证开始日期
     */
    private Date cardStart;

    /**
     * 居住证结束日期
     */
    private Date cardEnd;

    /**
     * 居住证登记
     */
    private String cardRegister;

    /**
     * 身份证号
     */
    private String cardID;

    /**
     * 婚姻情况
     */
    private String maritalStatus;

    /**
     * 电话
     */
    private String mobile;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 最高学历
     */
    private Dict highestDegree;

    /**
     * 毕业院校
     */
    private String school;

    /**
     * 所学专业
     */
    private String profession;

    /**
     * 参加工作时间
     */
    private Date workTime;

    /**
     * 学历
     * @return
     */
    private String edu;

    /**
     * 户籍地址
     */
    private String permanentAddress;

    /**
     * 联系电话
     */
    private String tel;

    /**
     * 邮编地址
     */
    private String postcode;

    /**
     * 居住地址
     */
    private String address;

    /**
     * 紧急联系人
     */
    private String contactLinkman;

    /**
     * 紧急联络电话
     */
    private String contactTel;

    /**
     * 邮编地址
     */
    private String postcode1;

    /**
     * 兴趣特长与技能
     */
    private String others;

    /**
     * 身份证复印件
     */
    private Set<FileManage> copyIDCard;

    /**
     * 照片
     */
    private FileManage headImage;

    /**
     * 审核
     * @return
     */
    private Employment employment;

    /**
     *
     * @return
     */
    private Set<Family> familySet;

    /**
     * 是否党员(0/1)
     */
    private String partyMember;

    /**
     * 入党时间
     */
    private Date partyDate;

    /**
     * 介绍人
     */
    private String introducer;

    /**
     * 工会费
     */
    private BigDecimal dues;

    /**
     * 党费缴纳基数
     */
    private BigDecimal baseMoney;

    /**
     * 奖惩记录
     * @return
     */
    private Set<Records> records;

    /**
     * 续签
     * @return
     */
    private Renew renew;

    /**
     * 是否在职(0在职 1离职,2退休)
     * @return
     */
    private String ifJob;

    /**
     * 员工工号
     */
    private String staffid;

    /**
     * 备注
     */
    private String remark;

    private LeaveDetail leaveDetail;

    @OneToOne(fetch = FetchType.LAZY)
    public LeaveDetail getLeaveDetail() {
        return leaveDetail;
    }

    public void setLeaveDetail(LeaveDetail leaveDetail) {
        this.leaveDetail = leaveDetail;
    }

    @OneToMany(fetch = FetchType.LAZY,mappedBy ="employees")
    public Set<Records> getRecords() {
        return records;
    }

    public void setRecords(Set<Records> records) {
        this.records = records;
    }

    @OneToMany(fetch = FetchType.LAZY,mappedBy ="employees")
    public Set<Family> getFamilySet() {
        return familySet;
    }

    public void setFamilySet(Set<Family> familySet) {
        this.familySet = familySet;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getTrialStart() {
        return trialStart;
    }

    public void setTrialStart(Date trialStart) {
        this.trialStart = trialStart;
    }

    public Date getTrialEnd() {
        return trialEnd;
    }

    public void setTrialEnd(Date trialEnd) {
        this.trialEnd = trialEnd;
    }
    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDep() {
        return dep;
    }

    public void setDep(Department dep) {
        this.dep = dep;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Post getEmpPost() {
        return empPost;
    }

    public void setEmpPost(Post empPost) {
        this.empPost = empPost;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    @Column
    @Enumerated(EnumType.STRING)
    public BaseEnum.SexEnum getSex() {
        return sex;
    }

    public void setSex(BaseEnum.SexEnum sex) {
        this.sex = sex;
    }
    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getHouseLocation() {
        return houseLocation;
    }

    public void setHouseLocation(String houseLocation) {
        this.houseLocation = houseLocation;
    }

    public Date getCardStart() {
        return cardStart;
    }

    public void setCardStart(Date cardStart) {
        this.cardStart = cardStart;
    }

    public Date getCardEnd() {
        return cardEnd;
    }

    public void setCardEnd(Date cardEnd) {
        this.cardEnd = cardEnd;
    }

    public String getCardRegister() {
        return cardRegister;
    }

    public void setCardRegister(String cardRegister) {
        this.cardRegister = cardRegister;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getHighestDegree() {
        return highestDegree;
    }

    public void setHighestDegree(Dict highestDegree) {
        this.highestDegree = highestDegree;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Date getWorkTime() {
        return workTime;
    }

    public void setWorkTime(Date workTime) {
        this.workTime = workTime;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactLinkman() {
        return contactLinkman;
    }

    public void setContactLinkman(String contactLinkman) {
        this.contactLinkman = contactLinkman;
    }

    public String getContactTel() {
        return contactTel;
    }

    public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }


    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getPostcode1() {
        return postcode1;
    }

    public void setPostcode1(String postcode1) {
        this.postcode1 = postcode1;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="ec_employess_copyIDCard",joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="copyIDCardId"))
    @OrderBy("name asc")
    public Set<FileManage> getCopyIDCard() {
        return copyIDCard;
    }

    public void setCopyIDCard(Set<FileManage> copyIDCard) {
        this.copyIDCard = copyIDCard;
    }


    @OneToOne(fetch = FetchType.LAZY)
    public FileManage getHeadImage() {
        return headImage;
    }

    public void setHeadImage(FileManage headImage) {
        this.headImage = headImage;
    }
    @OneToOne(fetch = FetchType.LAZY)
    public Employment getEmployment() {
        return employment;
    }

    public void setEmployment(Employment employment) {
        this.employment = employment;
    }

    public String getPartyMember() {
        return partyMember;
    }

    public void setPartyMember(String partyMember) {
        this.partyMember = partyMember;
    }

    public Date getPartyDate() {
        return partyDate;
    }

    public void setPartyDate(Date partyDate) {
        this.partyDate = partyDate;
    }

    public String getIntroducer() {
        return introducer;
    }

    public void setIntroducer(String introducer) {
        this.introducer = introducer;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @OneToOne
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Date getRenewalStart() {
        return renewalStart;
    }

    public void setRenewalStart(Date renewalStart) {
        this.renewalStart = renewalStart;
    }

    public Date getRenewalEnd() {
        return renewalEnd;
    }

    public void setRenewalEnd(Date renewalEnd) {
        this.renewalEnd = renewalEnd;
    }

    public BigDecimal getRenewMoney() {
        return renewMoney;
    }

    public void setRenewMoney(BigDecimal renewMoney) {
        this.renewMoney = renewMoney;
    }

    public String getNextStepApproversName() {
        return nextStepApproversName;
    }

    public void setNextStepApproversName(String nextStepApproversName) {
        this.nextStepApproversName = nextStepApproversName;
    }

    public String getNextStepApproversId() {
        return nextStepApproversId;
    }

    public void setNextStepApproversId(String nextStepApproversId) {
        this.nextStepApproversId = nextStepApproversId;
    }
    @OneToOne
    public Renew getRenew() {
        return renew;
    }

    public void setRenew(Renew renew) {
        this.renew = renew;
    }

    public String getEdu() {
        return edu;
    }
    public void setEdu(String edu) {
        this.edu = edu;
    }

    public BigDecimal getDues() {
        return dues;
    }

    public void setDues(BigDecimal dues) {
        this.dues = dues;
    }

    public BigDecimal getBaseMoney() {
        return baseMoney;
    }

    public void setBaseMoney(BigDecimal baseMoney) {
        this.baseMoney = baseMoney;
    }

    public String getIfJob() {
        return ifJob;
    }

    public void setIfJob(String ifJob) {
        this.ifJob = ifJob;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

}
