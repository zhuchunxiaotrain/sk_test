package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_family")
public class Family extends BaseFlowEntity {

    /**
     * 称谓
     */
    private String appellation;

    /**
     * 年龄
     */
    private String age;

    /**
     * 政治面貌
     */
    private String politicalStatus;

    /**
     * 工作单位
     */
    private String workAdd;

    /**
     * 职务
     */
    private String  post;

    /**
     * 联系电话
     */
    private String tel;

    /**
     *
     * @return
     */
    private Employees employees;

    @ManyToOne(fetch = FetchType.LAZY)
    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public String getAppellation() {
        return appellation;
    }

    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPoliticalStatus() {
        return politicalStatus;
    }

    public void setPoliticalStatus(String politicalStatus) {
        this.politicalStatus = politicalStatus;
    }

    public String getWorkAdd() {
        return workAdd;
    }

    public void setWorkAdd(String workAdd) {
        this.workAdd = workAdd;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
