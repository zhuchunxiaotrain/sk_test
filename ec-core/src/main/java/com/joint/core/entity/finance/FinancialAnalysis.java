package com.joint.core.entity.finance;

import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
@Entity
@Table(name="ec_financialanalysis")
public class FinancialAnalysis extends BaseFlowEntity {
    /**
     * 所属季度
     */
    private int quarter;
    /**
     * 财务分析附件
     */
    private List<FileManage> file;
    /**
     * 所属年份
     */
    private String year;


    public int getQuarter() {
        return quarter;
    }

    public void setQuarter(int quarter) {
        this.quarter = quarter;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_financialanalysis_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
