package com.joint.core.entity;

import com.fz.us.base.bean.BaseEnum;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Post;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Created by root on 16/12/16.
 */
@Entity
@Table(name="ec_leave")
public class Leave extends BaseFlowEntity{

    /**
     * 请假类型(0年假,1病假,2事假,3其他,)
     */
    private String type;

    /**
     * 所属年份
     */
    private Integer time;

    /**
     * 请假天数
     */
    private BigDecimal dayNum;

    /**
     * 今年年假
     */
    private BigDecimal annuaLeave;

    /**
     * 今年年假(已用)
     */
    private BigDecimal usedAnnuaLeave;

    /**
    * 今年年假(可用)
    */
    private BigDecimal unusedAnnuaLeave;

    /**
    * 已申请病假
    */
    private BigDecimal sickLeave;

    /**
    * 已申请事假
    */
    private BigDecimal thingLeave;

    /**
    * 已申请其他假期
    */
    private BigDecimal othersLeave;

    /**
    * 当年已申请天数
    */
    private BigDecimal usedDayNum;

    /**
     * 工作交接安排
     */
    private List<FileManage> file;

    /**
     * 下一步审批人的Name的集合，用逗号分隔
     */
    private String nextStepApproversName;

    /**
     * 下一步审批人的id的集合，用逗号分隔
     */
    private String nextStepApproversId;

    /**
     * 产业中心人员(0不是 1是)
     */
    private int ifCentralStaff;

    /**
     * 部门总经理(0不是 1是)
     */
    private int ifManager;

    /**
     * 员工基本信息
     */
    private Employees employees;

    @OneToOne(fetch = FetchType.LAZY)
    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public BigDecimal getDayNum() {
        return dayNum;
    }

    public void setDayNum(BigDecimal dayNum) {
        this.dayNum = dayNum;
    }

    public BigDecimal getAnnuaLeave() {
        return annuaLeave;
    }

    public void setAnnuaLeave(BigDecimal annuaLeave) {
        this.annuaLeave = annuaLeave;
    }

    public BigDecimal getUsedAnnuaLeave() {
        return usedAnnuaLeave;
    }

    public void setUsedAnnuaLeave(BigDecimal usedAnnuaLeave) {
        this.usedAnnuaLeave = usedAnnuaLeave;
    }

    public BigDecimal getUnusedAnnuaLeave() {
        return unusedAnnuaLeave;
    }

    public void setUnusedAnnuaLeave(BigDecimal unusedAnnuaLeave) {
        this.unusedAnnuaLeave = unusedAnnuaLeave;
    }

    public BigDecimal getSickLeave() {
        return sickLeave;
    }

    public void setSickLeave(BigDecimal sickLeave) {
        this.sickLeave = sickLeave;
    }

    public BigDecimal getThingLeave() {
        return thingLeave;
    }

    public void setThingLeave(BigDecimal thingLeave) {
        this.thingLeave = thingLeave;
    }

    public BigDecimal getOthersLeave() {
        return othersLeave;
    }

    public void setOthersLeave(BigDecimal othersLeave) {
        this.othersLeave = othersLeave;
    }

    public BigDecimal getUsedDayNum() {
        return usedDayNum;
    }

    public void setUsedDayNum(BigDecimal usedDayNum) {
        this.usedDayNum = usedDayNum;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_leave_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }

    public int getIfManager() {
        return ifManager;
    }

    public void setIfManager(int ifManager) {
        this.ifManager = ifManager;
    }

    public String getNextStepApproversName() {
        return nextStepApproversName;
    }

    public void setNextStepApproversName(String nextStepApproversName) {
        this.nextStepApproversName = nextStepApproversName;
    }

    public String getNextStepApproversId() {
        return nextStepApproversId;
    }

    public void setNextStepApproversId(String nextStepApproversId) {
        this.nextStepApproversId = nextStepApproversId;
    }
}
