package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_summary")
public class Summary extends BaseFlowEntity {

    /**
     * 所属培训
     */
    private Dict type;

    /**
     * 培训评估
     * @return
     */
    private List<FileManage> assessment;

    /**
     * 课程讲师
     * @return
     */
    private List<Users> teacher;

    /**
     * 招聘表
     * @return
     */
    private Train train;

    @ManyToOne(fetch = FetchType.LAZY)
    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_summary_assessment", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="assessmentId"))
    public List<FileManage> getAssessment() {
        return assessment;
    }

    public void setAssessment(List<FileManage> assessment) {
        this.assessment = assessment;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_summary_teacher", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "teacherId"))
    public List<Users> getTeacher() {
        return teacher;
    }

    public void setTeacher(List<Users> teacher) {
        this.teacher = teacher;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getType() {
        return type;
    }

    public void setType(Dict type) {
        this.type = type;
    }
}
