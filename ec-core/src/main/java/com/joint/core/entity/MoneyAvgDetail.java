package com.joint.core.entity;

import com.joint.base.entity.Department;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * 党费缴纳
 * Created by Administrator on 2017/1/5.
 */
@Entity
@Table(name = "sk_moneyavgdetail")
public class MoneyAvgDetail extends BaseFlowEntity {


    private static final long serialVersionUID = 7219837143947416481L;
    /**
     * 支部名称
     */
    private Department department;
    /**
     * 支部党员人数
     */
    private Integer departmentHuman;
    /**
     * 分摊金额
     */
    private Double departAvgMoney;

    /**
     * 单个部门累计可支配党费金额(逐年累加)
     */
    private Double departAllEnableMoney;

    /**
     * 单个部门累计已使用支配党费金额(部门上缴支出一次，更新一次)
     * @return
     */
    private Double departEveryUseDetail;

    /**
     * 单个部门剩余可使用支配党费金额(部门上缴支出一次，更新一次)
     * @return
     */
    private Double departResidueEnableMoney;

    /**
     * 党费平均的年份
     * @return
     */
    private String avgYear;


    @ManyToOne
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Integer getDepartmentHuman() {
        return departmentHuman;
    }

    public void setDepartmentHuman(Integer departmentHuman) {
        this.departmentHuman = departmentHuman;
    }

    public Double getDepartAvgMoney() {
        return departAvgMoney;
    }

    public void setDepartAvgMoney(Double departAvgMoney) {
        this.departAvgMoney = departAvgMoney;
    }

    public Double getDepartAllEnableMoney() {
        return departAllEnableMoney;
    }

    public void setDepartAllEnableMoney(Double departAllEnableMoney) {
        this.departAllEnableMoney = departAllEnableMoney;
    }

    public Double getDepartEveryUseDetail() {
        return departEveryUseDetail;
    }

    public void setDepartEveryUseDetail(Double departEveryUseDetail) {
        this.departEveryUseDetail = departEveryUseDetail;
    }

    public Double getDepartResidueEnableMoney() {
        return departResidueEnableMoney;
    }

    public void setDepartResidueEnableMoney(Double departResidueEnableMoney) {
        this.departResidueEnableMoney = departResidueEnableMoney;
    }

    public String getAvgYear() {
        return avgYear;
    }

    public void setAvgYear(String avgYear) {
        this.avgYear = avgYear;
    }
}
