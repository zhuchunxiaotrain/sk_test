package com.joint.core.entity.manage;

import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@Entity
@Table(name="ec_contract")
public class Contract extends BaseFlowEntity {
    /**
     * 经办部门
     */
    private Department department;
    /**
     * 经办人
     */
    private String manager;
    /**
     * 经办人职务
     */
    private String managerPost;
    /**
     * 合同甲方
     */
    private String contractA;
    /**
     * 合同一方
     */
    private String contractB;
    /**
     * 合同第三方
     */
    private String contractC;
    /**
     * 合同其他方
     */
    private String contractD;
    /**
     * 预算编号
     */
    private String budgetNo;
    /**
     * 合同标的（小写）
     */
    private String subjectSmall;
    /**
     * 合同标的（大写）
     */
    private String subjectBig;
    /**
     * 合同日期
     */
    private Date date;
    /**
     * 主要事宜
     */
    private String mainThing;
    /**
     * 合同附件
     */
    private List<FileManage> file;
    /**
     * 是否需要会签
     */
    private boolean mulExam;
    /**
     * 会签人员
     */
    private List<Users> examUsers;
    /**
     * 是否需要上报中心
     */
    private boolean leaderExam;
    /**
     * 是否是产业中心人员(0不是 1是)
     */
    private int ifCentralStaff;
    /**
     * 是否需要党政联合会议
     */
    private boolean partyExam;

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getManagerPost() {
        return managerPost;
    }

    public void setManagerPost(String managerPost) {
        this.managerPost = managerPost;
    }

    public String getContractA() {
        return contractA;
    }

    public void setContractA(String contractA) {
        this.contractA = contractA;
    }

    public String getContractB() {
        return contractB;
    }

    public void setContractB(String contractB) {
        this.contractB = contractB;
    }

    public String getContractC() {
        return contractC;
    }

    public void setContractC(String contractC) {
        this.contractC = contractC;
    }

    public String getContractD() {
        return contractD;
    }

    public void setContractD(String contractD) {
        this.contractD = contractD;
    }

    public String getBudgetNo() {
        return budgetNo;
    }

    public void setBudgetNo(String budgetNo) {
        this.budgetNo = budgetNo;
    }

    public String getSubjectSmall() {
        return subjectSmall;
    }

    public void setSubjectSmall(String subjectSmall) {
        this.subjectSmall = subjectSmall;
    }

    public String getSubjectBig() {
        return subjectBig;
    }

    public void setSubjectBig(String subjectBig) {
        this.subjectBig = subjectBig;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMainThing() {
        return mainThing;
    }

    public void setMainThing(String mainThing) {
        this.mainThing = mainThing;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_contract_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    public boolean isMulExam() {
        return mulExam;
    }

    public void setMulExam(boolean mulExam) {
        this.mulExam = mulExam;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_contract_examUsers", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "examUsersId"))
    public List<Users> getExamUsers() {
        return examUsers;
    }

    public void setExamUsers(List<Users> examUsers) {
        this.examUsers = examUsers;
    }

    public boolean isLeaderExam() {
        return leaderExam;
    }

    public void setLeaderExam(boolean leaderExam) {
        this.leaderExam = leaderExam;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }

    public boolean isPartyExam() {
        return partyExam;
    }

    public void setPartyExam(boolean partyExam) {
        this.partyExam = partyExam;
    }
}
