package com.joint.core.entity;

import com.joint.base.entity.Department;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/1/5.
 */
@Entity
@Table(name = "sk_annualbudget")
public class AnnualBudget extends BaseFlowEntity {

    private static final long serialVersionUID = 9105696510630318760L;
    /**
     * 申请的年份
     */
    private String annualBudgetYear;
    /**
     * 所属单位
     */
    private Department department;
    /**
     *申请预算详情
     */
    private List<AnnualBudgetDetail> annualBudgetDetailList;
    /**
     * 年度预算总金额
     */
    private BigDecimal annualBudgetMoney;

    @ManyToOne
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "annualBudget")

    public List<AnnualBudgetDetail> getAnnualBudgetDetailList() {
        return annualBudgetDetailList;
    }

    public void setAnnualBudgetDetailList(List<AnnualBudgetDetail> annualBudgetDetailList) {
        this.annualBudgetDetailList = annualBudgetDetailList;
    }


    public BigDecimal getAnnualBudgetMoney() {
        return annualBudgetMoney;
    }

    public void setAnnualBudgetMoney(BigDecimal annualBudgetMoney) {
        this.annualBudgetMoney = annualBudgetMoney;
    }

    public String getAnnualBudgetYear() {
        return annualBudgetYear;
    }

    public void setAnnualBudgetYear(String annualBudgetYear) {
        this.annualBudgetYear = annualBudgetYear;
    }

}
