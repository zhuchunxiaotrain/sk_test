package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Power;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 * 收文登记表
 */
@Entity
@Table(name="ec_dispatch")
public class Receive extends BaseFlowEntity {

    private static final long serialVersionUID = 6761451604434170688L;

    /**
     * 收文类别
     */
    private Dict type;

    /**
     * 收文编号
     */
    private String receiveNo;

    /**
     * 收文标题
     */
    private String title;

    /**
     * 收文时间
     */
    private Date time;

    /**
     * 有效期限
     */
    private Date validDate;

    /**
     * 传阅对象
     */
    private List<Users> passObject;

    /**
     * 正文附件
     */
    private List<FileManage> file;

    /**
     * 是否失效
     */
    private boolean invalid;

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getType() {
        return type;
    }

    public void setType(Dict type) {
        this.type = type;
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getValidDate() {
        return validDate;
    }

    public void setValidDate(Date validDate) {
        this.validDate = validDate;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_receive_passObject", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "passObjectId"))
    public List<Users> getPassObject() {
        return passObject;
    }

    public void setPassObject(List<Users> passObject) {
        this.passObject = passObject;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_receive_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }
}
