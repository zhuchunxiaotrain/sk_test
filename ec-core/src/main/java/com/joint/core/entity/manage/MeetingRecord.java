package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 * 会议记录登记
 */
@Entity
@Table(name="ec_meeting_record")
public class MeetingRecord extends BaseFlowEntity {


    private static final long serialVersionUID = 4680599172252503621L;

    /**
     * 会议室使用登记表
     */
    private  MeetingUse meetingUse;
    /**
     * 会议纪要上传附件
     */
    private List<FileManage> file;

    /**
     * 备注
     */
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_meeting_record_file", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public MeetingUse getMeetingUse() {
        return meetingUse;
    }

    public void setMeetingUse(MeetingUse meetingUse) {
        this.meetingUse = meetingUse;
    }
}
