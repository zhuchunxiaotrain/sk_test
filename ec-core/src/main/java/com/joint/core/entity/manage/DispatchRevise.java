package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Power;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/1/12.
 * 发文附件修订登记表
 */
@Entity
@Table(name="ec_dispatch_revise")
public class DispatchRevise extends BaseFlowEntity {


    private static final long serialVersionUID = 7110207096758788894L;

    /**
     * 发文申请表
     */
    private Dispatch dispatch;
    /**
     * 修订原因
     */
    private String reason;
    /**
     * 是否具备有效期
     */
    private boolean hasValid;
    /**
     * 有效期
     */
    private Date validDate;
    /**
     * 发文对象
     *  1全体员工  2部门 3个人
     */
    private String object;
    /**
     * 通知部门
     */
    private List<Department> noticeDepartment;
    /**
     * 通知个人
     */
    private List<Users> noticeUsers;
    /**
     * 原附件
     */
    private List<FileManage> file;
    /**
     * 修订附件
     */
    private List<FileManage> fileRevise;

    /**
     * 是否需要会签
     */
    private boolean mulExam;

    /**
     * 会签人员
     */
    private List<Users> examUsers;

    /**
     * 是否需要中心领导审核
     */
    private boolean leaderExam;

    /**
     * 中心审核对象
     */
    private List<Power> examObject;


    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isHasValid() {
        return hasValid;
    }

    public void setHasValid(boolean hasValid) {
        this.hasValid = hasValid;
    }

    public Date getValidDate() {
        return validDate;
    }

    public void setValidDate(Date validDate) {
        this.validDate = validDate;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_dispatchRevise_noticeDepartment", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "noticeDepartmentId"))
    public List<Department> getNoticeDepartment() {
        return noticeDepartment;
    }

    public void setNoticeDepartment(List<Department> noticeDepartment) {
        this.noticeDepartment = noticeDepartment;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_dispatchRevise_noticeUsers", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "noticeUsersId"))
    public List<Users> getNoticeUsers() {
        return noticeUsers;
    }

    public void setNoticeUsers(List<Users> noticeUsers) {
        this.noticeUsers = noticeUsers;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_dispatchRevise_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_dispatchRevise_fileRevise", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileReviseId"))
    public List<FileManage> getFileRevise() {
        return fileRevise;
    }

    public void setFileRevise(List<FileManage> fileRevise) {
        this.fileRevise = fileRevise;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Dispatch getDispatch() {
        return dispatch;
    }

    public void setDispatch(Dispatch dispatch) {
        this.dispatch = dispatch;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_dispatchRevise_examObject", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "examObjectId"))
    public List<Power> getExamObject() {
        return examObject;
    }

    public void setExamObject(List<Power> examObject) {
        this.examObject = examObject;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isMulExam() {
        return mulExam;
    }

    public void setMulExam(boolean mulExam) {
        this.mulExam = mulExam;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_dispatchRevise_examUsers", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "examUsersId"))
    public List<Users> getExamUsers() {
        return examUsers;
    }

    public void setExamUsers(List<Users> examUsers) {
        this.examUsers = examUsers;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isLeaderExam() {
        return leaderExam;
    }

    public void setLeaderExam(boolean leaderExam) {
        this.leaderExam = leaderExam;
    }
}
