package com.joint.core.entity.finance;

import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/2.
 */
@Entity
@Table(name="ec_finance")
public class Finance extends BaseFlowEntity {
    /**
     * 年份
     */
    private String year;
    /**
     * 月份
     */
    private String month;
    /**
     * 发布部门,申报单位
     */
    private Department department;
    /**
     * 资产负债表
     */
    private List<FileManage> liabilitie;
    /**
     * 损益表
     */
    private List<FileManage> profitLoss;
    /**
     * 应收款明细表
     */
    private List<FileManage> receivables;
    /**
     * 其他应收款明细表
     */
    private List<FileManage> otherReceivables;
    /**
     * 应付款明细表
     */
    private List<FileManage> payment;
    /**
     * 其他应付款明细表
     */
    private List<FileManage> otherPayment;
    /**
     * 费用明细表
     */
    private List<FileManage> cost;
    /**
     * 是否是产业中心人员(0不是 1是)
     */
    private int ifCentralStaff;




    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_finance_liabilitie", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="liabilitieId"))
    public List<FileManage> getLiabilitie() {
        return liabilitie;
    }

    public void setLiabilitie(List<FileManage> liabilitie) {
        this.liabilitie = liabilitie;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_finance_profitLoss", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="profitLossId"))
    public List<FileManage> getProfitLoss() {
        return profitLoss;
    }

    public void setProfitLoss(List<FileManage> profitLoss) {
        this.profitLoss = profitLoss;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_finance_receivables", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="receivablesId"))
    public List<FileManage> getReceivables() {
        return receivables;
    }

    public void setReceivables(List<FileManage> receivables) {
        this.receivables = receivables;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_finance_otherReceivables", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="otherReceivablesId"))
    public List<FileManage> getOtherReceivables() {
        return otherReceivables;
    }

    public void setOtherReceivables(List<FileManage> otherReceivables) {
        this.otherReceivables = otherReceivables;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_finance_payment", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="paymentId"))
    public List<FileManage> getPayment() {
        return payment;
    }

    public void setPayment(List<FileManage> payment) {
        this.payment = payment;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_finance_otherPayment", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="otherPaymentId"))
    public List<FileManage> getOtherPayment() {
        return otherPayment;
    }

    public void setOtherPayment(List<FileManage> otherPayment) {
        this.otherPayment = otherPayment;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_finance_cost", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="costId"))
    public List<FileManage> getCost() {
        return cost;
    }

    public void setCost(List<FileManage> cost) {
        this.cost = cost;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }

}
