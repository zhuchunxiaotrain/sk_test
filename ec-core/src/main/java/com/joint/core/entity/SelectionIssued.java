package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 证书发放
 * Created by 9Joint on 2016/12/26.
 */
@Entity
@Table(name="sk_selectionissued")
public class SelectionIssued extends BaseFlowEntity{

    private static final long serialVersionUID = -6617587104280751493L;
    /**
     * 结果公示对象
     *//*
    private SelectionPublicity selectionPublicity;*/
    /**
     * 公告对象
     */
    private SelectionNotice selectionNotice;
    /**
     * 聘书发放
     */
    private List<FileManage> attchment;
    /**
     * 聘书发放文本
     */
    private String attchmentText;
    /**
     * 聘任开始时间
     */
    private Date issueStartDate;
    /**
     * 聘任结束时间
     */
    private Date issueEndDate;
   /* @OneToOne
    public SelectionPublicity getSelectionPublicity() {
        return selectionPublicity;
    }

    public void setSelectionPublicity(SelectionPublicity selectionPublicity) {
        this.selectionPublicity = selectionPublicity;
    }*/

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_selectionissued_attchment",joinColumns = @JoinColumn(name = "id"),inverseJoinColumns =@JoinColumn (name ="attchmentId"))
    @OrderBy("id desc ")
    public List<FileManage> getAttchment() {
        return attchment;
    }

    public void setAttchment(List<FileManage> attchment) {
        this.attchment = attchment;
    }

    public String getAttchmentText() {
        return attchmentText;
    }

    public void setAttchmentText(String attchmentText) {
        this.attchmentText = attchmentText;
    }

    public Date getIssueStartDate() {
        return issueStartDate;
    }

    public void setIssueStartDate(Date issueStartDate) {
        this.issueStartDate = issueStartDate;
    }

    public Date getIssueEndDate() {
        return issueEndDate;
    }

    public void setIssueEndDate(Date issueEndDate) {
        this.issueEndDate = issueEndDate;
    }
    @OneToOne
    public SelectionNotice getSelectionNotice() {
        return selectionNotice;
    }

    public void setSelectionNotice(SelectionNotice selectionNotice) {
        this.selectionNotice = selectionNotice;
    }
}
