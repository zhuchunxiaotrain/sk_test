package com.joint.core.entity.finance;

import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/6.
 */
@Entity
@Table(name="ec_calculationyear")
public class CalculationYear extends BaseFlowEntity {

    /**
     * 当前年份
     */
    private String year;
    /**
     * 全年预算报表
     */
    private List<FileManage> budget;
    /**
     * 全年决算报表
     */
    private List<FileManage> finalAccounts;
    /**
     * 是否失效
     */
    private boolean invalid;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_calculationyear_budget", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="budgetId"))
    public List<FileManage> getBudget() {
        return budget;
    }

    public void setBudget(List<FileManage> budget) {
        this.budget = budget;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_calculationyear_finalAccounts", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="finalAccountsId"))
    public List<FileManage> getFinalAccounts() {
        return finalAccounts;
    }

    public void setFinalAccounts(List<FileManage> finalAccounts) {
        this.finalAccounts = finalAccounts;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
