package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;
import com.joint.core.entity.manage.MeetingUse;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * Created by 9Joint on 2016/12/26.
 */
@Entity
@Table(name="sk_activitymanager")
public class ActivityManager extends BaseFlowEntity{

    private static final long serialVersionUID = -247480216126473733L;
    /**
     * 年份
     */
    private String year;
    /**
     * 月份
     */
    private String month;
    /**
     * 活动类型
     */
    private List<Dict> activityType;
    /**
     * 是否需要党费
     */
    private  String requirdMoneyType;
    /**
     * 活动支部
     */
    private Department department;
    /**
     * 活动费用
     */
    private BigDecimal activityMoney;
    /**
     *可支配金额
     */
    private BigDecimal enableMoney;
    /**
     * 相应的会议记录(引用党政模块里的对象)
     */
    private List<MeetingUse> meetingUse;
    /**
     * 选择审核支部书记
     */
    private List<Users> checkUsers;

    public String getRequirdMoneyType() {
        return requirdMoneyType;
    }

    public void setRequirdMoneyType(String requirdMoneyType) {
        this.requirdMoneyType = requirdMoneyType;
    }

    public BigDecimal getActivityMoney() {
        return activityMoney;
    }

    public void setActivityMoney(BigDecimal activityMoney) {
        this.activityMoney = activityMoney;
    }

    public BigDecimal getEnableMoney() {
        return enableMoney;
    }

    public void setEnableMoney(BigDecimal enableMoney) {
        this.enableMoney = enableMoney;
    }



    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_activitymanager_checkusers",joinColumns =@JoinColumn(name = "id"),inverseJoinColumns = @JoinColumn(name = "checkusersId"))
    @OrderBy("createDate desc ")
    public List<Users> getCheckUsers() {
        return checkUsers;
    }

    public void setCheckUsers(List<Users> checkUsers) {
        this.checkUsers = checkUsers;
    }
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_activitymanager_activitytype",joinColumns =@JoinColumn(name = "id"),inverseJoinColumns = @JoinColumn(name = "activitytypeId"))
    @OrderBy("createDate desc ")
    public List<Dict> getActivityType() {
        return activityType;
    }
    public void setActivityType(List<Dict> activityType) {
        this.activityType = activityType;
    }
    @ManyToOne
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_activitymanager_meetinguse",joinColumns =@JoinColumn(name = "id"),inverseJoinColumns = @JoinColumn(name = "meetingUseId"))
    @OrderBy("createDate desc ")
    public List<MeetingUse> getMeetingUse() {
        return meetingUse;
    }

    public void setMeetingUse(List<MeetingUse> meetingUse) {
        this.meetingUse = meetingUse;
    }
}
