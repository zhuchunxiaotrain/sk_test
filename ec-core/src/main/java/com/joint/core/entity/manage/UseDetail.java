package com.joint.core.entity.manage;

import com.joint.base.entity.Department;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/29.
 * 资产使用明细
 */
@Entity
@Table(name="ec_usedetail")
public class UseDetail extends BaseFlowEntity {
    /**
     * 主资产使用登记表
     */
    private AssetUse assetUse;
    /**
     * 现使用部门
     */
    private Department newDepartment;
    /**
     * 先使用人员
     */
    private Users newUser;
    /**
     * 需要的审核人(会签人员)
     */
    private List<Users> examUsers;

    @ManyToOne(fetch = FetchType.LAZY)
    public AssetUse getAssetUse() {
        return assetUse;
    }

    public void setAssetUse(AssetUse assetUse) {
        this.assetUse = assetUse;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getNewDepartment() {
        return newDepartment;
    }

    public void setNewDepartment(Department newDepartment) {
        this.newDepartment = newDepartment;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Users getNewUser() {
        return newUser;
    }

    public void setNewUser(Users newUser) {
        this.newUser = newUser;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_usedetail_examUsers", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "examUsersId"))
    public List<Users> getExamUsers() {
        return examUsers;
    }

    public void setExamUsers(List<Users> examUsers) {
        this.examUsers = examUsers;
    }
}
