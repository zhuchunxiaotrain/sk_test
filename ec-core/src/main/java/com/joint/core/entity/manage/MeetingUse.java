package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 * 会议使用登记
 */
@Entity
@Table(name="ec_meeting_use")
public class MeetingUse extends BaseFlowEntity {

    private static final long serialVersionUID = -7348035296961187044L;

    /**
     * 会议室名称
     */
    Dict meetname;

    /**
     * 会议类型
     */
    Dict type;

    /**
     * 会议议题
     */
    String topic;

    /**
     *预计开始时间
     */
    Date begintime;

    /**
     *预计结束时间
     */
    Date endtime;

    /**
     *参会人员
     */
    private List<Users> joinUsers;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否失效
     */
    private boolean invalid;

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getMeetname() {
        return meetname;
    }

    public void setMeetname(Dict meetname) {
        this.meetname = meetname;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getType() {
        return type;
    }

    public void setType(Dict type) {
        this.type = type;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Date getBegintime() {
        return begintime;
    }

    public void setBegintime(Date begintime) {
        this.begintime = begintime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_meeting_use_joinusers", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "noticeUsersId"))
    public List<Users> getJoinUsers() {
        return joinUsers;
    }

    public void setJoinUsers(List<Users> joinUsers) {
        this.joinUsers = joinUsers;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }
}
