package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;
import org.apache.ibatis.annotations.Many;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 干部选拔报名
 * Created by 9Joint on 2016/12/26.
 */
@Entity
@Table(name="sk_selectionapply")
public class SelectionApply extends BaseFlowEntity{

    private static final long serialVersionUID = 6204836700788749352L;
    /**
     * 相关资料文本
     */
    private String attchmentText;
    /**
     * 相关资料
     */
    private List<FileManage> attchment;

    /**
     * 多个报名对应一个公告
     */
    private SelectionNotice selectionNotice;
    /**
     * 备注
     */
    private String remark;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_selectionapply_attchment",joinColumns =@JoinColumn(name = "id"),inverseJoinColumns = @JoinColumn(name = "attchemntId"))
    @OrderBy("createDate desc ")
    public List<FileManage> getAttchment() {
        return attchment;
    }

    public void setAttchment(List<FileManage> attchment) {
        this.attchment = attchment;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttchmentText() {
        return attchmentText;
    }

    public void setAttchmentText(String attchmentText) {
        this.attchmentText = attchmentText;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public SelectionNotice getSelectionNotice() {
        return selectionNotice;
    }

    public void setSelectionNotice(SelectionNotice selectionNotice) {
        this.selectionNotice = selectionNotice;
    }
}
