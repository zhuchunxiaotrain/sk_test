package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by ZhuChunXiao on 2017/3/20.
 * 公司资质管理
 */
@Entity
@Table(name="ec_qualifications")
public class Qualifications extends BaseFlowEntity {
    /**
     * 所属单位
     */
    private Department department;
    /**
     * 资质类别 1企业证照 2企业资质
     */
    private String type;
    /**
     * 资质名称
     */
    private QualificationsDict qualificationsDict;
    /**
     * 资质号
     */
    private String qualificationsNo;
    /**
     * 办理人员
     */
    private Users handler;
    /**
     * 使用年限
     */
    private Date useDate;
    /**
     * 当前状态 1使用中 2到期提醒 3已失效
     */
    private String step;
    /**
     * 是否失效
     */
    private boolean invalid;
    /**
     * 提前提醒周期
     */
    private Date remindDate;
    /**
     * 评定日期
     */
    private Date evaluateDate;

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public QualificationsDict getQualificationsDict() {
        return qualificationsDict;
    }

    public void setQualificationsDict(QualificationsDict qualificationsDict) {
        this.qualificationsDict = qualificationsDict;
    }

    public String getQualificationsNo() {
        return qualificationsNo;
    }

    public void setQualificationsNo(String qualificationsNo) {
        this.qualificationsNo = qualificationsNo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Users getHandler() {
        return handler;
    }

    public void setHandler(Users handler) {
        this.handler = handler;
    }

    public Date getUseDate() {
        return useDate;
    }

    public void setUseDate(Date useDate) {
        this.useDate = useDate;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }

    public Date getRemindDate() {
        return remindDate;
    }

    public void setRemindDate(Date remindDate) {
        this.remindDate = remindDate;
    }

    public Date getEvaluateDate() {
        return evaluateDate;
    }

    public void setEvaluateDate(Date evaluateDate) {
        this.evaluateDate = evaluateDate;
    }
}
