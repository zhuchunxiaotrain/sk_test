package com.joint.core.entity.finance;

import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
@Entity
@Table(name="ec_financeyear")
public class FinanceYear extends BaseFlowEntity {
    /**
     * 年份
     */
    private String year;
    /**
     * 申报单位，部门
     */
    private Department department;
    /**
     * 财务年报附件
     */
    private List<FileManage> file;


    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_financeyear_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }


}
