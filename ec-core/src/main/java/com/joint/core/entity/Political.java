package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_political")
public class Political extends BaseFlowEntity {

    /**
     * 是否党员(0/1)
     */
    private String partyMember;

    /**
     * 入党时间
     */
    private Date partyDate;

    /**
     * 介绍人
     */
    private String introducer;

    /**
     * 工会费
     */
    private BigDecimal money;

    /**
     * 党费缴纳基数
     */
    private BigDecimal baseMoney;

    public String getPartyMember() {
        return partyMember;
    }

    public void setPartyMember(String partyMember) {
        this.partyMember = partyMember;
    }

    public Date getPartyDate() {
        return partyDate;
    }

    public void setPartyDate(Date partyDate) {
        this.partyDate = partyDate;
    }

    public String getIntroducer() {
        return introducer;
    }

    public void setIntroducer(String introducer) {
        this.introducer = introducer;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getBaseMoney() {
        return baseMoney;
    }

    public void setBaseMoney(BigDecimal baseMoney) {
        this.baseMoney = baseMoney;
    }
}
