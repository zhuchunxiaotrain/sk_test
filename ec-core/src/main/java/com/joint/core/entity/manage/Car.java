package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/21.
 */
@Entity
@Table(name="ec_car")
public class Car extends BaseFlowEntity {
    /**
     * 车辆配置表
     */
    private CarConfig carConfig;
    /**
     * 申请用途
     */
    private String purpose;
    /**
     * 详细说明
     */
    private String description;
    /**
     * 附件
     */
    private List<FileManage> file;
    /**
     * 使用时间1
     */
    private Date dateStart;
    /**
     * 使用时间2
     */
    private Date dateEnd;
    /**
     * 当前公里数
     */
    private int kilometre;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 当前状态
     */
    private String step;
    /**
     *相关的车辆管理员
     */
    private List<Users> carAdmin;
    /**
     * 新建时判断
     */
    private int isUse;


    @ManyToOne(fetch = FetchType.LAZY)
    public CarConfig getCarConfig() {
        return carConfig;
    }

    public void setCarConfig(CarConfig carConfig) {
        this.carConfig = carConfig;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_car_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getKilometre() {
        return kilometre;
    }

    public void setKilometre(int kilometre) {
        this.kilometre = kilometre;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_car_caradmin", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="caradminId"))
    public List<Users> getCarAdmin() {
        return carAdmin;
    }

    public void setCarAdmin(List<Users> carAdmin) {
        this.carAdmin = carAdmin;
    }

    public int getIsUse() {
        return isUse;
    }

    public void setIsUse(int isUse) {
        this.isUse = isUse;
    }
}
