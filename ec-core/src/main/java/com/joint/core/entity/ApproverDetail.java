package com.joint.core.entity;

import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;


import javax.persistence.*;
import java.util.Date;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Entity
@Table(name = "sk_approverdetail")
public class ApproverDetail extends BaseFlowEntity {

    private static final long serialVersionUID = -996051285631680384L;
    /**
     *审批人
     */
    private Users approver;
    /**
     * 操作日期
     */
    private Date approverDate;
    /**
     * 意见
      */
    private String approverAdvice;


    @ManyToOne(fetch = FetchType.LAZY)
    public Users getApprover() {
        return approver;
    }

    public void setApprover(Users approver) {
        this.approver = approver;
    }

    public Date getApproverDate() {
        return approverDate;
    }

    public void setApproverDate(Date approverDate) {
        this.approverDate = approverDate;
    }

    public String getApproverAdvice() {
        return approverAdvice;
    }

    public void setApproverAdvice(String approverAdvice) {
        this.approverAdvice = approverAdvice;
    }
}
