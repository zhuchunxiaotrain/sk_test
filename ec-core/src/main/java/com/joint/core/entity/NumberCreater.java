package com.joint.core.entity;

import com.joint.base.parent.NameEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 流水号生成
 *
 */
@Entity
@Table(name="ec_number_creater")
public class NumberCreater extends NameEntity{
    /**
     * 下一个编号
     */
    private String nextNumber;
    /**
     * 实体名称
     */
    //private String entityName;

    /**
     * 编号年份
     */
    private String usingYear;

    public String getNextNumber() {
        return nextNumber;
    }

    public void setNextNumber(String nextNumber) {
        this.nextNumber = nextNumber;
    }

//    public String getEntityName() {
//        return entityName;
//    }
//
//    public void setEntityName(String entityName) {
//        this.entityName = entityName;
//    }

    public String getUsingYear() {
        return usingYear;
    }

    public void setUsingYear(String usingYear) {
        this.usingYear = usingYear;
    }
}
