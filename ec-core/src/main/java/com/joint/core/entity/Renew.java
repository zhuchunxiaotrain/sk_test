package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.Post;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_renew")
public class Renew extends BaseFlowEntity {

    /**
     * 用人性质
     */
    private String nature;

    /**
     * 合同开始日期
     */
    private Date startDate;

    /**
     * 合同结束日期
     */
    private Date endDate;

    /**
     * 续签合同开始时间
     */
    private Date renewalStart;

    /**
     * 续签合同结束时间
     */
    private Date renewalEnd;

    /**
     * 岗位
     */
    private Post renewPost;

    /**
     * 续签薪资
     */
    private BigDecimal renewMoney;

    /**
     * 下一步审批人的Name的集合，用逗号分隔
     */
    private String nextStepApproversName;

    /**
     * 下一步审批人的id的集合，用逗号分隔
     */
    private String nextStepApproversId;

    /**
     * 所属部门
     */
    private Department department;

    /**
     * 岗位
     */
    private Post post;

    /**
     * 员工基本表
     * @return
     */
    private Employees employees;

    /**
     * 产业中心人员(0不是 1是)
     */
    private int ifCentralStaff;


    @OneToOne
    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getRenewalStart() {
        return renewalStart;
    }

    public void setRenewalStart(Date renewalStart) {
        this.renewalStart = renewalStart;
    }

    public Date getRenewalEnd() {
        return renewalEnd;
    }

    public void setRenewalEnd(Date renewalEnd) {
        this.renewalEnd = renewalEnd;
    }

    public BigDecimal getRenewMoney() {
        return renewMoney;
    }

    public void setRenewMoney(BigDecimal renewMoney) {
        this.renewMoney = renewMoney;
    }

    public String getNextStepApproversName() {
        return nextStepApproversName;
    }

    public void setNextStepApproversName(String nextStepApproversName) {
        this.nextStepApproversName = nextStepApproversName;
    }

    public String getNextStepApproversId() {
        return nextStepApproversId;
    }

    public void setNextStepApproversId(String nextStepApproversId) {
        this.nextStepApproversId = nextStepApproversId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Post getRenewPost() {
        return renewPost;
    }

    public void setRenewPost(Post renewPost) {
        this.renewPost = renewPost;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }
}
