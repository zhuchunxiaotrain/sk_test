package com.joint.core.entity;

import cn.jpush.api.report.UsersResult;
import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 竞聘演讲
 * Created by 9Joint on 2016/12/26.
 */
@Entity
@Table(name="sk_selectionspeech")
public class SelectionSpeech extends BaseFlowEntity{

    private static final long serialVersionUID = -7121184031573742335L;
    /**
     * 公告对象
     */
    private SelectionNotice selectionNotice;
    /**
     * 报名对象
     */
    private SelectionApply selectionApply;
    /**
     * 附件上传
     */
    private List<FileManage>  attchmentList;
    /**
     * 附加文本
     */
    private String attchmentText;
    /**
     * 文档状态
     */
    private String  speechState;
    /**
     * 结果 0:未审批，1：通过，2未通过
     */
    private String result;

    /**
     * 审批人
     * @return
     */
    private Users approver;

    /**
     * 操作人审批时间
     * @return
     */
    private Date approverDate;


    @OneToOne(fetch = FetchType.LAZY)
    public SelectionApply getSelectionApply() {
        return selectionApply;
    }

    public void setSelectionApply(SelectionApply selectionApply) {
        this.selectionApply = selectionApply;
    }
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_selectionspeech_attchmentList",joinColumns =@JoinColumn(name = "id"),inverseJoinColumns = @JoinColumn(name = "attchmentListId"))
    @OrderBy("createDate desc ")
    public List<FileManage> getAttchmentList() {
        return attchmentList;
    }

    public void setAttchmentList(List<FileManage> attchmentList) {
        this.attchmentList = attchmentList;
    }
    @OneToOne(fetch = FetchType.LAZY)
    public SelectionNotice getSelectionNotice() {
        return selectionNotice;
    }

    public void setSelectionNotice(SelectionNotice selectionNotice) {
        this.selectionNotice = selectionNotice;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getSpeechState() {
        return speechState;
    }

    public void setSpeechState(String speechState) {
        this.speechState = speechState;
    }

    public String getAttchmentText() {
        return attchmentText;
    }

    public void setAttchmentText(String attchmentText) {
        this.attchmentText = attchmentText;
    }
    @ManyToOne
    public Users getApprover() {
        return approver;
    }

    public void setApprover(Users approver) {
        this.approver = approver;
    }

    public Date getApproverDate() {
        return approverDate;
    }

    public void setApproverDate(Date approverDate) {
        this.approverDate = approverDate;
    }
}
