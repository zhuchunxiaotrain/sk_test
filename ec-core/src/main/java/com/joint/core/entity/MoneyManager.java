package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 党费缴纳
 * Created by Administrator on 2017/1/5.
 */
@Entity
@Table(name = "sk_moneyManager")
public class MoneyManager extends BaseFlowEntity {

    private static final long serialVersionUID = 6663295247742031081L;
    /**
     * 登记年月
     */
    private Date recordDate;
    /**
     * 支部名称
     */
    private Department department;
    /**
     * 党费等级详情表
     */
    private List<MoneyManagerDetail> moneyManagerDetailList;
    /**
     * 本月总上缴党费
     */
    private BigDecimal cumulativeAmountPartyMoney;

    /**
     * 登记的年份
     * @return
     */
    private String registerYear;

    /**
     * 登记的月份
     * @return
     */
    private String registerMonth;

    @ManyToOne
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public BigDecimal getCumulativeAmountPartyMoney() {
        return cumulativeAmountPartyMoney;
    }

    public void setCumulativeAmountPartyMoney(BigDecimal cumulativeAmountPartyMoney) {
        this.cumulativeAmountPartyMoney = cumulativeAmountPartyMoney;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy="moneyManager")
    public List<MoneyManagerDetail> getMoneyManagerDetailList() {
        return moneyManagerDetailList;
    }

    public void setMoneyManagerDetailList(List<MoneyManagerDetail> moneyManagerDetailList) {
        this.moneyManagerDetailList = moneyManagerDetailList;
    }

    public String getRegisterYear() {
        return registerYear;
    }

    public void setRegisterYear(String registerYear) {
        this.registerYear = registerYear;
    }

    public String getRegisterMonth() {
        return registerMonth;
    }

    public void setRegisterMonth(String registerMonth) {
        this.registerMonth = registerMonth;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }


}
