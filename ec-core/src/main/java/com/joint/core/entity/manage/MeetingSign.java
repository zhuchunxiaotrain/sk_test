package com.joint.core.entity.manage;

import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 * 会议签到
 */
@Entity
@Table(name="ec_meeting_sign")
public class MeetingSign extends BaseFlowEntity {


    private static final long serialVersionUID = -2865784733359367718L;
    /**
     * 会议室使用登记表
     */
    private  MeetingUse meetingUse;



    @ManyToOne(fetch = FetchType.LAZY)
    public MeetingUse getMeetingUse() {
        return meetingUse;
    }

    public void setMeetingUse(MeetingUse meetingUse) {
        this.meetingUse = meetingUse;
    }
}
