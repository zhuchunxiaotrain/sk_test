package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_train")
public class Train extends BaseFlowEntity {

    /**
     * 培训类型
     */
    private Dict type;

    /**
     * 培训内容
     * @return
     */
    private List<FileManage> content;
    private String contextText;

    /**
     * 培训周期
     */
    private Date startDate;
    private Date endDate;

    /**
     * 培训讲师
     * @return
     */
    private List<Users> trainUsers;

    /**
     * 备注
     * @return
     */
    private String remark;

    /**
     * 参与人
     */
    private Set<Participant> participantSet;

    /**
     * 完成 1完成
     */
    private String finish;

    @OneToMany
    public Set<Participant> getParticipantSet() {
        return participantSet;
    }

    public void setParticipantSet(Set<Participant> participantSet) {
        this.participantSet = participantSet;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_train_content", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="contentId"))

    public List<FileManage> getContent() {
        return content;
    }

    public void setContent(List<FileManage> content) {
        this.content = content;
    }

    public String getContextText() {
        return contextText;
    }

    public void setContextText(String contextText) {
        this.contextText = contextText;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_train_trainUsers", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "trainUsersId"))
    public List<Users> getTrainUsers() {
        return trainUsers;
    }

    public void setTrainUsers(List<Users> trainUsers) {
        this.trainUsers = trainUsers;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getType() {
        return type;
    }

    public void setType(Dict type) {
        this.type = type;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }
}

