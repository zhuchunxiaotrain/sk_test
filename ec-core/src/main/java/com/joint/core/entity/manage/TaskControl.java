package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Power;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/1/12.
 * 任务督办
 */
@Entity
@Table(name="ec_taskcontrol")
public class TaskControl extends BaseFlowEntity {


    private static final long serialVersionUID = -5818605133117554538L;
    /**
     * 任务编号
     */
    private String taskNo;
    /**
     * 任务主题
     */
    private String topic;
    /**
     * 详细要求
     */
    private String demand;
    /**
     * 任务难度系数
     */
    private String degree;
    /**
     * 相关附件
     */
    private List<FileManage> file;
    /**
     * 任务责任人
     */
    private List<Users> dutyMans;
    /**
     * 会办部门
     */
    private List<Department> dealDepartment;
    /**
     * 要求完成时间
     */
    private Date planFinishDate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 实际完成时间
     */
    private Date actualFinishDate;

    /**
     * 完成标志
     */
    private boolean finish;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDemand() {
        return demand;
    }

    public void setDemand(String demand) {
        this.demand = demand;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_taskcontrol_file", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_taskcontrol_dutyMans", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "dutyMansId"))
    public List<Users> getDutyMans() {
        return dutyMans;
    }

    public void setDutyMans(List<Users> dutyMans) {
        this.dutyMans = dutyMans;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_taskcontrol_dealDepartment", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "dealDepartmentId"))
    public List<Department> getDealDepartment() {
        return dealDepartment;
    }

    public void setDealDepartment(List<Department> dealDepartment) {
        this.dealDepartment = dealDepartment;
    }

    public Date getPlanFinishDate() {
        return planFinishDate;
    }

    public void setPlanFinishDate(Date planFinishDate) {
        this.planFinishDate = planFinishDate;
    }

    public Date getActualFinishDate() {
        return actualFinishDate;
    }

    public void setActualFinishDate(Date actualFinishDate) {
        this.actualFinishDate = actualFinishDate;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isFinish() {
        return finish;
    }

    public void setFinish(boolean finish) {
        this.finish = finish;
    }
}
