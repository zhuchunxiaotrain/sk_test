package com.joint.core.entity.manage;

import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/30.
 * 接待管理
 */
@Entity
@Table(name="ec_reception")
public class Reception extends BaseFlowEntity {

    private static final long serialVersionUID = 8764854684897049429L;

    /**
     * 附件上传
     */
    private List<FileManage> file;
    /**
     * 公函号
     */
    private String letterNo;
    /**
     * 来宾姓名
     */
    private String guestName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 来访单位
     */
    private String department;
    /**
     * 职务
     */
    private String post;
    /**
     * 主要随员
     */
    private String mainAttache;
    /**
     * 人数
     */
    private String count;
    /**
     * 主陪姓名
     */
    private Users mainName;
    /**
     * 其他陪同人员
     */
    private String otherPerson;
    /**
     * 接待事由
     */
    private String receptionReason;
    /**
     * 接待时间
     */
    private Date date;
    /**
     * 公务内容
     */
    private String content;
    /**
     * 就餐地点
     */
    private String eatPlace;
    /**
     * 就餐标准
     */
    private String eatStandard;
    /**
     * 住宿地点
     */
    private String livePlace;
    /**
     * 住宿标准
     */
    private String liveStandard;
    /**
     * 本次费用合计
     */
    private String money;
    /**
     * 其他项目
     */
    private String object;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 是否上报中心
     */
    private boolean leaderExam;
    /**
     * 是否是产业中心人员
     */
    private boolean ifCentralStaff;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_reception_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    public String getLetterNo() {
        return letterNo;
    }

    public void setLetterNo(String letterNo) {
        this.letterNo = letterNo;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getMainAttache() {
        return mainAttache;
    }

    public void setMainAttache(String mainAttache) {
        this.mainAttache = mainAttache;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Users getMainName() {
        return mainName;
    }

    public void setMainName(Users mainName) {
        this.mainName = mainName;
    }

    public String getOtherPerson() {
        return otherPerson;
    }

    public void setOtherPerson(String otherPerson) {
        this.otherPerson = otherPerson;
    }

    public String getReceptionReason() {
        return receptionReason;
    }

    public void setReceptionReason(String receptionReason) {
        this.receptionReason = receptionReason;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEatPlace() {
        return eatPlace;
    }

    public void setEatPlace(String eatPlace) {
        this.eatPlace = eatPlace;
    }

    public String getEatStandard() {
        return eatStandard;
    }

    public void setEatStandard(String eatStandard) {
        this.eatStandard = eatStandard;
    }

    public String getLivePlace() {
        return livePlace;
    }

    public void setLivePlace(String livePlace) {
        this.livePlace = livePlace;
    }

    public String getLiveStandard() {
        return liveStandard;
    }

    public void setLiveStandard(String liveStandard) {
        this.liveStandard = liveStandard;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    @Column(length = 16777216)
    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isLeaderExam() {
        return leaderExam;
    }

    public void setLeaderExam(boolean leaderExam) {
        this.leaderExam = leaderExam;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(boolean ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }
}
