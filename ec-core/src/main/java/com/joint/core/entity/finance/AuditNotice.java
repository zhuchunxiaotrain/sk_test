package com.joint.core.entity.finance;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/10.
 */
@Entity
@Table(name="ec_auditnotice")
public class AuditNotice extends BaseFlowEntity {
    /**
     * 审计类型
     */
    private Dict type;
    /**
     * 被审计单位
     */
    private Department department;
    /**
     * 被审计对象
     * 1部门 2个人
     */
    private String object;
    /**
     * 通知部门
     */
    private List<Department> noticeDepartment;
    /**
     * 通知个人
     */
    private List<Users> noticeUsers;
    /**
     * 审计日期
     */
    private Date auditDate;
    /**
     * 审计概况文本
     */
    private String auditSurveyText;
    /**
     * 审计概况附件
     */
    private List<FileManage> auditSurveyFile;
    /**
     * 审计事务所
     */
    private Dict office;
    /**
     * 审计通知书附件
     */
    private List<FileManage> auditNoticeFile;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 当前进度
     */
    private String step;
    /**
     * 是否失效【是否完成】
     */
    private boolean invalid;
    /**
     * 审计初稿
     */
    private AuditFirst auditFirst;
    /**
     * 审计终稿
     */
    private AuditLast auditLast;
    /**
     * 审计整改通知单
     */
    private AuditEdit auditEdit;
    /**
     * 审计答复
     */
    private List<AuditAnswer> auditAnswerList;

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getType() {
        return type;
    }

    public void setType(Dict type) {
        this.type = type;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_auditnotice_noticeDepartment", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "noticeDepartmentId"))
    public List<Department> getNoticeDepartment() {
        return noticeDepartment;
    }

    public void setNoticeDepartment(List<Department> noticeDepartment) {
        this.noticeDepartment = noticeDepartment;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_auditbotice_noticeUsers", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "noticeUsersId"))
    public List<Users> getNoticeUsers() {
        return noticeUsers;
    }

    public void setNoticeUsers(List<Users> noticeUsers) {
        this.noticeUsers = noticeUsers;
    }

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public String getAuditSurveyText() {
        return auditSurveyText;
    }

    public void setAuditSurveyText(String auditSurveyText) {
        this.auditSurveyText = auditSurveyText;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_auditnotice_auditsurveyfile", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="auditsurveyfileId"))
    public List<FileManage> getAuditSurveyFile() {
        return auditSurveyFile;
    }

    public void setAuditSurveyFile(List<FileManage> auditSurveyFile) {
        this.auditSurveyFile = auditSurveyFile;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getOffice() {
        return office;
    }

    public void setOffice(Dict office) {
        this.office = office;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_auditnotice_auditnoticefile", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="auditnoticefileId"))
    public List<FileManage> getAuditNoticeFile() {
        return auditNoticeFile;
    }

    public void setAuditNoticeFile(List<FileManage> auditNoticeFile) {
        this.auditNoticeFile = auditNoticeFile;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    @OneToOne(fetch = FetchType.LAZY)
    public AuditFirst getAuditFirst() {
        return auditFirst;
    }

    public void setAuditFirst(AuditFirst auditFirst) {
        this.auditFirst = auditFirst;
    }

    @OneToOne(fetch = FetchType.LAZY)
    public AuditLast getAuditLast() {
        return auditLast;
    }

    public void setAuditLast(AuditLast auditLast) {
        this.auditLast = auditLast;
    }

    @OneToOne(fetch = FetchType.LAZY)
    public AuditEdit getAuditEdit() {
        return auditEdit;
    }

    public void setAuditEdit(AuditEdit auditEdit) {
        this.auditEdit = auditEdit;
    }

    @OneToMany(fetch = FetchType.LAZY)
    public List<AuditAnswer> getAuditAnswerList() {
        return auditAnswerList;
    }

    public void setAuditAnswerList(List<AuditAnswer> auditAnswerList) {
        this.auditAnswerList = auditAnswerList;
    }
}
