package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
@Entity
@Table(name="ec_oil")
public class Oil extends BaseFlowEntity {
//    /**
//     * 车辆品牌
//     */
//    private Dict brand;
//    /**
//     * 车辆型号
//     */
//    private Dict type;
//    /**
//     * 车辆号牌
//     */
//    private Dict carNo;
    /**
     * 车辆配置表
     */
    private CarConfig carConfig;
    /**
     * 当前里程数
     */
    private int kilometre;
    /**
     * 加油时间
     */
    private Date oilDate;
    /**
     * 加油金额
     */
    private Double oilMoney;
    /**
     * 当前累计加油金额
     */
    private Double oilMoneyNum;
    /**
     * 备注
     */
    private String remarks;

//    @ManyToOne(fetch = FetchType.LAZY)
//    public Dict getBrand() {
//        return brand;
//    }
//
//    public void setBrand(Dict brand) {
//        this.brand = brand;
//    }
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    public Dict getType() {
//        return type;
//    }
//
//    public void setType(Dict type) {
//        this.type = type;
//    }
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    public Dict getCarNo() {
//        return carNo;
//    }
//
//    public void setCarNo(Dict carNo) {
//        this.carNo = carNo;
//    }

    public int getKilometre() {
        return kilometre;
    }

    public void setKilometre(int kilometre) {
        this.kilometre = kilometre;
    }

    public Date getOilDate() {
        return oilDate;
    }

    public void setOilDate(Date oilDate) {
        this.oilDate = oilDate;
    }

    public Double getOilMoney() {
        return oilMoney;
    }

    public void setOilMoney(Double oilMoney) {
        this.oilMoney = oilMoney;
    }

    public Double getOilMoneyNum() {
        return oilMoneyNum;
    }

    public void setOilMoneyNum(Double oilMoneyNum) {
        this.oilMoneyNum = oilMoneyNum;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @OneToOne(fetch = FetchType.LAZY)
    public CarConfig getCarConfig() {
        return carConfig;
    }

    public void setCarConfig(CarConfig carConfig) {
        this.carConfig = carConfig;
    }
}
