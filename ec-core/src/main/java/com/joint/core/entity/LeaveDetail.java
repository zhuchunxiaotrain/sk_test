package com.joint.core.entity;

import com.joint.base.entity.Department;
import com.joint.base.entity.Post;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_leavedetail")
public class LeaveDetail extends BaseFlowEntity {


    /**
     *
     */
    private Users users;
    /**
     * 今年年假
     */
    private BigDecimal annuaLeave;

    /**
     * 今年年假(已用)
     */
    private BigDecimal usedAnnuaLeave;

    /**
     * 今年年假(可用)
     */
    private BigDecimal unusedAnnuaLeave;

    /**
     * 已申请病假
     */
    private BigDecimal sickLeave;

    /**
     * 已申请事假
     */
    private BigDecimal thingLeave;

    /**
     * 已申请其他假期
     */
    private BigDecimal othersLeave;

    /**
     * 当年已申请天数
     */
    private BigDecimal usedDayNum;

    /**
     * 年份
     *
     */
    private Integer time;


    public BigDecimal getAnnuaLeave() {
        return annuaLeave;
    }

    public void setAnnuaLeave(BigDecimal annuaLeave) {
        this.annuaLeave = annuaLeave;
    }

    public BigDecimal getUsedAnnuaLeave() {
        return usedAnnuaLeave;
    }

    public void setUsedAnnuaLeave(BigDecimal usedAnnuaLeave) {
        this.usedAnnuaLeave = usedAnnuaLeave;
    }

    public BigDecimal getUnusedAnnuaLeave() {
        return unusedAnnuaLeave;
    }

    public void setUnusedAnnuaLeave(BigDecimal unusedAnnuaLeave) {
        this.unusedAnnuaLeave = unusedAnnuaLeave;
    }

    public BigDecimal getSickLeave() {
        return sickLeave;
    }

    public void setSickLeave(BigDecimal sickLeave) {
        this.sickLeave = sickLeave;
    }

    public BigDecimal getThingLeave() {
        return thingLeave;
    }

    public void setThingLeave(BigDecimal thingLeave) {
        this.thingLeave = thingLeave;
    }

    public BigDecimal getOthersLeave() {
        return othersLeave;
    }

    public void setOthersLeave(BigDecimal othersLeave) {
        this.othersLeave = othersLeave;
    }

    public BigDecimal getUsedDayNum() {
        return usedDayNum;
    }

    public void setUsedDayNum(BigDecimal usedDayNum) {
        this.usedDayNum = usedDayNum;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    @OneToOne
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
}
