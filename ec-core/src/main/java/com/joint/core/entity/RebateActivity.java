package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Administrator on 2017/1/5.
 */
@Entity
@Table(name = "sk_rebateactivity")
public class RebateActivity extends BaseFlowEntity {

    private static final long serialVersionUID = -5336720156510500484L;
    /**
     * 年份
     */
    private String year;
    /**
     * 月份
     */
    private String month;
    /**
     * 活动类型
     */
    private List<Dict> activityType;
    /**
     * 所属单位
     */
    private Department department;

    /**
     * 退管会活动详情
     * @return
     */
    private List<RebateActivityDetail> rebateActivityDetailList;


    @ManyToOne
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sk_rebateactivity_activitytype", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "activitytypeId"))
    @OrderBy("id desc ")
    public List<Dict> getActivityType() {
        return activityType;
    }

    public void setActivityType(List<Dict> activityType) {
        this.activityType = activityType;
    }
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "rebateActivity")
    public List<RebateActivityDetail> getRebateActivityDetailList() {
        return rebateActivityDetailList;
    }

    public void setRebateActivityDetailList(List<RebateActivityDetail> rebateActivityDetailList) {
        this.rebateActivityDetailList = rebateActivityDetailList;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
