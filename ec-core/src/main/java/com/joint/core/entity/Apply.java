package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.Post;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_apply")
public class Apply extends BaseFlowEntity {

    /**
     * 合同开始日期
     */
    private Date startDate;

    /**
     * 合同结束日期
     */
    private Date endDate;

    /**
     * 试用开始时间
     */
    private Date trialStart;

    /**
     * 试用结束时间
     */
    private Date trialEnd;

    /**
     * 下一步审批人的Name的集合，用逗号分隔
     */
    private String nextStepApproversName;

    /**
     * 下一步审批人的id的集合，用逗号分隔
     */
    private String nextStepApproversId;

    /**
     * 所属部门
     */
    private Department department;

    /**
     * 岗位
     */
    private Post post;

    /**
     * 性别
     */
    private String sex;

    /**
     * 用人性质
     */
    private String nature;

    /**
     * 产业中心人员(0不是 1是)
     */
    private int ifCentralStaff;

    /**
     * 员工基本表
     * @return
     */
    private Employees employees;



    @OneToOne
    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getTrialStart() {
        return trialStart;
    }

    public void setTrialStart(Date trialStart) {
        this.trialStart = trialStart;
    }

    public Date getTrialEnd() {
        return trialEnd;
    }

    public void setTrialEnd(Date trialEnd) {
        this.trialEnd = trialEnd;
    }

    public String getNextStepApproversName() {
        return nextStepApproversName;
    }

    public void setNextStepApproversName(String nextStepApproversName) {
        this.nextStepApproversName = nextStepApproversName;
    }

    public String getNextStepApproversId() {
        return nextStepApproversId;
    }

    public void setNextStepApproversId(String nextStepApproversId) {
        this.nextStepApproversId = nextStepApproversId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }
}
