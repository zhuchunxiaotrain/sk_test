package com.joint.core.entity.finance;

import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/10.
 * 财务年终工作总结及计划
 */
@Entity
@Table(name="ec_workplan")
public class WorkPlan extends BaseFlowEntity{
    /**
     * 工作计划总结附件
     */
    private List<FileManage> file;
    /**
     * 是否失效
     */
    private boolean invalid;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="sk_workplan_file" , joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }
}
