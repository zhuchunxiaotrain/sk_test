package com.joint.core.entity.manage;

import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 * 会议请假
 */
@Entity
@Table(name="ec_meeting_leave")
public class MeetingLeave extends BaseFlowEntity {


    private static final long serialVersionUID = -6970014903790081753L;
    /**
     * 会议室使用登记表
     */
    private  MeetingUse meetingUse;



    /**
     * 请假原因
     */
    private String reason;



    @ManyToOne(fetch = FetchType.LAZY)
    public MeetingUse getMeetingUse() {
        return meetingUse;
    }

    public void setMeetingUse(MeetingUse meetingUse) {
        this.meetingUse = meetingUse;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }


}
