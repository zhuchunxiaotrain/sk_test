package com.joint.core.entity;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.Post;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Created by root on 16/12/16.
 */
@Entity
@Table(name="ec_manage")
public class Manage extends BaseFlowEntity{

    /**
     * 招聘部门
     */
    private Department recruitDepart;

    /**
     * 招聘岗位
     */
    private Post post;

    /**
     * 招聘人数
     */
    private int recruitNum;

    /**
     * 年龄
     */
    private String age;

    /**
     * 性别(0男 1女)
     */
    private BaseEnum.SexEnum sex;

    /**
     * 专业
     */
    private String profession;

    /**
     * 学历
     */
    private String edu;

    /**
     * 工作经验
     */
    private String experience;

    /**
     *  面试负责人
     */
    private Set<Users> chargeUsers;

    /**
     * 其他要求
     */
    private String others;

    /**
     * 下一步审批人的Name的集合，用逗号分隔
     */
    private String nextStepApproversName;

    /**
     * 下一步审批人的id的集合，用逗号分隔
     */
    private String nextStepApproversId;

    /**
     * 面试登记表
     * @return
     */
    private Set<Interview> interviewSet;
    /**
     * 招聘结果
     */
    private String result;

    /**
     * 审核表
     * @return
     */
    private Set<Employment> employmentSet;

    /**
     * 产业中心人员(0不是 1是)
     */
    private int ifCentralStaff;

    @OneToMany(fetch = FetchType.LAZY,mappedBy ="manage")
    public Set<Employment> getEmploymentSet() {
        return employmentSet;
    }

    public void setEmploymentSet(Set<Employment> employmentSet) {
        this.employmentSet = employmentSet;
    }

    @OneToMany(fetch = FetchType.LAZY,mappedBy ="manage")
    public Set<Interview> getInterviewSet() {
        return interviewSet;
    }

    public void setInterviewSet(Set<Interview> interviewSet) {
        this.interviewSet = interviewSet;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getRecruitDepart() {
        return recruitDepart;
    }

    public void setRecruitDepart(Department recruitDepart) {
        this.recruitDepart = recruitDepart;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Column
    @Enumerated(EnumType.STRING)
    public BaseEnum.SexEnum getSex() {
        return sex;
    }

    public void setSex(BaseEnum.SexEnum sex) {
        this.sex = sex;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }


    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }
    public String getNextStepApproversName() {
        return nextStepApproversName;
    }

    public void setNextStepApproversName(String nextStepApproversName) {
        this.nextStepApproversName = nextStepApproversName;
    }
    @Column(length = 510)
    public String getNextStepApproversId() {
        return nextStepApproversId;
    }

    public void setNextStepApproversId(String nextStepApproversId) {
        this.nextStepApproversId = nextStepApproversId;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public int getRecruitNum() {
        return recruitNum;
    }

    public void setRecruitNum(int recruitNum) {
        this.recruitNum = recruitNum;
    }


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="ec_manage_chargeUsers",joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="chargeUsersId"))
    @OrderBy("name asc")
    public Set<Users> getChargeUsers() {
        return chargeUsers;
    }

    public void setChargeUsers(Set<Users> chargeUsers) {
        this.chargeUsers = chargeUsers;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }
}
