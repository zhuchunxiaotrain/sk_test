package com.joint.core.entity;

import com.joint.base.entity.Department;
import com.joint.base.entity.Post;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_change")
public class Change extends BaseFlowEntity {

    /**
     * 用人性质
     */
    private String nature;

    /**
     * 调动员工
     */
    private Users users;

    /**
     * 调动原因
     */
    private String reason;

    /**
     * 调动部门
     */
    private Department department;

    /**
     * 岗位
     */
    private Post oldPost;

    /**
     * 晋升／降级岗位
     */
    private Post post;

    /**
     * 原薪资
     */
    private BigDecimal oldMoney;

    /**
     * 调用薪资
     */
    private BigDecimal money;

    /**
     * 备注
     */
    private String remark;

    /**
     * 员工基本表
     * @return
     */
    private Employees employees;

    /**
     * 下一步审批人的Name的集合，用逗号分隔
     */
    private String nextStepApproversName;

    /**
     * 下一步审批人的id的集合，用逗号分隔
     */
    private String nextStepApproversId;

    /**
     * 文档类型(0调动申请,1晋升／降)
     * @return
     */
    private String fileType;

    /**
     * 产业中心人员(0不是 1是)
     */
    private int ifCentralStaff;



    @OneToOne
    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }
    @OneToOne
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
    @ManyToOne(fetch = FetchType.LAZY)
    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public BigDecimal getOldMoney() {
        return oldMoney;
    }

    public void setOldMoney(BigDecimal oldMoney) {
        this.oldMoney = oldMoney;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getNextStepApproversName() {
        return nextStepApproversName;
    }

    public void setNextStepApproversName(String nextStepApproversName) {
        this.nextStepApproversName = nextStepApproversName;
    }
    @Column(length = 510)
    public String getNextStepApproversId() {
        return nextStepApproversId;
    }

    public void setNextStepApproversId(String nextStepApproversId) {
        this.nextStepApproversId = nextStepApproversId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Post getOldPost() {
        return oldPost;
    }

    public void setOldPost(Post oldPost) {
        this.oldPost = oldPost;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }
}
