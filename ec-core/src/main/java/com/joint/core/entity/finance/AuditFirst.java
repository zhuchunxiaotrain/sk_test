package com.joint.core.entity.finance;

import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/14.
 */
@Entity
@Table(name="ec_auditfirst")
public class AuditFirst extends BaseFlowEntity {
    /**
     * 审计通知书
     */
    private AuditNotice auditNotice;
    /**
     * 附件上传
     */
    private List<FileManage> file;
    /**
     * 备注
     */
    private String remarks;

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    public AuditNotice getAuditNotice() {
        return auditNotice;
    }

    public void setAuditNotice(AuditNotice auditNotice) {
        this.auditNotice = auditNotice;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_auditfirst_file", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
