package com.joint.core.entity.manage;

import com.joint.base.entity.Department;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;

/**
 * Created by ZhuChunXiao on 2017/3/27.
 */
@Entity
@Table(name="ec_purchase")
public class Purchase extends BaseFlowEntity {
    /**
     * 资产类别,1 低值易耗品，2 固定资产
     */
    private String assetType;
    /**
     * 采购类别
     */
    private String purchaseType;
    /**
     * 申请部门
     */
    private Department department;
    /**
     * 采购详情json
     */
    private String purchaseDetail;
    /**
     * 合计json
     */
    private String detailNum;
    /**
     * 其他需要说明情况
     */
    private String other;
    /**
     * 支付方式，1 转账，2 现金
     */
    private String pay;
    /**
     * 收款方名称
     */
    private String toName;
    /**
     * 收款方账号
     */
    private String toNo;
    /**
     * 是否需要上报中心
     */
    private boolean leaderExam;

    /**
     * 当前流程状态
     */
    private String step;

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Column(length = 16777216)
    public String getPurchaseDetail() {
        return purchaseDetail;
    }

    public void setPurchaseDetail(String purchaseDetail) {
        this.purchaseDetail = purchaseDetail;
    }

    @Column(length = 16777216)
    public String getDetailNum() {
        return detailNum;
    }

    public void setDetailNum(String detailNum) {
        this.detailNum = detailNum;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getPay() {
        return pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getToNo() {
        return toNo;
    }

    public void setToNo(String toNo) {
        this.toNo = toNo;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isLeaderExam() {
        return leaderExam;
    }

    public void setLeaderExam(boolean leaderExam) {
        this.leaderExam = leaderExam;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }
}
