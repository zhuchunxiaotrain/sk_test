package com.joint.core.entity;

import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Post;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by root on 16/12/29.
 */
@Entity
@Table(name="ec_interview")
public class Interview extends BaseFlowEntity {

    /**
     * ID
     */
    private String cardID;

    /**
     * 面试日期
     */
    private Date interviewDate;

    /**
     * 面试人
     */
    private Set<Users> interviewer;

    /**
     * 面试记录
     */
    private Set<FileManage> record;

    private String recordText;

    /**
     * 面试结果
     */
    private String result;

    /**
     * 备注
     */
    private String remark;

    /**
     * 岗位
     */
    private Post post;

    /**
     * 部门
     */
    private Department department;

    /**
     * 是否已被录用（0是，1否）
     */
    private int ifGot;

    /**
     * 招聘表
     * @return
     */
    private Manage manage;

    @ManyToOne(fetch = FetchType.LAZY)
    public Manage getManage() {
        return manage;
    }

    public void setManage(Manage manage) {
        this.manage = manage;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Post getPost() {
        return post;
    }
    public void setPost(Post post) {
        this.post = post;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="ec_interview_interviewer",joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="interviewerId"))
    @OrderBy("name asc")
    public Set<Users> getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(Set<Users> interviewer) {
        this.interviewer = interviewer;
    }

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(name="ec_interview_record",joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="recordId"))
    @OrderBy("name asc")
    public Set<FileManage> getRecord() {
        return record;
    }

    public void setRecord(Set<FileManage> record) {
        this.record = record;
    }

    public String getRecordText() {
        return recordText;
    }

    public void setRecordText(String recordText) {
        this.recordText = recordText;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }
    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Date getInterviewDate() {
        return interviewDate;
    }

    public void setInterviewDate(Date interviewDate) {
        this.interviewDate = interviewDate;
    }

    public int getIfGot() {
        return ifGot;
    }

    public void setIfGot(int ifGot) {
        this.ifGot = ifGot;
    }
}
