package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Administrator on 2017/1/5.
 */
@Entity
@Table(name = "sk_RebateActivityDetail")
public class RebateActivityDetail extends BaseFlowEntity {

    private static final long serialVersionUID = -5442144471844445465L;
    /**
     * 退管会活动退休
     */
    private RebateActivity rebateActivity;
    /**
     *申请预算详情
     */
    private AnnualBudgetDetail annualBudgetDetail;
    /**
     * 已用金额
     */
    private BigDecimal usedAmount;
    /**
     * 实际金额
     */
    private BigDecimal actualAmount;

    /**
     * 剩余金额
     * @return
     */
    private BigDecimal remainAmount;
    @ManyToOne
    public AnnualBudgetDetail getAnnualBudgetDetail() {
        return annualBudgetDetail;
    }

    public void setAnnualBudgetDetail(AnnualBudgetDetail annualBudgetDetail) {
        this.annualBudgetDetail = annualBudgetDetail;
    }




    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public BigDecimal getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(BigDecimal remainAmount) {
        this.remainAmount = remainAmount;
    }

    public BigDecimal getUsedAmount() {
        return usedAmount;
    }

    public void setUsedAmount(BigDecimal usedAmount) {
        this.usedAmount = usedAmount;
    }
    @ManyToOne
    public RebateActivity getRebateActivity() {
        return rebateActivity;
    }

    public void setRebateActivity(RebateActivity rebateActivity) {
        this.rebateActivity = rebateActivity;
    }
}
