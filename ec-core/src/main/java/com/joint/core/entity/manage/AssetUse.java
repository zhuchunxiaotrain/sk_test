package com.joint.core.entity.manage;

import com.joint.base.entity.Department;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by ZhuChunXiao on 2017/3/28.
 * 资产使用
 */
@Entity
@Table(name="ec_assetuse")
public class AssetUse extends BaseFlowEntity {
    /**
     * 关联采购登记表
     */
    private Purchase purchase;
    /**
     * 固定资产型号
     */
    private String type;
    /**
     * 采购金额
     */
    private String money;
    /**
     * 采购日期
     */
    private Date date;
    /**
     * 使用部门
     */
    private Department department;
    /**
     * 使用人
     */
    private Users user;

    @ManyToOne(fetch = FetchType.LAZY)
    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
