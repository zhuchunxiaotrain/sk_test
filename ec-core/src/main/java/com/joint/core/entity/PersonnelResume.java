package com.joint.core.entity;

import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_personnelresume")
public class PersonnelResume extends BaseFlowEntity {

    /**
     * 培训类型
     */
    private String type;

    /**
     * 人员
     */
    private Users users;

    @OneToOne
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
