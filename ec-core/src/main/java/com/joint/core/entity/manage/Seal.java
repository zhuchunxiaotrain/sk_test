package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/20.
 * 印章使用管理
 */
@Entity
@Table(name="ec_seal")
public class Seal extends BaseFlowEntity {
    /**
     * 使用类型 1 内用  2 外借
     */
    private String type;
    /**
     * 归还日期
     */
    private Date returnDate;
    /**
     *印章名称
     */
    private Dict sealName;
    /**
     * 盖章事由
     */
    private String reason;
    /**
     * 盖章内容/份数
     */
    private String content;
    /**
     * 附件
     */
    private List<FileManage> file;
    /**
     * 是否需要上报中心
     */
    private boolean reportCenter;
    /**
     * 是否已归还
     */
    private boolean giveBack;
    /**
     * 当前状态 1申请中，2外借未归还，3已归还
     */
    private String step;
    /**
     * 相关的印章管理员
     */
    private List<Users> sealAdmin;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getSealName() {
        return sealName;
    }

    public void setSealName(Dict sealName) {
        this.sealName = sealName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_seal_file", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isReportCenter() {
        return reportCenter;
    }

    public void setReportCenter(boolean reportCenter) {
        this.reportCenter = reportCenter;
    }

    @Column(columnDefinition = "tinyint(1) default 0")
    public boolean isGiveBack() {
        return giveBack;
    }

    public void setGiveBack(boolean giveBack) {
        this.giveBack = giveBack;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_seal_sealadmin", joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="sealadminId"))
    public List<Users> getSealAdmin() {
        return sealAdmin;
    }

    public void setSealAdmin(List<Users> sealAdmin) {
        this.sealAdmin = sealAdmin;
    }
}
