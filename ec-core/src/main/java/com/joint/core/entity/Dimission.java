package com.joint.core.entity;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * Created by root on 17/1/6.
 */
@Entity
@Table(name="ec_dimission")
public class Dimission extends BaseFlowEntity {

    /**
     * 用人性质
     */
    private String nature;

    /**
     * 合同开始日期
     */
    private Date startDate;

    /**
     * 合同结束日期
     */
    private Date endDate;

    /**
     * 身份证号
     */
    private String cardID;

    /**
     * 离职性质
     */
    private String dimissionNature;

    /**
     * 离职原因
     */
    private String reasons;

    /**
     * 辞职报告
     */
    private Set<FileManage> report;
    private String reportText;

    /**
     * 预计离职时间
     */
    private Date leaveDate;

    /**
     * 备注
     */
    private String remark;

    /**
     * 下一步审批人的Name的集合，用逗号分隔
     */
    private String nextStepApproversName;

    /**
     * 下一步审批人的id的集合，用逗号分隔
     */
    private String nextStepApproversId;

    /**
     * 所属部门
     */
    private Department department;

    /**
     * 政治面貌
     */
    private String politicalStatus;

    /**
     * 员工基本表
     * @return
     */
    private Employees employees;

    /**
     * 产业中心人员(0不是 1是)
     */
    private int ifCentralStaff;


    @OneToOne
    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    @Column(length = 510)
    public String getNextStepApproversName() {
        return nextStepApproversName;
    }

    public void setNextStepApproversName(String nextStepApproversName) {
        this.nextStepApproversName = nextStepApproversName;
    }

    public String getNextStepApproversId() {
        return nextStepApproversId;
    }

    public void setNextStepApproversId(String nextStepApproversId) {
        this.nextStepApproversId = nextStepApproversId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getDimissionNature() {
        return dimissionNature;
    }

    public void setDimissionNature(String dimissionNature) {
        this.dimissionNature = dimissionNature;
    }

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(name="ec_dimission_report",joinColumns = @JoinColumn(name="id"),inverseJoinColumns = @JoinColumn(name="reportId"))
    @OrderBy("name asc")
    public Set<FileManage> getReport() {
        return report;
    }

    public void setReport(Set<FileManage> report) {
        this.report = report;
    }

    public Date getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(Date leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPoliticalStatus() {
        return politicalStatus;
    }

    public void setPoliticalStatus(String politicalStatus) {
        this.politicalStatus = politicalStatus;
    }

    public String getReportText() {
        return reportText;
    }

    public void setReportText(String reportText) {
        this.reportText = reportText;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }
}
