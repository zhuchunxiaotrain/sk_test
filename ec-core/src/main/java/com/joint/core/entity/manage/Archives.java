package com.joint.core.entity.manage;

import com.fz.us.dict.entity.Dict;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@Entity
@Table(name="ec_archives")
public class Archives extends BaseFlowEntity {
    /**
     * 档案类型
     */
    private Dict type;
    /**
     * 档案类目
     */
    private Dict category;
    /**
     * 所属项目
     */
    private String project;
    /**
     * 档案明细
     */
    private List<ArchivesDetail> archivesDetailList;

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getType() {
        return type;
    }

    public void setType(Dict type) {
        this.type = type;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Dict getCategory() {
        return category;
    }

    public void setCategory(Dict category) {
        this.category = category;
    }

    @OneToMany(fetch = FetchType.LAZY)
    public List<ArchivesDetail> getArchivesDetailList() {
        return archivesDetailList;
    }

    public void setArchivesDetailList(List<ArchivesDetail> archivesDetailList) {
        this.archivesDetailList = archivesDetailList;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }
}
