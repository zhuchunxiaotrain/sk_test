package com.joint.core.entity.manage;


import com.joint.base.entity.FileManage;
import com.joint.base.parent.BaseFlowEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/1/12.
 * 任务反馈单
 */
@Entity
@Table(name="ec_taskback")
public class TaskBack extends BaseFlowEntity {


    private static final long serialVersionUID = -2167972784190047620L;
    /**
     * 任务督办单
     */
    private TaskControl taskControl;
    /**
     * 完成程度
     */
    private String degree;
    /**
     * 实际完成时间
     */
    private Date finishTime;
    /**
     * 任务反馈内容
     */
    private String content;

    /**
     *相关附件
     */
    private List<FileManage> file;

    /**
     * 备注
     */
    private String remark;

    @ManyToOne(fetch = FetchType.LAZY)
    public TaskControl getTaskControl() {
        return taskControl;
    }

    public void setTaskControl(TaskControl taskControl) {
        this.taskControl = taskControl;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ec_taskback_file", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "fileId"))
    public List<FileManage> getFile() {
        return file;
    }

    public void setFile(List<FileManage> file) {
        this.file = file;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
