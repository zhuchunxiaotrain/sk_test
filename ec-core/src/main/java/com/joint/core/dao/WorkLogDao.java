package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.WorkLog;

/**
 * Created by ZhuChunXiao on 2017/3/21.
 */
public interface WorkLogDao extends BaseFlowDao<WorkLog,String> {
}
