package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.CarConfig;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
public interface CarConfigDao extends BaseFlowDao<CarConfig,String> {
}
