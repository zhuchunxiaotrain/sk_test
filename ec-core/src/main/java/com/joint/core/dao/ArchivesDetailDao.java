package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.ArchivesDetail;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
public interface ArchivesDetailDao extends BaseFlowDao<ArchivesDetail,String> {
}
