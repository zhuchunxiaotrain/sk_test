package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.LetterRegisterDao;
import com.joint.core.dao.StrongBigDao;
import com.joint.core.entity.LetterRegister;
import com.joint.core.entity.StrongBig;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class LetterRegisterImpl extends BaseFlowDaoImpl<LetterRegister, String > implements LetterRegisterDao{
}
