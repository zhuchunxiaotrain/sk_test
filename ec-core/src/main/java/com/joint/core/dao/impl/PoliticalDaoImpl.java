package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.PoliticalDao;
import com.joint.core.entity.Political;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class PoliticalDaoImpl extends BaseFlowDaoImpl<Political,String> implements PoliticalDao {

}
