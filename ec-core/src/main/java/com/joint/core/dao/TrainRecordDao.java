package com.joint.core.dao;


import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.TrainRecord;

public interface TrainRecordDao extends BaseFlowDao<TrainRecord,String> {

}
