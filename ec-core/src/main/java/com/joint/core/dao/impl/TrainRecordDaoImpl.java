package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.TrainRecordDao;
import com.joint.core.entity.TrainRecord;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class TrainRecordDaoImpl extends BaseFlowDaoImpl<TrainRecord,String> implements TrainRecordDao {



}
