package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.CarConfig;
import com.joint.core.entity.manage.Oil;
import com.joint.core.entity.manage.OilQuery;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
public interface OilQueryDao extends BaseFlowDao<OilQuery,String> {
    Double getMoneyNum(CarConfig carConfig);
}
