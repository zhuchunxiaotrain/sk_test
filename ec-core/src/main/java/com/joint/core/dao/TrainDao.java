package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Train;
import org.apache.fop.area.Trait;

public interface TrainDao extends BaseFlowDao<Train,String> {

}
