package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.QualificationsDao;
import com.joint.core.entity.manage.Qualifications;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/20.
 */
@Repository
public class QualificationsDaoImpl extends BaseFlowDaoImpl<Qualifications, String > implements QualificationsDao {
}
