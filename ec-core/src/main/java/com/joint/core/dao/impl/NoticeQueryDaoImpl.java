package com.joint.core.dao.impl;


import com.joint.base.dao.impl.BaseEntityDaoImpl;
import com.joint.core.dao.NoticeQueryDao;

import com.joint.core.entity.manage.NoticeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class NoticeQueryDaoImpl extends BaseEntityDaoImpl<NoticeQuery,String> implements NoticeQueryDao {
    @Override
    public List<NoticeQuery> findUsersByNoticeQuery(String userId) {
        Assert.notNull(userId, "userId is required");
        String hql = "from NoticeQuery where users.id = :userId";
        List<NoticeQuery> noticeQueryList = getSession().createQuery(hql).setParameter("userId", userId).list();
        return noticeQueryList;
    }

    @Override
    public List<NoticeQuery> findUsersByNoticeQuery(String userId, String keyId) {
        Assert.notNull(userId, "userId is required");
        Assert.notNull(keyId, "keyId is required");
        String hql = "from NoticeQuery where users.id = :userId and keyId=:keyId";
        List<NoticeQuery> noticeQueryList = getSession().createQuery(hql).setParameter("userId", userId).setParameter("keyId", keyId).list();
        return noticeQueryList;
    }
}
