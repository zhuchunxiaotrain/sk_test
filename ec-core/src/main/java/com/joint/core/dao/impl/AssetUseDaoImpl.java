package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.AssetUseDao;
import com.joint.core.entity.manage.AssetUse;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/28.
 */
@Repository
public class AssetUseDaoImpl extends BaseFlowDaoImpl<AssetUse,String> implements AssetUseDao {
}
