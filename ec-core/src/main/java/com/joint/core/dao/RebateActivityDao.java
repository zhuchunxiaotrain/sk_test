package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.AnnualBudget;
import com.joint.core.entity.RebateActivity;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface RebateActivityDao extends BaseFlowDao <RebateActivity,String> {
}
