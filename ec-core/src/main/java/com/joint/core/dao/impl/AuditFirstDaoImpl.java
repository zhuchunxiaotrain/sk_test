package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.AuditFirstDao;
import com.joint.core.entity.finance.AuditFirst;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/14.
 */
@Repository
public class AuditFirstDaoImpl extends BaseFlowDaoImpl<AuditFirst,String> implements AuditFirstDao {
}
