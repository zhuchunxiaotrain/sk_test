package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.finance.AuditNotice;

import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/13.
 */
public interface AuditNoticeDao extends BaseFlowDao<AuditNotice,String> {
}
