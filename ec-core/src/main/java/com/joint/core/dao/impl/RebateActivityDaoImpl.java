package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.AnnualBudgetDao;
import com.joint.core.dao.RebateActivityDao;
import com.joint.core.entity.AnnualBudget;
import com.joint.core.entity.RebateActivity;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class RebateActivityDaoImpl extends BaseFlowDaoImpl<RebateActivity, String > implements RebateActivityDao{
}
