package com.joint.core.dao;


import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.PersonnelResume;
import com.joint.core.entity.TrainRecord;

public interface PersonnelResumeDao extends BaseFlowDao<PersonnelResume,String> {

}
