package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.MeetingSignDao;
import com.joint.core.dao.RegisteDao;
import com.joint.core.entity.manage.MeetingSign;
import com.joint.core.entity.manage.Registe;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
@Repository
public class RegisteDaoImpl extends BaseFlowDaoImpl<Registe,String> implements RegisteDao {
}
