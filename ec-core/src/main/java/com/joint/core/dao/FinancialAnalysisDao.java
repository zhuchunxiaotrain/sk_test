package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.finance.FinancialAnalysis;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
public interface FinancialAnalysisDao extends BaseFlowDao<FinancialAnalysis,String> {
}
