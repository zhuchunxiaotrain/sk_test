package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.FinanceYearDao;
import com.joint.core.entity.finance.FinanceYear;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
@Repository
public class FinanceYearDaoImpl extends BaseFlowDaoImpl<FinanceYear,String> implements FinanceYearDao {
}
