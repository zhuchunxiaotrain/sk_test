package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.RecordsDao;
import com.joint.core.entity.Records;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class RecordsDaoImpl extends BaseFlowDaoImpl<Records,String> implements RecordsDao {

}
