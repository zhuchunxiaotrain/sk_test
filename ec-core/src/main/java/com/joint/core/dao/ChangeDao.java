package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Change;

public interface ChangeDao extends BaseFlowDao<Change,String> {

}
