package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.WorkPlanDao;
import com.joint.core.entity.finance.WorkPlan;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/10.
 */
@Repository
public class WorkPlanDaoImpl extends BaseFlowDaoImpl<WorkPlan,String> implements WorkPlanDao {
}
