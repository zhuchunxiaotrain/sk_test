package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.CarConfigDao;
import com.joint.core.entity.manage.CarConfig;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
@Repository
public class CarConfigDaoImpl extends BaseFlowDaoImpl<CarConfig,String> implements CarConfigDao {
}
