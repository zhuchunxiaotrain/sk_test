package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.SelectionPublicityDao;
import com.joint.core.dao.SelectionSpeechDao;
import com.joint.core.entity.SelectionPublicity;
import com.joint.core.entity.SelectionSpeech;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class SelectionPublicityDaoImpl extends BaseFlowDaoImpl<SelectionPublicity, String > implements SelectionPublicityDao {
}
