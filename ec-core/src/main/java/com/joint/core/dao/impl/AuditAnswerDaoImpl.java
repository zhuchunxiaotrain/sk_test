package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.AuditAnswerDao;
import com.joint.core.entity.finance.AuditAnswer;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/15.
 */
@Repository
public class AuditAnswerDaoImpl extends BaseFlowDaoImpl<AuditAnswer,String> implements AuditAnswerDao {
}
