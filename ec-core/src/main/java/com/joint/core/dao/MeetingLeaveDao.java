package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.MeetingLeave;
import com.joint.core.entity.manage.MeetingSign;

import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
public interface MeetingLeaveDao extends BaseFlowDao<MeetingLeave,String>{
    List<MeetingLeave> findDataByUsersParentId(String userId, String parentId);
}
