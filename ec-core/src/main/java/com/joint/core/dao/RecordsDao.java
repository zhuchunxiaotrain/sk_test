package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Records;

public interface RecordsDao extends BaseFlowDao<Records,String> {

}
