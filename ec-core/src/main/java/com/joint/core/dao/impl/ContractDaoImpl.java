package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ContractDao;
import com.joint.core.entity.manage.Contract;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@Repository
public class ContractDaoImpl extends BaseFlowDaoImpl<Contract,String> implements ContractDao {
}
