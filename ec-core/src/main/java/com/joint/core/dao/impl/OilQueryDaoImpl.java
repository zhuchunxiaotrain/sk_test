package com.joint.core.dao.impl;

import com.joint.base.bean.FlowEnum;
import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.OilQueryDao;
import com.joint.core.entity.manage.CarConfig;
import com.joint.core.entity.manage.Oil;
import com.joint.core.entity.manage.OilQuery;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
@Repository
public class OilQueryDaoImpl extends BaseFlowDaoImpl<OilQuery,String> implements OilQueryDao {
    @Override
    public Double getMoneyNum(CarConfig carConfig) {
        FlowEnum.ProcessState state=FlowEnum.ProcessState.Draft;
        Double max=(Double)getSession().createQuery("select max(o.oilMoneyNum) from OilQuery o where o.carConfig=? and o.processState!=?")
                .setParameter(0,carConfig)
                .setParameter(1,state)
                .uniqueResult();
        System.out.println(max+"----------------------");
        if(max==null){
            max=0d;
        }
        return max;
    }
}
