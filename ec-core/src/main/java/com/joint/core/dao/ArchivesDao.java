package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.Archives;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
public interface ArchivesDao extends BaseFlowDao<Archives,String> {
}
