package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.AnnualBudgetDao;
import com.joint.core.dao.StrongBigDao;
import com.joint.core.entity.AnnualBudget;
import com.joint.core.entity.StrongBig;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class AnnualBudgetDaoImpl extends BaseFlowDaoImpl<AnnualBudget, String > implements AnnualBudgetDao{
}
