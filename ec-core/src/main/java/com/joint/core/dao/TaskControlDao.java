package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.TaskControl;

public interface TaskControlDao extends BaseFlowDao<TaskControl,String> {

}
