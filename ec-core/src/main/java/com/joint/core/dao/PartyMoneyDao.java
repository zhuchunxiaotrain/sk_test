package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.PartyMoney;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface PartyMoneyDao extends BaseFlowDao <PartyMoney,String> {
}
