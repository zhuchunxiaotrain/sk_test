package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.FinancialAnalysisDao;
import com.joint.core.entity.finance.FinancialAnalysis;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
@Repository
public class FinancialAnalysisDaoImpl extends BaseFlowDaoImpl<FinancialAnalysis,String> implements FinancialAnalysisDao {
}
