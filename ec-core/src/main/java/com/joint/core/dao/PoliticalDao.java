package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Political;

public interface PoliticalDao extends BaseFlowDao<Political,String> {

}
