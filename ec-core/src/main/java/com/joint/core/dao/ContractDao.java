package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.Contract;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
public interface ContractDao extends BaseFlowDao<Contract,String> {
}
