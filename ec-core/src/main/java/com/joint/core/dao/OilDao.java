package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.Oil;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
public interface OilDao extends BaseFlowDao<Oil,String> {
}
