package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.DispatchReviseDao;
import com.joint.core.entity.manage.DispatchRevise;
import org.springframework.stereotype.Repository;


@Repository
public class DispatchReviseDaoImpl extends BaseFlowDaoImpl<DispatchRevise,String> implements DispatchReviseDao {
}
