package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Family;
import com.joint.core.entity.Manage;

public interface FamilyDao extends BaseFlowDao<Family,String> {

}
