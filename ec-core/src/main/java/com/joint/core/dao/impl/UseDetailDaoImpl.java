package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.UseDetailDao;
import com.joint.core.entity.manage.UseDetail;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/29.
 */
@Repository
public class UseDetailDaoImpl extends BaseFlowDaoImpl<UseDetail,String> implements UseDetailDao {
}
