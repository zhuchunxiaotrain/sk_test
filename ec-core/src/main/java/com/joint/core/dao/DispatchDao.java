package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.Dispatch;

public interface DispatchDao extends BaseFlowDao<Dispatch,String> {

}
