package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.OilDao;
import com.joint.core.entity.manage.Oil;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
@Repository
public class OilDaoImpl extends BaseFlowDaoImpl<Oil, String > implements OilDao {
}
