package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.finance.NewsMonth;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
public interface NewsMonthDao extends BaseFlowDao<NewsMonth,String> {
}
