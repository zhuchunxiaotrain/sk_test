package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.PartyMoneyDao;
import com.joint.core.dao.PartyMoneyDetailDao;
import com.joint.core.entity.PartyMoney;
import com.joint.core.entity.PartyMoneyDetail;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class PartyMoneyDetailDaoImpl extends BaseFlowDaoImpl<PartyMoneyDetail, String > implements PartyMoneyDetailDao {
}
