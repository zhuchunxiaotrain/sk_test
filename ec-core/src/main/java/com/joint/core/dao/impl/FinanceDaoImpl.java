package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.FinanceDao;
import com.joint.core.entity.finance.Finance;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/2.
 */
@Repository
public class FinanceDaoImpl extends BaseFlowDaoImpl<Finance,String> implements FinanceDao {
}
