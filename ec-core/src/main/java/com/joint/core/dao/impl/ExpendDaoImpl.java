package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ExpendDao;
import com.joint.core.entity.finance.Expend;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/4/1.
 */
@Repository
public class ExpendDaoImpl extends BaseFlowDaoImpl<Expend,String> implements ExpendDao {
}
