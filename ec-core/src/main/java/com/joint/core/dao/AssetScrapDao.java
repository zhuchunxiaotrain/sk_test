package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.AssetScrap;

/**
 * Created by ZhuChunXiao on 2017/3/30.
 */
public interface AssetScrapDao extends BaseFlowDao<AssetScrap,String> {
}
