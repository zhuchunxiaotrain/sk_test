package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Interview;
import com.joint.core.entity.Manage;

public interface InterviewDao extends BaseFlowDao<Interview,String> {

}
