package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.AuditNoticeDao;
import com.joint.core.entity.finance.AuditNotice;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/13.
 */
@Repository
public class AuditNoticeDaoImpl extends BaseFlowDaoImpl<AuditNotice,String> implements AuditNoticeDao {
}
