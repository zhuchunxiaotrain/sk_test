package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.MeetingUse;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
public interface MeetingUseDao extends BaseFlowDao<MeetingUse,String>{
}
