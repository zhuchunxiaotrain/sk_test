package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.MoneyManager;
import com.joint.core.entity.MoneyManagerDetail;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface MoneyManagerDetailDao extends BaseFlowDao <MoneyManagerDetail,String> {
}
