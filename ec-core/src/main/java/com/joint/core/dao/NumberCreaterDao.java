package com.joint.core.dao;

import com.joint.base.dao.BaseEntityDao;
import com.joint.base.entity.Company;
import com.joint.core.entity.NumberCreater;

/**
 * Created by dqf on 2015/8/25.
 */
public interface NumberCreaterDao extends BaseEntityDao<NumberCreater,String> {
    public String getNumber(String entityName, String header, int length, Company company);
}
