package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.ManageProInfo;

public interface ManageProInfoDao extends BaseFlowDao<ManageProInfo,String> {

}
