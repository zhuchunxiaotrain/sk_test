package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.finance.ModifyYear;

/**
 * Created by ZhuChunXiao on 2017/3/7.
 */
public interface ModifyYearDao extends BaseFlowDao<ModifyYear,String> {
    void updateCalFile(String id,String sql);
}
