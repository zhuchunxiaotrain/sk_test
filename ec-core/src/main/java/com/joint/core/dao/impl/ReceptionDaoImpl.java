package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ReceptionDao;
import com.joint.core.entity.manage.Reception;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/30.
 */
@Repository
public class ReceptionDaoImpl extends BaseFlowDaoImpl<Reception,String> implements ReceptionDao {
}
