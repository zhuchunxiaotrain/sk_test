package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.finance.WorkPlan;

/**
 * Created by ZhuChunXiao on 2017/3/10.
 */
public interface WorkPlanDao extends BaseFlowDao<WorkPlan,String> {
}
