package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.Reception;

/**
 * Created by ZhuChunXiao on 2017/3/30.
 */
public interface ReceptionDao extends BaseFlowDao<Reception,String> {
}
