package com.joint.core.dao;

import com.joint.base.dao.BaseEntityDao;

import com.joint.core.entity.manage.NoticeQuery;

import java.util.List;

public interface NoticeQueryDao extends BaseEntityDao<NoticeQuery,String> {

    public List<NoticeQuery> findUsersByNoticeQuery(String userId);

    public List<NoticeQuery> findUsersByNoticeQuery(String userId, String keyId);

}
