package com.joint.core.dao;


import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Apply;

public interface ApplyDao extends BaseFlowDao<Apply,String> {

}
