package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.LeaveDetailDao;
import com.joint.core.entity.LeaveDetail;
import com.joint.core.entity.manage.NoticeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.Iterator;
import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class LeaveDetailDaoImpl extends BaseFlowDaoImpl<LeaveDetail,String> implements LeaveDetailDao {


    @Override
    public LeaveDetail findLeaveDetail(String userId, Integer time) {
        Assert.notNull(userId, "userId is required");
        Assert.notNull(time, "time is required");
        String hql = "from LeaveDetail where users.id = :userId and time=:time";
        List<LeaveDetail> leaveDetailList = getSession().createQuery(hql).setParameter("userId", userId).setParameter("time", time).list();
        LeaveDetail leaveDetail = leaveDetailList.get(0);
        return leaveDetail;

    }

    @Override
    public LeaveDetail findLeaveDetailByUsers(String userId) {
        Assert.notNull(userId, "userId is required");
        String hql = "from LeaveDetail where users.id = :userId";
        List<LeaveDetail> leaveDetailList = getSession().createQuery(hql).setParameter("userId", userId).list();
        LeaveDetail leaveDetail = leaveDetailList.get(0);
        return leaveDetail;    }
}
