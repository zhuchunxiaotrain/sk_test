package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.AnnualBudgetDao;
import com.joint.core.dao.AnnualBudgetDetailDao;
import com.joint.core.entity.AnnualBudget;
import com.joint.core.entity.AnnualBudgetDetail;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class AnnualBudgetDetailDaoImpl extends BaseFlowDaoImpl<AnnualBudgetDetail, String > implements AnnualBudgetDetailDao{
}
