package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.CalculationYearDao;
import com.joint.core.entity.finance.CalculationYear;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/6.
 */
@Repository
public class CalculationYearDaoImpl extends BaseFlowDaoImpl<CalculationYear,String> implements CalculationYearDao {
}
