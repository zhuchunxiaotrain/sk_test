package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.SealDao;
import com.joint.core.entity.manage.Seal;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/20.
 */
@Repository
public class SealDaoImpl extends BaseFlowDaoImpl<Seal, String > implements SealDao {
}
