package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.Schedule;

public interface ScheduleDao extends BaseFlowDao<Schedule,String> {

}
