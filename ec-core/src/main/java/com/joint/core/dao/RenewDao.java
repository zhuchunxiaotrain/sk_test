package com.joint.core.dao;


import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Renew;

public interface RenewDao extends BaseFlowDao<Renew,String> {

}
