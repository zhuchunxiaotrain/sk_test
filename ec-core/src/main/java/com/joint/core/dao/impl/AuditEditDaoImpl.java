package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.AuditEditDao;
import com.joint.core.entity.finance.AuditEdit;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/15.
 */
@Repository
public class AuditEditDaoImpl extends BaseFlowDaoImpl<AuditEdit,String> implements AuditEditDao {
}
