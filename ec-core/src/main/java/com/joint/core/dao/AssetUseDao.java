package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.AssetUse;

/**
 * Created by ZhuChunXiao on 2017/3/28.
 */
public interface AssetUseDao extends BaseFlowDao<AssetUse,String> {
}
