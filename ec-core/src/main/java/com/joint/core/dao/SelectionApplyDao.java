package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.SelectionApply;
import com.joint.core.entity.SelectionNotice;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface SelectionApplyDao extends BaseFlowDao <SelectionApply,String> {
}
