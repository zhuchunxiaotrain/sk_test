package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.finance.CalculationYear;

/**
 * Created by ZhuChunXiao on 2017/3/6.
 */
public interface CalculationYearDao extends BaseFlowDao<CalculationYear,String> {
}
