package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Leave;
import com.joint.core.entity.Train;

public interface LeaveDao extends BaseFlowDao<Leave,String> {

}
