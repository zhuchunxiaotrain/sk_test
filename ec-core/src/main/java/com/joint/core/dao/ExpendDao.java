package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.finance.Expend;

/**
 * Created by ZhuChunXiao on 2017/4/1.
 */
public interface ExpendDao extends BaseFlowDao<Expend,String> {
}
