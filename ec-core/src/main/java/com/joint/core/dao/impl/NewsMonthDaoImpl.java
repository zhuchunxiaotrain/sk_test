package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.NewsMonthDao;
import com.joint.core.entity.finance.NewsMonth;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
@Repository
public class NewsMonthDaoImpl extends BaseFlowDaoImpl<NewsMonth,String> implements NewsMonthDao {
}
