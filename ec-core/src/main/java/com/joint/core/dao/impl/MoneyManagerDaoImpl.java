package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.MoneyManagerDao;
import com.joint.core.entity.MoneyManager;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class MoneyManagerDaoImpl extends BaseFlowDaoImpl<MoneyManager, String > implements MoneyManagerDao {
}
