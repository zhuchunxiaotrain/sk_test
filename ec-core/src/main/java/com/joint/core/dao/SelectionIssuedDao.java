package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.SelectionIssued;
import com.joint.core.entity.SelectionPublicity;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface SelectionIssuedDao extends BaseFlowDao <SelectionIssued,String> {
}
