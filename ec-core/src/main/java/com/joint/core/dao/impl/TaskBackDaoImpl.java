package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.TaskBackDao;
import com.joint.core.entity.manage.TaskBack;
import org.springframework.stereotype.Repository;


@Repository
public class TaskBackDaoImpl extends BaseFlowDaoImpl<TaskBack,String> implements TaskBackDao {
}
