package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.AuditLastDao;
import com.joint.core.entity.finance.AuditLast;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/15.
 */
@Repository
public class AuditLastDaoImpl extends BaseFlowDaoImpl<AuditLast,String> implements AuditLastDao {
}
