package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ApplyDao;
import com.joint.core.entity.Apply;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class ApplyDaoImpl extends BaseFlowDaoImpl<Apply,String> implements ApplyDao {

}
