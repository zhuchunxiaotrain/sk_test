package com.joint.core.dao;


import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Employees;

import java.util.List;

public interface EmployeesDao extends BaseFlowDao<Employees,String> {

    public List<Employees> findEmployees(String partyMember);

}
