package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ParticipantDao;
import com.joint.core.dao.TrainDao;
import com.joint.core.entity.Participant;
import com.joint.core.entity.Train;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class ParticipantDaoImpl extends BaseFlowDaoImpl<Participant,String> implements ParticipantDao {



}
