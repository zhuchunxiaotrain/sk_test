package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.SelectionIssuedDao;
import com.joint.core.dao.SelectionPublicityDao;
import com.joint.core.entity.SelectionIssued;
import com.joint.core.entity.SelectionPublicity;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class SelectionIssuedDaoImpl extends BaseFlowDaoImpl<SelectionIssued, String > implements SelectionIssuedDao{
}
