package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.MeetingRecordDao;
import com.joint.core.dao.MeetingSignDao;
import com.joint.core.entity.manage.MeetingRecord;
import com.joint.core.entity.manage.MeetingSign;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;

/**ing
 * Created by ZhuChunXiao on 2017/3/24.
 */
@Repository
public class MeetingSignDaoImpl extends BaseFlowDaoImpl<MeetingSign,String> implements MeetingSignDao {
    @Override
    public List<MeetingSign> findDataByUsers(String userId) {
        Assert.notNull(userId, "userId is required");
        String hql = "from MeetingSign where creater.id = :userId";
        List<MeetingSign> meetingSignList = getSession().createQuery(hql).setParameter("userId", userId).list();
        return meetingSignList;
    }

    @Override
    public List<MeetingSign> findDataByUsersParentId(String userId, String parentId) {
        Assert.notNull(userId, "userId is required");
        Assert.notNull(parentId, "parentId is required");
        String hql = "from MeetingSign where creater.id = :userId and meetingUse.id = :parentId";
        List<MeetingSign> meetingSignList = getSession().createQuery(hql).setParameter("userId", userId)
                .setParameter("parentId", parentId).list();
        return meetingSignList;
    }
}
