package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.ActivityManager;
import com.joint.core.entity.SelectionApply;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface ActivityManagerDao extends BaseFlowDao <ActivityManager,String> {
}
