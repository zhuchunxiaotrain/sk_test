package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Participant;
import com.joint.core.entity.Train;

public interface ParticipantDao extends BaseFlowDao<Participant,String> {

}
