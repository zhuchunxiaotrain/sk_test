package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.MeetingRecordDao;
import com.joint.core.dao.MeetingUseDao;
import com.joint.core.entity.manage.MeetingRecord;
import com.joint.core.entity.manage.MeetingUse;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
@Repository
public class MeetingRecordDaoImpl extends BaseFlowDaoImpl<MeetingRecord,String> implements MeetingRecordDao {
}
