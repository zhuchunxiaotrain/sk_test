package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Summary;
import com.joint.core.entity.Train;

public interface SummaryDao extends BaseFlowDao<Summary,String> {

}
