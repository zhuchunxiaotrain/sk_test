package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.MoneyAvgDao;
import com.joint.core.dao.MoneyAvgDetailDao;
import com.joint.core.entity.MoneyAvg;
import com.joint.core.entity.MoneyAvgDetail;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class MoneyAvgDetailDaoImpl extends BaseFlowDaoImpl<MoneyAvgDetail, String > implements MoneyAvgDetailDao {
}
