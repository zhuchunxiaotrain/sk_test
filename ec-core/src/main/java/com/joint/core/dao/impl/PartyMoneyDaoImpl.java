package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.PartyMoneyDao;
import com.joint.core.entity.PartyMoney;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class PartyMoneyDaoImpl extends BaseFlowDaoImpl<PartyMoney, String > implements PartyMoneyDao {
}
