package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.EmployeesDao;
import com.joint.core.dao.EmploymentDao;
import com.joint.core.entity.Employees;
import com.joint.core.entity.Employment;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class EmployeesDaoImpl extends BaseFlowDaoImpl<Employees,String> implements EmployeesDao {

    @Override
    public List<Employees> findEmployees(String partyMember) {

        String hql = "from"+this.entityClass.getName()+"employees where employees.partyMember=:partyMember";
        List<Employees> employeesList = getSession().createQuery(hql).setParameter("partyMember", partyMember).list();
        return employeesList;
    }
}
