package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.ManageProInfo;
import com.joint.core.entity.manage.Notice;

public interface NoticeDao extends BaseFlowDao<Notice,String> {

}
