package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.DimissionDao;
import com.joint.core.entity.Dimission;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class DimissionDaoImpl extends BaseFlowDaoImpl<Dimission,String> implements DimissionDao {

}
