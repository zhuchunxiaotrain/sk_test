package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.LetterRegister;
import com.joint.core.entity.StrongBig;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface LetterRegisterDao extends BaseFlowDao <LetterRegister,String> {
}
