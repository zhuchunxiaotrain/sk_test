package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.EmploymentDao;
import com.joint.core.dao.ManageDao;
import com.joint.core.entity.Employment;
import com.joint.core.entity.Manage;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class EmploymentDaoImpl extends BaseFlowDaoImpl<Employment,String> implements EmploymentDao {
}
