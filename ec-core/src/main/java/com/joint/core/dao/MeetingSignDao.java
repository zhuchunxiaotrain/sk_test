package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.MeetingSign;
import com.joint.core.entity.manage.MeetingUse;

import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
public interface MeetingSignDao extends BaseFlowDao<MeetingSign,String>{
    List<MeetingSign> findDataByUsers(String userId);

    List<MeetingSign> findDataByUsersParentId(String userId, String parentId);
}
