package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.finance.AuditLast;

/**
 * Created by ZhuChunXiao on 2017/3/15.
 */
public interface AuditLastDao extends BaseFlowDao<AuditLast,String> {
}
