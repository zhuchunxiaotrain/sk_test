package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.DispatchDao;
import com.joint.core.entity.manage.Dispatch;
import org.springframework.stereotype.Repository;


@Repository
public class DispatchDaoImpl extends BaseFlowDaoImpl<Dispatch,String> implements DispatchDao {
}
