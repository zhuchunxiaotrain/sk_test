package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.WorkLogDao;
import com.joint.core.entity.manage.WorkLog;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/21.
 */
@Repository
public class WorkLogDaoImpl extends BaseFlowDaoImpl<WorkLog,String> implements WorkLogDao {
}
