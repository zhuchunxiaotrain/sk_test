package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.QualificationsDict;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
public interface QualificationsDictDao extends BaseFlowDao<QualificationsDict,String> {
}
