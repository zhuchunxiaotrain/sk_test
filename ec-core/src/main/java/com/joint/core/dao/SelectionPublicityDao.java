package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.SelectionPublicity;
import com.joint.core.entity.SelectionSpeech;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface SelectionPublicityDao extends BaseFlowDao <SelectionPublicity,String> {
}
