package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.SelectionApplyDao;
import com.joint.core.dao.SelectionNoticeDao;
import com.joint.core.entity.SelectionApply;
import com.joint.core.entity.SelectionNotice;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class SelectionApplyDaoImpl extends BaseFlowDaoImpl<SelectionApply, String > implements SelectionApplyDao{
}
