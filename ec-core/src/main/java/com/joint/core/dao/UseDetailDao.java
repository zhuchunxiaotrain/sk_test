package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.UseDetail;

/**
 * Created by ZhuChunXiao on 2017/3/29.
 */
public interface UseDetailDao extends BaseFlowDao<UseDetail,String> {
}
