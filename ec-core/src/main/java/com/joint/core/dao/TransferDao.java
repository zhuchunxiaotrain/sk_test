package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Change;

public interface TransferDao extends BaseFlowDao<Change,String> {

}
