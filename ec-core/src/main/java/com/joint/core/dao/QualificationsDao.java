package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.Qualifications;

/**
 * Created by ZhuChunXiao on 2017/3/20.
 */
public interface QualificationsDao extends BaseFlowDao<Qualifications,String> {
}
