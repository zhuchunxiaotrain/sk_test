package com.joint.core.dao.impl;

import com.fz.us.base.bean.BaseEnum;
import com.joint.base.dao.UsersDao;
import com.joint.base.dao.impl.BaseEntityDaoImpl;
import com.joint.base.entity.Company;
import com.joint.base.entity.Users;
import com.joint.base.util.StringUtils;
import com.joint.core.dao.NumberCreaterDao;
import com.joint.core.entity.NumberCreater;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by dqf on 2015/8/25.
 */
@Repository
public class NumberCreaterDaoImpl extends BaseEntityDaoImpl<NumberCreater,String> implements NumberCreaterDao {
    @Resource
    UsersDao usersDao;

    @Override
    public String getNumber(String entityName, String header, int length, Company company) {
        Assert.notNull(entityName, "entityName is required");
        if (length <=0){
            length = 4;
        }

        if (company == null){
            Subject subject= SecurityUtils.getSubject();
            String mobile = (String) subject.getPrincipal();
            Users users = usersDao.getUsersByMobile(mobile);
            company = users.getCompany();
        }

        String hql = " from "+this.entityClass.getName()+" creater where creater.name= :entityName and creater.company= :company ";
        List<NumberCreater> list = this.getSession().createQuery(hql).setParameter("entityName",entityName).setParameter("company",company).list();
        String number = "";
        Calendar calendar= Calendar.getInstance();
        String this_year = String.valueOf(calendar.get(Calendar.YEAR));
        if (list.size() == 0){
            NumberCreater numberCreater = new NumberCreater();
            //numberCreater.setEntityName(entityName);
            numberCreater.setNextNumber("2");
            numberCreater.setUsingYear(this_year);
            numberCreater.setName(entityName);
            numberCreater.setCreateDate(new Date());
            numberCreater.setTimestamp(String.valueOf(System.currentTimeMillis()));
            numberCreater.setCompany(company);
            numberCreater.setState(BaseEnum.StateEnum.Enable);
            this.getSession().save(numberCreater);
            number = "1";
        } else {
            NumberCreater numberCreater = list.get(0);
            number = numberCreater.getNextNumber();
            String year = numberCreater.getUsingYear();
            if (StringUtils.equals(this_year, numberCreater.getUsingYear())){
                numberCreater.setNextNumber((Integer.parseInt(numberCreater.getNextNumber())+1)+"");
            } else {
                numberCreater.setUsingYear(this_year);
                numberCreater.setNextNumber("2");
                number = "1";
            }
            this.getSession().update(numberCreater);
        }

        if (number.length() < length){
            int zerol = length-number.length();
            for (int i=0;i< zerol;i++){
                number = "0"+number;
            }
        }
        if (number.length() > length){
            number = number.substring(number.length()-length);
        }
        return header+number;
    }
}



























