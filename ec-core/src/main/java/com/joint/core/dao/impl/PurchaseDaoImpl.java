package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.PurchaseDao;
import com.joint.core.entity.manage.Purchase;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/27.
 */
@Repository
public class PurchaseDaoImpl extends BaseFlowDaoImpl<Purchase, String > implements PurchaseDao {
}
