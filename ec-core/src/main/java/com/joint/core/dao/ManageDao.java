package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Manage;

import java.util.List;

public interface ManageDao extends BaseFlowDao<Manage,String> {

}
