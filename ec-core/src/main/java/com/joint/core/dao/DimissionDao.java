package com.joint.core.dao;


import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Dimission;

public interface DimissionDao extends BaseFlowDao<Dimission,String> {

}
