package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ChangeDao;
import com.joint.core.entity.Change;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class ChangeDaoImpl extends BaseFlowDaoImpl<Change,String> implements ChangeDao {



}
