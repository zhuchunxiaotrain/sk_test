package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Manage;
import com.joint.core.entity.finance.Finance;

/**
 * Created by ZhuChunXiao on 2017/3/2.
 */
public interface FinanceDao extends BaseFlowDao<Finance,String> {
}
