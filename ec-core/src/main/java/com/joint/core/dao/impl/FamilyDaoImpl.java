package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.FamilyDao;
import com.joint.core.dao.ManageDao;
import com.joint.core.entity.Family;
import com.joint.core.entity.Manage;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class FamilyDaoImpl extends BaseFlowDaoImpl<Family,String> implements FamilyDao {
}
