package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.DispatchRevise;

public interface DispatchReviseDao extends BaseFlowDao<DispatchRevise,String> {

}
