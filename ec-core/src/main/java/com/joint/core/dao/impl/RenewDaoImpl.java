package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.RenewDao;
import com.joint.core.entity.Renew;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class RenewDaoImpl extends BaseFlowDaoImpl<Renew,String> implements RenewDao {

}
