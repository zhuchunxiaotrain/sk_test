package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.Receive;

public interface ReceiveDao extends BaseFlowDao<Receive,String> {

}
