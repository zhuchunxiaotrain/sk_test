package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ReceiveDao;
import com.joint.core.entity.manage.Receive;
import org.springframework.stereotype.Repository;


@Repository
public class ReceiveDaoImpl extends BaseFlowDaoImpl<Receive,String> implements ReceiveDao {
}
