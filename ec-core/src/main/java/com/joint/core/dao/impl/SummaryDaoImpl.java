package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.SummaryDao;
import com.joint.core.entity.Summary;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class SummaryDaoImpl extends BaseFlowDaoImpl<Summary,String> implements SummaryDao {



}
