package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ActivityManagerDao;
import com.joint.core.dao.StrongBigDao;
import com.joint.core.entity.ActivityManager;
import com.joint.core.entity.StrongBig;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class ActivityManagerDaoImpl extends BaseFlowDaoImpl<ActivityManager, String > implements ActivityManagerDao{
}
