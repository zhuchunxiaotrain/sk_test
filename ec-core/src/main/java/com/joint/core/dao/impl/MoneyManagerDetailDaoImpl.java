package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.MoneyManagerDao;
import com.joint.core.dao.MoneyManagerDetailDao;
import com.joint.core.entity.MoneyManager;
import com.joint.core.entity.MoneyManagerDetail;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class MoneyManagerDetailDaoImpl extends BaseFlowDaoImpl<MoneyManagerDetail, String > implements MoneyManagerDetailDao {
}
