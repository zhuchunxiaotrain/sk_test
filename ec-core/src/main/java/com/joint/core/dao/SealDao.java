package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.Seal;

/**
 * Created by ZhuChunXiao on 2017/3/20.
 */
public interface SealDao extends BaseFlowDao<Seal,String> {
}
