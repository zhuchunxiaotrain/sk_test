package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ArchivesDao;
import com.joint.core.entity.manage.Archives;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@Repository
public class ArchivesDaoImpl extends BaseFlowDaoImpl<Archives,String> implements ArchivesDao {
}
