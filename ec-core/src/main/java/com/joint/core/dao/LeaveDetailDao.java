package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.LeaveDetail;
import com.joint.core.entity.manage.NoticeQuery;

import java.util.List;

public interface LeaveDetailDao extends BaseFlowDao<LeaveDetail,String> {

    public LeaveDetail findLeaveDetail(String userId, Integer time);
    public LeaveDetail findLeaveDetailByUsers(String userId);

}
