package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ScheduleDao;
import com.joint.core.entity.manage.Schedule;
import org.springframework.stereotype.Repository;


@Repository
public class SchedulesDaoImpl extends BaseFlowDaoImpl<Schedule,String> implements ScheduleDao {
}
