package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.TaskBack;

public interface TaskBackDao extends BaseFlowDao<TaskBack,String> {

}
