package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.TaskControlDao;
import com.joint.core.entity.manage.TaskControl;
import org.springframework.stereotype.Repository;


@Repository
public class TaskControlDaoImpl extends BaseFlowDaoImpl<TaskControl,String> implements TaskControlDao {
}
