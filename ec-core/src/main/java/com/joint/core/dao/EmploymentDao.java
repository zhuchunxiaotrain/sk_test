package com.joint.core.dao;


import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.Employment;

public interface EmploymentDao extends BaseFlowDao<Employment,String> {

}
