package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.AssetScrapDao;
import com.joint.core.entity.manage.AssetScrap;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/30.
 */
@Repository
public class AssetScrapDaoImpl extends BaseFlowDaoImpl<AssetScrap,String> implements AssetScrapDao {
}
