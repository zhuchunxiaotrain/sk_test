package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.QualificationsDictDao;
import com.joint.core.entity.manage.QualificationsDict;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
@Repository
public class QualificationsDictDaoImpl extends BaseFlowDaoImpl<QualificationsDict, String > implements QualificationsDictDao {
}
