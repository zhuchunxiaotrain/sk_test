package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ApproverDetailDao;
import com.joint.core.entity.ApproverDetail;
import org.springframework.stereotype.Repository;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Repository
public class ApproverDetailDaoImpl extends BaseFlowDaoImpl<ApproverDetail, String> implements ApproverDetailDao {
}
