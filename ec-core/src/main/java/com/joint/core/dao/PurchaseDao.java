package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.manage.Purchase;

/**
 * Created by ZhuChunXiao on 2017/3/27.
 */
public interface PurchaseDao extends BaseFlowDao<Purchase,String> {
}
