package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ManageProInfoDao;
import com.joint.core.entity.ManageProInfo;
import org.springframework.stereotype.Repository;

/**
 * Created by dqf on 2015/8/26.
 */
@Repository
public class ManageProInfoDaoImpl extends BaseFlowDaoImpl<ManageProInfo,String> implements ManageProInfoDao {
}
