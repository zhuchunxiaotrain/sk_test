package com.joint.core.dao.impl;

import com.fz.us.dict.entity.Dict;
import com.joint.base.bean.FlowEnum;
import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.base.entity.Users;
import com.joint.core.dao.CarDao;
import com.joint.core.entity.manage.Car;
import com.joint.core.entity.manage.CarConfig;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/21.
 */
@Repository
public class CarDaoImpl extends BaseFlowDaoImpl<Car,String> implements CarDao {
    @Override
    public int getMaxKilometre(Dict carNo) {
        int max=(int)getSession().createQuery("select max(c.kilometre) from Car c where c.carNo=?").setParameter(0,carNo).uniqueResult();
        return max;
    }

    @Override
    public List<Car> getUseInfo(Dict carNo) {
        List<Car> carList=getSession().createQuery("from Car c where c.carNo=?").setParameter(0,carNo).list();
        return carList;
    }

    @Override
    public boolean checkUse(CarConfig carConfig, Date dateStart,Users user) {
        List<Car> carList=getSession().createQuery("from Car c where c.carConfig=? and c.dateStart<=? and c.dateEnd>=? and c.step in ('1','2') and c.processState!=?")
                .setParameter(0, carConfig)
                .setParameter(1, dateStart)
                .setParameter(2, dateStart)
                .setParameter(3, FlowEnum.ProcessState.Draft)
                .list();
        if(carList!=null&&carList.size()>0){
            return false;
        }else{
            return true;
        }
    }
}
