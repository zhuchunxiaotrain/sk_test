package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ModifyYearDao;
import com.joint.core.entity.finance.ModifyYear;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/7.
 */
@Repository
public class ModifyYearDaoImpl extends BaseFlowDaoImpl<ModifyYear,String> implements ModifyYearDao {
    @Override
    public void updateCalFile(String keyId, String sql) {
        String now="delete from sk_calculationyear_budget where id='"+keyId+"'";
        System.out.println("最终的SQL是："+now);
        getSession().createSQLQuery(now).executeUpdate();
        getSession().createSQLQuery(sql).executeUpdate();
    }
}
