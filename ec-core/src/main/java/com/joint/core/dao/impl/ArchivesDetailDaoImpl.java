package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.ArchivesDetailDao;
import com.joint.core.entity.manage.ArchivesDetail;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@Repository
public class ArchivesDetailDaoImpl extends BaseFlowDaoImpl<ArchivesDetail,String> implements ArchivesDetailDao {
}
