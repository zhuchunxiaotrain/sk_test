package com.joint.core.dao;

import com.joint.base.dao.BaseFlowDao;
import com.joint.core.entity.finance.AuditFirst;

/**
 * Created by ZhuChunXiao on 2017/3/14.
 */
public interface AuditFirstDao extends BaseFlowDao<AuditFirst,String> {
}
