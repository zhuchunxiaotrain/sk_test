package com.joint.core.dao.impl;

import com.joint.base.dao.impl.BaseFlowDaoImpl;
import com.joint.core.dao.MeetingLeaveDao;
import com.joint.core.dao.MeetingSignDao;
import com.joint.core.entity.manage.MeetingLeave;
import com.joint.core.entity.manage.MeetingSign;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
@Repository
public class MeetingLeaveDaoImpl extends BaseFlowDaoImpl<MeetingLeave,String> implements MeetingLeaveDao {
    @Override
    public List<MeetingLeave> findDataByUsersParentId(String userId, String parentId) {
        Assert.notNull(userId, "userId is required");
        Assert.notNull(parentId, "parentId is required");
        String hql = "from MeetingLeave where creater.id = :userId and meetingUse.id = :parentId";
        List<MeetingLeave> meetingLeaveList = getSession().createQuery(hql).setParameter("userId", userId)
                .setParameter("parentId", parentId).list();
        return meetingLeaveList;

    }
}
