package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ChangeDao;
import com.joint.core.entity.Change;
import com.joint.core.service.ChangeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class ChangeServiceImpl extends BaseFlowServiceImpl<Change,String> implements ChangeService {
    @Resource
    ChangeDao changeDao;

    @Override
    public BaseFlowDao<Change, String> getBaseFlowDao() {
        return changeDao;
    }
}
