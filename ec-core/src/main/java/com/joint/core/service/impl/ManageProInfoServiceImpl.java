package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ManageProInfoDao;
import com.joint.core.entity.ManageProInfo;
import com.joint.core.service.ManageProInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class ManageProInfoServiceImpl extends BaseFlowServiceImpl<ManageProInfo,String> implements ManageProInfoService {
    @Resource
    ManageProInfoDao manageProInfoDao;

    @Override
    public BaseFlowDao<ManageProInfo, String> getBaseFlowDao() {
        return manageProInfoDao;
    }
}
