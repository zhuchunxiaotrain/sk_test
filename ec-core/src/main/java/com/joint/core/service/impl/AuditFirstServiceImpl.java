package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.AuditFirstDao;
import com.joint.core.entity.finance.AuditFirst;
import com.joint.core.service.AuditFirstService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/14.
 */
@Service
public class AuditFirstServiceImpl extends BaseFlowServiceImpl<AuditFirst,String> implements AuditFirstService {
    @Resource
    AuditFirstDao auditFirstDao;

    @Override
    public BaseFlowDao<AuditFirst, String> getBaseFlowDao() {
        return auditFirstDao;
    }
}
