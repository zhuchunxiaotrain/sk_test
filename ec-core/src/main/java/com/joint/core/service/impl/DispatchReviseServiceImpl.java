package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.DispatchReviseDao;
import com.joint.core.entity.manage.DispatchRevise;
import com.joint.core.service.DispatchReviseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class DispatchReviseServiceImpl extends BaseFlowServiceImpl<DispatchRevise,String> implements DispatchReviseService {
    @Resource
    DispatchReviseDao dispatchReviseDao;

    @Override
    public BaseFlowDao<DispatchRevise, String> getBaseFlowDao() {
        return dispatchReviseDao;
    }
}
