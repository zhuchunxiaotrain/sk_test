package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.RebateActivityDetailDao;
import com.joint.core.dao.StrongBigDao;
import com.joint.core.entity.RebateActivityDetail;
import com.joint.core.entity.StrongBig;
import com.joint.core.service.RebateActivityDetailService;
import com.joint.core.service.StrongBigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class RebateActivityDetailServiceImpl extends BaseFlowServiceImpl<RebateActivityDetail, String> implements RebateActivityDetailService {
    @Resource
    RebateActivityDetailDao rebateActivityDetailDao;

    @Override
    public BaseFlowDao<RebateActivityDetail, String> getBaseFlowDao() {
        return rebateActivityDetailDao;
    }
}
