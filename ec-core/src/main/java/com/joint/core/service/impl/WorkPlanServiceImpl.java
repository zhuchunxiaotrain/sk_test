package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.WorkPlanDao;
import com.joint.core.entity.finance.WorkPlan;
import com.joint.core.service.WorkPlanService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/10.
 */
@Service
public class WorkPlanServiceImpl extends BaseFlowServiceImpl<WorkPlan,String> implements WorkPlanService {
    @Resource
    WorkPlanDao workPlanDao;

    @Override
    public BaseFlowDao<WorkPlan, String> getBaseFlowDao() {
        return workPlanDao;
    }
}
