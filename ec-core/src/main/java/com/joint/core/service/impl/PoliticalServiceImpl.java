package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.PoliticalDao;
import com.joint.core.entity.Political;
import com.joint.core.service.PoliticalService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class PoliticalServiceImpl extends BaseFlowServiceImpl<Political,String> implements PoliticalService {
    @Resource
    PoliticalDao politicalDao;

    @Override
    public BaseFlowDao<Political, String> getBaseFlowDao() {
        return politicalDao;
    }

}
