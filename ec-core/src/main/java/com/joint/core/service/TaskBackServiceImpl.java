package com.joint.core.service;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.TaskBackDao;
import com.joint.core.entity.manage.TaskBack;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;


@Service
public class TaskBackServiceImpl extends BaseFlowServiceImpl<TaskBack,String> implements TaskBackService {
    @Resource
    TaskBackDao taskBackDao;

    @Override
    public BaseFlowDao<TaskBack, String> getBaseFlowDao() {
        return taskBackDao;
    }
}
