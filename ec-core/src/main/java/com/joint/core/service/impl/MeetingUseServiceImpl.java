package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.MeetingUseDao;
import com.joint.core.entity.manage.MeetingUse;
import com.joint.core.service.MeetingUseService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
@Service
public class MeetingUseServiceImpl extends BaseFlowServiceImpl<MeetingUse,String> implements MeetingUseService {
    @Resource
    MeetingUseDao meetingUseDao;

    @Override
    public BaseFlowDao<MeetingUse, String> getBaseFlowDao() {
        return meetingUseDao;
    }

}
