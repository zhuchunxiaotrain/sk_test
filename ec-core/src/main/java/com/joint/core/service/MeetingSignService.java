package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.MeetingRecord;
import com.joint.core.entity.manage.MeetingSign;

import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
public interface MeetingSignService extends BaseFlowService<MeetingSign,String> {

    public List<MeetingSign> findDataByUsers(String userId);

    List<MeetingSign> findDataByUsersParentId(String userId, String parentId);
}
