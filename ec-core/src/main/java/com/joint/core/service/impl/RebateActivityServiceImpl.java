package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.AnnualBudgetDao;
import com.joint.core.dao.RebateActivityDao;
import com.joint.core.entity.AnnualBudget;
import com.joint.core.entity.RebateActivity;
import com.joint.core.service.AnnualBudgetService;
import com.joint.core.service.RebateActivityService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class RebateActivityServiceImpl extends BaseFlowServiceImpl<RebateActivity, String> implements RebateActivityService{
    @Resource
    RebateActivityDao rebateActivityDao;

    @Override
    public BaseFlowDao<RebateActivity, String> getBaseFlowDao() {
        return rebateActivityDao;
    }
}
