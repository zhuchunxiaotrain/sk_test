package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.NoticeDao;
import com.joint.core.entity.manage.Notice;
import com.joint.core.service.NoticeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class NoticeServiceImpl extends BaseFlowServiceImpl<Notice,String> implements NoticeService {
    @Resource
    NoticeDao noticeDao;

    @Override
    public BaseFlowDao<Notice, String> getBaseFlowDao() {
        return noticeDao;
    }
}
