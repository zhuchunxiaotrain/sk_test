package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.WorkLogDao;
import com.joint.core.entity.manage.WorkLog;
import com.joint.core.service.WorkLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/21.
 */
@Service
public class WorkLogServiceImpl extends BaseFlowServiceImpl<WorkLog,String> implements WorkLogService {
    @Resource
    WorkLogDao workLogDao;

    @Override
    public BaseFlowDao<WorkLog, String> getBaseFlowDao() {
        return workLogDao;
    }
}
