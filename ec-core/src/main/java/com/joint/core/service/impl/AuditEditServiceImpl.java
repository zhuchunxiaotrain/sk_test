package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.AuditEditDao;
import com.joint.core.entity.finance.AuditEdit;
import com.joint.core.service.AuditEditService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/15.
 */
@Service
public class AuditEditServiceImpl extends BaseFlowServiceImpl<AuditEdit,String> implements AuditEditService {
    @Resource
    AuditEditDao auditEditDao;

    @Override
    public BaseFlowDao<AuditEdit, String> getBaseFlowDao() {
        return auditEditDao;
    }
}
