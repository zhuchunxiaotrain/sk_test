package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.MeetingRecordDao;
import com.joint.core.dao.MeetingSignDao;
import com.joint.core.entity.manage.MeetingRecord;
import com.joint.core.entity.manage.MeetingSign;
import com.joint.core.service.MeetingRecordService;
import com.joint.core.service.MeetingSignService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
@Service
public class MeetingSignServiceImpl extends BaseFlowServiceImpl<MeetingSign,String> implements MeetingSignService {
    @Resource
    MeetingSignDao meetingSignDao;

    @Override
    public BaseFlowDao<MeetingSign, String> getBaseFlowDao() {
        return meetingSignDao;
    }

    @Override
    public List<MeetingSign> findDataByUsers(String userId) {
        return meetingSignDao.findDataByUsers(userId);
    }

    @Override
    public List<MeetingSign> findDataByUsersParentId(String userId, String parentId) {
        return meetingSignDao.findDataByUsersParentId(userId, parentId);
    }
}
