package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.CarConfigDao;
import com.joint.core.entity.manage.CarConfig;
import com.joint.core.service.CarConfigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
@Service
public class CarConfigServiceImpl extends BaseFlowServiceImpl<CarConfig,String> implements CarConfigService {
    @Resource
    CarConfigDao carConfigDao;

    @Override
    public BaseFlowDao<CarConfig, String> getBaseFlowDao() {
        return carConfigDao;
    }
}
