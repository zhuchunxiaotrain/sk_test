package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.Interview;
import com.joint.core.entity.Manage;

import java.util.Map;
import java.util.Objects;

/**
 * Created by dqf on 2015/8/26.
 */
public interface InterviewService extends BaseFlowService<Interview,String> {

}
