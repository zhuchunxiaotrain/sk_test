package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.TaskControlDao;
import com.joint.core.entity.manage.TaskControl;
import com.joint.core.service.TaskControlService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;


@Service
public class TaskControlServiceImpl extends BaseFlowServiceImpl<TaskControl,String> implements TaskControlService {
    @Resource
    TaskControlDao taskControlDao;

    @Override
    public BaseFlowDao<TaskControl, String> getBaseFlowDao() {
        return taskControlDao;
    }
}
