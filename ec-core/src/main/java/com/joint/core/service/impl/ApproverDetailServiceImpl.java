package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ApproverDetailDao;
import com.joint.core.entity.ApproverDetail;
import com.joint.core.service.ApproverDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class ApproverDetailServiceImpl extends BaseFlowServiceImpl<ApproverDetail, String> implements ApproverDetailService {
    @Resource
    ApproverDetailDao approverDetailDao;

    @Override
    public BaseFlowDao<ApproverDetail, String> getBaseFlowDao() {
        return approverDetailDao;
    }
}
