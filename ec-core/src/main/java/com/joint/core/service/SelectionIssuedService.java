package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.SelectionIssued;
import com.joint.core.entity.SelectionPublicity;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface SelectionIssuedService extends BaseFlowService<SelectionIssued,String > {
}
