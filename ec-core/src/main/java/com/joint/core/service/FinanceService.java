package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.finance.Finance;

/**
 * Created by ZhuChunXiao on 2017/3/2.
 */
public interface FinanceService extends BaseFlowService<Finance,String> {
}
