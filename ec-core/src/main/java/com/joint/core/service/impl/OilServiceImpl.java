package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.OilDao;
import com.joint.core.entity.manage.Oil;
import com.joint.core.service.OilService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
@Service
public class OilServiceImpl extends BaseFlowServiceImpl<Oil,String> implements OilService {
    @Resource
    OilDao oilDao;

    @Override
    public BaseFlowDao<Oil, String> getBaseFlowDao() {
        return oilDao;
    }
}
