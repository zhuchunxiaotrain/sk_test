package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ModifyYearDao;
import com.joint.core.entity.finance.ModifyYear;
import com.joint.core.service.ModifyYearService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/7.
 */
@Service
public class ModifyYearServiceImpl extends BaseFlowServiceImpl<ModifyYear,String> implements ModifyYearService {
    @Resource
    ModifyYearDao modifyYearDao;

    @Override
    public BaseFlowDao<ModifyYear, String> getBaseFlowDao() {
        return modifyYearDao;
    }

    @Override
    public void updateCalFile(String id, String sql) {
        modifyYearDao.updateCalFile(id,sql);
    }
}
