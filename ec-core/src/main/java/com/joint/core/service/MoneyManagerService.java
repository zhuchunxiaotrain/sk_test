package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.MoneyManager;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface MoneyManagerService extends BaseFlowService<MoneyManager,String > {
}
