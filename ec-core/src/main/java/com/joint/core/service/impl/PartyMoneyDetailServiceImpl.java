package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.PartyMoneyDao;
import com.joint.core.dao.PartyMoneyDetailDao;
import com.joint.core.entity.PartyMoney;
import com.joint.core.entity.PartyMoneyDetail;
import com.joint.core.service.PartyMoneyDetailService;
import com.joint.core.service.PartyMoneyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class PartyMoneyDetailServiceImpl extends BaseFlowServiceImpl<PartyMoneyDetail,String> implements PartyMoneyDetailService {
    @Resource
    PartyMoneyDetailDao partyMoneyDetailDao;

    @Override
    public BaseFlowDao<PartyMoneyDetail, String> getBaseFlowDao() {
        return partyMoneyDetailDao;
    }
}
