package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.DispatchDao;
import com.joint.core.entity.manage.Dispatch;
import com.joint.core.service.DispatchService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;


@Service
public class DispatchServiceImpl extends BaseFlowServiceImpl<Dispatch,String> implements DispatchService {
    @Resource
    DispatchDao dispatchDao;

    @Override
    public BaseFlowDao<Dispatch, String> getBaseFlowDao() {
        return dispatchDao;
    }
}
