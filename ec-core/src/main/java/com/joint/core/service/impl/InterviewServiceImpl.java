package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.InterviewDao;
import com.joint.core.dao.ManageDao;
import com.joint.core.entity.Interview;
import com.joint.core.entity.Manage;
import com.joint.core.service.InterviewService;
import com.joint.core.service.ManageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class InterviewServiceImpl extends BaseFlowServiceImpl<Interview,String> implements InterviewService {
    @Resource
    InterviewDao interviewDao;

    @Override
    public BaseFlowDao<Interview, String> getBaseFlowDao() {
        return interviewDao;
    }
}
