package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.WorkLog;

/**
 * Created by ZhuChunXiao on 2017/3/21.
 */
public interface WorkLogService extends BaseFlowService<WorkLog,String> {
}
