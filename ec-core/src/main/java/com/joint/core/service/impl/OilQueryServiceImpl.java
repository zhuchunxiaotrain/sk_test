package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.OilQueryDao;
import com.joint.core.entity.manage.CarConfig;
import com.joint.core.entity.manage.Oil;
import com.joint.core.entity.manage.OilQuery;
import com.joint.core.service.OilQueryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
@Service
public class OilQueryServiceImpl extends BaseFlowServiceImpl<OilQuery,String> implements OilQueryService {
    @Resource
    OilQueryDao oilQueryDao;


    @Override
    public BaseFlowDao<OilQuery, String> getBaseFlowDao() {
        return oilQueryDao;
    }

    @Override
    public Double getMoneyNum(CarConfig carConfig) {
        return oilQueryDao.getMoneyNum(carConfig);
    }
}
