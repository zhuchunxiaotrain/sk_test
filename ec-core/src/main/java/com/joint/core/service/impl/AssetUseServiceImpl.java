package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.AssetUseDao;
import com.joint.core.entity.manage.AssetUse;
import com.joint.core.service.AssetUseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/28.
 */
@Service
public class AssetUseServiceImpl extends BaseFlowServiceImpl<AssetUse,String> implements AssetUseService {
    @Resource
    AssetUseDao assetUseDao;

    @Override
    public BaseFlowDao<AssetUse, String> getBaseFlowDao() {
        return assetUseDao;
    }
}
