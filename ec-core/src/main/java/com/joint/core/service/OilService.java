package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Oil;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
public interface OilService extends BaseFlowService<Oil,String> {
}
