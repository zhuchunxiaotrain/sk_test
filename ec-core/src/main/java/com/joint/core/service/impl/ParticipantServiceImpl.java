package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ParticipantDao;
import com.joint.core.dao.TrainDao;
import com.joint.core.entity.Participant;
import com.joint.core.entity.Train;
import com.joint.core.service.ParticipantService;
import com.joint.core.service.TrainService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class ParticipantServiceImpl extends BaseFlowServiceImpl<Participant,String> implements ParticipantService {
    @Resource
    ParticipantDao participantDao;

    @Override
    public BaseFlowDao<Participant, String> getBaseFlowDao() {
        return participantDao;
    }
}


