package com.joint.core.service.impl;

import com.joint.base.dao.BaseEntityDao;
import com.joint.base.entity.Company;
import com.joint.base.service.impl.BaseEntityServiceImpl;
import com.joint.core.dao.NumberCreaterDao;
import com.joint.core.entity.NumberCreater;
import com.joint.core.service.NumberCreaterService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/25.
 */
@Service
public class NumberCreaterServiceImpl extends BaseEntityServiceImpl<NumberCreater,String> implements NumberCreaterService {
    @Resource
    NumberCreaterDao numberCreaterDao;

    @Override
    public BaseEntityDao<NumberCreater, String> getBaseEntityDao() {
        return numberCreaterDao;
    }

    @Override
    public String getNumber(String entityName, String header, int length, Company company) {
        return numberCreaterDao.getNumber(entityName,header,length, company);
    }
}
