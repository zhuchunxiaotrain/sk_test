package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Contract;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
public interface ContractService extends BaseFlowService<Contract,String> {
}
