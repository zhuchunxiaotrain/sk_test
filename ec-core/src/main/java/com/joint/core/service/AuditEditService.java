package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.finance.AuditEdit;

/**
 * Created by ZhuChunXiao on 2017/3/15.
 */
public interface AuditEditService extends BaseFlowService<AuditEdit,String> {
}
