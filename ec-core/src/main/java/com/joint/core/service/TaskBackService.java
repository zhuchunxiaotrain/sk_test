package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.TaskBack;


public interface TaskBackService extends BaseFlowService<TaskBack,String> {
}
