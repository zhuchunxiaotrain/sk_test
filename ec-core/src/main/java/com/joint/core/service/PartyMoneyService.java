package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.PartyMoney;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface PartyMoneyService extends BaseFlowService<PartyMoney,String > {
}
