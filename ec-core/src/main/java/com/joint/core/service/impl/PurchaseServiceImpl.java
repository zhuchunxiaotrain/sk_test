package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.PurchaseDao;
import com.joint.core.entity.manage.Purchase;
import com.joint.core.service.PurchaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/27.
 */
@Service
public class PurchaseServiceImpl extends BaseFlowServiceImpl<Purchase, String> implements PurchaseService {
    @Resource
    PurchaseDao purchaseDao;

    @Override
    public BaseFlowDao<Purchase, String> getBaseFlowDao() {
        return purchaseDao;
    }
}
