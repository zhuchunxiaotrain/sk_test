package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.MeetingRecord;
import com.joint.core.entity.manage.MeetingUse;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
public interface MeetingRecordService extends BaseFlowService<MeetingRecord,String> {
}
