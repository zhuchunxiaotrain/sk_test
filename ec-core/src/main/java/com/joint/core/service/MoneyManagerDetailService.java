package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.MoneyManager;
import com.joint.core.entity.MoneyManagerDetail;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface MoneyManagerDetailService extends BaseFlowService<MoneyManagerDetail,String > {
}
