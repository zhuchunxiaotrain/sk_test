package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.finance.FinancialAnalysis;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
public interface FinancialAnalysisService extends BaseFlowService<FinancialAnalysis,String> {
}
