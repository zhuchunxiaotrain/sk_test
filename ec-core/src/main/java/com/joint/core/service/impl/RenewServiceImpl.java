package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.RenewDao;
import com.joint.core.entity.Renew;
import com.joint.core.service.RenewService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class RenewServiceImpl extends BaseFlowServiceImpl<Renew,String> implements RenewService {
    @Resource
    RenewDao renewDao;

    @Override
    public BaseFlowDao<Renew, String> getBaseFlowDao() {
        return renewDao;
    }

}
