package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.UseDetail;

/**
 * Created by ZhuChunXiao on 2017/3/29.
 */
public interface UseDetailService extends BaseFlowService<UseDetail,String> {
}
