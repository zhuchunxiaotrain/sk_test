package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.AnnualBudgetDao;
import com.joint.core.dao.AnnualBudgetDetailDao;
import com.joint.core.entity.AnnualBudget;
import com.joint.core.entity.AnnualBudgetDetail;
import com.joint.core.service.AnnualBudgetDetailService;
import com.joint.core.service.AnnualBudgetService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class AnnualBudgetDetailServiceImpl extends BaseFlowServiceImpl<AnnualBudgetDetail, String> implements AnnualBudgetDetailService{
    @Resource
    AnnualBudgetDetailDao annualBudgetDetailDao;

    @Override
    public BaseFlowDao<AnnualBudgetDetail, String> getBaseFlowDao() {
        return annualBudgetDetailDao;
    }
}
