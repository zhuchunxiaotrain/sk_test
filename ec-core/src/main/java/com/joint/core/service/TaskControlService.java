package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.TaskControl;


public interface TaskControlService extends BaseFlowService<TaskControl,String> {
}
