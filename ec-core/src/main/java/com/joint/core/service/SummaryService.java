package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.Summary;
import com.joint.core.entity.Train;

/**
 * Created by dqf on 2015/8/26.
 */
public interface SummaryService extends BaseFlowService<Summary,String> {

}
