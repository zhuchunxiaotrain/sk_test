package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.Train;
import com.joint.core.entity.TrainRecord;

/**
 * Created by dqf on 2015/8/26.
 */
public interface TrainRecordService extends BaseFlowService<TrainRecord,String> {

}
