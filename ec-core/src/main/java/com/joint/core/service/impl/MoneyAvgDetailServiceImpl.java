package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.MoneyAvgDao;
import com.joint.core.dao.MoneyAvgDetailDao;
import com.joint.core.entity.MoneyAvg;
import com.joint.core.entity.MoneyAvgDetail;
import com.joint.core.service.MoneyAvgDetailService;
import com.joint.core.service.MoneyAvgService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class MoneyAvgDetailServiceImpl extends BaseFlowServiceImpl<MoneyAvgDetail, String> implements MoneyAvgDetailService {
    @Resource
    MoneyAvgDetailDao moneyAvgDetailDao;

    @Override
    public BaseFlowDao<MoneyAvgDetail, String> getBaseFlowDao() {
        return moneyAvgDetailDao;
    }
}
