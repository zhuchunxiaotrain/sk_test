package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.PartyMoneyDao;
import com.joint.core.entity.PartyMoney;
import com.joint.core.service.PartyMoneyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class PartyMoneyServiceImpl extends BaseFlowServiceImpl<PartyMoney,String> implements PartyMoneyService {
    @Resource
    PartyMoneyDao partyMoneyDao;

    @Override
    public BaseFlowDao<PartyMoney, String> getBaseFlowDao() {
        return partyMoneyDao;
    }
}
