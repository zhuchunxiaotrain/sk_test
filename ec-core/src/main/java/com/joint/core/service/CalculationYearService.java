package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.finance.CalculationYear;

/**
 * Created by ZhuChunXiao on 2017/3/6.
 */
public interface CalculationYearService extends BaseFlowService<CalculationYear,String> {
}
