package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.CarConfig;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
public interface CarConfigService extends BaseFlowService<CarConfig,String> {
}
