package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.Employees;
import com.joint.core.entity.Employment;

import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
public interface EmployeesService extends BaseFlowService<Employees,String> {

    /**
     * 通过是否为党员获取实体
     */
    public List<Employees> findEmployees(String partyMember);
}
