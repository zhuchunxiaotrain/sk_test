package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.SelectionPublicity;
import com.joint.core.entity.SelectionSpeech;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface SelectionPublicityService extends BaseFlowService<SelectionPublicity,String > {
}
