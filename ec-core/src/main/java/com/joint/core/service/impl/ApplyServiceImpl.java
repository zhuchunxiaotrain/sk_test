package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ApplyDao;
import com.joint.core.entity.Apply;
import com.joint.core.service.ApplyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class ApplyServiceImpl extends BaseFlowServiceImpl<Apply,String> implements ApplyService {
    @Resource
    ApplyDao applyDao;

    @Override
    public BaseFlowDao<Apply, String> getBaseFlowDao() {
        return applyDao;
    }

}
