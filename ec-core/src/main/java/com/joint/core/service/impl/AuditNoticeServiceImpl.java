package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.AuditNoticeDao;
import com.joint.core.entity.finance.AuditNotice;
import com.joint.core.service.AuditNoticeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/13.
 */
@Service
public class AuditNoticeServiceImpl extends BaseFlowServiceImpl<AuditNotice,String> implements AuditNoticeService {
    @Resource
    AuditNoticeDao auditNoticeDao;

    @Override
    public BaseFlowDao<AuditNotice, String> getBaseFlowDao() {
        return auditNoticeDao;
    }
}
