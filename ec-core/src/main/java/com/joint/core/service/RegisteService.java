package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Notice;
import com.joint.core.entity.manage.Registe;

/**
 * Created by dqf on 2015/8/26.
 */
public interface RegisteService extends BaseFlowService<Registe,String> {
}
