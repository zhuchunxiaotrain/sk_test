package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.ArchivesDetail;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
public interface ArchivesDetailService extends BaseFlowService<ArchivesDetail,String> {
}
