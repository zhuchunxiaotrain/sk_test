package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ContractDao;
import com.joint.core.entity.manage.Contract;
import com.joint.core.service.ContractService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@Service
public class ContractServiceImpl extends BaseFlowServiceImpl<Contract,String> implements ContractService {
    @Resource
    ContractDao contractDao;

    @Override
    public BaseFlowDao<Contract, String> getBaseFlowDao() {
        return contractDao;
    }
}
