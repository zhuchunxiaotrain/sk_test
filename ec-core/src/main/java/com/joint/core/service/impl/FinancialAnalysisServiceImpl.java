package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.FinancialAnalysisDao;
import com.joint.core.entity.finance.FinancialAnalysis;
import com.joint.core.service.FinancialAnalysisService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
@Service
public class FinancialAnalysisServiceImpl extends BaseFlowServiceImpl<FinancialAnalysis,String> implements FinancialAnalysisService {
    @Resource
    FinancialAnalysisDao financialAnalysisDao;

    @Override
    public BaseFlowDao<FinancialAnalysis, String> getBaseFlowDao() {
        return financialAnalysisDao;
    }
}
