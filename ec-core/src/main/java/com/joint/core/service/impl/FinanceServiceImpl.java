package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.FinanceDao;
import com.joint.core.entity.finance.Finance;
import com.joint.core.service.FinanceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/2.
 */
@Service
public class FinanceServiceImpl extends BaseFlowServiceImpl<Finance,String> implements FinanceService {
    @Resource
    FinanceDao financeDao;

    @Override
    public BaseFlowDao<Finance, String> getBaseFlowDao() {
        return financeDao;
    }
}
