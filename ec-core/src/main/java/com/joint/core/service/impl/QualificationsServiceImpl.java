package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.QualificationsDao;
import com.joint.core.entity.manage.Qualifications;
import com.joint.core.service.QualificationsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/20.
 */
@Service
public class QualificationsServiceImpl extends BaseFlowServiceImpl<Qualifications, String> implements QualificationsService {
    @Resource
    QualificationsDao qualificationsDao;

    @Override
    public BaseFlowDao<Qualifications, String> getBaseFlowDao() {
        return qualificationsDao;
    }
}
