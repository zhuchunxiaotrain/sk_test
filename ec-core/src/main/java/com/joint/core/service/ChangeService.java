package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.Change;
import com.joint.core.entity.Manage;

/**
 * Created by dqf on 2015/8/26.
 */
public interface ChangeService extends BaseFlowService<Change,String> {
}
