package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ExpendDao;
import com.joint.core.entity.finance.Expend;
import com.joint.core.service.ExpendService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/4/1.
 */
@Service
public class ExpendServiceImpl extends BaseFlowServiceImpl<Expend,String> implements ExpendService {
    @Resource
    ExpendDao expendDao;

    @Override
    public BaseFlowDao<Expend, String> getBaseFlowDao() {
        return expendDao;
    }
}
