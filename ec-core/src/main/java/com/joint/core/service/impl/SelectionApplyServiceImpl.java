package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.SelectionApplyDao;
import com.joint.core.dao.SelectionNoticeDao;
import com.joint.core.entity.SelectionApply;
import com.joint.core.entity.SelectionNotice;
import com.joint.core.service.SelectionApplyService;
import com.joint.core.service.SelectionNoticeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class SelectionApplyServiceImpl extends BaseFlowServiceImpl<SelectionApply, String> implements SelectionApplyService{
    @Resource
    SelectionApplyDao selectionApplyDao;


    @Override
    public BaseFlowDao<SelectionApply, String> getBaseFlowDao() {
        return selectionApplyDao;
    }
}
