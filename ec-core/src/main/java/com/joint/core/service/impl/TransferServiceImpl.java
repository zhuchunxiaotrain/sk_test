package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.TransferDao;
import com.joint.core.entity.Change;
import com.joint.core.service.TransferService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class TransferServiceImpl extends BaseFlowServiceImpl<Change,String> implements TransferService {
    @Resource
    TransferDao transferDao;

    @Override
    public BaseFlowDao<Change, String> getBaseFlowDao() {
        return transferDao;
    }
}
