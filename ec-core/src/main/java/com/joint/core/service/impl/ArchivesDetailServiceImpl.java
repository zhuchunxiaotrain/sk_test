package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ArchivesDetailDao;
import com.joint.core.entity.manage.ArchivesDetail;
import com.joint.core.service.ArchivesDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@Service
public class ArchivesDetailServiceImpl extends BaseFlowServiceImpl<ArchivesDetail,String> implements ArchivesDetailService {
    @Resource
    ArchivesDetailDao archivesDetailDao;

    @Override
    public BaseFlowDao<ArchivesDetail, String> getBaseFlowDao() {
        return archivesDetailDao;
    }
}
