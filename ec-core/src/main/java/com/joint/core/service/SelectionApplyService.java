package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.SelectionApply;
import com.joint.core.entity.SelectionNotice;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface SelectionApplyService extends BaseFlowService<SelectionApply,String > {
}
