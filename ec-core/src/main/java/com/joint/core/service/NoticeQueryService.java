package com.joint.core.service;

import com.joint.base.service.BaseEntityService;
import com.joint.core.entity.manage.NoticeQuery;

import java.util.List;


public interface NoticeQueryService extends BaseEntityService<NoticeQuery,String> {
    public List<NoticeQuery> findUsersByNoticeQuery(String userId);

    public List<NoticeQuery> findUsersByNoticeQuery(String userId, String keyId);
}
