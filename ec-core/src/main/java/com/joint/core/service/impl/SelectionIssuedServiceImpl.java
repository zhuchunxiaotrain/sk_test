package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.SelectionIssuedDao;
import com.joint.core.dao.SelectionPublicityDao;
import com.joint.core.entity.SelectionIssued;
import com.joint.core.entity.SelectionPublicity;
import com.joint.core.service.SelectionIssuedService;
import com.joint.core.service.SelectionPublicityService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class SelectionIssuedServiceImpl extends BaseFlowServiceImpl<SelectionIssued, String> implements SelectionIssuedService {
    @Resource
    SelectionIssuedDao selectionIssuedDao;


    @Override
    public BaseFlowDao<SelectionIssued, String> getBaseFlowDao() {
        return selectionIssuedDao;
    }
}
