package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.MeetingRecordDao;
import com.joint.core.dao.MeetingUseDao;
import com.joint.core.entity.manage.MeetingRecord;
import com.joint.core.entity.manage.MeetingUse;
import com.joint.core.service.MeetingRecordService;
import com.joint.core.service.MeetingUseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
@Service
public class MeetingRecordServiceImpl extends BaseFlowServiceImpl<MeetingRecord,String> implements MeetingRecordService {
    @Resource
    MeetingRecordDao meetingRecordDao;

    @Override
    public BaseFlowDao<MeetingRecord, String> getBaseFlowDao() {
        return meetingRecordDao;
    }

}
