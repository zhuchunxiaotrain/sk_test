package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ActivityManagerDao;
import com.joint.core.dao.StrongBigDao;
import com.joint.core.entity.ActivityManager;
import com.joint.core.entity.StrongBig;
import com.joint.core.service.ActivityManagerService;
import com.joint.core.service.StrongBigService;
import org.activiti.bpmn.model.Activity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class ActivityManagerServiceImpl extends BaseFlowServiceImpl<ActivityManager, String> implements ActivityManagerService{
    @Resource
    ActivityManagerDao activityManagerDao;

    @Override
    public BaseFlowDao<ActivityManager, String> getBaseFlowDao() {
        return activityManagerDao;
    }
}
