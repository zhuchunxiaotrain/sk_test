package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.QualificationsDictDao;
import com.joint.core.entity.manage.QualificationsDict;
import com.joint.core.service.QualificationsDictService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
@Service
public class QualificationsDictServiceImpl extends BaseFlowServiceImpl<QualificationsDict, String> implements QualificationsDictService {
    @Resource
    QualificationsDictDao qualificationsDictDao;

    @Override
    public BaseFlowDao<QualificationsDict, String> getBaseFlowDao() {
        return qualificationsDictDao;
    }
}
