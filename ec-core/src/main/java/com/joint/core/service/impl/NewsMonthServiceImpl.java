package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.NewsMonthDao;
import com.joint.core.entity.finance.NewsMonth;
import com.joint.core.service.NewsMonthService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
@Service
public class NewsMonthServiceImpl extends BaseFlowServiceImpl<NewsMonth,String> implements NewsMonthService {
    @Resource
    NewsMonthDao newsMonthDao;

    @Override
    public BaseFlowDao<NewsMonth, String> getBaseFlowDao() {
        return newsMonthDao;
    }
}
