package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.AssetScrapDao;
import com.joint.core.entity.manage.AssetScrap;
import com.joint.core.service.AssetScrapService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/30.
 */
@Service
public class AssetScrapServiceImpl extends BaseFlowServiceImpl<AssetScrap,String> implements AssetScrapService {
    @Resource
    AssetScrapDao assetScrapDao;

    @Override
    public BaseFlowDao<AssetScrap, String> getBaseFlowDao() {
        return assetScrapDao;
    }
}
