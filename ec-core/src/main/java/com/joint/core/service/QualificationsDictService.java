package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.QualificationsDict;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
public interface QualificationsDictService extends BaseFlowService<QualificationsDict,String > {
}
