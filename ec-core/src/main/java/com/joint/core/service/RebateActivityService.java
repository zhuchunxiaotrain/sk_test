package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.AnnualBudget;
import com.joint.core.entity.RebateActivity;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface RebateActivityService extends BaseFlowService<RebateActivity,String > {
}
