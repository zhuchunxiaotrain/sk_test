package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.MoneyManagerDao;
import com.joint.core.entity.MoneyManager;
import com.joint.core.service.MoneyManagerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class MoneyManagerServiceImpl extends BaseFlowServiceImpl<MoneyManager, String> implements MoneyManagerService {
    @Resource
    MoneyManagerDao MoneyManagerDao;

    @Override
    public BaseFlowDao<MoneyManager, String> getBaseFlowDao() {
        return MoneyManagerDao;
    }
}
