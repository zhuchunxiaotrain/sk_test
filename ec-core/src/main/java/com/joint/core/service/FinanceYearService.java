package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.finance.FinanceYear;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
public interface FinanceYearService extends BaseFlowService<FinanceYear,String> {
}
