package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.LetterRegisterDao;
import com.joint.core.dao.StrongBigDao;
import com.joint.core.entity.LetterRegister;
import com.joint.core.entity.StrongBig;
import com.joint.core.service.LetterRegisterService;
import com.joint.core.service.StrongBigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class LetterRegisterServiceImpl extends BaseFlowServiceImpl<LetterRegister, String> implements LetterRegisterService {
    @Resource
    LetterRegisterDao LetterRegisterDao;

    @Override
    public BaseFlowDao<LetterRegister, String> getBaseFlowDao() {
        return LetterRegisterDao;
    }
}
