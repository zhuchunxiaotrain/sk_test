package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.SelectionPublicityDao;
import com.joint.core.dao.SelectionSpeechDao;
import com.joint.core.entity.SelectionPublicity;
import com.joint.core.entity.SelectionSpeech;
import com.joint.core.service.SelectionPublicityService;
import com.joint.core.service.SelectionSpeechService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class SelectionPublicityServiceImpl extends BaseFlowServiceImpl<SelectionPublicity, String> implements SelectionPublicityService{
    @Resource
    SelectionPublicityDao selectionPublicityDao;


    @Override
    public BaseFlowDao<SelectionPublicity, String> getBaseFlowDao() {
        return selectionPublicityDao;
    }
}
