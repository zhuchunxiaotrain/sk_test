package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.LeaveDetailDao;
import com.joint.core.entity.LeaveDetail;
import com.joint.core.service.LeaveDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class LeaveDetailServiceImpl extends BaseFlowServiceImpl<LeaveDetail,String> implements LeaveDetailService {
    @Resource
    LeaveDetailDao leaveDetailDao;

    @Override
    public BaseFlowDao<LeaveDetail, String> getBaseFlowDao() {
        return leaveDetailDao;
    }

    @Override
    public LeaveDetail findLeaveDetail(String userId, Integer time) {
        return leaveDetailDao.findLeaveDetail(userId,time);
    }

    @Override
    public LeaveDetail findLeaveDetailByusers(String userId) {
        return leaveDetailDao.findLeaveDetailByUsers(userId);
    }
}


