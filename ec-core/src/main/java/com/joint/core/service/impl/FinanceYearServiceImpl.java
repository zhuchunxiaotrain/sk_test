package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.FinanceYearDao;
import com.joint.core.entity.finance.FinanceYear;
import com.joint.core.service.FinanceYearService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/9.
 */
@Service
public class FinanceYearServiceImpl extends BaseFlowServiceImpl<FinanceYear,String> implements FinanceYearService {
    @Resource
    FinanceYearDao financeYearDao;

    @Override
    public BaseFlowDao<FinanceYear, String> getBaseFlowDao() {
        return financeYearDao;
    }
}
