package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Seal;

/**
 * Created by ZhuChunXiao on 2017/3/20.
 */
public interface SealService extends BaseFlowService<Seal,String> {
}
