package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ReceptionDao;
import com.joint.core.entity.manage.Reception;
import com.joint.core.service.ReceptionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/30.
 */
@Service
public class ReceptionServiceImpl extends BaseFlowServiceImpl<Reception,String> implements ReceptionService {
    @Resource
    ReceptionDao receptionDao;

    @Override
    public BaseFlowDao<Reception, String> getBaseFlowDao() {
        return receptionDao;
    }
}
