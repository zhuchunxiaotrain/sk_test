package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.finance.AuditFirst;

/**
 * Created by ZhuChunXiao on 2017/3/14.
 */
public interface AuditFirstService extends BaseFlowService<AuditFirst,String> {
}
