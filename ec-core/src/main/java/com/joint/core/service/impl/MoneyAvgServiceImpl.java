package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.MoneyAvgDao;
import com.joint.core.dao.StrongBigDao;
import com.joint.core.entity.MoneyAvg;
import com.joint.core.entity.StrongBig;
import com.joint.core.service.MoneyAvgService;
import com.joint.core.service.StrongBigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class MoneyAvgServiceImpl extends BaseFlowServiceImpl<MoneyAvg, String> implements MoneyAvgService {
    @Resource
    MoneyAvgDao moneyAvgDao;

    @Override
    public BaseFlowDao<MoneyAvg, String> getBaseFlowDao() {
        return moneyAvgDao;
    }
}
