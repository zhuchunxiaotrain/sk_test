package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.NoticeDao;
import com.joint.core.dao.RegisteDao;
import com.joint.core.entity.manage.Notice;
import com.joint.core.entity.manage.Registe;
import com.joint.core.service.NoticeService;
import com.joint.core.service.RegisteService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class RegisteServiceImpl extends BaseFlowServiceImpl<Registe,String> implements RegisteService {
    @Resource
    RegisteDao registeDao;

    @Override
    public BaseFlowDao<Registe, String> getBaseFlowDao() {
        return registeDao;
    }
}
