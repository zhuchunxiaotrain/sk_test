package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.EmployeesDao;
import com.joint.core.dao.TrainDao;
import com.joint.core.entity.Employees;
import com.joint.core.entity.Train;
import com.joint.core.service.EmployeesService;
import com.joint.core.service.TrainService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class TrainServiceImpl extends BaseFlowServiceImpl<Train,String> implements TrainService {
    @Resource
    TrainDao trainDao;

    @Override
    public BaseFlowDao<Train, String> getBaseFlowDao() {
        return trainDao;
    }
}


