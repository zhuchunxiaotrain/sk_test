package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.MeetingLeave;
import com.joint.core.entity.manage.MeetingSign;
import com.joint.core.entity.manage.MeetingUse;

import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
public interface MeetingLeaveService extends BaseFlowService<MeetingLeave,String> {
    List<MeetingLeave> findDataByUsersParentId(String userId, String parentId);
}
