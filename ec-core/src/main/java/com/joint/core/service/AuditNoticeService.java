package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.finance.AuditNotice;

import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/13.
 */
public interface AuditNoticeService extends BaseFlowService<AuditNotice,String> {
}
