package com.joint.core.service;

import com.joint.base.entity.Company;
import com.joint.base.service.BaseEntityService;
import com.joint.core.entity.NumberCreater;

/**
 * Created by dqf on 2015/8/25.
 */
public interface NumberCreaterService extends BaseEntityService<NumberCreater,String> {
    /**
     * 依据实体名称获取编号，
     * @param entityName
     * @param header 编号抬头
     * @param length 数字编号长度
     * @param company 操作人所在公司，默认为登录账号公司
     * @return
     */
    public String getNumber(String entityName, String header, int length, Company company);
}
