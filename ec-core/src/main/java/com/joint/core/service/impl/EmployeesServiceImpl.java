package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.EmployeesDao;
import com.joint.core.dao.EmploymentDao;
import com.joint.core.entity.Employees;
import com.joint.core.entity.Employment;
import com.joint.core.service.EmployeesService;
import com.joint.core.service.EmploymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class EmployeesServiceImpl extends BaseFlowServiceImpl<Employees,String> implements EmployeesService {
    @Resource
    EmployeesDao employeesDao;

    @Override
    public BaseFlowDao<Employees, String> getBaseFlowDao() {
        return employeesDao;
    }

    @Override
    public List<Employees> findEmployees(String partyMember) {
        return employeesDao.findEmployees(partyMember);
    }
}
