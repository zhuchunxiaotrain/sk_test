package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.AnnualBudget;
import com.joint.core.entity.AnnualBudgetDetail;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface AnnualBudgetDetailService extends BaseFlowService<AnnualBudgetDetail,String > {
}
