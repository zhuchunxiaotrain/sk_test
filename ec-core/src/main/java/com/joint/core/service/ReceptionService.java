package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Reception;

/**
 * Created by ZhuChunXiao on 2017/3/30.
 */
public interface ReceptionService extends BaseFlowService<Reception,String> {
}
