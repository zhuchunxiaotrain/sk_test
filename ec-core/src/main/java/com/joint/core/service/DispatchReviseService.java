package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.DispatchRevise;


public interface DispatchReviseService extends BaseFlowService<DispatchRevise,String> {
}
