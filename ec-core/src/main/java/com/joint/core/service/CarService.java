package com.joint.core.service;

import com.fz.us.dict.entity.Dict;
import com.joint.base.entity.Users;
import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Car;
import com.joint.core.entity.manage.CarConfig;

import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/21.
 */
public interface CarService extends BaseFlowService<Car,String> {
    int getMaxKilometre(Dict carNo);
    List<Car> getUseInfo(Dict carNo);
    boolean checkUse(CarConfig carConfig,Date dateStart,Users user);
}
