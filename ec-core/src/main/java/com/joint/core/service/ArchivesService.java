package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Archives;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
public interface ArchivesService extends BaseFlowService<Archives,String> {
}
