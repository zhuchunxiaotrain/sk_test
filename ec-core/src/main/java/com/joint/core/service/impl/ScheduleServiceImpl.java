package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.NoticeDao;
import com.joint.core.dao.ScheduleDao;
import com.joint.core.entity.manage.Notice;
import com.joint.core.entity.manage.Schedule;
import com.joint.core.service.NoticeService;
import com.joint.core.service.ScheduleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by zhuchunxiao on 2015/8/26.
 */
@Service
public class ScheduleServiceImpl extends BaseFlowServiceImpl<Schedule,String> implements ScheduleService {
    @Resource
    ScheduleDao scheduleDao;

    @Override
    public BaseFlowDao<Schedule, String> getBaseFlowDao() {
        return scheduleDao;
    }
}
