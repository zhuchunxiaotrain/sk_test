package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Receive;


public interface ReceiveService extends BaseFlowService<Receive,String> {
}
