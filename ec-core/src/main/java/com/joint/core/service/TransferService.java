package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.Change;


/**
 * Created by dqf on 2015/8/26.
 */
public interface TransferService extends BaseFlowService<Change,String> {
}
