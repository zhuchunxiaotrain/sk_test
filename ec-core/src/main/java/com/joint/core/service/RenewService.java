package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.Renew;

/**
 * Created by dqf on 2015/8/26.
 */
public interface RenewService extends BaseFlowService<Renew,String> {
}
