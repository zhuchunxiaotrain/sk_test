package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.SelectionNoticeDao;
import com.joint.core.dao.StrongBigDao;
import com.joint.core.entity.SelectionNotice;
import com.joint.core.entity.StrongBig;
import com.joint.core.service.SelectionNoticeService;
import com.joint.core.service.StrongBigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class SelectionNoticeServiceImpl extends BaseFlowServiceImpl<SelectionNotice, String> implements SelectionNoticeService {
    @Resource
    SelectionNoticeDao selectionNoticeDao;


    @Override
    public BaseFlowDao<SelectionNotice, String> getBaseFlowDao() {
        return selectionNoticeDao;
    }
}
