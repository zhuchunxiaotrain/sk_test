package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.finance.Expend;

/**
 * Created by ZhuChunXiao on 2017/4/1.
 */
public interface ExpendService extends BaseFlowService<Expend,String> {
}
