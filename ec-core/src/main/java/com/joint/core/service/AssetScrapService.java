package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.AssetScrap;

/**
 * Created by ZhuChunXiao on 2017/3/30.
 */
public interface AssetScrapService extends BaseFlowService<AssetScrap,String> {
}
