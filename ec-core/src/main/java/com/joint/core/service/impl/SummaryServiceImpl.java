package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.SummaryDao;
import com.joint.core.dao.TrainDao;
import com.joint.core.entity.Summary;
import com.joint.core.entity.Train;
import com.joint.core.service.SummaryService;
import com.joint.core.service.TrainService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class SummaryServiceImpl extends BaseFlowServiceImpl<Summary,String> implements SummaryService {
    @Resource
    SummaryDao summaryDao;

    @Override
    public BaseFlowDao<Summary, String> getBaseFlowDao() {
        return summaryDao;
    }
}


