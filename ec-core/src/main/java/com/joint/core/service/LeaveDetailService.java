package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.LeaveDetail;

import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
public interface LeaveDetailService extends BaseFlowService<LeaveDetail,String> {

    public LeaveDetail findLeaveDetail(String userId, Integer time);
    public LeaveDetail findLeaveDetailByusers(String userId);


}
