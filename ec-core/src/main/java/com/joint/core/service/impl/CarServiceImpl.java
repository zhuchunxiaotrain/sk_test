package com.joint.core.service.impl;

import com.fz.us.dict.entity.Dict;
import com.joint.base.dao.BaseFlowDao;
import com.joint.base.entity.Users;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.CarDao;
import com.joint.core.entity.manage.Car;
import com.joint.core.entity.manage.CarConfig;
import com.joint.core.service.CarService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/21.
 */
@Service
public class CarServiceImpl extends BaseFlowServiceImpl<Car,String> implements CarService {
    @Resource
    CarDao carDao;

    @Override
    public BaseFlowDao<Car, String> getBaseFlowDao() {
        return carDao;
    }

    @Override
    public int getMaxKilometre(Dict carNo) {
        return carDao.getMaxKilometre(carNo);
    }

    @Override
    public List<Car> getUseInfo(Dict carNo) {
        return carDao.getUseInfo(carNo);
    }

    @Override
    public boolean checkUse(CarConfig carConfig, Date dateStart, Users user) {
        return carDao.checkUse(carConfig,dateStart,user);
    }
}
