package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.AuditAnswerDao;
import com.joint.core.entity.finance.AuditAnswer;
import com.joint.core.service.AuditAnswerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/15.
 */
@Service
public class AuditAnswerServiceImpl extends BaseFlowServiceImpl<AuditAnswer,String> implements AuditAnswerService {
    @Resource
    AuditAnswerDao auditAnswerDao;

    @Override
    public BaseFlowDao<AuditAnswer, String> getBaseFlowDao() {
        return auditAnswerDao;
    }
}
