package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.TrainDao;
import com.joint.core.dao.TrainRecordDao;
import com.joint.core.entity.Train;
import com.joint.core.entity.TrainRecord;
import com.joint.core.service.TrainRecordService;
import com.joint.core.service.TrainService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class TrainRecordServiceImpl extends BaseFlowServiceImpl<TrainRecord,String> implements TrainRecordService {
    @Resource
    TrainRecordDao trainRecordDao;

    @Override
    public BaseFlowDao<TrainRecord, String> getBaseFlowDao() {
        return trainRecordDao;
    }
}


