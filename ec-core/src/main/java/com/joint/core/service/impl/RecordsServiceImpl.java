package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.RecordsDao;
import com.joint.core.entity.Records;
import com.joint.core.service.RecordsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class RecordsServiceImpl extends BaseFlowServiceImpl<Records,String> implements RecordsService {
    @Resource
    RecordsDao recordsDao;

    @Override
    public BaseFlowDao<Records, String> getBaseFlowDao() {
        return recordsDao;
    }

}
