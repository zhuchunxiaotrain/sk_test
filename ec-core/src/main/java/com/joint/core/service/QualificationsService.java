package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Qualifications;

/**
 * Created by ZhuChunXiao on 2017/3/20.
 */
public interface QualificationsService extends BaseFlowService<Qualifications,String > {
}
