package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.UseDetailDao;
import com.joint.core.entity.manage.UseDetail;
import com.joint.core.service.UseDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/29.
 */
@Service
public class UseDetailServiceImpl extends BaseFlowServiceImpl<UseDetail,String> implements UseDetailService {
    @Resource
    UseDetailDao useDetailDao;

    @Override
    public BaseFlowDao<UseDetail, String> getBaseFlowDao() {
        return useDetailDao;
    }
}
