package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.finance.WorkPlan;

/**
 * Created by ZhuChunXiao on 2017/3/10.
 */
public interface WorkPlanService extends BaseFlowService<WorkPlan,String> {
}
