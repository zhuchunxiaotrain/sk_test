package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.ManageProInfo;
import com.joint.core.entity.manage.Notice;


public interface NoticeService extends BaseFlowService<Notice,String> {
}
