package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.AuditLastDao;
import com.joint.core.entity.finance.AuditLast;
import com.joint.core.service.AuditLastService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/15.
 */
@Service
public class AuditLastServiceImpl extends BaseFlowServiceImpl<AuditLast,String> implements AuditLastService {
    @Resource
    AuditLastDao auditLastDao;

    @Override
    public BaseFlowDao<AuditLast, String> getBaseFlowDao() {
        return auditLastDao;
    }
}
