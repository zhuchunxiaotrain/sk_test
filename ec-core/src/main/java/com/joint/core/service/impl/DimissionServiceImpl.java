package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.DimissionDao;
import com.joint.core.entity.Dimission;
import com.joint.core.service.DimissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class DimissionServiceImpl extends BaseFlowServiceImpl<Dimission,String> implements DimissionService {
    @Resource
    DimissionDao dimissionDao;

    @Override
    public BaseFlowDao<Dimission, String> getBaseFlowDao() {
        return dimissionDao;
    }

}
