package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Dispatch;


public interface DispatchService extends BaseFlowService<Dispatch,String> {
}
