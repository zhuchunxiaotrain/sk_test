package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.PartyMoney;
import com.joint.core.entity.PartyMoneyDetail;

/**
 * Created by 9Joint on 2016/12/16.
 */
public interface PartyMoneyDetailService extends BaseFlowService<PartyMoneyDetail,String > {
}
