package com.joint.core.service.impl;

import com.joint.base.dao.BaseEntityDao;
import com.joint.base.service.impl.BaseEntityServiceImpl;
import com.joint.core.dao.NoticeQueryDao;
import com.joint.core.entity.manage.NoticeQuery;
import com.joint.core.service.NoticeQueryService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

@Service
public class NoticeQueryServiceImpl extends BaseEntityServiceImpl<NoticeQuery,String> implements NoticeQueryService {
    @Resource
    NoticeQueryDao noticeQueryDao;

    @Override
    public BaseEntityDao<NoticeQuery, String> getBaseEntityDao() {
        return noticeQueryDao;
    }

    @Override
    public List<NoticeQuery> findUsersByNoticeQuery(String userId) {
        return noticeQueryDao.findUsersByNoticeQuery(userId);
    }

    @Override
    public List<NoticeQuery> findUsersByNoticeQuery(String userId, String keyId) {
        return noticeQueryDao.findUsersByNoticeQuery(userId, keyId);
    }
}
