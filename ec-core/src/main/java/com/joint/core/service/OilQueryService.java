package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.CarConfig;
import com.joint.core.entity.manage.Oil;
import com.joint.core.entity.manage.OilQuery;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
public interface OilQueryService extends BaseFlowService<OilQuery,String> {
    Double getMoneyNum(CarConfig carConfig);
}
