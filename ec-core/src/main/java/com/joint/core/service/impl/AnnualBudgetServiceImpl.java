package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.AnnualBudgetDao;
import com.joint.core.dao.StrongBigDao;
import com.joint.core.entity.AnnualBudget;
import com.joint.core.entity.StrongBig;
import com.joint.core.service.AnnualBudgetService;
import com.joint.core.service.StrongBigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class AnnualBudgetServiceImpl extends BaseFlowServiceImpl<AnnualBudget, String> implements AnnualBudgetService{
    @Resource
    AnnualBudgetDao annualBudgetDao;

    @Override
    public BaseFlowDao<AnnualBudget, String> getBaseFlowDao() {
        return annualBudgetDao;
    }
}
