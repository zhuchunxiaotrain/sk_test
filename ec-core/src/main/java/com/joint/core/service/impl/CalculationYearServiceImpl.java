package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.CalculationYearDao;
import com.joint.core.entity.finance.CalculationYear;
import com.joint.core.service.CalculationYearService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/6.
 */
@Service
public class CalculationYearServiceImpl extends BaseFlowServiceImpl<CalculationYear,String> implements CalculationYearService {

    @Resource
    CalculationYearDao calculationYearDao;

    @Override
    public BaseFlowDao<CalculationYear, String> getBaseFlowDao() {
        return calculationYearDao;
    }
}
