package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.FamilyDao;
import com.joint.core.dao.ManageDao;
import com.joint.core.entity.Family;
import com.joint.core.entity.Manage;
import com.joint.core.service.FamilyService;
import com.joint.core.service.ManageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class FamilyServiceImpl extends BaseFlowServiceImpl<Family,String> implements FamilyService {
    @Resource
    FamilyDao familyDao;

    @Override
    public BaseFlowDao<Family, String> getBaseFlowDao() {
        return familyDao;
    }
}
