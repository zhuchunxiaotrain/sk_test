package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.EmploymentDao;
import com.joint.core.dao.ManageDao;
import com.joint.core.entity.Employment;
import com.joint.core.entity.Manage;
import com.joint.core.service.EmploymentService;
import com.joint.core.service.ManageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class EmploymentServiceImpl extends BaseFlowServiceImpl<Employment,String> implements EmploymentService {
    @Resource
    EmploymentDao employmentDao;

    @Override
    public BaseFlowDao<Employment, String> getBaseFlowDao() {
        return employmentDao;
    }
}
