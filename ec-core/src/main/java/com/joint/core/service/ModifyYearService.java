package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.finance.ModifyYear;

/**
 * Created by ZhuChunXiao on 2017/3/7.
 */
public interface ModifyYearService extends BaseFlowService<ModifyYear,String> {
    void updateCalFile(String id,String sql);
}
