package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ArchivesDao;
import com.joint.core.entity.manage.Archives;
import com.joint.core.service.ArchivesService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@Service
public class ArchivesServiceImpl extends BaseFlowServiceImpl<Archives,String> implements ArchivesService {
    @Resource
    ArchivesDao archivesDao;

    @Override
    public BaseFlowDao<Archives, String> getBaseFlowDao() {
        return archivesDao;
    }
}
