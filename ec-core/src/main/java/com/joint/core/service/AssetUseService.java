package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.AssetUse;

/**
 * Created by ZhuChunXiao on 2017/3/28.
 */
public interface AssetUseService extends BaseFlowService<AssetUse,String> {
}
