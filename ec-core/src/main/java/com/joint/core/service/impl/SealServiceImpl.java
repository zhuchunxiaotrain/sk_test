package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.SealDao;
import com.joint.core.entity.manage.Seal;
import com.joint.core.service.SealService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhuChunXiao on 2017/3/20.
 */
@Service
public class SealServiceImpl extends BaseFlowServiceImpl<Seal,String> implements SealService {
    @Resource
    SealDao sealDao;

    @Override
    public BaseFlowDao<Seal, String> getBaseFlowDao() {
        return sealDao;
    }
}
