package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.Family;
import com.joint.core.entity.Manage;

/**
 * Created by dqf on 2015/8/26.
 */
public interface FamilyService extends BaseFlowService<Family,String> {
}
