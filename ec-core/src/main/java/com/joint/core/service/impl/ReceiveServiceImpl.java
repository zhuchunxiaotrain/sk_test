package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.ReceiveDao;
import com.joint.core.entity.manage.Receive;
import com.joint.core.service.ReceiveService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;


@Service
public class ReceiveServiceImpl extends BaseFlowServiceImpl<Receive,String> implements ReceiveService {
    @Resource
    ReceiveDao receiveDao;

    @Override
    public BaseFlowDao<Receive, String> getBaseFlowDao() {
        return receiveDao;
    }
}
