package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.MeetingLeaveDao;
import com.joint.core.dao.MeetingSignDao;
import com.joint.core.entity.manage.MeetingLeave;
import com.joint.core.entity.manage.MeetingSign;
import com.joint.core.service.MeetingLeaveService;
import com.joint.core.service.MeetingSignService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ZhuChunXiao on 2017/3/24.
 */
@Service
public class MeetingLeaveServiceImpl extends BaseFlowServiceImpl<MeetingLeave,String> implements MeetingLeaveService {
    @Resource
    MeetingLeaveDao meetingLeaveDao;

    @Override
    public BaseFlowDao<MeetingLeave, String> getBaseFlowDao() {
        return meetingLeaveDao;
    }

    @Override
    public List<MeetingLeave> findDataByUsersParentId(String userId, String parentId) {
        return meetingLeaveDao.findDataByUsersParentId(userId, parentId);
    }
}
