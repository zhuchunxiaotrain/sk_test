package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.LeaveDao;
import com.joint.core.entity.Leave;
import com.joint.core.service.LeaveService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dqf on 2015/8/26.
 */
@Service
public class LeaveServiceImpl extends BaseFlowServiceImpl<Leave,String> implements LeaveService {
    @Resource
    LeaveDao leaveDao;

    @Override
    public BaseFlowDao<Leave, String> getBaseFlowDao() {
        return leaveDao;
    }
}


