package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.manage.Purchase;

/**
 * Created by ZhuChunXiao on 2017/3/27.
 */
public interface PurchaseService extends BaseFlowService<Purchase,String > {
}
