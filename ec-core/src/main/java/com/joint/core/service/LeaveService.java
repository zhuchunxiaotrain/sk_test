package com.joint.core.service;

import com.joint.base.service.BaseFlowService;
import com.joint.core.entity.Employees;
import com.joint.core.entity.Leave;
import com.joint.core.entity.Train;

import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
public interface LeaveService extends BaseFlowService<Leave,String> {


}
