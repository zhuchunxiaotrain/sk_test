package com.joint.core.service.impl;

import com.joint.base.dao.BaseFlowDao;
import com.joint.base.service.impl.BaseFlowServiceImpl;
import com.joint.core.dao.SelectionApplyDao;
import com.joint.core.dao.SelectionSpeechDao;
import com.joint.core.entity.SelectionApply;
import com.joint.core.entity.SelectionSpeech;
import com.joint.core.service.SelectionApplyService;
import com.joint.core.service.SelectionSpeechService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by 9Joint on 2016/12/16.
 */
@Service
public class SelectionSpeechServiceImpl extends BaseFlowServiceImpl<SelectionSpeech, String> implements SelectionSpeechService{
    @Resource
    SelectionSpeechDao selectionSpeechDao;


    @Override
    public BaseFlowDao<SelectionSpeech, String> getBaseFlowDao() {
        return selectionSpeechDao;
    }
}
