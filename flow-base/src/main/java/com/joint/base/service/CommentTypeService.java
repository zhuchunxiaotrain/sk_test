package com.joint.base.service;

import com.joint.base.entity.CommentType;
import com.joint.base.entity.TaskFlow;
import com.joint.base.entity.Users;

import java.util.List;


/**
 * Service接口 - 组织
 * ============================================================================
 * 版权所有 2013
 * ----------------------------------------------------------------------------
 * 
 * @author 
 * 
 * @version 0.1 2013-05-09
 */

public interface CommentTypeService extends BaseEntityService<CommentType, String> {
    public CommentType findCommentType(String commentId);
}