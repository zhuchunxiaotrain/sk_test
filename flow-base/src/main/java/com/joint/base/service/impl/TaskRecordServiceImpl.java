package com.joint.base.service.impl;

import com.joint.base.dao.BaseEntityDao;
import com.joint.base.dao.TaskRecordDao;
import com.joint.base.entity.TaskRecord;
import com.joint.base.service.TaskRecordService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;


/**
 * Service实现类 - 
 * ============================================================================
 * 版权所有 2013
 * ----------------------------------------------------------------------------
 * 
 * @author 
 * 
 * @version 0.1 2011-6-13
 */

@Service
public class TaskRecordServiceImpl extends BaseEntityServiceImpl<TaskRecord, String> implements TaskRecordService {
	
	@Resource
    TaskRecordDao taskRecordDao;

	@Override
	public BaseEntityDao<TaskRecord, String> getBaseEntityDao() {
		return taskRecordDao;
	}


	@Override
	public List<TaskRecord> getDataByUserKeyId(String keyId, String userId) {
		return taskRecordDao.getDataByUserKeyId(keyId, userId);
	}

	@Override
	public List<TaskRecord> getDataByKeyId(String keyId) {
		return taskRecordDao.getDataByKeyId(keyId);
	}
}