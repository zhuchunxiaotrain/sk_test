package com.joint.base.service.impl;

import com.joint.base.dao.BaseEntityDao;
import com.joint.base.dao.RemindDao;
import com.joint.base.dao.TaskRecordDao;
import com.joint.base.entity.Remind;
import com.joint.base.entity.TaskRecord;
import com.joint.base.service.RemindService;
import com.joint.base.service.TaskRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * Service实现类 - 
 * ============================================================================
 * 版权所有 2013
 * ----------------------------------------------------------------------------
 * 
 * @author 
 * 
 * @version 0.1 2011-6-13
 */

@Service
public class RemindServiceImpl extends BaseEntityServiceImpl<Remind, String> implements RemindService {
	
	@Resource
	RemindDao remindDao;

	@Override
	public BaseEntityDao<Remind, String> getBaseEntityDao() {
		return remindDao;
	}



}