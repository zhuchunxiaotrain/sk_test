package com.joint.base.service.impl;

import com.joint.base.dao.BaseEntityDao;
import com.joint.base.dao.ReadersDao;
import com.joint.base.entity.Readers;
import com.joint.base.service.ReadersService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Set;


/**
 * Service实现类 - 
 * ============================================================================
 * 版权所有 2013
 * ----------------------------------------------------------------------------
 * 
 * @author 
 * 
 * @version 0.1 2011-6-13
 */

@Service
public class ReadersServiceImpl extends BaseEntityServiceImpl<Readers, String> implements ReadersService {
	
	@Resource
    ReadersDao readersDao;

	@Override
	public BaseEntityDao<Readers, String> getBaseEntityDao() {
		return readersDao;
	}

    @Override
    public Set<String> findBussinessIdByReaders(String userId, String key) {
        return readersDao.findBussinessIdByReaders(userId, key);
    }

    @Override
    public Set<String> findBussinessIdByReaders(String userId, String key, int type) {
        return readersDao.findBussinessIdByReaders(userId, key, type);
    }

    @Override
    public int deleteByKeyIdBussinessKey(String keyId) {
        return readersDao.deleteByKeyIdBussinessKey(keyId);
    }
}