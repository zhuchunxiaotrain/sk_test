package com.joint.base.service.impl;

import com.joint.base.dao.BaseEntityDao;
import com.joint.base.dao.CommentTypeDao;
import com.joint.base.dao.TaskFlowDao;
import com.joint.base.entity.CommentType;
import com.joint.base.entity.TaskFlow;
import com.joint.base.entity.Users;
import com.joint.base.service.CommentTypeService;
import com.joint.base.service.TaskFlowService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * Service实现类 - 
 * ============================================================================
 * 版权所有 2013
 * ----------------------------------------------------------------------------
 * 
 * @author 
 * 
 * @version 0.1 2011-6-13
 */

@Service
public class CommentTypeServiceImpl extends BaseEntityServiceImpl<CommentType, String> implements CommentTypeService {
	
	@Resource
    CommentTypeDao commentTypeDao;

	@Override
	public BaseEntityDao<CommentType, String> getBaseEntityDao() {
		return commentTypeDao;
	}


    @Override
    public CommentType findCommentType(String commentId) {
        return commentTypeDao.findCommentType(commentId);
    }
}