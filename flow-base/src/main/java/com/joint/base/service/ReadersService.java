package com.joint.base.service;

import com.joint.base.entity.Readers;

import java.util.Set;


/**
 * Service接口 - 组织
 * ============================================================================
 * 版权所有 2013
 * ----------------------------------------------------------------------------
 * 
 * @author 
 * 
 * @version 0.1 2013-05-09
 */

public interface ReadersService extends BaseEntityService<Readers, String> {
    public Set<String> findBussinessIdByReaders(String userId, String key);

    public Set<String> findBussinessIdByReaders(String userId, String key, int type);

    public int deleteByKeyIdBussinessKey(String keyId);
}