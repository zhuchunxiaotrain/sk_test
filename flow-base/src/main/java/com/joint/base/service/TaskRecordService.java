package com.joint.base.service;


import com.joint.base.entity.TaskRecord;

import java.util.List;

/**
 * Service接口 - 组织
 * ============================================================================
 * 版权所有 2013
 * ----------------------------------------------------------------------------
 * 
 * @author 
 * 
 * @version 0.1 2013-05-09
 */

public interface TaskRecordService extends BaseEntityService<TaskRecord, String> {

    public List<TaskRecord> getDataByUserKeyId(String keyId, String userId);

    public List<TaskRecord> getDataByKeyId(String keyId);
}