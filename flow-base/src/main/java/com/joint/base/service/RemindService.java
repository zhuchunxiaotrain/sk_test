package com.joint.base.service;


import com.joint.base.entity.Remind;
import com.joint.base.entity.TaskRecord;

import java.util.List;

/**
 * Service接口 - 提醒
 * ============================================================================
 * 版权所有 2013
 * ----------------------------------------------------------------------------
 * 
 * @author 
 * 
 * @version 0.1 2013-05-09
 */

public interface RemindService extends BaseEntityService<Remind, String> {


}