package com.joint.base.service.impl;

import com.joint.base.dao.BaseEntityDao;
import com.joint.base.dao.TaskFlowDao;
import com.joint.base.entity.TaskFlow;
import com.joint.base.entity.Users;
import com.joint.base.service.TaskFlowService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;


/**
 * Service实现类 - 
 * ============================================================================
 * 版权所有 2013
 * ----------------------------------------------------------------------------
 * 
 * @author 
 * 
 * @version 0.1 2011-6-13
 */

@Service
public class TaskFlowServiceImpl extends BaseEntityServiceImpl<TaskFlow, String> implements TaskFlowService {
	
	@Resource
    TaskFlowDao taskFlowDao;

	@Override
	public BaseEntityDao<TaskFlow, String> getBaseEntityDao() {
		return taskFlowDao;
	}

    @Override
    public List<Users> findUsersByTaskFlow(String taskId) {
        return taskFlowDao.findUsersByTaskFlow(taskId);
    }
}