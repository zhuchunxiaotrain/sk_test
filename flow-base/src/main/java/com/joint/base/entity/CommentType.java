package com.joint.base.entity;

import com.joint.base.parent.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by ZhuChunXiao on 2017/2/6.
 */
@Entity
@Table(name="sys_commenttype")
public class CommentType extends BaseEntity {

    private static final long serialVersionUID = 924429000630168317L;
    private String commentId;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }
}
