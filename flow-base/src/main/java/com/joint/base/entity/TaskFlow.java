package com.joint.base.entity;

import com.joint.base.parent.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * 任务流程信息表
 * Created by ZhuChunXiao on 2017/2/6.
 */
@Entity
@Table(name="sys_taskflow")
public class TaskFlow  extends BaseEntity {
    private static final long serialVersionUID = -5937796218882237669L;
    private String taskId;
    private Users users;

    @ManyToOne(fetch = FetchType.LAZY)
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
