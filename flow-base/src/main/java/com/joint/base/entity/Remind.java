package com.joint.base.entity;

import com.joint.base.parent.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by ZhuChunXiao on 2017/2/14.
 * 提醒事项
 */
@Entity
@Table(name="sys_remind")
public class Remind extends BaseEntity {


    private static final long serialVersionUID = -7180287983805184898L;
    //文档id
    private String keyId;
    //所属的实体类名
    private String bussinessKey;
    //关联的用户
    private Users users;
    //提醒内容
    private String content;


    @ManyToOne(fetch = FetchType.LAZY)
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public String getBussinessKey() {
        return bussinessKey;
    }

    public void setBussinessKey(String bussinessKey) {
        this.bussinessKey = bussinessKey;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
