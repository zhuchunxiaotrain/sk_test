package com.joint.base.entity;

import com.joint.base.parent.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by ZhuChunXiao on 2017/2/6.
 */
@Entity
@Table(name="sys_readers")
public class Readers extends BaseEntity {

    private static final long serialVersionUID = -4511982228277669073L;
    //文档id
    private String keyId;
    //文档流程引擎关键字
    private String bussinessKey;
    //关联的可阅读用户
    private Users users;
    //可见的类型，1为文档阅览者，2为已归档读者
    private int type;

    @ManyToOne(fetch = FetchType.LAZY)
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public String getBussinessKey() {
        return bussinessKey;
    }

    public void setBussinessKey(String bussinessKey) {
        this.bussinessKey = bussinessKey;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
