package com.joint.base.entity;

import com.joint.base.parent.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by ZhuChunXiao on 2017/2/14.
 * 已办事项表
 */
@Entity
@Table(name="sys_taskrecord")
public class TaskRecord extends BaseEntity {

    private static final long serialVersionUID = 8586158941742653610L;
    //文档id
    private String keyId;
    //所属的实体类名
    private String bussinessKey;
    //关联的用户
    private Users users;
    //类型，1为未结，2为已结
    private int type;
    //所属模块
    private String module;

    @ManyToOne(fetch = FetchType.LAZY)
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public String getBussinessKey() {
        return bussinessKey;
    }

    public void setBussinessKey(String bussinessKey) {
        this.bussinessKey = bussinessKey;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }
}
