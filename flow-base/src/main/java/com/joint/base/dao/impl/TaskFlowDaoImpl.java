package com.joint.base.dao.impl;


import com.joint.base.dao.CompanyDao;
import com.joint.base.dao.TaskFlowDao;
import com.joint.base.entity.Company;
import com.joint.base.entity.Power;
import com.joint.base.entity.TaskFlow;
import com.joint.base.entity.Users;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Dao实现类 - 岗位
 * ============================================================================
 * 版权所有 2013 。
 * 
 * @author 
 * @version 0.1 2013-1-16
 * ============================================================================
 */

@Repository
public class TaskFlowDaoImpl extends BaseEntityDaoImpl<TaskFlow, String> implements TaskFlowDao {

    @Override
    public List<Users> findUsersByTaskFlow(String taskId) {
        Assert.notNull(taskId,"taskId is required");
        String hql="SELECT users from TaskFlow where taskId =:taskId";
        List<Users> usersList =  getSession().createQuery(hql).setParameter("taskId", taskId).list();
        return  usersList;
    }
}