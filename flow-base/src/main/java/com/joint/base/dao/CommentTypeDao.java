package com.joint.base.dao;

import com.joint.base.entity.CommentType;


import java.util.List;

/**
 *
 * ============================================================================
 * 版权所有 2013 。
 * 
 * @author 
 * @version 0.1 2013-1-16
 * ============================================================================
 */

public interface CommentTypeDao extends BaseEntityDao<CommentType, String> {

    public CommentType findCommentType(String commentId);
}