package com.joint.base.dao;


import com.joint.base.entity.TaskRecord;
import com.joint.base.entity.Users;

import java.util.List;


/**
 *
 * ============================================================================
 * 版权所有 2013 。
 * 
 * @author 
 * @version 0.1 2013-1-16
 * ============================================================================
 */

public interface TaskRecordDao extends BaseEntityDao<TaskRecord, String> {

    public List<TaskRecord> getDataByUserKeyId(String keyId, String userId);

    public List<TaskRecord> getDataByKeyId(String keyId);
}