package com.joint.base.dao;

import com.joint.base.entity.TaskFlow;
import com.joint.base.entity.Users;

import java.util.List;

/**
 *
 * ============================================================================
 * 版权所有 2013 。
 * 
 * @author 
 * @version 0.1 2013-1-16
 * ============================================================================
 */

public interface TaskFlowDao extends BaseEntityDao<TaskFlow, String> {

    public List<Users> findUsersByTaskFlow(String taskId);
}