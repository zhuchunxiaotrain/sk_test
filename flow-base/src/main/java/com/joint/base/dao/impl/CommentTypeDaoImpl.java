package com.joint.base.dao.impl;


import com.joint.base.dao.CommentTypeDao;
import com.joint.base.dao.TaskFlowDao;
import com.joint.base.entity.CommentType;
import com.joint.base.entity.TaskFlow;
import com.joint.base.entity.Users;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Dao实现类 - 岗位
 * ============================================================================
 * 版权所有 2013 。
 * 
 * @author 
 * @version 0.1 2013-1-16
 * ============================================================================
 */

@Repository
public class CommentTypeDaoImpl extends BaseEntityDaoImpl<CommentType, String> implements CommentTypeDao {

    @Override
    public CommentType findCommentType(String commentId) {
        Assert.notNull(commentId,"commentId is required");
        String hql="from CommentType where commentId =:commentId";
        List<CommentType> commentTypeList =  getSession().createQuery(hql).setParameter("commentId", commentId).list();
        if(commentTypeList.size()>0){
            return commentTypeList.get(0);
        }
        return null;
    }
}