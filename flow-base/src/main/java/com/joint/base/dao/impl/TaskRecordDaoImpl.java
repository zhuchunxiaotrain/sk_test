package com.joint.base.dao.impl;


import com.joint.base.dao.TaskRecordDao;
import com.joint.base.entity.TaskRecord;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;


/**
 * Dao实现类 - 任务流程信息表
 * ============================================================================
 * 版权所有 2013 。
 * 
 * @author 
 * @version 0.1 2013-1-16
 * ============================================================================
 */

@Repository
public class TaskRecordDaoImpl extends BaseEntityDaoImpl<TaskRecord, String> implements TaskRecordDao {

    @Override
    public List<TaskRecord> getDataByUserKeyId(String keyId, String userId) {
        Assert.notNull(keyId, "keyId is required");
        Assert.notNull(userId, "userId is required");
        String hql="from TaskRecord where keyId =:keyId and users.id =:userId";
        List<TaskRecord> taskRecordList = getSession().createQuery(hql).setParameter("keyId", keyId).setParameter("userId", userId).list();
        return taskRecordList;
    }

    @Override
    public List<TaskRecord> getDataByKeyId(String keyId) {
        Assert.notNull(keyId, "keyId is required");
        String hql="from TaskRecord where keyId =:keyId ";
        List<TaskRecord> taskRecordList = getSession().createQuery(hql).setParameter("keyId", keyId).list();
        return taskRecordList;
    }
}