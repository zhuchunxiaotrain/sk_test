package com.joint.base.dao.impl;


import com.fz.us.base.util.LogUtil;
import com.google.common.collect.Sets;
import com.joint.base.dao.ReadersDao;
import com.joint.base.entity.Readers;

import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Set;

/**
 * Dao实现类 - 岗位
 * ============================================================================
 * 版权所有 2013 。
 * 
 * @author 
 * @version 0.1 2013-1-16
 * ============================================================================
 */

@Repository
public class ReadersDaoImpl extends BaseEntityDaoImpl<Readers, String> implements ReadersDao {


    @Override
    public Set<String> findBussinessIdByReaders(String userId, String key) {
        Assert.notNull(userId,"userId is required");
        Assert.notNull(key,"key is required");
        Set<String> stringSet = Sets.newHashSet();
        String hql="SELECT distinct(keyId) from Readers where bussinessKey =:key and users.id = :userId";
        List<String> stringList =  getSession().createQuery(hql).setParameter("key", key).setParameter("userId", userId).list();
        stringSet.addAll(stringList);
        return stringSet;
    }

    @Override
    public Set<String> findBussinessIdByReaders(String userId, String key, int type) {
        Assert.notNull(userId,"userId is required");
        Assert.notNull(key,"key is required");
        Assert.notNull(type,"type is required");
        Set<String> stringSet = Sets.newHashSet();
        String hql="SELECT distinct(keyId) from Readers where bussinessKey =:key and users.id = :userId and type=:type";
        List<String> stringList =  getSession().createQuery(hql).setParameter("key", key).setParameter("userId", userId).setParameter("type",type).list();
        stringSet.addAll(stringList);
        return stringSet;
    }

    @Override
    public int deleteByKeyIdBussinessKey(String keyId) {
        Assert.notNull(keyId,"keyId is required");
        String hql= "delete from Readers where keyId =:keyId";
        int rs = getSession().createQuery(hql).setParameter("keyId", keyId).executeUpdate();
        LogUtil.info("rs:"+rs);
        return rs;
    }
}