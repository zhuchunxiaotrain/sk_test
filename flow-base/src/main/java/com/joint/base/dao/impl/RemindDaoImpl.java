package com.joint.base.dao.impl;


import com.joint.base.dao.RemindDao;
import com.joint.base.dao.TaskRecordDao;
import com.joint.base.entity.Remind;
import com.joint.base.entity.TaskRecord;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;


/**
 * Dao实现类 - 提醒表
 * ============================================================================
 * 版权所有 2013 。
 * 
 * @author 
 * @version 0.1 2013-1-16
 * ============================================================================
 */

@Repository
public class RemindDaoImpl extends BaseEntityDaoImpl<Remind, String> implements RemindDao {


}