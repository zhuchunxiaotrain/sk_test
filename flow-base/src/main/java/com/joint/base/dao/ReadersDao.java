package com.joint.base.dao;

import com.joint.base.entity.Readers;
import java.util.Set;

/**
 *
 * ============================================================================
 * 版权所有 2013 。
 * 
 * @author 
 * @version 0.1 2013-1-16
 * ============================================================================
 */

public interface ReadersDao extends BaseEntityDao<Readers, String> {

    public Set<String> findBussinessIdByReaders(String userId, String key);

    public Set<String> findBussinessIdByReaders(String userId, String key, int type);

    public int deleteByKeyIdBussinessKey(String keyId);
}