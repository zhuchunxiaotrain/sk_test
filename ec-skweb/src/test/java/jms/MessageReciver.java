package jms;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.TextMessage;

/**
 * Created by dqf on 2015/9/10.
 */
public class MessageReciver {
    public static void main(String args[]) throws JMSException {
        ApplicationContext context = new ClassPathXmlApplicationContext("jms/applicationContext-jms-advanced.xml");
        JmsTemplate jmsTemplate = (JmsTemplate) context.getBean("advancedJmsTemplate");
        Destination destination = (Destination) context.getBean("destination");

        TextMessage msg = null;
        boolean isContinue = true;
        while (isContinue){
            msg = (TextMessage) jmsTemplate.receive(destination);
            System.out.println("receive:"+msg.getText());
            if (msg.getText().equals("end")){
                isContinue = false;
                System.out.println("receive end and exit this application");
            }
        }
        System.out.println("exit!");
    }
}
