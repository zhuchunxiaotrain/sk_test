package jms;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Destination;


/**
 * Created by dqf on 2015/9/10.
 */
public class MessageSender extends Thread {
    public static void main(String args[]) throws InterruptedException {
        ApplicationContext context = new ClassPathXmlApplicationContext("jms/applicationContext-jms-advanced.xml");
        JmsTemplate jmsTemplate = (JmsTemplate) context.getBean("advancedJmsTemplate");
        Destination destination = (Destination) context.getBean("destination");
        for (int i=1;i<100;i++){
            System.out.println("send i="+i);
            MyMessageCreator myMessageCreator = new MyMessageCreator();
            myMessageCreator.n = i;
            jmsTemplate.send(destination,myMessageCreator);
            sleep(10000);
        }
    }
}
