package jms;

import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

/**
 * Created by dqf on 2015/9/10.
 */
public class MyMessageCreator implements MessageCreator {
    public int n =0;
    private static String str1 = "this is the ";
    private static String str2 = " message";
    private String str = "";

    @Override
    public Message createMessage(Session session) throws JMSException {
        System.out.println("MyMessageCreator n="+n);
        if (n == 9){
            return session.createTextMessage("end");
        }
        str = str1+n+str2;
        return session.createTextMessage(str);
    }


}
