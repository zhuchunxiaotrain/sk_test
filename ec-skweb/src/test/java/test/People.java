package test;

import java.io.Serializable;

/**
 * Created by dqf on 2015/9/14.
 */
public class People implements Serializable {
    private String name;
    private String sex;
    private int age;
    private String city;

    public People(String name, String sex, int age, String city){
        this.setName(name);
        this.setAge(age);
        this.setCity(city);
        this.setSex(sex);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
