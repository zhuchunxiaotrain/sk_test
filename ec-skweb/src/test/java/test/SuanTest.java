package test;

import org.junit.Test;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by dqf on 2015/9/16.
 */
public class SuanTest {
    private Long f(int num){
        if (num == 1) {
            return 1L;
        }
        return num*f(num-1);
    }

    @Test
    public void Test1() throws IOException {
        int n=3;
        Long count = 0L;
        for (int i=1;i<=n;i++){
            count += f(i);
        }
        System.out.println(count);
        DataInputStream in = new DataInputStream(new FileInputStream("dest.txt"));
    }

}
