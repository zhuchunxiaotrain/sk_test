package test;

import java.io.*;

/**
 * Created by dqf on 2015/9/14.
 */
public class FileExample {
    public static void main(String[] args) {
        //createFile();
        //checkFileLength();
        //fileCopy();
        objectStream();
    }

    public static void createFile() {
        File f=new File("D:/java/files/create.txt");
        try{
            f.createNewFile();
            System.out.println("该分区大小 "+f.getTotalSpace()/(1024*1024*1024)+"G");
            f.mkdirs();
            System.out.println("文件名 "+f.getName());
            System.out.println("文件父目录字符串 "+f.getParent());

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void checkFileLength(){
        int count = 0;
        InputStream reader = null;
        try{
            reader = new FileInputStream(new File("D:/java/files/create.txt"));
            byte[] b = new byte[128];
            int tmp = 0;
            do{
                tmp = reader.read(b);
                System.out.println(new String(b,"UTF8"));
                if (tmp > 0)
                    count +=tmp;
            }while(tmp>0);
            while (reader.read(b)>0){
                count++;
            }
            System.out.println("长度是:" + count + "字节");
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try{
                reader.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public static void fileCopy(){
        byte[] b = new byte[512];
        int numberRead = 0;
        FileInputStream input = null;
        FileOutputStream out = null;
        try{
            input = new FileInputStream("D:/java/files/create.txt");
            out = new FileOutputStream("D:/java/files/new.txt");
            while ((numberRead = input.read(b))!=-1){
                out.write(b,0,numberRead);
            }

        }catch (final IOException e){
            e.printStackTrace();
        }finally {
            try{
                input.close();
                out.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public static void objectStream(){
        ObjectOutputStream objectwriter = null;
        ObjectInputStream objectreader = null;

        try{
            objectwriter = new ObjectOutputStream(new FileOutputStream("D:/java/files/object.txt"));
            objectwriter.writeObject(new People("Jack","M",30,"ShanHai"));
            objectwriter.writeObject(new People("Tom","F",22,"BeiJing"));
            objectreader = new ObjectInputStream(new FileInputStream("D:/java/files/object.txt"));
            for (int i=0;i<2;i++){
                People object = (People) objectreader.readObject();
                System.out.println(object.getName());
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }finally {
            try {
                objectreader.close();
                objectwriter.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}



























