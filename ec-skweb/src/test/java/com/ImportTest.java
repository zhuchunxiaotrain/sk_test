package com;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import test.SpringTransactionalTestCase;

import java.io.File;

/**
 * Created by amin on 2015/3/17.
 */
@TransactionConfiguration(defaultRollback=true)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class ImportTest extends SpringTransactionalTestCase {

    @Test
    public void delete(){
        String path = "E:/git/ec/ec-web/src/main/webapp/resource/excel/项目信息数据.xls";
        File file = new File(path);
        FileUtils.deleteQuietly(file);
    }
}
