package com;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by dqf on 2016/3/8.
 */
public class DateL {

    public static int counter(String start, String end) throws ParseException {
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Date start_date = dateFormat.parse(start);
        Date end_date = dateFormat.parse(end);

        int days = (int) ((end_date.getTime() - start_date.getTime())/(1000*3600*24) + 1);
        int weeks = days/7;
        int lest = days%7;

        int count = weeks * 5;

        if (lest > 0){
            Calendar calendar=Calendar.getInstance();
            calendar.setTime(end_date);
            calendar.add(Calendar.DATE, 1 - lest);
            int day_of_week = calendar.get(Calendar.DAY_OF_WEEK);
            day_of_week = day_of_week-1>0 ? day_of_week-1:7;

            int others = day_of_week + lest;
            switch (others){
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6: others=0;break;
                case 7: others=1;break;
                default: others=2;break;
            }
            count += (lest-others);
        }
        return count;
    }

    public static void main(String[] args) throws ParseException {
        System.out.println(counter("2016-04-04","2016-04-04"));
    }
}
