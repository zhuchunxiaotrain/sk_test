package com;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.service.memcached.SpyMemcachedClient;
import com.fz.us.base.util.PinyinUtil;
import com.fz.us.dict.service.DictService;

import com.joint.base.bean.EnumManage;
import com.joint.base.bean.SystemConfig;

import com.joint.base.entity.Duty;
import com.joint.base.entity.Users;
import com.joint.base.entity.system.Admin;
import com.joint.base.service.AdminService;
import com.joint.base.service.CompanyService;
import com.joint.base.service.DutyService;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import test.SpringContextTestCase;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by amin on 2015/4/20.
 */
@TransactionConfiguration(defaultRollback=false)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class UsersTest extends SpringContextTestCase {
    @Resource
    UsersService usersService;
    @Resource
    AdminService adminService;
    @Resource
    private DutyService dutyService;
    @Resource
    private SpyMemcachedClient spyMemcachedClient;
    @Resource
    private DictService dictService;
    @Resource
    private CompanyService companyService;
    @Resource
    private SystemConfig systemConfig;

    @Test
    public void update(){
        String salt = new SecureRandomNumberGenerator().nextBytes().toHex();
        String password="pass";
        Admin admin=adminService.getByMobile("15370036857");
        admin.setPassword(usersService.encodedPassword(admin.getUsermobile(),password,salt));
        admin.setSalt(salt);
        Users users=usersService.getByAdmin(admin);
        users.setPassword(password);
        adminService.update(admin);
        usersService.update(users);
    }

    @Test
    public void register(){
        usersService.registerCompanyAndUser("15370036857", "pass");
    }

    @Test
    public void updateData(){
        List<Users> usersList = usersService.getAll();
        for(Users users : usersList){
            users.setPinYin(PinyinUtil.getPingYin(users.getName()));
            users.setPinYinHead(PinyinUtil.getPinYinHeadChar(users.getName()));
            usersService.updateAndEnable(users);
        }
    }

    @Test
    public void updateDuty(){
        List<Duty> dutyList= dutyService.getAll();
        for(Duty duty : dutyList){
            if(duty.getDutyState()!=null) continue;
            duty.setDutyState(EnumManage.DutyState.Default);
            dutyService.update(duty);
        }
    }

    @Test
    public void testdate(){
        Date date=new Date();
        Users users=usersService.get("8a8a8b9a4fd4f69f014fd4fe06750004");
        int day= DataUtil.DateDiff(date, users.getCreateDate());
        Pager pager= usersService.findByPagerAndStates(new Pager(0), new BaseEnum.StateEnum[]{BaseEnum.StateEnum.Enable});
        System.out.print("人数"+pager.getList().size());
    }



    @Test
    public void testDateDiff(){
        Date data1 = DataUtil.StringToDate("2016-10-20", "yyyy-MM");
        Date data2 = DataUtil.StringToDate("2016-10-10", "yyyy-MM");
        System.out.println("相差：" + DataUtil.DateDiff(data2, data1));
    }

    @Test
    public void compare() {
        String  date[] = {"2016-10-20", "2016-10-10", "2016-10-21","2015-09-20","2015-09-19"};
        String  amount[] = {"50","51","52","53","54"};
        for (int i = 0; i < date.length - 1; i++) {    //最多做n-1趟排序
            for (int j = 0; j < date.length - i - 1; j++) {    //对当前无序区间score[0......length-i-1]进行排序(j的范围很关键，这个范围是在逐步缩小的)
                if (date[j].compareTo(date[j + 1]) > 0) {    //把小的值交换到后面
                    String temp = date[j];
                    date[j] = date[j + 1];
                    date[j + 1] = temp;
                    String temp2 = amount[j];
                    amount[j] = amount[j + 1];
                    amount[j + 1] = temp2;
                }
            }
        }
        for (int a = 0; a < date.length; a++) {
            System.out.print(date[a] + "\t");
        }
        System.out.print("\n");
        for (int b = 0; b < amount.length; b++) {
            System.out.print(amount[b] + "\t");
        }

    }
}
