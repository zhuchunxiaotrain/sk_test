package manage;

import com.alibaba.druid.pool.DruidDataSource;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.*;

/**
 * Created by dqf on 2015/9/6.
 */
public class JDBCTest {
    private static final String MYSQL_DB_JNDINAME = "java:comp/env/jdbc/MysqlDataSource";
    private static final String ORACLE_DB_JNDINAME = "java:comp/env/jdbc/OracleDataSource";
    private static final String SQLSERVER_DB_JNDINAME = "java:comp/env/jdbc/SqlServerDataSource";

    private static DruidDataSource dsOracle = null;
    private static DruidDataSource dsMySql = null;
    private static DruidDataSource dsSqlServer = null;

    public static void source() {
        try {
            Context ctx = new InitialContext();
            dsMySql = (DruidDataSource) ctx.lookup(MYSQL_DB_JNDINAME);
        } catch (NamingException  e){
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception{
        Connection conn = null;
        String sql;

        String url = "jdbc:mysql://localhost:3306/ec?"+
                "user=root&password=root&useUnicode=true&characterEncoding=UTF8";

        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("成功加载MySQL驱动程序");
            conn = DriverManager.getConnection(url);

            Statement stmt = conn.createStatement();
            sql = "create table student(NO char(20),name varchar(20),primary key(NO))";
            int result = stmt.executeUpdate(sql);
            if (result != -1){
                System.out.println("创建数据库成功!");
                sql = "insert into student(NO,name) values('2012001','陶伟基')";
                result = stmt.executeUpdate(sql);
                sql = "insert into student(NO,name) values('2012002','周小俊')";
                result = stmt.executeUpdate(sql);
                sql = "select * from student";
                ResultSet rs = stmt.executeQuery(sql);// executeQuery会返回结果的集合，否则返回空值
                System.out.println("学号\t姓名");
                while (rs.next()) {
                    System.out
                            .println(rs.getString(1) + "\t" + rs.getString(2));// 入如果返回的是int类型可以用getInt()
                }
            }

        }catch (SQLException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            conn.close();
        }
    }
}

































