package mp;

import com.fz.us.base.service.memcached.SpyMemcachedClient;
import com.fz.us.base.util.LogUtil;
import com.joint.base.bean.SystemConfig;
import com.joint.base.mp.*;
import com.joint.base.service.FileManageService;
import com.joint.base.service.UsersService;
import fz.me.chanjar.weixin.common.api.WxConsts;
import fz.me.chanjar.weixin.common.bean.WxMenu;
import fz.me.chanjar.weixin.common.exception.WxErrorException;
import fz.me.chanjar.weixin.mp.bean.WxMpCustomMessage;
import fz.me.chanjar.weixin.mp.bean.WxMpXmlMessage;
import fz.me.chanjar.weixin.mp.bean.WxMpXmlOutMessage;
import fz.me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import test.SpringTransactionalTestCase;

import javax.annotation.Resource;

/**
 * Created with us-parent -> com.fz.us.core.action.
 * User: min_xu
 * Date: 2014/9/11
 * Time: 14:08
 * 说明：
 * 身份验证失败请捕获AuthenticationException 或其子类，常见的如：
 DisabledAccountException（禁用的帐号）、
 LockedAccountException（锁定的帐号）、
 UnknownAccountException（错误的帐号）、
 ExcessiveAttemptsException（登录失败次数过多）、
 IncorrectCredentialsException （错误的凭证）、
 ExpiredCredentialsException（过期的凭证）等，
 具体请查看其继承关系；对于页面的错误消息展示，最好使用如“用户名/密码
 错误”而不是“用户名错误”/“密码错误”，防止一些恶意用户非法扫描帐号库；

 */
@TransactionConfiguration(defaultRollback=false)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class WechatTest extends SpringTransactionalTestCase {
    private String openId = "okEz0jqzrskD0ea5cp0Z_q2UDI5k";

    @Resource
    private SystemConfig systemConfig;
    @Resource
    private WxEchoMpServiceImpl wxMpService;
    @Resource
    private WxMpInCacheConfigStorage wxMpConfigStorage;
    @Resource
    private SpyMemcachedClient spyMemcachedClient;
    @Resource
    private WxEchoMpMessageRouter wxMpMessageRouter;
    @Resource
    private FileManageService fileManageService;
    @Resource
    private UsersService usersService;

    @Test
    public void accessTokenClean(){
        wxMpConfigStorage.deleteTokenFromCache();
    }

    @Test
    public void accessTokenTest() throws WxErrorException {
        String t = wxMpConfigStorage.getTokenFromCache();
        LogUtil.info(t);

        if(StringUtils.isEmpty(t)){
            wxMpService.accessTokenRefresh();
        }
        WxMpCustomMessage message = new WxMpCustomMessage();
        message.setMsgType(WxConsts.CUSTOM_MSG_TEXT);
        message.setToUser(openId);
        message.setContent("欢迎欢迎，热烈欢迎\n换行测试\n超链接:<a href=\"http://www.baidu.com\">Hello World</a>");
        wxMpService.customMessageSend(message);

        WxMpUser wxUser = wxMpService.userInfo(openId,null);
        System.out.println(wxUser.getNickname());

    }

    @Test
    public void createticket() throws WxErrorException {
//        String t = wxMpConfigStorage.getTokenFromCache();
//        LogUtil.info(t);
//
//        if(StringUtils.isEmpty(t)){
//            wxMpService.accessTokenRefresh();
//        }
//
//        InputStream is = null;
//        String content=null;
//
//        String url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token="
//                + t;
//        String postData="{\"action_name\": \"QR_LIMIT_SCENE\", \"action_info\": {\"scene\": {\"scene_id\": 123}}}";
//
//        content= wxMpService.post(url,postData);
//        content="{\"ticket\":\"gQGb8DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xLzhFd0RPRHZsdXh6QzFZdTlXR0FUAAIELcXoVgMEAAAAAA==\",\"url\":\"http:\\/\\/weixin.qq.com\\/q\\/8EwDODvluxzC1Yu9WGAT\"}";
//
//        JSONObject jsonObject=JSONObject.fromObject(content);

//        Users users=usersService.getUsersByMobile("18301785732");
//        Company company=usersService.getCompanyByUser(users);
//        WxMpQrCodeTicket wxMpQrCodeTicket= wxMpService.qrCodeCreateLastTicket(123);
//        File file= wxMpService.qrCodePicture(wxMpQrCodeTicket);
//        try {
//            fileManageService.gridFSSave(new FileInputStream(file),"APPLICATION/OCTET-STREAM",file.getName(),company,users);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//

        String s= String.valueOf(System.nanoTime());
        System.out.println("时间戳1"+s);
        System.out.println("时间戳2"+s.substring(6,s.length()));
    }


    /**
     * 设置微信公众号的菜单
     * yMap.put("type", "view");
     yMap.put("name", "首页");
     yMap.put("url", "http://"+host+"/smartsales/mp/homepage!index.action");
     dataRows.add(JSONObject.fromObject(yMap));

     yMap = new HashMap<String, Object>();
     yMap.put("type", "view");
     yMap.put("name", "工作日历");
     yMap.put("url", "http://"+host+"/smartsales/mp/calendar!list.action");
     dataRows.add(JSONObject.fromObject(yMap));
     List<JSONObject> subRows = new ArrayList<JSONObject>();
     Map<String, Object> xMap = new HashMap<String, Object>();
    xMap = new HashMap<String, Object>();
    xMap.put("type", "click");//view
    xMap.put("name", "账号");
    xMap.put("key", "account");
    subRows.add(JSONObject.fromObject(xMap));
    xMap = new HashMap<String, Object>();
    xMap.put("type", "click");
    xMap.put("name", "动态");
    xMap.put("key", "active");
    subRows.add(JSONObject.fromObject(xMap));
    xMap = new HashMap<String, Object>();
    xMap.put("type", "click");
    xMap.put("name", "反馈");
    xMap.put("key", "feedback");
    subRows.add(JSONObject.fromObject(xMap));
    xMap = new HashMap<String, Object>();
    xMap.put("type", "click");
    xMap.put("name", "版本");
    xMap.put("key", "version");
    subRows.add(JSONObject.fromObject(xMap));
    xMap = new HashMap<String, Object>();
    xMap.put("type", "click");
    xMap.put("name", "下载APP");
    xMap.put("key", "download");
    subRows.add(JSONObject.fromObject(xMap));
    yMap = new HashMap<String, Object>();
    yMap.put("name", "我的");
    yMap.put("sub_button", subRows);
    dataRows.add(JSONObject.fromObject(yMap));

     * */
    @Test
    public void setAppMenu(){
        String base = wxMpConfigStorage.getHttpHost()+"/"+systemConfig.getWebroot();
      //  System.out.println("base"+base);
        WxMenu menu = new WxMenu();
        WxMenu.WxMenuButton button1 = new WxMenu.WxMenuButton();
        button1.setType(WxConsts.BUTTON_VIEW);
        button1.setName("个人中心");
        button1.setUrl( base +"/mp/index.action");

        WxMenu.WxMenuButton button3 = new WxMenu.WxMenuButton();
        button3.setName("我的1");

        menu.getButtons().add(button1);
        menu.getButtons().add(button3);

        WxMenu.WxMenuButton button31 = new WxMenu.WxMenuButton();
        button31.setType(WxConsts.BUTTON_CLICK);
        button31.setName("账号");
        button31.setKey(WxBean.EventKeyEnum.account.name());

//        WxMenu.WxMenuButton button32 = new WxMenu.WxMenuButton();
//        button32.setType(WxConsts.BUTTON_CLICK);
//        button32.setName("动态");
//        button32.setKey(WxBean.EventKeyEnum.active.name());

        WxMenu.WxMenuButton button33 = new WxMenu.WxMenuButton();
        button33.setType(WxConsts.BUTTON_CLICK);
        button33.setName("反馈");
        button33.setKey(WxBean.EventKeyEnum.feedback.name());

        WxMenu.WxMenuButton button34 = new WxMenu.WxMenuButton();
        button34.setType(WxConsts.BUTTON_CLICK);
        button34.setName("版本");
        button34.setKey(WxBean.EventKeyEnum.version.name());

//        WxMenu.WxMenuButton button35 = new WxMenu.WxMenuButton();
//        button35.setType(WxConsts.BUTTON_CLICK);
//        button35.setName("下载App");
//        button35.setKey(WxBean.EventKeyEnum.download.name());

        button3.getSubButtons().add(button31);
//        button3.getSubButtons().add(button32);
        button3.getSubButtons().add(button33);
        button3.getSubButtons().add(button34);
//        button3.getSubButtons().add(button35);
        LogUtil.info(menu.toJson());
        try {
            wxMpService.menuCreate(menu);
        }catch (WxErrorException e){
            e.printStackTrace();
        }
    }



    @Test
    public void messageIn2OutTest(){
        //Object[] objects = (Object[]) messages2()[0];

        WxMpXmlMessage msg = new WxMpXmlMessage();
        msg.setMsgType(WxConsts.XML_MSG_EVENT);
        msg.setEvent(WxConsts.EVT_LOCATION);
        StringBuffer sb = new StringBuffer();
        WxMpXmlOutMessage outMessage = wxMpMessageRouter.route(msg);
        //Assert.assertEquals(sb.toString(), (String)objects[1]);

        /*if (outMessage != null) {
            if ("raw".equals(encryptType)) {
                response.getWriter().write(outMessage.toXml());
            } else if ("aes".equals(encryptType)) {
                response.getWriter().write(outMessage.toEncryptedXml(wxMpConfigStorage));
            }
            return;
        }*/
        System.out.println(outMessage.toXml());

    }

    @Resource
    private WxEchoMpMessageHandler wxEchoMpMessageHandler;
    @Test
    public void feedback(){
        String feedback = wxEchoMpMessageHandler.getFeedback();
        LogUtil.info(feedback);
    }

    public Object[][] messages2() {
        WxMpXmlMessage message1 = new WxMpXmlMessage();
        message1.setMsgType(WxConsts.XML_MSG_TEXT);

        WxMpXmlMessage message2 = new WxMpXmlMessage();
        message2.setEvent(WxConsts.EVT_CLICK);

        WxMpXmlMessage message3 = new WxMpXmlMessage();
        message3.setEventKey("KEY_1");

        WxMpXmlMessage message4 = new WxMpXmlMessage();
        message4.setContent("CONTENT_1");

        WxMpXmlMessage message5 = new WxMpXmlMessage();
        message5.setContent("BLA");

        WxMpXmlMessage message6 =  new WxMpXmlMessage();
        message6.setContent("abcd");

        WxMpXmlMessage c2 = new WxMpXmlMessage();
        c2.setMsgType(WxConsts.XML_MSG_TEXT);
        c2.setEvent(WxConsts.EVT_CLICK);

        WxMpXmlMessage c3 = new WxMpXmlMessage();
        c3.setMsgType(WxConsts.XML_MSG_TEXT);
        c3.setEvent(WxConsts.EVT_CLICK);
        c3.setEventKey("KEY_1");

        WxMpXmlMessage c4 = new WxMpXmlMessage();
        c4.setMsgType(WxConsts.XML_MSG_TEXT);
        c4.setEvent(WxConsts.EVT_CLICK);
        c4.setEventKey("KEY_1");
        c4.setContent("CONTENT_1");

        return new Object[][] {
                new Object[] { message1, WxConsts.XML_MSG_TEXT + "," },
                new Object[] { message2, WxConsts.EVT_CLICK + "," },
                new Object[] { message3, "KEY_1," },
                new Object[] { message4, "CONTENT_1," },
                new Object[] { message5, "ALL," },
                new Object[] { message6, "abcd," },
                new Object[] { c2, "COMBINE_2," },
                new Object[] { c3, "COMBINE_3," },
                new Object[] { c4, "COMBINE_4," }
        };

    }



}