package com.joint.web.quartz;


import com.fz.us.base.util.LogUtil;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.Car;
import com.joint.core.entity.manage.Dispatch;
import com.joint.core.entity.manage.Notice;
import com.joint.core.entity.manage.Qualifications;
import com.joint.core.service.CarService;
import com.joint.core.service.DispatchService;
import com.joint.core.service.NoticeService;
import com.joint.core.service.QualificationsService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class JobImpl implements Job {
    @Resource
    private NoticeService noticeService;
    @Resource
    private DispatchService dispatchService;
    @Resource
    private QualificationsService qualificationsService;
    @Resource
    private CarService carService;

    @Override
    public void calNoticeValidate() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("invalid", false);
        List<Notice> noticeList = noticeService.getList(map);
        LogUtil.info("noticeListSize:" + noticeList.size());
        Date today = new Date();
        for(Notice notice:noticeList){
            if(notice.isHasValid() && notice.getValidDate() != null){
                LogUtil.info("do");
                if(today.after(DataUtil.getAfterDay(notice.getValidDate(),1))){
                    notice.setInvalid(true);
                    noticeService.update(notice);
                }
            }

        }
    }

    @Override
    public void calDispatchValidate() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("invalid", false);
        List<Dispatch> dispatchList = dispatchService.getList(map);
        Date today = new Date();
        for(Dispatch dispatch:dispatchList ){
            if(dispatch.isHasValid() && dispatch.getValidDate() != null){
               // LogUtil.info("do");
                if(today.after(DataUtil.getAfterDay(dispatch.getValidDate(),1))){
                    dispatch.setInvalid(true);
                    dispatchService.update(dispatch);
                }
            }

        }
    }

    @Override
    public void calQualificationsValidate() {
        Map<String, Object> map1 = new HashMap<String, Object>();
        map1.put("step", "1");
        List<Qualifications> qualificationsList1 = qualificationsService.getList(map1);
        Date today = new Date();
        for(Qualifications qualifications:qualificationsList1){
            Date changeDay=new Date(qualifications.getUseDate().getTime()-(long)90 * 24 * 60 * 60 * 1000);
            if(qualifications.getRemindDate()!=null){
                if(today.after(DataUtil.getAfterDay(qualifications.getRemindDate(),1))){
                    qualifications.setStep("2");
                    qualificationsService.update(qualifications);
                }
            }else{
                if(today.after(DataUtil.getAfterDay(changeDay,1))){
                    qualifications.setStep("2");
                    qualificationsService.update(qualifications);
                }
            }
        }
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("step", "2");
        map2.put("invalid", false);
        List<Qualifications> qualificationsList2 = qualificationsService.getList(map2);
        for(Qualifications qualifications:qualificationsList2){
            if(qualifications.getUseDate()!=null){
                if(today.after(DataUtil.getAfterDay(qualifications.getUseDate(),1))){
                    qualifications.setStep("3");
                    qualificationsService.update(qualifications);
                }
            }
        }
    }

    @Override
    public void calCarValidate() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("step", "2");
        List<Car> carList = carService.getList(map);
        Date today = new Date();
        for(Car car:carList){
            if(car.getDateEnd()!=null){
                if(today.after(DataUtil.getAfterDay(car.getDateEnd(),1))){
                    car.setStep("3");
                    carService.update(car);
                }
            }
        }
    }
}
