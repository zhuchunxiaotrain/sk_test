package com.joint.web.quartz;

public interface Job {

    /**
     * 计算公告通知是否失效
     */
    public void calNoticeValidate();

    /**
     * 计算发文登记是否失效
     */
    public void calDispatchValidate();

    /**
     * 资质登记表使用期限前90天转到到期提醒,并且判断超过使用期限转为失效
     */
    public void calQualificationsValidate();

    /**
     * 判断车辆使用转到已使用状态
     */
    public void calCarValidate();
}