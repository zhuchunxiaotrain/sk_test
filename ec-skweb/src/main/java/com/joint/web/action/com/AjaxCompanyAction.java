package com.joint.web.action.com;

import com.joint.base.entity.Company;
import com.joint.base.entity.system.Admin;
import com.joint.base.service.AdminService;
import com.joint.base.service.CompanyService;
import com.joint.base.util.StringUtils;
import com.joint.web.action.BaseAdminAction;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;

/**
 * Created by amin on 2015/3/2.
 */
@ParentPackage("com")
public class AjaxCompanyAction extends BaseAdminAction {
    @Resource
    private CompanyService companyService;
    @Resource
    private AdminService adminService;

    private String value;
    private String name;
    private String province;
    private String city;
    private String county;
    private String adminId;

    public String accountEdit(){
        Company company=null;
        Admin admin=null;
        if(StringUtils.isNotEmpty(keyId)){
            System.out.print("");
            company=companyService.get(keyId);
        }
        if(StringUtils.isNotEmpty(adminId)){
            admin=adminService.get(adminId);
        }
        if("name".equals(name)){
            company.setName(value);
        }
        if("subname".equals(name)){
            company.setSubname(value);
        }
        if("area".equals(name)){
            company.setProvince(province);
            company.setCity(city);
            company.setCounty(county);
        }
        if("manager".equals(name)){
            admin.setUsername(value);
        }
        if("mphone".equals(name)){
            admin.setUsermobile(value);
        }
        if(StringUtils.isNotEmpty(keyId)){
            companyService.update(company);
        }
        if(StringUtils.isNotEmpty(adminId)){
            adminService.update(admin);
        }
        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
