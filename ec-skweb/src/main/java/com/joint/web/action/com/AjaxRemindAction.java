package com.joint.web.action.com;


import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.joint.base.entity.Remind;
import com.joint.base.entity.TaskRecord;
import com.joint.base.entity.Users;
import com.joint.base.service.RemindService;
import com.joint.base.service.TaskFlowService;
import com.joint.base.service.TaskRecordService;
import com.joint.base.service.UsersService;
import com.joint.base.service.activiti.WorkflowService;
import com.joint.base.util.DataUtil;
import com.joint.web.action.BaseAdminAction;
import net.sf.json.JSONObject;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.*;


@ParentPackage("com")
public class AjaxRemindAction extends BaseAdminAction {

    @Resource
    private UsersService usersService;
    @Resource
    private RemindService remindService;
    @Resource
    private WorkflowService workflowService;
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private HistoryService historyService;
    @Resource
    private TaskService taskService;
    @Resource
    private TaskFlowService taskFlowService;

    protected Logger logger = LoggerFactory.getLogger(getClass());


    /**
     * 返回视图
     * @return
     */
    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
       // Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("users", user);

        pager= remindService.findByPager(pager,  params);
        List<Remind> remindList = (List<Remind>) pager.getList();
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        String[] manage ={"notice","schedule","dispatch","receive","meetingleave","meetingsign","meetinguse","registe","meetingrecord","taskcontrol","taskback",
                "qualifications","seal","worklog","car","oil","oilquery","contract","purchase","assetuse","usedetail","assetscrap","reception","carconfig","qualificationsdict"};
        String[] party ={"strongbig","selectionnotice","selectionapply",
                "selectionspeech","selectionpublicity","selectionissued","activitymanager",
                "moneymanager","moneyManagerdetail","moneyavg","moneyavgdetail","letterregister",
                "annualbudget","annualbudgetdetail","rebateactivity","rebateactivitydetail","partymoney"};
        String[] finance ={"finance","calculationyear","modifyyear","newsmonth","financeyear",
                "financialanalysis","workplan","auditnotice","auditfirst","auditlast","auditedit","auditanswer",
                "expend"};
        for(Remind remind: remindList){
            rMap = new HashMap<String, Object>();
            String bussinessKey =  remind.getBussinessKey();
            String bussinessId =  remind.getKeyId();
            String url = "";
            String model = "";
            if(StringUtils.isNotEmpty(bussinessKey)){
                String key =  bussinessKey.toLowerCase().substring(bussinessKey.lastIndexOf(".")+1);
                ProcessDefinition processDefinition = workflowService.getProDefinitionByKey(key);
                if(Arrays.asList(manage).contains(processDefinition.getKey())){
                    model = "manage";
                }else if(Arrays.asList(party).contains(processDefinition.getKey())){
                    model = "party";
                }else if(Arrays.asList(finance).contains(processDefinition.getKey())){
                    model = "finance";
                }else{
                    model = "com";
                }
                url =  "../" + model + "/ajax-" + processDefinition.getKey() + "!read.action?keyId=" +bussinessId + "&remind=1" ;
            }
            rMap.put("content",remind.getContent());
            rMap.put("id", remind.getId());
            rMap.put("createDate", DataUtil.DateToString(remind.getCreateDate(), "yyyy-MM-dd"));
            rMap.put("url",url);
            LogUtil.info("-----"+url);
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);

        }


        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


}
