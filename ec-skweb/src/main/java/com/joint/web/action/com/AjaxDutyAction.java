package com.joint.web.action.com;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.BaseEnum.OrderType;
import com.fz.us.base.bean.BaseEnum.StateEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.joint.base.bean.EnumManage;
import com.joint.base.entity.*;
import com.joint.base.service.DepartmentService;
import com.joint.base.service.DutyService;
import com.joint.base.service.PostService;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.web.action.BaseAdminAction;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hpj on 2014/9/16.
 */
@ParentPackage("com")
public class AjaxDutyAction extends BaseAdminAction {

    @Resource
    private UsersService usersService;
    @Resource
    private PostService postService;
    @Resource
    private DutyService dutyService;
    @Resource
    private DepartmentService departmentService;

    protected Users user;
    protected Post post;
    protected List<Post> postList;
    private  List<Duty> dutyList;

    private String no;
    private String name;
    private String description;

    //添加@coptrigth Liu Liang
    private String  departId;//部门的id
    private Department department;

    /**
     * 查询指定部门下的所有员工的个数
     */
    public String count(){
        Company company=usersService.getCompanyByUser();
        pager = new Pager();
        pager.setPageNumber(page);
        pager.setPageSize(rows);
        pager.setOrderBy("id");
        pager.setOrderType(OrderType.asc);

        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        LogUtil.info("departid:"+departId);
        if(departId!=null){
            department=departmentService.get(departId);
        }
        params.put("department",department);
        params.put("state", BaseEnum.StateEnum.Enable);
        pager=dutyService.findByPager(pager,params);

       /* pager = dutyService.findByPagerAndCompany(pager, null, company, new StateEnum[]{StateEnum.Enable});*/
        dutyList= (List<Duty>) pager.getList();
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        int count=dutyList.size();
        rMap=new HashMap<String, Object>();
        rMap.put("count",count);
        LogUtil.info("count_action:"+count);
        dataRows.add(JSONObject.fromObject(rMap));
       /* for(Duty duty:dutyList){
            rMap=new HashMap<String, Object>();
            if(duty.getUsers().getId()){

            }
            rMap.put("id",duty.getId());
            rMap.put("departId",duty.getDepartment().getId());
            int count=0;
            String dId=duty.getDepartment().getId();
            if(departId.equals(dId)){
                count++;
            }
            rMap.put("count",count);
            rMap.put("createDate",  DataUtil.DateTimeToString(duty.getCreateDate()));
            dataRows.add(JSONObject.fromObject(rMap));
        }*/
        /*long total =pager.getTotalCount();
        if(total%rows==0){
            total = total/rows;
        }else{
            if(total<rows){
                total = 1;
            }else{
                total = total/rows + 1;
            }
        }*/
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        data.put("total",total);
        data.put("count",count);
        data.put("records", pager.getTotalCount());
        return  ajaxJson(JSONObject.fromObject(data).toString());

    }

    /**
     * 返回职责视图
     * @return
     */
    public String list(){
        Company company=usersService.getCompanyByUser();
        pager = new Pager();
        pager.setPageNumber(page);
        pager.setPageSize(rows);
        pager.setOrderBy("id");
        pager.setOrderType(OrderType.asc);
        pager = dutyService.findByPagerAndCompany(pager, null, company, new StateEnum[]{StateEnum.Enable});
        dutyList= (List<Duty>) pager.getList();
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(Duty duty:dutyList){
            rMap=new HashMap<String, Object>();
            rMap.put("id",duty.getId());
            rMap.put("user",duty.getUsers().getName());
            rMap.put("post",duty.getPost().getName());
            rMap.put("depart",duty.getDepartment().getName());
            rMap.put("departId",duty.getDepartment().getId());
            rMap.put("createDate",  DataUtil.DateTimeToString(duty.getCreateDate()));
            dataRows.add(JSONObject.fromObject(rMap));
        }
        long total =pager.getTotalCount();
        if(total%rows==0){
            total = total/rows;
        }else{
            if(total<rows){
                total = 1;
            }else{
                total = total/rows + 1;
            }
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        data.put("total",total);
        data.put("records", pager.getTotalCount());
        return  ajaxJson(JSONObject.fromObject(data).toString());

    }

    /**
     * 删除职责信息
     * @return
     */
    public String delete(){
        if(StringUtils.isEmpty(keyId)){
            return ajaxHtmlCallback("400", "请选择员工！","操作状态");
        }
        Duty duty = dutyService.get(keyId);
        if("Principal".equals(duty.getDutyState())){
            duty.setDutyState(EnumManage.DutyState.Default);
        }
        else{
            duty.setDutyState(EnumManage.DutyState.Principal);
        }
        duty.setState(StateEnum.Delete);
        dutyService.update(duty);
        return ajaxHtmlCallback("200", "删除成功！","操作状态");
    }

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public List<Post> getPostList() {
        return postList;
    }

    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Duty> getDutyList() {
        return dutyList;
    }

    public void setDutyList(List<Duty> dutyList) {
        this.dutyList = dutyList;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }
}
