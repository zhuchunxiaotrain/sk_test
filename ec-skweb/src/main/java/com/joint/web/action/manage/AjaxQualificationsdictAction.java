package com.joint.web.action.manage;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Users;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.QualificationsDict;
import com.joint.core.service.QualificationsDictService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ZhuChunXiao on 2017/4/7.
 */
@ParentPackage("manage")
public class AjaxQualificationsdictAction extends BaseFlowAction {
    @Resource
    private QualificationsDictService qualificationsDictService;

    /**
     * 资质名称
     */
    private String name;
    /**
     * 资质号
     */
    private String no;

    private Users loginUser;
    private QualificationsDict qualificationsDict;

    public String execute(){
        return "qualificationsdict";
    }

    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        //所有单子
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
        pager=qualificationsDictService.findByPagerAndLimit(true, "qualificationsdict", pager, params);
        List<QualificationsDict> qualificationsDictList;
        if (pager.getTotalCount() > 0){
            qualificationsDictList = (List<QualificationsDict>) pager.getList();
        }else{
            qualificationsDictList= new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(QualificationsDict q: qualificationsDictList){
            rMap = new HashMap<String, Object>();
            rMap.put("id",q.getId());
            rMap.put("name",q.getName());
            rMap.put("no",q.getNo());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String read(){
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)) {
            qualificationsDict = qualificationsDictService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
        }
        return "read";
    }

    public String input(){
        if(StringUtils.isNotEmpty(keyId)){
            qualificationsDict=qualificationsDictService.get(keyId);
        }
        return "input";
    }

    public void setData(){
        if(StringUtils.isNotEmpty(keyId)){
            qualificationsDict=qualificationsDictService.get(keyId);
        }else{
            qualificationsDict=new QualificationsDict();
            qualificationsDict.setCreater(usersService.getLoginInfo());
        }
        qualificationsDict.setName(name);
        qualificationsDict.setNo(no);
        qualificationsDict.setCompany(usersService.getLoginInfo().getCompany());
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                qualificationsDictService.update(qualificationsDict);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                qualificationsDictService.save(qualificationsDict, "qualificationsdict", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        Company company = usersService.getCompanyByUser();
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("processState", new FlowEnum.ProcessState[]{FlowEnum.ProcessState.Finished,FlowEnum.ProcessState.Running});
        params.put("no", no);
        List<QualificationsDict> list = (List<QualificationsDict>)qualificationsDictService.findByPagerAndCompany(null, null, company, params).getList();
        if(list.size()>0){
            return ajaxHtmlCallback("400", "已经存在这个资质号了", "操作状态");
        }
        setData();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                qualificationsDictService.approve(qualificationsDict, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
            } else {
                keyId = qualificationsDictService.commit(qualificationsDict,FlowEnum.ProcessState.Finished, "qualificationsdict", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public QualificationsDict getQualificationsDict() {
        return qualificationsDict;
    }

    public void setQualificationsDict(QualificationsDict qualificationsDict) {
        this.qualificationsDict = qualificationsDict;
    }
}
