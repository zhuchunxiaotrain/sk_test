package com.joint.web.action.party;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.DepartmentService;
import com.joint.base.service.RoleService;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.*;
import com.joint.core.entity.ActivityManager;
import com.joint.core.entity.manage.MeetingUse;
import com.joint.core.service.*;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dqf on 2015/8/26.
 */
@ParentPackage("party")
public class AjaxActivitymanagerAction extends BaseFlowAction {
  
   
    @Resource
    private ActivityManagerService activityManagerService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private MoneyAvgDetailService moneyAvgDetailService;
    @Resource
    private SelectionNoticeService selectionNoticeService;
    @Resource
    private SelectionSpeechService selectionSpeechService;
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private RoleService roleService;
    @Resource
    private MeetingUseService meetingUseService;


    private ActivityManager activityManager;
    
    private SelectionSpeech selectionSpeech;




    private int numStatus;
    private Users loginUser;

    //前台传到后台的数据
    private String activityTypeId;
    private String activityManagerDepartId;
    private String checkUsersId;
    //相对于的集合进行保存对象
   /* private List<Dict> activityTypeList;
    private List<Dict> activityBranchList;
    private List<Users> checkUsersList;
*/

    //从后台传回前台的数据
    private String attchmentFileId;
    private String meetingUseId;



    /**
     * 活动类型
     */
    private List<Dict> activityType;
    /**
     * 是否需要党费
     */
    private  String requirdMoneyType;
    /**
     * 活动支部
     */
    private Department department;
    /**
     * 活动费用
     */
    private BigDecimal activityMoney;
    /**
     *可支配金额
     */
    private BigDecimal enableMoney;
    /**
     * 相应的会议记录(引用党政模块里的对象)
     */
    private List<MeetingUse> meetingUse;
    /**
     * 选择审核支部书记
     */
    private List<Users> checkUsers;

    private String viewtype;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "activitymanager";
    }




    public String listfordialog(){

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);

        pager=activityManagerService.findByPagerAndProcessState(pager, user, "activitymanager", FlowEnum.ProcessState.Finished, params);
        List<ActivityManager> proInfoList;
        if (pager.getTotalCount() > 0){
            proInfoList = (List<ActivityManager>) pager.getList();
        }else{
            proInfoList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(ActivityManager mproInfo:proInfoList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",mproInfo.getId());
            rMap.put("name",mproInfo.getName());
            //rMap.put("no",mproInfo.getProjectNo());
            rMap.put("create",mproInfo.getCreater().getName());

            String sAddr = "";

            // rMap.put("buildAddress" , mproInfo.getBuildAddress());

            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }


        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        //内嵌视图这里要加上
       /* if (StringUtils.isNotEmpty(parentId)){
            selectionNotice=selectionNoticeService.get(parentId);
        }*/




       /* params.put("selectionNotice",selectionNotice);*//*将类的属性，放入到集合中去，到查询数据库中就是查询条件，值就是你输入的值
                                                      如果是引用的别的类，只要叫类对象放入到集合中去看，到数据库中已ID的形式查询*/
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                pager=activityManagerService.findByPagerAndLimit(false, "activitymanager", pager, params);
            }else if(viewtype.equals("2")){
                //已归档
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                pager=activityManagerService.findByPagerAndFinish("activitymanager", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=activityManagerService.findByPagerAndLimit(true, "activitymanager", pager, params);
        }

        List<ActivityManager> activityManagersList;
        if (pager.getTotalCount() > 0){

            activityManagersList = (List<ActivityManager>) pager.getList();
        }else{
            activityManagersList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(ActivityManager activityManager:activityManagersList){
            rMap=new HashMap<>();
            rMap.put("id",activityManager.getId()!=null?activityManager.getId():"");
            if(activityManager.getCreateDate()!=null){
                String year=new SimpleDateFormat("yyyy").format(activityManager.getCreateDate());
                String month=new SimpleDateFormat("MM").format(activityManager.getCreateDate());
                rMap.put("year",year);
                rMap.put("month",month);
            }else{
                rMap.put("year","");
                rMap.put("month","");
            }
            String actType="";
            if(activityManager.getActivityType()!=null){
                for(Dict dict:activityManager.getActivityType()){
                    actType+=dict.getName()+",";
                }
                rMap.put("activityType",actType);
            }else {
                rMap.put("activityType","你没有输入活动类型");
            }
            rMap.put("department",activityManager.getDepartment()!=null?activityManager.getDepartment().getName():"");


        rMap.put("activityMoney",activityManager.getActivityMoney()!=null?activityManager.getActivityMoney():0);
        if(activityManager.getProcessState().name()!=null){
                if(activityManager.getProcessState().name().equals("Finished")){
                    rMap.put("state","已通过");
                }else if(activityManager.getProcessState().name().equals("Deny")){
                    rMap.put("state","被否决");
                }else if(activityManager.getProcessState().name().equals("Running")){
                    rMap.put("state","流转中");
                }else if(activityManager.getProcessState().name().equals("Draft")){
                    rMap.put("state","保存");
                } else if(activityManager.getProcessState().name().equals("Backed")) {
                    rMap.put("state", "被退回");
                }else {
                    rMap.put("state", "");
                }
         }
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }
    public Map getSearchFilterParams(String _search,Map<String,Object> params,String filters){
        //createAlias Key集合
        List<String> alias = new ArrayList<String>();
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(_search)){
            boolean __search = Boolean.parseBoolean(_search);
            if(__search&& org.apache.commons.lang3.StringUtils.isNotEmpty(filters)){
                JSONObject filtersJson = JSONObject.fromObject(filters);
                if(org.apache.commons.lang3.StringUtils.equals(filtersJson.getString("groupOp"), "AND")){
                    //查询条件
                    JSONArray rulesJsons = filtersJson.getJSONArray("rules");
                    //遍历条件
                    for(int i=0;i<rulesJsons.size();i++){
                        JSONObject rule = rulesJsons.getJSONObject(i);
                        if(rule.has("field")&& org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("field"))&&rule.has("data")&& org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                            String field = rule.getString("field");
                            if(field.contains("_")){
                                String[] fields = field.split("_");
                                if(org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                                    params.put(field.replaceAll("_", "."), rule.getString("data"));
                                }
                            }else{
                                if(org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                                    switch (field){
                                        case "activityMoney":
                                            params.put("activityMoney", new BigDecimal(rule.getString("data")));
                                            break;
                                        default:
                                            params.put(field, rule.getString("data"));
                                    }


                                    /*if(field.equals("createDate")){
                                        params.put("createDate", DataUtil.StringToDate(rule.getString("data")));
                                    }
                                    else {
                                        params.put(field, rule.getString("data"));
                                    }*/
                                }
                            }
                        }
                    }

                }
            }
        }

        return params;
    }
    public String read(){

        activityType = Lists.newArrayList();
        checkUsers=Lists.newArrayList();
        loginUser = usersService.getLoginInfo();
        department=new Department();
        meetingUse=Lists.newArrayList();
        if(StringUtils.isNotEmpty( keyId)) {
            Company company = usersService.getCompanyByUser();
            activityManager = activityManagerService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);

            //活动类型
            if(activityManager.getActivityType()!=null){
                for (Dict d: activityManager.getActivityType()){
                    activityType.add(d);
                }
            }
            //活动支部
            if(activityManager.getDepartment()!=null){
                department=activityManager.getDepartment();
            }
            //选择审核支部书记
            if(activityManager.getCheckUsers()!=null){
                for (Users users:activityManager.getCheckUsers()){
                    checkUsers.add(users);
                }
            }
            //相应会议对象
            if(activityManager.getMeetingUse()!=null){
                meetingUse=activityManager.getMeetingUse();
            }

        }
        return "read";
    }

    public String input(){
        activityType = Lists.newArrayList();
        department=new Department();
        checkUsers=Lists.newArrayList();
        meetingUse=Lists.newArrayList();
        if (StringUtils.isNotEmpty(keyId)){
            activityManager = activityManagerService.get(keyId);
            loginUser = usersService.getLoginInfo();
            Company company = usersService.getCompanyByUser();
            numStatus = workflowService.getNumStatus(keyId, loginUser);

            //活动类型
            if(activityManager.getActivityType()!=null){
                for (Dict d: activityManager.getActivityType()){
                    activityType.add(d);
                }
            }
            //活动支部
            if(activityManager.getDepartment()!=null){
                department=activityManager.getDepartment();
            }
            //选择审核支部书记
            if(activityManager.getCheckUsers()!=null){
                for (Users users:activityManager.getCheckUsers()){
                    checkUsers.add(users);
                }
            }
            //相应会议对象
            if(activityManager.getMeetingUse()!=null){
                meetingUse=activityManager.getMeetingUse();
            }


        } else {
            loginUser=usersService.getLoginInfo();
            loginUser.getDutySet();

        }



        return "input";
    }

    private void setData(){
      activityType=Lists.newArrayList();
      department=new Department();
      checkUsers=Lists.newArrayList();
      meetingUse=Lists.newArrayList();
        if(StringUtils.isNotEmpty(keyId)){
            activityManager = activityManagerService.get(keyId);


        }else{
            activityManager = new ActivityManager();
            activityManager.setCreater(usersService.getLoginInfo());

        }


        if(StringUtils.isNotEmpty(activityTypeId)){
            for (String s:activityTypeId.split(",")){
                activityType.add(dictService.get(s.trim()));

            }
        }
        if(StringUtils.isNotEmpty(activityManagerDepartId)){
           department=departmentService.get(activityManagerDepartId);
        }
        if(StringUtils.isNotEmpty(checkUsersId)){
            for (String s:checkUsersId.split(",")){
                checkUsers.add(usersService.get(s.trim()));
            }
        }
        //相应会议对象
        if(StringUtils.isNotEmpty(meetingUseId)){
            for (String s:meetingUseId.split(",")){
                meetingUse.add(meetingUseService.get(s.trim()));
            }
        }
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Date date=new Date();
        activityManager.setCreateDate(date);
        activityManager.setYear(new SimpleDateFormat("yyyy").format(date));
        activityManager.setMonth(new SimpleDateFormat("MM").format(date));
        activityManager.setCompany(usersService.getLoginInfo().getCompany());
        activityManager.setActivityType(activityType);
        activityManager.setDepartment(department);
        activityManager.setRequirdMoneyType(requirdMoneyType);
        if(activityMoney!=null){
            activityManager.setActivityMoney(activityMoney);
        }else{
            BigDecimal bigDecimal=new BigDecimal(0);
            activityManager.setActivityMoney(bigDecimal);
        }

        if(enableMoney!=null){
            activityManager.setEnableMoney(enableMoney);
        }else{
            BigDecimal bigDecimal=new BigDecimal(0);
            activityManager.setEnableMoney(bigDecimal);
        }
        activityManager.setMeetingUse(meetingUse);
        activityManager.setCheckUsers(checkUsers);
        activityManagerService.save(activityManager);
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                activityManagerService.update(activityManager);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                activityManagerService.save(activityManager, "activitymanager", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        setData();

        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        var2.put("initDuty", curDutyId);
        ArrayList<String> list = new ArrayList<String>();
        Set<Duty> dutySet= null;
        Set<Post> postSet=null;
        if(StringUtils.isNotEmpty(activityManagerDepartId)){
            department=departmentService.get(activityManagerDepartId);
            Set<Power> powers = department.getPowerSet();
            for(Power power:powers){
                Post post1 = power.getPost();
                if(post1.getName().equals("支部书记")==true){
                    List<Users> usersList=dutyService.getPersons(department, post1);//更具岗位对象获取该部门下的指定岗位下的 所有人
                    for(Users users1:usersList){
                        list.add(users1.getId());
                    }
                }
            }
            var2.put("approvers",list);
        }

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                activityManagerService.approve(activityManager, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            } else {
                activityManagerService.commit(activityManager, "activitymanager", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批
    public String approve1(){
        activityManager = activityManagerService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();

       /* var1.put("numStatus", 2);*/
        var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                activityManagerService.approve(activityManager, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
               Task overtask = workflowService.getCurrentTask(keyId);
                if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){//*如果在同一级还有下一个审批人numStatus的值还是上一级的numStatus的值*//*
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 2);//*如果同一级制定的人数都审批完成，则numStatus的值则是下一集审批人*//*
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }
    // 会签
    public String approve2() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 3);

        activityManager = activityManagerService.get(keyId);
        /*Task task=workflowService.getCurrentTask(keyId, usersService.getLoginInfo());
      

        int multComplete = (Integer)runtimeService.getVariable(task.getExecutionId(),"nrOfCompletedInstances");
        int nrOfInstances = (Integer)runtimeService.getVariable(task.getExecutionId(),"nrOfInstances");
        LogUtil.info("multComplete:"+multComplete);
        LogUtil.info("nrOfInstances:"+nrOfInstances);

        if((double)multComplete/nrOfInstances>=1){
            var2.put("numStatus", 2);
            LogUtil.info("do:numStatus2");
        } else {
            var2.put("numStatus", 1);
            LogUtil.info("do:numStatus1");
        }*/

          var2.put("curDutyId", curDutyId);
        try {

            activityManagerService.approve(activityManager, FlowEnum.ProcessState.Running, var2, curDutyId, comment);

            /*Task overtask = workflowService.getCurrentTask(keyId);
            LogUtil.info("overtask:"+overtask);
            if(overtask!=null){
                if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 3);
                }
            }*/
            /*if(overtask==null){
            }else {
                if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 3);
                }
            }*/
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String approve3() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        activityManager = activityManagerService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 4);
        var2.put("curDutyId", curDutyId);
        try {
            activityManagerService.approve(activityManager, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        Company company=usersService.getLoginInfo().getCompany();
        activityManager=activityManagerService.get(keyId);
        //BigDecimal applyMoney=activityManager.getActivityMoney();
        Department department=activityManager.getDepartment();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",company);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("department",department);
        List<MoneyAvgDetail>moneyAvgDetailList=moneyAvgDetailService.getList(params);
        List<Date> createDateList=Lists.newArrayList();
        for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
            createDateList.add(moneyAvgDetail.getCreateDate());
        }
        for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
            if(moneyAvgDetail.getCreateDate()==Collections.min(createDateList)){
                Double departThisUseMoney=activityManager.getActivityMoney().doubleValue();
                Double departUsedMoney=moneyAvgDetail.getDepartEveryUseDetail()!=null?moneyAvgDetail.getDepartEveryUseDetail():0.00;//累计已使用支配党费金额
                Double departEnableAllMoney=moneyAvgDetail.getDepartAllEnableMoney()!=null?moneyAvgDetail.getDepartAllEnableMoney():0.00;//累计可支配金额
                //Double departResidueEnableMoney=moneyAvgDetail.getDepartResidueEnableMoney()!=null?moneyAvgDetail.getDepartResidueEnableMoney():0.00;//剩余可支配金额
                moneyAvgDetail.setDepartEveryUseDetail(departUsedMoney+departThisUseMoney);
                moneyAvgDetail.setDepartResidueEnableMoney(departEnableAllMoney-departUsedMoney-departThisUseMoney);
                moneyAvgDetailService.update(moneyAvgDetail);
                break;
            }
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            activityManager = activityManagerService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("activitymanager");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            System.out.println("comment:"+comment);
            LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            activityManagerService.reject(activityManager, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            activityManager = activityManagerService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("activitymanager");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:"+key);
            if(com.joint.base.util.StringUtils.isEmpty(comment)){
                comment="";
            }
            activityManagerService.deny(activityManager, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }



    public ActivityManager getActivityManager() {
        return activityManager;
    }

    public void setActivityManager(ActivityManager activityManager) {
        this.activityManager = activityManager;
    }
    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }




    public String getCheckUsersId() {
        return checkUsersId;
    }

    public void setCheckUsersId(String checkUsersId) {
        this.checkUsersId = checkUsersId;
    }

    public String getAttchmentFileId() {
        return attchmentFileId;
    }

    public void setAttchmentFileId(String attchmentFileId) {
        this.attchmentFileId = attchmentFileId;
    }

    public SelectionSpeech getSelectionSpeech() {
        return selectionSpeech;
    }

    public void setSelectionSpeech(SelectionSpeech selectionSpeech) {
        this.selectionSpeech = selectionSpeech;
    }


    public String getRequirdMoneyType() {
        return requirdMoneyType;
    }

    public void setRequirdMoneyType(String requirdMoneyType) {
        this.requirdMoneyType = requirdMoneyType;
    }

    public BigDecimal getActivityMoney() {
        return activityMoney;
    }

    public void setActivityMoney(BigDecimal activityMoney) {
        this.activityMoney = activityMoney;
    }

    public BigDecimal getEnableMoney() {
        return enableMoney;
    }

    public void setEnableMoney(BigDecimal enableMoney) {
        this.enableMoney = enableMoney;
    }



    public String getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }



    public List<Dict> getActivityType() {
        return activityType;
    }

    public void setActivityType(List<Dict> activityType) {
        this.activityType = activityType;
    }

    public String getActivityManagerDepartId() {
        return activityManagerDepartId;
    }

    public void setActivityManagerDepartId(String activityManagerDepartId) {
        this.activityManagerDepartId = activityManagerDepartId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<Users> getCheckUsers() {
        return checkUsers;
    }

    public void setCheckUsers(List<Users> checkUsers) {
        this.checkUsers = checkUsers;
    }

    public String getMeetingUseId() {
        return meetingUseId;
    }

    public void setMeetingUseId(String meetingUseId) {
        this.meetingUseId = meetingUseId;
    }

    public List<MeetingUse> getMeetingUse() {
        return meetingUse;
    }

    public void setMeetingUse(List<MeetingUse> meetingUse) {
        this.meetingUse = meetingUse;
    }
}


