package com.joint.web.action.com;
import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.DepartmentService;
import com.joint.base.service.PostService;
import com.joint.base.util.DataUtil;
import com.joint.base.util.StringUtils;
import com.joint.core.entity.*;
import com.joint.core.service.*;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by root on 16/12/16.
 */
@ParentPackage("com")
public class AjaxEmployeesAction extends BaseFlowAction {

    @Resource
    private EmployeesService employeesService;
    @Resource
    private EmploymentService employmentService;
    @Resource
    private DictService dictService;
    @Resource
    private FamilyService familyService;
    @Resource
    private PostService postService;
    @Resource
    private LeaveDetailService leaveDetailService;

    private Users loginUser;
    private Employment employment;
    private Employees employees;
    private String name;
    private String nature;
    private String startDate;
    private String endDate;
    private String trialStart;
    private String trialEnd;
    private Department dep;
    private BigDecimal money;
    private String sex;
    private String nativePlace;
    private String nation;
    private String birthDate;
    private String houseLocation;
    private String cardRegister;
    private String cardStart;
    private String cardEnd;
    private String cardID;
    private String maritalStatus;
    private String mobile;
    private String email;
    private String highestDegreeDictId;
    private String school;
    private String profession;
    private String workTime;
    private String permanentAddress;
    private String postId;
    private String postName;
    private String depId;
    private String depName;
    private String copyIDCardIds;
    private String copyfileId;
    private Set<Family> familySet;
    private String ifOneself;
    private String natureValue;
    private String gender;
    private LeaveDetail leaveDetail;
    private String participantDepartmentId;

    /**
     * 联系电话
     */
    private String tel;

    /**
     * 邮编地址
     */
    private String postcode;

    /**
     * 居住地址
     */
    private String address;

    /**
     * 紧急联系人
     */
    private String contactLinkman;

    /**
     * 紧急联络电话
     */
    private String contactTel;

    /**
     * 邮编地址
     */
    private String postcode1;

    /**
     * 兴趣特长与技能
     */
    private String others;

    /**
     * 照片
     */
    private FileManage headImage;

    private BigDecimal annuaLeaveNum;

    protected ArrayList<Map<String,Object>> typesArrayList;
    protected ArrayList<Map<String,Object>> eduArrayList;
    private String remark;

    /**
     * 称谓
     */
    private String appellation;
    private List<String> appellationList;

    /**
     * 年龄
     */
    private String age1;
    private List<String> age1List;

    /**
     * 政治面貌
     */
    private String politicalStatus;
    private List<String> politicalStatusList;

    /**
     * 工作单位
     */
    private String workAdd;
    private List<String> workAddList;

    /**
     * 职务
     */
    private String post1;
    private List<String> post1List;

    /**
     * 联系电话
     */
    private String tel1;
    private List<String> tel1List;

    /**
     * 姓名
     */
    private String name1;
    private List<String> name1List;

    private Family family;
    /**
     * 视图类型
     */
    private String viewtype;


    /**
     * 党政模块定义的属性
     */
    private String departId;
    private String payDateString;
    @Resource
    private DepartmentService departmentService;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "employees";
    }

    /**
     * 党政模块定义
     */
    public String count(){

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(org.apache.commons.lang.StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        if(org.apache.commons.lang.StringUtils.isNotEmpty(departId)){//根据传来的父类部门，得到他的子类部门，再将父类部门放在子类集合中
            Department parentDepartment=departmentService.get(departId);
            Set<Department> childrenDepartment=parentDepartment.getChildren();
            childrenDepartment.add(parentDepartment);
            params.put("dep",childrenDepartment);
        }
        params.put("partyMember","0");//是否是党员的字段，0：是党员，1：不是党员
        params.put("ifJob",new String[]{"0","2"});//ifJob  0：在职，1：离职，2：退休

        //已归档
        pager=employeesService.findByPagerAndFinish("employees",pager,params);
        List<Employees> employeesList;
        if(pager.getTotalCount()>0){
            employeesList=(List<Employees>)pager.getList();
        }else{
            employeesList= Lists.newArrayList();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        Date date=null;
        if(StringUtils.isNotEmpty(payDateString)){
            String[] dateStr=payDateString.split("-");
            int year=Integer.parseInt(dateStr[0].trim());
            int month=Integer.parseInt(dateStr[1].trim());
            Calendar c= Calendar.getInstance();
            c.set(year,month-1,01);
            date=c.getTime();
        }
        if(employeesList!=null){
            for(Employees employees:employeesList){
                if(date!=null){
                    if(employees.getStartDate()!=null&&employees.getEndDate()!=null){
                        if(date.after(employees.getStartDate())&&employees.getEndDate().after(date)){
                            rMap=new HashMap<String,Object>();
                            rMap.put("id",employees.getId()!=null?employees.getId():"");
                            rMap.put("userName",employees.getUsers()!=null?employees.getUsers().getName():"");
                            rMap.put("baseMoney",employees.getBaseMoney()!=null?employees.getBaseMoney():0);
                            JSONObject o = JSONObject.fromObject(rMap);
                            dataRows.add(o);
                        }
                    }
                }
            }
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    /**
     * 员工dialog
     * @return
     */
    public String listfordialog(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(org.apache.commons.lang.StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("ifJob","0");
        //已完成
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
        pager=employeesService.findByPagerAndFinish( "employees", pager, params);
        List<Employees> eList;
        if (pager.getTotalCount() > 0){
            eList = (List<Employees>) pager.getList();
        }else{
            eList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(Employees e:eList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",e.getId());
            rMap.put("name",e.getName());
            rMap.put("department",e.getEmployment().getDepartment()==null?"":e.getEmployment().getDepartment().getId());
            rMap.put("post",e.getEmployment().getPost()==null?"":e.getEmployment().getPost().getId());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);
        LogUtil.info("dataRows"+dataRows);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(org.apache.commons.lang.StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        //所有单子
        //params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny,FlowEnum.ProcessState.Draft});
        pager=employeesService.findByPagerAndLimit(true, "employees", pager, params);
        List<Employees> eList;
        if (pager.getTotalCount() > 0){
            eList = (List<Employees>) pager.getList();
        }else{
            eList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(Employees e:eList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",e.getId());
            rMap.put("name",e.getName());
            rMap.put("gender",e.getGender());
            rMap.put("age",e.getAge());
            rMap.put("department",e.getDep()==null?"":e.getDep().getName());
            rMap.put("post",e.getEmpPost()==null?"":e.getEmpPost().getName());
            rMap.put("profession",e.getProfession()==null?"":e.getProfession());
            rMap.put("highestDegree",e.getHighestDegree()==null?"":e.getHighestDegree().getName());
            rMap.put("state",e.getProcessState()==null?"":e.getProcessState().value());

            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);
      //  LogUtil.info("dataRows"+dataRows);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    /**
     * 聘用
     * @return
     */
    public String employlist(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(org.apache.commons.lang.StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("nature","2");
        //已归档
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running});
        pager=employeesService.findByPagerAndLimit(true, "employees", pager, params);

        List<Employees> eList;
        if (pager.getTotalCount() > 0){
            eList = (List<Employees>) pager.getList();
        }else{
            eList = new ArrayList<>();
        }
        LogUtil.info("elist:" + eList);
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(Employees e:eList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",e.getId());
            rMap.put("name",e.getName());
            rMap.put("age",e.getAge());
            rMap.put("gender",e.getGender());
            //LogUtil.info("e.getManage():" + e.getManage().getInterviewSet());
            rMap.put("post",e.getEmpPost()==null?"":e.getEmpPost().getName());
            rMap.put("dep",e.getDep()==null?"":e.getDep().getName());
            String nature = e.getNature();
            if(nature.equals("0")){
                rMap.put("nature","正式");
            }else if(nature.equals("1")){
                rMap.put("nature","试用");
            }else if(nature.equals("2")){
                rMap.put("nature","聘用");
            }
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        LogUtil.info("dataRows"+dataRows);
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    /**
     * 身份证识别男女
     * @return
     */
    public static String getGenderByIdCard(String idCard) {
        String sex;

        if (idCard.length() == 18) {
            if (Integer.parseInt(idCard.substring(16).substring(0, 1)) % 2 == 0) {// 判断性别
                sex = "女";
            } else {
                sex = "男";
            }

        }else {
            String usex = idCard.substring(14, 15);// 用户的性别
            if (Integer.parseInt(usex) % 2 == 0) {
                sex = "女";
            } else {
                sex = "男";
            }

        }
        return sex;
    }


    public String read(){
        copyfileId="";
        familySet= Sets.newHashSet();
        loginUser = usersService.getLoginInfo();
        List<Dict> eduType=dictService.listFormDefinedEnable(DictBean.DictEnum.Edu,usersService.getCompanyByUser().getId());
        Map<String,Object> rMap = null;
        if(StringUtils.isNotEmpty(keyId)) {
            employees=employeesService.get(keyId);
            numStatus=workflowService.getNumStatus(keyId,loginUser);
            String na = employees.getNature();
            if(na.equals("0")){
                nature="正式";
            }else if(na.equals("1")){
                nature="试用";
            }else if(na.equals("2")){
                nature="聘用";
            }else {
                nature=" ";
            }
            if(employees.getFamilySet()!=null){
                familySet=employees.getFamilySet();

            }
            leaveDetail=employees.getLeaveDetail();

            if(employees.getCopyIDCard().size()>0){
                for (FileManage c:employees.getCopyIDCard()){
                    copyfileId+=c.getId()+",";
                }

            }
            if(eduType.size()>0){
                eduArrayList = new ArrayList<Map<String, Object>>();
                for(Dict l:eduType){
                    rMap= Maps.newHashMap();
                    rMap.put("id", l.getId());
                    rMap.put("selected", "");
                    if(employees.getHighestDegree()!=null && StringUtils.equals(l.getId(), employees.getHighestDegree().getId())){
                        rMap.put("selected","selected");
                    }
                }
            }
            
        }
        //LogUtil.info("familySet"+familySet);

        return "read";
    }

    public String input(){

        familySet= Sets.newHashSet();
        Company company = usersService.getCompanyByUser();
        List<Dict> eduList = dictService.listFormDefinedEnable(DictBean.DictEnum.Edu, company.getId());
        eduArrayList = new ArrayList<Map<String, Object>>();
        Map<String,Object> rMap = null;
        if(StringUtils.isNotEmpty(keyId)){
            employees = employeesService.get(keyId);
            String na = employees.getNature();
            if(na.equals("0")){
                natureValue="正式";
            }else if(na.equals("1")){
                natureValue="试用";
            }else {
                natureValue="聘用";
            }
            gender = getGenderByIdCard(employees.getCardID());

            if(employees.getFamilySet()!=null){
                for(Family family:employees.getFamilySet()){
                    familySet.add(family);
                }
            }

            leaveDetail=employees.getLeaveDetail();
            copyfileId="";
            if(employees.getCopyIDCard().size()>0){
                for (FileManage c:employees.getCopyIDCard()){
                    copyfileId+=c.getId()+",";
                }

            }
            for(Dict typeObj:eduList){
                rMap = new HashMap<String, Object>();
                rMap.put("id",typeObj.getId());
                rMap.put("name",typeObj.getName());
                rMap.put("selected","");
                LogUtil.info("name"+typeObj.getName());
                if(employees.getHighestDegree()!=null && com.joint.base.util.StringUtils.equals(typeObj.getId(), employees.getHighestDegree().getId())){
                    rMap.put("selected","selected");
                }
                eduArrayList.add(rMap);
            }

            LogUtil.info("familySet"+familySet);

        }else {

            for(Dict typeObj:eduList){
                rMap = new HashMap<String, Object>();
                rMap.put("id",typeObj.getId());
                rMap.put("name",typeObj.getName());
                rMap.put("selected","");
                LogUtil.info("name"+typeObj.getName());
                if(employees.getHighestDegree()!=null && com.joint.base.util.StringUtils.equals(typeObj.getId(), employees.getHighestDegree().getId())){
                    rMap.put("selected","selected");
                }
                eduArrayList.add(rMap);
            }
            leaveDetail=employees.getLeaveDetail();

        }
        return "input";

    }

    /**
     * 判断是否为本人
     * @return
     */
    public String checkOneself(){

        loginUser=usersService.getLoginInfo();

        Map<String,String> data=new HashMap<String, String>();
        if(StringUtils.isNotEmpty(keyId)){
            employees = employeesService.get(keyId);
            Users creater = employees.getCreater();
            if(loginUser.equals(creater)){
                ifOneself="1";
                data.put("ifOneself",ifOneself);
            }

        }else {
            data.put("ifOneself",ifOneself);
        }

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    /**
     * 转正
     */
    public String update(){
        //setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                Employees entity =employeesService.get(keyId);
                entity.setNature("0");
                employeesService.update(entity);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");

    }

    public void setData(){

        familySet= Sets.newHashSet();
        Family family =null;
        String Fid=null;
        Set<FileManage> copyIDCard= Sets.newHashSet();
        age1List=Lists.newArrayList();
        appellationList=Lists.newArrayList();
        politicalStatusList=Lists.newArrayList();
        workAddList=Lists.newArrayList();
        tel1List=Lists.newArrayList();
        post1List=Lists.newArrayList();
        name1List=Lists.newArrayList();
        if(StringUtils.isNotEmpty(keyId)){
            LogUtil.info("keyId"+keyId);
            employees = employeesService.get(keyId);
            Set<Family> familySet = employees.getFamilySet();
            if(familySet.size()>0){
                for (Family family1 :familySet){
                    Fid = family1.getId();
                    if(StringUtils.isNotEmpty(Fid)){
                        familyService.delete(familyService.get(Fid));

                    }
                }
            }


        }else{
            employees = new Employees();
/*
            family=new Family();
*/
            employees.setCreater(usersService.getLoginInfo());
        }

        if(StringUtils.isNotEmpty(appellation)){
            for (String a:appellation.split(",")){
                appellationList.add(a.trim());
            }

        }

       if(StringUtils.isNotEmpty(age1)){
           for (String a:age1.split(",")){
               age1List.add(a.trim());
           }

        }
        if(StringUtils.isNotEmpty(politicalStatus)){
            for (String p:politicalStatus.split(",")){
                politicalStatusList.add(p.trim());
            }

        }
        if(StringUtils.isNotEmpty(workAdd)){
            for (String w:workAdd.split(",")){
                workAddList.add(w.trim());
            }

        }
        if(StringUtils.isNotEmpty(post1)){
            for(String p:post1.split(",")){
                post1List.add(p.trim());
            }

        }
        if(StringUtils.isNotEmpty(tel1)){
            for (String t:tel1.split(",")){
                tel1List.add(t.trim());
            }

        }
       if(StringUtils.isNotEmpty(name1)){
            for (String n:name1.split(",")){
                name1List.add(n.trim());
            }
       }

       int len=appellationList.size();
       //LogUtil.info("len"+len);
       Family[] families= new Family[len];
        if(StringUtils.isNotEmpty(appellation)) {
            //家庭情况
            for (int i = 0; i < families.length; i++) {
                families[i]=new Family();
                families[i].setAppellation(appellationList.get(i));
                families[i].setName(name1List.get(i));
                families[i].setAge(age1List.get(i));
                families[i].setPoliticalStatus(politicalStatusList.get(i));
                families[i].setWorkAdd(workAddList.get(i));
                families[i].setPost(post1List.get(i));
                families[i].setTel(tel1List.get(i));
                families[i].setCompany(usersService.getLoginInfo().getCompany());
                families[i].setEmployees(employeesService.get(employees.getId()));
                families[i].setState(BaseEnum.StateEnum.Enable);

                familyService.save(families[i]);
                familySet.add(families[i]);
            }
        }
        if(StringUtils.isNotEmpty(startDate)){
            employees.setStartDate(DataUtil.StringToDate(startDate));
        }
        if(StringUtils.isNotEmpty(endDate)){
            employees.setEndDate(DataUtil.StringToDate(endDate));
        }
        if(StringUtils.isNotEmpty(trialStart)){
            employees.setTrialStart(DataUtil.StringToDate(trialStart));
        }
        if(StringUtils.isNotEmpty(trialEnd)){
            employees.setTrialEnd(DataUtil.StringToDate(trialEnd));
        }
        if(StringUtils.isNotEmpty(birthDate)){
            employees.setBirthDate(DataUtil.StringToDate(birthDate));
        }

        employees.setMon(DataUtil.DateToString(DataUtil.StringToDate(birthDate),"MM"));
        if(StringUtils.isNotEmpty(cardStart)){
            employees.setCardStart(DataUtil.StringToDate(cardStart));
        }
        if(StringUtils.isNotEmpty(cardEnd)){
            employees.setCardEnd(DataUtil.StringToDate(cardEnd));
        }

        if (StringUtils.isNotEmpty(workTime)){
            employees.setWorkTime(DataUtil.StringToTime(workTime));
        }

        if(StringUtils.isNotEmpty(copyIDCardIds)){
            for (String fId:copyIDCardIds.split(",")){
                copyIDCard.add(fileManageService.get(fId));
            }

        }
        //employees.setAnnuaLeaveNum(annuaLeaveNum);
        LeaveDetail leaveDetail = new LeaveDetail();
        leaveDetail.setAnnuaLeave(annuaLeaveNum);
        leaveDetail.setUsedAnnuaLeave(BigDecimal.ZERO);
        leaveDetail.setUnusedAnnuaLeave(annuaLeaveNum);
        leaveDetail.setSickLeave(BigDecimal.ZERO);
        leaveDetail.setThingLeave(BigDecimal.ZERO);
        leaveDetail.setOthersLeave(BigDecimal.ZERO);
        leaveDetail.setUsedDayNum(BigDecimal.ZERO);
        leaveDetail.setUsers(employees.getUsers());

        //leaveDetail.setTime();
        leaveDetailService.save(leaveDetail);

        employees.setLeaveDetail(leaveDetail);
        employees.setNativePlace(nativePlace);
        employees.setNation(nation);
        employees.setHouseLocation(houseLocation);
        employees.setCardRegister(cardRegister);
        employees.setMoney(money);
        employees.setCopyIDCard(copyIDCard);
        employees.setGender(gender);
        employees.setFamilySet(familySet);
        employees.setNativePlace(nativePlace);
        employees.setHighestDegree(dictService.get(highestDegreeDictId));
        employees.setPermanentAddress(permanentAddress);
        employees.setNation(nation);
        employees.setCompany(usersService.getLoginInfo().getCompany());
        employees.setMobile(mobile);
        employees.setTel(tel);
        employees.setEmail(email);
        employees.setSchool(school);
        employees.setAddress(address);
        employees.setIfJob("0");
        employees.setMaritalStatus(maritalStatus);
        employees.setPostcode(postcode);
        employees.setContactLinkman(contactLinkman);
        employees.setContactTel(contactTel);
        employees.setPostcode1(postcode1);
        employees.setRemark(remark);

    }

    /**
     * 保存
     * @return
     *//*
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                employeesService.update(employees);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("initDuty", curDutyId);
                employeesService.save(employees, "employees", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }
*/
    /**
     * 提交
     * @return
     */
    public String commit(){

        setData();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);

        ArrayList<String> list = new ArrayList<String>();

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                employeesService.commit(employees, FlowEnum.ProcessState.Finished,"employees", var1,var2, curDutyId);
            }

        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTrialStart() {
        return trialStart;
    }

    public void setTrialStart(String trialStart) {
        this.trialStart = trialStart;
    }

    public String getTrialEnd() {
        return trialEnd;
    }

    public void setTrialEnd(String trialEnd) {
        this.trialEnd = trialEnd;
    }

    public String getCardStart() {
        return cardStart;
    }

    public void setCardStart(String cardStart) {
        this.cardStart = cardStart;
    }

    public String getCardEnd() {
        return cardEnd;
    }

    public void setCardEnd(String cardEnd) {
        this.cardEnd = cardEnd;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Department getDep() {
        return dep;
    }

    public void setDep(Department dep) {
        this.dep = dep;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getHouseLocation() {
        return houseLocation;
    }

    public void setHouseLocation(String houseLocation) {
        this.houseLocation = houseLocation;
    }

    public String getCardRegister() {
        return cardRegister;
    }

    public void setCardRegister(String cardRegister) {
        this.cardRegister = cardRegister;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactLinkman() {
        return contactLinkman;
    }

    public void setContactLinkman(String contactLinkman) {
        this.contactLinkman = contactLinkman;
    }

    public String getContactTel() {
        return contactTel;
    }

    public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }

    public String getPostcode1() {
        return postcode1;
    }

    public void setPostcode1(String postcode1) {
        this.postcode1 = postcode1;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public FileManage getHeadImage() {
        return headImage;
    }

    public void setHeadImage(FileManage headImage) {
        this.headImage = headImage;
    }

    public ArrayList<Map<String, Object>> getTypesArrayList() {
        return typesArrayList;
    }

    public void setTypesArrayList(ArrayList<Map<String, Object>> typesArrayList) {
        this.typesArrayList = typesArrayList;
    }

    public ArrayList<Map<String, Object>> getEduArrayList() {
        return eduArrayList;
    }

    public void setEduArrayList(ArrayList<Map<String, Object>> eduArrayList) {
        this.eduArrayList = eduArrayList;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getDepId() {
        return depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public String getCopyfileId() {
        return copyfileId;
    }

    public void setCopyfileId(String copyfileId) {
        this.copyfileId = copyfileId;
    }

    public String getAppellation() {
        return appellation;
    }

    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }

    public String getAge1() {
        return age1;
    }

    public void setAge1(String age1) {
        this.age1 = age1;
    }

    public String getPoliticalStatus() {
        return politicalStatus;
    }

    public void setPoliticalStatus(String politicalStatus) {
        this.politicalStatus = politicalStatus;
    }

    public String getWorkAdd() {
        return workAdd;
    }

    public void setWorkAdd(String workAdd) {
        this.workAdd = workAdd;
    }

    public String getPost1() {
        return post1;
    }

    public void setPost1(String post1) {
        this.post1 = post1;
    }
    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }


    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getHighestDegreeDictId() {
        return highestDegreeDictId;
    }

    public void setHighestDegreeDictId(String highestDegreeDictId) {
        this.highestDegreeDictId = highestDegreeDictId;
    }

    public Set<Family> getFamilySet() {
        return familySet;
    }

    public void setFamilySet(Set<Family> familySet) {
        this.familySet = familySet;
    }


    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }



    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public Employment getEmployment() {
        return employment;
    }

    public void setEmployment(Employment employment) {
        this.employment = employment;
    }

    public String getIfOneself() {
        return ifOneself;
    }

    public void setIfOneself(String ifOneself) {
        this.ifOneself = ifOneself;
    }

    public String getNatureValue() {
        return natureValue;
    }

    public void setNatureValue(String natureValue) {
        this.natureValue = natureValue;
    }


    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public List<String> getAppellationList() {
        return appellationList;
    }

    public void setAppellationList(List<String> appellationList) {
        this.appellationList = appellationList;
    }

    public List<String> getAge1List() {
        return age1List;
    }

    public void setAge1List(List<String> age1List) {
        this.age1List = age1List;
    }

    public List<String> getPoliticalStatusList() {
        return politicalStatusList;
    }

    public void setPoliticalStatusList(List<String> politicalStatusList) {
        this.politicalStatusList = politicalStatusList;
    }

    public List<String> getWorkAddList() {
        return workAddList;
    }

    public void setWorkAddList(List<String> workAddList) {
        this.workAddList = workAddList;
    }

    public List<String> getPost1List() {
        return post1List;
    }

    public void setPost1List(List<String> post1List) {
        this.post1List = post1List;
    }

    public List<String> getTel1List() {
        return tel1List;
    }

    public void setTel1List(List<String> tel1List) {
        this.tel1List = tel1List;
    }

    public List<String> getName1List() {
        return name1List;
    }

    public void setName1List(List<String> name1List) {
        this.name1List = name1List;
    }

    public BigDecimal getAnnuaLeaveNum() {
        return annuaLeaveNum;
    }

    public void setAnnuaLeaveNum(BigDecimal annuaLeaveNum) {
        this.annuaLeaveNum = annuaLeaveNum;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCopyIDCardIds() {
        return copyIDCardIds;
    }

    public void setCopyIDCardIds(String copyIDCardIds) {
        this.copyIDCardIds = copyIDCardIds;
    }

    public LeaveDetail getLeaveDetail() {
        return leaveDetail;
    }

    public void setLeaveDetail(LeaveDetail leaveDetail) {
        this.leaveDetail = leaveDetail;
    }

    public String getParticipantDepartmentId() {
        return participantDepartmentId;
    }

    public void setParticipantDepartmentId(String participantDepartmentId) {
        this.participantDepartmentId = participantDepartmentId;
    }

    public String getPayDateString() {
        return payDateString;
    }

    public void setPayDateString(String payDateString) {
        this.payDateString = payDateString;
    }
}


