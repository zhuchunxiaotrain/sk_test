package com.joint.web.action.manage;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Users;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.CarConfig;
import com.joint.core.entity.manage.Oil;
import com.joint.core.service.CarConfigService;
import com.joint.core.service.OilService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
@ParentPackage("manage")
public class AjaxOilAction extends BaseFlowAction {
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private OilService oilService;
    @Resource
    private CarConfigService carConfigService;

//    /**
//     * 车辆品牌
//     */
//    private String brandId;
//    private List<Map<String, Object>> brandDict;
//    /**
//     * 车辆型号
//     */
//    private String typeId;
//    private List<Map<String, Object>> typeDict;
//    /**
//     * 车辆号牌
//     */
//    private String carNoId;
//    private List<Map<String, Object>> carNoDict;
    /**
     * 车辆品牌
     */
    private String brand;
    /**
     * 车辆型号
     */
    private String type;
    /**
     * 车辆号牌
     */
    private String carNo;
    /**
     * 当前里程数
     */
    private int kilometre;
    /**
     * 加油时间
     */
    private String oilDate;
    /**
     * 加油金额
     */
    private Double oilMoney;
    /**
     * 当前累计加油金额
     */
    private Double oilMoneyNum;
    /**
     * 备注
     */
    private String remarks;

    private Oil oil;
    private CarConfig carConfig;
    private Users loginUser;
    private int numStatus;
    public String execute(){
        return "oil";
    }


    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
        pager=carConfigService.findByPagerAndLimit(true, "carconfig", pager, params);
        List<CarConfig> CarConfigList;
        if (pager.getTotalCount() > 0){
            CarConfigList = (List<CarConfig>) pager.getList();
        }else{
            CarConfigList= new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(CarConfig carConfig: CarConfigList){
            rMap = new HashMap<String, Object>();
            rMap.put("id",carConfig.getId());
            rMap.put("brand",carConfig.getBrand());
            rMap.put("type",carConfig.getType());
            rMap.put("carNo",carConfig.getCarNo());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String read(){
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)) {
            carConfig = carConfigService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
        }
        return "read";
    }

//    public String input(){
//        Company company = usersService.getCompanyByUser();
//        List<Dict> brandList = dictService.listFormDefinedEnable(DictBean.DictEnum.CarBrand, usersService.getCompanyByUser().getId());
//        List<Dict> typeList = dictService.listFormDefinedEnable(DictBean.DictEnum.CarType, usersService.getCompanyByUser().getId());
//        List<Dict> noList = dictService.listFormDefinedEnable(DictBean.DictEnum.CarNo, usersService.getCompanyByUser().getId());
//        Map<String,Object> rMap = null;
//        typeDict = new ArrayList<Map<String, Object>>();
//        brandDict = new ArrayList<Map<String, Object>>();
//        carNoDict = new ArrayList<Map<String, Object>>();
//        if (StringUtils.isNotEmpty(keyId)){
//            oil=oilService.get(keyId);
//            for(Dict brand:brandList){
//                rMap = new HashMap<String, Object>();
//                rMap.put("id",brand.getId());
//                rMap.put("name",brand.getName());
//                rMap.put("selected","");
//                if(oil.getBrand()!=null && StringUtils.equals(brand.getId(), oil.getBrand().getId())){
//                    rMap.put("selected","selected");
//                }
//                brandDict.add(rMap);
//            }
//            for(Dict type:typeList){
//                rMap = new HashMap<String, Object>();
//                rMap.put("id",type.getId());
//                rMap.put("name",type.getName());
//                rMap.put("selected","");
//                if(oil.getType()!=null && StringUtils.equals(type.getId(), oil.getType().getId())){
//                    rMap.put("selected","selected");
//                }
//                typeDict.add(rMap);
//            }
//            for(Dict no:noList){
//                rMap = new HashMap<String, Object>();
//                rMap.put("id",no.getId());
//                rMap.put("name",no.getName());
//                rMap.put("selected","");
//                if(oil.getCarNo()!=null && StringUtils.equals(no.getId(), oil.getCarNo().getId())){
//                    rMap.put("selected","selected");
//                }
//                carNoDict.add(rMap);
//            }
//        } else {
//            for(Dict brand:brandList){
//                rMap = new HashMap<String, Object>();
//                rMap.put("id",brand.getId());
//                rMap.put("name",brand.getName());
//                rMap.put("selected","");
//                brandDict.add(rMap);
//            }
//            for(Dict type:typeList){
//                rMap = new HashMap<String, Object>();
//                rMap.put("id",type.getId());
//                rMap.put("name",type.getName());
//                rMap.put("selected","");
//                typeDict.add(rMap);
//            }
//            for(Dict no:noList){
//                rMap = new HashMap<String, Object>();
//                rMap.put("id",no.getId());
//                rMap.put("name",no.getName());
//                rMap.put("selected","");
//                carNoDict.add(rMap);
//            }
//
//        }
//        return "input";
//    }

//    public void setData(){
//        if(StringUtils.isNotEmpty(keyId)){
//            oil=oilService.get(keyId);
//        }else{
//            oil=new Oil();
//            oil.setCreater(usersService.getLoginInfo());
//        }
//        oil.setCompany(usersService.getLoginInfo().getCompany());
//        oil.setBrand(dictService.get(brandId));
//        oil.setType(dictService.get(typeId));
//        oil.setCarNo(dictService.get(carNoId));
//        oil.setKilometre(kilometre);
//        oil.setOilDate(DataUtil.StringToDate(oilDate));
//        oil.setOilMoney(oilMoney);
//        oil.setOilMoneyNum(oilMoneyNum);
//        oil.setRemarks(remarks);
//    }

    // 保存
//    public String save(){
//        setData();
//        try {
//            if(StringUtils.isNotEmpty(keyId)){
//                oilService.update(oil);
//            } else {
//                Map<String, Object> various = new HashMap<String, Object>();
//                various.put("numStatus", 0);
//                various.put("curDutyId", curDutyId);
//                various.put("initDuty", curDutyId);
//                oilService.save(oil, "oil", various);
//            }
//        } catch (DutyNotExistsException e) {
//            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
//        } catch (PcfgNotExistException e) {
//            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
//        }
//
//        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
//    }

    // 提交
//    public String commit(){
//        setData();
//        Map<String, Object> var1 = new HashMap<String, Object>();
//        var1.put("numStatus", 0);
//        var1.put("curDutyId", curDutyId);
//        var1.put("initDuty", curDutyId);
//        Map<String, Object> var2 = new HashMap<String, Object>();
//        var2.put("numStatus", 1);
//        var2.put("curDutyId", curDutyId);
//
//        try {
//            if (StringUtils.isNotEmpty(keyId)) {
//                oilService.approve(oil, FlowEnum.ProcessState.Finished, var2, curDutyId, comment);
//            } else {
//                keyId = oilService.commit(oil, FlowEnum.ProcessState.Finished,"oil", var1, var2, curDutyId);
//            }
//        } catch (DutyNotExistsException e) {
//            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
//        } catch (PcfgNotExistException e) {
//            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
//        }
//        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
//    }


//    public String getBrandId() {
//        return brandId;
//    }
//
//    public void setBrandId(String brandId) {
//        this.brandId = brandId;
//    }
//
//    public List<Map<String, Object>> getBrandDict() {
//        return brandDict;
//    }
//
//    public void setBrandDict(List<Map<String, Object>> brandDict) {
//        this.brandDict = brandDict;
//    }
//
//    public String getTypeId() {
//        return typeId;
//    }
//
//    public void setTypeId(String typeId) {
//        this.typeId = typeId;
//    }
//
//    public List<Map<String, Object>> getTypeDict() {
//        return typeDict;
//    }
//
//    public void setTypeDict(List<Map<String, Object>> typeDict) {
//        this.typeDict = typeDict;
//    }
//
//    public String getCarNoId() {
//        return carNoId;
//    }
//
//    public void setCarNoId(String carNoId) {
//        this.carNoId = carNoId;
//    }
//
//    public List<Map<String, Object>> getCarNoDict() {
//        return carNoDict;
//    }
//
//    public void setCarNoDict(List<Map<String, Object>> carNoDict) {
//        this.carNoDict = carNoDict;
//    }

    public int getKilometre() {
        return kilometre;
    }

    public void setKilometre(int kilometre) {
        this.kilometre = kilometre;
    }

    public String getOilDate() {
        return oilDate;
    }

    public void setOilDate(String oilDate) {
        this.oilDate = oilDate;
    }

    public Double getOilMoney() {
        return oilMoney;
    }

    public void setOilMoney(Double oilMoney) {
        this.oilMoney = oilMoney;
    }

    public Double getOilMoneyNum() {
        return oilMoneyNum;
    }

    public void setOilMoneyNum(Double oilMoneyNum) {
        this.oilMoneyNum = oilMoneyNum;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Oil getOil() {
        return oil;
    }

    public void setOil(Oil oil) {
        this.oil = oil;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public CarConfig getCarConfig() {
        return carConfig;
    }

    public void setCarConfig(CarConfig carConfig) {
        this.carConfig = carConfig;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }
}
