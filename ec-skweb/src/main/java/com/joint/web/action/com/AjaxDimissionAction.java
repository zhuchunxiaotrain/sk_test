package com.joint.web.action.com;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.DepartmentService;
import com.joint.base.service.ReadersService;
import com.joint.base.service.RemindService;
import com.joint.base.util.DataUtil;
import com.joint.base.util.StringUtils;
import com.joint.core.entity.Apply;
import com.joint.core.entity.Dimission;
import com.joint.core.entity.Employees;
import com.joint.core.entity.PersonnelResume;
import com.joint.core.service.DimissionService;
import com.joint.core.service.EmployeesService;
import com.joint.core.service.PersonnelResumeService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by root on 16/12/16.
 */
@ParentPackage("com")
public class AjaxDimissionAction extends BaseFlowAction {

    @Resource
    private EmployeesService employeesService;
    @Resource
    private DimissionService dimissionService;
    @Resource
    private DictService dictService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private RemindService remindService;
    @Resource
    private ReadersService readersService;
    @Resource
    private PersonnelResumeService personnelResumeService;

    private Employees employees;
    private Dimission dimission;
    private String name;
    private String postId;
    private String depId;
    private String startDate;
    private String endDate;
    private Users loginUser;
    private String applyUserName;
    private String applyUserId;
    private String applyDate;
    private String nature;
    private String natureName;
    private String reportId;
    private String reportfileId;
    private String politicalStatus;
    private String politicalStatusName;
    private String partyMember;
    private Users creater;
    private String createDate;
    private int ifCentralStaff;
    private String ifPersonnel;
    private String reportText;


    /**
     * 身份证号
     */
    private String cardID;

    /**
     * 离职性质
     */
    private String dimissionNature;

    /**
     * 离职原因
     */
    private String reasons;

    /**
     * 辞职报告
     */
    private Set<FileManage> report;

    /**
     * 预计离职时间
     */
    private String leaveDate;

    /**
     * 备注
     */
    private String remark;
    


    /**
     * 下一步审批人的Name的集合，用逗号分隔
     */
    private String nextStepApproversName;

    /**
     * 下一步审批人的id的集合，用逗号分隔
     */
    private String nextStepApproversId;

    /**
     * 视图类型
     */
    private String viewtype;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "dimission";
    }


    /**
     * 离职员工
     * @return
     */
    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        //所有单子
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
        pager=dimissionService.findByPagerAndLimit(true, "dimission", pager, params);

        List<Dimission> eList;
        if (pager.getTotalCount() > 0){
            eList = (List<Dimission>) pager.getList();
        }else{
            eList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(Dimission e:eList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",e.getId());
            rMap.put("name",e.getName());
            rMap.put("state",e.getProcessState().value());
            rMap.put("post",e.getEmployees()==null?"":e.getEmployees().getEmpPost().getName());
            rMap.put("department",e.getDepartment()==null?"":e.getDepartment().getName());
            rMap.put("startDate",DataUtil.DateToString(e.getStartDate(),"yyyy-MM-dd"));
            rMap.put("endDate",DataUtil.DateToString(e.getEndDate(),"yyyy-MM-dd"));
            String dimissionNature=e.getDimissionNature();
            if(StringUtils.isNotEmpty(dimissionNature)){
                if(dimissionNature.equals("0")){
                    rMap.put("dimissionNature","辞职");
                }
                if(dimissionNature.equals("1")){
                    rMap.put("dimissionNature","辞退");
                }
                if(dimissionNature.equals("2")){
                    rMap.put("dimissionNature","退休");
                }

            }
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    public String read(){

        reportfileId="";
        loginUser=usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)){
            dimission = dimissionService.get(keyId);
            LogUtil.info("dimission"+dimission);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            String ps = dimission.getPoliticalStatus();
            if(ps.equals("0")){
                //LogUtil.info("+++");
                politicalStatusName="党员";
            }else{
                politicalStatusName="非党员";
            }
            if(dimission.getReport().size()>0){
                for (FileManage r:dimission.getReport()){
                    reportfileId+=r.getId()+",";
                }
            }

            String na = dimission.getNature();
            if(StringUtils.isNotEmpty(na)){
                if(na.equals("0")){
                    natureName="正式";
                }else if(na.equals("1")){
                    natureName="试用";
                }else {
                    natureName="聘用";
                }

            }

        }

        return "read";

    }

    public String input(){
        reportfileId="";
        loginUser = usersService.getLoginInfo();

        if(StringUtils.isNotEmpty(keyId)){
            dimission = dimissionService.get(keyId);

            if(dimission.getReport().size()>0){
                for (FileManage r:dimission.getReport()){
                    reportfileId+=r.getId()+",";

                }
            }
            employees=dimission.getEmployees();
            politicalStatus = employees.getPartyMember();
            String ps = dimission.getPoliticalStatus();
            if(ps.equals("0")){
                politicalStatusName="党员";
            }else{
                politicalStatusName="非党员";
            }
            String na = dimission.getNature();
            if(na.equals("0")){
                natureName="正式";
            }else if(na.equals("1")){
                natureName="试用";
            }else {
                natureName="聘用";
            }
            createDate= DataUtil.DateToString(dimission.getCreateDate(),"yyyy-MM-dd");
            creater=dimission.getCreater();
        }else {
            if(StringUtils.isNotEmpty(parentId)){
                employees = employeesService.get(parentId);
                politicalStatus = employees.getPartyMember();
                if(StringUtils.isNotEmpty(politicalStatus)){
                    if(politicalStatus.equals("0")){
                        politicalStatusName="党员";
                    }else{
                        politicalStatusName="非党员";
                    }
                }

            }

            String na = employees.getNature();
            if(na.equals("0")){
                natureName="正式";
            }else if(na.equals("1")){
                natureName="试用";
            }else {
                natureName="聘用";
            }
            creater=loginUser;
            createDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        }
        return "input";

    }

    public void setData(){

        Set<FileManage> reportFile= Sets.newHashSet();
        if(StringUtils.isNotEmpty(keyId)){
            dimission = dimissionService.get(keyId);
            employees=dimission.getEmployees();
        }else{
            dimission = new Dimission();
            dimission.setCreater(usersService.getLoginInfo());
        }
        if(StringUtils.isNotEmpty(reportId)){
            for (String r:reportId.split(",")){
                reportFile.add(fileManageService.get(r.trim()));
            }

        }
        //更新employees
        if(StringUtils.isNotEmpty(parentId)){
            employees = employeesService.get(parentId);

            /*if(StringUtils.isNotEmpty(dimissionNature)){
                if(dimission.equals("2")){
                    employees.setIfJob("0");
                }else {
                    employees.setIfJob("1");
                }
            }
            employeesService.update(employees);
*/            dimission.setEmployees(employees);
        }

        //判断是否为产业中心人员
        Set<Duty> dutySet = usersService.getLoginInfo().getDutySet();
        for (Duty duty:dutySet){
            if(duty.getDepartment().getParent()!=null){
               // LogUtil.info("ceshi"+duty.getDepartment().getParent().getName());
                if(org.apache.commons.lang.StringUtils.equals(duty.getDepartment().getParent().getName(),"产业中心")){
                    dimission.setIfCentralStaff(1);
                }
            }

        }

        if(StringUtils.isNotEmpty(depId)){
            dimission.setDepartment(departmentService.get(depId));
        }
        if(StringUtils.isNotEmpty(startDate)){
            dimission.setStartDate(DataUtil.StringToDate(startDate));
        }
        if(StringUtils.isNotEmpty(endDate)){
            dimission.setEndDate(DataUtil.StringToDate(endDate));
        }
        if(StringUtils.isNotEmpty(leaveDate)){
            dimission.setLeaveDate(DataUtil.StringToDate(leaveDate));
        }

        dimission.setReportText(reportText);
        dimission.setName(name);
        dimission.setCreater(usersService.getLoginInfo());
        dimission.setReport(reportFile);
        dimission.setDimissionNature(dimissionNature);
        dimission.setReasons(reasons);
        dimission.setRemark(remark);
        dimission.setNature(nature);
        dimission.setCardID(cardID);
        dimission.setPoliticalStatus(politicalStatus);
        dimission.setNextStepApproversId(nextStepApproversId);
        dimission.setNextStepApproversName(nextStepApproversName);
        dimission.setCompany(usersService.getLoginInfo().getCompany());

    }

    /**
     * 保存
     * @return
     */
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                dimissionService.update(dimission);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("initDuty", curDutyId);
                dimissionService.save(dimission, "dimission", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    /**
     * 判断是否为人事专员
     * @return
     */
    public String checkUser(){

        employees = employeesService.get(parentId);
        Department dep = employees.getDep();
        List<Duty> dutys = dutyService.getDutys(dep);
        Map<String,String> data=new HashMap<String, String>();
        loginUser=usersService.getLoginInfo();
        for (Duty duty:dutys){
            Users users = duty.getUsers();
            if(users.equals(loginUser)){
                ifPersonnel="1";
                data.put("ifPersonnel",ifPersonnel);
            }else {
                data.put("ifPersonnel",ifPersonnel);
            }
        }

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    /**
     * 提交
     * @return
     */
    public String commit(){

        setData();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);

        ArrayList<String> list = new ArrayList<String>();
        for (String uid: nextStepApproversId.split(",")) {
            list.add(uid.trim());
        }
        var2.put("approvers", list);    // 会签人Ids
        var2.put("ifCentralStaff", dimission.getIfCentralStaff());

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                dimissionService.approve(dimission, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            }else{
                keyId=dimissionService.commit(dimission, "dimission", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        Set<Users> usersSet= Sets.newHashSet();
        usersSet=dimission.getEmployees().getEmployment().getManage().getChargeUsers();
        //增加归档后读者
        for(Users users:usersSet) {
            Readers readers = new Readers();
            readers.setUsers(users);
            readers.setBussinessKey("dimission");
            readers.setKeyId(keyId);
            readers.setType(2);
            readersService.save(readers);
        }
        for(Users users:usersSet){
            Remind remind = new Remind();
            remind.setUsers(users);
            remind.setBussinessKey("dimission");
            remind.setKeyId(keyId);
            remind.setContent("员工离职已归档");
            remindService.save(remind);
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 审批1
     * @return
     */
    public String approve1(){

        dimission= dimissionService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();

        if(dimission.getIfCentralStaff()==1){
            var1.put("numStatus",3);
        }else{
            var1.put("numStatus", 2);

        }
        var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (org.apache.commons.lang.StringUtils.isNotEmpty(keyId)) {
                dimissionService.approve(dimission, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }


    /**
     * 审批2
     * @return
     */
    public String approve2() {
        if(org.apache.commons.lang.StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(org.apache.commons.lang.StringUtils.isEmpty(comment)) comment="";
        dimission= dimissionService.get(keyId);
        PersonnelResume personnelResume=new PersonnelResume();

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 3);
        //  var2.put("curDutyId", curDutyId);
        try {
            dimissionService.approve(dimission, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);

            employees=dimission.getEmployees();
            //是否为党员
            if(StringUtils.equals(dimission.getPoliticalStatus(),"2")){
                employees.setIfJob("0");
            }else {
                employees.setIfJob("1");

            }
            //删除
            Users users=employees.getUsers();
            users.setState(BaseEnum.StateEnum.Delete);
            usersService.delete(users);
            employeesService.update(employees);
            Users user = employees.getUsers();
            personnelResume.setUsers(user);
            personnelResume.setType("离职申请");
            personnelResume.setCompany(usersService.getLoginInfo().getCompany());
            personnelResumeService.save(personnelResume);

        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 会签
     * @return
     */
    public String approve3() {
        if(org.apache.commons.lang.StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(org.apache.commons.lang.StringUtils.isEmpty(comment)) comment="";

        dimission = dimissionService.get(keyId);
        PersonnelResume personnelResume = new PersonnelResume();

        Map<String, Object> var2 = new HashMap<String, Object>();
        // var2.put("curDutyId", curDutyId);
        try {
            dimissionService.approve(dimission, FlowEnum.ProcessState.Running, var2, curDutyId, comment);

            Task overtask = workflowService.getCurrentTask(keyId);
            LogUtil.info("overtask:"+overtask);
            if(overtask==null){
                Dimission entity=dimissionService.get(keyId);
                entity.setProcessState(FlowEnum.ProcessState.Finished);
                dimissionService.update(entity);
                employees=entity.getEmployees();
                if(StringUtils.equals(entity.getPoliticalStatus(),"2")){
                    employees.setIfJob("2");
                }else {
                    employees.setIfJob("1");

                }
                employeesService.update(employees);

                Users user = employees.getUsers();
                personnelResume.setUsers(user);
                personnelResume.setType("离职申请");
                personnelResume.setCompany(usersService.getLoginInfo().getCompany());
                personnelResumeService.save(personnelResume);


            }else {

                if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){

                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 4);
                }
            }

        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }


    /**
     * 退回
     * @return
     */
    public String reject() {
        if (org.apache.commons.lang.StringUtils.isNotEmpty(keyId)) {
            dimission = dimissionService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("dimission");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            System.out.println("comment:"+comment);
            LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            dimissionService.reject(dimission, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (StringUtils.isNotEmpty(keyId)) {
            dimission = dimissionService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("dimission");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:"+key);
            if(StringUtils.isEmpty(comment)){
                comment="";
            }
            dimissionService.deny(dimission, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }

    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getDepId() {
        return depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getNextStepApproversName() {
        return nextStepApproversName;
    }

    public void setNextStepApproversName(String nextStepApproversName) {
        this.nextStepApproversName = nextStepApproversName;
    }

    public String getNextStepApproversId() {
        return nextStepApproversId;
    }

    public void setNextStepApproversId(String nextStepApproversId) {
        this.nextStepApproversId = nextStepApproversId;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public String getApplyUserName() {
        return applyUserName;
    }

    public void setApplyUserName(String applyUserName) {
        this.applyUserName = applyUserName;
    }

    public String getApplyUserId() {
        return applyUserId;
    }

    public void setApplyUserId(String applyUserId) {
        this.applyUserId = applyUserId;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Dimission getDimission() {
        return dimission;
    }

    public void setDimission(Dimission dimission) {
        this.dimission = dimission;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getReportfileId() {
        return reportfileId;
    }

    public void setReportfileId(String reportfileId) {
        this.reportfileId = reportfileId;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getDimissionNature() {
        return dimissionNature;
    }

    public void setDimissionNature(String dimissionNature) {
        this.dimissionNature = dimissionNature;
    }

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    public Set<FileManage> getReport() {
        return report;
    }

    public void setReport(Set<FileManage> report) {
        this.report = report;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPoliticalStatus() {
        return politicalStatus;
    }

    public void setPoliticalStatus(String politicalStatus) {
        this.politicalStatus = politicalStatus;
    }

    public String getPartyMember() {
        return partyMember;
    }

    public void setPartyMember(String partyMember) {
        this.partyMember = partyMember;
    }

    public String getNatureName() {
        return natureName;
    }

    public void setNatureName(String natureName) {
        this.natureName = natureName;
    }

    public String getPoliticalStatusName() {
        return politicalStatusName;
    }

    public void setPoliticalStatusName(String politicalStatusName) {
        this.politicalStatusName = politicalStatusName;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Users getCreater() {
        return creater;
    }

    public void setCreater(Users creater) {
        this.creater = creater;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }

    public String getIfPersonnel() {
        return ifPersonnel;
    }

    public void setIfPersonnel(String ifPersonnel) {
        this.ifPersonnel = ifPersonnel;
    }

    public void setLeaveDate(String leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getLeaveDate() {
        return leaveDate;
    }

    public String getReportText() {
        return reportText;
    }

    public void setReportText(String reportText) {
        this.reportText = reportText;
    }
}
