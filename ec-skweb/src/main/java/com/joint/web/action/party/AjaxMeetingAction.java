package com.joint.web.action.party;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Readers;
import com.joint.base.entity.Remind;
import com.joint.base.entity.Users;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.*;
import com.joint.base.service.activiti.WorkflowService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.MeetingUse;
import com.joint.core.service.MeetingUseService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.*;


@ParentPackage("party")
public class AjaxMeetingAction extends BaseFlowAction {
    @Resource
    private WorkflowService workflowService;
    @Resource
    private MeetingUseService meetingUseService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private PostService postService;
    @Resource
    private PowerService powerService;
    @Resource
    private DutyService dutyService;
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private ReadersService readersService;
    @Resource
    private TaskRecordService taskRecordService;
    @Resource
    private RemindService remindService;

    /**
     * 会议室使用登记对象(读)
     */
    private MeetingUse meetinguse;

    /**
     * 登录人(读)
     */
    private Users loginUser;

    /**
     * 申请会议室（读）
     */
    private List<Map<String, Object>> meetnameDict;

    /**
     * 会议类型（读）
     */
    private List<Map<String, Object>> typeDict;

    /**
     *  申请会议室
     */
    private String meetnamedictId;

    /**
     * 会议类型
     */
    private String typedictId;

    /**
     *  会议议题
     */
    private String topic;

    /**
     * 预计开始时间
     */
    private String begintime;

    /**
     * 预计结束时间
     */
    private String endtime;

    /**
     * 参会人员
     */
    private String joinUsersId;
    /**
     * 备注
     */
    private String remark;

    /**
     * 视图类型
     */
    private String viewtype;

    /**
     * 调整标志
     */
    private String update;

    /**
     * 日历标志
     */
    private String calandar;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "meetinguse";
    }

    public String list(){
        pager = new Pager(0);

        // rows = LISTVIEW_MP_SIZE;
        //  pager.setPageSize(rows);
        // pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();

        params = getSearchFilterParams(_search,params,filters);

        /*内嵌视图这里要加上
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(parentId)){
            manageProInfo = manageProInfoService.get(parentId);
            params.put("manageProInfo",manageProInfo);
        }*/
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1") || viewtype.equals("2")){
                //待使用，已使用
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished});
                params.put("invalid", false);
                pager=meetingUseService.findByPagerAndLimit(true, "meetinguse", pager, params);
            }else if(viewtype.equals("3")){
                //已取消
                params.put("invalid", true);
                pager=meetingUseService.findByPagerAndLimit(true, "meetinguse", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager= meetingUseService.findByPagerAndLimit(true, "meetinguse", pager, params);
        }

        Date today = new Date();
        List<MeetingUse>  meetingUseList;
        if (pager.getTotalCount() > 0){
            meetingUseList = (List<MeetingUse>) pager.getList();
        }else{
            meetingUseList= new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(MeetingUse meetingUse: meetingUseList){
            if(StringUtils.equals("1", viewtype)){
                if(meetingUse.getBegintime() != null){
                    if( today.after(meetingUse.getBegintime())){
                        continue;
                    }
                }
            }
            if(StringUtils.equals("2", viewtype)){
                if(meetingUse.getBegintime() != null){
                    if( today.before(meetingUse.getBegintime())){
                        continue;
                    }
                }
            }
            rMap = new HashMap<String, Object>();
            rMap.put("id", meetingUse.getId());
            rMap.put("type",meetingUse.getType()!=null?meetingUse.getType().getName():"");
            rMap.put("name",meetingUse.getTopic());
            rMap.put("meetname",meetingUse.getMeetname()!=null?meetingUse.getMeetname().getName():"");
            rMap.put("begintime", DataUtil.DateToString(meetingUse.getBegintime(),"yyyy-MM-dd HH:mm"));
            rMap.put("creater", meetingUse.getCreater() != null ? meetingUse.getCreater().getName() : "");
            rMap.put("createDate", DataUtil.DateToString(meetingUse.getCreateDate(),"yyyy-MM-dd"));
            rMap.put("state", meetingUse.getProcessState().value());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);


        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    /*public String list(){
        pager = new Pager(0);
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();

        params = getSearchFilterParams(_search,params,filters);

        *//*内嵌视图这里要加上
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(parentId)){
            manageProInfo = manageProInfoService.get(parentId);
            params.put("manageProInfo",manageProInfo);
        }*//*
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1") || viewtype.equals("2")){
                //待使用，已使用
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished});
                params.put("invalid", false);
                pager=meetingUseService.findByPagerAndLimit(true, "meetinguse", pager, params);
           }else if(viewtype.equals("3")){
                //已取消
                params.put("invalid", true);
                pager=meetingUseService.findByPagerAndLimit(true, "meetinguse", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager= meetingUseService.findByPagerAndLimit(true, "meetinguse", pager, params);
        }

        Date today = new Date();
        List<MeetingUse>  meetingUseList;
        if (pager.getTotalCount() > 0){
            LogUtil.info("recordsNum_1:"+pager.getTotalCount());
            meetingUseList = (List<MeetingUse>) pager.getList();
        }else{
            meetingUseList= new ArrayList<>();
        }
        LogUtil.info("meetingUserLength:"+meetingUseList.size());
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(MeetingUse meetingUse: meetingUseList){
            LogUtil.info("repart_1");
            if(StringUtils.equals("1", viewtype)){
                if(meetingUse.getBegintime() != null){
                    if( today.after(meetingUse.getBegintime())){
                        continue;
                    }
                }
            }
            if(StringUtils.equals("2", viewtype)){
                if(meetingUse.getBegintime() != null){
                    if( today.before(meetingUse.getBegintime())){
                        continue;
                    }
                }
            }
            rMap = new HashMap<String, Object>();
            rMap.put("id", meetingUse.getId());
            rMap.put("type",meetingUse.getType()!=null?meetingUse.getType().getName():"");
            rMap.put("name",meetingUse.getTopic());
            rMap.put("meetname",meetingUse.getMeetname()!=null?meetingUse.getMeetname().getName():"");
            rMap.put("begintime", DataUtil.DateToString(meetingUse.getBegintime(),"yyyy-MM-dd HH:mm"));
            rMap.put("creater", meetingUse.getCreater() != null ? meetingUse.getCreater().getName() : "");
            rMap.put("createDate", DataUtil.DateToString(meetingUse.getCreateDate(),"yyyy-MM-dd"));
            rMap.put("state", meetingUse.getProcessState().value());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);//第几页
        data.put("rows",rows);//每页显示多少行

        long recordsNum = pager.getTotalCount();
        LogUtil.info("recordsNum_2:"+recordsNum);
        List<MeetingUse> meetingUseList1=meetingUseService.getList(params);
        int i=0;
        if(meetingUseList1!=null){
            for(MeetingUse meetingUse:meetingUseList1){
                if(today.after(meetingUse.getBegintime())){
                    i++;
                }
            }
        }
        data.put("total",calcPageNum(recordsNum));//总共的页数
        data.put("records", i);//总共的记录数


        return  ajaxJson(JSONObject.fromObject(data).toString());
    }*/


    public String read(){
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)) {
            meetinguse = meetingUseService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            flowNumStatus = workflowService.getFlowNumStatus(keyId);
        }
        return "read";
    }


    public String input(){
        Company company = usersService.getCompanyByUser();
        List<Dict> meetName = dictService.listFormDefinedEnable(DictBean.DictEnum.MeetName, company.getId());
        List<Dict> meetType = dictService.listFormDefinedEnable(DictBean.DictEnum.MeetType, company.getId());
        Map<String,Object> rMap = null;
        meetnameDict = new ArrayList<Map<String, Object>>();
        typeDict = new ArrayList<Map<String, Object>>();
        if (StringUtils.isNotEmpty(keyId)){
            meetinguse = meetingUseService.get(keyId);
            for(Dict nameObj:meetName){
                rMap = new HashMap<String, Object>();
                rMap.put("id",nameObj.getId());
                rMap.put("name",nameObj.getName());
                rMap.put("selected","");
                if(meetinguse.getMeetname()!=null && StringUtils.equals(nameObj.getId(), meetinguse.getMeetname().getId())){
                    rMap.put("selected","selected");
                }
                meetnameDict.add(rMap);
            }
            for(Dict typeObj:meetType){
                rMap = new HashMap<String, Object>();
                rMap.put("id",typeObj.getId());
                rMap.put("name",typeObj.getName());
                rMap.put("selected","");
                if(meetinguse.getType()!=null && StringUtils.equals(typeObj.getId(), meetinguse.getType().getId())){
                    rMap.put("selected","selected");
                }
                typeDict.add(rMap);
            }
        } else {
            for(Dict nameObj:meetName){
                rMap = new HashMap<String, Object>();
                rMap.put("id",nameObj.getId());
                rMap.put("name",nameObj.getName());
                rMap.put("selected","");
                meetnameDict.add(rMap);
            }
            for(Dict typeObj:meetType){
                rMap = new HashMap<String, Object>();
                rMap.put("id",typeObj.getId());
                rMap.put("name",typeObj.getName());
                rMap.put("selected","");
                typeDict.add(rMap);
            }

        }

        return "input";
    }

    private void setData(){
        if(StringUtils.isNotEmpty(keyId)){
            meetinguse = meetingUseService.get(keyId);
        }else{
            meetinguse = new MeetingUse();
            meetinguse.setCreater(usersService.getLoginInfo());
        }
        if(StringUtils.isNotEmpty(typedictId)){
            meetinguse.setType(dictService.get(typedictId));
        }
        if(StringUtils.isNotEmpty(meetnamedictId)){
            meetinguse.setMeetname(dictService.get(meetnamedictId));
        }
        meetinguse.setTopic(topic);
        meetinguse.setBegintime(DataUtil.StringToDate(begintime, "yyyy-MM-dd HH:mm"));
        meetinguse.setEndtime(DataUtil.StringToDate(endtime, "yyyy-MM-dd HH:mm"));
        List<Users> usersList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(joinUsersId)){
            String[] idArr = joinUsersId.split(",");
            for(String id:idArr){
                usersList.add(usersService.get(id.trim()));
            }
        }
        meetinguse.setJoinUsers(usersList);
        meetinguse.setRemark(remark);
        meetinguse.setCompany(usersService.getLoginInfo().getCompany());
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                meetingUseService.update(meetinguse);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                meetingUseService.save(meetinguse, "meetinguse", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }


    // 提交
    public String commit(){
        setData();
        Company company = usersService.getCompanyByUser();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        Set<Users> usersSet = Sets.newHashSet();
        Map<String, Object> map = new HashMap<String, Object>();
        List<Users> usersList = meetinguse.getJoinUsers();
        for(Users users:usersList){
            usersSet.add(users);
        }

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                meetingUseService.approve(meetinguse, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
            } else {
                keyId =  meetingUseService.commit(meetinguse, FlowEnum.ProcessState.Finished, "meetinguse", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        //先删除读者
        readersService.deleteByKeyIdBussinessKey(keyId);
        //增加归档后读者
        for(Users users:usersSet){
            Readers readers = new Readers();
            readers.setUsers(users);
            readers.setBussinessKey("meetinguse");
            readers.setKeyId(keyId);
            readers.setType(2);
            readersService.save(readers);
            Remind remind = new Remind();
            remind.setUsers(users);
            remind.setBussinessKey(meetinguse.getClass().getName());
            remind.setKeyId(keyId);
            remind.setContent("您有一条会议议程，请查阅。");
            remindService.save(remind);
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 会议调整
     * @return
     */
    public String update(){
        setData();
        if(StringUtils.isNotEmpty(keyId)){
            meetingUseService.update(meetinguse);
            List<Users> usersList = meetinguse.getJoinUsers();
            for(Users users:usersList){
                Remind remind = new Remind();
                remind.setUsers(users);
                remind.setBussinessKey(meetinguse.getClass().getName());
                remind.setKeyId(keyId);
                remind.setContent(meetinguse.getMeetname().getName()+"("+ meetinguse.getTopic() +")现有调整，请查看。");
                remindService.save(remind);
            }
        }else{
            return ajaxHtmlCallback("404", "没有keyId,请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    /**
     * 失效
     * @return
     */
    public String doInvalid(){
        if(StringUtils.isNotEmpty(keyId)) {
            meetinguse = meetingUseService.get(keyId);
            meetinguse.setInvalid(true);
            meetingUseService.update(meetinguse);
            List<Users> usersList = meetinguse.getJoinUsers();
            for(Users users:usersList){
                Remind remind = new Remind();
                remind.setUsers(users);
                remind.setBussinessKey(meetinguse.getClass().getName());
                remind.setKeyId(keyId);
                remind.setContent(meetinguse.getMeetname().getName() + "(" + meetinguse.getTopic() + ")" + "取消，请自行调整个人安排");
                remindService.save(remind);
            }

        }
        return ajaxHtmlCallback("200", "操作成功！", "操作状态");
    }

    /**
     * 检查开始时间
     * @return
     */
    public  String checkBeginTime(){
        pager = new Pager(0);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        Company com = usersService.getCompanyByUser();
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished});
        params.put("invalid", false);
        pager=meetingUseService.findByPagerAndLimit(false, "meetinguse", pager, params);
        Date today = new Date();
        List<MeetingUse>  meetingUseList;
        if (pager.getTotalCount() > 0){
            meetingUseList = (List<MeetingUse>) pager.getList();
        }else{
            meetingUseList= new ArrayList<>();
        }
        List<MeetingUse> meetingUses = new ArrayList<>();
        for(MeetingUse meetingUse: meetingUseList){
            if(StringUtils.equals("1", viewtype)){
                if(meetingUse.getBegintime() != null){
                    if( today.after(meetingUse.getBegintime())){
                        continue;
                    }
                }
            }
            meetingUses.add(meetingUse);
        }
        MeetingUse meetingUse = null;
        if(StringUtils.isNotEmpty(keyId)) {
            meetingUse = meetingUseService.get(keyId);
        }
        meetingUses.remove(meetingUse);
        for(MeetingUse meetingUse1:meetingUses){
            if(meetingUse1.getBegintime() != null && begintime != null && meetingUse1.getEndtime() != null){
                Date begindate = DataUtil.StringToDate(begintime, "yyyy-MM-dd HH:mm");
                if(begindate.after(meetingUse1.getBegintime())
                  && begindate.before(meetingUse1.getEndtime()) ){
                    return ajaxText("false");
                }
            }
        }
        return ajaxText("true");
    }


    /**
     * 检查结束时间
     * @return
     */
    public  String checkEndTime(){
        pager = new Pager(0);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        Company com = usersService.getCompanyByUser();
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished});
        params.put("invalid", false);
        pager=meetingUseService.findByPagerAndLimit(false, "meetinguse", pager, params);
        Date today = new Date();
        List<MeetingUse>  meetingUseList;
        if (pager.getTotalCount() > 0){
            meetingUseList = (List<MeetingUse>) pager.getList();
        }else{
            meetingUseList= new ArrayList<>();
        }
        List<MeetingUse> meetingUses = new ArrayList<>();
        for(MeetingUse meetingUse: meetingUseList){
            if(StringUtils.equals("1", viewtype)){
                if(meetingUse.getBegintime() != null){
                    if( today.after(meetingUse.getBegintime())){
                        continue;
                    }
                }
            }
            meetingUses.add(meetingUse);
        }
        MeetingUse meetingUse = null;
        if(StringUtils.isNotEmpty(keyId)) {
            meetingUse = meetingUseService.get(keyId);
        }
        meetingUses.remove(meetingUse);
        for(MeetingUse meetingUse1:meetingUses){
            if(meetingUse1.getBegintime() != null && endtime != null && meetingUse1.getEndtime() != null){
                Date enddate = DataUtil.StringToDate(endtime, "yyyy-MM-dd HH:mm");
                if(enddate.after(meetingUse1.getBegintime())
                        && enddate.before(meetingUse1.getEndtime()) ){
                    return ajaxText("false");
                }
            }
        }
        return ajaxText("true");
    }

    //检查新建人
    public String checkCreater(){
        Users users = usersService.getLoginInfo();
        if(users == null){
            return ajaxHtmlCallback("400", "您没有操作此文档的权限", "操作状态");
        }
        if(StringUtils.isEmpty(keyId)){
            return ajaxHtmlCallback("400", "您没有操作此文档的权限", "操作状态");
        }
        MeetingUse meetingUse = meetingUseService.get(keyId);
        if(meetingUse == null || meetingUse.getCreater() == null){
            return ajaxHtmlCallback("400", "您没有操作此文档的权限", "操作状态");
        }
        if(StringUtils.equals(meetingUse.getCreater().getId(), users.getId())){
            return ajaxHtmlCallback("200", "身份合法", "操作状态");
        }
        return ajaxHtmlCallback("400", "您没有操作此文档的权限", "操作状态");
    }


    /**
     * 日历数据
     * @return
     */
    public String getCalendarJson(){
        pager = new Pager(0);
        pager.setOrderBy("modifyDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        //所有单子
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
        params.put("invalid", false);
        pager=meetingUseService.findByPagerAndLimit(true, "meetinguse", pager, params);

        List<MeetingUse> meetingUseList;
        if (pager.getTotalCount() > 0){
            meetingUseList = (List<MeetingUse>) pager.getList();
        }else{
            meetingUseList = new ArrayList<>();
        }
        Date today = new Date();
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String,Object> rMap;
        for(MeetingUse meetingUse: meetingUseList){
            if(meetingUse.getBegintime() != null){
                if( today.after(meetingUse.getBegintime())){
                    continue;
                }
            }
            rMap = new HashMap<String, Object>();
            rMap.put("id",meetingUse.getId());
            rMap.put("title",meetingUse.getTopic());
            rMap.put("start", DataUtil.DateToString(meetingUse.getBegintime(), "yyyy-MM-dd HH:mm"));
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        return  ajaxJson(dataRows.toString());


    }


    public MeetingUse getMeetinguse() {
        return meetinguse;
    }

    public void setMeetinguse(MeetingUse meetinguse) {
        this.meetinguse = meetinguse;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public List<Map<String, Object>> getMeetnameDict() {
        return meetnameDict;
    }

    public void setMeetnameDict(List<Map<String, Object>> meetnameDict) {
        this.meetnameDict = meetnameDict;
    }

    public List<Map<String, Object>> getTypeDict() {
        return typeDict;
    }

    public void setTypeDict(List<Map<String, Object>> typeDict) {
        this.typeDict = typeDict;
    }

    public String getMeetnamedictId() {
        return meetnamedictId;
    }

    public void setMeetnamedictId(String meetnamedictId) {
        this.meetnamedictId = meetnamedictId;
    }

    public String getTypedictId() {
        return typedictId;
    }

    public void setTypedictId(String typedictId) {
        this.typedictId = typedictId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getBegintime() {
        return begintime;
    }

    public void setBegintime(String begintime) {
        this.begintime = begintime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getJoinUsersId() {
        return joinUsersId;
    }

    public void setJoinUsersId(String joinUsersId) {
        this.joinUsersId = joinUsersId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getCalandar() {
        return calandar;
    }

    public void setCalandar(String calandar) {
        this.calandar = calandar;
    }
}

