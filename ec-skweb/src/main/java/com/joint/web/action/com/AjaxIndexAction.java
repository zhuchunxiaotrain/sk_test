package com.joint.web.action.com;


import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Permission;
import com.joint.base.entity.Users;
import com.joint.base.service.FileManageService;
import com.joint.base.service.PermissionService;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.Notice;
import com.joint.core.service.NoticeService;
import com.joint.web.action.BaseAdminAction;
import net.sf.json.JSONObject;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.*;


@ParentPackage("com")
public class AjaxIndexAction extends BaseAdminAction {


    private static final long serialVersionUID = -6732809121268618245L;
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private UsersService usersService;
    @Resource
    private TaskService taskService;
    @Resource
    private FileManageService fileManageService;
    @Resource
    private PermissionService permissionService;
    @Resource
    private NoticeService noticeService;

    private Users user;
    private String password;
    private String username;
    private String _t;
    private String filePath;
    private String ajax;

    private List<Permission> permissionList;

    private Date today;

    /**
     * 主页
     * @return
     */
    public String page(){
        today = new Date();
        LogUtil.info("today:" + today);
        return "page";
    }

    /**
     * 日历，公告数据
     * @return
     */
    public String getNoticeJson(){
        pager = new Pager(0);
        pager.setOrderBy("modifyDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
        pager=noticeService.findByPagerAndLimit(true, "notice", pager, params);

        List<Notice> noticeList;
        if (pager.getTotalCount() > 0){
            noticeList = (List<Notice>) pager.getList();
        }else{
            noticeList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String,Object> rMap;
        for(Notice notice: noticeList){
            if(notice.isTop()){
                rMap = new HashMap<String, Object>();
                rMap.put("id",notice.getId());
                rMap.put("title",notice.getTopic());
               // rMap.put("textColor","red");
                rMap.put("start", DataUtil.DateToString(notice.getModifyDate(), "yyyy-MM-dd HH:mm"));
                JSONObject o = JSONObject.fromObject(rMap);
                dataRows.add(o);
            }
        }
        for(Notice notice: noticeList){
            if(!notice.isTop()){
                rMap = new HashMap<String, Object>();
                rMap.put("id",notice.getId());
                rMap.put("title",notice.getTopic());
                rMap.put("start", DataUtil.DateToString(notice.getModifyDate(), "yyyy-MM-dd HH:mm"));
                JSONObject o = JSONObject.fromObject(rMap);
                dataRows.add(o);
            }
        }
        return  ajaxJson(dataRows.toString());
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String get_t() {
        return _t;
    }

    public void set_t(String _t) {
        this._t = _t;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public List<Permission> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<Permission> permissionList) {
        this.permissionList = permissionList;
    }

    public String getAjax() {
        return ajax;
    }

    public void setAjax(String ajax) {
        this.ajax = ajax;
    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }
}
