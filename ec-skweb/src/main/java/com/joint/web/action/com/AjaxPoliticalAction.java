package com.joint.web.action.com;

import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Users;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.core.entity.Employees;
import com.joint.core.service.EmployeesService;
import com.joint.core.service.PoliticalService;
import com.joint.web.action.BaseFlowAction;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by root on 16/12/16.
 */
@ParentPackage("com")
public class AjaxPoliticalAction extends BaseFlowAction {
    @Resource
    private PoliticalService politicalService;
    @Resource
    private DictService dictService;
    @Resource
    private EmployeesService employeesService;

    private Employees employees;
    private Users loginUser;
    protected ArrayList<Map<String,Object>> typesArrayList;

    /**
     * 是否党员(0/1)
     */
    private String partyMember;

    /**
     * 入党时间
     */
    private Date partyDate;

    /**
     * 介绍人
     */
    private String introducer;

    /**
     * 工会费
     */
    private BigDecimal dues;

    /**
     * 党费缴纳基数
     */
    private BigDecimal baseMoney;
    /**
     * 视图类型
     */
    private String viewtype;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "political";
    }


    /**
     * 新建
     * @return
     */
    public String input(){
        loginUser = usersService.getLoginInfo();
        if (StringUtils.isNotEmpty(keyId)){
            employees = employeesService.get(keyId);
            LogUtil.info("AAA"+employees);
        }
        return "input";
    }


    /**
     * 查看
     * @return
     */
    public String read(){
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)) {
            employees = employeesService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);

        }
        return "read";
    }

    private void setData(){

        if(StringUtils.isNotEmpty(keyId)) {
            employees = employeesService.get(keyId);
            employees.setPartyMember(partyMember);
            employees.setPartyDate(partyDate);
            employees.setIntroducer(introducer);
            employees.setDues(dues);
            employees.setBaseMoney(baseMoney);
        }

    }

    /**
     * 提交
     * @return
     */
    public String commit(){

        setData();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 2);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 3);
        var2.put("curDutyId", curDutyId);
        ArrayList<String> list = new ArrayList<String>();
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                employeesService.commit(employees, FlowEnum.ProcessState.Finished,"employees", var1,var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public ArrayList<Map<String, Object>> getTypesArrayList() {
        return typesArrayList;
    }

    public void setTypesArrayList(ArrayList<Map<String, Object>> typesArrayList) {
        this.typesArrayList = typesArrayList;
    }

    public String getPartyMember() {
        return partyMember;
    }

    public void setPartyMember(String partyMember) {
        this.partyMember = partyMember;
    }

    public Date getPartyDate() {
        return partyDate;
    }

    public void setPartyDate(Date partyDate) {
        this.partyDate = partyDate;
    }

    public String getIntroducer() {
        return introducer;
    }

    public void setIntroducer(String introducer) {
        this.introducer = introducer;
    }

    public BigDecimal getDues() {
        return dues;
    }

    public void setDues(BigDecimal dues) {
        this.dues = dues;
    }

    public BigDecimal getBaseMoney() {
        return baseMoney;
    }

    public void setBaseMoney(BigDecimal baseMoney) {
        this.baseMoney = baseMoney;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }
}
