package com.joint.web.action.com;

import com.fz.us.base.bean.Result;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.joint.base.entity.*;
import com.joint.base.service.*;
import com.joint.base.util.StringUtils;
import com.joint.web.action.BaseAdminAction;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by hpj on 2015/2/10.
 */
@ParentPackage("com")
public class AjaxCommoncfgAction extends BaseAdminAction {

    @Resource
    private CommonConfigService commonConfigService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private PostService postService;
    @Resource
    private RoleService roleService;
    @Resource
    private PowerService powerService;


    private CommonConfig commonConfig;
    private List<String> type;
    private String roleId;
    private String departId;
    private String postId;
    private String powerId;
    private String bcfgId;
    private String selectType;
    private String sType;
    private String variable;
    private String allMans;

    public String save(){
        if(StringUtils.isNotEmpty(keyId)){
            commonConfig=commonConfigService.get(keyId);
        }else{
            commonConfig=new CommonConfig();
        }
        Map<String,Object> rMap=new HashMap<String,Object>();
        Map<String,Object> data = Maps.newHashMap();
        List<JSONObject> dataRows = Lists.newArrayList();

        commonConfig.setStype(getCtype(type));
        commonConfig.setVariable(variable);
        if(StringUtils.equals(allMans,"1")){
            commonConfig.setAllmans(true);
        }else{
            commonConfig.setAllmans(false);
        }
        commonConfig.setRoleSet(getRoleSpit(roleId));
        commonConfig.setDepartmentSet(getDepartSpit(departId));
        commonConfig.setPostSet(getPostSpit(postId));
        commonConfig.setPowerSet(getPowerSpit(powerId));
        commonConfig.setCreater(usersService.getLoginInfo());
        commonConfig.setCompany(usersService.getCompanyByUser());



        rMap.put("state","200");
        rMap.put("title","操作状态");
        rMap.put("message","保存成功");

        if(StringUtils.isNotEmpty(keyId)){
            if(type!=null && type.size()>0){
                if(!type.contains("000001")){
                    commonConfig.setRoleSet(null);
                }
                if(!type.contains("000010")){
                    commonConfig.setDepartmentSet(null);
                }
                if(!type.contains("000100")){
                    commonConfig.setPostSet(null);
                }
                if(!type.contains("001000")){
                    commonConfig.setPowerSet(null);
                }
                if(!type.contains("010000")){
                    commonConfig.setVariable(null);
                }
                if(!type.contains("100000")){
                    commonConfig.setAllmans(false);
                }
            }else{
                commonConfig.setRoleSet(null);
                commonConfig.setDepartmentSet(null);
                commonConfig.setPostSet(null);
                commonConfig.setPowerSet(null);
                commonConfig.setVariable(null);
                commonConfig.setAllmans(false);
            }

            commonConfigService.update(commonConfig);
            keyId = commonConfig.getId();
            rMap.put("message", "更新成功");
        }else{
            keyId = commonConfigService.save(commonConfig);
        }
        rMap.put("keyId", keyId);
        rMap.put("selectType",selectType);

        dataRows.add(JSONObject.fromObject(rMap));
        data.put("data", JSONArray.fromObject(dataRows));
        Result rs = new Result(200,"操作成功",data);
        return ajaxHtmlAppResult(rs);
    }

    public String read(){
        if (StringUtils.isNotEmpty(keyId)){
            commonConfig=commonConfigService.get(keyId);
            sType = fillChar(String.valueOf(commonConfig.getStype()));
        }
        return "read";
    }
    public String input(){
        if (StringUtils.isNotEmpty(keyId)){
            commonConfig=commonConfigService.get(keyId);
            sType = fillChar(String.valueOf(commonConfig.getStype()));
        }
        return "input";
    }
    private int getCtype(List<String> typeList){
        int cType = -1;
        if(typeList != null ){
            cType = 0;
            for(String type : typeList){
                cType = Integer.parseInt(type) + cType;
            }
        }
        return cType;
    }

    private Set<Department> getDepartSpit(String str){
        Set<Department> departSet=new HashSet<Department>();
        String[] ary=str.split(",");
        for(String item:ary){
            if(StringUtils.isNotEmpty(item)) {
                departSet.add(departmentService.get(item));
            }
        }
        return  departSet;
    }
    private Set<Post> getPostSpit(String str){
        Set<Post> postSet=new HashSet<Post>();
        String[] ary=str.split(",");
        for(String item:ary){
            if(StringUtils.isNotEmpty(item)) {
                postSet.add(postService.get(item));
            }
        }
        return  postSet;
    }
    private Set<Role> getRoleSpit(String str){
        Set<Role> roleSet = Sets.newHashSet();
        String[] ary=str.split(",");
        for(String item:ary){
            if(StringUtils.isNotEmpty(item)) {
                roleSet.add(roleService.get(item));
            }
        }
        return  roleSet;
    }
    private Set<Power> getPowerSpit(String str){
        Set<Power> powerSet = Sets.newHashSet();
        String[] ary=str.split(",");
        for(String item:ary){
            if(StringUtils.isNotEmpty(item)) {
                powerSet.add(powerService.get(item));
            }
        }
        return  powerSet;
    }
    /**
     * 字符串填充
     * @param str
     * @return
     */
    private String fillChar(String str){
        if(str.equals("-1")){
            return "000000";
        }
        int len = str.length();
        String type = str;
        for(int i=len;i<6;i++){
            type = "0"+type;
        }

        return type;
    }

    public CommonConfig getCommonConfig() {
        return commonConfig;
    }

    public void setCommonConfig(CommonConfig commonConfig) {
        this.commonConfig = commonConfig;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPowerId() {
        return powerId;
    }

    public void setPowerId(String powerId) {
        this.powerId = powerId;
    }

    public String getBcfgId() {
        return bcfgId;
    }

    public void setBcfgId(String bcfgId) {
        this.bcfgId = bcfgId;
    }

    public String getSelectType() {
        return selectType;
    }

    public void setSelectType(String selectType) {
        this.selectType = selectType;
    }

    public String getSType() {
        return sType;
    }

    public void setSType(String sType) {
        this.sType = sType;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getAllMans() {
        return allMans;
    }

    public void setAllMans(String allMans) {
        this.allMans = allMans;
    }
}
