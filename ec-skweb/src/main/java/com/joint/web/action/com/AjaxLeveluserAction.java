package com.joint.web.action.com;


import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.BaseEnum.StateEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.PinyinUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.joint.base.bean.EnumManage;
import com.joint.base.entity.*;
import com.joint.base.entity.system.Admin;
import com.joint.base.service.*;
import com.joint.base.util.DataUtil;
import com.joint.web.action.BaseAdminAction;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by zhucx on 2014/9/16.
 */
@ParentPackage("com")
public class AjaxLeveluserAction extends BaseAdminAction {

    @Resource
    private AdminService adminService;
    @Resource
    private UsersService usersService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private PostService postService;
    @Resource
    private DutyService dutyService;
    @Resource
    private TaskService taskService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private HistoryService historyService;
    @Resource
    private FileManageService fileManageService;
    @Resource
    private PowerService powerService;
    @Resource
    private ProcessConfigService processConfigService;
    @Resource
    private PermissionService permissionService;



    protected Users users;
    private List<Users> usersList;
    private List<Department> departmentList;
    private List<Duty> dutyList;
    protected Company company;
    private String pid;

    /**
     *返回人员管理视图
     * @return
     */
    /**
     * 返回部门和岗位视图
     * @return
     */
    public String subGrid(){
        if(StringUtils.isEmpty(pid)){
            return  null;
        }
        Set<Users> usersSet = Sets.newHashSet();
        Department depart = departmentService.get(pid);
        dutyList = dutyService.getDutys(depart);
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        for(Duty duty:dutyList){
            if(params.get("name") != null && duty.getUsers().getName().indexOf(params.get("name").toString()) == -1 ){
                continue;
            }
            if(params.get("post") != null  ){
                if (duty.getPower() != null) {
                    if(duty.getPower().getPost().getName().indexOf(params.get("post").toString()) == -1){
                        continue;
                    }
                }else{
                    continue;
                }
            }
            usersSet.add(duty.getUsers());
        }

        for(Users users:usersSet){
            List<Duty> dutyList = dutyService.getDutys(depart, users);
            String postStr = "";
            for(Duty duty:dutyList){
                postStr += duty.getPost().getName()+",";
            }
            if(StringUtils.isNotEmpty(postStr)){
                postStr = postStr.substring(0, postStr.length()-1);
            }
            rMap=new HashMap<String, Object>();
            rMap.put("name",users.getName());
            rMap.put("createDate", DataUtil.DateTimeToString(users.getCreateDate()));
            rMap.put("id", users.getId());
            rMap.put("post", postStr);
            dataRows.add(JSONObject.fromObject(rMap));
        }
        data.put("dataRows", dataRows);
        return  ajaxJson(JSONObject.fromObject(data).toString());

    }
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Duty> getDutyList() {
        return dutyList;
    }

    public void setDutyList(List<Duty> dutyList) {
        this.dutyList = dutyList;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}