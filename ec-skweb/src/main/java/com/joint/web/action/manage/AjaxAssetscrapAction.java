package com.joint.web.action.manage;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.DepartmentService;
import com.joint.base.service.activiti.WorkflowService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.AssetScrap;
import com.joint.core.entity.manage.AssetUse;
import com.joint.core.service.AssetScrapService;
import com.joint.core.service.AssetUseService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 * Created by ZhuChunXiao on 2017/3/30.
 */
@ParentPackage("manage")
public class AjaxAssetscrapAction extends BaseFlowAction {
    @Resource
    private WorkflowService workflowService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private AssetUseService assetUseService;
    @Resource
    private AssetScrapService assetScrapService;
    @Resource
    private DictService dictService;

    /**
     * 报废原因
     */
    private String scrapReasonId;
    private List<Map<String, Object>> scrapReasonDict;
    /**
     * 是否是产业中心人员
     */
    private int ifCentralStaff;
    /**
     * 是否需要上报中心
     */
    private int leaderExam;
    /**
     * 资产使用登记表的数据
     */
    private String assetUseId;
    private List<Map<String,Object>> assetUseList;

    private AssetUse assetUse;
    private AssetScrap assetScrap;
    private Users loginUser;
    private String viewtype;
    private List<AssetScrap> assetScrapList;

    public String execute(){
        return "assetscrap";
    }

    public String list(){
        pager = new Pager(0);
        /*
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);*/
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Map<String,Object> params = new HashMap<String,Object>();
       // params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                pager=assetScrapService.findByPagerAndLimit(false, "assetscrap", pager, params);
            }else if(viewtype.equals("2")){
                //已报废
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                pager=assetScrapService.findByPagerAndFinish( "assetscrap", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=assetScrapService.findByPagerAndLimit(true, "assetscrap", pager, params);
        }

        List<AssetScrap> assetScrapList;
        if (pager.getTotalCount() > 0){
            assetScrapList = (List<AssetScrap>) pager.getList();
        }else{
            assetScrapList= new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(AssetScrap assetScrap: assetScrapList){
            rMap = new HashMap<String, Object>();
            rMap.put("id",assetScrap.getId());
            rMap.put("year",DataUtil.DateToString(assetScrap.getCreateDate(), "yyyy"));
            rMap.put("month",DataUtil.DateToString(assetScrap.getCreateDate(), "MM"));
            rMap.put("no",assetScrap.getAssetUse().getNo());
            rMap.put("name",assetScrap.getAssetUse().getName());
            rMap.put("type",assetScrap.getAssetUse().getType());
            rMap.put("money",assetScrap.getAssetUse().getMoney());
            rMap.put("reason",assetScrap.getScrapReason().getName());
            rMap.put("state",assetScrap.getProcessState().value());
            if(assetScrap.getProcessState().value().equals("已归档")){
                rMap.put("state","已报废");
            }
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String read(){
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)) {
            assetScrap = assetScrapService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            flowNumStatus = workflowService.getFlowNumStatus(keyId);
        }
        return "read";
    }

    public String input(){
        Company company = usersService.getCompanyByUser();
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
        List<AssetUse> list = (List<AssetUse>)assetUseService.findByPagerAndCompany(null, null, company, params).getList();
        List<Dict> scrapReasonList = dictService.listFormDefinedEnable(DictBean.DictEnum.ScrapReason, usersService.getCompanyByUser().getId());
        assetUseList= Lists.newArrayList();
        Map<String,Object> usemap;
        Map<String,Object> rMap = null;
        scrapReasonDict = new ArrayList<Map<String, Object>>();
        if (StringUtils.isNotEmpty(keyId)){
            assetScrap=assetScrapService.get(keyId);
            for(Dict scrapReason:scrapReasonList){
                rMap = new HashMap<String, Object>();
                rMap.put("id",scrapReason.getId());
                rMap.put("name",scrapReason.getName());
                rMap.put("selected","");
                if(assetScrap.getScrapReason()!=null && StringUtils.equals(scrapReason.getId(), assetScrap.getScrapReason().getId())){
                    rMap.put("selected","selected");
                }
                scrapReasonDict.add(rMap);
            }
            for(AssetUse a:list){
                usemap= Maps.newHashMap();
                usemap.put("id", a.getId());
                usemap.put("name", a.getName());
                if(assetScrap.getAssetUse()!=null && StringUtils.equals(a.getId(), assetScrap.getAssetUse().getId())) {
                    usemap.put("selected","selected");
                }
                assetUseList.add(usemap);
            }
        } else {
            for(Dict scrapReason:scrapReasonList){
                rMap = new HashMap<String, Object>();
                rMap.put("id",scrapReason.getId());
                rMap.put("name",scrapReason.getName());
                rMap.put("selected","");
                scrapReasonDict.add(rMap);
            }
            if(list.size()>0){
                for(AssetUse a:list){
                    usemap= Maps.newHashMap();
                    usemap.put("id", a.getId());
                    usemap.put("name", a.getName());
                    usemap.put("selected","");
                    assetUseList.add(usemap);
                }
            }
        }
        return "input";
    }

    public void setData(){
        if(StringUtils.isNotEmpty(keyId)){
            assetScrap=assetScrapService.get(keyId);
        }else{
            assetScrap=new AssetScrap();
            assetScrap.setCreater(usersService.getLoginInfo());
        }
        assetScrap.setAssetUse(assetUseService.get(assetUseId));
        assetScrap.setScrapReason(dictService.get(scrapReasonId));
        assetScrap.setCompany(usersService.getLoginInfo().getCompany());
    }

    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                assetScrapService.update(assetScrap);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                assetScrapService.save(assetScrap, "assetscrap", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        System.out.println("#####刚刚提交");
        setData();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);

        List<String> list=Lists.newArrayList();
        Set<Duty> dutySet=usersService.getLoginInfo().getDutySet();
        for(Duty duty:dutySet){
            Department dept=duty.getDepartment().getParent()==null?duty.getDepartment():duty.getDepartment().getParent();
            Set<Department> deptSet=dept.getChildren();
            for(Department cdept:deptSet){
                if(cdept.getName().contains("财务科")){
                    Set<Power> powers = cdept.getPowerSet();
                    for(Power power:powers){
                        if(power.getPost().getName().equals("科长")){
                            List<Users> usersList=dutyService.getPersons(cdept,power.getPost());
                            for(Users user:usersList){
                                list.add(user.getId());
                            }
                        }
                    }
                }

            }
        }
        var2.put("approver", list);
        var1.put("approver", list);

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                assetScrapService.approve(assetScrap, FlowEnum.ProcessState.Running, var2, curDutyId, comment);
            } else {
                keyId = assetScrapService.commit(assetScrap, "assetscrap", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批1
    public String approve1(){
        System.out.println("#####进入第一步审批");
        ifCentralStaff=0;
        assetScrap = assetScrapService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        Set<Duty> dutySet = assetScrap.getCreater().getDutySet();
        for (Duty duty:dutySet){
            if(duty.getDepartment().getParent()!=null){
                if(StringUtils.equals(duty.getDepartment().getParent().getName(),"产业中心")){
                    ifCentralStaff=1;
                    break;
                }
            }
        }
        if(ifCentralStaff==1){
            var1.put("numStatus",3);
            System.out.println("#####是产业人员，进3");
        }else{
            var1.put("numStatus",2);
            System.out.println("#####不是产业人员，进2");
        }
        var1.put("ifCentralStaff", ifCentralStaff);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                assetScrapService.approve(assetScrap, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批2
    public String approve2(){
        System.out.println("#####进入第2步审批");
        assetScrap = assetScrapService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(leaderExam==1){
            var1.put("numStatus", 3);
            System.out.println("#####需要中心审核，进3");
        }else{
            var1.put("numStatus", 6);
            System.out.println("#####不需要中心审核，进6");
        }
        assetScrap.setLeaderExam(leaderExam == 1 ? true : false);
        var1.put("leaderExam", leaderExam);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                assetScrapService.approve(assetScrap, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批3
    public String approve3(){
        System.out.println("#####进入第3步审批");
        assetScrap = assetScrapService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        var1.put("numStatus", 4);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                assetScrapService.approve(assetScrap, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批4
    public String approve4(){
        System.out.println("#####进入第4步审批");
        assetScrap = assetScrapService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        var1.put("numStatus", 5);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                assetScrapService.approve(assetScrap, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批5
    public String approve5(){
        System.out.println("#####进入第5步审批");
        assetScrap = assetScrapService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        var1.put("numStatus", 6);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                assetScrapService.approve(assetScrap, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批6
    public String approve6(){
        System.out.println("#####进入第6步审批，结束");
        assetScrap = assetScrapService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        var1.put("numStatus", 7);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                assetScrapService.approve(assetScrap, FlowEnum.ProcessState.Finished, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (StringUtils.isNotEmpty(keyId)) {
            assetScrap = assetScrapService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("assetscrap");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:" + key);
            // System.out.println("comment:"+comment);
            LogUtil.info("numStatus:" + numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            assetScrapService.reject(assetScrap, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (StringUtils.isNotEmpty(keyId)) {
            assetScrap = assetScrapService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("assetscrap");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:" + key);
            if(StringUtils.isEmpty(comment)){
                comment="";
            }
            assetScrapService.deny(assetScrap, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }

    /**
     * 获取资产使用的数据
     * @return
     */
    public String getAssetUseInfo(){
        Map<String, Object> map = new HashMap<String, Object>();
        if(StringUtils.isNotEmpty(assetUseId)){
            assetUse=assetUseService.get(assetUseId);
            map.put("no",assetUse.getNo());
            map.put("type",assetUse.getType());
            map.put("date", DataUtil.DateToString(assetUse.getDate()));
            map.put("money", assetUse.getMoney());
        }
        return ajaxHtmlAppResult(1, "", JSONObject.fromObject(map));
    }

    public void exportExcel()  {
        pager = new Pager(0);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("state", new BaseEnum.StateEnum[]{BaseEnum.StateEnum.Enable});
        Company company = usersService.getCompanyByUser();
        pager = assetScrapService.findByPagerAndCompany(pager,null,company,params);
        assetScrapList = (List<AssetScrap>) pager.getList();
        List<AssetScrap> list1 = new ArrayList();
        OutputStream outputStream = null;
        HSSFWorkbook workbook = new HSSFWorkbook();
        // 生成一个表格
        HSSFSheet sheet = workbook.createSheet("Sheet1");
        for(int i=0; i<=5;i++){
            sheet.setColumnWidth(i, 20 * 256);
        }
        CellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//粗体显示
        style.setFont(font);
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("固定资产名称");
        row.createCell(1).setCellValue("固定资产型号");
        row.createCell(2).setCellValue("固定资产编号");
        row.createCell(3).setCellValue("购买日期");
        row.createCell(4).setCellValue("采购金额");
        row.createCell(5).setCellValue("报废原因");
        for(int i=0; i<=5;i++){
            row.getCell(i).setCellStyle(style);
        }
        String fileName = "资产报废登记表.xls";
        for(AssetScrap assetScrap:assetScrapList){
            list1.add(assetScrap);
        }
        for(int i=0; i<list1.size(); i++){
            AssetScrap assetScrap = list1.get(i);
            row = sheet.createRow(i+1);
            row.createCell(0).setCellValue(assetScrap.getAssetUse().getName());
            row.createCell(1).setCellValue(assetScrap.getAssetUse().getType());
            row.createCell(2).setCellValue(assetScrap.getAssetUse().getNo());
            row.createCell(3).setCellValue(DataUtil.DateToString(assetScrap.getAssetUse().getDate()));
            row.createCell(4).setCellValue(assetScrap.getAssetUse().getMoney());
            row.createCell(5).setCellValue(assetScrap.getScrapReason().getName());
        }
        try {
            outputStream = new FileOutputStream("/temp/"+fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        OutputStream out = null;
        HttpServletRequest request = ServletActionContext.getRequest();
        //  if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {

        //  }
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("text/plain;charset=utf-8");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        try {
            response.setHeader("content-disposition", "attachment;filename="+new String(fileName.getBytes("UTF-8"), "ISO8859-1"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            out = response.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputStream in = null;
        try {
            in = new FileInputStream("/temp/"+fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        byte[] buffer = new byte[1024];
        int i = -1;
        try {
            while ((i = in.read(buffer)) != -1) {
                out.write(buffer, 0, i);
            }
            out.flush();
            out.close();
            in.close();
            out = null;
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public String getScrapReasonId() {
        return scrapReasonId;
    }

    public void setScrapReasonId(String scrapReasonId) {
        this.scrapReasonId = scrapReasonId;
    }

    public List<Map<String, Object>> getScrapReasonDict() {
        return scrapReasonDict;
    }

    public void setScrapReasonDict(List<Map<String, Object>> scrapReasonDict) {
        this.scrapReasonDict = scrapReasonDict;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }

    public int getLeaderExam() {
        return leaderExam;
    }

    public void setLeaderExam(int leaderExam) {
        this.leaderExam = leaderExam;
    }

    public AssetUse getAssetUse() {
        return assetUse;
    }

    public void setAssetUse(AssetUse assetUse) {
        this.assetUse = assetUse;
    }

    public AssetScrap getAssetScrap() {
        return assetScrap;
    }

    public void setAssetScrap(AssetScrap assetScrap) {
        this.assetScrap = assetScrap;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public List<Map<String, Object>> getAssetUseList() {
        return assetUseList;
    }

    public void setAssetUseList(List<Map<String, Object>> assetUseList) {
        this.assetUseList = assetUseList;
    }

    public String getAssetUseId() {
        return assetUseId;
    }

    public void setAssetUseId(String assetUseId) {
        this.assetUseId = assetUseId;
    }

    public List<AssetScrap> getAssetScrapList() {
        return assetScrapList;
    }

    public void setAssetScrapList(List<AssetScrap> assetScrapList) {
        this.assetScrapList = assetScrapList;
    }
}
