package com.joint.web.action.manage;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.*;
import com.joint.base.service.activiti.WorkflowService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.Dispatch;
import com.joint.core.service.DispatchService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;
import javax.annotation.Resource;
import java.util.*;


@ParentPackage("manage")
public class AjaxDispatchAction extends BaseFlowAction {
    @Resource
    private WorkflowService workflowService;
    @Resource
    private DispatchService dispatchService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private PostService postService;
    @Resource
    private PowerService powerService;
    @Resource
    private DutyService dutyService;
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private ReadersService readersService;
    @Resource
    private TaskRecordService taskRecordService;

    /**
     * 发文申请对象(读)
     */
    private Dispatch dispatch;

    /**
     * 登录人(读)
     */
    private Users loginUser;
    /**
     * 附件上传Id(读)
     */
    private String fileId;

    /**
     * 发文类别（读）
     */
    private List<Map<String, Object>> typeDict;

    /**
     * 中心审核对象(读)
     */
    private List<Map<String,Object>> examObject;

    /**
     *  发文类别
     */
    private String typedictId;
    /**
     *  发文标题
     */
    private String title;
    /**
     * 文件编号
     */
    private String fileNo;
    /**
     * 发文时间
     */
    private String time;
    /**
     * 发文对象
     */
    private String object;
    /**
     * 是否具有有效期
     */
    private String hasValid;
    /**
     * 有效期
     */
    private String validDate;
    /**
     * 通知部门
     */
    private String noticeDepartmentId;
    /**
     * 通知个人
     */
    private String noticeUsersId;
    /**
     * 附件上传Id
     */
    private String fileIds;
    /**
     * 视图类型
     */
    private String viewtype;
    /**
     * 职责列表
     */
    private List<Duty> dutyList;

    /**
     * 附件列表
     */
    private Set<String> fileUrl;

    /**
     * 是否需要会签
     */
    private String mulExam;

    /**
     * 会签人员id
     */
    private String examUsersId;

    /**
     * 是否需要中心领导审核
     */
    private String leaderExam;

    /**
     * 中心审核对象Id
     */
    private String examObjectId;
    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "dispatch";
    }



    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        if(viewtype !=null && viewtype.equals("2")){
            pager.setOrderBy("modifyDate");
        }
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();

        params = getSearchFilterParams(_search,params,filters);

        /*内嵌视图这里要加上
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(parentId)){
            manageProInfo = manageProInfoService.get(parentId);
            params.put("manageProInfo",manageProInfo);
        }*/
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
      //  LogUtil.info("viewtype:" + viewtype);
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                params.put("invalid", false);
                pager=dispatchService.findByPagerAndLimit(false, "dispatch", pager, params);
            }else if(viewtype.equals("2")){
                //已归档
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                params.put("invalid", false);
                pager=dispatchService.findByPagerAndFinish( "dispatch", pager, params);
            }else if(viewtype.equals("3")){
                //已失效
                params.put("invalid", true);
                pager=dispatchService.findByPagerAndLimit(true, "dispatch", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=dispatchService.findByPagerAndLimit(true, "dispatch", pager, params);
        }

        List<Dispatch>  dispatchList;
        if (pager.getTotalCount() > 0){
            dispatchList = (List<Dispatch>) pager.getList();
        }else{
            dispatchList= new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(Dispatch dispatch: dispatchList){
            rMap = new HashMap<String, Object>();
            rMap.put("id",dispatch.getId());
            rMap.put("type",dispatch.getType() != null?dispatch.getType().getName():"");
            rMap.put("fileNo",dispatch.getFileNo());
            rMap.put("title",dispatch.getTitle());
            rMap.put("time", DataUtil.DateToString(dispatch.getTime()));
            rMap.put("creater", dispatch.getCreater() != null ? dispatch.getCreater().getName() : "");
            rMap.put("createDate", DataUtil.DateToString(dispatch.getCreateDate(),"yyyy-MM-dd"));
            rMap.put("state",dispatch.getProcessState().value());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);


        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    public String read(){
        loginUser = usersService.getLoginInfo();

        if(StringUtils.isNotEmpty(keyId)) {
            fileId ="";
            dispatch = dispatchService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            flowNumStatus = workflowService.getFlowNumStatus(keyId);
            if(dispatch.getFile() != null && dispatch.getFile().size()>0){
                for(FileManage f:dispatch.getFile()){
                    fileId+=f.getId()+",";
                }
            }

        }
        examObject = Lists.newArrayList();
        Department department = null;
        List<Department> departmentList = departmentService.getDepartments("产业中心");
        if(departmentList.size()>0){
            department = departmentList.get(0);
        }
        Post post = postService.getPostByName("书记");
        Post post2 = postService.getPostByName("主任");
        List<Post> postList = Lists.newArrayList();
        postList.add(post);
        postList.add(post2);
        Map<String,Object> rmap;
        if(department != null ){
            for(Post post1:postList){
                if(post1 != null){
                    rmap= Maps.newHashMap();
                    Power power = powerService.getPowerByDepartAndPost(department, post1);
                    rmap.put("id", power.getId());
                    rmap.put("name", power.getName());
                    rmap.put("checked","");
                    for(Power power1:dispatch.getExamObject()){
                        if(StringUtils.equals(power1.getId(),power.getId())){
                            rmap.put("checked","checked");
                        }
                    }
                    examObject.add(rmap);
                }
            }

        }
        return "read";
    }


    public String input(){
        Company company = usersService.getCompanyByUser();
        List<Dict> dispatchType = dictService.listFormDefinedEnable(DictBean.DictEnum.DispatchType, usersService.getCompanyByUser().getId());
        Map<String,Object> rMap = null;
        typeDict = new ArrayList<Map<String, Object>>();
        if (StringUtils.isNotEmpty(keyId)){
            fileId ="";
            dispatch = dispatchService.get(keyId);
            if(dispatch.getFile() != null && dispatch.getFile().size()>0){
                for(FileManage f:dispatch.getFile()){
                    fileId+=f.getId()+",";
                }
            }
            for(Dict typeObj:dispatchType){
                rMap = new HashMap<String, Object>();
                rMap.put("id",typeObj.getId());
                rMap.put("name",typeObj.getName());
                rMap.put("selected","");
                if(dispatch.getType()!=null && StringUtils.equals(typeObj.getId(), dispatch.getType().getId())){
                    rMap.put("selected","selected");
                }
                typeDict.add(rMap);
            }
        } else {
            for(Dict typeObj:dispatchType){
                rMap = new HashMap<String, Object>();
                rMap.put("id",typeObj.getId());
                rMap.put("name",typeObj.getName());
                rMap.put("selected","");
                typeDict.add(rMap);
            }

        }

        return "input";
    }

    private void setData(){
        if(StringUtils.isNotEmpty(keyId)){
            dispatch = dispatchService.get(keyId);
        }else{
            dispatch = new Dispatch();
            dispatch.setCreater(usersService.getLoginInfo());
        }
        if(StringUtils.isNotEmpty(typedictId)){
            dispatch.setType(dictService.get(typedictId));
        }
        dispatch.setTitle(title);
        dispatch.setFileNo(fileNo);
        if(StringUtils.isNotEmpty(time)){
            dispatch.setTime(DataUtil.StringToDate(time));
        }
        dispatch.setObject(object);
        dispatch.setHasValid((hasValid != null && hasValid.equals("1")) ? true : false);
        if(StringUtils.isNotEmpty(validDate)){
            dispatch.setValidDate(DataUtil.StringToDate(validDate));
        }
        List<Department> departmentList = Lists.newArrayList();
        if(com.joint.base.util.StringUtils.isNotEmpty(noticeDepartmentId)){
            String[] idArr = noticeDepartmentId.split(",");
            for(String id:idArr){
                departmentList.add(departmentService.get(id.trim()));
            }
        }
        List<Users> usersList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(noticeUsersId)){
            String[] idArr = noticeUsersId.split(",");
            for(String id:idArr){
                usersList.add(usersService.get(id.trim()));
            }
        }
        List<FileManage> fileManageList = Lists.newArrayList();
        if(com.joint.base.util.StringUtils.isNotEmpty(fileIds)){
            for(String f:fileIds.split(",")){
                fileManageList.add(fileManageService.get(f.trim()));
            }
        }
        dispatch.setNoticeDepartment(departmentList);
        dispatch.setNoticeUsers(usersList);
        dispatch.setFile(fileManageList);
        dispatch.setCompany(usersService.getLoginInfo().getCompany());
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                dispatchService.update(dispatch);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                dispatchService.save(dispatch, "dispatch", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }


    // 提交
    public String commit(){
        setData();
        Company company = usersService.getCompanyByUser();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        Set<Users> usersSet = Sets.newHashSet();
        Map<String, Object> map = new HashMap<String, Object>();
        List<Department> listDept = dispatch.getNoticeDepartment();
        // LogUtil.info("do:"+object);
        if(StringUtils.equals(object,"1") == true){
            map.put("state", BaseEnum.StateEnum.Enable);
            List<Users> usersList = (List<Users>) usersService.findByPagerAndCompany(new Pager(0),null,company,map).getList();
            LogUtil.info("usersList:"+usersList.size());
            for(Users users:usersList){
                usersSet.add(users);
            }
        }else if(StringUtils.equals(object,"2") == true){
            for(Department department:listDept){
                dutyList = dutyService.getDutys(department);
                for(Duty duty:dutyList){
                    usersSet.add(duty.getUsers());
                }
            }
        }else if(StringUtils.equals(object,"3") == true){
            List<Users> usersList = dispatch.getNoticeUsers();
            for(Users users:usersList){
                usersSet.add(users);
            }
        }



        try {
            if (StringUtils.isNotEmpty(keyId)) {
                dispatchService.approve(dispatch, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            } else {
                keyId = dispatchService.commit(dispatch, "dispatch", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        //先删除读者
        readersService.deleteByKeyIdBussinessKey(keyId);
        //增加归档后读者
        for(Users users:usersSet){
            Readers readers = new Readers();
            readers.setUsers(users);
            readers.setBussinessKey("dispatch");
            readers.setKeyId(keyId);
            readers.setType(2);
            readersService.save(readers);
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批1
    public String approve1(){
        dispatch = dispatchService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();

        var1.put("numStatus", 2);
        var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                dispatchService.approve(dispatch, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批2
    public String approve2(){
        dispatch = dispatchService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        LogUtil.info("mulExam:"+mulExam);
        var1.put("numStatus", 3);
        var1.put("curDutyId", curDutyId);
        var1.put("mulExam", mulExam);
        var1.put("leaderExam", leaderExam);
        List<Users> usersList = Lists.newArrayList();
        List<String> stringList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(examUsersId)){
            String[] idArr = examUsersId.split(",");
            for(String id:idArr){
                usersList.add(usersService.get(id.trim()));
                stringList.add(id.trim());
            }
        }
       // LogUtil.info("usersList:"+usersList.size());
        var1.put("approvers", stringList);
        List<String> usersList2 = Lists.newArrayList();
        List<Power> powerList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(examObjectId)){
            String[] idArr =examObjectId.split(",");
            for(String id:idArr){
               Power power = powerService.get(id.trim());
               powerList.add(power);
               List<Duty> dutyList = dutyService.findByPower(power);
               for(Duty duty:dutyList) {
                   usersList2.add(duty.getUsers().getId());
               }
            }
        }
       // LogUtil.info("usersList2:"+usersList2.size());
        var1.put("approvers2", usersList2);
        dispatch.setMulExam(StringUtils.equals(mulExam, "1") ? true : false);
        dispatch.setLeaderExam(StringUtils.equals(leaderExam, "1") ? true : false);
        dispatch.setExamUsers(usersList);
        dispatch.setExamObject(powerList);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if(StringUtils.equals(mulExam,"1") == false && StringUtils.equals(leaderExam, "1") == false){
                dispatchService.approve(dispatch, FlowEnum.ProcessState.Finished, var1, curDutyId, comment);
            }else{
				dispatchService.approve(dispatch, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
			}
           
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        /*
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }*/
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批3
    public String approve3(){
        dispatch = dispatchService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                dispatchService.approve(dispatch, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            //LogUtil.info("overtask:"+overtask);
            //会签后没有流程将对象状态设为归档
            if(overtask==null){
                Dispatch dispatch = dispatchService.get(keyId);
                dispatch.setProcessState(FlowEnum.ProcessState.Finished);
                dispatchService.update(dispatch);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(dispatch.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 4);
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }


    //审批4
    public String approve4(){
        dispatch = dispatchService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                dispatchService.approve(dispatch, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            //LogUtil.info("overtask:"+overtask);
            //会签后没有流程将对象状态设为归档
            if(overtask==null){
                Dispatch dispatch = dispatchService.get(keyId);
                dispatch.setProcessState(FlowEnum.ProcessState.Finished);
                dispatchService.update(dispatch);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(dispatch.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }

            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 5);
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (StringUtils.isNotEmpty(keyId)) {
            dispatch = dispatchService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("dispatch");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
           // System.out.println("comment:"+comment);
             LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            dispatchService.reject(dispatch, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (StringUtils.isNotEmpty(keyId)) {
            dispatch = dispatchService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("dispatch");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:" + key);
            if(StringUtils.isEmpty(comment)){
                comment="";
            }
            dispatchService.deny(dispatch, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }

    /**
     * 失效
     * @return
     */
    public String doInvalid(){
        if(StringUtils.isNotEmpty(keyId)) {
            dispatch = dispatchService.get(keyId);
            dispatch.setInvalid(true);
            dispatchService.update(dispatch);
        }
        return ajaxHtmlCallback("200", "操作成功！", "操作状态");
    }

    public Dispatch getDispatch() {
        return dispatch;
    }

    public void setDispatch(Dispatch dispatch) {
        this.dispatch = dispatch;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public List<Map<String, Object>> getTypeDict() {
        return typeDict;
    }

    public void setTypeDict(List<Map<String, Object>> typeDict) {
        this.typeDict = typeDict;
    }

    public String getTypedictId() {
        return typedictId;
    }

    public void setTypedictId(String typedictId) {
        this.typedictId = typedictId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFileNo() {
        return fileNo;
    }

    public void setFileNo(String fileNo) {
        this.fileNo = fileNo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getHasValid() {
        return hasValid;
    }

    public void setHasValid(String hasValid) {
        this.hasValid = hasValid;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public String getNoticeDepartmentId() {
        return noticeDepartmentId;
    }

    public void setNoticeDepartmentId(String noticeDepartmentId) {
        this.noticeDepartmentId = noticeDepartmentId;
    }

    public String getNoticeUsersId() {
        return noticeUsersId;
    }

    public void setNoticeUsersId(String noticeUsersId) {
        this.noticeUsersId = noticeUsersId;
    }

    public String getFileIds() {
        return fileIds;
    }

    public void setFileIds(String fileIds) {
        this.fileIds = fileIds;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public List<Duty> getDutyList() {
        return dutyList;
    }

    public void setDutyList(List<Duty> dutyList) {
        this.dutyList = dutyList;
    }

    public Set<String> getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(Set<String> fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getMulExam() {
        return mulExam;
    }

    public void setMulExam(String mulExam) {
        this.mulExam = mulExam;
    }

    public String getExamUsersId() {
        return examUsersId;
    }

    public void setExamUsersId(String examUsersId) {
        this.examUsersId = examUsersId;
    }

    public String getLeaderExam() {
        return leaderExam;
    }

    public void setLeaderExam(String leaderExam) {
        this.leaderExam = leaderExam;
    }

    public String getExamObjectId() {
        return examObjectId;
    }

    public void setExamObjectId(String examObjectId) {
        this.examObjectId = examObjectId;
    }

    public List<Map<String, Object>> getExamObject() {
        return examObject;
    }

    public void setExamObject(List<Map<String, Object>> examObject) {
        this.examObject = examObject;
    }


}

