package com.joint.web.action.manage;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Users;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.CarConfig;
import com.joint.core.entity.manage.Oil;
import com.joint.core.entity.manage.OilQuery;
import com.joint.core.service.CarConfigService;
import com.joint.core.service.OilQueryService;
import com.joint.core.service.OilService;
import com.joint.web.action.BaseFlowAction;
import freemarker.template.utility.DateUtil;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ZhuChunXiao on 2017/3/22.
 */
@ParentPackage("manage")
public class AjaxOilqueryAction extends BaseFlowAction {
    @Resource
    private UsersService usersService;
    @Resource
    private OilQueryService oilQueryService;
    @Resource
    private OilService oilService;
    @Resource
    private CarConfigService carConfigService;

    /**
     * 当前里程数
     */
    private int oilQueryKilometre;
    /**
     * 加油时间
     */
    private String oilQueryDate;
    /**
     * 加油型号
     */
    private String oilQueryType;
    private String typeText;
    /**
     * 加油金额
     */
    private Double oilQueryMoney;
    /**
     * 当前累计加油金额
     */
    private Double oilQueryMoneyNum;
    private Double moneyHelp;
    /**
     * 备注
     */
    private String oilQueryRemarks;


    private OilQuery oilQuery;
    private CarConfig carConfig;
    private Oil oil;
    private Users loginUser;
    private int numStatus;
    public String execute(){
        return "oilquery";
    }

    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        //车辆信息对象
        if(StringUtils.isNotEmpty(parentId)){
            CarConfig carConfig=carConfigService.get(parentId);
            params.put("carConfig",carConfig);
        }
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
        pager=oilQueryService.findByPagerAndLimit(true, "oilquery", pager, params);

        List<OilQuery> oilqueryList;
        if (pager.getTotalCount() > 0){
            oilqueryList = (List<OilQuery>) pager.getList();
        }else{
            oilqueryList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(OilQuery oilQuery:oilqueryList){
            rMap=new HashMap<>();
            rMap.put("id", oilQuery.getId());
            rMap.put("kilometre",oilQuery.getKilometre());
            rMap.put("oilDate", DataUtil.DateToString(oilQuery.getOilDate()));
            switch(oilQuery.getType()){
                case "1":rMap.put("type","#92");break;
                case "2":rMap.put("type","#95");break;
                case "3":rMap.put("type","#98");break;
                default:rMap.put("type","未知");
            }
            rMap.put("oilMoney", oilQuery.getOilMoney());
            rMap.put("oilMoneyNum", oilQuery.getOilMoneyNum());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String input(){
        CarConfig carConfig=carConfigService.get(parentId);
        moneyHelp=oilQueryService.getMoneyNum(carConfig);
        System.out.println(moneyHelp+"-----------------action");
        if (StringUtils.isNotEmpty(keyId)){
            oilQuery=oilQueryService.get(keyId);
//            moneyHelp=oilQuery.getOilMoneyNum();
        }
        return "input";
    }

    public void setData(){
        if(StringUtils.isNotEmpty(keyId)){
            oilQuery=oilQueryService.get(keyId);
        }else{
            oilQuery=new OilQuery();
            oilQuery.setCreater(usersService.getLoginInfo());
        }
        oilQuery.setCompany(usersService.getLoginInfo().getCompany());
        oilQuery.setKilometre(oilQueryKilometre);
        oilQuery.setOilDate(DataUtil.StringToDate(oilQueryDate));
        oilQuery.setType(oilQueryType);
        oilQuery.setOilMoney(oilQueryMoney);
        oilQuery.setOilMoneyNum(oilQueryMoneyNum);
        oilQuery.setRemarks(oilQueryRemarks);
        oilQuery.setCarConfig(carConfigService.get(parentId));
//        oilQuery.setOil(oilService.get(parentId));
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                oilQueryService.update(oilQuery);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                oilQueryService.save(oilQuery, "oilquery", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        setData();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        carConfig=carConfigService.get(parentId);
        carConfig.setKilometre(oilQueryKilometre);
        carConfig.setOilDate(DataUtil.StringToDate(oilQueryDate));
        carConfig.setOilMoney(oilQueryMoney);
        carConfig.setOilMoneyNum(oilQueryMoneyNum);
        carConfig.setRemarks(oilQueryRemarks);
        carConfigService.update(carConfig);

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                oilQueryService.approve(oilQuery, FlowEnum.ProcessState.Finished, var2, curDutyId, comment);
            } else {
                keyId = oilQueryService.commit(oilQuery, FlowEnum.ProcessState.Finished,"oilquery", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String read(){
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)){
            oilQuery=oilQueryService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            switch (oilQuery.getType()){
                case "1":typeText="#92";break;
                case "2":typeText="#95";break;
                case "3":typeText="#98";break;
                default:typeText="未知";
            }
        }
        return "read";
    }


    public int getOilQueryKilometre() {
        return oilQueryKilometre;
    }

    public void setOilQueryKilometre(int oilQueryKilometre) {
        this.oilQueryKilometre = oilQueryKilometre;
    }

    public String getOilQueryDate() {
        return oilQueryDate;
    }

    public void setOilQueryDate(String oilQueryDate) {
        this.oilQueryDate = oilQueryDate;
    }

    public String getOilQueryType() {
        return oilQueryType;
    }

    public void setOilQueryType(String oilQueryType) {
        this.oilQueryType = oilQueryType;
    }

    public Double getOilQueryMoney() {
        return oilQueryMoney;
    }

    public void setOilQueryMoney(Double oilQueryMoney) {
        this.oilQueryMoney = oilQueryMoney;
    }

    public Double getOilQueryMoneyNum() {
        return oilQueryMoneyNum;
    }

    public void setOilQueryMoneyNum(Double oilQueryMoneyNum) {
        this.oilQueryMoneyNum = oilQueryMoneyNum;
    }

    public String getOilQueryRemarks() {
        return oilQueryRemarks;
    }

    public void setOilQueryRemarks(String oilQueryRemarks) {
        this.oilQueryRemarks = oilQueryRemarks;
    }

    public OilQuery getOilQuery() {
        return oilQuery;
    }

    public void setOilQuery(OilQuery oilQuery) {
        this.oilQuery = oilQuery;
    }

    public Oil getOil() {
        return oil;
    }

    public void setOil(Oil oil) {
        this.oil = oil;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public Double getMoneyHelp() {
        return moneyHelp;
    }

    public void setMoneyHelp(Double moneyHelp) {
        this.moneyHelp = moneyHelp;
    }

    public String getTypeText() {
        return typeText;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
    }
}
