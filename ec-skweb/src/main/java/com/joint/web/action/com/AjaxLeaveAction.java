package com.joint.web.action.com;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.Employees;
import com.joint.core.entity.LeaveDetail;
import com.joint.core.entity.Leave;
import com.joint.core.entity.manage.Notice;
import com.joint.core.service.EmployeesService;
import com.joint.core.service.LeaveDetailService;
import com.joint.core.service.LeaveService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by root on 16/12/16.
 */
@ParentPackage("com")
public class AjaxLeaveAction extends BaseFlowAction {
    @Resource
    private LeaveService leaveService;
    @Resource
    private EmployeesService employeesService;
    @Resource
    private UsersService usersService;
    @Resource
    private LeaveDetailService leaveDetailService;

    private LeaveDetail leaveDetail;
    private String type;
    private String name;
    private Integer time;
    private Leave leave;
    private Users creater;
    private String createDate;
    private int ifCentralStaff;
    private int ifManager;
    private Employees employees;
    private String fileId;
    private String fileIds;


    /**
     * 请假天数
     */
    private BigDecimal dayNum;

    /**
     * 今年年假
     */
    private BigDecimal annuaLeave;

    /**
     * 今年年假(已用)
     */
    private BigDecimal usedAnnuaLeave;

    /**
     * 今年年假(可用)
     */
    private BigDecimal unusedAnnuaLeave;

    /**
     * 已申请病假
     */
    private BigDecimal sickLeave;

    /**
     * 已申请事假
     */
    private BigDecimal thingLeave;

    /**
     * 已申请其他假期
     */
    private BigDecimal othersLeave;

    /**
     * 当年已申请天数
     */
    private BigDecimal usedDayNum;

    /**
     * 下一步审批人的Name的集合，用逗号分隔
     */
    private String nextStepApproversName;

    /**
     * 下一步审批人的id的集合，用逗号分隔
     */
    private String nextStepApproversId;

    private Users loginUser;
    /**
     * 视图类型
     */
    private String viewtype;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "leave";
    }

    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        //  LogUtil.info("viewtype:" + viewtype);
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                LogUtil.info("11"+1);
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                pager=leaveService.findByPagerAndLimit(false, "leave", pager, params);
            }else if(viewtype.equals("2")){
                //已完成
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                pager=leaveService.findByPagerAndFinish("leave", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=leaveService.findByPagerAndLimit(true, "leave", pager, params);
        }

        List<Leave> leaveList;
        if (pager.getTotalCount() > 0){
            leaveList = (List<Leave>) pager.getList();
        }else{
            leaveList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(Leave leave:leaveList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",leave.getId());
            rMap.put("creater",leave.getCreater()==null?"":leave.getCreater().getName());
            rMap.put("dayNum",leave.getDayNum());
            String type = leave.getType();
            if(StringUtils.isNotEmpty(type)){
                if(type.equals("0")){
                    rMap.put("type","年假");
                }else if(type.equals("1")){
                    rMap.put("type","病假");
                }else if(type.equals("2")){
                    rMap.put("type","事假");
                }else if(type.equals("3")){
                    rMap.put("type","其他");
                }
            }
            rMap.put("createDate",DataUtil.DateToString(leave.getCreateDate(),"yyyy-MM-dd"));
            String state=leave.getProcessState().name();
            if(StringUtils.isNotEmpty(state)) {
                if (state.equals("Finished")) {
                    rMap.put("state", "已归档");
                } else if (state.equals("Running")) {
                    rMap.put("state","审核中");
                }

            }
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    /**
     * 加载数据
     * @return
     */
    public String loadData(){
        loginUser=usersService.getLoginInfo();
       // LogUtil.info("++++"+111);
        LeaveDetail leaveDetail = leaveDetailService.findLeaveDetail(loginUser.getId(),time);
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        //data.put("leaveDetail",leaveDetail);
        data.put("annuaLeave",leaveDetail.getAnnuaLeave());
        data.put("usedAnnuaLeave",leaveDetail.getUsedAnnuaLeave());
        data.put("unusedAnnuaLeave",leaveDetail.getUnusedAnnuaLeave());
        data.put("sickLeave",leaveDetail.getSickLeave());
        data.put("thingLeave",leaveDetail.getThingLeave());
        data.put("othersLeave",leaveDetail.getOthersLeave());
        data.put("usedDayNum",leaveDetail.getUsedDayNum());
        return  ajaxJson(JSONObject.fromObject(data).toString());

    }

    public String getLeaveJson(){
        pager = new Pager(0);
        pager.setOrderBy("modifyDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();

        params.put("creater",user);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
        pager=leaveService.findByPagerAndLimit(true, "leave", pager, params);

        List<Leave> leaveList;
        if (pager.getTotalCount() > 0){
            leaveList = (List<Leave>) pager.getList();
        }else{
            leaveList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String,Object> rMap;
        for(Leave leave: leaveList){
            rMap = new HashMap<String, Object>();
            rMap.put("id",leave.getId());
            String type = leave.getType();
            if(StringUtils.isNotEmpty(type)){
                if(type.equals("0")){
                    rMap.put("title","请年假");
                }else if(type.equals("1")){
                    rMap.put("title","请病假");
                }else if(type.equals("2")){
                    rMap.put("title","请事假");
                }else if(type.equals("3")){
                    rMap.put("title","请其他假");
                }
            }
            rMap.put("start", DataUtil.DateToString(leave.getModifyDate(), "yyyy-MM-dd HH:mm"));
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);

        }

        return  ajaxJson(dataRows.toString());
    }

    /**
     * 详情
     * @return
     */
    public String read(){

        loginUser=usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)) {
            fileId="";
            leave= leaveService.get(keyId);
            Users creater = leave.getCreater();
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            //employees = employeesService.get("users", creater);
            leaveDetail= leaveDetailService.findLeaveDetailByusers(creater.getId());

            if(leave.getFile()!=null&&leave.getFile().size()>0){
                for (FileManage f:leave.getFile()){
                    fileId+=f.getId()+",";
                }
            }

        }

        return "read";
    }

    /**
     * 新建／编辑
     * @return
     */
    public String input(){
        loginUser=usersService.getLoginInfo();
        creater=loginUser;
        //leaveDetail= employees.getLeaveDetail();

        if (StringUtils.isNotEmpty(keyId)){
            fileId="";
            leave= leaveService.get(keyId);
            leaveDetail=leaveDetailService.findLeaveDetail(loginUser.getId(),leave.getTime());
            dayNum=leave.getDayNum();
            creater=leave.getCreater();
            createDate= DataUtil.DateToString(leave.getCreateDate(),"yyyy-MM-dd");

            if(leave.getFile()!=null&&leave.getFile().size()>0){
                for (FileManage f:leave.getFile()){
                    fileId+=f.getId()+",";
                }
            }
        } else {
            createDate= DataUtil.DateToString(new Date(),"yyyy-MM-dd");

        }

        return "input";
    }

    private void setData(){

        Set<Users> usersSet= Sets.newHashSet();
        if(StringUtils.isNotEmpty(keyId)){
            leave = leaveService.get(keyId);
        }else{
            leave = new Leave();
            leave.setCreater(usersService.getLoginInfo());
        }

        Set<Duty> dutySet = usersService.getLoginInfo().getDutySet();
        for (Duty duty:dutySet){

            if(duty.getDepartment().getParent()!=null){
                LogUtil.info("ceshi"+duty.getDepartment().getParent().getName());
                if(StringUtils.equals(duty.getDepartment().getParent().getName(),"产业中心")){
                    leave.setIfCentralStaff(1);
                }
            }
            Department department = duty.getDepartment();
            Set<Power> powerSet = department.getPowerSet();
            LogUtil.info("powerSet"+powerSet);
            for (Power power:powerSet){
                Post post = power.getPost();
                LogUtil.info("post"+post.getName());
                if(StringUtils.equals(post.getName(),"总经理")){
                    leave.setIfManager(1);
                }
            }
        }

        List<FileManage> fileManagesList= Lists.newArrayList();
        if(StringUtils.isNotEmpty(fileIds)){
            for (String f:fileIds.split(",")){
                fileManagesList.add(fileManageService.get(f.trim()));
            }
        }

        if(type.equals("0")){
            leave.setUsedAnnuaLeave(dayNum);
        }else if(type.equals("1")){
            leave.setSickLeave(dayNum);
        }else if(type.equals("2")){
            leave.setThingLeave(dayNum);
        }else {
            leave.setOthersLeave(dayNum);
        }

        leave.setTime(time);
        leave.setDayNum(dayNum);
        leave.setAnnuaLeave(annuaLeave);
        leave.setUnusedAnnuaLeave(unusedAnnuaLeave);
        leave.setUsedDayNum(usedDayNum);
        leave.setType(type);
        leave.setName(name);
        leave.setFile(fileManagesList);
        leave.setNextStepApproversId(nextStepApproversId);
        leave.setNextStepApproversName(nextStepApproversName);
        leave.setCompany(usersService.getLoginInfo().getCompany());

    }

    /**
     * 保存
     * @return
     */
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                leaveService.update(leave);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                leaveService.save(leave, "leave", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    /**
     * 提交
     * @return
     */
    public String commit(){
        setData();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);

        ArrayList<String> list = new ArrayList<String>();
        for (String uid: nextStepApproversId.split(",")) {
            list.add(uid.trim());
        }
        ArrayList<String> list1 = new ArrayList<String>();
        for (String uid: nextStepApproversId.split(",")) {
            list1.add(uid.trim());
        }
        /*Set<Users> usersSet= Sets.newHashSet();
        for(String id:chargeUsersId.split(",")){
            usersSet.add(usersService.get(id.trim()));
        }*/

        ArrayList<String> list2=new ArrayList<String>();

        Set<Duty> dutySet = usersService.getLoginInfo().getDutySet();

        for(Duty duty:dutySet){
            Department department = duty.getDepartment();
            Set<Power> powerSet = department.getPowerSet();
            LogUtil.info("powerSet"+powerSet);
            for (Power power:powerSet){
                Post post = power.getPost();
                LogUtil.info("post"+post.getName());

                if(StringUtils.equals(post.getName(),"人事负责人")){
                    List<Users> persons = dutyService.getPersons(department, post);
                    for (Users users:persons){
                        list2.add(users.getId());
                        //LogUtil.info("QQQ"+list2);
                    }
                }
            }
        }

        var2.put("approvers", list);    // 会签人Ids
        var2.put("examine", list2);    // 审核3 Ids

        //LogUtil.info("ifCentralStaff"+leave.getIfCentralStaff());
        var2.put("ifCentralStaff", leave.getIfCentralStaff());
        var2.put("ifManager", leave.getIfManager());

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                leaveService.approve(leave, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            } else {
                keyId=leaveService.commit(leave, "leave", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        /*Set<Users> usersSet= Sets.newHashSet();
        usersSet=leaveService.get(keyId).getEmployees().getEmployment().getManage().getChargeUsers();

        //增加归档后读者
        for(Users users:usersSet) {
            Readers readers = new Readers();
            readers.setUsers(users);
            readers.setBussinessKey("apply");
            readers.setKeyId(keyId);
            readers.setType(2);
            readersService.save(readers);
        }
        for(Users users:usersSet){
            Remind remind = new Remind();
            remind.setUsers(users);
            remind.setBussinessKey("apply");
            remind.setKeyId(keyId);
            remind.setContent("员工转正已归档");
            remindService.save(remind);
        }*/
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 审批1
     * @return
     */
    public String approve1(){
        leave = leaveService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        if(leave.getIfCentralStaff()==1){
            LogUtil.info("ifCentralStaff"+leave.getIfCentralStaff());
            var1.put("numStatus",3);
        }else{
            var1.put("numStatus", 2);
        }
        var1.put("curDutyId", curDutyId);

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                leaveService.approve(leave, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }


    /**
     * 审批2
     * @return
     */
    public String approve2() {
        LogUtil.info("approve2"+"审批2");
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        leave = leaveService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        if(leave.getIfManager()==1){
            //LogUtil.info("+++++++"+leave.getIfManager());
            var2.put("numStatus",3);
        }
        //var2.put("numStatus",2);
        //  var2.put("curDutyId", curDutyId);
        try {
            if(leave.getIfManager()==1){
                leaveService.approve(leave, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            }else {
                leaveService.approve(leave, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
                Leave entity=leaveService.get(keyId);
                Users creater = entity.getCreater();
               // employees = employeesService.get("users", creater);
                //LogUtil.info("employees"+employees);
                LeaveDetail leaveDetail = leaveDetailService.findLeaveDetail(creater.getId(),entity.getTime());
                //LogUtil.info("entity"+entity.getUsedAnnuaLeave());
                //LogUtil.info("leaveDetail"+leaveDetail.getUsedAnnuaLeave());
                if(StringUtils.equals(entity.getType(),"0")){
                    leaveDetail.setUsedAnnuaLeave(entity.getDayNum().add(leaveDetail.getUsedAnnuaLeave()));
                    leaveDetail.setUnusedAnnuaLeave(leaveDetail.getAnnuaLeave().subtract(leaveDetail.getUsedAnnuaLeave()));

                }else if(StringUtils.equals(entity.getType(),"1")){
                    leaveDetail.setSickLeave(leaveDetail.getSickLeave().add(entity.getDayNum()));

                }else if(StringUtils.equals(entity.getType(),"2")){
                    leaveDetail.setThingLeave(leaveDetail.getThingLeave().add(entity.getDayNum()));

                }else {
                    leaveDetail.setOthersLeave(leaveDetail.getOthersLeave().add(entity.getOthersLeave()));

                }
                leaveDetail.setUsedDayNum(leaveDetail.getUsedDayNum().add(entity.getDayNum()));
                leaveDetail.setTime(time);
                //employees.setLeaveDetail(leaveDetail);
                leaveDetailService.update(leaveDetail);
                //employeesService.update(employees);


            }

        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 会签
     * @return
     */
    public String approve3() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";

        leave = leaveService.get(keyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        // var2.put("curDutyId", curDutyId);
        try {
            leaveService.approve(leave, FlowEnum.ProcessState.Running, var2, curDutyId, comment);

            Task overtask = workflowService.getCurrentTask(keyId);
           // LogUtil.info("overtask:"+overtask);
            if(overtask==null){
                Leave entity=leaveService.get(keyId);
                entity.setProcessState(FlowEnum.ProcessState.Finished);
                leaveService.update(entity);
                entity=leaveService.get(keyId);

                Users creater = entity.getCreater();
                //employees = employeesService.get("users", creater);
               // LogUtil.info("employees"+employees);
                LeaveDetail leaveDetail = leaveDetailService.findLeaveDetail(creater.getId(),entity.getTime());
                //LogUtil.info("entity"+entity.getUsedAnnuaLeave());
                //LogUtil.info("leaveDetail"+leaveDetail.getUsedAnnuaLeave());
                if(StringUtils.equals(entity.getType(),"0")){
                    leaveDetail.setUsedAnnuaLeave(entity.getDayNum().add(leaveDetail.getUsedAnnuaLeave()));
                    leaveDetail.setUnusedAnnuaLeave(leaveDetail.getAnnuaLeave().subtract(leaveDetail.getUsedAnnuaLeave()));

                }else if(StringUtils.equals(entity.getType(),"1")){
                    leaveDetail.setSickLeave(leaveDetail.getSickLeave().add(entity.getDayNum()));

                }else if(StringUtils.equals(entity.getType(),"2")){
                    leaveDetail.setThingLeave(leaveDetail.getThingLeave().add(entity.getDayNum()));

                }else {
                    leaveDetail.setOthersLeave(leaveDetail.getOthersLeave().add(entity.getOthersLeave()));

                }
                leaveDetail.setUsedDayNum(leaveDetail.getUsedDayNum().add(entity.getDayNum()));
               // employees.setLeaveDetail(leaveDetail);
                leaveDetailService.update(leaveDetail);
               // employeesService.update(employees);

            }else {

                if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){

                runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 4);
                }
        }

        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }


    /**
     * 退回
     * @return
     */
    public String reject() {
        if (StringUtils.isNotEmpty(keyId)) {
            leave = leaveService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("leave");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            System.out.println("comment:"+comment);
            LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            leaveService.reject(leave, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Leave getLeave() {
        return leave;
    }

    public void setLeave(Leave leave) {
        this.leave = leave;
    }

    public Users getCreater() {
        return creater;
    }

    public void setCreater(Users creater) {
        this.creater = creater;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }

    public int getIfManager() {
        return ifManager;
    }

    public void setIfManager(int ifManager) {
        this.ifManager = ifManager;
    }

    public BigDecimal getDayNum() {
        return dayNum;
    }

    public void setDayNum(BigDecimal dayNum) {
        this.dayNum = dayNum;
    }

    public BigDecimal getAnnuaLeave() {
        return annuaLeave;
    }

    public void setAnnuaLeave(BigDecimal annuaLeave) {
        this.annuaLeave = annuaLeave;
    }

    public BigDecimal getUsedAnnuaLeave() {
        return usedAnnuaLeave;
    }

    public void setUsedAnnuaLeave(BigDecimal usedAnnuaLeave) {
        this.usedAnnuaLeave = usedAnnuaLeave;
    }

    public BigDecimal getUnusedAnnuaLeave() {
        return unusedAnnuaLeave;
    }

    public void setUnusedAnnuaLeave(BigDecimal unusedAnnuaLeave) {
        this.unusedAnnuaLeave = unusedAnnuaLeave;
    }

    public BigDecimal getSickLeave() {
        return sickLeave;
    }

    public void setSickLeave(BigDecimal sickLeave) {
        this.sickLeave = sickLeave;
    }

    public BigDecimal getThingLeave() {
        return thingLeave;
    }

    public void setThingLeave(BigDecimal thingLeave) {
        this.thingLeave = thingLeave;
    }

    /*public BigDecimal getOthersLeave() {
        return othersLeave;
    }

    public void setOthersLeave(BigDecimal othersLeave) {
        this.othersLeave = othersLeave;
    }*/
/*
    public BigDecimal getUsedDayNum() {
        return usedDayNum;
    }

    public void setUsedDayNum(BigDecimal usedDayNum) {
        this.usedDayNum = usedDayNum;
    }*/

    public String getNextStepApproversName() {
        return nextStepApproversName;
    }

    public void setNextStepApproversName(String nextStepApproversName) {
        this.nextStepApproversName = nextStepApproversName;
    }

    public String getNextStepApproversId() {
        return nextStepApproversId;
    }

    public void setNextStepApproversId(String nextStepApproversId) {
        this.nextStepApproversId = nextStepApproversId;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public LeaveDetail getLeaveDetail() {
        return leaveDetail;
    }

    public void setLeaveDetail(LeaveDetail leaveDetail) {
        this.leaveDetail = leaveDetail;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileIds() {
        return fileIds;
    }

    public void setFileIds(String fileIds) {
        this.fileIds = fileIds;
    }

    public BigDecimal getOthersLeave() {
        return othersLeave;
    }

    public void setOthersLeave(BigDecimal othersLeave) {
        this.othersLeave = othersLeave;
    }

    public BigDecimal getUsedDayNum() {
        return usedDayNum;
    }

    public void setUsedDayNum(BigDecimal usedDayNum) {
        this.usedDayNum = usedDayNum;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
}

