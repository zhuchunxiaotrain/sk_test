package com.joint.web.action.com;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.service.DictService;
import com.joint.base.entity.Company;
import com.joint.base.entity.Users;
import com.joint.base.service.DepartmentService;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.Employees;
import com.joint.core.entity.PersonnelResume;
import com.joint.core.service.*;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 16/12/16.
 */
@ParentPackage("com")
public class AjaxPersonnelresumeAction extends BaseFlowAction {

    @Resource
    private UsersService usersService;
    @Resource
    private EmployeesService employeesService;
    @Resource
    private PersonnelResumeService personnelResumeService;

    /**
     * 视图类型
     */
    private String viewtype;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "personnelresume";
    }

    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        //内嵌视图这里要加上
       if (StringUtils.isNotEmpty(parentId)){
           Employees employees = employeesService.get(parentId);
            params.put("users",employees.getUsers());
        }
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        //所有单子
       // params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
        pager=personnelResumeService.findByPager(pager,params);
        List<PersonnelResume> tList;
        if (pager.getTotalCount() > 0){
            tList = (List<PersonnelResume>) pager.getList();
        }else{
            tList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(PersonnelResume personnelResume:tList){
            rMap = new HashMap<String,Object>();
            rMap.put("type",personnelResume.getType());
            rMap.put("id",personnelResume.getId());
            rMap.put("time",DataUtil.DateToString(personnelResume.getCreateDate(),"yyyy-MM-dd"));

            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }
}
