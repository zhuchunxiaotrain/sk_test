package com.joint.web.action.party;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.DepartmentService;
import com.joint.base.service.PowerService;
import com.joint.base.service.RoleService;
import com.joint.base.service.UsersService;
import com.joint.core.entity.*;
import com.joint.core.entity.PartyMoney;
import com.joint.core.service.PartyMoneyService;
import com.joint.core.service.SelectionNoticeService;
import com.joint.core.service.PartyMoneyService;
import com.joint.core.service.SelectionSpeechService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dqf on 2015/8/26.
 */
@ParentPackage("party")
public class AjaxPartymoneyAction extends BaseFlowAction {
  
   
    @Resource
    private PartyMoneyService partyMoneyService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private PowerService powerService;
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private RoleService roleService;


    private PartyMoney partyMoney;
    private int numStatus;
    private Users loginUser;

    private String usersId;
    private String partyMoneyDepartId;



    /**
     * 缴纳党费时间
     */
    private Date payDate;
    /**
     * 缴纳的年份
     */
    private String payDateYear;
    /**
     * 缴纳的月份
     */
    private String  payDateMonth;
    /**
     * 是否是党员
     */
    private String isParty;
    /**
     * 用户
     */
    private Users users;
    /**
     * 用户所属于的部门
     */
    private Department department;
    /**
     * 每个月应缴党费
     */
    private BigDecimal everyPay;
   /* *//**
     * 党费详情
     * @return
     *//*
    private List<PartyMoneyDetail> partyMoneyDetailList;*/

    private String viewtype;

    private String departId;
    private String payMonth;
    private String payDateString;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        LogUtil.info("parentIdExcute = " +parentId);
        return "partymoney";
    }

    /**
     * 根据部门和登记月份查询人员
     * @return
     */
    public String count(){
        LogUtil.info("parentIdList = " +parentId);
        department=new Department();

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);


        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        List<Department> departmentList= Lists.newArrayList();
        if(StringUtils.isNotEmpty(departId)){
            department=departmentService.get(departId);
            departmentList.add(department);
            if(department.getChildren()!=null){
                for(Department department1:department.getChildren()){
                    departmentList.add(department1);
                }
            }
            params.put("department",departmentList);
        }
        List<String> stringList=Lists.newArrayList();
        if(StringUtils.isNotEmpty(payDateString)){
            for(String s:payDateString.split("-")){
                stringList.add(s.trim());
            }
            payDateYear=stringList.get(0);
            payDateMonth=stringList.get(1);
            params.put("payDateYear",payDateYear);
            params.put("payDateMonth",payDateMonth);
        }
        params.put("isParty","1");
        pager=partyMoneyService.findByPagerAndFinish("partymoney", pager, params);
        List<PartyMoney> partyMoneysList;
        if (pager.getTotalCount() > 0){

            partyMoneysList = (List<PartyMoney>) pager.getList();
        }else{
            partyMoneysList = new ArrayList<>();
        }

        List<JSONObject> dataRow=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        LogUtil.info("selectNoticesListSize = "+partyMoneysList.size());
        for(PartyMoney partyMoney:partyMoneysList){
            LogUtil.info("userName:"+partyMoney.getUsers().getName());
            LogUtil.info("departmentName"+partyMoney.getDepartment().getName());
            /*LogUtil.info("everyPay");*/


            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM");
            rMap=new HashMap<>();
            rMap.put("id",partyMoney.getId()!=null?partyMoney.getId():"");
            rMap.put("userName",partyMoney.getUsers()!=null?partyMoney.getUsers().getName():"");
            rMap.put("everyPay",partyMoney.getEveryPay()!=null?partyMoney.getEveryPay():"");
            JSONObject o = JSONObject.fromObject(rMap);
            dataRow.add(o);
        }

        data.put("dataRows",dataRow);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String listfordialog(){

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);


        pager=partyMoneyService.findByPagerAndProcessState(pager, user, "partymoney", FlowEnum.ProcessState.Finished, params);
        List<PartyMoney> partyMoneyList;
        if (pager.getTotalCount() > 0){
            partyMoneyList = (List<PartyMoney>) pager.getList();
        }else{
            partyMoneyList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(PartyMoney partyMoney:partyMoneyList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",partyMoney.getId()!=null?partyMoney.getId():"");
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }


        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String list(){
        LogUtil.info("parentIdList = " +parentId);

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);


        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        //  LogUtil.info("viewtype:" + viewtype);
        viewtype="";
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                pager=partyMoneyService.findByPagerAndLimit(false, "partymoney", pager, params);
            }else if(viewtype.equals("2")){
                //已归档
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                pager=partyMoneyService.findByPagerAndFinish("partymoney", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=partyMoneyService.findByPagerAndLimit(true, "partymoney", pager, params);
        }

        List<PartyMoney> partyMoneysList;
        if (pager.getTotalCount() > 0){

            partyMoneysList = (List<PartyMoney>) pager.getList();
        }else{
            partyMoneysList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        LogUtil.info("selectNoticesListSize = "+partyMoneysList.size());
        for(PartyMoney partyMoney:partyMoneysList){

            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM");
            rMap=new HashMap<>();
            rMap.put("id",partyMoney.getId()!=null?partyMoney.getId():"");
            rMap.put("payDate",partyMoney.getPayDate()!=null?simpleDateFormat.format(partyMoney.getPayDate()):"");
            rMap.put("userName",partyMoney.getUsers()!=null?partyMoney.getUsers().getName():"");
            rMap.put("department",partyMoney.getDepartment()!=null?partyMoney.getDepartment().getName():"");
            rMap.put("everyPay",partyMoney.getEveryPay()!=null?partyMoney.getEveryPay():"");
            /*if(partyMoney.getPartyMoneyDetailList()!=null){
                for(PartyMoneyDetail partyMoneyDetail:partyMoney.getPartyMoneyDetailList()){
                    rMap.put("power",partyMoneyDetail.getPower()!=null?partyMoneyDetail.getPower().getName():"");
                    rMap.put("everyPay",partyMoneyDetail.getEveryPay()!=null?partyMoneyDetail.getEveryPay():"");
                }
            }else{
                rMap.put("power","");
                rMap.put("everyPay","");
            }*/

            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String read(){
        users=new Users();
        department=new Department();

        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty( keyId)) {
            Company company = usersService.getCompanyByUser();
            partyMoney = partyMoneyService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            //用户
            if(partyMoney.getUsers()!=null){
                users=partyMoney.getUsers();
            }
            //部门
            if(partyMoney.getDepartment()!=null){
                department=partyMoney.getDepartment();
            }
        }
        return "read";
    }

    public String input(){
        users=new Users();
        department=new Department();
        LogUtil.info("parentIdInput = " +parentId);
        if (StringUtils.isNotEmpty(keyId)){
            partyMoney = partyMoneyService.get(keyId);
            Company company = usersService.getCompanyByUser();

            //用户
            if(partyMoney.getUsers()!=null){
                users=partyMoney.getUsers();
            }
            //部门
            if(partyMoney.getDepartment()!=null){
                department=partyMoney.getDepartment();
            }

        } else {

        }



        return "input";
    }

    private void setData(){
        users=new Users();
        department=new Department();
        List<Power> powerList=Lists.newArrayList();
        List<BigDecimal> bigDecimalList=Lists.newArrayList();
        Company company=usersService.getLoginInfo().getCompany();
        if(StringUtils.isNotEmpty(keyId)){
            partyMoney = partyMoneyService.get(keyId);
        }else{
            partyMoney = new PartyMoney();
            partyMoney.setCreater(usersService.getLoginInfo());
        }
        //用户
        if(StringUtils.isNotEmpty(usersId)){
            users=usersService.get(usersId);
        }
        //部门
        if(StringUtils.isNotEmpty(partyMoneyDepartId)){
            department=departmentService.get(partyMoneyDepartId);
        }
        //缴纳的年份
        if(payDate!=null){
            LogUtil.info("payDate"+payDate);
            payDateYear= new SimpleDateFormat("yyyy").format(payDate);
            payDateMonth=new SimpleDateFormat("MM").format(payDate);

            LogUtil.info("payDateYear"+payDateYear);
            LogUtil.info("payDateMonth"+payDateMonth);

        }


       /* //职权
        if(StringUtils.isNotEmpty(powerId)){
            for(String s:powerId.split(",")){
                powerList.add(powerService.get(s.trim()));
            }
        }
        //缴纳的党给
        if(StringUtils.isNotEmpty(everyPay)){
            for(String s:everyPay.split(",")){
               BigDecimal bigDecimal=new BigDecimal(s.trim());
                bigDecimalList.add(bigDecimal);
            }
        }
        for(int i=0;i<powerList.size();i++){
            PartyMoneyDetail  partyMoneyDetail=new PartyMoneyDetail();
            partyMoneyDetail.setPower(powerList.get(i));
            partyMoneyDetail.setEveryPay(bigDecimalList.get(i));
            partyMoneyDetail.setState(BaseEnum.StateEnum.Enable);
            partyMoneyDetail.setCompany(company);
            partyMoneyDetailList.add(partyMoneyDetail);
        }*/


        partyMoney.setCreateDate(new Date());
        partyMoney.setCompany(usersService.getLoginInfo().getCompany());
        partyMoney.setPayDate(payDate);
        partyMoney.setPayDateYear(payDateYear);
        partyMoney.setPayDateMonth(payDateMonth);
        partyMoney.setIsParty(isParty);
        partyMoney.setUsers(users);
        partyMoney.setDepartment(department);
        partyMoney.setEveryPay(everyPay);

        partyMoneyService.save(partyMoney);

    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                partyMoneyService.update(partyMoney);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                partyMoneyService.save(partyMoney, "partymoney", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        setData();
        LogUtil.info("commit");
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        var2.put("initDuty", curDutyId);
        ArrayList<String> list = new ArrayList<String>();


      /*  var2.put("approvers", list);    // 会签人Ids*/
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                partyMoneyService.approve(partyMoney, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);

            } else {
                String id=partyMoneyService.commit(partyMoney, "partymoney", var1, var2, curDutyId);
                PartyMoney entity=partyMoneyService.get(id);
                entity.setProcessState(FlowEnum.ProcessState.Finished);
                partyMoneyService.save(entity);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批
    public String approve1(){
        LogUtil.info("approve_1");
        partyMoney = partyMoneyService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();

        var1.put("numStatus", 2);
        //  var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                partyMoneyService.approve(partyMoney, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }
    // 会签
    public String approve2() {
        LogUtil.info("approve_2");
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        Map<String, Object> var2 = new HashMap<String, Object>();

        partyMoney = partyMoneyService.get(keyId);
        /*Task task=workflowService.getCurrentTask(keyId, usersService.getLoginInfo());
      

        int multComplete = (Integer)runtimeService.getVariable(task.getExecutionId(),"nrOfCompletedInstances");
        int nrOfInstances = (Integer)runtimeService.getVariable(task.getExecutionId(),"nrOfInstances");
        LogUtil.info("multComplete:"+multComplete);
        LogUtil.info("nrOfInstances:"+nrOfInstances);

        if((double)multComplete/nrOfInstances>=1){
            var2.put("numStatus", 2);
            LogUtil.info("do:numStatus2");
        } else {
            var2.put("numStatus", 1);
            LogUtil.info("do:numStatus1");
        }*/

        //  var2.put("curDutyId", curDutyId);
        try {

            partyMoneyService.approve(partyMoney, FlowEnum.ProcessState.Running, var2, curDutyId, comment);

            Task overtask = workflowService.getCurrentTask(keyId);
            LogUtil.info("overtask:"+overtask);
            if(overtask==null){/*没有task就到了归档，所以*/
                PartyMoney partyMoney=partyMoneyService.get(keyId);
                partyMoney.setProcessState(FlowEnum.ProcessState.Finished);
                partyMoneyService.save(partyMoney);


                /*selectionSpeech= new SelectionSpeech();
                selectionSpeech.setCreater(partyMoney.getCreater());
                selectionSpeech.setCreateDate(new Date());
                selectionSpeech.setSpeechState("竞聘演讲");
                selectionSpeech.setPartyMoney(partyMoney);
                selectionSpeech.setResult("未审批");
                selectionSpeech.setSelectionNotice(partyMoney.getSelectionNotice());
                selectionSpeech.setCompany(partyMoney.getCreater().getCompany());
                selectionSpeechService.save(selectionSpeech);*/


            }else {
                if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 3);
                }
            }
    } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String approve3() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        partyMoney = partyMoneyService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 4);
        //  var2.put("curDutyId", curDutyId);
        try {
            partyMoneyService.approve(partyMoney, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            partyMoney = partyMoneyService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("partymoney");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            System.out.println("comment:"+comment);
            LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            partyMoneyService.reject(partyMoney, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            partyMoney = partyMoneyService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("partymoney");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:"+key);
            if(com.joint.base.util.StringUtils.isEmpty(comment)){
                comment="";
            }
            partyMoneyService.deny(partyMoney, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }



    public PartyMoney getPartyMoney() {
        return partyMoney;
    }

    public void setPartyMoney(PartyMoney partyMoney) {
        this.partyMoney = partyMoney;
    }
    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getViewtype() {
        return viewtype;
    }
    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }


    @Override
    public String getResult() {
        return result;
    }

    @Override
    public void setResult(String result) {
        this.result = result;
    }

    public String getIsParty() {
        return isParty;
    }

    public void setIsParty(String isParty) {
        this.isParty = isParty;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getUsersId() {
        return usersId;
    }

    public void setUsersId(String usersId) {
        this.usersId = usersId;
    }

    public String getPartyMoneyDepartId() {
        return partyMoneyDepartId;
    }

    public void setPartyMoneyDepartId(String partyMoneyDepartId) {
        this.partyMoneyDepartId = partyMoneyDepartId;
    }

    public BigDecimal getEveryPay() {
        return everyPay;
    }

    public void setEveryPay(BigDecimal everyPay) {
        this.everyPay = everyPay;
    }

    public String getPayDateYear() {
        return payDateYear;
    }

    public void setPayDateYear(String payDateYear) {
        this.payDateYear = payDateYear;
    }

    public String getPayDateMonth() {
        return payDateMonth;
    }

    public void setPayDateMonth(String payDateMonth) {
        this.payDateMonth = payDateMonth;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public String getPayMonth() {
        return payMonth;
    }

    public void setPayMonth(String payMonth) {
        this.payMonth = payMonth;
    }

    public String getPayDateString() {
        return payDateString;
    }

    public void setPayDateString(String payDateString) {
        this.payDateString = payDateString;
    }
}


