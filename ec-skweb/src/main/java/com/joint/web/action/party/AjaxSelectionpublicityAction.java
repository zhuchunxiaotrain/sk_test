package com.joint.web.action.party;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.RoleService;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.SelectionPublicity;
import com.joint.core.entity.SelectionNotice;
import com.joint.core.entity.SelectionPublicity;
import com.joint.core.entity.SelectionSpeech;
import com.joint.core.service.SelectionPublicityService;
import com.joint.core.service.SelectionNoticeService;
import com.joint.core.service.SelectionPublicityService;
import com.joint.core.service.SelectionSpeechService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dqf on 2015/8/26.
 */
@ParentPackage("party")
public class AjaxSelectionpublicityAction extends BaseFlowAction {
  
    @Resource
    private SelectionPublicityService selectionPublicityService;
    @Resource
    private SelectionNoticeService selectionNoticeService;
    @Resource
    private SelectionSpeechService selectionSpeechService;
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private RoleService roleService;

    private SelectionPublicity selectionPublicity;
    private int numStatus;
    private Users loginUser;

    //演讲对象id
    private String selectionSpeechId;

    //附件对象id
    private String publicityAttchmentId;
    private String meetingDecisionAttchmentId;






    /**
     * 竞聘演讲对象
     */
    private SelectionSpeech selectionSpeech;
    /**
     * 公告对象
     */
    private SelectionNotice selectionNotice;
    /**
     * 结果公示
     */
    private List<FileManage> attchment;
    /**
     * 相关资料文本
     */
    private String attchmentText;
    /**
     * 党总支人事决策会议
     */
    private List<FileManage> meetingDecisionAttchment;
    /**
     * 党总支人事决策会议文本
     */
    private String meetingDecisionAttchmentText;

    /**
     * 公示开始日期
     */
    private Date publicStartDate;
    /**
     * 公示结束日期
     */
    private Date publicEndDate;
    /**
     * 公示结果
     */
    private String result;


    private String viewtype;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "selectionpublicity";
    }

    /**
     * ajax异步查询一个岗位是否已经结果公示
     * @return
     */
    public String check(){
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        //公告对象
        SelectionNotice selectionNotice=new SelectionNotice();
        if(StringUtils.isNotEmpty(parentId)){
            String[] str=parentId.split(",");
            for(int i=0;i<str.length-1;i++){
                if(str[i].trim()==str[i+1].trim()){
                    str[i] = "";
                }
            }
            for(int j=0;j<str.length;j++){
                if(str[j] != ""){
                    selectionNotice=selectionNoticeService.get(str[j].trim());
                }
            }
            params.put("selectionNotice",selectionNotice);
        }
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("result","0");
        params.put("processState",FlowEnum.ProcessState.Finished);
        List<SelectionPublicity> selectionPublicitysList=null;
        selectionPublicitysList =  selectionPublicityService.getList(params);
        Map<String, Object> data = new HashMap<String, Object>();
        int num =0;
        if(selectionPublicitysList.size()>0){
            num=selectionPublicitysList.size();
            data.put("num",num);
        }else{
            data.put("num",0);
        }
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    public String listfordialog(){

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);


        pager=selectionPublicityService.findByPagerAndProcessState(pager, user, "selectionpublicity", FlowEnum.ProcessState.Finished, params);
        List<SelectionPublicity> selectionPublicityList;
        if (pager.getTotalCount() > 0){
            selectionPublicityList = (List<SelectionPublicity>) pager.getList();
        }else{
            selectionPublicityList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(SelectionPublicity selectionPublicity:selectionPublicityList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",selectionPublicity.getId()!=null?selectionPublicity.getId():"");
            rMap.put("name",selectionPublicity.getSelectionSpeech()!=null?selectionPublicity.getSelectionSpeech().getCreater().getName():"");
            rMap.put("post",selectionPublicity.getSelectionSpeech().getSelectionNotice()!=null?selectionPublicity.getSelectionSpeech().getSelectionNotice().getPost().getName():"");
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }


        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String list(){

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        //公告对象
        SelectionNotice selectionNotice=new SelectionNotice();
        if(StringUtils.isNotEmpty(parentId)){
            String[] str=parentId.split(",");
            for(int i=0;i<str.length-1;i++){
                if(str[i].trim()==str[i+1].trim()){
                    str[i] = "";
                }
            }
            for(int j=0;j<str.length;j++){
                if(str[j] != ""){
                    selectionNotice=selectionNoticeService.get(str[j].trim());
                }
            }
            params.put("selectionNotice",selectionNotice);
        }

        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);

        viewtype="";
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                pager=selectionPublicityService.findByPagerAndLimit(false, "selectionpublicity", pager, params);
            }else if(viewtype.equals("2")){
                //已归档
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                pager=selectionPublicityService.findByPagerAndFinish("selectionpublicity", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=selectionPublicityService.findByPagerAndLimit(true, "selectionpublicity", pager, params);
        }

        List<SelectionPublicity> selectionPublicitysList;
        if (pager.getTotalCount() > 0){

            selectionPublicitysList = (List<SelectionPublicity>) pager.getList();
        }else{
            selectionPublicitysList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(SelectionPublicity selectionPublicity:selectionPublicitysList){

            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            rMap=new HashMap<>();
            rMap.put("id",selectionPublicity.getId()!=null?selectionPublicity.getId():"");
            rMap.put("applyUsers",selectionPublicity.getApplyUsers()!=null?selectionPublicity.getApplyUsers().getName():"");
            if(selectionPublicity.getResult()!=""){
                if(selectionPublicity.getResult().equals("1")){
                    rMap.put("result","不通过");
                }else if(selectionPublicity.getResult().equals("0")){
                    rMap.put("result","通过");
                }else{
                    rMap.put("result","发生未知错误");
                }
            }else{
                rMap.put("result","发生未知错误");
            }
            if(selectionPublicity.getPublicStartDate()!=null&&selectionPublicity.getPublicEndDate()!=null){
                rMap.put("publicDate",new SimpleDateFormat("yyyy-MM-dd").format(selectionPublicity.getPublicStartDate())+" --- "+new SimpleDateFormat("yyyy-MM-dd").format(selectionPublicity.getPublicEndDate()));
            }else{
                rMap.put("publicDate","");
            }
            if(selectionPublicity.getProcessState()!=null){
                String name=selectionPublicity.getProcessState().name();
                if(name.equals("Finished")){
                    rMap.put("processState","已归档");
                }else if(name.equals("Running")){
                    rMap.put("processState","流转中");
                }else if(name.equals("Backed")){
                    rMap.put("processState","退回中");
                }else{
                    rMap.put("processState","被否决");
                }
            }
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    /**
     * 自定义过滤器
     * @param _search
     * @param params
     * @param filters
     * @return
     */
    public Map getSearchFilterParams(String _search,Map<String,Object> params,String filters){
        //createAlias Key集合
        List<String> alias = new ArrayList<String>();
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(_search)){
            boolean __search = Boolean.parseBoolean(_search);
            if(__search&& org.apache.commons.lang3.StringUtils.isNotEmpty(filters)){
                JSONObject filtersJson = JSONObject.fromObject(filters);
                if(org.apache.commons.lang3.StringUtils.equals(filtersJson.getString("groupOp"), "AND")){
                    //查询条件
                    JSONArray rulesJsons = filtersJson.getJSONArray("rules");
                    //遍历条件
                    for(int i=0;i<rulesJsons.size();i++){
                        JSONObject rule = rulesJsons.getJSONObject(i);
                        if(rule.has("field")&& org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("field"))&&rule.has("data")&& org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                            String field = rule.getString("field");
                            if(field.contains("_")){
                                String[] fields = field.split("_");
                                if(org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                                    params.put(field.replaceAll("_", "."), rule.getString("data"));
                                }
                            }else{
                                if(org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                                    if(field.equals("result")){
                                        String result=rule.getString("data");
                                        if(result.equals("通过")){
                                            params.put("result", "0");
                                        }else if(result.equals("不通过")){
                                            params.put("result","1");
                                        } else {//输入错误
                                            params.put("result",new String []{"0","1"});

                                        }
                                    }

                                    if(field.equals("publicStartDate")){
                                        params.put("publicStartDate", DataUtil.StringToDate(rule.getString("data")));
                                    }

                                }
                            }
                        }
                    }

                }
            }
        }

        return params;
    }
    public String read(){
        selectionSpeech=new SelectionSpeech();
        selectionNotice=new SelectionNotice();
        attchment=Lists.newArrayList();
        meetingDecisionAttchment=Lists.newArrayList();
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty( keyId)) {
            Company company = usersService.getCompanyByUser();
            selectionPublicity = selectionPublicityService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            //公告对象
            if(selectionPublicity.getSelectionNotice()!=null){
                selectionNotice=selectionPublicity.getSelectionNotice();
                parentId=selectionNotice.getId();
            }

            // 竞聘演讲对象
            if(selectionPublicity.getSelectionSpeech()!=null){
                selectionSpeech=selectionPublicity.getSelectionSpeech();
            }
            //附件对象
            if(selectionPublicity.getAttchment()!=null){
                attchment=selectionPublicity.getAttchment();
            }
            //党总支人事决策会议附件对象
            if(selectionPublicity.getMeetingDecisionAttchment()!=null){
               meetingDecisionAttchment=selectionPublicity.getMeetingDecisionAttchment();
            }
        }
        return "read";
    }

    public String input(){
        selectionNotice=new SelectionNotice();
        selectionSpeech=new SelectionSpeech();
        attchment=Lists.newArrayList();
        meetingDecisionAttchment=Lists.newArrayList();
        if (StringUtils.isNotEmpty(keyId)){
            selectionPublicity = selectionPublicityService.get(keyId);
            Company company = usersService.getCompanyByUser();
            //公告对象
            if(selectionPublicity.getSelectionNotice()!=null){
                selectionNotice=selectionPublicity.getSelectionNotice();
                parentId=selectionNotice.getId();
            }

            // 竞聘演讲对象
            if(selectionPublicity.getSelectionSpeech()!=null){
                selectionSpeech=selectionPublicity.getSelectionSpeech();
            }
            //附件对象
            if(selectionPublicity.getAttchment()!=null){
                attchment=selectionPublicity.getAttchment();
            }
            //党总支人事决策会议附件对象
            if(selectionPublicity.getMeetingDecisionAttchment()!=null){
                meetingDecisionAttchment=selectionPublicity.getMeetingDecisionAttchment();
            }

        } else {

        }



        return "input";
    }

    private void setData(){

        attchment=Lists.newArrayList();
        meetingDecisionAttchment=Lists.newArrayList();
        selectionSpeech=new SelectionSpeech();
        selectionNotice=new SelectionNotice();
        if(StringUtils.isNotEmpty(keyId)){
            selectionPublicity = selectionPublicityService.get(keyId);
        }else{
            selectionPublicity = new SelectionPublicity();
            selectionPublicity.setCreater(usersService.getLoginInfo());
        }

        // 竞聘演讲对象
        if(StringUtils.isNotEmpty(selectionSpeechId)){
            selectionSpeech=selectionSpeechService.get(selectionSpeechId);
        }
        //公告对象
        if(StringUtils.isNotEmpty(parentId)){
            String[] str=parentId.split(",");
            for(int i=0;i<str.length-1;i++){
                if(str[i].trim()==str[i+1].trim()){
                    str[i] = "";
                }
            }
            for(int j=0;j<str.length;j++){
                if(str[j] != ""){
                    selectionNotice=selectionNoticeService.get(str[j].trim());
                }
            }
        }
        //附件对象
        if(StringUtils.isNotEmpty(publicityAttchmentId)){
            for(String s:publicityAttchmentId.split(",")){
                attchment.add(fileManageService.get(s.trim()));
            }
        }
        //党总支人事决策会议
        if(StringUtils.isNotEmpty(meetingDecisionAttchmentId)){
            for(String s:meetingDecisionAttchmentId.split(",")){
                meetingDecisionAttchment.add(fileManageService.get(s.trim()));
            }
        }

        selectionPublicity.setCreateDate(new Date());
        selectionPublicity.setCompany(usersService.getLoginInfo().getCompany());
        selectionPublicity.setSelectionSpeech(selectionSpeech);
        selectionPublicity.setSelectionNotice(selectionNotice);
        selectionPublicity.setApplyUsers(selectionSpeech.getCreater());
        selectionPublicity.setAttchment(attchment);
        selectionPublicity.setAttchmentText(attchmentText);
        selectionPublicity.setMeetingDecisionAttchmentText(meetingDecisionAttchmentText);
        selectionPublicity.setMeetingDecisionAttchment(meetingDecisionAttchment);
        selectionPublicity.setPublicStartDate(publicStartDate);
        selectionPublicity.setPublicEndDate(publicEndDate);
        selectionPublicity.setResult(result);

        selectionPublicityService.save(selectionPublicity);

    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                selectionPublicityService.update(selectionPublicity);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                selectionPublicityService.save(selectionPublicity, "selectionpublicity", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        setData();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        var2.put("initDuty", curDutyId);
        ArrayList<String> list = new ArrayList<String>();


      /*  var2.put("approvers", list);    // 会签人Ids*/
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                selectionPublicityService.approve(selectionPublicity, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);

            } else {
                selectionPublicityService.commit(selectionPublicity, FlowEnum.ProcessState.Finished,"selectionpublicity",var1,var2,curDutyId);
                /*String id=selectionPublicityService.commit(selectionPublicity, "selectionpublicity", var1, var2, curDutyId);
                SelectionPublicity entity=selectionPublicityService.get(id);
                entity.setProcessState(FlowEnum.ProcessState.Finished);
                selectionPublicityService.save(entity);*/
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批
    public String approve1(){
        selectionPublicity = selectionPublicityService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();

        var1.put("numStatus", 2);
        //  var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                selectionPublicityService.approve(selectionPublicity, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }
    // 会签
    public String approve2() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        Map<String, Object> var2 = new HashMap<String, Object>();

        selectionPublicity = selectionPublicityService.get(keyId);
        /*Task task=workflowService.getCurrentTask(keyId, usersService.getLoginInfo());
      

        int multComplete = (Integer)runtimeService.getVariable(task.getExecutionId(),"nrOfCompletedInstances");
        int nrOfInstances = (Integer)runtimeService.getVariable(task.getExecutionId(),"nrOfInstances");
        LogUtil.info("multComplete:"+multComplete);
        LogUtil.info("nrOfInstances:"+nrOfInstances);

        if((double)multComplete/nrOfInstances>=1){
            var2.put("numStatus", 2);
            LogUtil.info("do:numStatus2");
        } else {
            var2.put("numStatus", 1);
        }*/

        //  var2.put("curDutyId", curDutyId);
        try {

            selectionPublicityService.approve(selectionPublicity, FlowEnum.ProcessState.Running, var2, curDutyId, comment);

            Task overtask = workflowService.getCurrentTask(keyId);
            if(overtask==null){/*没有task就到了归档，所以*/
                SelectionPublicity selectionPublicity=selectionPublicityService.get(keyId);
                selectionPublicity.setProcessState(FlowEnum.ProcessState.Finished);
                selectionPublicityService.update(selectionPublicity);


                /*selectionSpeech= new SelectionSpeech();
                selectionSpeech.setCreater(selectionPublicity.getCreater());
                selectionSpeech.setCreateDate(new Date());
                selectionSpeech.setSpeechState("竞聘演讲");
                selectionSpeech.setSelectionPublicity(selectionPublicity);
                selectionSpeech.setResult("未审批");
                selectionSpeech.setSelectionNotice(selectionPublicity.getSelectionNotice());
                selectionSpeech.setCompany(selectionPublicity.getCreater().getCompany());
                selectionSpeechService.save(selectionSpeech);*/


            }else {
                if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 3);
                }
            }
    } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String approve3() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        selectionPublicity = selectionPublicityService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 4);
        //  var2.put("curDutyId", curDutyId);
        try {
            selectionPublicityService.approve(selectionPublicity, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            selectionPublicity = selectionPublicityService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("selectionpublicity");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            System.out.println("comment:"+comment);
            LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            selectionPublicityService.reject(selectionPublicity, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            selectionPublicity = selectionPublicityService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("selectionpublicity");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:"+key);
            if(com.joint.base.util.StringUtils.isEmpty(comment)){
                comment="";
            }
            selectionPublicityService.deny(selectionPublicity, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }



    public SelectionPublicity getSelectionPublicity() {
        return selectionPublicity;
    }

    public void setSelectionPublicity(SelectionPublicity selectionPublicity) {
        this.selectionPublicity = selectionPublicity;
    }
    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getViewtype() {
        return viewtype;
    }
    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public String getPublicityAttchmentId() {
        return publicityAttchmentId;
    }

    public void setPublicityAttchmentId(String publicityAttchmentId) {
        this.publicityAttchmentId = publicityAttchmentId;
    }

    public String getSelectionSpeechId() {
        return selectionSpeechId;
    }

    public void setSelectionSpeechId(String selectionSpeechId) {
        this.selectionSpeechId = selectionSpeechId;
    }

    public SelectionSpeech getSelectionSpeech() {
        return selectionSpeech;
    }

    public void setSelectionSpeech(SelectionSpeech selectionSpeech) {
        this.selectionSpeech = selectionSpeech;
    }

    public List<FileManage> getAttchment() {
        return attchment;
    }

    public void setAttchment(List<FileManage> attchment) {
        this.attchment = attchment;
    }

    public String getAttchmentText() {
        return attchmentText;
    }

    public void setAttchmentText(String attchmentText) {
        this.attchmentText = attchmentText;
    }

    public Date getPublicStartDate() {
        return publicStartDate;
    }

    public void setPublicStartDate(Date publicStartDate) {
        this.publicStartDate = publicStartDate;
    }

    public Date getPublicEndDate() {
        return publicEndDate;
    }

    public void setPublicEndDate(Date publicEndDate) {
        this.publicEndDate = publicEndDate;
    }

    public SelectionNotice getSelectionNotice() {
        return selectionNotice;
    }

    public void setSelectionNotice(SelectionNotice selectionNotice) {
        this.selectionNotice = selectionNotice;
    }

    public String getMeetingDecisionAttchmentId() {
        return meetingDecisionAttchmentId;
    }

    public void setMeetingDecisionAttchmentId(String meetingDecisionAttchmentId) {
        this.meetingDecisionAttchmentId = meetingDecisionAttchmentId;
    }

    public List<FileManage> getMeetingDecisionAttchment() {
        return meetingDecisionAttchment;
    }

    public void setMeetingDecisionAttchment(List<FileManage> meetingDecisionAttchment) {
        this.meetingDecisionAttchment = meetingDecisionAttchment;
    }

    public String getMeetingDecisionAttchmentText() {
        return meetingDecisionAttchmentText;
    }

    public void setMeetingDecisionAttchmentText(String meetingDecisionAttchmentText) {
        this.meetingDecisionAttchmentText = meetingDecisionAttchmentText;
    }

    @Override
    public String getResult() {
        return result;
    }

    @Override
    public void setResult(String result) {
        this.result = result;
    }

}


