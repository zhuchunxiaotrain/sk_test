package com.joint.web.action.manage;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.*;
import com.joint.base.service.activiti.WorkflowService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.Dispatch;
import com.joint.core.entity.manage.DispatchRevise;
import com.joint.core.entity.manage.MeetingRecord;
import com.joint.core.entity.manage.MeetingUse;
import com.joint.core.service.DispatchReviseService;
import com.joint.core.service.DispatchService;
import com.joint.core.service.MeetingRecordService;
import com.joint.core.service.MeetingUseService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.*;


@ParentPackage("manage")
public class AjaxMeetingrecordAction extends BaseFlowAction {
    @Resource
    private WorkflowService workflowService;
    @Resource
    private MeetingUseService meetingUseService;
    @Resource
    private MeetingRecordService meetingRecordService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private PostService postService;
    @Resource
    private PowerService powerService;
    @Resource
    private DutyService dutyService;
    @Resource
    private UsersService usersService;
    @Resource
    private ReadersService readersService;
    @Resource
    private TaskRecordService taskRecordService;

    /**
     * 会议室使用登记表(读)
     */
    private MeetingUse meetinguse;
    /**
     * 会议记录登记对象(读)
     */
    private MeetingRecord meetingrecord;
    /**
     * 登录人(读)
     */
    private Users loginUser;
    /**
     * 会议纪要上传附件(读)
     */
    private String fileId;
    /**
     * 备注
     */
    private String remark;

    /**
     * 会议纪要上传Id
     */
    private String fileIds;


    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "meetingrecord";
    }



    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");

        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        //内嵌视图这里要加上
        if (StringUtils.isNotEmpty(parentId)){
            meetinguse = meetingUseService.get(parentId);
            params.put("meetingUse",meetinguse);
        }
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);

        //所有单子
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
        pager=meetingRecordService.findByPagerAndLimit(true, "meetingrecord", pager, params);

        List<MeetingRecord>  meetingRecordList;
        if (pager.getTotalCount() > 0){
            meetingRecordList = (List<MeetingRecord>) pager.getList();
        }else{
            meetingRecordList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(MeetingRecord meetingRecord: meetingRecordList){
            rMap = new HashMap<String, Object>();
            rMap.put("id",meetingRecord.getId());
            rMap.put("creater", meetingRecord.getCreater() != null ? meetingRecord.getCreater().getName() : "");
            rMap.put("createDate", DataUtil.DateToString(meetingRecord.getCreateDate(),"yyyy-MM-dd"));
            rMap.put("state",meetingRecord.getProcessState().value());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    public String read(){
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)) {
            fileId ="";
            meetingrecord = meetingRecordService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            flowNumStatus = workflowService.getFlowNumStatus(keyId);
            if(meetingrecord.getFile() != null && meetingrecord.getFile().size()>0){
                for(FileManage f:meetingrecord.getFile()){
                    fileId+=f.getId()+",";
                }
            }

        }

        return "read";
    }


    public String input(){
      //  Company company = usersService.getCompanyByUser();
      //  Map<String,Object> rMap = null;
        if (StringUtils.isNotEmpty(parentId)){

        }
        if (StringUtils.isNotEmpty(keyId)){
            fileId ="";
            meetingrecord = meetingRecordService.get(keyId);
            if(meetingrecord.getFile() != null && meetingrecord.getFile().size()>0){
                for(FileManage f:meetingrecord.getFile()){
                    fileId+=f.getId()+",";
                }
            }

        } else {


        }

        return "input";
    }

    private void setData(){
        if(StringUtils.isNotEmpty(keyId)){
            meetingrecord = meetingRecordService.get(keyId);
        }else{
            meetingrecord  = new MeetingRecord();
            meetingrecord.setCreater(usersService.getLoginInfo());
        }

        if(StringUtils.isNotEmpty(parentId)){
            meetinguse = meetingUseService.get(parentId);
            meetingrecord.setMeetingUse(meetinguse);
        }

        List<FileManage> fileManageList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(fileIds)){
            for(String f:fileIds.split(",")){
                fileManageList.add(fileManageService.get(f.trim()));
            }
        }
        meetingrecord.setFile(fileManageList);
        meetingrecord.setCompany(usersService.getLoginInfo().getCompany());
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                meetingRecordService.update(meetingrecord);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                meetingRecordService.save(meetingrecord, "meetingrecord", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }


    // 提交
    public String commit(){
        setData();
        Company company = usersService.getCompanyByUser();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        Set<Users> usersSet = Sets.newHashSet();
        Map<String, Object> map = new HashMap<String, Object>();

        meetinguse = meetingrecord.getMeetingUse();
        if(meetinguse!=null && meetinguse.getJoinUsers() != null){
            for(Users users: meetinguse.getJoinUsers()){
                usersSet.add(users);
            }
        }



        try {
            if (StringUtils.isNotEmpty(keyId)) {
                meetingRecordService.approve(meetingrecord, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
            } else {
                keyId = meetingRecordService.commit(meetingrecord, FlowEnum.ProcessState.Finished, "meetingrecord", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        //先删除读者
        readersService.deleteByKeyIdBussinessKey(keyId);
        //增加归档后读者
        for(Users users:usersSet){
            Readers readers = new Readers();
            readers.setUsers(users);
            readers.setBussinessKey("meetingrecord");
            readers.setKeyId(keyId);
            readers.setType(2);
            readersService.save(readers);
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 检验身份
     * @return
     */
    public String checkUser(){
        if(StringUtils.isNotEmpty(parentId)){
            MeetingUse meetingUse = meetingUseService.get(parentId);
            Users users = usersService.getLoginInfo();
            if(users != null && meetingUse.getCreater() != null){
                if(StringUtils.equals(users.getId(),meetingUse.getCreater().getId())){
                    return ajaxHtmlCallback("200", "身份合法", "操作状态");
                }
            }

        }
        return ajaxHtmlCallback("400", "身份非法", "操作状态");
    }

    public MeetingUse getMeetinguse() {
        return meetinguse;
    }

    public void setMeetinguse(MeetingUse meetinguse) {
        this.meetinguse = meetinguse;
    }

    public MeetingRecord getMeetingrecord() {
        return meetingrecord;
    }

    public void setMeetingrecord(MeetingRecord meetingrecord) {
        this.meetingrecord = meetingrecord;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFileIds() {
        return fileIds;
    }

    public void setFileIds(String fileIds) {
        this.fileIds = fileIds;
    }
}

