package com.joint.web.action.manage;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.*;
import com.joint.base.service.activiti.WorkflowService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.Contract;
import com.joint.core.service.ContractService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@ParentPackage("manage")
public class AjaxContractAction extends BaseFlowAction {
    @Resource
    private WorkflowService workflowService;
    @Resource
    private ContractService contractService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private DutyService dutyService;
    @Resource
    private UsersService usersService;
    @Resource
    private ReadersService readersService;
    @Resource
    private TaskRecordService taskRecordService;

    private Contract contract;
    private String viewtype;
    private int numStatus;
    private Users loginUser;

    /**
     * 合同编号
     */
    private String no;
    /**
     * 合同名称
     */
    private String name;
    /**
     * 经办部门
     */
    private String departmentId;
    /**
     * 经办人
     */
    private String manager;
    /**
     * 经办人职务
     */
    private String managerPost;
    /**
     * 合同甲方
     */
    private String contractA;
    /**
     * 合同一方
     */
    private String contractB;
    /**
     * 合同第三方
     */
    private String contractC;
    /**
     * 合同其他方
     */
    private String contractD;
    /**
     * 预算编号
     */
    private String budgetNo;
    /**
     * 合同标的（小写）
     */
    private String subjectSmall;
    /**
     * 合同标的（大写）
     */
    private String subjectBig;
    /**
     * 合同日期
     */
    private String date;
    /**
     * 主要事宜
     */
    private String mainThing;
    /**
     * 附件
     */
    private String fileId;
    private String fileIds;
    /**
     * 是否需要会签
     */
    private int mulExam;
    /**
     * 会签人员id
     */
    private String examUsersId;
    /**
     * 是否需要上报中心
     */
    private int leaderExam;
    /**
     * 是否需要党政联合会议
     */
    private int partyExam;
    /**
     * 是否是产业中心人员(0不是 1是)
     */
    private int ifCentralStaff;

    public String execute(){
        return "contract";
    }

    public String list(){
        pager = new Pager(0);
        /*
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);*/
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
       // params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                pager=contractService.findByPagerAndLimit(false, "contract", pager, params);
            }else if(viewtype.equals("2")){
                //已归档
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                pager=contractService.findByPagerAndFinish( "contract", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=contractService.findByPagerAndLimit(true, "contract", pager, params);
        }
        List<Contract> contractList;
        if (pager.getTotalCount() > 0){
            contractList = (List<Contract>) pager.getList();
        }else{
            contractList= new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(Contract contract: contractList){
            rMap = new HashMap<String, Object>();
            rMap.put("id",contract.getId());
            rMap.put("no",contract.getNo());
            rMap.put("name",contract.getName());
            rMap.put("contracta",contract.getContractA());
            rMap.put("contractb",contract.getContractB());
            rMap.put("subject",contract.getSubjectSmall());
            rMap.put("state",contract.getProcessState().value());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String read(){
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)) {
            fileId ="";
            contract = contractService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            flowNumStatus = workflowService.getFlowNumStatus(keyId);
            if(contract.getFile() != null && contract.getFile().size()>0){
                for(FileManage f:contract.getFile()){
                    fileId+=f.getId()+",";
                }
            }
        }
        return "read";
    }

    public String input(){
        if (StringUtils.isNotEmpty(keyId)){
            fileId ="";
            contract = contractService.get(keyId);
            if(contract.getFile() != null && contract.getFile().size()>0){
                for(FileManage f:contract.getFile()){
                    fileId+=f.getId()+",";
                }
            }
        }
        return "input";
    }

    public void setData(){
        if(StringUtils.isNotEmpty(keyId)){
            contract=contractService.get(keyId);
        }else{
            contract=new Contract();
            contract.setCreater(usersService.getLoginInfo());
        }
        contract.setNo(no);
        contract.setName(name);
        contract.setDepartment(departmentService.get(departmentId));
        contract.setManager(manager);
        contract.setManagerPost(managerPost);
        contract.setContractA(contractA);
        contract.setContractB(contractB);
        contract.setContractC(contractC);
        contract.setContractD(contractD);
        contract.setBudgetNo(budgetNo);
        contract.setSubjectSmall(subjectSmall);
        contract.setSubjectBig(subjectBig);
        contract.setDate(DataUtil.StringToDate(date));
        contract.setMainThing(mainThing);
        List<FileManage> fileManageList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(fileIds)){
            for(String f:fileIds.split(",")){
                fileManageList.add(fileManageService.get(f.trim()));
            }
        }
        contract.setFile(fileManageList);
        contract.setCompany(usersService.getLoginInfo().getCompany());
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                contractService.update(contract);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                contractService.save(contract, "contract", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        ifCentralStaff=0;
        setData();
        Company company = usersService.getCompanyByUser();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);

        Set<Duty> dutySet = usersService.getLoginInfo().getDutySet();
        for (Duty duty:dutySet){
            if(duty.getDepartment().getParent()!=null){
                if(StringUtils.equals(duty.getDepartment().getParent().getName(),"产业中心")){
                    ifCentralStaff=1;
                    break;
                }
            }
        }
        if(ifCentralStaff==1){
            var1.put("numStatus",2);
            var2.put("numStatus",2);
        }else{
            var1.put("numStatus",1);
            var2.put("numStatus",1);
        }

        List<String> personList = Lists.newArrayList();
        Set<Duty> dutySets= null;
        dutySets=usersService.getLoginInfo().getDutySet();
        for(Duty duty:dutySets){
            Department dept = duty.getDepartment().getParent()==null?duty.getDepartment():duty.getDepartment().getParent();
            Users user=dutyService.getPrincipal(dept);
            personList.add(user.getId());
        }
        var2.put("person", personList);

        var2.put("curDutyId", curDutyId);
        var2.put("ifCentralStaff", ifCentralStaff);

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                contractService.approve(contract, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            } else {
                keyId = contractService.commit(contract, "contract", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批1
    public String approve1(){
        contract = contractService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 2);
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                contractService.approve(contract, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批2
    public String approve2(){
        ifCentralStaff=0;
        contract = contractService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 3);
        var1.put("curDutyId", curDutyId);
        var1.put("mulExam", mulExam);
        var1.put("partyExam", partyExam);
        if(mulExam==1){
            var1.put("numStatus", 3);
        }else{
            Set<Duty> dutySet = contract.getCreater().getDutySet();
            for (Duty duty:dutySet){
                if(duty.getDepartment().getParent()!=null){
                    if(StringUtils.equals(duty.getDepartment().getParent().getName(),"产业中心")){
                        ifCentralStaff=1;
                        break;
                    }
                }
            }
            var1.put("ifCentralStaff", ifCentralStaff);
            if(ifCentralStaff==1){
                var1.put("numStatus", 5);
            }else{
                var1.put("numStatus", 4);
            }
        }
        List<Users> usersList = Lists.newArrayList();
        List<String> stringList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(examUsersId)){
            String[] idArr = examUsersId.split(",");
            for(String id:idArr){
                usersList.add(usersService.get(id.trim()));
                stringList.add(id.trim());
            }
        }
        var1.put("approvers", stringList);
        contract.setMulExam(mulExam == 1 ? true : false);
        contract.setPartyExam(partyExam == 1 ? true : false);
        contract.setExamUsers(usersList);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                contractService.approve(contract, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        /*
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }*/
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批3
    public String approve3(){
        ifCentralStaff=0;
        contract = contractService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                contractService.approve(contract, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            if(overtask==null){
                Contract contract = contractService.get(keyId);
                contract.setProcessState(FlowEnum.ProcessState.Finished);
                contractService.update(contract);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(contract.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    Set<Duty> dutySet = contract.getCreater().getDutySet();
                    for (Duty duty:dutySet){
                        if(duty.getDepartment().getParent()!=null){
                            if(StringUtils.equals(duty.getDepartment().getParent().getName(),"产业中心")){
                                ifCentralStaff=1;
                                break;
                            }
                        }
                    }
                    var1.put("ifCentralStaff", ifCentralStaff);
                    if(ifCentralStaff==1){
                        runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 5);
                    }else{
                        runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 4);
                    }
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批4
    public String approve4(){
        contract = contractService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        var1.put("leaderExam", leaderExam);
        contract.setLeaderExam(leaderExam == 1 ? true : false);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                contractService.approve(contract, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            if(overtask==null){
                Contract contract = contractService.get(keyId);
                contract.setProcessState(FlowEnum.ProcessState.Finished);
                contractService.update(contract);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(contract.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    if(leaderExam==1){
                        runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 5);
                    }else{
                        if(partyExam==1){
                            runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 6);
                        }else{
                            runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 7);
                        }
                    }

                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批5
    public String approve5(){
        contract = contractService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                contractService.approve(contract, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            if(overtask==null){
                Contract contract = contractService.get(keyId);
                contract.setProcessState(FlowEnum.ProcessState.Finished);
                contractService.update(contract);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(contract.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    if(partyExam==1){
                        runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 6);
                    }else{
                        runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 7);
                    }

                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批6
    public String approve6(){
        contract = contractService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                contractService.approve(contract, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            if(overtask==null){
                Contract contract = contractService.get(keyId);
                contract.setProcessState(FlowEnum.ProcessState.Finished);
                contractService.update(contract);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(contract.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 7);
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批7
    public String approve7(){
        contract = contractService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                contractService.approve(contract, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            if(overtask==null){
                Contract contract = contractService.get(keyId);
                contract.setProcessState(FlowEnum.ProcessState.Finished);
                contractService.update(contract);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(contract.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 8);
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (StringUtils.isNotEmpty(keyId)) {
            contract = contractService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("contract");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            // System.out.println("comment:"+comment);
            LogUtil.info("numStatus:" + numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            contractService.reject(contract, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (StringUtils.isNotEmpty(keyId)) {
            contract = contractService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("contract");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:" + key);
            if(StringUtils.isEmpty(comment)){
                comment="";
            }
            contractService.deny(contract, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }


    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getManagerPost() {
        return managerPost;
    }

    public void setManagerPost(String managerPost) {
        this.managerPost = managerPost;
    }

    public String getContractA() {
        return contractA;
    }

    public void setContractA(String contractA) {
        this.contractA = contractA;
    }

    public String getContractB() {
        return contractB;
    }

    public void setContractB(String contractB) {
        this.contractB = contractB;
    }

    public String getContractC() {
        return contractC;
    }

    public void setContractC(String contractC) {
        this.contractC = contractC;
    }

    public String getContractD() {
        return contractD;
    }

    public void setContractD(String contractD) {
        this.contractD = contractD;
    }

    public String getBudgetNo() {
        return budgetNo;
    }

    public void setBudgetNo(String budgetNo) {
        this.budgetNo = budgetNo;
    }

    public String getSubjectSmall() {
        return subjectSmall;
    }

    public void setSubjectSmall(String subjectSmall) {
        this.subjectSmall = subjectSmall;
    }

    public String getSubjectBig() {
        return subjectBig;
    }

    public void setSubjectBig(String subjectBig) {
        this.subjectBig = subjectBig;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMainThing() {
        return mainThing;
    }

    public void setMainThing(String mainThing) {
        this.mainThing = mainThing;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileIds() {
        return fileIds;
    }

    public void setFileIds(String fileIds) {
        this.fileIds = fileIds;
    }

    public int getMulExam() {
        return mulExam;
    }

    public void setMulExam(int mulExam) {
        this.mulExam = mulExam;
    }

    public String getExamUsersId() {
        return examUsersId;
    }

    public void setExamUsersId(String examUsersId) {
        this.examUsersId = examUsersId;
    }

    public int getLeaderExam() {
        return leaderExam;
    }

    public void setLeaderExam(int leaderExam) {
        this.leaderExam = leaderExam;
    }

    public int getPartyExam() {
        return partyExam;
    }

    public void setPartyExam(int partyExam) {
        this.partyExam = partyExam;
    }

    public int getIfCentralStaff() {
        return ifCentralStaff;
    }

    public void setIfCentralStaff(int ifCentralStaff) {
        this.ifCentralStaff = ifCentralStaff;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
