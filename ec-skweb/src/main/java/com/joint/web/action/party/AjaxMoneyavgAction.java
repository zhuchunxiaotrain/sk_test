package com.joint.web.action.party;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Department;
import com.joint.base.entity.Users;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.DepartmentService;
import com.joint.base.service.RoleService;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.*;
import com.joint.core.entity.MoneyAvg;
import com.joint.core.service.*;
import com.joint.core.service.MoneyAvgService;
import com.joint.web.action.BaseFlowAction;
import freemarker.template.utility.DateUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dqf on 2015/8/26.
 */
@ParentPackage("party")
public class AjaxMoneyavgAction extends BaseFlowAction {
   
   
    @Resource
    private MoneyAvgService moneyAvgService;
    @Resource
    private MoneyAvgDetailService moneyAvgDetailService;
    @Resource
    private MoneyManagerService moneyManagerService;
    @Resource
    private EmployeesService employeesService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private RoleService roleService;

   
    private MoneyAvg moneyAvg;
    private List<Map<String,Object>> postArrayList;

    private int numStatus;
    private Users loginUser;

   private String moneyAvgDepartId;


    /**
     * 所有支部在这本年度所有月份的总金额
     */
    private BigDecimal allDepartMoney;
    /**
     * 总计党员人数
     */
    private Integer allPartyHuman;
    /**
     * 各个支部分摊详情
     */
    private List<MoneyAvgDetail> moneyAvgDetailList;
    /**
     * 总计分摊金额
     * @return
     */
    private Double allPartyAvgMoney;

    private String viewtype;
    private String detail;

    //部门名称
    private String departId;
    private List<Department> departmentList;
    //党员人数
    private String departmentHuman;
    private List<Integer> departHumanList;
    //分摊的党费
    private String departAvgMoney;
    private List<Double> departAvgMoneyList;
    private String moneyAvgDetailId;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        if(StringUtils.isNotEmpty(detail)){
            if(detail.equals("1")){
                return "moneydetail";
            }else{
                return "moneyavg";
            }
        }else{
            return "moneyavg";
        }

    }


    public String listfordialog(){

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);

        pager=moneyAvgService.findByPagerAndProcessState(pager, user, "moneyavg", FlowEnum.ProcessState.Finished, params);
        List<MoneyAvg> moneyAvgList;
        if (pager.getTotalCount() > 0){
            moneyAvgList = (List<MoneyAvg>) pager.getList();
        }else{
            moneyAvgList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(MoneyAvg moneyAvg:moneyAvgList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",moneyAvg.getId()!=null?moneyAvg.getId():"");
            rMap.put("creater",moneyAvg.getCreater()!=null?moneyAvg.getCreater().getName():"");
            rMap.put("selectDate",moneyAvg.getCreateDate()!=null?new SimpleDateFormat("yyyy-MM-dd").format(moneyAvg.getCreateDate()):"");
            String temp="";
            /*if(moneyAvg.getMoneyAvgDetailList()!=null){
                for (MoneyAvgDetail moneyAvgDetail:moneyAvg.getMoneyAvgDetailList()){
                    temp+=moneyAvgDetail.getMoneyAvgItem().getName()+",";
                }
                rMap.put("dictName",temp);
            }else {
                rMap.put("dictName","");
            }
            rMap.put("moneyAvgMoney",moneyAvg.getMoneyAvgMoney()!=null?moneyAvg.getMoneyAvgMoney().toString():"");
            rMap.put("departmentName",moneyAvg.getDepartment()!=null?moneyAvg.getDepartment().getName():"");*/
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }


        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    public String getEnableMoney(){


        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
       /* params.put("processState",new FlowEnum.ProcessState[]{FlowEnum.ProcessState.Finished});*/
        Department department=new Department();
        if(StringUtils.isNotEmpty(departId)){
            department=departmentService.get(departId.trim());
            params.put("department",department);
        }

        List<MoneyAvgDetail> moneyAvgDetailList=moneyAvgDetailService.getList(params);
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        BigDecimal money=new BigDecimal(0);
        List<Date> createDateList=Lists.newArrayList();
        for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
            createDateList.add(moneyAvgDetail.getCreateDate());
        }
        for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
            if(moneyAvgDetail.getCreateDate()==Collections.min(createDateList)){
                rMap= Maps.newHashMap();
                rMap.put("departResidueEnableMoney",moneyAvgDetail.getDepartResidueEnableMoney()!=null?moneyAvgDetail.getDepartResidueEnableMoney():0.00);
                JSONObject o=JSONObject.fromObject(rMap);
                dataRows.add(o);
            }
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        data.put("money",money);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String count(){

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("processState",FlowEnum.ProcessState.Finished);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);

        pager=moneyAvgService.findByPagerAndProcessState(pager, user, "moneyavg", FlowEnum.ProcessState.Finished, params);
        List<MoneyAvg> moneyAvgList;
        if (pager.getTotalCount() > 0){
            moneyAvgList = (List<MoneyAvg>) pager.getList();
        }else{
            moneyAvgList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        List<Date> createDateArr=Lists.newArrayList();
        BigDecimal money=new BigDecimal(0);

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        data.put("money",money);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }
    public Map getSearchFilterParams(String _search,Map<String,Object> params,String filters){
        //createAlias Key集合
        List<String> alias = new ArrayList<String>();
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(_search)){
            boolean __search = Boolean.parseBoolean(_search);
            if(__search&& org.apache.commons.lang3.StringUtils.isNotEmpty(filters)){
                JSONObject filtersJson = JSONObject.fromObject(filters);
                if(org.apache.commons.lang3.StringUtils.equals(filtersJson.getString("groupOp"), "AND")){
                    //查询条件
                    JSONArray rulesJsons = filtersJson.getJSONArray("rules");
                    //遍历条件
                    for(int i=0;i<rulesJsons.size();i++){
                        JSONObject rule = rulesJsons.getJSONObject(i);
                        if(rule.has("field")&& org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("field"))&&rule.has("data")&& org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                            String field = rule.getString("field");
                            if(field.contains("_")){
                                String[] fields = field.split("_");
                                if(org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                                    params.put(field.replaceAll("_", "."), rule.getString("data"));
                                }
                            }else{
                                if(org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                                    switch(field){
                                        case "year":
                                            params.put("createDate", DataUtil.StringToDate(rule.getString("data"),"yyyy"));
                                            break;
                                        case "allDepartMoney":
                                            params.put("allDepartMoney", new BigDecimal(rule.getString("data")));
                                            break;
                                        case "allPartyHuman":
                                            params.put("allPartyHuman", new BigDecimal(rule.getString("data")));
                                            break;
                                        default:
                                                params.put(field, rule.getString("data"));
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }

        return params;
    }
    public String list(){
        pager = new Pager(0);
        /*if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);*/
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        loginUser = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
       // params = getSearchFilterParams(_search,params,filters);

        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                pager=moneyAvgService.findByPagerAndLimit(false, "moneyavg", pager, params);
            }else if(viewtype.equals("2")){
                //已归档
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                pager=moneyAvgService.findByPagerAndFinish("moneyavg", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=moneyAvgService.findByPagerAndLimit(true, "moneyavg", pager, params);
        }

        List<MoneyAvg> moneyAvgsList;
        if (pager.getTotalCount() > 0){
            moneyAvgsList = (List<MoneyAvg>) pager.getList();
        }else{
            moneyAvgsList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(MoneyAvg moneyAvg:moneyAvgsList){

            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            rMap = new HashMap<String,Object>();
            rMap.put("id",moneyAvg.getId()!=null?moneyAvg.getId():"");
            if(moneyAvg.getCreateDate()!=null){
                String year=new SimpleDateFormat("yyyy").format(moneyAvg.getCreateDate());
                String month=new SimpleDateFormat("MM").format(moneyAvg.getCreateDate());
                rMap.put("year",year);

            }else{
                rMap.put("year","");

            }
            rMap.put("allDepartMoney",moneyAvg.getAllDepartMoney()!=null?moneyAvg.getAllDepartMoney():"");
            rMap.put("allPartyHuman",moneyAvg.getAllPartyHuman()!=null?moneyAvg.getAllPartyHuman():"");
            Double allDepartAvgMoney=new Double(0.00);
            if(moneyAvg.getMoneyAvgDetailList()!=null){
                List<MoneyAvgDetail> moneyAvgDetailList=moneyAvg.getMoneyAvgDetailList();
                for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
                        allDepartAvgMoney+=moneyAvgDetail.getDepartAvgMoney();
                }
                Long l=Math.round(allDepartAvgMoney*100);
                Double ret=l/100.0;
                rMap.put("allDepartAvgMoney",ret);
            }else{
                rMap.put("allDepartAvgMoney",0);
            }
            rMap.put("state",moneyAvg.getProcessState().value()!=null?moneyAvg.getProcessState().value():"");
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String listDepart(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        loginUser = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);

        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        String avgYear=new SimpleDateFormat("yyyy").format(new Date());
        /*params.put("avgYear",avgYear);*/
        pager=moneyAvgService.findByPagerAndFinish("moneyavg",pager,params);

        List<MoneyAvg> moneyAvgList;
        if (pager.getTotalCount() > 0){
            moneyAvgList = (List<MoneyAvg>) pager.getList();
        }else{
            moneyAvgList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        if(moneyAvgList.size()>1){
            List<Date> createDateList=Lists.newArrayList();
            for(MoneyAvg moneyAvg:moneyAvgList){
               // List<MoneyAvgDetail> moneyAvgDetailList=moneyAvg.getMoneyAvgDetailList();
                createDateList.add(moneyAvg.getCreateDate());
            }
            MoneyAvg tempMoneyAvg=null;
            for(MoneyAvg moneyAvg:moneyAvgList){
                if(moneyAvg.getCreateDate()==Collections.min(createDateList)){
                    tempMoneyAvg=moneyAvg;
                }
            }
            List<MoneyAvgDetail> moneyAvgDetailList=tempMoneyAvg.getMoneyAvgDetailList();
            for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
                rMap = new HashMap<String,Object>();
                rMap.put("id",moneyAvgDetail.getId()!=null?moneyAvgDetail.getId():"");
                rMap.put("department",moneyAvgDetail.getDepartment()!=null?moneyAvgDetail.getDepartment().getName():"");
                rMap.put("departAllEnableMoney",moneyAvgDetail.getDepartAllEnableMoney()!=null?moneyAvgDetail.getDepartAllEnableMoney():0.00);
                rMap.put("departEveryUseDetail",moneyAvgDetail.getDepartEveryUseDetail()!=null?moneyAvgDetail.getDepartEveryUseDetail():0.00);
                rMap.put("departResidueEnableMoney",moneyAvgDetail.getDepartResidueEnableMoney()!=null?moneyAvgDetail.getDepartResidueEnableMoney():0.00);
                JSONObject o = JSONObject.fromObject(rMap);
                dataRows.add(o);
            }
        }else{
            for(MoneyAvg moneyAvg:moneyAvgList){
                List<MoneyAvgDetail> moneyAvgDetailList=moneyAvg.getMoneyAvgDetailList();
                for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
                    rMap = new HashMap<String,Object>();
                    rMap.put("id",moneyAvgDetail.getId()!=null?moneyAvgDetail.getId():"");
                    rMap.put("department",moneyAvgDetail.getDepartment()!=null?moneyAvgDetail.getDepartment().getName():"");
                    rMap.put("departAllEnableMoney",moneyAvgDetail.getDepartAllEnableMoney()!=null?moneyAvgDetail.getDepartAllEnableMoney():0.00);
                    rMap.put("departEveryUseDetail",moneyAvgDetail.getDepartEveryUseDetail()!=null?moneyAvgDetail.getDepartEveryUseDetail():0.00);
                    rMap.put("departResidueEnableMoney",moneyAvgDetail.getDepartResidueEnableMoney()!=null?moneyAvgDetail.getDepartResidueEnableMoney():0.00);
                    JSONObject o = JSONObject.fromObject(rMap);
                    dataRows.add(o);
                }
            }
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String read(){
        moneyAvgDetailList=Lists.newArrayList();
        if(StringUtils.isNotEmpty(keyId)) {
            loginUser=usersService.getLoginInfo();
            moneyAvg = moneyAvgService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            if(moneyAvg.getMoneyAvgDetailList()!=null){
                moneyAvgDetailList=moneyAvg.getMoneyAvgDetailList();
            }
        }
        return "read";
    }

    public String input(){
        moneyAvgDetailList=Lists.newArrayList();
        Company company=usersService.getLoginInfo().getCompany();
        if (StringUtils.isNotEmpty(keyId)){

            moneyAvg = moneyAvgService.get(keyId);
            loginUser=usersService.getLoginInfo();
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            if(moneyAvg.getMoneyAvgDetailList()!=null){
                moneyAvgDetailList=moneyAvg.getMoneyAvgDetailList();
            }


        } else {
            moneyAvg=new MoneyAvg();
            moneyAvgDetailList=Lists.newArrayList();
           //获取所有支部
            Users users=usersService.getLoginInfo();
            Map<String ,Object> params=new HashMap<String, Object>();
            params=getSearchFilterParams(_search,params,filters);
            params.put("company",company);
            params.put("state",BaseEnum.StateEnum.Enable);
            List<Department> departmentList=departmentService.getList(params);
            List<Department> parentDepartmentList=Lists.newArrayList();//所有一级部门
            List<Department> childDepartmentList=Lists.newArrayList();//所有二级部门
            List<Department> allDepartmentList=Lists.newArrayList();//所有部门
           if(departmentList!=null){
               for(Department department:departmentList){
                   if(department.getParent()==null){
                       parentDepartmentList.add(department);//获取所有一级部门
                   }else{
                       childDepartmentList.add(department);
                   }
                   allDepartmentList.add(department);
               }
           }
           //获取所有支部的党员

            Map<String ,Object> params1=new HashMap<String, Object>();
            params1=getSearchFilterParams(_search,params1,filters);
            params1.put("company",company);
            params1.put("state",BaseEnum.StateEnum.Enable);
            /*params1.put("dep",parentDepartmentList);*/
            pager=employeesService.findByPagerAndFinish("employees",pager,params1);
            List<Employees> employeesList;
            if(pager.getTotalCount()>0){
                employeesList=(List<Employees>)pager.getList();
            }else{
                employeesList=Lists.newArrayList();
            }
            List<Integer> numDepart=Lists.newArrayList();//将每个部门的人数
            int i;
            String nowYear=new SimpleDateFormat("yyyy").format(new Date());
            String nowMonth=new SimpleDateFormat("MM").format(new Date());//指定年份，指定月份，所有部门上缴的党费
            //BigDecimal bigDecimalYear=new BigDecimal(0);
            //BigDecimal bigDecimalMonth=new BigDecimal(0);

            for(Department parentDepartment:parentDepartmentList){
                i=0;
                for(Employees employees:employeesList){
                    if(employees.getDep().getParent()==null){
                        if(parentDepartment==employees.getDep()){
                            i++;
                        }
                    }else{
                        if(parentDepartment==employees.getDep().getParent()){
                            i++;
                        }
                    }
                }
                numDepart.add(i);
            }

            //指定年份，指定月份（每年每个部门每个月党员的缴纳人数相等），所有部门总计党员人数
            allPartyHuman=0;
            for(Integer integer:numDepart){
                allPartyHuman+=integer;
            }
            //指定年份，所有部门上缴的总党费

            Map<String ,Object> params2=new HashMap<String, Object>();
            params2=getSearchFilterParams(_search,params2,filters);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy");
            String year=sdf.format(new Date());
            params2.put("company",company);
            params2.put("state",BaseEnum.StateEnum.Enable);
            params2.put("registerYear",year);
            params2.put("processState",new FlowEnum.ProcessState[]{FlowEnum.ProcessState.Finished});
            List<MoneyManager> moneyManagerList=moneyManagerService.getList(params2);
            List<BigDecimal> everyDepartYearMonthAllMoney=Lists.newArrayList();//一年之内一个部门，每个月份的缴纳的总金额
            List<BigDecimal> everyDepartYearAllMoey=Lists.newArrayList();//一年之内一个部门缴纳的所有金额
            if(moneyManagerList.size()>0){
                loop1:for(Department department:parentDepartmentList){
                    loop2:for(MoneyManager moneyManager:moneyManagerList){
                            if(department==moneyManager.getDepartment()){
                                List<MoneyManagerDetail> moneyManagerDetailList=moneyManager.getMoneyManagerDetailList();
                                BigDecimal allMonthPay=new BigDecimal(0);
                                loop3:for(MoneyManagerDetail moneyManagerDetail:moneyManagerDetailList){
                                        allMonthPay=moneyManagerDetail.getMonthPayDues().add(allMonthPay);
                                }
                                everyDepartYearMonthAllMoney.add(allMonthPay);
                            }
                    }
                    BigDecimal b=new BigDecimal(0);
                    for(BigDecimal bigDecimal:everyDepartYearMonthAllMoney){
                        b=bigDecimal.add(b);
                    }
                    everyDepartYearMonthAllMoney.clear();
                    everyDepartYearAllMoey.add(b);
                }
            }


            allDepartMoney=new BigDecimal(0);
            for(BigDecimal bigDecimal:everyDepartYearAllMoey){
                allDepartMoney=bigDecimal.add(allDepartMoney);
            }


            int len=parentDepartmentList.size();
            for(int j=0;j<len;j++){
                MoneyAvgDetail moneyAvgDetail=new MoneyAvgDetail();
                moneyAvgDetail.setDepartment(parentDepartmentList.get(j));
                moneyAvgDetail.setDepartmentHuman(numDepart.get(j));
                if(allPartyHuman==0){
                    moneyAvgDetail.setDepartAvgMoney(0.0);
                }else{
                    Double avgMoney=allDepartMoney.doubleValue()*0.36*numDepart.get(j)/allPartyHuman;
                    Long l=Math.round(avgMoney*100);
                    Double money=l/100.0;
                    moneyAvgDetail.setDepartAvgMoney(money);
                }
                moneyAvgDetail.setCompany(company);
                moneyAvgDetail.setCreater(users);
                moneyAvgDetail.setState(BaseEnum.StateEnum.Enable);
                moneyAvgDetailList.add(moneyAvgDetail);

            }
            moneyAvg.setAllDepartMoney(allDepartMoney);
            moneyAvg.setAllPartyHuman(allPartyHuman);
        }

        return "input";
    }

    private void setData(){
        String[] remarkArray={""};
        loginUser=usersService.getLoginInfo();
        departmentList=Lists.newArrayList();
        departHumanList=Lists.newArrayList();
        departAvgMoneyList=Lists.newArrayList();
        moneyAvgDetailList=Lists.newArrayList();

        Company company=usersService.getLoginInfo().getCompany();
        if(StringUtils.isNotEmpty(keyId)){
            moneyAvg = moneyAvgService.get(keyId);
        }else{
            moneyAvg = new MoneyAvg();
            moneyAvg.setCreater(usersService.getLoginInfo());
        }
        //部门
        if(StringUtils.isNotEmpty(departId)){
            for(String s:departId.split(",")){
                departmentList.add(departmentService.get(s.trim()));
            }
        }
        //人数
        if(StringUtils.isNotEmpty(departmentHuman)){
            for(String s:departmentHuman.split(",")){
                departHumanList.add(Integer.parseInt(s.trim()));
            }
        }
        if(StringUtils.isNotEmpty(departAvgMoney)){
            for(String s:departAvgMoney.split(",")){
                departAvgMoneyList.add(Double.parseDouble(s.trim()));
            }
        }
        //分摊详情
        List<MoneyAvgDetail> moneyAvgDetailList1=Lists.newArrayList();
        if(StringUtils.isNotEmpty(moneyAvgDetailId)){
            for(String s:moneyAvgDetailId.split(",")){

                if(StringUtils.isNotEmpty(s.trim())){
                    moneyAvgDetailList1.add(moneyAvgDetailService.get(s.trim()));
                }
            }
        }
        String avgYear=new SimpleDateFormat("yyyy").format(new Date());
        int len=departmentList.size();
        if(moneyAvgDetailList1.size()>0){
            for(int j=0;j<len;j++){
                MoneyAvgDetail moneyAvgDetail=moneyAvgDetailList1.get(j);
                moneyAvgDetail.setDepartment(departmentList.get(j));
                moneyAvgDetail.setDepartmentHuman(departHumanList.get(j));
                Double avgMoney;
                if(allPartyHuman==0){
                    moneyAvgDetail.setDepartAvgMoney(0.0);
                }else{
                    avgMoney=allDepartMoney.doubleValue()*0.36*departHumanList.get(j)/allPartyHuman;
                    DecimalFormat df=new DecimalFormat("#.00");
                    df.format(avgMoney);
                    moneyAvgDetail.setDepartAvgMoney(avgMoney);
                }
                moneyAvgDetail.setAvgYear(avgYear);
                moneyAvgDetail.setCompany(company);
                moneyAvgDetail.setCreater(loginUser);
                moneyAvgDetail.setState(BaseEnum.StateEnum.Enable);
                moneyAvgDetailService.update(moneyAvgDetail);
                moneyAvgDetailList.add(moneyAvgDetail);
            }
        }else{
            for(int j=0;j<len;j++){
                MoneyAvgDetail moneyAvgDetail=new MoneyAvgDetail();
                moneyAvgDetail.setDepartment(departmentList.get(j));
                moneyAvgDetail.setDepartmentHuman(departHumanList.get(j));
                Double avgMoney;
                if(allPartyHuman==0){
                    moneyAvgDetail.setDepartAvgMoney(0.0);
                }else{
                    avgMoney=allDepartMoney.doubleValue()*0.36*departHumanList.get(j)/allPartyHuman;
                    DecimalFormat df=new DecimalFormat("#.00");
                    df.format(avgMoney);
                    moneyAvgDetail.setDepartAvgMoney(avgMoney);
                }
                moneyAvgDetail.setAvgYear(avgYear);
                moneyAvgDetail.setCompany(company);
                moneyAvgDetail.setCreater(loginUser);
                moneyAvgDetail.setState(BaseEnum.StateEnum.Enable);
                moneyAvgDetailService.save(moneyAvgDetail);
                moneyAvgDetailList.add(moneyAvgDetail);

            }
        }





        moneyAvg.setAllDepartMoney(allDepartMoney);
        moneyAvg.setAllPartyHuman(allPartyHuman);
        Double avgMoney=allDepartMoney.doubleValue()*0.36;
        DecimalFormat df=new DecimalFormat("#.00");
        df.format(avgMoney);
        moneyAvg.setAllPartyAvgMoney(avgMoney);
        moneyAvg.setMoneyAvgDetailList(moneyAvgDetailList);
        moneyAvg.setCompany(company);
        moneyAvg.setCreater(loginUser);
        moneyAvg.setState(BaseEnum.StateEnum.Enable);
        moneyAvgService.save(moneyAvg);
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                moneyAvgService.update(moneyAvg);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                moneyAvgService.save(moneyAvg, "moneyavg", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        setData();

        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        var2.put("initDuty", curDutyId);
        ArrayList<String> list = new ArrayList<String>();
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        String id="";
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                moneyAvgService.approve(moneyAvg, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
            } else {
                id=moneyAvgService.commit(moneyAvg, FlowEnum.ProcessState.Finished,"moneyavg",var1,var2,curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        //获取所有一级部门
        Users users=usersService.getLoginInfo();
        Company company=usersService.getLoginInfo().getCompany();
        Map<String ,Object> params=new HashMap<String, Object>();
        params=getSearchFilterParams(_search,params,filters);
        params.put("company",company);
        params.put("state",BaseEnum.StateEnum.Enable);
        List<Department> departmentList=departmentService.getList(params);
        List<Department> parentDepartmentList=Lists.newArrayList();//所有一级部门
        List<Department> childDepartmentList=Lists.newArrayList();//所有二级部门
        List<Department> allDepartmentList=Lists.newArrayList();//所有部门
        if(departmentList!=null){
            for(Department department:departmentList){
                if(department.getParent()==null){
                    parentDepartmentList.add(department);//获取所有一级部门
                }else{
                    childDepartmentList.add(department);
                }
                allDepartmentList.add(department);
            }
        }
        //获取所有的党费分摊
        Map<String ,Object> params1=new HashMap<String, Object>();
        params1=getSearchFilterParams(_search,params1,filters);
        params1.put("company",company);
        params1.put("state",BaseEnum.StateEnum.Enable);
        params1.put("processState",FlowEnum.ProcessState.Finished);
        List<MoneyAvg> moneyAvgList=moneyAvgService.getList(params1);
        List<Date> createDateList=Lists.newArrayList();
        if(moneyAvgList.size()>1){
            for(MoneyAvg moneyAvg:moneyAvgList){
                createDateList.add(moneyAvg.getCreateDate());
            }
            Date tempDate=Collections.min(createDateList);
            MoneyAvg tempMoneyAvg=null;
            for(MoneyAvg moneyAvg:moneyAvgList){
                if(tempDate==moneyAvg.getCreateDate()){
                    tempMoneyAvg=moneyAvg;
                }
            }
            if(StringUtils.isNotEmpty(keyId)){
                MoneyAvg moneyAvg=moneyAvgService.get(keyId);
                List<MoneyAvgDetail> moneyAvgDetailList=moneyAvg.getMoneyAvgDetailList();
                List<MoneyAvgDetail> tempMoneyAvgDetailList=tempMoneyAvg.getMoneyAvgDetailList();
                loop1:for(MoneyAvgDetail tempMoneyAvgDetail:tempMoneyAvgDetailList){
                    loop2:for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
                        if(tempMoneyAvgDetail.getDepartment()==moneyAvgDetail.getDepartment()){
                            //Double tempDepartAvgMoney=tempMoneyAvgDetail.getDepartAvgMoney()!=null?tempMoneyAvgDetail.getDepartAvgMoney():0.00;//分摊的金额
                            Double tempDepartEnableAllMoney=tempMoneyAvgDetail.getDepartAllEnableMoney()!=null?tempMoneyAvgDetail.getDepartAllEnableMoney():0.00;//累计可支配金额
                            Double tempDepartResidueEnableMoney=tempMoneyAvgDetail.getDepartResidueEnableMoney()!=null?tempMoneyAvgDetail.getDepartResidueEnableMoney():0.00;//剩余可支配金额

                            Double departAvgMoney=moneyAvgDetail.getDepartAvgMoney()!=null?moneyAvgDetail.getDepartAvgMoney():0.00;//分摊的金额
                            tempMoneyAvgDetail.setDepartAllEnableMoney(tempDepartEnableAllMoney+departAvgMoney);
                            tempMoneyAvgDetail.setDepartResidueEnableMoney(tempDepartResidueEnableMoney+departAvgMoney);
                            moneyAvgDetailService.update(tempMoneyAvgDetail);
                            continue loop1;

                        }
                    }
                }
            }else{
                if(StringUtils.isNotEmpty(id)){
                    MoneyAvg moneyAvg=moneyAvgService.get(id);
                    List<MoneyAvgDetail> moneyAvgDetailList=moneyAvg.getMoneyAvgDetailList();
                    List<MoneyAvgDetail> tempMoneyAvgDetailList=tempMoneyAvg.getMoneyAvgDetailList();
                    loop1:for(MoneyAvgDetail tempMoneyAvgDetail:tempMoneyAvgDetailList){
                        loop2:for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
                            if(tempMoneyAvgDetail.getDepartment()==moneyAvgDetail.getDepartment()){
                                Double tempDepartEnableAllMoney=tempMoneyAvgDetail.getDepartAllEnableMoney()!=null?tempMoneyAvgDetail.getDepartAllEnableMoney():0.00;//累计可支配金额
                                Double tempDepartResidueEnableMoney=tempMoneyAvgDetail.getDepartResidueEnableMoney()!=null?tempMoneyAvgDetail.getDepartResidueEnableMoney():0.00;//剩余可支配金额

                                Double departAvgMoney=moneyAvgDetail.getDepartAvgMoney()!=null?moneyAvgDetail.getDepartAvgMoney():0.00;//分摊的金额
                                tempMoneyAvgDetail.setDepartAllEnableMoney(tempDepartEnableAllMoney+departAvgMoney);
                                tempMoneyAvgDetail.setDepartResidueEnableMoney(tempDepartResidueEnableMoney+departAvgMoney);
                                moneyAvgDetailService.update(tempMoneyAvgDetail);
                                continue loop1;
                            }
                        }
                    }
                }
            }
        }else{
            if(StringUtils.isNotEmpty(keyId)){
                MoneyAvg moneyAvg=moneyAvgService.get(keyId);
                List<MoneyAvgDetail> moneyAvgDetailList=moneyAvg.getMoneyAvgDetailList();
                loop1:for(Department department:parentDepartmentList){
                    loop2:for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
                        if(department==moneyAvgDetail.getDepartment()){
                            Double departAvgMoney=moneyAvgDetail.getDepartAvgMoney()!=null?moneyAvgDetail.getDepartAvgMoney():0.00;//分摊的金额
                            Double departEnableAllMoney=moneyAvgDetail.getDepartAllEnableMoney()!=null?moneyAvgDetail.getDepartAllEnableMoney():0.00;//累计可支配金额
                            Double departResidueEnableMoney=moneyAvgDetail.getDepartResidueEnableMoney()!=null?moneyAvgDetail.getDepartResidueEnableMoney():0.00;//剩余可支配金额
                            moneyAvgDetail.setDepartAllEnableMoney(departAvgMoney+departEnableAllMoney);
                            moneyAvgDetail.setDepartResidueEnableMoney(departAvgMoney+departResidueEnableMoney);
                            moneyAvgDetailService.update(moneyAvgDetail);
                            continue loop1;
                        }
                    }
                }
            }else{
                if(StringUtils.isNotEmpty(id)){
                    MoneyAvg moneyAvg=moneyAvgService.get(id);
                    List<MoneyAvgDetail> moneyAvgDetailList=moneyAvg.getMoneyAvgDetailList();
                    loop1:for(Department department:parentDepartmentList){
                        loop2:for(MoneyAvgDetail moneyAvgDetail:moneyAvgDetailList){
                            if(department==moneyAvgDetail.getDepartment()){
                                Double departAvgMoney=moneyAvgDetail.getDepartAvgMoney()!=null?moneyAvgDetail.getDepartAvgMoney():0.00;//分摊的金额
                                Double departEnableAllMoney=moneyAvgDetail.getDepartAllEnableMoney()!=null?moneyAvgDetail.getDepartAllEnableMoney():0.00;//累计可支配金额
                                Double departResidueEnableMoney=moneyAvgDetail.getDepartResidueEnableMoney()!=null?moneyAvgDetail.getDepartResidueEnableMoney():0.00;//剩余可支配金额
                                moneyAvgDetail.setDepartAllEnableMoney(departAvgMoney+departEnableAllMoney);
                                moneyAvgDetail.setDepartResidueEnableMoney(departAvgMoney+departResidueEnableMoney);
                                moneyAvgDetailService.update(moneyAvgDetail);
                                continue loop1;
                            }
                        }
                    }
                }
            }
        }


        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批
    public String approve1(){
        moneyAvg = moneyAvgService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();

        var1.put("numStatus", 2);
        //  var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                moneyAvgService.approve(moneyAvg, FlowEnum.ProcessState.Finished, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }
    // 会签
    public String approve2() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";

        moneyAvg = moneyAvgService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        //  var2.put("curDutyId", curDutyId);
        try {
            moneyAvgService.approve(moneyAvg, FlowEnum.ProcessState.Finished, var2, curDutyId, comment);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String approve3() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        moneyAvg = moneyAvgService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 4);
        //  var2.put("curDutyId", curDutyId);
        try {
            moneyAvgService.approve(moneyAvg, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            moneyAvg = moneyAvgService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("moneyavg");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            System.out.println("comment:"+comment);
            LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            moneyAvgService.reject(moneyAvg, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            moneyAvg = moneyAvgService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("moneyavg");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:"+key);
            if(com.joint.base.util.StringUtils.isEmpty(comment)){
                comment="";
            }
            moneyAvgService.deny(moneyAvg, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }



    public MoneyAvg getMoneyAvg() {
        return moneyAvg;
    }

    public void setMoneyAvg(MoneyAvg moneyAvg) {
        this.moneyAvg = moneyAvg;
    }

    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }


    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }


    public String getViewtype() {


        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public List<Map<String, Object>> getPostArrayList() {
        return postArrayList;
    }

    public void setPostArrayList(List<Map<String, Object>> postArrayList) {
        this.postArrayList = postArrayList;
    }

    public String getMoneyAvgDepartId() {
        return moneyAvgDepartId;
    }

    public void setMoneyAvgDepartId(String moneyAvgDepartId) {
        this.moneyAvgDepartId = moneyAvgDepartId;
    }

    public BigDecimal getAllDepartMoney() {
        return allDepartMoney;
    }

    public void setAllDepartMoney(BigDecimal allDepartMoney) {
        this.allDepartMoney = allDepartMoney;
    }

    public Integer getAllPartyHuman() {
        return allPartyHuman;
    }

    public void setAllPartyHuman(Integer allPartyHuman) {
        this.allPartyHuman = allPartyHuman;
    }

    public List<MoneyAvgDetail> getMoneyAvgDetailList() {
        return moneyAvgDetailList;
    }

    public void setMoneyAvgDetailList(List<MoneyAvgDetail> moneyAvgDetailList) {
        this.moneyAvgDetailList = moneyAvgDetailList;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Double getAllPartyAvgMoney() {
        return allPartyAvgMoney;
    }

    public void setAllPartyAvgMoney(Double allPartyAvgMoney) {
        this.allPartyAvgMoney = allPartyAvgMoney;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    public String getDepartmentHuman() {
        return departmentHuman;
    }

    public void setDepartmentHuman(String departmentHuman) {
        this.departmentHuman = departmentHuman;
    }

    public List<Integer> getDepartHumanList() {
        return departHumanList;
    }

    public void setDepartHumanList(List<Integer> departHumanList) {
        this.departHumanList = departHumanList;
    }

    public String getDepartAvgMoney() {
        return departAvgMoney;
    }

    public void setDepartAvgMoney(String departAvgMoney) {
        this.departAvgMoney = departAvgMoney;
    }

    public List<Double> getDepartAvgMoneyList() {
        return departAvgMoneyList;
    }

    public void setDepartAvgMoneyList(List<Double> departAvgMoneyList) {
        this.departAvgMoneyList = departAvgMoneyList;
    }

    public String getMoneyAvgDetailId() {
        return moneyAvgDetailId;
    }

    public void setMoneyAvgDetailId(String moneyAvgDetailId) {
        this.moneyAvgDetailId = moneyAvgDetailId;
    }
}


