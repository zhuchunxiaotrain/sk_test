package com.joint.web.action.com;


import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.base.util.PinyinUtil;
import com.google.common.collect.Lists;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.service.*;
import com.joint.base.util.DataUtil;
import com.joint.web.action.BaseAdminAction;
import net.sf.json.JSONObject;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by hpj on 2014/9/16.
 */
@ParentPackage("com")
public class AjaxPostAction extends BaseAdminAction {

    @Resource
    private UsersService usersService;
    @Resource
    private PostService postService;
    @Resource
    private DutyService dutyService;
    @Resource
    private PowerService powerService;
    @Resource
    private DepartmentService departmentService;

    protected Pager pager;
    protected Users user;
    protected Post post;
    protected List<Post> postList;

    private String no;
    private String name;
    private String description;
    private String pname;
    private String depId;
    private String transferDepartId;

    /**
     * 返回岗位视图
     * @return
     */
    public String list(){
        Company company=usersService.getCompanyByUser();
        pager = new Pager();
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.asc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Map<String,Object> params=new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("state",BaseEnum.StateEnum.Enable);
        pager = postService.findByPagerAndCompany(pager, null,company, params);
        postList = (List<Post>) pager.getList();
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(Post post1:postList){
            rMap=new HashMap<String, Object>();
            rMap.put("id",post1.getId());
            rMap.put("name",post1.getName());
            rMap.put("description",post1.getDescription());
            rMap.put("createDate", DataUtil.DateTimeToString(post1.getCreateDate()));
            dataRows.add(JSONObject.fromObject(rMap));
        }
        long total =pager.getTotalCount();
        if(total%rows==0){
            total = total/rows;
        }else{
            if(total<rows){
                total = 1;
            }else{
                total = total/rows + 1;
            }
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        data.put("total",total);
        data.put("records", pager.getTotalCount());
        return  ajaxJson(JSONObject.fromObject(data).toString());

    }
    /**
     * 根据部门 返回岗位视图
     * @return
     */
    public String postList(){
        pager = new Pager();
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.asc);
        Map<String,Object> params=new HashMap<>();
        Company com=usersService.getCompanyByUser();
        params.put("state", new BaseEnum.StateEnum[]{BaseEnum.StateEnum.Enable});
        List<String> idList= Lists.newArrayList();

        if(StringUtils.isNotEmpty(depId)){
            Department department = departmentService.get(depId);
            Set<Power> powerSet = department.getPowerSet();//得到部门的部门的职权集合
            for (Power power:powerSet){
                String id = power.getPost().getId();
                idList.add(id);

            }
            params.put("id",idList);

        }
        pager = postService.findByPagerAndCompany(pager,user,com,params);
        postList = (List<Post>) pager.getList();
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(Post post1:postList){
            rMap=new HashMap<String, Object>();
            rMap.put("id",post1.getId());
            rMap.put("name",post1.getName());
            rMap.put("description",post1.getDescription());
            rMap.put("createDate", DataUtil.DateTimeToString(post1.getCreateDate()));
            dataRows.add(JSONObject.fromObject(rMap));
        }
        long total =pager.getTotalCount();
        if(total%rows==0){
            total = total/rows;
        }else{
            if(total<rows){
                total = 1;
            }else{
                total = total/rows + 1;
            }
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        data.put("total",total);
        data.put("records", pager.getTotalCount());
        return  ajaxJson(JSONObject.fromObject(data).toString());

    }

    /**
     * 返回编辑岗位视图
     * @return
     */
    public String read(){
        if(StringUtils.isNotEmpty(keyId)){
            post = postService.get(keyId);
        }

        return "read";
    }

    /**
     * 部门唯一性校验
     * @return
     */
    public void isUnique(){
        PrintWriter out= null;
        try {
            out=getResponse().getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("keyId:" + keyId);
        if(postService.isUnique(validatePro,keyId,newValue)){
            out.print("true");
        }else{
            out.print("false");
        }

    }
    /**
     * 添加岗位
     * @return
     */
    public String save(){
        Post post;
        if(StringUtils.isNotEmpty(keyId)){
            post = postService.get(keyId);
        }else {
            post = new Post();
        }
        post.setBeSystem(0);
        post.setDescription(description);
        post.setName(name);
        post.setValue(PinyinUtil.getPinYinHeadChar(name));
        post.setState(BaseEnum.StateEnum.Enable);
        post.setPname(pname);
        post.setNo(no);
        post.setCompany(usersService.getCompanyByUser());
        if(StringUtils.isNotEmpty(keyId)){
            postService.update(post);
        }else {
            postService.save(post);
        }

        return ajaxHtmlCallback("200", "保存成功！","操作状态");
    }

    /**
     * 删除岗位
     * @return
     */
    public String delete(){
        if(StringUtils.isEmpty(keyId)){
            return ajaxHtmlCallback("200", "请选择岗位！","操作状态");
        }
        Post post = postService.get(keyId);
        List<Power> powerList =  powerService.getByPostId(keyId);
        if(powerList.size()>0){
            return  ajaxHtmlCallback("400","请先删除关联的职权","操作状态");
        }
        post.setState(BaseEnum.StateEnum.Delete);
        postService.update(post);
        return ajaxHtmlCallback("200", "保存成功！","操作状态");
    }
    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public List<Post> getPostList() {
        return postList;
    }

    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getDepId() {
        return depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }


}
