package com.joint.web.action.com;


import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Permission;
import com.joint.base.entity.Users;
import com.joint.base.service.FileManageService;
import com.joint.base.service.PermissionService;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.Notice;
import com.joint.core.service.NoticeService;
import com.joint.web.action.BaseAdminAction;
import net.sf.json.JSONObject;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.*;


@ParentPackage("com")
public class AjaxCalendarAction extends BaseAdminAction {


    private static final long serialVersionUID = -6732809121268618245L;
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private UsersService usersService;
    @Resource
    private TaskService taskService;
    @Resource
    private FileManageService fileManageService;
    @Resource
    private PermissionService permissionService;
    @Resource
    private NoticeService noticeService;

    private Users user;
    private String password;
    private String username;
    private String _t;
    private String filePath;
    private String ajax;

    private List<Permission> permissionList;

    private Date today;

    /**
     * 主页
     * @return
     */
    public String page(){
        today = new Date();
        LogUtil.info("today:" + today);
        return "page";
    }


        public String getAjax() {
        return ajax;
    }

    public void setAjax(String ajax) {
        this.ajax = ajax;
    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }
}
