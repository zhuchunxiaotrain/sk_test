package com.joint.web.action.com;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Users;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.Employees;
import com.joint.core.service.EmployeesService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 16/12/16.
 */
@ParentPackage("com")
public class AjaxBirthdayAction extends BaseFlowAction {

    @Resource
    private EmployeesService employeesService;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "birthday";
    }

    /**
     * 生日一览
     * @return
     */
    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(org.apache.commons.lang.StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("nature","0");
        params.put("ifJob","0");

        //params.put("type",0);
        //已归档
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
        pager=employeesService.findByPagerAndLimit(true, "employees", pager, params);

        List<Employees> eList;
        if (pager.getTotalCount() > 0){
            eList = (List<Employees>) pager.getList();
        }else{
            eList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(Employees e:eList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",e.getId());
            rMap.put("name",e.getName());
            rMap.put("staffid",e.getStaffid());
            rMap.put("mon", DataUtil.DateToString(e.getBirthDate(),"MM"));
            rMap.put("age",e.getAge());
            rMap.put("sex",e.getGender());
            rMap.put("post",e.getEmpPost()==null?"":e.getEmpPost().getName());
            rMap.put("dep",e.getDep()==null?"":e.getDep().getName());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        LogUtil.info("dataRows"+dataRows);
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


}
