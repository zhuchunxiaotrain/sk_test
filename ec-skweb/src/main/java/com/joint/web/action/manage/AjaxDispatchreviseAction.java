package com.joint.web.action.manage;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.*;
import com.joint.base.service.activiti.WorkflowService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.manage.Dispatch;
import com.joint.core.entity.manage.DispatchRevise;
import com.joint.core.service.DispatchReviseService;
import com.joint.core.service.DispatchService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.*;


@ParentPackage("manage")
public class AjaxDispatchreviseAction extends BaseFlowAction {
    @Resource
    private WorkflowService workflowService;
    @Resource
    private DispatchService dispatchService;
    @Resource
    private DispatchReviseService dispatchreviseService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private PostService postService;
    @Resource
    private PowerService powerService;
    @Resource
    private DutyService dutyService;
    @Resource
    private UsersService usersService;
    @Resource
    private ReadersService readersService;
    @Resource
    private TaskRecordService taskRecordService;

    /**
     * 发文申请表(读)
     */
    private Dispatch dispatch;
    /**
     * 发文申请附件修订对象(读)
     */
    private DispatchRevise dispatchrevise;
    /**
     * 登录人(读)
     */
    private Users loginUser;
    /**
     * 原附件(读)
     */
    private String oldfileId;
    /**
     * 修订附件上传Id(读)
     */
    private String filereviseId;

    /**
     * 中心审核对象(读)
     */
    private List<Map<String,Object>> examObject;
    /**
     * 修订原因
     */
    private String reason;
    /**
     * 是否具有有效期
     */
    private String hasValid;
    /**
     * 有效期
     */
    private String validDate;
    /**
     * 发文对象
     */
    private String object;
    /**
     * 通知部门
     */
    private String noticeDepartmentId;
    /**
     * 通知个人
     */
    private String noticeUsersId;

    /**
     * 修订附件上传Id
     */
    private String oldfileIds;
    /**
     * 修订附件上传Id
     */
    private String filereviseIds;

    /**
     * 职责列表
     */
    private List<Duty> dutyList;

    /**
     * 是否需要会签
     */
    private String mulExam;

    /**
     * 会签人员id
     */
    private String examUsersId;

    /**
     * 是否需要中心领导审核
     */
    private String leaderExam;

    /**
     * 中心审核对象Id
     */
    private String examObjectId;


    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "dispatchrevise";
    }



    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");

        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        //内嵌视图这里要加上
        if (StringUtils.isNotEmpty(parentId)){
            dispatch = dispatchService.get(parentId);
            params.put("dispatch",dispatch);
        }
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);

        //所有单子
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
        pager=dispatchreviseService.findByPagerAndLimit(true, "dispatchrevise", pager, params);

        List<DispatchRevise>  dispatchRevisesList;
        if (pager.getTotalCount() > 0){
            dispatchRevisesList = (List<DispatchRevise>) pager.getList();
        }else{
            dispatchRevisesList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(DispatchRevise dispatchRevise: dispatchRevisesList){
            rMap = new HashMap<String, Object>();
            rMap.put("id",dispatchRevise.getId());
            rMap.put("creater", dispatchRevise.getCreater() != null ? dispatchRevise.getCreater().getName() : "");
            rMap.put("createDate", DataUtil.DateToString(dispatchRevise.getCreateDate(),"yyyy-MM-dd"));
            rMap.put("state",dispatchRevise.getProcessState().value());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    public String read(){
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)) {
            filereviseId ="";
            oldfileId ="";
            dispatchrevise = dispatchreviseService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            flowNumStatus = workflowService.getFlowNumStatus(keyId);
            if(dispatchrevise.getFile() != null && dispatchrevise.getFile().size()>0){
                for(FileManage f:dispatchrevise.getFile()){
                    oldfileId+=f.getId()+",";
                }
            }
            if(dispatchrevise.getFileRevise() != null && dispatchrevise.getFileRevise().size()>0){
                for(FileManage f:dispatchrevise.getFileRevise()){
                    filereviseId+=f.getId()+",";
                }
            }
        }
        examObject = Lists.newArrayList();
        Department department = null;
        List<Department> departmentList = departmentService.getDepartments("产业中心");
        if(departmentList.size()>0){
            department = departmentList.get(0);
        }
        Post post = postService.getPostByName("书记");
        Post post2 = postService.getPostByName("主任");
        List<Post> postList = Lists.newArrayList();
        postList.add(post);
        postList.add(post2);
        Map<String,Object> rmap;
        if(department != null ){
            for(Post post1:postList){
                if(post1 != null){
                    rmap= Maps.newHashMap();
                    Power power = powerService.getPowerByDepartAndPost(department, post1);
                    rmap.put("id", power.getId());
                    rmap.put("name", power.getName());
                    rmap.put("checked","");
                    for(Power power1:dispatchrevise.getExamObject()){
                        if(StringUtils.equals(power1.getId(),power.getId())){
                            rmap.put("checked","checked");
                        }
                    }
                    examObject.add(rmap);
                }
            }

        }
        return "read";
    }


    public String input(){
      //  Company company = usersService.getCompanyByUser();
      //  Map<String,Object> rMap = null;
        if (StringUtils.isNotEmpty(parentId)){
            oldfileId ="";
            dispatch = dispatchService.get(parentId);
            if(dispatch != null  && dispatch.getFile() != null && dispatch.getFile().size()>0){
                for(FileManage f:dispatch.getFile()){
                    oldfileId+=f.getId()+",";
                }
            }
            dispatchrevise = new DispatchRevise();
            dispatchrevise.setDispatch(dispatch);
            dispatchrevise.setProcessState(FlowEnum.ProcessState.Draft);
        }
        if (StringUtils.isNotEmpty(keyId)){
            filereviseId ="";
            dispatchrevise = dispatchreviseService.get(keyId);
            if(dispatchrevise.getFile() != null && dispatchrevise.getFile().size()>0){
                for(FileManage f:dispatchrevise.getFile()){
                    filereviseId+=f.getId()+",";
                }
            }

        } else {


        }

        return "input";
    }

    private void setData(){
        if(StringUtils.isNotEmpty(keyId)){
            dispatchrevise = dispatchreviseService.get(keyId);
        }else{
            dispatchrevise = new DispatchRevise();
            dispatchrevise.setCreater(usersService.getLoginInfo());
        }
        if(StringUtils.isNotEmpty(parentId)){
            dispatch = dispatchService.get(parentId);
            dispatchrevise.setDispatch(dispatch);
        }
        dispatchrevise.setReason(reason);
        dispatchrevise.setHasValid((hasValid != null && hasValid.equals("1")) ? true : false);
        dispatchrevise.setValidDate(DataUtil.StringToDate(validDate));
        dispatchrevise.setObject(object);
        List<Department> departmentList = Lists.newArrayList();
        if(com.joint.base.util.StringUtils.isNotEmpty(noticeDepartmentId)){
            String[] idArr = noticeDepartmentId.split(",");
            for(String id:idArr){
                departmentList.add(departmentService.get(id.trim()));
            }
        }
        List<Users> usersList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(noticeUsersId)){
            String[] idArr = noticeUsersId.split(",");
            for(String id:idArr){
                usersList.add(usersService.get(id.trim()));
            }
        }
        List<FileManage> fileManageList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(filereviseIds)){
            for(String f:filereviseIds.split(",")){
                fileManageList.add(fileManageService.get(f.trim()));
            }
        }
        List<FileManage> fileManageList2 = Lists.newArrayList();
        if(StringUtils.isNotEmpty(oldfileIds)){
            for(String f:oldfileIds.split(",")){
                fileManageList2.add(fileManageService.get(f.trim()));
            }
        }
        dispatchrevise.setNoticeDepartment(departmentList);
        dispatchrevise.setNoticeUsers(usersList);
        dispatchrevise.setFile(fileManageList2);
        dispatchrevise.setFileRevise(fileManageList);
        dispatchrevise.setCompany(usersService.getLoginInfo().getCompany());
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                dispatchreviseService.update(dispatchrevise);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                dispatchreviseService.save(dispatchrevise, "dispatchrevise", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }


    // 提交
    public String commit(){
        setData();
        Company company = usersService.getCompanyByUser();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        Set<Users> usersSet = Sets.newHashSet();
        Map<String, Object> map = new HashMap<String, Object>();
        List<Department> listDept = dispatchrevise.getNoticeDepartment();
        // LogUtil.info("do:"+object);
        if(StringUtils.equals(object,"1") == true){
            map.put("state", BaseEnum.StateEnum.Enable);
            List<Users> usersList = (List<Users>) usersService.findByPagerAndCompany(new Pager(0),null,company,map).getList();
            LogUtil.info("usersList:"+usersList.size());
            for(Users users:usersList){
                usersSet.add(users);
            }
        }else if(StringUtils.equals(object,"2") == true){
            for(Department department:listDept){
                dutyList = dutyService.getDutys(department);
                for(Duty duty:dutyList){
                    usersSet.add(duty.getUsers());
                }
            }
        }else if(StringUtils.equals(object,"3") == true){
            List<Users> usersList = dispatchrevise.getNoticeUsers();
            for(Users users:usersList){
                usersSet.add(users);
            }
        }


        try {
            if (StringUtils.isNotEmpty(keyId)) {
                dispatchreviseService.approve(dispatchrevise, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            } else {
                keyId = dispatchreviseService.commit(dispatchrevise, "dispatchrevise", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        //先删除读者
        readersService.deleteByKeyIdBussinessKey(keyId);
        //增加归档后读者
        for(Users users:usersSet){
            Readers readers = new Readers();
            readers.setUsers(users);
            readers.setBussinessKey("dispatchrevise");
            readers.setKeyId(keyId);
            readers.setType(2);
            readersService.save(readers);
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批1
    public String approve1(){
        dispatchrevise = dispatchreviseService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();

        var1.put("numStatus", 2);
        var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                dispatchreviseService.approve(dispatchrevise, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批2
    public String approve2(){
        dispatchrevise = dispatchreviseService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        LogUtil.info("mulExam:"+mulExam);
        var1.put("numStatus", 3);
        var1.put("curDutyId", curDutyId);
        var1.put("mulExam", mulExam);
        var1.put("leaderExam", leaderExam);
        List<Users> usersList = Lists.newArrayList();
        List<String> stringList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(examUsersId)){
            String[] idArr = examUsersId.split(",");
            for(String id:idArr){
                usersList.add(usersService.get(id.trim()));
                stringList.add(id.trim());
            }
        }
       // LogUtil.info("usersList:"+usersList.size());
        var1.put("approvers", stringList);
        List<String> usersList2 = Lists.newArrayList();
        List<Power> powerList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(examObjectId)){
            String[] idArr =examObjectId.split(",");
            for(String id:idArr){
               Power power = powerService.get(id.trim());
               powerList.add(power);
               List<Duty> dutyList = dutyService.findByPower(power);
               for(Duty duty:dutyList) {
                   usersList2.add(duty.getUsers().getId());
               }
            }
        }
       // LogUtil.info("usersList2:"+usersList2.size());
        var1.put("approvers2", usersList2);
        dispatchrevise.setMulExam(StringUtils.equals(mulExam, "1") ? true : false);
        dispatchrevise.setLeaderExam(StringUtils.equals(leaderExam, "1") ? true : false);
        dispatchrevise.setExamUsers(usersList);
        dispatchrevise.setExamObject(powerList);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if(StringUtils.equals(mulExam,"1") == false && StringUtils.equals(leaderExam, "1") == false){
                dispatchreviseService.approve(dispatchrevise, FlowEnum.ProcessState.Finished, var1, curDutyId, comment);
            }else{
                dispatchreviseService.approve(dispatchrevise, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
			}
           
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        /*
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }*/
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批3
    public String approve3(){
        dispatchrevise = dispatchreviseService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                dispatchreviseService.approve(dispatchrevise, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            //LogUtil.info("overtask:"+overtask);
            //会签后没有流程将对象状态设为归档
            if(overtask==null){
                DispatchRevise dispatchRevise = dispatchreviseService.get(keyId);
                dispatchRevise.setProcessState(FlowEnum.ProcessState.Finished);
                dispatchreviseService.update(dispatchRevise);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(dispatchRevise.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 4);
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }


    //审批4
    public String approve4(){
        dispatchrevise = dispatchreviseService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                dispatchreviseService.approve(dispatchrevise, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            //LogUtil.info("overtask:"+overtask);
            //会签后没有流程将对象状态设为归档
            if(overtask==null){
                DispatchRevise dispatchRevise = dispatchreviseService.get(keyId);
                dispatchRevise.setProcessState(FlowEnum.ProcessState.Finished);
                dispatchreviseService.update(dispatchRevise);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(dispatchRevise.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
                if(dispatchRevise.getDispatch() != null){
                    LogUtil.info("do update");
                    dispatch = dispatchRevise.getDispatch();
                    dispatch.setHasValid(dispatchRevise.isHasValid());
                    dispatch.setValidDate(dispatchRevise.getValidDate());
                    dispatch.setObject(dispatchRevise.getObject());
                    dispatch.setNoticeUsers(dispatchRevise.getNoticeUsers());
                    dispatch.setNoticeDepartment(dispatchRevise.getNoticeDepartment());
                    dispatch.setFile(dispatchRevise.getFileRevise());
                    dispatchService.update(dispatch);
                    Set<Users> usersSet = Sets.newHashSet();
                    Map<String, Object> map = new HashMap<String, Object>();
                    List<Department> listDept = dispatch.getNoticeDepartment();

                    if(StringUtils.equals(dispatch.getObject(),"1") == true){
                        map.put("state", BaseEnum.StateEnum.Enable);
                        List<Users> usersList = (List<Users>) usersService.findByPagerAndCompany(new Pager(0),null,usersService.getCompanyByUser(),map).getList();
                        for(Users users:usersList){
                            usersSet.add(users);
                        }
                    }else if(StringUtils.equals(dispatch.getObject(),"2") == true){
                        for(Department department:listDept){
                            dutyList = dutyService.getDutys(department);
                            for(Duty duty:dutyList){
                                usersSet.add(duty.getUsers());
                            }
                        }
                    }else if(StringUtils.equals(dispatch.getObject(),"3") == true){
                        List<Users> usersList = dispatchrevise.getNoticeUsers();
                        for(Users users:usersList){
                            usersSet.add(users);
                        }
                    }
                    //先删除读者
                    readersService.deleteByKeyIdBussinessKey(dispatch.getId());
                    //增加归档后读者
                    for(Users users:usersSet){
                        Readers readers = new Readers();
                        readers.setUsers(users);
                        readers.setBussinessKey("dispatch");
                        readers.setKeyId(dispatch.getId());
                        readers.setType(2);
                        readersService.save(readers);
                    }
                }

            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 5);
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (StringUtils.isNotEmpty(keyId)) {
            dispatchrevise = dispatchreviseService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("dispatch");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
           // System.out.println("comment:"+comment);
             LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            dispatchreviseService.reject(dispatchrevise, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (StringUtils.isNotEmpty(keyId)) {
            dispatchrevise = dispatchreviseService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("dispatch");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:" + key);
            if(StringUtils.isEmpty(comment)){
                comment="";
            }
            dispatchreviseService.deny(dispatchrevise, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }


    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getFilereviseId() {
        return filereviseId;
    }

    public void setFilereviseId(String filereviseId) {
        this.filereviseId = filereviseId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getHasValid() {
        return hasValid;
    }

    public void setHasValid(String hasValid) {
        this.hasValid = hasValid;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getNoticeDepartmentId() {
        return noticeDepartmentId;
    }

    public void setNoticeDepartmentId(String noticeDepartmentId) {
        this.noticeDepartmentId = noticeDepartmentId;
    }

    public String getNoticeUsersId() {
        return noticeUsersId;
    }

    public void setNoticeUsersId(String noticeUsersId) {
        this.noticeUsersId = noticeUsersId;
    }

    public String getFilereviseIds() {
        return filereviseIds;
    }

    public void setFilereviseIds(String filereviseIds) {
        this.filereviseIds = filereviseIds;
    }

    public List<Duty> getDutyList() {
        return dutyList;
    }

    public void setDutyList(List<Duty> dutyList) {
        this.dutyList = dutyList;
    }

    public String getMulExam() {
        return mulExam;
    }

    public void setMulExam(String mulExam) {
        this.mulExam = mulExam;
    }

    public String getExamUsersId() {
        return examUsersId;
    }

    public void setExamUsersId(String examUsersId) {
        this.examUsersId = examUsersId;
    }

    public String getLeaderExam() {
        return leaderExam;
    }

    public void setLeaderExam(String leaderExam) {
        this.leaderExam = leaderExam;
    }

    public String getExamObjectId() {
        return examObjectId;
    }

    public void setExamObjectId(String examObjectId) {
        this.examObjectId = examObjectId;
    }

    public Dispatch getDispatch() {
        return dispatch;
    }

    public void setDispatch(Dispatch dispatch) {
        this.dispatch = dispatch;
    }

    public List<Map<String, Object>> getExamObject() {
        return examObject;
    }

    public void setExamObject(List<Map<String, Object>> examObject) {
        this.examObject = examObject;
    }

    public DispatchRevise getDispatchrevise() {
        return dispatchrevise;
    }

    public void setDispatchrevise(DispatchRevise dispatchrevise) {
        this.dispatchrevise = dispatchrevise;
    }

    public String getOldfileId() {
        return oldfileId;
    }

    public void setOldfileId(String oldfileId) {
        this.oldfileId = oldfileId;
    }

    public String getOldfileIds() {
        return oldfileIds;
    }

    public void setOldfileIds(String oldfileIds) {
        this.oldfileIds = oldfileIds;
    }
}

