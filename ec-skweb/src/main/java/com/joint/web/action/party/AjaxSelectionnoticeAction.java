package com.joint.web.action.party;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.entity.Users;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.*;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.base.util.WorkflowUtils;
import com.joint.core.entity.*;
import com.joint.core.entity.SelectionNotice;
import com.joint.core.service.*;
import com.joint.core.service.SelectionNoticeService;
import com.joint.web.action.BaseFlowAction;
import com.opensymphony.xwork2.ActionContext;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.security.access.method.P;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
@ParentPackage("party")
public class AjaxSelectionnoticeAction extends BaseFlowAction {
    @Resource
    private SelectionNoticeService  selectionNoticeService;
    @Resource
    private SelectionPublicityService selectionPublicityService;
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private RoleService roleService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private TaskRecordService taskRecordService;

    private SelectionNotice selectionNotice;
    private SelectionPublicity selectionPublicity;
    private List<Map<String,Object>> postArrayList;

    private int numStatus;
    private Users loginUser;

    //前台传到后台的数据
    private String postCategoryId;
    private String makePlanId;
    private String roleId;

    //从后台传回前台的数据
    private String makePlanFileId;

    //申请人信息
    private String applyUserName;
    private String applyUserId;
    private String applyUserDate;
    /**
     * 岗位
     */
    private Dict post;
    /**
     * 要求
     */
    private String requires;
    /**
     * 报名周期
     */
    private Date selectDate;
    /**
     * 竞聘方案
     */
    private List<FileManage> makePlan;
    /**
     * 竞聘方案文本
     */
    private String makePlanText;
    /**
     * 审核小组
     */
    private Role role;

    /**
     * 备注
     */
    private String remark;

    private String viewtype;
    private String flag;

   /* private String todo;*/

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "selectionnotice";
    }


    public String listfordialog(){

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("processState",FlowEnum.ProcessState.Finished);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        pager=selectionNoticeService.findByPagerAndCompany(pager,null,com,params);
        /*pager=selectionNoticeService.findByPagerAndProcessState(pager, user, "selectionnotice", FlowEnum.ProcessState.Finished, params);*/
        List<SelectionNotice> selectionNoticeList;
        if (pager.getTotalCount() > 0){
            selectionNoticeList = (List<SelectionNotice>) pager.getList();
        }else{
            selectionNoticeList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(SelectionNotice selectionNotice:selectionNoticeList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",selectionNotice.getId()!=null?selectionNotice.getId():"");
            rMap.put("post",selectionNotice.getPost()!=null?selectionNotice.getPost().getName():"");
            rMap.put("creater",selectionNotice.getCreater()!=null?selectionNotice.getCreater().getName():"");
            rMap.put("selectDate",selectionNotice.getCreateDate()!=null?selectionNotice.getCreateDate().toString():"");
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }


        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        loginUser = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        if(flag.equals("0")){
            params.put("selectionFinished","0");
        }else if(flag.equals("1")){
            params.put("selectionFinished","1");
        }else{
            if(todo.equals("1")){
                params.put("selectionFinished","0");
            }
        }
        pager = selectionNoticeService.findByPagerAndLimit(true, "selectionnotice", pager, params);

        List<SelectionNotice> selectionNoticesList;
        if (pager.getTotalCount() > 0){
            selectionNoticesList = (List<SelectionNotice>) pager.getList();
        }else{
            selectionNoticesList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(SelectionNotice selectionNotice:selectionNoticesList){
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            rMap = new HashMap<String,Object>();
            rMap.put("id",selectionNotice.getId()!=null?selectionNotice.getId():"");
            rMap.put("post",selectionNotice.getPost()!=null?selectionNotice.getPost().getName():"");
            rMap.put("selectDate",selectionNotice.getSelectDate()!=null?simpleDateFormat.format(selectionNotice.getSelectDate()):"");
            /*rMap.put("applyUser",selectionNotice.getApplyUser()!=null?selectionNotice.getApplyUser().getName():"该岗位还未选拔成功");*/
            if(selectionNotice.getSelectionFinished()!=null){
                if(selectionNotice.getSelectionFinished().equals("0")){
                    rMap.put("selectionFinished","流转中");
                }else {
                    rMap.put("selectionFinished","已归档");
                }
            }
            if(selectionNotice.getProcessState()!=null){
                String name=selectionNotice.getProcessState().name();
                if(name.equals("Finished")){
                    rMap.put("processState","已归档");
                }else if(name.equals("Running")){
                    rMap.put("processState","流转中");
                }else if(name.equals("Backed")){
                    rMap.put("processState","退回中");
                }else{
                    rMap.put("processState","被否决");
                }
            }
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    /**
     * 重新定义的参数判断
     * @param _search
     * @param params
     * @param filters
     * @return
     */
        public Map getSearchFilterParams(String _search,Map<String,Object> params,String filters){
        //createAlias Key集合
        List<String> alias = new ArrayList<String>();
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(_search)){
            boolean __search = Boolean.parseBoolean(_search);
            if(__search&& org.apache.commons.lang3.StringUtils.isNotEmpty(filters)){
                JSONObject filtersJson = JSONObject.fromObject(filters);
                if(org.apache.commons.lang3.StringUtils.equals(filtersJson.getString("groupOp"), "AND")){
                    //查询条件
                    JSONArray rulesJsons = filtersJson.getJSONArray("rules");
                    //遍历条件
                    for(int i=0;i<rulesJsons.size();i++){
                        JSONObject rule = rulesJsons.getJSONObject(i);
                        if(rule.has("field")&& org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("field"))&&rule.has("data")&& org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                            String field = rule.getString("field");
                            if(field.contains("_")){
                                String[] fields = field.split("_");
                                if(org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                                    params.put(field.replaceAll("_", "."), rule.getString("data"));
                                    //params.put(field.replace("_",","),rule.getString("data"));
                                }
                            }else{
                                if(org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                                    if(field.equals("selectDate")){
                                        params.put("selectDate", DataUtil.StringToDate(rule.getString("data")));
                                    }


                                    if(field.equals("processState")){//有输入
                                        String processState=rule.getString("data");
                                        if(processState.equals("流转中")){
                                            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running});
                                        }else if(processState.equals("已归档")){
                                            params.put("processState",new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                                        }else if(processState.equals("退回中")){
                                            params.put("processState",new FlowEnum.ProcessState []{FlowEnum.ProcessState.Backed});
                                        } else {//输入错误
                                            params.put("processState", new FlowEnum.ProcessState[]{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});

                                        }
                                    } else {
                                        params.put("processState", new FlowEnum.ProcessState[]{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
                                    }

                                }
                            }
                        }
                    }

                }
            }
        }

        return params;
    }

    public String read(){
        post=new Dict();
        makePlan=Lists.newArrayList();
        role=new Role();
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty( keyId)) {
            Company company = usersService.getCompanyByUser();
            selectionNotice = selectionNoticeService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            //岗位
            if(selectionNotice.getPost()!=null){
                post=selectionNotice.getPost();
            }
            //竞聘方案附件
            if (selectionNotice.getMakePlan()!=null){
               makePlan=selectionNotice.getMakePlan();
            }
            //审核小组
            if(selectionNotice.getRole()!=null){
                role=selectionNotice.getRole();
            }
        }
        return "read";
    }

    public String input(){
        post=new Dict();
        makePlan=Lists.newArrayList();
        role=new Role();
        loginUser = usersService.getLoginInfo();
        if (StringUtils.isNotEmpty(keyId)){
            selectionNotice = selectionNoticeService.get(keyId);
            Company company = usersService.getCompanyByUser();
            //岗位
            if(selectionNotice.getPost()!=null){
                post=selectionNotice.getPost();
            }
            //竞聘方案附件
            if (selectionNotice.getMakePlan()!=null){
                makePlan=selectionNotice.getMakePlan();
            }
            //审核小组
            if(selectionNotice.getRole()!=null){
                role=selectionNotice.getRole();
            }
        } else {

        }


        return "input";
    }

    private void setData(){

        makePlan=Lists.newArrayList();
       role=new Role();
        if(StringUtils.isNotEmpty(keyId)){
            selectionNotice = selectionNoticeService.get(keyId);
        }else{
            selectionNotice = new SelectionNotice();
            selectionNotice.setCreater(usersService.getLoginInfo());
        }
        if(StringUtils.isNotEmpty(postCategoryId)){
            post=dictService.get(postCategoryId.trim());
        }
      //  post=dictService.get(postDictId.trim());
        if(StringUtils.isNotEmpty(makePlanId)){
            for(String  s:makePlanId.split(",")){
                makePlan.add(fileManageService.get(s.trim())) ;
            }
        }
        //审核小组
        if(StringUtils.isNotEmpty(roleId)){
            role=roleService.get(roleId);
        }

        selectionNotice.setCreateDate(new Date());
        selectionNotice.setPost(post);
        selectionNotice.setRequires(requires);
        selectionNotice.setSelectDate(selectDate);
        selectionNotice.setMakePlan(makePlan);
        selectionNotice.setMakePlanText(makePlanText);
        selectionNotice.setRole(role);
        selectionNotice.setRemark(remark);
        selectionNotice.setSelectionFinished("0");//公告完成标记
        selectionNotice.setApplyFinished("0");//公告完成标记
        selectionNotice.setCompany(usersService.getLoginInfo().getCompany());
        selectionNoticeService.save(selectionNotice);

    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                selectionNoticeService.update(selectionNotice);


            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                selectionNoticeService.save(selectionNotice, "selectionnotice", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        setData();

        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        var2.put("initDuty", curDutyId);
        ArrayList<String> list = new ArrayList<String>();

        /*if(role.getUsersSet()!=null){
            Set<Users> usersSet=role.getUsersSet();
            for(Users users:usersSet){
                list.add(users.getId());
            }
        }*/

       List<Department> departments=departmentService.getAll();
        List<Users> usersList=null;
        Set<Power> powerSet=null;
       if(departments!=null){
           loop1:for (Department department:departments){
               if(department.getName().equals("产业中心")){
                   powerSet=department.getPowerSet();
                    if(powerSet!=null){
                         loop2:for(Power power:powerSet){
                             Post post=power.getPost();
                             if((post.getName().equals("书记")==true) || (post.getName().equals("主任")==true)){
                                 usersList =dutyService.getPersons(department,post);
                                 for(Users users:usersList){
                                     list.add(users.getId());
                                 }
                             }
                         }
                     }
               }
           }
       }
       for(String id:list){
       }
        var2.put("approvers",list);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                selectionNoticeService.approve(selectionNotice, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            } else {
                selectionNoticeService.commit(selectionNotice, "selectionnotice", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批
    public String approve1(){
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        selectionNotice = selectionNoticeService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        loginUser = usersService.getLoginInfo();
        /*var1.put("numStatus", 2);*/
        var1.put("curDutyId", curDutyId);

        try {

                selectionNoticeService.approve(selectionNotice, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
                Task overtask=workflowService.getCurrentTask(keyId);
                if(overtask==null){

                    SelectionNotice entity=selectionNoticeService.get(keyId);
                    entity.setProcessState(FlowEnum.ProcessState.Finished);
                    selectionNoticeService.update(entity);

                    List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(entity.getId());
                    for(TaskRecord taskRecord:taskRecords){
                        taskRecord.setType(2);
                        taskRecordService.update(taskRecord);
                    }
                }else{
                    if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){
                        runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 2);
                    }
                }

        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }
    // 会签
    /*
    public String approve2() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";

        selectionNotice = selectionNoticeService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        //  var2.put("curDutyId", curDutyId);
        try {
            selectionNoticeService.approve(selectionNotice, FlowEnum.ProcessState.Finished, var2, curDutyId, comment);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String approve3() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        selectionNotice = selectionNoticeService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 4);
        //  var2.put("curDutyId", curDutyId);
        try {
            selectionNoticeService.approve(selectionNotice, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }*/

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            selectionNotice = selectionNoticeService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("selectionnotice");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            System.out.println("comment:"+comment);
            LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            selectionNoticeService.reject(selectionNotice, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            selectionNotice = selectionNoticeService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("selectionnotice");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:"+key);
            if(com.joint.base.util.StringUtils.isEmpty(comment)){
                comment="";
            }
            selectionNoticeService.deny(selectionNotice, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }



    public SelectionNotice getSelectionNotice() {
        return selectionNotice;
    }

    public void setSelectionNotice(SelectionNotice selectionNotice) {
        this.selectionNotice = selectionNotice;
    }











    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }


    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }


    public String getViewtype() {


        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public List<Map<String, Object>> getPostArrayList() {
        return postArrayList;
    }

    public void setPostArrayList(List<Map<String, Object>> postArrayList) {
        this.postArrayList = postArrayList;
    }

    public Dict getPost() {
        return post;
    }

    public void setPost(Dict post) {
        this.post = post;
    }

    public String getRequires() {
        return requires;
    }

    public void setRequires(String requires) {
        this.requires = requires;
    }

    public Date getSelectDate() {
        return selectDate;
    }

    public void setSelectDate(Date selectDate) {
        this.selectDate = selectDate;
    }

    public List<FileManage> getMakePlan() {
        return makePlan;
    }

    public void setMakePlan(List<FileManage> makePlan) {
        this.makePlan = makePlan;
    }



    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMakePlanId() {
        return makePlanId;
    }

    public void setMakePlanId(String makePlanId) {
        this.makePlanId = makePlanId;
    }



    public String getMakePlanText() {
        return makePlanText;
    }

    public void setMakePlanText(String makePlanText) {
        this.makePlanText = makePlanText;
    }

    public String getApplyUserName() {
        return applyUserName;
    }

    public void setApplyUserName(String applyUserName) {
        this.applyUserName = applyUserName;
    }

    public String getApplyUserId() {
        return applyUserId;
    }

    public void setApplyUserId(String applyUserId) {
        this.applyUserId = applyUserId;
    }

    public String getMakePlanFileId() {
        return makePlanFileId;
    }

    public void setMakePlanFileId(String makePlanFileId) {
        this.makePlanFileId = makePlanFileId;
    }



    public String getApplyUserDate() {
        return applyUserDate;
    }

    public void setApplyUserDate(String applyUserDate) {
        this.applyUserDate = applyUserDate;
    }

    public SelectionPublicity getSelectionPublicity() {
        return selectionPublicity;
    }

    public void setSelectionPublicity(SelectionPublicity selectionPublicity) {
        this.selectionPublicity = selectionPublicity;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }


    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPostCategoryId() {
        return postCategoryId;
    }

    public void setPostCategoryId(String postCategoryId) {
        this.postCategoryId = postCategoryId;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

}


