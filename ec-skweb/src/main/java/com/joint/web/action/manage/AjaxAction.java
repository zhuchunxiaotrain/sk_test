package com.joint.web.action.manage;

import com.joint.base.service.CompanyService;
import com.joint.base.service.UsersService;
import com.joint.web.action.BaseFlowAction;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.Date;

@ParentPackage("manage")
public class AjaxAction extends BaseFlowAction {

    private static final long serialVersionUID = -6296292946341485313L;
    @Resource
    private CompanyService companyService;

    @Resource
    private UsersService usersService;
    /**
     * 视图标志
     */
    private String viewtype;
    /**
     * 今天
     */
    private Date today;
    /**
     * 当前进度
     */
    private String step;
    /**
     * 一个车辆要使用到的Id
     */
    private String carNoId3;
    /**
     * 公告通知
     * @return
     */
    public String notice(){return "notice";}

    /**
     * 公告通知明细
     * @return
     */
    public String noticequery(){return "noticequery";}

    /**
     * 日程安排
     * @return
     */
    public String schedule(){
        today = new Date();
        return "schedule";
    }
    /**
     * 发文申请
     * @return
     */
    public String dispatch(){return "dispatch";}

    /**
     * 发文申请附件修改
     */
    public String dispatchrevise(){
        return "dispatchrevise";
    }

    /**
     * 收文登记
     * @return
     */
    public String receive(){return "receive";}

    /**
     * 会议室使用登记
     * @return
     */
    public String meetinguse(){return "meetinguse";}

    /**
     * 会议记录
     * @return
     */
    public String meetingrecord(){return "meetingrecord";}

    /**
     * 会议签到
     * @return
     */
    public String meetingsign(){return "meetingsign";}

    /**
     * 会议请假
     * @return
     */
    public String meetingleave(){return "meetingleave";}

    /**
     * 会议日历
     */
    public String meetingusecalandar(){
        today = new Date();
        return  "meetingusecalandar";
    }

    /**
     * 资料管理
     * @return
     */
    public String registe(){return "registe";}

    /**
     *公司资质管理
     * @return
     */
    public String qualifications(){return "qualifications"; }

    /**
     *公司资质字典
     * @return
     */
    public String qualificationsdict(){return "qualificationsdict"; }

    /**
     * 印章使用管理
     * @return
     */
    public String seal(){return "seal"; }

    /**
     * 工作日志
     * @return
     */
    public String worklog(){return "worklog"; }

    /**
     * 车辆配置表
     * @return
     */
    public String carconfig(){return "carconfig"; }

    /**
     * 车辆使用管理
     * @return
     */
    public String car(){return "car"; }

    /**
     * 车辆加油管理
     * @return
     */
    public String oil(){return "oil"; }

    /**
     * 车辆加油明细
     * @return
     */
    public String oilquery(){return "oilquery"; }

    /**
     * 合同管理
     * @return
     */
    public String contract(){return "contract"; }

    /**
     * 采购管理
     * @return
     */
    public String purchase(){return "purchase"; }

    /**
     * 资产使用管理
     * @return
     */
    public String assetuse(){return "assetuse"; }

    /**
     * 资产使用明细
     * @return
     */
    public String usedetail(){return "usedetail"; }

    /**
     * 资产报废申请
     * @return
     */
    public String assetscrap(){return "assetscrap"; }

    /**
     * 接待管理
     * @return
     */
    public String reception(){return "reception"; }

    /**
     * 任务督办单
     * @return
     */
    public String taskcontrol(){return "taskcontrol";}

    /**
     * 任务督办反馈单
     * @return
     */
    public String taskback(){return "taskback";}

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }

    public String getCarNoId3() {
        return carNoId3;
    }

    public void setCarNoId3(String carNoId3) {
        this.carNoId3 = carNoId3;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }
}
