package com.joint.web.action.manage;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Users;
import com.joint.base.service.UsersService;
import com.joint.core.entity.manage.Archives;
import com.joint.core.entity.manage.ArchivesDetail;
import com.joint.core.service.ArchivesDetailService;
import com.joint.core.service.ArchivesService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ZhuChunXiao on 2017/3/23.
 */
@ParentPackage("manage")
public class AjaxArchivesAction extends BaseFlowAction {
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private ArchivesService archivesService;
    @Resource
    private ArchivesDetailService archivesDetailService;

    /**
     * 档案类型
     */
    private List<Map<String, Object>> typeDict;
    private String typeDictId;
    /**
     * 档案类目
     */
    private List<Map<String, Object>> categoryDict;
    private String categoryDictId;
    /**
     * 档案编号
     */
    private String no;
    /**
     * 文件题目
     */
    private String name;
    /**
     * 所属项目
     */
    private String project;

    private Archives archives;
    private List<ArchivesDetail> archivesDetailList;
    private ArchivesDetail archivesDetail;
    private String viewtype;
    private int numStatus;
    private Users loginUser;

    public String execute(){
        return "archives";
    }

    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                pager=archivesService.findByPagerAndLimit(false, "archives", pager, params);
            }else if(viewtype.equals("2")){
                //已归档
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                pager=archivesService.findByPagerAndFinish( "archives", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=archivesService.findByPagerAndLimit(true, "archives", pager, params);
        }
        List<Archives> archivesList;
        if (pager.getTotalCount() > 0){
            archivesList = (List<Archives>) pager.getList();
        }else{
            archivesList= new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(Archives archives: archivesList){
            rMap = new HashMap<String, Object>();
            rMap.put("id",archives.getId());
            rMap.put("no",archives.getNo());
            rMap.put("name",archives.getName());
            rMap.put("type",archives.getType().getName());
            rMap.put("category",archives.getCategory().getName());
            rMap.put("project",archives.getProject());
            rMap.put("state",archives.getProcessState().value());
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String input(){
        Company company = usersService.getCompanyByUser();
        List<Dict> archivesType = dictService.listFormDefinedEnable(DictBean.DictEnum.ArchivesType, company.getId());
        List<Dict> archivesCategory = dictService.listFormDefinedEnable(DictBean.DictEnum.ArchivesCategory, company.getId());
        Map<String,Object> rMap = null;
        typeDict = new ArrayList<Map<String, Object>>();
        categoryDict = new ArrayList<Map<String, Object>>();
        if(StringUtils.isNotEmpty(keyId)){
            archives=archivesService.get(keyId);
            for(Dict type:archivesType){
                rMap = new HashMap<String, Object>();
                rMap.put("id",type.getId());
                rMap.put("name",type.getName());
                rMap.put("selected","");
                if(archives.getType()!=null && StringUtils.equals(type.getId(), archives.getType().getId())){
                    rMap.put("selected","selected");
                }
                typeDict.add(rMap);
            }
            for(Dict category:archivesCategory){
                rMap = new HashMap<String, Object>();
                rMap.put("id",category.getId());
                rMap.put("name",category.getName());
                rMap.put("selected","");
                if(archives.getCategory()!=null && StringUtils.equals(category.getId(), archives.getCategory().getId())){
                    rMap.put("selected","selected");
                }
                categoryDict.add(rMap);
            }
            archivesDetailList=null;
            if(archives.getArchivesDetailList()!=null&&archives.getArchivesDetailList().size()>0){
                archivesDetailList=archives.getArchivesDetailList();
            }
        }else{
            for(Dict type:archivesType){
                rMap = new HashMap<String, Object>();
                rMap.put("id",type.getId());
                rMap.put("name",type.getName());
                rMap.put("selected","");
                typeDict.add(rMap);
            }
            for(Dict category:archivesCategory){
                rMap = new HashMap<String, Object>();
                rMap.put("id",category.getId());
                rMap.put("name",category.getName());
                rMap.put("selected","");
                categoryDict.add(rMap);
            }
        }
        return "input";
    }

    public List<Map<String, Object>> getTypeDict() {
        return typeDict;
    }

    public void setTypeDict(List<Map<String, Object>> typeDict) {
        this.typeDict = typeDict;
    }

    public String getTypeDictId() {
        return typeDictId;
    }

    public void setTypeDictId(String typeDictId) {
        this.typeDictId = typeDictId;
    }

    public List<Map<String, Object>> getCategoryDict() {
        return categoryDict;
    }

    public void setCategoryDict(List<Map<String, Object>> categoryDict) {
        this.categoryDict = categoryDict;
    }

    public String getCategoryDictId() {
        return categoryDictId;
    }

    public void setCategoryDictId(String categoryDictId) {
        this.categoryDictId = categoryDictId;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public Archives getArchives() {
        return archives;
    }

    public void setArchives(Archives archives) {
        this.archives = archives;
    }

    public ArchivesDetail getArchivesDetail() {
        return archivesDetail;
    }

    public void setArchivesDetail(ArchivesDetail archivesDetail) {
        this.archivesDetail = archivesDetail;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }
}
