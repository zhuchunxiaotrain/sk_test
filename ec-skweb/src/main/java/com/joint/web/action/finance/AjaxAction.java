package com.joint.web.action.finance;

import com.joint.base.service.CompanyService;
import com.joint.base.service.UsersService;
import com.joint.web.action.BaseFlowAction;

import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;


@ParentPackage("finance")
public class AjaxAction extends BaseFlowAction {


    private static final long serialVersionUID = -6296292946341485313L;
    @Resource
    private CompanyService companyService;

    @Resource
    private UsersService usersService;

    private String viewtype;
    private String step;
    private String auditId;


    public String finance(){return "finance"; }
    public String calculationyear(){return "calculationyear"; }
    public String modifyyear(){return "modifyyear"; }
    public String newsmonth(){return "newsmonth"; }
    public String financeyear(){return "financeyear"; }
    public String financialanalysis(){return "financialanalysis"; }
    public String workplan(){return "workplan"; }
    public String auditnotice(){return "auditnotice"; }
    public String auditfirst(){return "auditfirst"; }
    public String auditlast(){return "auditlast"; }
    public String auditedit(){return "auditedit"; }
    public String auditanswer(){return "auditanswer"; }
    public String expend(){return "expend"; }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getAuditId() {
        return auditId;
    }

    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }
}
