package com.joint.web.action.manage;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.DepartmentService;
import com.joint.base.service.ReadersService;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.ManageProInfo;
import com.joint.core.entity.manage.Notice;
import com.joint.core.entity.manage.NoticeQuery;
import com.joint.core.service.ManageProInfoService;
import com.joint.core.service.NoticeQueryService;
import com.joint.core.service.NoticeService;
import com.joint.core.service.NumberCreaterService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.*;


@ParentPackage("manage")
public class AjaxNoticeAction extends BaseFlowAction {
    @Resource
    private NoticeService noticeService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private ReadersService readersService;
    @Resource
    private NoticeQueryService noticeQueryService;
    /**
     * 公告通知对象(读)
     */
    private Notice notice;

    /**
     * 登录人(读)
     */
    private Users loginUser;
    /**
     * 附件上传Id(读)
     */
    private String fileId;

    /**
     * 公告类型（读）
     */
    private List<Map<String, Object>> noticeDict;
    /**
     *  通知主题
     */
    private String topic;
    /**
     * 是否置顶
     */
    private String top;
    /**
     *  公告类型
     */
    private String noticedictId;
    /**
     * 发布部门
     */
    private String departmentId;
    /**
     * 通知对象
     */
    private String object;
    /**
     * 通知部门
     */
    private String noticeDepartmentId;
    /**
     * 通知个人
     */
    private String noticeUsersId;
    /**
     * 内容
     */
    private String content;
    /**
     * 附件上传Id
     */
    private String fileIds;
    /**
     * 视图类型
     */
    private String viewtype;
    /**
     * 职责列表
     */
    private List<Duty> dutyList;

    /**
     * 附件列表
     */
    private Set<String> fileUrl;

    /**
     * 是否具有有效期
     */
    private String hasValid;

    /**
     * 有效期
     */
    private String validDate;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "notice";
    }



    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        if(viewtype !=null && viewtype.equals("2")){
            pager.setOrderBy("modifyDate");
        }
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();

        params = getSearchFilterParams(_search,params,filters);

        /*内嵌视图这里要加上
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(parentId)){
            manageProInfo = manageProInfoService.get(parentId);
            params.put("manageProInfo",manageProInfo);
        }*/
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
      //  LogUtil.info("viewtype:" + viewtype);
        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                params.put("invalid", false);
                pager=noticeService.findByPagerAndLimit(false, "notice", pager, params);
            }else if(viewtype.equals("2")){
                //已归档
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                params.put("invalid", false);
                pager=noticeService.findByPagerAndFinish( "notice", pager, params);
            }else if(viewtype.equals("3")){
                //已失效
                params.put("invalid", true);
                pager=noticeService.findByPagerAndLimit(true, "notice", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=noticeService.findByPagerAndLimit(true, "notice", pager, params);
        }

        List<Notice> noticeList;
        if (pager.getTotalCount() > 0){
            noticeList = (List<Notice>) pager.getList();
        }else{
            noticeList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        if(viewtype !=null && viewtype.equals("2")){
            for(Notice notice: noticeList){
                if(notice.isTop()){
                    rMap = new HashMap<String, Object>();
                    rMap.put("id",notice.getId());
                    rMap.put("type",notice.getType() != null?notice.getType().getName():"");
                    rMap.put("topic",notice.getTopic());
                    rMap.put("creater", notice.getCreater() != null ? notice.getCreater().getName() : "");
                    rMap.put("createDate", DataUtil.DateToString(notice.getCreateDate(),"yyyy-MM-dd"));
                    rMap.put("modifyDate", DataUtil.DateToString(notice.getModifyDate(),"yyyy-MM-dd"));
                    rMap.put("state",notice.getProcessState().value());
                    JSONObject o = JSONObject.fromObject(rMap);
                    dataRows.add(o);
                }
            }
            for(Notice notice: noticeList){
                if(!notice.isTop()){
                    rMap = new HashMap<String, Object>();
                    rMap.put("id",notice.getId());
                    rMap.put("type",notice.getType() != null?notice.getType().getName():"");
                    rMap.put("topic",notice.getTopic());
                    rMap.put("creater", notice.getCreater() != null ? notice.getCreater().getName() : "");
                    rMap.put("createDate", DataUtil.DateToString(notice.getCreateDate(),"yyyy-MM-dd"));
                    rMap.put("modifyDate", DataUtil.DateToString(notice.getModifyDate(),"yyyy-MM-dd"));
                    rMap.put("state",notice.getProcessState().value());
                    JSONObject o = JSONObject.fromObject(rMap);
                    dataRows.add(o);
                }
            }
        }else{
            for(Notice notice: noticeList){
                rMap = new HashMap<String,Object>();
                rMap.put("id",notice.getId());
                rMap.put("type",notice.getType() != null?notice.getType().getName():"");
                rMap.put("topic",notice.getTopic());
                rMap.put("creater", notice.getCreater() != null ? notice.getCreater().getName() : "");
                rMap.put("createDate", DataUtil.DateToString(notice.getCreateDate(),"yyyy-MM-dd"));
                rMap.put("state",notice.getProcessState().value());
                JSONObject o = JSONObject.fromObject(rMap);
                dataRows.add(o);
            }
        }


        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);


        return  ajaxJson(JSONObject.fromObject(data).toString());
    }



    public String read(){
        loginUser = usersService.getLoginInfo();
        List<Dict> noticeType =dictService.listFormDefinedEnable(DictBean.DictEnum.NoticeType, usersService.getCompanyByUser().getId());
        Map<String,Object> rMap = null;
        if(StringUtils.isNotEmpty(keyId)) {

            fileId ="";
            notice =noticeService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            if(notice.getFile() != null && notice.getFile().size()>0){
                for(FileManage f:notice.getFile()){
                    fileId+=f.getId()+",";
                }
            }

            if(StringUtils.equals(notice.getProcessState().name(),"Finished")){
                List<NoticeQuery>  noticeQueryList = noticeQueryService.findUsersByNoticeQuery(loginUser.getId(),keyId);
                if(noticeQueryList.size()==0){
                    NoticeQuery noticeQuery = new NoticeQuery();
                    noticeQuery.setUsers(loginUser);
                    noticeQuery.setKeyId(keyId);
                    noticeQueryService.save(noticeQuery);
                }
            }
        }
        return "read";
    }

    public String input(){
      //  loginUser = usersService.getLoginInfo();
        Company company = usersService.getCompanyByUser();
        List<Dict> noticeType = dictService.listFormDefinedEnable(DictBean.DictEnum.NoticeType, company.getId());
        Map<String,Object> rMap = null;
        noticeDict = new ArrayList<Map<String, Object>>();
        if (StringUtils.isNotEmpty(keyId)){
            fileId ="";
            notice = noticeService.get(keyId);
            if(notice.getFile() != null && notice.getFile().size()>0){
                for(FileManage f:notice.getFile()){
                    fileId+=f.getId()+",";
                }
            }
            for(Dict typeObj:noticeType){
                rMap = new HashMap<String, Object>();
                rMap.put("id",typeObj.getId());
                rMap.put("name",typeObj.getName());
                rMap.put("selected","");
                if(notice.getType()!=null && com.joint.base.util.StringUtils.equals(typeObj.getId(), notice.getType().getId())){
                    rMap.put("selected","selected");
                }
                noticeDict.add(rMap);
            }
        } else {
            for(Dict typeObj:noticeType){
                rMap = new HashMap<String, Object>();
                rMap.put("id",typeObj.getId());
                rMap.put("name",typeObj.getName());
                rMap.put("selected","");
                noticeDict.add(rMap);
            }

        }

        return "input";
    }

    private void setData(){
        if(StringUtils.isNotEmpty(keyId)){
            notice = noticeService.get(keyId);
        }else{
            notice = new Notice();
            notice.setCreater(usersService.getLoginInfo());
        }
        notice.setTopic(topic);
        notice.setTop(top.equals("1") ? true : false);
        notice.setType(dictService.get(noticedictId));
        notice.setDepartment(departmentService.get(departmentId));
        notice.setObject(object);
        notice.setHasValid((hasValid != null && hasValid.equals("1")) ? true : false);
        notice.setValidDate(DataUtil.StringToDate(validDate));
        List<Department> departmentList = Lists.newArrayList();
        if(com.joint.base.util.StringUtils.isNotEmpty(noticeDepartmentId)){
            String[] idArr = noticeDepartmentId.split(",");
            for(String id:idArr){
                departmentList.add(departmentService.get(id.trim()));
            }
        }
        List<Users> usersList = Lists.newArrayList();
        if(StringUtils.isNotEmpty(noticeUsersId)){
            String[] idArr = noticeUsersId.split(",");
            for(String id:idArr){
                usersList.add(usersService.get(id.trim()));
            }
        }
        List<FileManage> fileManageList = Lists.newArrayList();
        if(com.joint.base.util.StringUtils.isNotEmpty(fileIds)){
            for(String f:fileIds.split(",")){
                fileManageList.add(fileManageService.get(f.trim()));
            }
        }
        notice.setNoticeDepartment(departmentList);
        notice.setNoticeUsers(usersList);
        notice.setContent(content);
        notice.setFile(fileManageList);
        notice.setCompany(usersService.getLoginInfo().getCompany());
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                noticeService.update(notice);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                noticeService.save(notice, "notice", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
       // LogUtil.info("do1");
        setData();
       // LogUtil.info("do2");
        Company company = usersService.getCompanyByUser();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        // System.out.println("nextStepApproversId:"+nextStepApproversId);
        Set<Users> usersSet = Sets.newHashSet();
        Map<String, Object> map = new HashMap<String, Object>();
        List<Department> listDept = notice.getNoticeDepartment();
       // LogUtil.info("do:"+object);
        if(StringUtils.equals(object,"1") == true){
            map.put("state", BaseEnum.StateEnum.Enable);
            List<Users> usersList = (List<Users>) usersService.findByPagerAndCompany(new Pager(0),null,company,map).getList();
            LogUtil.info("usersList:"+usersList.size());
            for(Users users:usersList){
                usersSet.add(users);
            }
        }else if(StringUtils.equals(object,"2") == true){
            for(Department department:listDept){
                dutyList = dutyService.getDutys(department);
                for(Duty duty:dutyList){
                    usersSet.add(duty.getUsers());
                }
            }
        }else if(StringUtils.equals(object,"3") == true){
            List<Users> usersList = notice.getNoticeUsers();
            for(Users users:usersList){
                usersSet.add(users);
            }
        }
        /*
        Set<String> ids = Sets.newHashSet();
        for(Users users:usersSet){
            ids.add(users.getId());
        }
         var2.put("archiveReaders",  ids);    // 归档后读者域名
         */
        //LogUtil.info("do3");
        

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                noticeService.approve(notice, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            } else {
                keyId = noticeService.commit(notice, "notice", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        //先删除读者
        readersService.deleteByKeyIdBussinessKey(keyId);
        //增加归档后读者
        for(Users users:usersSet){
            Readers readers = new Readers();
            readers.setUsers(users);
            readers.setBussinessKey("notice");
            readers.setKeyId(keyId);
            readers.setType(2);
            readersService.save(readers);
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批
    public String approve1(){
        notice = noticeService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();

        var1.put("numStatus", 2);
        var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                noticeService.approve(notice, FlowEnum.ProcessState.Finished, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }
    // 会签
    public String approve2() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";

        notice = noticeService.get(keyId);
       // Task task=workflowService.getCurrentTask(keyId, usersService.getLoginInfo());
       // Map<String, Object> var2 = new HashMap<String, Object>();
        /*
        int multComplete = (Integer)runtimeService.getVariable(task.getExecutionId(),"nrOfCompletedInstances");
        int nrOfInstances = (Integer)runtimeService.getVariable(task.getExecutionId(),"nrOfInstances");
        LogUtil.info("multComplete:"+multComplete);
        LogUtil.info("nrOfInstances:"+nrOfInstances);

        if((double)multComplete/nrOfInstances>0.6){
            var2.put("numStatus", 2);
            LogUtil.info("do:numStatus2");
        } else {
            var2.put("numStatus", 1);
            LogUtil.info("do:numStatus1");
        }*/
        Map<String, Object> var2 = new HashMap<String, Object>();
      //  var2.put("curDutyId", curDutyId);
        try {
            noticeService.approve(notice, FlowEnum.ProcessState.Running, var2, curDutyId, comment);

            Task overtask = workflowService.getCurrentTask(keyId);
            //LogUtil.info("overtask:"+overtask);
            if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){

                runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 3);
            }

        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String approve3() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        notice = noticeService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 4);
      //  var2.put("curDutyId", curDutyId);
        try {
            noticeService.approve(notice, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            notice = noticeService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("notice");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            System.out.println("comment:"+comment);
            LogUtil.info("numStatus:" + numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            noticeService.reject(notice, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            notice = noticeService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("notice");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:" + key);
            if(com.joint.base.util.StringUtils.isEmpty(comment)){
                comment="";
            }
            noticeService.deny(notice, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }

    /**
     * 失效
     * @return
     */
    public String doInvalid(){
        if(StringUtils.isNotEmpty(keyId)) {
            notice = noticeService.get(keyId);
            notice.setInvalid(true);
            noticeService.update(notice);
        }
        return ajaxHtmlCallback("200", "操作成功！", "操作状态");
    }

    /**
     * 检查是否为公告
     * @return
     */
    public String isTop(){
        String json = "";
        if(StringUtils.isNotEmpty(keyId)) {
            notice = noticeService.get(keyId);
            if(notice.isTop()){
               json = "{\"rs\":\""+true+"\"}";
                return ajaxHtml(json);
            }else{
               json = "{\"rs\":\""+false+"\"}";
                return ajaxHtml(json);
            }
        }
        json = "{\"rs\":\""+false+"\"}";
        return ajaxHtml(json);
    }

    /**
     * 将公告置顶
     * @return
     */
    public String doTop(){
        if(StringUtils.isNotEmpty(keyId)) {
            notice = noticeService.get(keyId);
            notice.setTop(true);
            noticeService.update(notice);
        }
        return ajaxHtmlCallback("200", "操作成功！", "操作状态");
    }
    /**
     * 将公告取消置顶
     * @return
     */
    public String doCancelTop(){
        if(StringUtils.isNotEmpty(keyId)) {
            notice = noticeService.get(keyId);
            notice.setTop(false);
            noticeService.update(notice);
        }
        return ajaxHtmlCallback("200", "操作成功！", "操作状态");
    }

    public Notice getNotice() {
        return notice;
    }

    public void setNotice(Notice notice) {
        this.notice = notice;
    }



    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public List<Map<String, Object>> getNoticeDict() {
        return noticeDict;
    }

    public void setNoticeDict(List<Map<String, Object>> noticeDict) {
        this.noticeDict = noticeDict;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public String getNoticedictId() {
        return noticedictId;
    }

    public void setNoticedictId(String noticedictId) {
        this.noticedictId = noticedictId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getNoticeDepartmentId() {
        return noticeDepartmentId;
    }

    public void setNoticeDepartmentId(String noticeDepartmentId) {
        this.noticeDepartmentId = noticeDepartmentId;
    }

    public String getNoticeUsersId() {
        return noticeUsersId;
    }

    public void setNoticeUsersId(String noticeUsersId) {
        this.noticeUsersId = noticeUsersId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFileIds() {
        return fileIds;
    }

    public void setFileIds(String fileIds) {
        this.fileIds = fileIds;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public String getHasValid() {
        return hasValid;
    }

    public void setHasValid(String hasValid) {
        this.hasValid = hasValid;
    }
}

