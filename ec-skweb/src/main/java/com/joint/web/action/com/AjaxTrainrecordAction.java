package com.joint.web.action.com;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.service.DictService;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.service.*;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.*;
import com.joint.core.service.*;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by root on 16/12/16.
 */
@ParentPackage("com")
public class AjaxTrainrecordAction extends BaseFlowAction {

    @Resource
    private TrainRecordService trainRecordService;
    @Resource
    private TrainService trainService;
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private ManageService manageService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private ParticipantService participantService;
    @Resource
    private EmployeesService employeesService;

    /**
     * 视图类型
     */
    private String viewtype;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "trainrecord";
    }

    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        //内嵌视图这里要加上
       if (StringUtils.isNotEmpty(parentId)){
           Employees employees = employeesService.get(parentId);
            params.put("users",employees.getUsers());
        }
        //params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        //所有单子
       // params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
        pager=trainRecordService.findByPager(pager, params);
        List<TrainRecord> tList;
        if (pager.getTotalCount() > 0){
            tList = (List<TrainRecord>) pager.getList();
        }else{
            tList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(TrainRecord trainRecord:tList){
            rMap = new HashMap<String,Object>();
            rMap.put("name",trainRecord.getName());
            rMap.put("id",trainRecord.getId());
            rMap.put("type",trainRecord.getType()==null?"":trainRecord.getType());
            rMap.put("time","从"+DataUtil.DateToString(trainRecord.getStartDate(),"yyyy-MM-dd")+"到"+DataUtil.DateToString(trainRecord.getEndDate(),"yyyy-MM-dd"));
            rMap.put("time1",DataUtil.DateToString(trainRecord.getCreateDate(),"yyyy-MM-dd"));
            rMap.put("state","培训完成");

            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);
        //LogUtil.info("dataRows"+dataRows);
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }
}
