package com.joint.web.action.com;

import com.joint.web.action.BaseFlowAction;
import org.apache.struts2.convention.annotation.ParentPackage;

/**
 * Created by root on 16/12/16.
 */
@ParentPackage("com")
public class AjaxEmployAction extends BaseFlowAction {


    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "employ";
    }


}
