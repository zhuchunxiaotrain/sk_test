package com.joint.web.action.com;


import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.BaseEnum.StateEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.bean.Result;
import com.fz.us.base.util.LogUtil;
import com.fz.us.base.util.PinyinUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.joint.base.bean.EnumManage;
import com.joint.base.entity.*;
import com.joint.base.entity.system.Admin;
import com.joint.base.service.*;
import com.joint.base.util.DataUtil;

import com.joint.web.action.BaseAdminAction;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by hpj on 2014/9/16.
 */
@ParentPackage("com")
public class AjaxUsersAction extends BaseAdminAction {

    @Resource
    private AdminService adminService;
    @Resource
    private UsersService usersService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private PostService postService;
    @Resource
    private DutyService dutyService;
    @Resource
    private TaskService taskService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private HistoryService historyService;
    @Resource
    private FileManageService fileManageService;
    @Resource
    private PowerService powerService;
    @Resource
    private ProcessConfigService processConfigService;
    @Resource
    private PermissionService permissionService;


    protected Pager pager;
    protected Users user;
    protected List<Users> usersList;
    private ArrayList<Map<String,Object>> departArrayList;
    private List<Map<String,Object>> postArrayList;
    private Map<String,Object> dutyMap;
    private Set<Post> postSet;
    private List<Duty> dutyList;
    private List<Department> departmentList;
    private ArrayList<Map<String,Object>> dutyArrayList;
    private ArrayList<Map<String,Object>> taskArrayList;
    private List<Map<String,Object>> usersmapList;
    private List<Map<String,Object>> commentList;

    private String keyId;
    private String email;
    private String mobile;
    private String name;
    private String password;
    private String no;
    private String rowId;
    private String rowIds;
    private String idCard;
    private String address;
    private String province;
    private String city;
    private String county;
    private String phone;
    private String terminal;
    private String personalSign;
    private String departmentname;
    private String postname;
    private File filedata;
    private FileManage fileManage;
    private String fileId;
    private String filename;
    private String filedataFileName;
    private String filedataContentType;
    private String imgUrl;
    private String todoTask;
    private String finishTask;
    private String businessKeyList;
    private String repassword;
    private String validPeriod;
    private List<String> adminType;
    private int todoNum;
    private int finishNum;
    private int totalTask;
    private int x;
    private int y;
    private int w;
    private int h;

    private BigDecimal latitude;
    private BigDecimal longitude;

    private String authcode;
    private String newId;
    private Users users;

    private String userId;
    private String userName;
    protected Company company;

    private String departmentId;


    /**
     *返回人员管理视图
     * @return
     */
    public String list(){

        pager = new Pager();
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("no");
        pager.setOrderType(BaseEnum.OrderType.asc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("state", new StateEnum[]{StateEnum.Enable});
        if(StringUtils.isNotEmpty("userName")){
            params.put("name", userName);
        }
        params = getSearchFilterParams(_search,params,filters);

        Company company = usersService.getCompanyByUser();
        pager = usersService.findByPagerAndCompany(pager,null,company,params);
        usersList = (List<Users>) pager.getList();
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(Users users:usersList){
            rMap = new HashMap<String, Object>();
            rMap.put("dutyDefault", "");
            Set<Duty> dutys = users.getDutySet();
            for(Duty duty: dutys){
                if(duty.getState().toString().equals("Enable") && duty.getDutyDefault() == 1){
                    rMap.put("dutyDefault", "1");
                    break;
                }
            }
            rMap.put("id", users.getId());
            rMap.put("no",users.getNo());
            rMap.put("iscompany", StringUtils.equals(company.getAdminId(), users.getAdminId())?"1":0);

            if(StateEnum.Enable.value().equals(users.getState().value())){
                rMap.put("state","state");
            }else{
                rMap.put("state","");
            }
            String imgUrl = "";
            List<FileManage> fileManageList = fileManageService.getFileByKeyId(users.getId());
            if(fileManageList.size() > 0){
                FileManage fileManage = fileManageList.get(0);
                imgUrl = fileManage.getUrl();
            }
            if (usersService.getLoginInfo().getId().equals(users.getId())) {
                rMap.put("isCurrentUser","1");
            } else {
                rMap.put("isCurrentUser","0");
            }
            Admin admin = adminService.get(users.getAdminId());
//            String openId =  admin.getOpenId();
//            String deviceId =  admin.getDeviceId();

//            if (usersService.getLoginInfo().getAdminType().indexOf("3")>=0 && StringUtils.isNotEmpty(openId)){
//                rMap.put("isCurrentUser",rMap.get("isCurrentUser")+"1");
//            }else{
//                rMap.put("isCurrentUser",rMap.get("isCurrentUser")+"0");
//            }
            rMap.put("userImg",imgUrl);
            rMap.put("name",users.getName());
            rMap.put("mobile",users.getMobile());
            rMap.put("createDate", DataUtil.DateTimeToString(users.getCreateDate()));
            rMap.put("adminType", users.getAdminType()!=null?users.getAdminType():"");
//            rMap.put("isBind", StringUtils.isNotEmpty(openId) || StringUtils.isNotEmpty(deviceId));
//            if(StringUtils.isNotEmpty(users.getAdminId())){
//                rMap.put("role",adminService.get(users.getAdminId()).getAccountType());
//            }

     //       rMap.put("validPeriod",adminInfoService.getDaysByUsers(users));
     //       rMap.put("codeStatus",adminInfoService.getStatusByUsers(users));

            dataRows.add(JSONObject.fromObject(rMap));
        }
        long total =pager.getTotalCount();
        if(total%rows==0){
            total = total/rows;
        }else{
            if(total<rows){
                total = 1;
            }else{
                total = total/rows + 1;
            }
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        data.put("total",total);
        data.put("records",pager.getTotalCount());
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    /**
     * 人事模块
     */
    public String userByDepart(){

        pager = new Pager();
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("no");
        pager.setOrderType(BaseEnum.OrderType.asc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("state", new StateEnum[]{StateEnum.Enable});
        if(StringUtils.isNotEmpty("userName")){
            params.put("name", userName);
        }
        List<String> idList= Lists.newArrayList();

        params = getSearchFilterParams(_search,params,filters);

        if(StringUtils.isNotEmpty("departmentId")){
            Department department = departmentService.get(departmentId);
            if(department!=null){
                dutyList = dutyService.getDutys(department);
                for(Duty duty:dutyList){
                    idList.add(duty.getUsers().getId());
                }
                params.put("id",idList);

            }

        }

        Company company = usersService.getCompanyByUser();
        pager = usersService.findByPagerAndCompany(pager,null,company,params);
        usersList = (List<Users>) pager.getList();
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(Users users:usersList){
            rMap = new HashMap<String, Object>();
            rMap.put("dutyDefault", "");
            Set<Duty> dutys = users.getDutySet();
            for(Duty duty: dutys){
                if(duty.getState().toString().equals("Enable") && duty.getDutyDefault() == 1){
                    rMap.put("dutyDefault", "1");
                    break;
                }
            }
            rMap.put("id", users.getId());
            rMap.put("no",users.getNo());
            rMap.put("iscompany", StringUtils.equals(company.getAdminId(), users.getAdminId())?"1":0);

            if(StateEnum.Enable.value().equals(users.getState().value())){
                rMap.put("state","state");
            }else{
                rMap.put("state","");
            }
            String imgUrl = "";
            List<FileManage> fileManageList = fileManageService.getFileByKeyId(users.getId());
            if(fileManageList.size() > 0){
                FileManage fileManage = fileManageList.get(0);
                imgUrl = fileManage.getUrl();
            }
            if (usersService.getLoginInfo().getId().equals(users.getId())) {
                rMap.put("isCurrentUser","1");
            } else {
                rMap.put("isCurrentUser","0");
            }
            Admin admin = adminService.get(users.getAdminId());
//            String openId =  admin.getOpenId();
//            String deviceId =  admin.getDeviceId();

//            if (usersService.getLoginInfo().getAdminType().indexOf("3")>=0 && StringUtils.isNotEmpty(openId)){
//                rMap.put("isCurrentUser",rMap.get("isCurrentUser")+"1");
//            }else{
//                rMap.put("isCurrentUser",rMap.get("isCurrentUser")+"0");
//            }
            rMap.put("userImg",imgUrl);
            rMap.put("name",users.getName());
            rMap.put("mobile",users.getMobile());
            rMap.put("createDate", DataUtil.DateTimeToString(users.getCreateDate()));
            rMap.put("adminType", users.getAdminType()!=null?users.getAdminType():"");
//            rMap.put("isBind", StringUtils.isNotEmpty(openId) || StringUtils.isNotEmpty(deviceId));
//            if(StringUtils.isNotEmpty(users.getAdminId())){
//                rMap.put("role",adminService.get(users.getAdminId()).getAccountType());
//            }

            //       rMap.put("validPeriod",adminInfoService.getDaysByUsers(users));
            //       rMap.put("codeStatus",adminInfoService.getStatusByUsers(users));

            dataRows.add(JSONObject.fromObject(rMap));
        }
        long total =pager.getTotalCount();
        if(total%rows==0){
            total = total/rows;
        }else{
            if(total<rows){
                total = 1;
            }else{
                total = total/rows + 1;
            }
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);
        data.put("total",total);
        data.put("records",pager.getTotalCount());
        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    /**
     * 返回人员编辑视图
     * @return
     */
    public String read(){
        if(StringUtils.isNotEmpty(keyId)){
            user = usersService.get(keyId);
            dutyList=dutyService.getDutys(user);
        }
        return "read";
    }
    /**
     * 冻结、激活
     */
    public String setUserState(){
        if(StringUtils.isNotEmpty(keyId)){
            Users users=usersService.get(keyId);

            if(users.getState().equals(StateEnum.Enable)){
                users.setState(StateEnum.Disenable);
            }else {
                users.setState(StateEnum.Enable);
            }
            usersService.update(users);
        }
        return ajaxHtmlCallback("200", "设置成功!", "操作状态");
    }



    /**
     * 重置密码
     * @return
     */
    public String reset(){
        if(StringUtils.isEmpty(keyId)){
            return ajaxHtmlCallback("400", "请选择员工！","操作状态");
        }
        String salt = new SecureRandomNumberGenerator().nextBytes().toHex();
        Users user = usersService.get(keyId);
//        user.setSalt(salt);
        user.setPassword(usersService.encodedPassword(user.getEmail(), password, salt));
        usersService.update(user);
        return ajaxHtmlCallback("200", "重置成功！", "操作状态");
    }




    /**
     * 人员添加页面
     */
    public String input(){
        if(StringUtils.isNotEmpty(keyId)){
            user = usersService.get(keyId);
        }
        return  "input";
    }

    /**
     * 用户唯一性校验
     * @return
     */
    public void isUnique() {
        PrintWriter out = null;
        Users user= usersService.getUsersByEmail(email);
        try {
            out = getResponse().getWriter();
            if (user== null){
                out.print("true");
            } else{
                out.print("false");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 姓名唯一性校验(通过姓名)
     * @return
     */
    public void isUniqueByName() {
        LogUtil.info("name:"+name);
        PrintWriter out= null;
        try {
            out=getResponse().getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Users users= usersService.getUsersByUsername(name);
        if(users == null){
            out.print("true");
        }else{
            out.print("false");
        }
    }


    /**
     * 用户唯一性校验(通过登陆手机号)
     * @return
     */
    public void isUniqueByMobile() {
        LogUtil.info("mobile:" + mobile);
        PrintWriter out= null;
        try {
            out=getResponse().getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Users users= usersService.getUsersByMobile(mobile);
        if(users == null){
            out.print("true");
        }else{
            out.print("false");
        }
    }

    /**
     * 添加人员
     * @return
     */
    public String save(){
        if(!StringUtils.equals(password, repassword)){
            return ajaxHtmlCallback("400", "两次输入密码不相同！","操作状态");
        }

        if(StringUtils.isNotEmpty(keyId)){
            user = usersService.get(keyId);
        }else{
            user = usersService.getUsersByMobileDel(mobile);
            if (null == user) {
                user = new Users();
            }
        }
        String aType = "";
        if (adminType != null && adminType.size()>0) {
            for (String type : adminType) {
                aType = aType + type;
            }
            user.setAdminType(aType);
        }
        user.setName(name);
        user.setMobile(mobile);
        user.setPassword(password);
        user.setState(StateEnum.Enable);
        user.setPinYin(PinyinUtil.getPingYin(name));
        user.setPinYinHead(PinyinUtil.getPinYinHeadChar(name));


        if (StringUtils.isNotEmpty(keyId)) {
            usersService.update(user);
            Admin admin = adminService.getByMobile(user.getMobile());
            admin.setUsername(name);
            adminService.update(admin);
        }else {
            usersService.registerUserAndAdmin(user, usersService.getCompanyByUser());
        }

        return ajaxHtmlCallback("200", "保存成功！","操作状态");
    }

    /**
     * 人员删除
     * @return
     */
    public String delete(){
        if(StringUtils.isEmpty(keyId)){
            return ajaxHtmlCallback("400", "请选择成员！","操作状态");
        }
        Users user = usersService.get(keyId);
        List<Task> taskList=taskService.createTaskQuery().taskAssignee(user.getId()).active().orderByTaskCreateTime().desc().list();
        List<Duty> dutyList1=dutyService.getDutys(user);
        if(dutyList1.size()>0){
            return ajaxHtmlCallback("400","请先删除职责","操作状态");
        }
        if(taskList.size()>0){
            return ajaxHtmlCallback("400","请先处理待办","操作状态");
        }

        Admin admin=null;
        if(StringUtils.isNotEmpty(user.getAdminId())){
            admin = adminService.get(user.getAdminId());
            if(admin!=null) {
                admin.setState(StateEnum.Delete);
                admin.setOpenId(null);
                adminService.update(admin);
            }
        }
        user.setState(StateEnum.Delete);
        usersService.update(user);
        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    public String bindcancel(){
        if(StringUtils.isEmpty(keyId)){
            return ajaxHtmlCallback("400", "请选择成员！","操作状态");
        }
        Users user = usersService.get(keyId);
        Admin adminInfo = adminService.get(user.getAdminId());
        adminInfo.setOpenId(null);
        adminInfo.setDeviceId(null);
        adminService.update(adminInfo);
        return ajaxHtmlCallback("200", "解绑成功！", "操作状态");
    }

    public String profile(){
        if(StringUtils.isNotEmpty(keyId)){
            user=usersService.get(keyId);
        }
        return "profile";
    }

    /**
     * 上传图片
     * @return
     */
    public String uploadImg() throws IOException {
        fileManage = gridFSSave(new FileInputStream(filedata), filename,keyId,"APPLICATION/OCTET-STREAM",Users.class);
        fileId=fileManage.getId();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("gridId", fileManage.getId());
        data.put("fileUrl", fileManage.getUrl());
        data.put("status","200");
        data.put("title","操作状态");
        data.put("message","上传头像成功！");

        return ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String location(){
		/*String webUsername = (String) getSession("webUsername");
		Admin admin = adminService.getAdminByUsername(webUsername);
		admin.setLat(lat);
		admin.setLng(lng);
		adminService.update(admin);*/
        Users users = usersService.getLoginInfo();
        users.setLatitude(latitude);
        users.setLongitude(longitude);
        usersService.updateAndEnable(users);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("state", "200");
        data.put("title", "设置状态");
        data.put("message", "默认地图显示地址设置成功");
        return ajaxJson(JSONObject.fromObject(data).toString());
    }

    /**
     * 查看职责信息
     * @return
     */
    public String leftView(){
        if(StringUtils.isEmpty(keyId)){
            return ajaxHtmlCallback("400", "请选择员工！","操作状态");
        }
        Users user  = usersService.get(keyId);
        Duty duty=dutyService.get(keyId);
        dutyList = dutyService.getDutys(user);
        dutyArrayList=new ArrayList<Map<String, Object>>();
        Map<String,Object> rMap;
        for(Duty dutys:dutyList){
            rMap=new HashMap<String, Object>();
            if (dutys.getPower() != null) {
                rMap.put("department", dutys.getPower().getDepartment());
                rMap.put("post",dutys.getPower().getPost());
            } else {
                rMap.put("department", dutys.getDepartment());
                rMap.put("post","");
            }
            rMap.put("id",dutys.getId());
            rMap.put("checked","");
            if(dutys.getDutyDefault() == 1){
                rMap.put("checked", "checked");
            }
            dutyArrayList.add(rMap);
        }
        return "leftView";
    }

    /**
     * 点击分配岗位弹出dialog中的确定按钮事件
     * @return
     */
    public String config(){
        JSONArray dataRows = JSONArray.fromObject(rowIds);
        if(StringUtils.isEmpty("keyId")){
            return ajaxHtmlCallback("200", "请选择员工！","操作状态");
        }
        Users user = usersService.get(keyId);
        Set<Power> powerSet=Sets.newHashSet();
        List<Duty> dutyList= dutyService.getDutys(user);
        for(Duty duty:dutyList){
            if(duty.getPower()!=null){
                powerSet.add(duty.getPower());
            }
        }
        for(int i=0;i<dataRows.size();i++){
            JSONObject obj = dataRows.getJSONObject(i);
            String rowId = obj.getString("id");
            String departId = rowId.split("-")[0];
            String postId = rowId.split("-")[1];
            //老数据要做维护，由于cst-duty是部门+人员,eco-duty是人员+职权(部门+岗位)，因此这边做职责时，更新以前的职责记录
            Department department = departmentService.get(departId);
           // Duty onlyduty=dutyService.getUsersDepartmentDuty(department, user);
            Post post = postService.get(postId);

            Power power=powerService.getPowerByDepartAndPost(department, post);

            if(power==null){
                continue;
            }
            if(powerSet.contains(power)){
                continue;
            }
            /**
             *  updated by zhucx
             *  必须是没有职权的否则会造成同一部门的不同职权多个只能存一个
             */
            /*
            if(onlyduty!=null && onlyduty.getPower() == null){
                onlyduty.setPost(post);
                onlyduty.setPower(power);
                dutyService.update(onlyduty);
                continue;
            }*/

            Duty duty= new Duty(user,power,department,post);
            duty.setCompany(usersService.getCompanyByUser());
            duty.setDutyState(EnumManage.DutyState.Default);
            dutyService.save(duty);
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    /**
     * 根据部门来查找职责
     * @return
     */
    public Set<Post> getDutyByDepartment(List<Duty> dutyList, Department department){
        Set<Post> postSet =Sets.newHashSet();
        for(Duty duty:dutyList){
            if(duty.getDepartment() == department){
                postSet.add(duty.getPost());
            }
        }
        return postSet;
    }
    /**
     * 点击分配岗位弹出dialog
     * @return
     */

    public String configView(){
        Users users=usersService.get(keyId);
        List<Duty> dutyList= dutyService.getDutys(users);
        Set<Department> departmentSet=Sets.newHashSet();
       // Set<Post> postSet1 =Sets.newHashSet();
        for(Duty duty:dutyList){
            departmentSet.add(duty.getDepartment());
         //   postSet1.add(duty.getPost());
        }
        Company company=usersService.getCompanyByUser();
        departArrayList = new ArrayList<Map<String, Object>>();
        postArrayList = new ArrayList<Map<String, Object>>();
        if(StringUtils.isEmpty("keyId")){
            return ajaxHtmlCallback("200", "请选择成员！","操作状态");
        }
        pager = new Pager(0);
        pager= departmentService.findByPagerAndCompany(pager,null,company,new StateEnum[]{StateEnum.Enable});
        List<Department> departments= (List<Department>) pager.getList();
        Map<String,Object> rmap=null;
        Map<String,Object> pmap=null;
        for(Department department:departments){
            rmap=new HashMap<String, Object>();
            rmap.put("departId", department.getId());
            rmap.put("departName", department.getName());
            rmap.put("checked", "");

            if(departmentSet.contains(department)){
                rmap.put("checked", "checked");
                List<Power> powerList=powerService.getByDepartId(department.getId());
                if(powerList!=null && powerList.size()>0){
                    Set<Post> postSet =new HashSet<Post>();
                    Set<Post> postSet1 = getDutyByDepartment(dutyList, department);
                    for(Power power:powerList){
                        if(power.getPost()!=null){
                            postSet.add(power.getPost());
                        }
                    }
                    if(postSet!=null && postSet.size()>0){
                        for(Post post:postSet){
                            pmap=new HashMap<String, Object>();
                            pmap.put("id",department.getId()+"-"+post.getId());
                            pmap.put("name", department.getName() + "-" + post.getName());
                            if(postSet1.contains(post)){
                                pmap.put("checked", "checked");
                            }else{
                                pmap.put("checked", "");
                            }
                            postArrayList.add(pmap);
                        }
                    }
                }
            }
            departArrayList.add(rmap);
        }


        return "configView";
    }
    /**
     * 点击分配岗位弹出dialog中的部门checkBox，查询出该部门下的岗位
     * @return
     */
    public String configPost(){
        if(StringUtils.isEmpty(rowId)){
            return ajaxHtmlCallback("400", "请选择部门！","操作状态");
        }
        Department department=departmentService.get(rowId);
        List<JSONObject> dataRows = new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String, Object> rMap = null;
        List<Power> powerList=powerService.getByDepartId(rowId);
        Set<Post> postSet;
        Map<String,Object> rmap;
        postArrayList=new ArrayList<Map<String, Object>>();
        if(powerList!=null && powerList.size()>0){
            postSet=new HashSet<Post>();
            for(Power power:powerList){
                if(power.getPost()!=null){
                    postSet.add(power.getPost());
                }
            }
            if(postSet!=null && postSet.size()>0){
                for(Post post:postSet){
                    rmap=new HashMap<String, Object>();
                    rmap.put("id",rowId+"-"+post.getId());
                    rmap.put("name",department.getName()+"-"+post.getName());
                    rmap.put("checked","");
                    dataRows.add(JSONObject.fromObject(rmap));
                }
            }
        }
        data.put("state","200");
        data.put("rows", JSONArray.fromObject(dataRows));
        data.put("title", "操作状态");
        return ajaxJson(JSONObject.fromObject(data).toString());
    }

    /**
     * 默认职责更新
     */
    public String defaultDuty(){
        if(StringUtils.isEmpty(keyId) || StringUtils.isEmpty(userId)){
            return ajaxHtmlCallback("400", "选择人员职责信息！","操作状态");
        }
        Users users = usersService.get(userId);
        Map dmap = new HashMap<String, Object>();
        dmap.put("dutyDefault", 1);
        dmap.put("users", users);
        List<Duty> dutys = dutyService.getList(dmap);
        for(Duty info:dutys){
           if(!info.getId().equals(keyId)){
               info.setDutyDefault(0);
               dutyService.update(info);
           }
        }
        Duty duty=dutyService.get(keyId);
        if(duty.getDutyDefault() != 1){
            duty.setDutyDefault(1);
        }else{
            duty.setDutyDefault(0);
        }
        dutyService.update(duty);
        return ajaxHtmlCallback("200", "设置成功！", "操作状态");
    }

    /**
     * 删除职责信息
     * @return
     */
    public String deleteDuty(){
        // Todo 查看到底是设置默认职责还是删除职责
        if(StringUtils.isEmpty(keyId)){
            return ajaxHtmlCallback("400", "请选择职责！","操作状态");
        }
        Duty duty = dutyService.get(keyId);
        duty.setState(StateEnum.Delete);
        dutyService.update(duty);
        return ajaxHtmlCallback("200", "删除成功！","操作状态");
    }

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<Map<String, Object>> getDepartArrayList() {
        return departArrayList;
    }

    public void setDepartArrayList(ArrayList<Map<String, Object>> departArrayList) {
        this.departArrayList = departArrayList;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getRowIds() {
        return rowIds;
    }

    public void setRowIds(String rowIds) {
        this.rowIds = rowIds;
    }

    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    public Set<Post> getPostSet() {
        return postSet;
    }

    public void setPostSet(Set<Post> postSet) {
        this.postSet = postSet;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getPersonalSign() {
        return personalSign;
    }

    public void setPersonalSign(String personalSign) {
        this.personalSign = personalSign;
    }

    public String getDepartmentname() {
        return departmentname;
    }

    public void setDepartmentname(String departmentname) {
        this.departmentname = departmentname;
    }

    public String getPostname() {
        return postname;
    }

    public void setPostname(String postname) {
        this.postname = postname;
    }

    public Map<String, Object> getDutyMap() {
        return dutyMap;
    }

    public void setDutyMap(Map<String, Object> dutyMap) {
        this.dutyMap = dutyMap;
    }

    public List<Duty> getDutyList() {
        return dutyList;
    }

    public void setDutyList(List<Duty> dutyList) {
        this.dutyList = dutyList;
    }

    public File getFiledata() {
        return filedata;
    }

    public void setFiledata(File filedata) {
        this.filedata = filedata;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFiledataFileName() {
        return filedataFileName;
    }

    public void setFiledataFileName(String filedataFileName) {
        this.filedataFileName = filedataFileName;
    }

    public String getFiledataContentType() {
        return filedataContentType;
    }

    public void setFiledataContentType(String filedataContentType) {
        this.filedataContentType = filedataContentType;
    }

    public List<Map<String, Object>> getUsersmapList() {
        return usersmapList;
    }

    public void setUsersmapList(List<Map<String, Object>> usersmapList) {
        this.usersmapList = usersmapList;
    }

    public List<Map<String, Object>> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Map<String, Object>> commentList) {
        this.commentList = commentList;
    }

    public ArrayList<Map<String, Object>> getDutyArrayList() {
        return dutyArrayList;
    }

    public void setDutyArrayList(ArrayList<Map<String, Object>> dutyArrayList) {
        this.dutyArrayList = dutyArrayList;
    }

    public ArrayList<Map<String, Object>> getTaskArrayList() {
        return taskArrayList;
    }

    public void setTaskArrayList(ArrayList<Map<String, Object>> taskArrayList) {
        this.taskArrayList = taskArrayList;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public String getTodoTask() {
        return todoTask;
    }

    public void setTodoTask(String todoTask) {
        this.todoTask = todoTask;
    }

    public String getFinishTask() {
        return finishTask;
    }

    public void setFinishTask(String finishTask) {
        this.finishTask = finishTask;
    }

    public int getTodoNum() {
        return todoNum;
    }

    public void setTodoNum(int todoNum) {
        this.todoNum = todoNum;
    }

    public int getFinishNum() {
        return finishNum;
    }

    public void setFinishNum(int finishNum) {
        this.finishNum = finishNum;
    }

    public int getTotalTask() {
        return totalTask;
    }

    public void setTotalTask(int totalTask) {
        this.totalTask = totalTask;
    }

    public List<Map<String, Object>> getPostArrayList() {
        return postArrayList;
    }

    public void setPostArrayList(List<Map<String, Object>> postArrayList) {
        this.postArrayList = postArrayList;
    }

    public String getBusinessKeyList() {
        return businessKeyList;
    }

    public void setBusinessKeyList(String businessKeyList) {
        this.businessKeyList = businessKeyList;
    }

    public String getRepassword() {
        return repassword;
    }

    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }

    public FileManage getFileManage() {
        return fileManage;
    }

    public void setFileManage(FileManage fileManage) {
        this.fileManage = fileManage;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public List<String> getAdminType() {
        return adminType;
    }

    public void setAdminType(List<String> adminType) {
        this.adminType = adminType;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getNewId() {
        return newId;
    }

    public void setNewId(String newId) {
        this.newId = newId;
    }

    public String getValidPeriod() {
        return validPeriod;
    }

    public void setValidPeriod(String validPeriod) {
        this.validPeriod = validPeriod;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }
}
