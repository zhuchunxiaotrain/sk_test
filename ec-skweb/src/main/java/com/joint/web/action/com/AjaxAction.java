package com.joint.web.action.com;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.BaseEnum.StateEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.service.memcached.SpyMemcachedClient;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.joint.base.entity.Company;
import com.joint.base.entity.Duty;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.entity.system.Admin;
import com.joint.base.service.CompanyService;
import com.joint.base.service.UsersService;

import com.joint.core.service.*;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

@ParentPackage("com")
public class AjaxAction extends BaseFlowAction {

    private static final long serialVersionUID = 2332217693929363302L;

    @Resource
    private CompanyService companyService;

    @Resource
    private UsersService usersService;
    @Resource
    private SpyMemcachedClient spyMemcachedClient;

    private String flowName;
    private List<Duty> dutyList;

    private Admin admin;
    private Company org;
    private Users users;
    private int number;

    private Set<Users> usersSet;
    private Map<String,Object> businessData;
    private String param;
    private String depart;
    private String view_type;
    private String type;
    //上传文件使用
    protected File filedata;
    protected String filename;
    protected String filedataFileName;
    protected String filedataContentType;


    public String company(){
        org = usersService.getCompanyByUser();
        number= companyService.findUsersByCompany(org).size();
        admin = adminService.get(org.getAdminId());
        return "company";
    }

    public String organization() {
        org = usersService.getCompanyByUser();
        number = companyService.findUsersByCompany(org).size();
        admin = adminService.get(org.getAdminId());
        //读取所有的部门
        return "organization";
    }

    /**
     * 上传文件
     * @return
     * @throws java.io.IOException
     */
    public String uploadFile() throws IOException {
        Map<String, Object> data = new HashMap<String, Object>();
        FileManage fileManage;
        if(StringUtils.isNotEmpty(keyId)){
            fileManage = gridFSSave(new FileInputStream(filedata), filename, keyId,"APPLICATION/OCTET-STREAM");
        }else {
            fileManage = gridFSSave(new FileInputStream(filedata), filename);
        }
        data.put("fileId",fileManage.getId());
        data.put("state",200);
        data.put("name",fileManage.getName());
        data.put("url",fileManage.getUrl());
        data.put("msg","上传成功");
        return ajaxJson(JSONObject.fromObject(data).toString());
    }

    /**
     * 删除附件
     * @return
     */
    public String delFile(){
        if(StringUtils.isNotEmpty(keyId)){
            FileManage fileManage = fileManageService.get(keyId);
            fileManageService.delete(fileManage);
            return ajaxHtmlCallback("200", "删除成功！","操作状态");
        }else {
            return ajaxHtmlCallback("400", "请选择删除文件！","操作状态");
        }
    }
    public String fileList(){
        Map<String, Object> data = new HashMap<String, Object>();
        ArrayList<Map<String,Object>> dataRows=Lists.newArrayList();
        Map<String,Object> rMap ;
        if(StringUtils.isNotEmpty(keyId)){
            for(String fileId : StringUtils.split(keyId, ",")){
                FileManage fileManage = fileManageService.get(fileId.trim());
                if(fileManage == null){
                    continue;
                }
                rMap =  Maps.newHashMap();
                rMap.put("fileId",fileManage.getId());
                rMap.put("name",fileManage.getName());
                rMap.put("url",fileManage.getUrl());
                dataRows.add(JSONObject.fromObject(rMap));
            }
        }
        data.put("dataRows",dataRows);
        data.put("num",dataRows.size());
        data.put("state",200);
        return ajaxJson(JSONObject.fromObject(data).toString());
    }



    public String modeler(){return "modeler";}

    public String flow(){return "flow";}

    public String dict(){
        return "dict";
    }

    public String importFile(){
        return "importFile";
    }

    public String downloadFile(){
        return "import";
    }

    public String users() {
        return "users";
    }

    public String depart() {
        return "depart";
    }

    public String post() {
        return "post";
    }

    public String power() {
        return "power";
    }

    public String role() {
        return "role";
    }

    public String toDoList(){return  "toDoList"; }

    public String taskRecordList(){return "taskRecordList"; }

    public String draftList(){return "draftList"; }

    public String remindList(){return "remindList"; }

    public String systemSetting(){
        return ajaxHtmlCallback("400", "不提醒！","");
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getView_type() {
        return view_type;
    }

    public void setView_type(String view_type) {
        this.view_type = view_type;
    }

    public List<Duty> getDutyList() {
        return dutyList;
    }

    public void setDutyList(List<Duty> dutyList) {
        this.dutyList = dutyList;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Company getOrg() {
        return org;
    }

    public void setOrg(Company org) {
        this.org = org;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Map<String, Object> getBusinessData() {
        return businessData;
    }

    public void setBusinessData(Map<String, Object> businessData) {
        this.businessData = businessData;
    }

    public Set<Users> getUsersSet() {
        return usersSet;
    }

    public void setUsersSet(Set<Users> usersSet) {
        this.usersSet = usersSet;
    }



    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    @Override
    public File getFiledata() {
        return filedata;
    }

    @Override
    public void setFiledata(File filedata) {
        this.filedata = filedata;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public String getFiledataFileName() {
        return filedataFileName;
    }

    @Override
    public void setFiledataFileName(String filedataFileName) {
        this.filedataFileName = filedataFileName;
    }

    @Override
    public String getFiledataContentType() {
        return filedataContentType;
    }

    @Override
    public void setFiledataContentType(String filedataContentType) {
        this.filedataContentType = filedataContentType;
    }
}
