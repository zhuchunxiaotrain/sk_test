package com.joint.web.action.party;

import cn.jpush.api.report.UsersResult;
import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.TaskRecordService;
import com.joint.base.service.UsersService;
import com.joint.core.entity.StrongBig;
import com.joint.core.entity.manage.MeetingUse;
import com.joint.core.service.MeetingUseService;
import com.joint.core.service.StrongBigService;
import com.joint.core.service.NumberCreaterService;
import com.joint.web.action.BaseFlowAction;

import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import java.awt.*;
import java.security.AlgorithmConstraints;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Created by dqf on 2015/8/26.
 */
@ParentPackage("party")
public class AjaxStrongbigAction extends BaseFlowAction {

    @Resource
    private StrongBigService strongBigService;

    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private NumberCreaterService numberCreaterService;
    @Resource
    private TaskRecordService taskRecordService;
    @Resource
    private MeetingUseService meetingUseService;


    private StrongBig strongBig;

    protected ArrayList<Map<String,Object>> typesArrayList;

    private int numStatus;
    private Users loginUser;
    private String viewtype;

    //通知对象id和name
    private String tellUsersId;
    private String tellUsersName;

    //附件ID
    private String attchmentId;
    //相应会议记录对象
    private String meetingUseId;

    /**
     * 登记事宜名称
     */
    private String registerName;

    /**
     * 登记事宜名称
     */
    private String  name;
    /**
     *三重一大会议类型
     */
    private String type;
    /**
     * 是否需要提交中心审核
     */
    private String isCheck;
    /**
     * 备案
     */
    private String strongBigRecord;
    /**
     * 结论
     */
    private String conclusion;
    /**
     * 通知对象
     */
    private List<Users> tellUsers;
    /**
     * 相应会议记录
     */
    private List<MeetingUse> meetingUse;
    /**
     *申请人名称
     */
    private String applyUserName;

    /**
     * 三中一大附件
     */
    private List<FileManage> attchment;
    /**
     *三中一大文本
     */
    private String attchmentText;

    public String execute(){/*没有写方法名，默认执行的方法*/
        return "strongbig";
    }


    private void setData(){
        tellUsers = Lists.newArrayList();
        attchment=Lists.newArrayList();
        meetingUse=Lists.newArrayList();

        if(StringUtils.isNotEmpty(keyId)){
            strongBig = strongBigService.get(keyId);
        }else{
            strongBig = new StrongBig();
            strongBig.setCreater(usersService.getLoginInfo());/*设置创建者*/
        }


        if(StringUtils.isNotEmpty(tellUsersId)){
            for (String u:tellUsersId.split(",")){
                tellUsers.add(usersService.get(u.trim()));
            }
        }
        if(StringUtils.isNotEmpty(attchmentId)){
            for (String s:attchmentId.split(",")){
                attchment.add(fileManageService.get(s.trim()));
            }
        }
        //会议使用对象
        if(StringUtils.isNotEmpty(meetingUseId)){
            for (String s:meetingUseId.split(",")){
                meetingUse.add(meetingUseService.get(s.trim()));
            }
        }

        strongBig.setRegisterName(registerName);
        strongBig.setType(type);
        strongBig.setIsCheck(isCheck);
        strongBig.setStrongBigRecord(strongBigRecord);
        strongBig.setConclusion(conclusion);
        strongBig.setTellUsers(tellUsers);
        strongBig.setMeetingUse(meetingUse);
        strongBig.setAttchmentText(attchmentText);
        strongBig.setAttchment(attchment);
        Date date=new Date();
        strongBig.setCreateDate(date);
        strongBig.setYear(new SimpleDateFormat("yyyy").format(date));
        strongBig.setMonth(new SimpleDateFormat("MM").format(date));
        strongBig.setCompany(usersService.getLoginInfo().getCompany());
        strongBigService.save(strongBig);
    }

    public String listfordialog(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);

        pager=strongBigService.findByPagerAndProcessState(pager, user, "strongbig", FlowEnum.ProcessState.Finished, params);
        List<StrongBig> proInfoList;
        if (pager.getTotalCount() > 0){
            proInfoList = (List<StrongBig>) pager.getList();
        }else{
            proInfoList = new ArrayList<>();
        }
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(StrongBig strongBig:proInfoList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",strongBig.getId()!=null?strongBig.getId():"");
            rMap.put("name",strongBig.getName()!=null?strongBig.getName():"");
            rMap.put("year",strongBig.getYear()!=null?strongBig.getYear():"");
            rMap.put("month",strongBig.getMonth()!=null?strongBig.getMonth():"");
            rMap.put("state",strongBig.getProcessState().value()!=null?strongBig.getProcessState().value():"");

            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }


        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String list(){

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);

        params.put("initDuty", curDutyId);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);


        if(StringUtils.isNotEmpty(viewtype)){
            if(viewtype.equals("1")){
                //流转中
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed});
                pager=strongBigService.findByPagerAndLimit(false, "strongbig", pager, params);
            }else if(viewtype.equals("2")){
                //已归档
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished});
                pager=strongBigService.findByPagerAndFinish("strongbig", pager, params);
            }

        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=strongBigService.findByPagerAndLimit(true, "strongbig", pager, params);
        }

        List<StrongBig> strongBigList;
        if (pager.getTotalCount() > 0){

            strongBigList = (List<StrongBig>) pager.getList();
        }else{
            strongBigList = new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;

        for(StrongBig strongBig:strongBigList){
            rMap = new HashMap<String,Object>();
            rMap.put("id",strongBig.getId()!=null?strongBig.getId():"");
            rMap.put("registerName",strongBig.getRegisterName()!=null?strongBig.getRegisterName():"");
            if(strongBig.getCreateDate()!=null){
                String year=new SimpleDateFormat("yyyy").format(strongBig.getCreateDate());
                String month=new SimpleDateFormat("MM").format(strongBig.getCreateDate());
                rMap.put("year",year);
                rMap.put("month",month);
            }else{
                rMap.put("year","");
                rMap.put("month","");
            }
            rMap.put("processState",strongBig.getProcessState().value()!=null?strongBig.getProcessState().value():"");


            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);
        return  ajaxJson(JSONObject.fromObject(data).toString());

    }

    public String read(){
        loginUser = usersService.getLoginInfo();
        attchment=Lists.newArrayList();
        meetingUse=Lists.newArrayList();
        if(StringUtils.isNotEmpty(keyId)) {
            strongBig = strongBigService.get(keyId);/*根据keyid找到点击文件的id，找到对应的对象*/
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            //通知对象
            tellUsers=strongBig.getTellUsers();

            //附件对象
            if(strongBig.getAttchment()!=null){
                attchment=strongBig.getAttchment();
            }
            //相应会议记录对象
            if(strongBig.getMeetingUse()!=null){
                meetingUse=strongBig.getMeetingUse();
            }
        }
        return "read";
    }

    public String input(){
        loginUser = usersService.getLoginInfo();
        attchment=Lists.newArrayList();
        tellUsers=Lists.newArrayList();
        meetingUse=Lists.newArrayList();
        if (StringUtils.isNotEmpty(keyId)){
            strongBig = strongBigService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);

            //通知对象
            if(strongBig.getTellUsers()!=null){
                tellUsers=strongBig.getTellUsers();
            }
            //附件对象
            if(strongBig.getAttchment()!=null){
                attchment=strongBig.getAttchment();
            }
            //相应会议对象
            if(strongBig.getMeetingUse()!=null){
                meetingUse=strongBig.getMeetingUse();
            }

        } else {

        }
        return "input";
    }



    // 保存到草稿箱
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                strongBigService.update(strongBig);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                strongBigService.save(strongBig, "strongbig", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        setData();

        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        var2.put("initDuty", curDutyId);
        ArrayList<String> list =Lists.newArrayList();
        Set<Duty> dutySet= null;
        ArrayList<Department> departmentArrayList=null;
        dutySet=usersService.getLoginInfo().getDutySet();

        /*loop1:for(Duty duty:dutySet){//当前登录用户的所有职责（duty）
           // LogUtil.info("duty:"+duty.getId());
            Post post = duty.getPost();//根据当前登录用户的职责，得到当前登陆用户的岗位
            if(StringUtils.equals(post.getName(),"党务负责人")  == false && StringUtils.equals(post.getName(),"行政负责人") == false){
                continue;//进入当前登录人的下一个职权
            }
            Department dept = duty.getDepartment().getParent();//根据当前登陆人的职责，得到当前人的部门的上级部门
            if (dept == null) {//上级部门为空的话，进入当前人的下一个职责
                continue;
            }
            Set<Power> powers = dept.getPowerSet();//获取当前登陆人的职责，得到部门的部门的职权集合
            loop2:for(Power power:powers){//遍历姥姥级部门下的所有职权
             //   LogUtil.info("power:"+power.getId());
                Post post1 = power.getPost();
                if(post1.getName().equals("支部书记") == true) {
                    List<Users> usersList=dutyService.getPersons(dept, post1);//更具岗位对象获取该部门下的指定岗位下的 所有人
                    for(Users users1:usersList){
                        LogUtil.info("userName:"+users1.getName());
                        list.add(users1.getId());
                    }
                    break loop1;
                }
            }
        }
        var2.put("approvers",list);*/
        if(StringUtils.isNotEmpty(curDutyId)){
            Duty duty=dutyService.get(curDutyId);
            //所选择的部门属于二级部门
            if(duty.getDepartment().getParent()!=null){
                Department dept = duty.getDepartment().getParent();//根据当前登陆人的职责，得到当前人的部门的上级部门
                Set<Power> powers = dept.getPowerSet();//获取当前登陆人的职责，得到部门的部门的职权集合
                loop2:for(Power power:powers){//遍历姥姥级部门下的所有职权
                    Post post1 = power.getPost();
                    if(post1.getName().equals("支部书记") == true) {
                        List<Users> usersList=dutyService.getPersons(dept, post1);//更具岗位对象获取该部门下的指定岗位下的 所有人
                        for(Users users1:usersList){
                            list.add(users1.getId());
                        }
                    }
                }
            }else{//所选择的部门属于一级部门
                Department dept = duty.getDepartment();//根据当前登陆人的职责，得到当前人的部门的上级部门
                Set<Power> powers = dept.getPowerSet();//获取当前登陆人的职责，得到部门的部门的职权集合
                loop2:for(Power power:powers){//遍历姥姥级部门下的所有职权
                    Post post1 = power.getPost();
                    if(post1.getName().equals("支部书记") == true) {
                        List<Users> usersList=dutyService.getPersons(dept, post1);//更具岗位对象获取该部门下的指定岗位下的 所有人
                        for(Users users1:usersList){
                            list.add(users1.getId());
                        }
                    }
                }
            }
            var2.put("approvers",list);
        }


        var2.put("isCheck", isCheck);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                    strongBigService.approve(strongBig, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            } else {
                    strongBigService.commit(strongBig, "strongbig", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批
    public String approve1(){

        strongBig = strongBigService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();


        var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                if(isCheck.equals("1")){
                    var1.put("numStatus", 2);
                    strongBigService.approve(strongBig, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
                }else{
                    strongBigService.approve(strongBig, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
                    Task overtask = workflowService.getCurrentTask(keyId);
                    if(overtask==null){
                        StrongBig entity=strongBigService.get(keyId);
                        entity.setProcessState(FlowEnum.ProcessState.Finished);
                        strongBigService.update(entity);

                        List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(entity.getId());
                        for(TaskRecord taskRecord:taskRecords){
                            taskRecord.setType(2);
                            taskRecordService.update(taskRecord);
                        }
                    }else{
                        if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){
                            runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 2);
                        }
                    }


                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }
    //
    public String approve2() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        strongBig = strongBigService.get(keyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 3);
        var2.put("curDutyId", curDutyId);
        try {
            strongBigService.approve(strongBig, FlowEnum.ProcessState.Finished, var2, curDutyId, comment);

            List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(strongBig.getId());
            for(TaskRecord taskRecord:taskRecords){
                taskRecord.setType(2);
                taskRecordService.update(taskRecord);
            }

        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String approve3() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
            strongBig = strongBigService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 4);
        var2.put("curDutyId", curDutyId);
        try {
            strongBigService.approve(strongBig, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        LogUtil.info("startreject");
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            strongBig = strongBigService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("manageproinfo");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            System.out.println("comment:"+comment);
            LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            strongBigService.reject(strongBig, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        LogUtil.info("startdeny");
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            strongBig = strongBigService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("manageproinfo");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:"+key);
            if(com.joint.base.util.StringUtils.isEmpty(comment)){
                comment="";
            }
            strongBigService.deny(strongBig, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }





    public ArrayList<Map<String, Object>> getTypesArrayList() {
        return typesArrayList;
    }

    public void setTypesArrayList(ArrayList<Map<String, Object>> typesArrayList) {
        this.typesArrayList = typesArrayList;
    }


    @Override
    public int getNumStatus() {
        return numStatus;
    }
    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public Users getLoginUser() {
        return loginUser;
    }
    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getViewtype() {
        return viewtype;
    }
    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public StrongBig getStrongBig() {
        return strongBig;
    }

    public void setStrongBig(StrongBig strongBig) {
        this.strongBig = strongBig;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public List<Users> getTellUsers() {
        return tellUsers;
    }

    public void setTellUsers(List<Users> tellUsers) {
        this.tellUsers = tellUsers;
    }

    public String getMeetingUseId() {
        return meetingUseId;
    }

    public void setMeetingUseId(String meetingUseId) {
        this.meetingUseId = meetingUseId;
    }

    public List<MeetingUse> getMeetingUse() {
        return meetingUse;
    }

    public void setMeetingUse(List<MeetingUse> meetingUse) {
        this.meetingUse = meetingUse;
    }

    public String getTellUsersId() {
        return tellUsersId;
    }

    public void setTellUsersId(String tellUsersId) {
        this.tellUsersId = tellUsersId;
    }

    public String getTellUsersName() {
        return tellUsersName;
    }

    public void setTellUsersName(String tellUsersName) {
        this.tellUsersName = tellUsersName;
    }



    public String getApplyUserName() {
        return applyUserName;
    }

    public void setApplyUserName(String applyUserName) {
        this.applyUserName = applyUserName;
    }

    public String getAttchmentId() {
        return attchmentId;
    }

    public void setAttchmentId(String attchmentId) {
        this.attchmentId = attchmentId;
    }

    public String getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(String isCheck) {
        this.isCheck = isCheck;
    }

    public List<FileManage> getAttchment() {
        return attchment;
    }

    public void setAttchment(List<FileManage> attchment) {
        this.attchment = attchment;
    }

    public String getAttchmentText() {
        return attchmentText;
    }

    public void setAttchmentText(String attchmentText) {
        this.attchmentText = attchmentText;
    }

    public String getRegisterName() {
        return registerName;
    }

    public void setRegisterName(String registerName) {
        this.registerName = registerName;
    }

    public String getStrongBigRecord() {
        return strongBigRecord;
    }


    public void setStrongBigRecord(String strongBigRecord) {
        this.strongBigRecord = strongBigRecord;
    }

}

