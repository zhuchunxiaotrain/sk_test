package com.joint.web.action.party;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.DutyService;
import com.joint.base.service.RoleService;
import com.joint.base.service.UsersService;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.*;
import com.joint.core.entity.SelectionSpeech;
import com.joint.core.service.*;
import com.joint.core.service.SelectionSpeechService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dqf on 2015/8/26.
 */
@ParentPackage("party")
public class AjaxSelectionspeechAction extends BaseFlowAction {
    
    @Resource
    private SelectionSpeechService selectionSpeechService;
    @Resource
    private SelectionApplyService selectionApplyService;
    @Resource
    private  SelectionNoticeService selectionNoticeService;
    @Resource
    private SelectionPublicityService selectionPublicityService;
    @Resource
    private DutyService dutyService;

    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private RoleService roleService;


    private SelectionSpeech selectionSpeech;
    private Power power;
    private Duty duty;



    private int numStatus;
    private Users loginUser;

    //前台传到后台的数据
    private String speechAttchmentId;
    private String checkUsersId;



    //从后台传回前台的数据
    private String attchmentFileId;
    private  boolean flag;// 权限


    //申请人信息
   private Users applyUser;



    /**
     * 公告对象
     */
    private SelectionNotice selectionNotice;
    /**
     * 报名对象
     */
    private SelectionApply selectionApply;
    /**
     * 附件上传
     */
    private List<FileManage>  attchmentList;
    /**
     * 附加文本
     */
    private String attchmentText;
    /**
     * 结果
     */
    private String result;
    /**
     * 操作人
     * @return
     */
    private Users approver;
    /**
     * 操作人审批时间
     * @return
     */
    private Date approverDate;

    private String viewtype;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        Users user = usersService.getLoginInfo();
        Set<Duty> duties= user.getDutySet();
        List<Post> postList=Lists.newArrayList();
        List<Power> powerList=Lists.newArrayList();
        flag =false;
        for(Duty duty:duties){
            powerList.add(duty.getPower());
            postList.add(duty.getPost()) ;
        }
        for(Power power:powerList){
            if(power.getName().equals("产业中心党政综合办-党务负责人")){
                flag=true;
                break;
            }
        }
        /*for(Post p:postList){
            if(p.getName().equals("党办负责人")){
                flag=true;
                break;
            }
        }*/
        return "selectionspeech";
    }

    /**
     * 更新数据
     * @return
     */
    public String update(){
        setData();
        try {
                selectionSpeechService.update(selectionSpeech);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "更新成功！", "操作状态");
    }
    public String listfordialog(){

        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);
        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }


        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();

        //公告对象
        selectionNotice=new SelectionNotice();
        if(StringUtils.isNotEmpty(parentId)){
            String[] str=parentId.split(",");
            for(int i=0;i<str.length-1;i++){
                if(str[i].trim()==str[i+1].trim()){
                    str[i] = "";
                }
            }
            for(int j=0;j<str.length;j++){
                if(str[j] != ""){
                    selectionNotice=selectionNoticeService.get(str[j].trim());
                }
            }
            params.put("selectionNotice",selectionNotice);
        }

        params.put("result","1");
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);


        params = getSearchFilterParams(_search,params,filters);
        pager=selectionSpeechService.findByPagerAndCompany(pager,null,com,params);
        pager.getList();
        List<SelectionSpeech> selectionSpeechList;
        if (pager.getTotalCount() > 0){
            selectionSpeechList = (List<SelectionSpeech>) pager.getList();
        }else{
            selectionSpeechList = new ArrayList<>();
        }

        /**
         * 查询已经结果公示的人
         */
        Map<String,Object> params1 = new HashMap<String,Object>();
        params1.put("company",com);
        params1.put("state",BaseEnum.StateEnum.Enable);
        params1.put("selectionNotice",selectionNotice);
        params1.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Draft});
        params1.put("result",new String []{"1","0"});

        List<SelectionPublicity> selectionPublicityList=selectionPublicityService.getList(params1);
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        List<SelectionSpeech> selectionSpeechList1=Lists.newArrayList();
        //找到重复的数据
        if(selectionPublicityList.size()>0&&selectionPublicityList!=null){
            loop1:for(SelectionSpeech selectionSpeech:selectionSpeechList){
                 loop2:for (SelectionPublicity selectionPublicity:selectionPublicityList){
                    if(selectionPublicity.getSelectionSpeech()==selectionSpeech){
                       selectionSpeechList1.add(selectionSpeech);
                    }else{
                        continue loop2;
                    }
                }
            }
        }else{
            for(SelectionSpeech selectionSpeech:selectionSpeechList){
                    rMap = new HashMap<String,Object>();
                    rMap.put("id",selectionSpeech.getId()!=null?selectionSpeech.getId():"");
                    rMap.put("name",selectionSpeech.getSelectionApply().getCreater()!=null?selectionSpeech.getSelectionApply().getCreater().getName():"");
                    if(selectionSpeech.getResult()!=null){
                        if(selectionSpeech.getResult().equals("0")){
                            rMap.put("result","未审批");
                        }else if(selectionSpeech.getResult().equals("1")){
                            rMap.put("result","通过");
                        }else if(selectionSpeech.getResult().equals("2")) {
                            rMap.put("result","未通过");
                        }
                    }
                    rMap.put("post",selectionSpeech.getSelectionNotice()!=null?selectionSpeech.getSelectionNotice().getPost().getName():"");
                    JSONObject o = JSONObject.fromObject(rMap);
                    dataRows.add(o);
            }
        }


        //去除重复的数据
        if(selectionPublicityList.size()>0&&selectionPublicityList!=null){

            if(selectionSpeechList.removeAll(selectionSpeechList1)){
                for(SelectionSpeech selectionSpeech:selectionSpeechList){
                    rMap = new HashMap<String,Object>();
                    rMap.put("id",selectionSpeech.getId()!=null?selectionSpeech.getId():"");
                    rMap.put("name",selectionSpeech.getSelectionApply().getCreater()!=null?selectionSpeech.getSelectionApply().getCreater().getName():"");
                    if(selectionSpeech.getResult()!=null){
                        if(selectionSpeech.getResult().equals("0")){
                            rMap.put("result","未审批");
                        }else if(selectionSpeech.getResult().equals("1")){
                            rMap.put("result","通过");
                        }else if(selectionSpeech.getResult().equals("2")) {
                            rMap.put("result","未通过");
                        }
                    }
                    rMap.put("post",selectionSpeech.getSelectionNotice()!=null?selectionSpeech.getSelectionNotice().getPost().getName():"");
                    JSONObject o = JSONObject.fromObject(rMap);
                    dataRows.add(o);
                }
            }
        }



        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String list(){
        List<Power> powerList=Lists.newArrayList();
        //List<Post> postList=Lists.newArrayList();
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }

        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        Set<Duty> duties=user.getDutySet();
        for(Duty duty:duties){//一个人只有一个职责成立
            powerList.add(duty.getPower());
        }
        for(Power power:powerList){
            if(power.getName().equals("产业中心党政综合办-党务负责人")){
                break;
            }else{
                if(!params.containsKey("creater")){
                    params.put("creater",usersService.getLoginInfo());
                }
            }

        }



        //公告对象
        selectionNotice=new SelectionNotice();
        if(StringUtils.isNotEmpty(parentId)){
            String[] str=parentId.split(",");
            for(int i=0;i<str.length-1;i++){
                if(str[i].trim()==str[i+1].trim()){
                    str[i] = "";
                }
            }
            for(int j=0;j<str.length;j++){
                if(str[j] != ""){
                    selectionNotice=selectionNoticeService.get(str[j].trim());
                }
            }
            params.put("selectionNotice",selectionNotice);
        }
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        pager=selectionSpeechService.findByPagerAndCompany(pager,null,com,params);
        List<SelectionSpeech> selectionSpeechsList=(List<SelectionSpeech>)pager.getList();

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(SelectionSpeech selectionSpeech:selectionSpeechsList){

            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            rMap=new HashMap<>();
            rMap.put("id",selectionSpeech.getId()!=null?selectionSpeech.getId():"");
            rMap.put("creater",selectionSpeech.getSelectionApply()!=null?selectionSpeech.getSelectionApply().getCreater().getName():"");
            rMap.put("createDate",selectionSpeech.getSelectionApply()!=null?simpleDateFormat.format(selectionSpeech.getSelectionApply().getCreateDate()):"");
            rMap.put("speechState",selectionSpeech.getSpeechState()!=null?selectionSpeech.getSpeechState():"");
            if(selectionSpeech.getResult()!=null){
                if(selectionSpeech.getResult().equals("0")){
                    rMap.put("result","未审批");
                }else if(selectionSpeech.getResult().equals("1")){
                    rMap.put("result","通过");
                }else if(selectionSpeech.getResult().equals("2")){
                    rMap.put("result","不通过");
                }
            }

            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }

        data.put("dataRows",dataRows);
        data.put("curDutyId",curDutyId);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);
        data.put("flag",flag);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }
    public Map getSearchFilterParams(String _search,Map<String,Object> params,String filters){
        //createAlias Key集合
        List<String> alias = new ArrayList<String>();
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(_search)){
            boolean __search = Boolean.parseBoolean(_search);
            if(__search&& org.apache.commons.lang3.StringUtils.isNotEmpty(filters)){
                JSONObject filtersJson = JSONObject.fromObject(filters);
                if(org.apache.commons.lang3.StringUtils.equals(filtersJson.getString("groupOp"), "AND")){
                    //查询条件
                    JSONArray rulesJsons = filtersJson.getJSONArray("rules");
                    //遍历条件
                    for(int i=0;i<rulesJsons.size();i++){
                        JSONObject rule = rulesJsons.getJSONObject(i);
                        if(rule.has("field")&& org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("field"))&&rule.has("data")&& org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                            String field = rule.getString("field");
                            if(field.contains("_")){
                                String[] fields = field.split("_");
                                if(org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                                    params.put(field.replaceAll("_", "."), rule.getString("data"));
                                }
                            }else{
                                if(org.apache.commons.lang3.StringUtils.isNotEmpty(rule.getString("data"))){
                                    if(field.equals("result")){
                                        String result=rule.getString("data");
                                        if(result.equals("通过")){
                                            params.put("result", "1");
                                        }else if(result.equals("不通过")){
                                            params.put("result","2");
                                        }else if(result.equals("未审批")){
                                            params.put("result","0");
                                        } else {//输入错误
                                            params.put("result", new String[]{"0","1","2"});

                                        }
                                    }

                                    if(field.equals("createDate")){
                                        params.put("createDate", DataUtil.StringToDate(rule.getString("data")));
                                    }
                                    /*else {
                                        params.put(field, rule.getString("data"));
                                    }*/
                                }
                            }
                        }
                    }

                }
            }
        }

        return params;
    }
    public String read(){

        loginUser = usersService.getLoginInfo();
        applyUser=new Users();
        approver=new Users();
        if(StringUtils.isNotEmpty( keyId)) {
            Company company = usersService.getCompanyByUser();
            selectionSpeech = selectionSpeechService.get(keyId);
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            //竞聘人员
            if(selectionSpeech.getCreater()!=null){
                applyUser=selectionSpeech.getCreater();
            }
            //操作人员
            if(selectionSpeech.getApprover()!=null){
                approver=selectionSpeech.getApprover();
            }
            //报名对象
            if(selectionSpeech.getSelectionApply()!=null){
                selectionApply=selectionSpeech.getSelectionApply();

            }
            //附件
            if(selectionSpeech.getAttchmentList()!=null){
                attchmentList=selectionSpeech.getAttchmentList();
            }
        }
        return "read";
    }

    public String input(){
        attchmentFileId="";//变量从后台传输数据到前台，变量都要初始值，如果不赋值，对象的默认值是null值


        if (StringUtils.isNotEmpty(keyId)){
            selectionSpeech = selectionSpeechService.get(keyId);
            Company company = usersService.getCompanyByUser();





        } else {

        }



        return "input";
    }

    private void setData(){
        attchmentList=Lists.newArrayList();
        selectionSpeech=new SelectionSpeech();
        if(StringUtils.isNotEmpty(keyId)){
            selectionSpeech=selectionSpeechService.get(keyId);

            //上传附件
            if(speechAttchmentId!=null){
                for (String s:speechAttchmentId.split(",")){
                    attchmentList.add(fileManageService.get(s.trim()));
                }
            }
            //操作人
            Users users=usersService.getLoginInfo();
            selectionSpeech.setResult(result);
            selectionSpeech.setAttchmentList(attchmentList);
            selectionSpeech.setAttchmentText(attchmentText);
            selectionSpeech.setApprover(users);
            selectionSpeech.setApproverDate(new Date());
        }else{
            selectionSpeech.setCreater(usersService.getLoginInfo());
            selectionSpeechService.save(selectionSpeech);
            selectionSpeech.setCompany(usersService.getLoginInfo().getCompany());

        }
    }

    // 保存
    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                selectionSpeechService.update(selectionSpeech);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                selectionSpeechService.save(selectionSpeech, "selectionspeech", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    // 提交
    public String commit(){
        setData();
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        var2.put("initDuty", curDutyId);
        ArrayList<String> list = new ArrayList<String>();
        if(StringUtils.isNotEmpty(checkUsersId)){
            for (String uid: checkUsersId.split(",")) {
                list.add(uid.trim());
            }
        }

        var2.put("approvers", list);    // 会签人Ids
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                selectionSpeechService.approve(selectionSpeech, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            } else {
                selectionSpeechService.commit(selectionSpeech, "selectionspeech", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //审批
    public String approve1(){
        selectionSpeech = selectionSpeechService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();

        var1.put("numStatus", 2);
        //  var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                selectionSpeechService.approve(selectionSpeech, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }
    // 会签
    public String approve2() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        Map<String, Object> var2 = new HashMap<String, Object>();

        selectionSpeech = selectionSpeechService.get(keyId);
        /*Task task=workflowService.getCurrentTask(keyId, usersService.getLoginInfo());
      

        int multComplete = (Integer)runtimeService.getVariable(task.getExecutionId(),"nrOfCompletedInstances");
        int nrOfInstances = (Integer)runtimeService.getVariable(task.getExecutionId(),"nrOfInstances");
        LogUtil.info("multComplete:"+multComplete);
        LogUtil.info("nrOfInstances:"+nrOfInstances);

        if((double)multComplete/nrOfInstances>=1){
            var2.put("numStatus", 2);
            LogUtil.info("do:numStatus2");
        } else {
            var2.put("numStatus", 1);
            LogUtil.info("do:numStatus1");
        }*/

        //  var2.put("curDutyId", curDutyId);
        try {

            selectionSpeechService.approve(selectionSpeech, FlowEnum.ProcessState.Running, var2, curDutyId, comment);

            Task overtask = workflowService.getCurrentTask(keyId);
            if(overtask==null){/*没有task就到了归档，所以*/
                SelectionSpeech selectionSpeech=selectionSpeechService.get(keyId);
                selectionSpeech.setProcessState(FlowEnum.ProcessState.Finished);
                selectionSpeechService.save(selectionSpeech);

            }else {
                if(runtimeService.getVariable(overtask.getExecutionId(),"nrOfCompletedInstances")==null){
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 3);
                }
            }
    } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String approve3() {
        if(StringUtils.isEmpty(keyId))return ajaxHtmlCallback("404", "文档ID未找到！", "操作状态");
        if(StringUtils.isEmpty(comment)) comment="";
        selectionSpeech = selectionSpeechService.get(keyId);

        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 4);
        //  var2.put("curDutyId", curDutyId);
        try {
            selectionSpeechService.approve(selectionSpeech, FlowEnum.ProcessState.Finished, var2, curDutyId,comment);
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            selectionSpeech = selectionSpeechService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("selectionspeech");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            System.out.println("comment:"+comment);
            LogUtil.info("numStatus:"+numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            selectionSpeechService.reject(selectionSpeech, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (com.joint.base.util.StringUtils.isNotEmpty(keyId)) {
            selectionSpeech = selectionSpeechService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("selectionspeech");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:"+key);
            if(com.joint.base.util.StringUtils.isEmpty(comment)){
                comment="";
            }
            selectionSpeechService.deny(selectionSpeech, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }



    public SelectionSpeech getSelectionSpeech() {
        return selectionSpeech;
    }

    public void setSelectionSpeech(SelectionSpeech selectionSpeech) {
        this.selectionSpeech = selectionSpeech;
    }
    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getViewtype() {
        return viewtype;
    }
    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }


    public Users getApplyUser() {
        return applyUser;
    }

    public void setApplyUser(Users applyUser) {
        this.applyUser = applyUser;
    }

    public Users getApprover() {
        return approver;
    }

    public void setApprover(Users approver) {
        this.approver = approver;
    }

    public String getSpeechAttchmentId() {
        return speechAttchmentId;
    }

    public void setSpeechAttchmentId(String speechAttchmentId) {
        this.speechAttchmentId = speechAttchmentId;
    }

    public String getCheckUsersId() {
        return checkUsersId;
    }

    public void setCheckUsersId(String checkUsersId) {
        this.checkUsersId = checkUsersId;
    }

    public String getAttchmentFileId() {
        return attchmentFileId;
    }

    public void setAttchmentFileId(String attchmentFileId) {
        this.attchmentFileId = attchmentFileId;
    }

    public SelectionApply getSelectionApply() {
        return selectionApply;
    }

    public void setSelectionApply(SelectionApply selectionApply) {
        this.selectionApply = selectionApply;
    }

    public List<FileManage> getAttchmentList() {
        return attchmentList;
    }

    public void setAttchmentList(List<FileManage> attchmentList) {
        this.attchmentList = attchmentList;
    }

    @Override
    public String getResult() {
        return result;
    }

    @Override
    public void setResult(String result) {
        this.result = result;
    }

    public SelectionNotice getSelectionNotice() {
        return selectionNotice;
    }

    public void setSelectionNotice(SelectionNotice selectionNotice) {
        this.selectionNotice = selectionNotice;
    }

    public Power getPower() {
        return power;
    }

    public void setPower(Power power) {
        this.power = power;
    }

    public Duty getDuty() {
        return duty;
    }

    public void setDuty(Duty duty) {
        this.duty = duty;
    }

    public String getAttchmentText() {
        return attchmentText;
    }

    public void setAttchmentText(String attchmentText) {
        this.attchmentText = attchmentText;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}


