package com.joint.web.action.com;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.Company;
import com.joint.base.entity.Department;
import com.joint.base.entity.FileManage;
import com.joint.base.entity.Users;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.util.DataUtil;
import com.joint.base.util.StringUtils;
import com.joint.core.entity.Employees;
import com.joint.core.entity.Family;
import com.joint.core.service.EmployeesService;
import com.joint.core.service.EmploymentService;
import com.joint.core.service.FamilyService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by root on 16/12/16.
 */
@ParentPackage("com")
public class AjaxExpiringAction extends BaseFlowAction {

    @Resource
    private EmployeesService employeesService;
    @Resource
    private EmploymentService employmentService;
    @Resource
    private DictService dictService;
    @Resource
    private FamilyService familyService;
    private Users loginUser;
    private Employees employees;
    private String name;
    private String nature;
    private String startDate;
    private String endDate;
    private String trialStart;
    private String trialEnd;
    private Department dep;
    //private Dict post;

    /**
     * 视图类型
     */
    private String viewtype;

    /**
     * 项目视图
     * @return
     */
    public String execute(){
        return "expiring";
    }

    /**
     * 即将到期员工
     * @return
     */
    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("createDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();

        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        params.put("ifJob","0");

        //所有单子
        params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
        pager=employeesService.findByPagerAndLimit(true, "employees", pager, params);

        List<Employees> eList;
        if (pager.getTotalCount() > 0){
            eList = (List<Employees>) pager.getList();
        }else{
            eList = new ArrayList<>();
        }
        LogUtil.info("elist:" + eList);
        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        JSONObject o;
        for(Employees e:eList){
            rMap = new HashMap<String,Object>();

            rMap.put("id",e.getId());
            rMap.put("name",e.getName());
            rMap.put("age",e.getAge());
            rMap.put("gender",e.getGender());
            rMap.put("startDate",e.getStartDate());
            rMap.put("endDate",e.getEndDate());
            //LogUtil.info("e.getManage():" + e.getManage().getInterviewSet());
            rMap.put("post",e.getEmpPost()==null?"":e.getEmpPost().getName());
            rMap.put("dep",e.getDep()==null?"":e.getDep().getName());
            String nature = e.getNature();
            if(nature.equals("0")){
                rMap.put("nature","正式");
            }else if(nature.equals("1")){
                rMap.put("nature","试用");
            }else if(nature.equals("2")){
                rMap.put("nature","聘用");
            }
            if(StringUtils.equals("0",e.getNature())){
                if(DataUtil.DateDiff(e.getEndDate(),new Date())<=90){
                    o = JSONObject.fromObject(rMap);
                    dataRows.add(o);
                }
            }
            if(StringUtils.equals("1",e.getNature())){
                if(DataUtil.DateDiff(e.getTrialEnd(),new Date())<=30){
                    o = JSONObject.fromObject(rMap);
                    dataRows.add(o);
                }
            }


        }
        LogUtil.info("dataRows"+dataRows);
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records",recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }


    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTrialStart() {
        return trialStart;
    }

    public void setTrialStart(String trialStart) {
        this.trialStart = trialStart;
    }

    public String getTrialEnd() {
        return trialEnd;
    }

    public void setTrialEnd(String trialEnd) {
        this.trialEnd = trialEnd;
    }

    public Department getDep() {
        return dep;
    }

    public void setDep(Department dep) {
        this.dep = dep;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }
}


