package com.joint.web.action.com;


import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Result;
import com.fz.us.base.util.LogUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.joint.base.entity.*;
import com.joint.base.service.*;
import com.joint.base.service.activiti.WorkflowService;
import com.joint.base.util.FileUtil;
import com.joint.base.util.StringUtils;
import com.joint.base.util.WorkflowUtils;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.impl.bpmn.behavior.MultiInstanceActivityBehavior;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.task.TaskDefinition;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by hpj on 2014/9/16.
 */
@ParentPackage("com")
public class AjaxConfigAction extends BaseFlowAction {

    @Resource
    private WorkflowService workflowService;
    @Resource
    private UsersService usersService;
    @Resource
    private FileManageService fileManageService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private PostService postService;
    @Resource
    private BusinessConfigService businessConfigService;
    @Resource
    private ProcessConfigService processConfigService;
    @Resource
    private RoleService roleService;
    @Resource
    private PowerService powerService;
    @Resource
    private TaskService taskService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private CommonConfigService commonConfigService;

    private Set<Role> roleSet;
    private Set<Power> powerSet;
    private Set<Department> departmentSet;
    private Set<Post> postSet;

    private String createId;
    private String archReaderId;
    private String archEditorId;
    private String docReaderId;

    private Map<String, Object> properties;

    private ExecutionEntity executionEntity;
    private ProcessDefinition processDefinition;
    private BusinessConfig businessConfig;
    private ProcessConfig processConfig;
    private TaskDefinition taskDefinition;
    private MultiInstanceActivityBehavior multiBehavior;

    private String processName;
    private String processKey;

    //所有角色
    private String createIds;
    private String readIds;
    private String editIds;
    private String docReadIds;

    //所有部门
    private String create_selectIds;
    private String read_selectIds;
    private String edit_selectIds;
    private String docRead_selectIds;

    //所有岗位
    private String create_p_selectIds;
    private String read_p_selectIds;
    private String edit_p_selectIds;
    private String docRead_p_selectIds;

    //所有职权
    private String create_power_ids;
    private String read_power_ids;
    private String edit_power_ids;
    private String docRead_power_ids;


    //业务配置
    private CommonConfig commonConfig;
    private CommonConfig creadConfig;
    private CommonConfig ceditConfig;
    private CommonConfig ccreateConfig;
    private CommonConfig cdocConfig;

    //Process配置表Id
    private String roleId;
    private String departId;
    private String postId;
    private String powerId;

    private String filePath;
    private String cselectType;
    private String rselectType;
    private String eselectType;
    private String dselectType;
    private String selectType;
    private String activityId;
    private String processDefinitionId;
    private String name;
    private int version;

    private String nodeType;
    private int ptype;
    private int otype;
    private Expression assExpre;
    private List<String> type;
    private List<String> actionType;
    private List<Map<String,Object>> tablekeyMap;
    private String tablekey;
    private List<Map<String,Object>> fieldMap;

    private String fieldName;
    private String fields;


    private String dealType;
    private String doType;
    private String triggleTime;

    /**
     * 保存业务配置
     *
     * @return
     */
    public String save() {

        if (StringUtils.isNotEmpty(createId)) ccreateConfig = commonConfigService.get(createId);

        if (StringUtils.isNotEmpty(archReaderId)) creadConfig = commonConfigService.get(archReaderId);

        if (StringUtils.isNotEmpty(archEditorId)) ceditConfig = commonConfigService.get(archEditorId);

        if (StringUtils.isNotEmpty(docReaderId)) cdocConfig = commonConfigService.get(docReaderId);

        if (StringUtils.isEmpty(keyId)) return ajaxHtmlCallback("404", "找不到文档Id,请联系管理员", "操作状态");

        BusinessConfig businessCfg = businessConfigService.getBusinessConfigByDefintionId(keyId);
        if (businessCfg == null){
            businessCfg = new BusinessConfig();
        }

        if(businessCfg.getId()!=null && businessCfg.getPtype()!=ptype && ptype==1){
            businessCfg=new BusinessConfig();
            BusinessConfig newConfig=businessCfg;
            businessCfg.setVersion(newConfig.getVersion()+1);
        }

        if(StringUtils.isNotEmpty(tablekey)){
            businessCfg.setTableKey(tablekey);
        }

        businessCfg.setOtype(otype);
        businessCfg.setPtype(ptype);
        businessCfg.setProcessDefinitionId(keyId);
        businessCfg.setCreateConfig(ccreateConfig);
        businessCfg.setReadConfig(creadConfig);
        businessCfg.setEditConfig(ceditConfig);
        businessCfg.setDocConfig(cdocConfig);
        businessCfg.setCompany(usersService.getCompanyByUser());
        businessCfg.setCreater(usersService.getLoginInfo());


//        Date triggle=null;
//        Date now = new Date();
//        now = DataUtil.StringToDate(DataUtil.DateToString(now));

//        if(StringUtils.isNotEmpty(triggleTime)){
//            triggle = DataUtil.StringToDate(triggleTime);
//            businessCfg.setTriggleTime(triggle);
//        }
//        if(DataUtil.DateDiff(triggle,now)>0){
//            businessConfigService.cacheSave(businessCfg,AjaxConfigAction.class);
//        }

//        if (DataUtil.isSameDay(triggle,now)){
            if (StringUtils.isEmpty(businessCfg.getId())) {
                businessConfigService.save(businessCfg);
            }else {
                if(businessCfg.getPtype()!=ptype  && ptype==1){
                    businessConfigService.save(businessCfg);
                }else{
                    businessConfigService.updateAndEnable(businessCfg);
                }
            }
//        }
        return ajaxHtmlCallback("200", "保存成功", "操作状态");
    }

    /**
     * quartz 执行保存操作
     */
//    public void jobSave(String keyId){
//        businessConfigService.jobSave(keyId);
//        LogUtil.info(" { quartz } jobSave");
//    }
    /**
     * 保存流程配置信息
     *
     * @return
     */
    public String processSave() {
        if (StringUtils.isEmpty(keyId)) {
            ProcessConfig processCfg = processConfigService.findConfigByActivityId(processDefinitionId, activityId);

            processConfig = new ProcessConfig();
            ccreateConfig = new CommonConfig();

            if (processCfg != null) {
                processConfig = processCfg;
                if(processConfig.getCommonConfig()!=null)ccreateConfig = processConfig.getCommonConfig();
            }
            BusinessConfig businessConfig = businessConfigService.getBusinessConfigByDefintionId(processDefinitionId);
            processConfig.setVariable(fields);
            processConfig.setProcessDefinitionId(processDefinitionId);
            processConfig.setActivityId(activityId);
            processConfig.setState(BaseEnum.StateEnum.Enable);
            processConfig.setBusinessConfig(businessConfig);
            processConfig.setType(getCtype(type));
            processConfig.setActionType(getCtype(actionType));

            ccreateConfig.setRoleSet(getRoleSpit(roleId));
            ccreateConfig.setPowerSet(getPowerSpit(powerId));
            ccreateConfig.setDepartmentSet(getDepartSpit(departId));
            ccreateConfig.setPostSet(getPostSpit(postId));

            processConfig.setCommonConfig(ccreateConfig);
            commonConfigService.updateAndEnable(ccreateConfig);
            processConfigService.updateAndEnable(processConfig);
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String processInput() throws IOException {
        if (StringUtils.isNotEmpty(keyId)) {
            businessConfig = businessConfigService.getBusinessConfigByDefintionId(keyId);
            if (businessConfig == null) {
                return ajaxHtmlCallback("400", "请先保存业务配置", "操作状态");
            }
            processDefinition = workflowService.findProDefinitionById(keyId);
            List<FileManage> fileManageList = fileManageService.getFileByKeyId(keyId);
            if (fileManageList.size() > 0) {
                FileManage fileManage = fileManageList.get(0);
                filePath = fileManage.getUrl();
                return "processInput";
            }

            String path = "/resource/bpm/images";
            //获取绝对路径
            String exportDir = ServletActionContext.getServletContext().getRealPath(path);
            processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(keyId).singleResult();
            if(processDefinition == null){
                return ajaxHtmlCallback("200", "查找不到当前流程请联系管理员！","操作状态");
            }
            String diagramResourceName = processDefinition.getDiagramResourceName();
            String key = processDefinition.getKey();
            int version = processDefinition.getVersion();
            WorkflowUtils.exportDiagramToFile(repositoryService, processDefinition, exportDir);
            String diagramDir = exportDir + "/" + key + "/" + version;
            String  diagramPath = diagramDir + "/" + diagramResourceName;
            File file = new File(diagramPath);
            FileManage fileManage = gridFSSave(new FileInputStream(file),diagramResourceName,processDefinition.getId());
            filePath = fileManage.getUrl();
            //删除本地文件
            //FileUtils.deleteQuietly(file);
            FileUtil.del(exportDir);
            return "processInput";
        }
        return null;
    }

    /**
     * 检查流程配置是否保存
     * @return
     */
    public String checkBusinessConfig() {
        if (StringUtils.isNotEmpty(keyId)) {
            BusinessConfig bCfg = businessConfigService.getBusinessConfigByDefintionId(keyId);
            if (bCfg == null) {
                return ajaxHtmlCallback("400", "请先保存业务配置", "操作状态");
            }else{
                return ajaxHtmlCallback("200", "success", "操作状态");
            }
        }else{
            return ajaxHtmlCallback("400", "请先选择数据", "操作状态");
        }

    }
    /**
     * 点击配置弹出对应的右侧导航
     *
     * @return
     */
    public String leftView() {
        if (StringUtils.isNotEmpty(keyId)) {
            commonConfig = commonConfigService.get(keyId);
            cselectType = fillChar(String.valueOf(ccreateConfig.getStype()));
        }

        return "leftView";
    }

    public String processConfig() throws ClassNotFoundException {
        if (StringUtils.isNotEmpty(keyId)) {
            processConfig = processConfigService.findConfigByActivityId(keyId, activityId); //keyId:流程定义ID
            businessConfig = businessConfigService.getBusinessConfigByDefintionId(keyId);
 //           String variable = processConfig!=null ? processConfig.getVariable() : "";
//            List<Map<String,Object>> rmap = getEntityFields(businessConfig);

//            fieldName = "";
//            if(StringUtils.isNotEmpty(variable)){
//                for(String field:variable.split(",")){
//                    for(Map<String,Object> map:rmap){
//                        if(StringUtils.equals(field,map.get("filedname").toString())){
//                            fieldName+=map.get("title").toString()+",";
//                        }
//                    }
//                }
//            }

            ActivityImpl activity = workflowService.getActivityById(keyId, activityId);
            properties = activity.getProperties();

            String types = (String) properties.get("type");
            nodeType = WorkflowUtils.parseToZhType(types);
            nodeName = properties.get("name").toString();
//            LogUtil.info("{ variable }" + activity.getVariables());
//            for (String key : properties.keySet()) {
//                System.out.println(key + "->" + properties.get(key));
//            }
            //是否多
            if (properties.containsKey("multiInstance")) {
                multiBehavior = (MultiInstanceActivityBehavior) activity.getActivityBehavior();
            }
            taskDefinition = (TaskDefinition) properties.get("taskDefinition");
            assExpre = taskDefinition.getAssigneeExpression();
            if (processConfig != null) {
                dealType = fillChar(String.valueOf(processConfig.getType()));
                doType = fillChar(String.valueOf(processConfig.getActionType()));
            }
        }
        processDefinition = workflowService.findProDefinitionById(keyId);

        return "processConfig";

    }

    public String input() {
        if (StringUtils.isNotEmpty(keyId)) {
            processDefinition = workflowService.findProDefinitionById(keyId);
            businessConfig = businessConfigService.getBusinessConfigByDefintionId(keyId);
            Deployment deployment = repositoryService.createDeploymentQuery().deploymentId(processDefinition.getDeploymentId()).list().get(0);
            name=deployment.getName();
            if (businessConfig != null) {
                ccreateConfig = businessConfig.getCreateConfig();
                creadConfig = businessConfig.getReadConfig();
                ceditConfig = businessConfig.getEditConfig();
                cdocConfig = businessConfig.getDocConfig();
                version = businessConfig.getVersion();
                if (businessConfig.getCreateConfig() != null) {
                    cselectType = fillChar(String.valueOf(businessConfig.getCreateConfig().getStype()));
                }
                if (businessConfig.getReadConfig() != null) {
                    rselectType = fillChar(String.valueOf(businessConfig.getReadConfig().getStype()));
                }
                if (businessConfig.getEditConfig() != null) {
                    eselectType = fillChar(String.valueOf(businessConfig.getEditConfig().getStype()));
                }
                if (businessConfig.getDocConfig() != null) {
                    dselectType = fillChar(String.valueOf(businessConfig.getDocConfig().getStype()));
                }
            }else{
               //添加需要选的业务表
            }
        }
        return "input";
    }


    /**
     * 获取流程配置中的actionType
     */
    public String operateType() {
        Users users = usersService.getLoginInfo();
        LogUtil.info("keyId:"+keyId);
        List<Task> taskList = taskService.createTaskQuery().processInstanceBusinessKey(keyId).taskCandidateOrAssigned(users.getId()).orderByTaskCreateTime().desc().list();
        LogUtil.info("taskList.size():"+taskList.size());
        Result result = new Result(200, "操作状态");
        if (taskList.size() <= 0) {
            result.setState(400);
            return ajaxHtmlAppResult(result);
        }
        Task task = taskList.get(0);
        System.out.println("taskId:" + task.getTaskDefinitionKey());

        LogUtil.info("(task.getProcessDefinitionId):" + task.getProcessDefinitionId());
        LogUtil.info("(task.getTaskDefinitionKey()):"+task.getTaskDefinitionKey());
        ProcessConfig pcfg = processConfigService.findConfigByActivityId(task.getProcessDefinitionId(), task.getTaskDefinitionKey());
        if (pcfg.getActionType() == -1) {
            result.setState(400);
            return ajaxHtmlAppResult(result);
        }
        String type =String.valueOf(pcfg.getActionType());
        System.out.println("type:" + type);
        List<JSONObject> datarow = Lists.newArrayList();
        Map<String, Object> rMap;
        Map<String, Object> data = new HashMap<String, Object>();
        for (int i = 0; i < type.length(); i++) {

            if (i == 0 && type.charAt(i) == '1') {
                rMap = Maps.newHashMap();
                rMap.put("action", "approve");
                rMap.put("name", "通过");
                datarow.add(JSONObject.fromObject(rMap));
            }
            if (i == 1 && type.charAt(i) == '1') {
                rMap = Maps.newHashMap();
                rMap.put("action", "sendback");
                rMap.put("name", "退回");
                datarow.add(JSONObject.fromObject(rMap));
            }
            if (i == 2 && type.charAt(i) == '1') {
                rMap = Maps.newHashMap();
                rMap.put("action", "deny");
                rMap.put("name", "否决");
                datarow.add(JSONObject.fromObject(rMap));
            }
//            if (i == 3 && type.charAt(i) == '1') {
//                rMap = Maps.newHashMap();
//                rMap.put("action", "request");
//                rMap.put("name", "请求");
//                datarow.add(JSONObject.fromObject(rMap));
//            }


        }
        data.put("datarows", datarow);
        result.setData(data);
        return ajaxHtmlAppResult(result);
    }

    /**
     * 分割字符串
     *
     * @param str
     * @return
     */
    private Set<Users> getUserSpit(String str) {
        Set<Users> usersSet = new HashSet<Users>();
        String[] ary = str.split(",");
        for (String item : ary) {
            if (StringUtils.isNotEmpty(item)) {
                usersSet.add(usersService.get(item));
            }

        }
        return usersSet;
    }

    private Set<Department> getDepartSpit(String str) {
        Set<Department> departSet = new HashSet<Department>();
        if(StringUtils.isEmpty(str)) return departSet;
        String[] ary = str.split(",");
        for (String item : ary) {
            if (StringUtils.isNotEmpty(item)) {
                departSet.add(departmentService.get(item));
            }
        }
        return departSet;
    }

    private Set<Post> getPostSpit(String str) {
        Set<Post> postSet = new HashSet<Post>();
        if(StringUtils.isEmpty(str)) return postSet;
        String[] ary = str.split(",");
        for (String item : ary) {
            if (StringUtils.isNotEmpty(item)) {
                postSet.add(postService.get(item));
            }
        }
        return postSet;
    }

    private Set<Role> getRoleSpit(String str) {
        Set<Role> roleSet = Sets.newHashSet();
        if(StringUtils.isEmpty(str)) return roleSet;
        String[] ary = str.split(",");
        for (String item : ary) {
            if (StringUtils.isNotEmpty(item)) {
                roleSet.add(roleService.get(item));
            }
        }
        return roleSet;
    }

    private Set<Power> getPowerSpit(String str) {
        Set<Power> powerSet = Sets.newHashSet();
        if(StringUtils.isEmpty(str)) return powerSet;
        String[] ary = str.split(",");
        for (String item : ary) {
            if (StringUtils.isNotEmpty(item)) {
                powerSet.add(powerService.get(item));
            }
        }
        return powerSet;
    }

    private int getCtype(List<String> typeList) {
        int cType = -1;
        if (typeList != null) {
            cType = 0;
            for (String type : typeList) {
                cType = Integer.parseInt(type) + cType;
            }
        }
        return cType;
    }

    /**
     * 字符串填充
     *
     * @param str
     * @return
     */
    private String fillChar(String str) {
        if (str.equals("-1")) {
            return "0000";
        }
        int len = str.length();
        String type = str;
        for (int i = len; i < 3; i++) {
            type = "0" + type;
        }

        return type;
    }

    public ProcessDefinition getProcessDefinition() {
        return processDefinition;
    }

    public void setProcessDefinition(ProcessDefinition processDefinition) {
        this.processDefinition = processDefinition;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessKey() {
        return processKey;
    }

    public void setProcessKey(String processKey) {
        this.processKey = processKey;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getCreateIds() {
        return createIds;
    }

    public void setCreateIds(String createIds) {
        this.createIds = createIds;
    }

    public String getReadIds() {
        return readIds;
    }

    public void setReadIds(String readIds) {
        this.readIds = readIds;
    }

    public String getEditIds() {
        return editIds;
    }

    public void setEditIds(String editIds) {
        this.editIds = editIds;
    }

    public String getCreate_selectIds() {
        return create_selectIds;
    }

    public void setCreate_selectIds(String create_selectIds) {
        this.create_selectIds = create_selectIds;
    }

    public String getRead_selectIds() {
        return read_selectIds;
    }

    public void setRead_selectIds(String read_selectIds) {
        this.read_selectIds = read_selectIds;
    }

    public String getEdit_selectIds() {
        return edit_selectIds;
    }

    public void setEdit_selectIds(String edit_selectIds) {
        this.edit_selectIds = edit_selectIds;
    }

    public String getCreate_p_selectIds() {
        return create_p_selectIds;
    }

    public void setCreate_p_selectIds(String create_p_selectIds) {
        this.create_p_selectIds = create_p_selectIds;
    }

    public String getRead_p_selectIds() {
        return read_p_selectIds;
    }

    public void setRead_p_selectIds(String read_p_selectIds) {
        this.read_p_selectIds = read_p_selectIds;
    }

    public String getEdit_p_selectIds() {
        return edit_p_selectIds;
    }

    public void setEdit_p_selectIds(String edit_p_selectIds) {
        this.edit_p_selectIds = edit_p_selectIds;
    }

    public BusinessConfig getBusinessConfig() {
        return businessConfig;
    }

    public void setBusinessConfig(BusinessConfig businessConfig) {
        this.businessConfig = businessConfig;
    }


    public String getCselectType() {
        return cselectType;
    }

    public void setCselectType(String cselectType) {
        this.cselectType = cselectType;
    }


    public String getRselectType() {
        return rselectType;
    }

    public void setRselectType(String rselectType) {
        this.rselectType = rselectType;
    }

    public String getEselectType() {
        return eselectType;
    }

    public void setEselectType(String eselectType) {
        this.eselectType = eselectType;
    }

    public ProcessConfig getProcessConfig() {
        return processConfig;
    }

    public void setProcessConfig(ProcessConfig processConfig) {
        this.processConfig = processConfig;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocRead_p_selectIds() {
        return docRead_p_selectIds;
    }

    public void setDocRead_p_selectIds(String docRead_p_selectIds) {
        this.docRead_p_selectIds = docRead_p_selectIds;
    }

    public String getDocReadIds() {
        return docReadIds;
    }

    public void setDocReadIds(String docReadIds) {
        this.docReadIds = docReadIds;
    }

    public String getDocRead_selectIds() {
        return docRead_selectIds;
    }

    public void setDocRead_selectIds(String docRead_selectIds) {
        this.docRead_selectIds = docRead_selectIds;
    }


    public String getDselectType() {
        return dselectType;
    }

    public void setDselectType(String dselectType) {
        this.dselectType = dselectType;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }


    public TaskDefinition getTaskDefinition() {
        return taskDefinition;
    }

    public void setTaskDefinition(TaskDefinition taskDefinition) {
        this.taskDefinition = taskDefinition;
    }

    public MultiInstanceActivityBehavior getMultiBehavior() {
        return multiBehavior;
    }

    public void setMultiBehavior(MultiInstanceActivityBehavior multiBehavior) {
        this.multiBehavior = multiBehavior;
    }

    public WorkflowService getWorkflowService() {
        return workflowService;
    }

    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    public String getCreate_power_ids() {
        return create_power_ids;
    }

    public void setCreate_power_ids(String create_power_ids) {
        this.create_power_ids = create_power_ids;
    }

    public String getRead_power_ids() {
        return read_power_ids;
    }

    public void setRead_power_ids(String read_power_ids) {
        this.read_power_ids = read_power_ids;
    }

    public String getEdit_power_ids() {
        return edit_power_ids;
    }

    public void setEdit_power_ids(String edit_power_ids) {
        this.edit_power_ids = edit_power_ids;
    }

    public String getDocRead_power_ids() {
        return docRead_power_ids;
    }

    public void setDocRead_power_ids(String docRead_power_ids) {
        this.docRead_power_ids = docRead_power_ids;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPowerId() {
        return powerId;
    }

    public void setPowerId(String powerId) {
        this.powerId = powerId;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDoType() {
        return doType;
    }

    public void setDoType(String doType) {
        this.doType = doType;
    }

    public int getPtype() {
        return ptype;
    }

    public void setPtype(int ptype) {
        this.ptype = ptype;
    }

    public int getOtype() {
        return otype;
    }

    public void setOtype(int otype) {
        this.otype = otype;
    }

    public Set<Role> getRoleSet() {
        return roleSet;
    }

    public void setRoleSet(Set<Role> roleSet) {
        this.roleSet = roleSet;
    }

    public Set<Power> getPowerSet() {
        return powerSet;
    }

    public void setPowerSet(Set<Power> powerSet) {
        this.powerSet = powerSet;
    }

    public Set<Department> getDepartmentSet() {
        return departmentSet;
    }

    public void setDepartmentSet(Set<Department> departmentSet) {
        this.departmentSet = departmentSet;
    }

    public Set<Post> getPostSet() {
        return postSet;
    }

    public void setPostSet(Set<Post> postSet) {
        this.postSet = postSet;
    }

    public CommonConfig getCreadConfig() {
        return creadConfig;
    }

    public void setCreadConfig(CommonConfig creadConfig) {
        this.creadConfig = creadConfig;
    }

    public CommonConfig getCeditConfig() {
        return ceditConfig;
    }

    public void setCeditConfig(CommonConfig ceditConfig) {
        this.ceditConfig = ceditConfig;
    }

    public CommonConfig getCcreateConfig() {
        return ccreateConfig;
    }

    public void setCcreateConfig(CommonConfig ccreateConfig) {
        this.ccreateConfig = ccreateConfig;
    }

    public CommonConfig getCdocConfig() {
        return cdocConfig;
    }

    public void setCdocConfig(CommonConfig cdocConfig) {
        this.cdocConfig = cdocConfig;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId;
    }

    public String getArchReaderId() {
        return archReaderId;
    }

    public void setArchReaderId(String archReaderId) {
        this.archReaderId = archReaderId;
    }

    public String getArchEditorId() {
        return archEditorId;
    }

    public void setArchEditorId(String archEditorId) {
        this.archEditorId = archEditorId;
    }

    public String getDocReaderId() {
        return docReaderId;
    }

    public void setDocReaderId(String docReaderId) {
        this.docReaderId = docReaderId;
    }

    public String getSelectType() {
        return selectType;
    }

    public void setSelectType(String selectType) {
        this.selectType = selectType;
    }

    public CommonConfig getCommonConfig() {
        return commonConfig;
    }

    public void setCommonConfig(CommonConfig commonConfig) {
        this.commonConfig = commonConfig;
    }

    public List<String> getActionType() {
        return actionType;
    }

    public void setActionType(List<String> actionType) {
        this.actionType = actionType;
    }

    public Expression getAssExpre() {
        return assExpre;
    }

    public void setAssExpre(Expression assExpre) {
        this.assExpre = assExpre;
    }

    public String getTriggleTime() {
        return triggleTime;
    }

    public void setTriggleTime(String triggleTime) {
        this.triggleTime = triggleTime;
    }

    public List<Map<String, Object>> getTablekeyMap() {
        return tablekeyMap;
    }

    public void setTablekeyMap(List<Map<String, Object>> tablekeyMap) {
        this.tablekeyMap = tablekeyMap;
    }

    public String getTablekey() {
        return tablekey;
    }

    public void setTablekey(String tablekey) {
        this.tablekey = tablekey;
    }

    public List<Map<String, Object>> getFieldMap() {
        return fieldMap;
    }

    public void setFieldMap(List<Map<String, Object>> fieldMap) {
        this.fieldMap = fieldMap;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }
}
