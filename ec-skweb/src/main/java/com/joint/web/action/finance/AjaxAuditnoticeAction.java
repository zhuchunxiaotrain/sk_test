package com.joint.web.action.finance;

import com.fz.us.base.bean.BaseEnum;
import com.fz.us.base.bean.Pager;
import com.fz.us.base.util.LogUtil;
import com.fz.us.dict.bean.DictBean;
import com.fz.us.dict.entity.Dict;
import com.fz.us.dict.service.DictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.joint.base.bean.FlowEnum;
import com.joint.base.entity.*;
import com.joint.base.exception.users.DutyNotExistsException;
import com.joint.base.exception.workflow.PcfgNotExistException;
import com.joint.base.service.*;
import com.joint.base.util.DataUtil;
import com.joint.core.entity.finance.AuditAnswer;
import com.joint.core.entity.finance.AuditNotice;
import com.joint.core.service.AuditNoticeService;
import com.joint.web.action.BaseFlowAction;
import net.sf.json.JSONObject;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.ParentPackage;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by ZhuChunXiao on 2017/3/13.
 */
@ParentPackage("finance")
public class AjaxAuditnoticeAction extends BaseFlowAction {
    @Resource
    private AuditNoticeService auditNoticeService;
    @Resource
    private DictService dictService;
    @Resource
    private UsersService usersService;
    @Resource
    private DutyService dutyService;
    @Resource
    private DepartmentService departmentService;
    @Resource
    private ReadersService readersService;
    @Resource
    private TaskRecordService taskRecordService;

    private AuditNotice auditNotice;
    private Users loginUser;
    private String viewtype;
    private String step;
    private int numStatus;
    /**
     * 审计类别（读）
     */
    private List<Map<String, Object>> typeDict;
    /**
     * 审计类别
     */
    private String typeDictId;
    /**
     * 审计事务所（读）
     */
    private List<Map<String, Object>> officeDict;
    /**
     * 审计概况附件上传ID（读）
     */
    private String auditSurveyFileId;
    /**
     * 审计概况附件上传ID（上传）
     */
    private String auditSurveyFileIds;
    /**
     * 审计通知书附件上传Id（读）
     */
    private String auditNoticeFileId;
    /**
     * 审计通知书附件上传Id（上传）
     */
    private String auditNoticeFileIds;
    /**
     * 被审计部门
     */
    private String departmentId;
    /**
     * 被审计对象
     */
    private String object;
    /**
     * 通知部门
     */
    private String noticeDepartmentId;
    /**
     * 通知个人
     */
    private String noticeUsersId;
    /**
     * 审计日期
     */
    private String auditDate;
    /**
     * 审计概况文本
     */
    private String auditSurveyText;
    /**
     * 审计事务所Id
     */
    private String officeDictId;
    /**
     * 备注
     */
    private String remarks;

    public String execute(){
        return "auditnotice";
    }

    public String list(){
        pager = new Pager();
        if (rows <= 0){
            rows = 10;
        }
        pager.setPageSize(rows);
        pager.setPageNumber(page);
        pager.setOrderBy("modifyDate");
        pager.setOrderType(BaseEnum.OrderType.desc);

        if(StringUtils.isNotEmpty(sidx)&& BaseEnum.OrderType.valueOf(sord)!=null){
            pager.setOrderBy(sidx);
            pager.setOrderType(BaseEnum.OrderType.valueOf(sord));
        }
        Company com = usersService.getCompanyByUser();
        Users user = usersService.getLoginInfo();
        Map<String,Object> params = new HashMap<String,Object>();
        params = getSearchFilterParams(_search,params,filters);
        params.put("company",com);
        params.put("state",BaseEnum.StateEnum.Enable);
        if(StringUtils.isNotEmpty(step)) {
            if (step.equals("1")) {
                //实施中
                params.put("step", new String[]{"1","2","3","4","5"});
                params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed,FlowEnum.ProcessState.Finished});
                params.put("invalid", false);
                pager = auditNoticeService.findByPagerAndLimit(false, "auditnotice", pager, params);
            } else if (step.equals("2")) {
                //已完成
                params.put("step", new String[]{"6"});
                params.put("invalid", true);
                pager = auditNoticeService.findByPagerAndLimit(false, "auditnotice", pager, params);
            }
        }else{
            //所有单子
            params.put("processState", new FlowEnum.ProcessState []{FlowEnum.ProcessState.Running, FlowEnum.ProcessState.Backed, FlowEnum.ProcessState.Finished, FlowEnum.ProcessState.Deny});
            pager=auditNoticeService.findByPagerAndLimit(true, "auditnotice", pager, params);
        }
        List<AuditNotice> auditNoticeList;
        if (pager.getTotalCount() > 0){
            auditNoticeList = (List<AuditNotice>) pager.getList();
        }else{
            auditNoticeList= new ArrayList<>();
        }

        List<JSONObject> dataRows=new ArrayList<JSONObject>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String,Object> rMap;
        for(AuditNotice an:auditNoticeList){
            rMap = new HashMap<String, Object>();
            rMap.put("id",an.getId());
            rMap.put("type",an.getType().getName());
            rMap.put("department",an.getDepartment().getName());
            rMap.put("date", DataUtil.DateToString(an.getAuditDate(), "yyyy-MM-dd"));
            switch (an.getStep()){
                case "1" :
                    rMap.put("step","审计通知书");
                    rMap.put("state",an.getProcessState().value());
                    break;
                case "2" :
                    rMap.put("step","审计初稿");
                    rMap.put("state",an.getAuditFirst().getProcessState().value());
                    break;
                case "3" :
                    rMap.put("step","审计终稿");
                    rMap.put("state",an.getAuditLast().getProcessState().value());
                    break;
                case "4" :
                    rMap.put("step","审计整改通知单");
                    rMap.put("state",an.getAuditEdit().getProcessState().value());
                    break;
                case "5" :
                    rMap.put("step","审计答复");
                    int i=an.getAuditAnswerList().size();
                    if(an.getAuditAnswerList().get(i-1).getAuditType()==1){
                        rMap.put("state","审计计划-"+an.getAuditAnswerList().get(i-1).getProcessState().value());
                    }else if(an.getAuditAnswerList().get(i-1).getAuditType()==2){
                        rMap.put("state","审计结果-"+an.getAuditAnswerList().get(i-1).getProcessState().value());
                    }
                    break;
                case "6" :
                    rMap.put("step","已完成");
                    break;
                default : rMap.put("step","未知");
            }
            JSONObject o = JSONObject.fromObject(rMap);
            dataRows.add(o);
        }
        data.put("dataRows",dataRows);
        data.put("page",page);
        data.put("rows",rows);

        long recordsNum = pager.getTotalCount();
        data.put("total",calcPageNum(recordsNum));
        data.put("records", recordsNum);

        return  ajaxJson(JSONObject.fromObject(data).toString());
    }

    public String input(){
        Company company = usersService.getCompanyByUser();
        Map<String,Object> rMap = null;
        typeDict = new ArrayList<Map<String, Object>>();
        officeDict = new ArrayList<Map<String, Object>>();
        List<Dict> auditType = dictService.listFormDefinedEnable(DictBean.DictEnum.AuditType, usersService.getCompanyByUser().getId());
        List<Dict> auditOffice = dictService.listFormDefinedEnable(DictBean.DictEnum.AuditOffice, usersService.getCompanyByUser().getId());
        if(StringUtils.isNotEmpty(keyId)){
            auditSurveyFileId="";
            auditNoticeFileId="";
            auditNotice=auditNoticeService.get(keyId);
            if(auditNotice.getAuditSurveyFile()!=null&&auditNotice.getAuditSurveyFile().size()>0){
                for(FileManage afile:auditNotice.getAuditSurveyFile()){
                    auditSurveyFileId+=afile.getId()+",";
                }
            }
            if(auditNotice.getAuditNoticeFile()!=null&&auditNotice.getAuditNoticeFile().size()>0){
                for(FileManage anfile:auditNotice.getAuditNoticeFile()){
                    auditNoticeFileId+=anfile.getId()+",";
                }
            }
            for(Dict atype:auditType){
                rMap = new HashMap<String, Object>();
                rMap.put("id",atype.getId());
                rMap.put("name",atype.getName());
                rMap.put("selected","");
                if(auditNotice.getType()!=null && StringUtils.equals(atype.getId(), auditNotice.getType().getId())){
                    rMap.put("selected","selected");
                }
                typeDict.add(rMap);
            }
            for(Dict aoffice:auditOffice){
                rMap = new HashMap<String, Object>();
                rMap.put("id",aoffice.getId());
                rMap.put("name",aoffice.getName());
                rMap.put("selected","");
                if(auditNotice.getOffice()!=null && StringUtils.equals(aoffice.getId(), auditNotice.getOffice().getId())){
                    rMap.put("selected","selected");
                }
                officeDict.add(rMap);
            }
        }else{
            for(Dict atype:auditType) {
                rMap = new HashMap<String, Object>();
                rMap.put("id", atype.getId());
                rMap.put("name", atype.getName());
                rMap.put("selected", "");
                typeDict.add(rMap);
            }
            for(Dict aoffice:auditOffice) {
                rMap = new HashMap<String, Object>();
                rMap.put("id", aoffice.getId());
                rMap.put("name", aoffice.getName());
                rMap.put("selected", "");
                officeDict.add(rMap);
            }
        }

        return "input";
    }

    public String read(){
        loginUser = usersService.getLoginInfo();
        if(StringUtils.isNotEmpty(keyId)){
            numStatus = workflowService.getNumStatus(keyId, loginUser);
            auditNotice=auditNoticeService.get(keyId);
            auditSurveyFileId="";
            auditNoticeFileId="";
            if(auditNotice.getAuditSurveyFile()!=null&&auditNotice.getAuditSurveyFile().size()>0){
                for(FileManage asfile:auditNotice.getAuditSurveyFile()){
                    auditSurveyFileId+=asfile.getId()+",";
                }
            }
            if(auditNotice.getAuditNoticeFile()!=null&&auditNotice.getAuditNoticeFile().size()>0){
                for(FileManage anfile:auditNotice.getAuditNoticeFile()){
                    auditNoticeFileId+=anfile.getId()+",";
                }
            }
        }
        return "read";
    }

    public void setData(){
        if(StringUtils.isNotEmpty(keyId)){
            auditNotice=auditNoticeService.get(keyId);
        }else{
            auditNotice=new AuditNotice();
            auditNotice.setCreater(usersService.getLoginInfo());
        }
        auditNotice.setType(dictService.get(typeDictId));
        auditNotice.setOffice(dictService.get(officeDictId));
        auditNotice.setDepartment(departmentService.get(departmentId));
        auditNotice.setObject(object);
        auditNotice.setAuditDate(DataUtil.StringToDate(auditDate));
        auditNotice.setAuditSurveyText(auditSurveyText);
        auditNotice.setRemarks(remarks);
        auditNotice.setCompany(usersService.getLoginInfo().getCompany());

        List<Department> deptList= Lists.newArrayList();
        if(StringUtils.isNotEmpty(noticeDepartmentId)){
            for(String dept:noticeDepartmentId.split(",")){
                deptList.add(departmentService.get(dept.trim()));
            }
        }
        auditNotice.setNoticeDepartment(deptList);

        List<Users> usersList=Lists.newArrayList();
        if(StringUtils.isNotEmpty(noticeUsersId)){
            for(String user:noticeUsersId.split(",")){
                usersList.add(usersService.get(user.trim()));
            }
        }
        auditNotice.setNoticeUsers(usersList);

        List<FileManage> asfileList=Lists.newArrayList();
        if(StringUtils.isNotEmpty(auditSurveyFileIds)){
            for(String asfile:auditSurveyFileIds.split(",")){
                asfileList.add(fileManageService.get(asfile.trim()));
            }
        }
        auditNotice.setAuditSurveyFile(asfileList);

        List<FileManage> acfileList=Lists.newArrayList();
        if(StringUtils.isNotEmpty(auditNoticeFileIds)){
            for(String acfile:auditNoticeFileIds.split(",")){
                acfileList.add(fileManageService.get(acfile.trim()));
            }
        }
        auditNotice.setAuditNoticeFile(acfileList);
    }

    public String save(){
        setData();
        try {
            if(StringUtils.isNotEmpty(keyId)){
                auditNoticeService.update(auditNotice);
            } else {
                Map<String, Object> various = new HashMap<String, Object>();
                various.put("numStatus", 0);
                various.put("curDutyId", curDutyId);
                various.put("initDuty", curDutyId);
                auditNoticeService.save(auditNotice, "auditnotice", various);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }

        return ajaxHtmlCallback("200", "保存成功！", "操作状态");
    }

    public String commit(){
        setData();
        auditNotice.setStep("1");
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("numStatus", 0);
        var1.put("curDutyId", curDutyId);
        var1.put("initDuty", curDutyId);
        Map<String, Object> var2 = new HashMap<String, Object>();
        var2.put("numStatus", 1);
        var2.put("curDutyId", curDutyId);
        var2.put("initDuty", curDutyId);

        ArrayList<String> list =Lists.newArrayList();
        List<Department> deptList=departmentService.getDepartments("产业中心");
        for(Department dept:deptList){
            Set<Power> powers = dept.getPowerSet();
            for(Power power:powers){
                if(power.getPost().getName().equals("书记")||power.getPost().getName().equals("主任")){
                    List<Users> usersList=dutyService.getPersons(dept,power.getPost());
                    for(Users user:usersList){
                        list.add(user.getId().trim());
                    }
                }
            }
        }
        var2.put("approvers",list);

        List<String> deptlist=Lists.newArrayList();
        Department dept=auditNotice.getDepartment().getParent()==null?auditNotice.getDepartment():auditNotice.getDepartment().getParent();
        for(Power power:dept.getPowerSet()){
            if(power.getPost().getName().equals("总经理")){
                List<Users> usersList=dutyService.getPersons(dept,power.getPost());
                for(Users user:usersList){
                    deptlist.add(user.getId().trim());
                }
            }
        }
        var2.put("dept",deptlist);

        Set<Users> usersSet = Sets.newHashSet();
        Map<String, Object> map = new HashMap<String, Object>();
        List<Department> listDept = auditNotice.getNoticeDepartment();
        if(StringUtils.equals(object,"1")){
            for(Department department:listDept){
                List<Duty> dutyList = dutyService.getDutys(department);
                for(Duty duty:dutyList){
                    usersSet.add(duty.getUsers());
                }
            }
        }
        if(StringUtils.equals(object,"2")){
            List<Users> usersList = auditNotice.getNoticeUsers();
            for(Users users:usersList){
                usersSet.add(users);
            }
        }

        try {
            if (StringUtils.isNotEmpty(keyId)) {
                auditNoticeService.approve(auditNotice, FlowEnum.ProcessState.Running, var2, curDutyId,comment);
            } else {
                keyId = auditNoticeService.commit(auditNotice, "auditnotice", var1, var2, curDutyId);
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        //先删除读者
        readersService.deleteByKeyIdBussinessKey(keyId);
        //增加归档后读者
        for(Users users:usersSet){
            Readers readers = new Readers();
            readers.setUsers(users);
            readers.setBussinessKey("auditnotice");
            readers.setKeyId(keyId);
            readers.setType(2);
            readersService.save(readers);
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    //会签
    public String approve1(){
        auditNotice = auditNoticeService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);

        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                auditNoticeService.approve(auditNotice, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            //LogUtil.info("overtask:"+overtask);
            //会签后没有流程将对象状态设为归档
            if(overtask==null){
                AuditNotice auditNotice = auditNoticeService.get(keyId);
                auditNotice.setProcessState(FlowEnum.ProcessState.Finished);
                auditNoticeService.update(auditNotice);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(auditNotice.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 2);
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String approve2(){
        auditNotice = auditNoticeService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                auditNoticeService.approve(auditNotice, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            //LogUtil.info("overtask:"+overtask);
            //会签后没有流程将对象状态设为归档
            if(overtask==null){
                AuditNotice auditNotice = auditNoticeService.get(keyId);
                auditNotice.setProcessState(FlowEnum.ProcessState.Finished);
                auditNoticeService.update(auditNotice);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(auditNotice.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 3);
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    public String approve3(){
        auditNotice = auditNoticeService.get(keyId);
        Map<String, Object> var1 = new HashMap<String, Object>();
        var1.put("curDutyId", curDutyId);
        if(StringUtils.isEmpty(comment)){
            comment="";
        }
        try {
            if (StringUtils.isNotEmpty(keyId)) {
                auditNoticeService.approve(auditNotice, FlowEnum.ProcessState.Running, var1, curDutyId, comment);
            }
            Task overtask = workflowService.getCurrentTask(keyId);
            //LogUtil.info("overtask:"+overtask);
            //会签后没有流程将对象状态设为归档
            if(overtask==null){
                AuditNotice auditNotice = auditNoticeService.get(keyId);
                auditNotice.setProcessState(FlowEnum.ProcessState.Finished);
                auditNoticeService.update(auditNotice);
                List<TaskRecord> taskRecords = taskRecordService.getDataByKeyId(auditNotice.getId());
                for(TaskRecord taskRecord:taskRecords){
                    taskRecord.setType(2);
                    taskRecordService.update(taskRecord);
                }
            }else {
                if (runtimeService.getVariable(overtask.getExecutionId(), "nrOfCompletedInstances") == null) {
                    runtimeService.setVariable(overtask.getExecutionId(), "numStatus", 4);
                }
            }
        } catch (DutyNotExistsException e) {
            return ajaxHtmlCallback("404", "当前人员无新建权限，请联系管理员！", "操作状态");
        } catch (PcfgNotExistException e) {
            return ajaxHtmlCallback("404", "找不到流程节点配置，请联系管理员！", "操作状态");
        }
        catch (Exception e){
            return ajaxHtmlCallback("404", "操作异常，请联系管理员！", "操作状态");
        }
        return ajaxHtmlCallback("200", "提交成功！", "操作状态");
    }

    /**
     * 退回
     * @return
     */
    public String reject() {
        if (StringUtils.isNotEmpty(keyId)) {
            auditNotice = auditNoticeService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("auditnotice");
            String key=activityList.get(0).getId();
            System.out.println("Activity key:"+key);
            LogUtil.info("numStatus:" + numStatus);
            if(StringUtils.isEmpty(comment)) comment="";
            auditNoticeService.reject(auditNotice, key, numStatus, comment, curDutyId);
        }

        return ajaxHtmlCallback("200", "退回成功！", "操作状态");
    }

    /**
     * 否决
     * @return
     */
    public String deny() {
        if (StringUtils.isNotEmpty(keyId)) {
            auditNotice = auditNoticeService.get(keyId);
            List<ActivityImpl> activityList= workflowService.findAllActivitiesByKey("auditnotice");
            String key= activityList.get(activityList.size()-1).getId();
            System.out.println("Activity key:" + key);
            if(StringUtils.isEmpty(comment)){
                comment="";
            }
            auditNoticeService.deny(auditNotice, key, comment, curDutyId);
        }
        return ajaxHtmlCallback("200", "否决成功！", "操作状态");
    }


    /**
     * 完成
     * @return
     */
    public String doInvalid(){
        System.out.println("进行完成操作："+keyId);
        if(StringUtils.isNotEmpty(keyId)) {
            auditNotice = auditNoticeService.get(keyId);
            List<AuditAnswer> aaList=auditNotice.getAuditAnswerList();
            if(aaList==null||aaList.size()<=0||!aaList.get(aaList.size()-1).getProcessState().name().equals("Finished")) {
                return ajaxHtmlCallback("400", "此条财务审计流程还没有执行完毕！", "操作状态");
            }
            auditNotice.setInvalid(true);
            auditNotice.setStep("6");
            auditNoticeService.update(auditNotice);
        }
        return ajaxHtmlCallback("200", "操作成功！", "操作状态");
    }


    public AuditNotice getAuditNotice() {
        return auditNotice;
    }

    public void setAuditNotice(AuditNotice auditNotice) {
        this.auditNotice = auditNotice;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    @Override
    public int getNumStatus() {
        return numStatus;
    }

    @Override
    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public List<Map<String, Object>> getTypeDict() {
        return typeDict;
    }

    public void setTypeDict(List<Map<String, Object>> typeDict) {
        this.typeDict = typeDict;
    }

    public List<Map<String, Object>> getOfficeDict() {
        return officeDict;
    }

    public void setOfficeDict(List<Map<String, Object>> officeDict) {
        this.officeDict = officeDict;
    }

    public String getAuditSurveyFileId() {
        return auditSurveyFileId;
    }

    public void setAuditSurveyFileId(String auditSurveyFileId) {
        this.auditSurveyFileId = auditSurveyFileId;
    }

    public String getAuditSurveyFileIds() {
        return auditSurveyFileIds;
    }

    public void setAuditSurveyFileIds(String auditSurveyFileIds) {
        this.auditSurveyFileIds = auditSurveyFileIds;
    }

    public String getAuditNoticeFileId() {
        return auditNoticeFileId;
    }

    public void setAuditNoticeFileId(String auditNoticeFileId) {
        this.auditNoticeFileId = auditNoticeFileId;
    }

    public String getAuditNoticeFileIds() {
        return auditNoticeFileIds;
    }

    public void setAuditNoticeFileIds(String auditNoticeFileIds) {
        this.auditNoticeFileIds = auditNoticeFileIds;
    }

    public String getTypeDictId() {
        return typeDictId;
    }

    public void setTypeDictId(String typeDictId) {
        this.typeDictId = typeDictId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getNoticeDepartmentId() {
        return noticeDepartmentId;
    }

    public void setNoticeDepartmentId(String noticeDepartmentId) {
        this.noticeDepartmentId = noticeDepartmentId;
    }

    public String getNoticeUsersId() {
        return noticeUsersId;
    }

    public void setNoticeUsersId(String noticeUsersId) {
        this.noticeUsersId = noticeUsersId;
    }

    public String getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(String auditDate) {
        this.auditDate = auditDate;
    }

    public String getAuditSurveyText() {
        return auditSurveyText;
    }

    public void setAuditSurveyText(String auditSurveyText) {
        this.auditSurveyText = auditSurveyText;
    }

    public String getOfficeDictId() {
        return officeDictId;
    }

    public void setOfficeDictId(String officeDictId) {
        this.officeDictId = officeDictId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }
}
