<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<div class="row" style="padding:2px">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<div class="alert alert-warning">
			<i class="fa fa-fw fa-warning"></i>
			<strong>注意:</strong>
			注册码默认时间为一年
		</div>
		<div >
			<a id="ajax_auth_produce" class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 生成注册码</a>
			<a  id="ajax_code_bind" class="btn btn-success pull-right" data-toggle="">
				<i class="fa fa-lg fa-book"></i>合同签署
			</a>
			<span id="info"></span>
		</div>
		<%--<div class="col-sm-12 col-md-12 col-lg-12"></div>--%>
	</div>
</div>
<p></p>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">
		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<input type="hidden" id="selectedIds" value=""/>
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
				 data-widget-colorbutton="false"
				 data-widget-togglebutton="false"
				 data-widget-deletebutton="false"
				 data-widget-fullscreenbutton="false"
				 data-widget-custombutton="false"
				 data-widget-sortable="false"
					>


				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="col-sm-12 col-md-12 col-lg-12">
							<div class=" ">
								<div class="row" id="ajax_authcode_list_row">
									<table id="ajax_authcode_table" class="table table-striped table-bordered table-hover">
									</table>
									<div id="ajax_authcode_list_page">
									</div>
								</div>

							</div>
						</div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	function reload(){
		loadURL("ajax!authcode.action",$('#content'));
	}

	$("#ajax_auth_produce").off("click").on("click",function(e){
		$.SmartMessageBox({
			title : "充值码",
			content : "请输入生成充值码的数目",
			buttons : "[取消][确定]",
			input : "text",
			placeholder : "请输入生成充值码的数目"
		}, function(ButtonPress, Value) {
			if(ButtonPress == "确定"){
				var url = "ajax-authcode!genCode.action";
				ajax_action(url,{num:Value},null,function(data){
					_showResult(data);
					$("#ajax_authcode_table").trigger("reloadGrid");
				});
			}

		});
		e.preventDefault();
	});
	$("#ajax_code_bind").off("click").on("click",function(e){
		var ids = $("#selectedIds").val();
		if(ids==""){
			_show({state:400,message:"请选择注册码",title:"操作状态"});
			return;
		}
		gDialog.fCreate({
			title:"合同签署",
			url:"ajax-authcode!bindCompany.action",
			param:{keyId:ids}
		});
	})
</script>
<script type="text/javascript">
	$(function(){
		load_authcode_jqGrid();
	});
	function statusFormat(cellvalue,option,cell){
		var id = cell["id"];
		var span = "<span class='label label-";
		switch(cellvalue){
			case "暂未使用": span = span + "warning";break;
			case "已过期": span = span + "default";break;
			case "未使用": span = span + "success";break;
			default:span = span + "info";
		}
		span = span + "'>"+cellvalue+"</span>"
		return span;
	}
	function optGrid(rowId, status){
		var ids = $("#selectedIds").val();
		ids = status==true ? (ids.indexOf(rowId) == -1? ids + rowId+",":ids) : ids.replace((rowId+","),"");
		$("#selectedIds").val(ids);
	}

	function load_authcode_jqGrid(){
		jQuery("#ajax_authcode_table").jqGrid({
			url:'ajax-authcode!list.action',
			datatype: "json",
			colNames:['注册码',"所属企业","当前状态","当前使用/已使用","绑定时间","剩余天数","创建时间",'创建人',"id"],
			colModel:[
				{name:'authCode',index:'authCode', width:120,sortable:true,search:false},
				{name:'company',index:'company', width:120,sortable:true,search:false},
				{name:'status',index:'status', width:80,search:false,formatter:statusFormat},
				{name:'use',index:'use', width:60,search:false},
				{name:'bindDate',index:'bindDate', width:60,search:false},
				{name:'days',index:'days', width:60,search:false},
				{name:'createDate',index:'createDate', width:80,search:false},
				{name:'creater',index:'creater_name', width:80,search:false},
				{name:'id',index:'id',hidden:true,search:false}
			],
			rowNum : 10,
			rowList:[10,20,30],
			pager : '#ajax_authcode_list_page',
			gridComplete:function(){
				var dIds=$("#ajax_authcode_table").jqGrid('getDataIDs');
				var sIds = $("#selectedIds").val();
				for(var i=0;i<dIds.length;i++){
					if(sIds.indexOf(dIds[i])!=-1) $("#ajax_authcode_table").jqGrid("setSelection", dIds[i]);
				}
				$(".ui-jqgrid-bdiv").css("overflow-x","hidden");
				jqGridStyle();
			},
			caption : "<i class='fa fa-arrow-circle-right'></i> 充值码一览",
			jsonReader: {
				root: "dataRows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems : false
			},
			multiselect : true,
			rownumbers:true,
			gridview:true,
			shrinkToFit:true,
			viewrecords: true,
			autowidth: true,
			height:'auto',
			forceFit:true,
			onSelectRow:function(rowId, status, e){
				optGrid(rowId, status)
			},
			onSelectAll:function(aRowids, status, e){
				for(var i=0;i<aRowids.length;i++){
					optGrid(aRowids[i], status);
				}
			},
			loadComplete: function() {
			}
		});
		$(window).on('resize.jqGrid', function() {
			jQuery("#ajax_authcode_table").jqGrid('setGridWidth', $("#ajax_authcode_list_row").width());
		})
		jQuery("#ajax_authcode_table").jqGrid('navGrid', "#ajax_authcode_list_page", {
			edit : false,
			add : false,
			del : false,
			search:false
		});
	};
</script>