<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html>
<style>
    .bluehead{
        background-color: #3240A0 ;
        background-image: -moz-linear-gradient(top,#f78c40,#d67632);
        background-image: -webkit-gradient(linear,0 0,0 100%,from(#f78c40),to(#d67632));
        background-image: -webkit-linear-gradient(top,#f78c40,#d67632);
        background-image: -o-linear-gradient(top,#f78c40,#d67632);
        background-image: linear-gradient(to bottom,#386AC5,#3C5F96) !important;
    }
</style>
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title>上海市卫生产业开发中心-协同运营平台</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Use the correct meta names below for your web application
         Ref: http://davidbcalhoun.com/2010/viewport-metatag

    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/font-awesome.min.css">
    <!-- SmartSales Styles : Please note (SmartSales-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/smartadmin-production.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/smartadmin-skins.css">

    <!-- <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/js/plugin/datepicker/css/datepicker3.css"> -->
    <!-- SmartSales RTL Support is under construction
    <link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/js/plugin/uploadify/uploadify.css">
    <!-- We recommend you use "your_style.css" to override SmartSales
    specific styles this will also ensure you retrain your customization
    with each SmartSales update.
    <link rel="stylesheet" type="text/css" media="screen" href="css/demo.css"> -->
    <link rel="stylesheet" type="text/css" href="../resource/jquery/Jcrop/jquery.Jcrop.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/demo.css">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="../resource/com/img/favicon/favicon-t.ico" type="image/x-icon">
    <link rel="icon" href="../resource/com/img/favicon/favicon-t.ico" type="image/x-icon">

    <link href="../resource/com/umeditor/themes/default/css/umeditor.min.css" type="text/css" rel="stylesheet">
    <!-- add css-->
    <link href="../resource/list.css" type="text/css" rel="stylesheet">

    <!--JSHOW CSS -->
    <link href="../resource/jshow/css/bootstrap-modal-bs3patch.css" type="text/css" rel="stylesheet">
    <link href="../resource/jshow/css/bootstrap-modal.css" type="text/css" rel="stylesheet">
    <!-- multiselect-->
    <link href="../resource/multiselect/css/bootstrap-multiselect.css" type="text/css" rel="stylesheet">

    <!-- datetimepicker -->
    <link href="../resource/activiti/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet">

    <!-- jqGrid CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/jqgrid-ui.css">

    <!-- ztree -->
    <link rel="stylesheet" href="../resource/com/js/plugin/ztree/css/zTreeStyle/metro.css">
    <!-- ipicture -->
    <link rel="stylesheet" href="../resource/activiti/ipicture/iPicture.css">

    <!-- activitiSmart -->
    <link rel="stylesheet" href="../resource/activiti/activitiSmart.css">
    <!-- jOrgChart -->
    <link rel="stylesheet" href="../resource/activiti/orgChart/css/jquery.jOrgChart.css">

    <link rel="stylesheet" type="text/css" href="../resource/com/css/newframe/theme_styles.css">



    <script type="text/javascript">
        var _loginUsersCompany = 0;
        <s:if test="users != null">
        _loginUsersCompany = 1;
        </s:if>
    </script>
</head>
<body class="smart-style-3 fixed-header">
<input hidden id="_t"  name="_t" value="<s:property value="_t"></s:property>">
<!-- possible classes:smart-style-2 minified, fixed-ribbon, fixed-header, fixed-width-->

<!-- HEADER -->
<header id="header" class="bluehead">
    <div id="logo-group" >

        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo" ><img src="../resource/com/img/logo-cst-white.png" alt="上海市卫生产业开发中心-协同运营平台">  </span>
        <!-- END LOGO PLACEHOLDER -->

        <!-- Note: The activity badge color changes when clicked and resets the number to 0
        Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications  <b class="badge"> 0 </b> -->
				<%--<span id="activity" class="activity-dropdown">--%>
				    <%--<i class="fa fa-bell swing animated"></i>--%>
				    <%--<b class="badge"><s:property value="numToDo()+numDraft()"/>--%>
                        <%--<!--<i class="fa fa-bell swing animated"></i>-->--%>
                    <%--</b>--%>
				<%--</span>--%>

        <!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
        <div class="ajax-dropdown">

            <!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
            <div class="btn-group btn-group-justified" data-toggle="buttons">
                <label class="btn btn-default">
                    <input type="radio" name="activity" id="ajax-comment!toUsersList.action">
                    <i class="fa fa-hand-o-right"></i> 动态消息 ( <s:property value="numToDo()"/>)  </label>
                <label class="btn btn-default">
                    <input type="radio" name="activity" id="ajax-announce!annList.action">
                    <i class="fa fa-hand-o-right"></i> 通知 ( <s:property value="numDraft()"/>)  </label>

            </div>

            <!-- notification content -->
            <div class="ajax-notifications custom-scroll">

                <div class="alert alert-transparent">
                    <h4><i class="fa fa-upload"></i> 请点击上面的按钮显示内容</h4>

                </div>

                <i class="fa fa-lock fa-4x fa-border"></i>

            </div>
            <!-- end notification content -->

            <!-- footer: refresh area -->
					<span> 最新个人信息一览
						<button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> 加载中..." class="btn btn-xs btn-default pull-right">
                            <i class="fa fa-refresh"></i>
                        </button> </span>
            <!-- end footer -->

        </div>
        <!-- END AJAX-DROPDOWN -->
    </div>

    <%--<div style="width:120px;margin-left:10px" >--%>
        <%--<a class="btn-header" id="show-shortcut"   rel="tooltip" data-placement="right" data-original-title=" 视图选择 " data-html="true" style="text-decoration:none;color:#fff" href="javascript:void(0);">--%>
            <%--<span  id="viewselect" > <i class="fa fa-desktop"></i><span> 视图选择 </span><i class="fa fa-angle-down"></i>  </span>--%>
        <%--</a>--%>
    <%--</div>--%>


    <!-- pulled right: nav area -->
    <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
					<span> <a rel="tooltip" data-placement="left" href="javascript:void(0);" title="折叠左侧菜单">
                        <i class="fa fa-bars"></i>
                    </a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a rel="tooltip" data-placement="left" href="logout.action" title="登出系统"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->

        <s:if test="_t == null || _t == 'web'">

            <!-- search mobile button (this is hidden till mobile view port) -->
            <div id="search-mobile" class="btn-header transparent pull-right">
                <span> <a  href="javascript:void(0)" title="查询"><i class="fa fa-search"></i></a> </span>
            </div>
            <!-- end search mobile button -->


            <!-- end input: search field -->

        </s:if>
        <!-- multiple lang dropdown : find all flags in the image folder
        <ul class="header-dropdown-list hidden-xs">
            <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img alt="" src="../resource/com/img/flags/cn.png"> <span> CN </span> <i class="fa fa-angle-down"></i> </a>
                <ul class="dropdown-menu pull-right">
                    <li class="active">
                        <a href="javascript:void(0);"><img alt="" src="../resource/com/img/flags/cn.png"> CN</a>
                    </li>
                </ul>
            </li>
        </ul>
        -->
        <!-- end multiple lang -->

    </div>
    <!-- end pulled right: nav area -->




    <!-- end projects dropdown -->

</header>
<!-- END HEADER -->

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">


    <!-- User info -->
    <div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->
					<s:if test="user.id == null">
                        <img id="btn-img" src="" alt="" class="online" />
                    </s:if>
					<s:else>
                        <img id="btn-img" height="35" width="35" alt="" src="file.action?keyId=<s:property value="users.id" />" alt="" class="online" />
                    </s:else>
					<a rel="tooltip" data-placement="right" title="" href="#ajax-users!profile.action?keyId=<s:property value="users.id" />" id="profile" >
                        <!-- 用户名 -->
                        <s:property value="users.name"/>

                        <i class="fa  fa-angle-right"></i>
                    </a>
				</span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive

    To make this navigation dynamic please make sure to link the node
    (the reference to the nav > ul) after page load. Or the navigation
    will not initialize.
    -->
    <nav>
        <!-- NOTE: Notice the gaps after each icon usage <i></i>..
        Please note that these links work a bit different than
        traditional href="" links. See documentation for details.
        -->

        <ul id="mainTree">
            <s:if test="_t == null || _t == 'web'">
                <!-- 数据平台 -->
                <li class="">
                    <a href="ajax!dashboard.action" title="监控台"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">监控台</span></a>
                </li>
				<li class="">
					<a href="ajax!authcode.action" title="注册码管理"><i class="fa fa-lg fa-fw fa-ticket"></i> <span class="menu-item-parent">注册码管理</span></a>
				</li>
                <%--<li class="">--%>
                <%--<a href="ajax!feedback.action" title="意见反馈"><i class="fa fa-lg fa-fw fa-comment-o"></i> <span class="menu-item-parent">意见反馈</span></a>--%>
                <%--</li>--%>
                <li class="">
                    <a href="ajax!companyData.action" title="企业数据监控"><i class="fa fa-lg fa-fw fa-tasks"></i> <span class="menu-item-parent">企业数据监控</span></a>
                </li>
                <li class="">
                    <a href="ajax!exPerUserData.action" title="体验数据监控"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">体验数据监控</span></a>
                </li>
                <%--<li>--%>
                <%--<a href="#"><i class="fa fa-lg fa-fw fa-sign-in"  title="注册清单"></i> <span class="menu-item-parent">注册清单</span></a>--%>
                <%--<ul>--%>
                <%--<li>--%>
                <%--<a href="ajax!company.action" title="企业注册清单"><i class="fa fa-fw fa-group"></i>企业注册清单</a>--%>
                <%--</li>--%>
                <%--<li>--%>
                <%--<a href="ajax!users.action" title="用户注册清单"><i class="fa fa-fw fa-user"></i>用户注册清单</a>--%>
                <%--</li>--%>
                <%--</ul>--%>
                <%--</li>--%>
                <%--<li class="">--%>
                <%--<a href="ajax!paylog.action" title="支付记录"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">支付记录</span></a>--%>
                <%--</li>--%>

                <%--<li>--%>
                    <%--<a href="ajax!dict.action" title="系统字典"><i class="fa fa-fw fa-file-text"></i> <span class="menu-item-parent"> 系统字典</span></a>--%>
                <%--</li>--%>
                <%--<li>--%>
                    <%--<a href="ajax!log.action" title="访问流量"><i class="fa fa-fw fa fa-rss"></i> <span class="menu-item-parent"> 访问流量</span></a>--%>
                <%--</li>--%>
            </s:if>




        </ul>
    </nav>
    <span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> 提示! 刷新会重新设置您的排版。" data-html="true"><i class="fa fa-refresh"></i></span> </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <!-- This is auto generated -->
        </ol>
        <!-- end breadcrumb -->

        <!-- You can also add more buttons to the
        ribbon for further usability

        Example below:

        <span class="ribbon-button-alignment pull-right">
        <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
        <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
        <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
        </span>
-->
    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
Note: These tiles are completely responsive,
you can add as many as you like
平台大功能分区菜单
-->
<div id="shortcut">
    <ul>
        <li>
            <a href="index.action?_t=web" class="<s:if test="_t == null || _t == 'web'"> selected </s:if> jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-cloud fa-4x"></i> <span>云数据板块 </span> </span> </a>
        </li>
        <li>
            <a href="index.action?_t=system" class="<s:if test="_t == 'system'"> selected </s:if> jarvismetro-tile big-cubes bg-color-red"> <span class="iconbox"> <i class="fa fa-cog fa-4x"></i> <span>云设置板块</span> </span> </a>
        </li>

    </ul>
</div>
<!-- END SHORTCUT AREA -->

<!-- end select-->
<!-- left panel start hidden================================================== -->

<!-- left panel end hidden================================================== -->


<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
<script>
    //if (!window.jQuery) {
    document.write('<script src="../resource/com/js/libs/jquery-2.0.2.min.js"><\/script>');
    //}

</script>
<!--
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>-->
<script>
    //if (!window.jQuery.ui) {
    document.write('<script src="../resource/com/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    //}
</script>
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

<!-- BOOTSTRAP JS -->
<script src="../resource/com/js/bootstrap/bootstrap.min.js"></script>

<!-- JSHOW JS-->
<script src="../resource/jshow/js/bootstrap-modalmanager.js"></script>
<script src="../resource/jshow/js/bootstrap-modal.js"></script>
<script src="../resource/jshow/js/jquery.ui.draggable.js"></script>
<script src="../resource/jshow/js/modal.manager.plugin1.0.js"></script>
<script src="../resource/jshow/js/jshow.utils.js"></script>

<!-- Jcrop JS-->
<script src="../resource/jquery/Jcrop/jquery.Jcrop.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="../resource/com/js/notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="../resource/com/js/smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="../resource/com/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="../resource/com/js/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="../resource/com/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="../resource/com/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="../resource/com/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="../resource/com/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!--
<script src="../resource/com/js/plugin/datepicker/bootstrap-datepicker.js"></script>
<script src="../resource/com/js/plugin/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>-->
<!-- browser msie issue fix -->
<script src="../resource/com/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- SmartClick: For mobile devices -->
<script src="../resource/com/js/plugin/smartclick/smartclick.js"></script>

<!-- highcharts -->
<script src="<%=path %>/resource/com/highcharts3/js/highcharts.js"></script>
<script type="text/javascript" src="<%=path %>/resource/com/highcharts3/js/themes/gray.js"></script>
<!--[if IE 7]>

<h1>你的浏览器已经过期，请重新下载</h1>

<![endif]-->



<!-- MAIN APP JS FILE -->
<script src="../resource/com/js/app.js"></script>
<!--在app.js之前加载init，确保左侧树形结构加载成功
<script src="../resource/activiti/init.js"></script>-->

<!-- umeditor -->
<script type="text/javascript" src="../resource/com/umeditor/umeditor.js"></script>
<script type="text/javascript" src="../resource/com/umeditor/umeditor.config.js"></script>
<script type="text/javascript" src="../resource/com/js/plugin/uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="../resource/com/common.js"></script>

<!-- multiselect-->
<script src="../resource/multiselect/js/bootstrap-multiselect.js"></script>

<!-- jqgrid multiselect-->
<script src="../resource/jqMultiSelect.js"></script>

<!-- datetimepicker -->
<script src="../resource/activiti/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="../resource/activiti/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<!-- timeLinr -->
<script src="../resource/activiti/timeLinr/js/jquery.mousewheel.js"></script>
<script src="../resource/activiti/timeLinr/js/jquery.timelinr.js"></script>
<!-- jqgrid -->
<script type="text/javascript" src="../resource/com/js/plugin/jqgrid/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../resource/com/js/plugin/jqgrid/jquery.jqGrid.min.js"></script>

<!-- ztree -->
<script type="text/javascript" src="../resource/com/js/plugin/ztree/js/jquery.ztree.all-3.5.min.js"></script>
<!-- echarts -->
<script type="text/javascript" src="../resource/com/js/plugin/echarts/echarts-plain.js"></script>
<!--tree select -->
<script src="../resource/activiti/treeSelect.js"></script>
<!--文件上传插件 -->
<script src="../resource/activiti/upload.js"></script>
<!--文件上传插件 -->
<script src="../resource/activiti/ipicture/jquery.ipicture.js"></script>
<!--金额格式化插件-->
<script src="../resource/activiti/accounting.min.js"></script>
<!--组织结构-->
<script src="../resource/activiti/orgChart/jquery.jOrgChart.js"></script>
<!--左侧滑动操作-->
<script src="../resource/activiti/leftview/leftview.js"></script>
<%--activiti定制appjs--%>
<script src="../resource/activiti/app.js"></script>
<%--activiti定制通用dialog弹出对话框--%>
<script src="../resource/activiti/dialog/oDialog.js"></script>




<script>
    var _hmt = _hmt || [];
    var umIndex = 0;
    $(function() {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?036b704c8a212107220e412a13d760a2";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    });
</script>
<script>
    $(function(){
        var url=$("#mainTree li:first a").attr("href");
        loadURL(url,$("#content"));
    });
</script>
<script type="text/javascript" src="http://api.map.baidu.com/api?ak=wSznDI1OGu0S1kvVU9GQLlZL&v=2.0"></script>

</html>
