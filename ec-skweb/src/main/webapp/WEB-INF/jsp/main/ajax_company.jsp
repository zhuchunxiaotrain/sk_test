<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i> 
				注册企业清单
			<span> 
				
			</span>
		</h1>
	</div>
</div>



							<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table  id="ajax_company_table"  mp-page="0" mp-keyword="">
				
			</table>
			<div id="ajax_company_page">
												
			</div>
		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->
		
<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();
	
	$(function(){
		run_jqgrid_function();
	})
	
	function run_jqgrid_function() {
		jQuery("#ajax_company_table")
				.jqGrid(
						{
							url : 'ajax-company!jqGrid.action',
							mtype : "GET",
							datatype : "json",
							page : 1,
							height : 'auto',
							colNames: ['企业名称','登录账号','关联手机','注册日期 <i class="fa fa-arrows-v"></i>','id','操作'],
							colModel : [
						{name:'name',index:'name', width:180,search:true,sortable:false},
						{name:'username',index:'username', width:120,search:true,fixed:true,sortable:false},
						{name:'usermobile',index:'usermobile', width:120,search:false,fixed:true,sortable:false},
						{name:'createDate',index:'createDate', width:84,fixed:true,search:false,sortable:true},
						
						{name:'id',index:'id', width:100,key:true,hidden:true,hidedlg:true,sortable:false}
						,{name:'act',index:'act', width:100,search:false,sortable:false,fixed:true}
						],
							rowNum : 10,
							rowList:[10,20,30],
							pager : '#ajax_company_page',
							sortname : 'createDate',
							sortorder : "desc",
							gridComplete : function() {
								var ids = jQuery("#ajax_company_table")
										.jqGrid('getDataIDs');
								for ( var i = 0; i < ids.length; i++) {
									var cl = ids[i];
									
									se = "<button class='btn btn-xs btn-default' data-original-title='编辑' onclick=\"fn_product_edit('"
											+ cl
											+ "');\"><i class='fa fa-edit'></i></button> ";
									ca = "<button class='btn btn-xs btn-default' data-original-title='删除' onclick=\"fn_product_delete('"
											+ cl
											+ "');\"><i class='fa fa-trash-o'></i></button> ";
									//ce = "<button class='btn btn-xs btn-default' onclick=\"jQuery('#jqgrid').restoreRow('"+cl+"');\"><i class='fa fa-times'></i></button>"; 
									//jQuery("#jqgrid").jqGrid('setRowData',ids[i],{act:be+se+ce});
									
									//jQuery("#ajax_company_table").jqGrid('setRowData',ids[i], {act : se + ca});
									
								}
							},
							caption : "<i class='fa fa-arrow-circle-right'></i> 注册企业清单",
				          	jsonReader: {    
				    				root: "dataRows",
				    				page: "page",
				    	 			total: "total",
				    				records: "records",
				    				repeatitems : false
				    		},
							multiselect : false,
				    		rownumbers:true,
				    		gridview:true,
				    		shrinkToFit:true,
				        	viewrecords: true,
				        	autowidth: true,
							loadComplete: function() {
								
							}
						});
		
		// update buttons
		$(window).on('resize.jqGrid', function() {
			jQuery("#ajax_company_table").jqGrid('setGridWidth', $("#content").width()-10);
		})

		jQuery("#ajax_company_table").jqGrid('navGrid', "#ajax_company_page", {
			edit : false,
			add : false,
			del : false,
			search:false
		});
		
		jQuery("#ajax_company_table").jqGrid('filterToolbar',{searchOperators : false});
		/* 
		 */

		jqGridStyle();
	} // end function


	
</script>
