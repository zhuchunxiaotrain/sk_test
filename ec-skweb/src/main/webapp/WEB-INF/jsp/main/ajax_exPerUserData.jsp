<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<input type="hidden" id="loginId" name="loginId" value="<s:property value="users.adminType"/>"/>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">
		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<input type="hidden" id="selectedIds" value=""/>
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
				data-widget-colorbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="true"
				data-widget-custombutton="false"
				data-widget-sortable="false">

				<%--<header>--%>
					<%--<span class="widget-icon"> <i class="fa fa-table"></i> </span>--%>
					<%--<h2>成员个人信息 </h2>--%>
				<%--</header>--%>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class=" ">
                                <div class="row" id="ajax_users_list_row">

						            <table id="ajax_users_table" class="table table-striped table-bordered table-hover">
						            </table>
                                    <div id="ajax_users_list_page">
						            </div>
                                </div>

						    </div>
                        </div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<div id="dialog-users_message">

</div><!-- #dialog-message -->

<div id="dialog-message">

</div><!-- #dialog-message -->

<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	//添加新成员
	$('#ajax_users_btn_add').click(function() {
	    var this_tr = $(this).parent().parent();
        gDialog.fCreate({
		    title:'添加成员信息',
		    url:"<%=path%>/com/ajax-users!input.action?keyId=" + $(this_tr).attr("id"),
		    width:500
		}).show();
	});




    //删除成员账户
function fn_delete(id){
	$.SmartMessageBox({
		title : "<i class='fa fa-minus-square txt-color-orangeDark'></i> <span class='txt-color-orangeDark'><strong>删除后该成员账号将不可登陆</strong></span>",
		content : "确定删除该成员账号吗？",
		buttons : '[取消][确定]'
	}, function(ButtonPressed) {
		if (ButtonPressed === "确定") {
			var vActionUrl = "<%=path%>/com/ajax-users!delete.action";
			ajax_action(vActionUrl,{keyId:id},{},function(pdata){
				_show(pdata);
			});
			jQuery("#ajax_users_table").trigger("reloadGrid");
		}
	});
}
	function fn_cancel(id){
		$.SmartMessageBox({
			title : "<i class='fa fa-minus-square txt-color-orangeDark'></i> <span class='txt-color-orangeDark'><strong>确定解除该用户的微信账号绑定吗？</strong></span>",
			content : "",
			buttons : '[取消][确定]'
		}, function(ButtonPressed) {
			if (ButtonPressed === "确定") {
				var vActionUrl = "<%=path%>/com/ajax-users!bindcancel.action";
				ajax_action(vActionUrl,{keyId:id},{},function(pdata){
					_show(pdata);
				});
				jQuery("#ajax_users_table").trigger("reloadGrid");
			}
		});
	}

    //编辑成员账户
    function fn_edit(id){
        gDialog.fCreate({
            title:'编辑成员信息',
            url:"<%=path%>/com/ajax-users!input.action?keyId=" + id,
            width:500
        }).show();
    }
</script>
<script type="text/javascript">
	pageSetUp();
    $(function(){
        load_users_jqGrid();
    });
    function imgFormat(cellvalue, options, cell){
          if(cellvalue != ""){
              var imgUrl = '<img style="border-radius: 50%;" height="30" width="30" alt="" src="'+cellvalue+'">';
          }else{
              var imgUrl = '<img style="border-radius: 50%;" height="30" width="30" alt="" src="../resource/com/img/avatars/male.png">';
          }
          return imgUrl;
    };
	function typeInfo(cellvalue, options, cell){
		if(cellvalue!=""){
			var role = "";
			if(cellvalue.indexOf("1") !=-1){
				role=role+"前台人员";
			}
			if (cellvalue.indexOf("2") !=-1){
				role= cellvalue.indexOf("1") !=-1 ? role+"," : "";
				role=role+"后台管理人员";
            }
			return role;
		}
		return cellvalue;
	}

	function btnSetUser(cellvalue, options, cell){
		if(cellvalue!=""){
			var id=cell["id"];
			var isBind=cell["isBind"];
			var typ = cell["isCurrentUser"];
			var btn = "<button class='btn btn-default' data-original-title='编辑' onclick=\"fn_users_actions_edit('"+id+"');\"><i class='fa fa-edit'></i>编辑</button>";
			if (typ.substr(0,1) == "0") {
				btn = btn + " " + "<button class='btn btn-default' data-original-title='删除' onclick=\"fn_users_actions_delete('" + id + "');\"><i class='fa fa-times'></i>删除</button>";
			}
			btn = btn+"<button class='btn btn-danger btn-sm' data-original-title='重置密码' onclick=\"ajax_users_reset_pass('"+id+"');\"><i class='fa fa-lg fa-lock'></i>重置密码</button>";
            if(isBind){
			    btn = btn + " " + "<button class='btn btn-default' data-original-title='解绑' onclick=\"fn_users_actions_cancel('" + id + "');\"><i class='fa fa-times'></i>解绑</button>";
            }

			return btn;
		}
		return false;
	}

	function chargeFormat(cellvalue, options, cell){
		var id=cell["id"];
		var codeState = cell["codeStatus"];
		var typ = cell["isCurrentUser"];

		var btn = cellvalue+"天 ";
		if(cell["iscompany"] == "1") return btn;
		var chargeBtn = "<button class='btn btn-success btn-xs' data-original-title='充值' onclick=\"ajax_users_charge('"+id+"');\"><i class='fa fa-user-plus'></i>充值</button>";
		if(cellvalue == "0"){
			btn = btn + chargeBtn;
		}

		if(cellvalue=="0" || typ != 0) return btn;
		if(codeState == "使用中"){
			btn = btn + chargeBtn;
			btn = btn+"<button  class='btn btn-warning btn-xs' data-original-title='有效期转移' onclick=\"ajax_users_move('"+id+"');\"><i class='fa fa-exchange'></i>转移</button>";
			btn = btn+"<button  class='btn btn-danger btn-xs' data-original-title='有效期暂停' onclick=\"ajax_users_pause('"+id+"');\"><i class='fa fa-pause'></i>暂停</button>";
		}
		if(codeState == "暂停中"){
			btn = btn + chargeBtn;
			btn = btn+"<button  class='btn btn-danger btn-xs' data-original-title='有效期恢复' onclick=\"ajax_users_resume('"+id+"');\"><i class='fa fa-repeat'></i>恢复</button>";
		}


		return btn;
	}


    function load_users_jqGrid(){
        jQuery("#ajax_users_table").jqGrid({
            	url:'ajax-users!jqGridData.action',
        	    datatype: "json",
            	colNames:["",'用户名称',"手机号","系统角色","注册日期","有效期","操作","id","isCurrentUser"],
            	colModel:[
				   {name:'userImg',index:'userImg', width:20,sortable:false,formatter:imgFormat,search:false},
				   {name:'name',index:'name', width:40,search:true,searchoptions:{sopt: ["cn"],clearSearch:true}},
				   {name:'mobile',index:'mobile', width:80,search:true,searchoptions:{sopt: ["cn"],clearSearch:true}},
				   {name:'adminType',index:'adminType', width:100,sortable:false,formatter:typeInfo,search:false},
                   {name:'createDate',index:'createDate', width:50,search:false},
				   {name:'validPeriod',index:'validPeriod', width:60,search:false,formatter:chargeFormat},
				   {name:'act',index:'act', width:350,sortable:false,fixed:true,formatter:btnSetUser,search:false},
				   {name:'id',index:'id',hidden:true,search:false},
					{name:'isCurrentUser',index:'isCurrentUser',hidden:true,search:false}
             	],
                rowNum : 10,
                rowList:[10,20,30],
                pager : '#ajax_users_list_page',
                sortname : 'isCompany',
                sortorder : "desc",
                gridComplete:function(){
					var dIds=$("#ajax_client_table").jqGrid('getDataIDs');
					var sIds = $("#selectedIds").val();
					for(var i=0;i<dIds.length;i++){
						if(sIds.indexOf(dIds[i])!=-1) $("#ajax_client_table").jqGrid("setSelection", dIds[i]);
					}
                    jqGridStyle();
                },
    	        jsonReader: {
    			    root: "dataRows",
    			    page: "page",
    			    total: "total",
    			    records: "records",
    			    repeatitems : false
    			},
    			multiselect : true,
				caption : "<i class='fa fa-arrow-circle-right'></i> 组织成员",
                rownumbers:true,
                gridview:true,
                shrinkToFit:true,
                viewrecords: true,
                autowidth: true,
                height:'auto',
                forceFit:true,
				onSelectRow:function(rowId, status, e){
					optGrid(rowId, status)
				},
				onSelectAll:function(aRowids, status, e){
					for(var i=0;i<aRowids.length;i++){

						optGrid(aRowids[i], status);
					}
				},
                loadComplete: function() {
                }
            });
            $(window).on('resize.jqGrid', function() {
                jQuery("#ajax_users_table").jqGrid('setGridWidth', $("#ajax_users_list_row").width());
            })
			jQuery("#ajax_users_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});
            jQuery("#ajax_users_table").jqGrid('navGrid', "#ajax_users_list_page", {
                edit : false,
            	add : false,
            	del : false,
            	search:false
            });
	};

	function optGrid(rowId, status){
		var ids = $("#selectedIds").val();
		ids = status==true ? (ids.indexOf(rowId) == -1? ids + rowId+",":ids) : ids.replace((rowId+","),"");
		$("#selectedIds").val(ids);
	}

     //编辑
    function fn_users_actions_edit(id){
        fn_edit(id);
    };
    //删除
    function fn_users_actions_delete(id){
         fn_delete(id);
    };
	function fn_users_actions_cancel(id){
		fn_cancel(id);
	};

    function ajax_users_reset_pass(id){
        $.SmartMessageBox({
            title : "<i class='fa fa-plus-square txt-color-orangeDark'></i>  重置  <span class='txt-color-orangeDark'><strong> 密码，请谨慎操作！ </strong></span> ",
            content : "请输入一个新密码",
            buttons : "[取消][确定]",
            input : "text",
            placeholder : "请输入新密码..."
        }, function(ButtonPress, Value) {
            if(ButtonPress=="确定"){
                if(Value == ""){
                    $.smallBox({
                        title : "操作失败",
                        content : "<i class='fa fa-clock-o'></i> <i>"
                        + "密码不能为空！" + "</i>",
                        color : "#C46A69",
                        iconSmall : "fa fa-times fa-2x fadeInRight animated",
                        timeout : 3000
                    });
                }else{
                    var vActionUrl = "<%=path%>/com/ajax-users!reset.action";
                    var data={keyId:id,password:Value};
                    ajax_action(vActionUrl,data,{},function(pdata){
                        _show(pdata);
                    });
                }
            }
        });
    }


	function ajax_users_charge(id){
		$.SmartMessageBox({
			title : "充值码",
			content : "请输入充值码",
			buttons : "[确定][取消]",
			input : "text",
			placeholder : "请输入充值码"
		}, function(ButtonPress, Value) {
			if(ButtonPress == "取消") return;
			var url = "<%=path%>/com/ajax-users!bindCode.action";
			ajax_action(url,{authcode:Value,keyId:id},null,function(data){
				_showResult(data);
				jQuery("#ajax_users_table").trigger("reloadGrid");
			});
		});

	}

	function ajax_users_move(id){
		gDialog.fCreate({
			title:"有效期转移",
			url:"<%=path%>/com/ajax-users!move.action",
			param:{keyId:id}
		}).show();
	}

	function ajax_users_pause(id){
		$.SmartMessageBox({
			title : "有效期中止",
			content : "人员不会被删除，但是该人员无法登录",
			buttons : "[确定][取消]"
		}, function(ButtonPress, Value) {
			if(ButtonPress == "取消") return;
			var url = "<%=path%>/com/ajax-users!pauseCode.action";
			ajax_action(url,{keyId:id},null,function(data){
				_showResult(data);
				jQuery("#ajax_users_table").trigger("reloadGrid");
			});
		});
	}
	function ajax_users_resume(id){

			var url = "<%=path%>/com/ajax-users!resumeCode.action";
			ajax_action(url,{keyId:id},null,function(data){
				_showResult(data);
				jQuery("#ajax_users_table").trigger("reloadGrid");
			});
	}


</script>
<script type="text/javascript">

	$("#ajax_users_btn_charge").off("click").on("click",function(){
		var ids = $("#selectedIds").val();
		if(ids==""){
			_show({state:400,message:"请选择人员",title:"操作状态"});
			return;
		}

		var arr = ids.split(",");
		var len = arr.length;
		$.SmartMessageBox({
			title : "帐号充值确认",
			content : "我们即将为你充值"+(len-1)+"个帐号",
			buttons : "[确定][取消]"
		}, function(ButtonPress, Value) {
			if(ButtonPress == "确定"){
				var url = "<%=path%>/com/ajax-users!bindMultiCode.action";
				var data = {keyId : ids};
				ajax_action(url,data,null,function(data){
					$("#selectedIds").val("");
					_showResult(data);
					$("#ajax_users_table").trigger("reloadGrid");
				});
			}
		});

	})
</script>
