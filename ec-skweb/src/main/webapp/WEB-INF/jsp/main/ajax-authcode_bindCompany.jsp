
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<div class="modal-body">
		<form class="form-horizontal" action="" method="post" id="ajax_bindcom_form">
			<input type="hidden" name="keyId" id="keyId" value="<s:property value="keyId" />">
			<fieldset>
				<legend>
					【当前<s:property value="num" />个充值码签署合同】
				</legend>

				<div class="form-group">
					<label class="col-md-3 control-label" for="companyId"><i class="fa fa-arrow-right"></i> 签署公司</label>
					<div class="col-md-9">
						<select  class="select2" id="companyId" name="companyId">
							<optgroup label="请选择签署公司">
								<option value="">请选择...</option>
								<s:iterator value="companyList" id="list">
									<option value="<s:property value="#list.id"/>"><s:property value="#list.name"/></option>
								</s:iterator>
							</optgroup>
						</select>
					</div>
				</div>
			</fieldset>
		</form>
</div>
<div class="modal-footer">
	<a role="button" class="btn btn-primary" id="ajax_out_btn"> <i class="fa fa-save"></i> 确定</a>
	<a href="#" class="btn btn-default"  data-dismiss="modal" aria-hidden="true">关闭</a>
</div>

<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

    $("#ajax_out_btn").off("click").on("click",function(e){
		if($("#companyId").val()==""){
			_show({
				title : "操作失败",
				state:"400",
				message:"请选择签署公司"
			})
			return ;
		};
		var keyId = $("#keyId").val();
		var companyId = $("#companyId").val();
		var url = "ajax-authcode!bindCom.action";
		var data = {keyId:keyId,companyId:companyId};
		ajax_action(url,data,null,function(data){
			gDialog.fClose();
			$("#selectedIds").val("");
			$("#ajax_authcode_table").trigger("reloadGrid");
			_showResult(data);

		});
	});
</script>