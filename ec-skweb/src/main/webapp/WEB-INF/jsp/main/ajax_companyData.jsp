<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i> 
				企业数据监控
			<span> 
				
			</span>
		</h1>
	</div>
</div>
<!-- row -->
<div class="row">
		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row" id="ajax_companyData_list_row">
				<table id="ajax_companyData_table" class="table table-striped table-bordered table-hover">
				</table>
				<div id="ajax_companyData_page">
				</div>
			</div>

		</article>
		<!-- WIDGET END -->
</div>
<!-- end row -->
<div id="ajax_product_dialog_message" style="overflow:hidden">

</div>

<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	$(function(){
		run_jqgrid_function();
		$("#ajax_product_dialog_message").dialog({
			autoOpen : false,
			modal : true,
			title : "工程泛联客数据监控",
			width:940
		});
	})

	function membersFormat(cellvalue, options, rowDate){
		var id = rowDate.id;
			var se = "<button class='btn btn-sm btn-default' data-original-title='取消' onclick=\"fn_company_members('"
					+ id
					+ "');\">"+cellvalue+"</button> ";
			return se;
	}


	function run_jqgrid_function() {
		jQuery("#ajax_companyData_table")
				.jqGrid(
						{
							url : 'ajax-company!jqGridData.action',
							mtype : "GET",
							datatype : "json",
							page : 1,
							height : 'auto',
							colNames: ['企业名称','登录账号','手机号','注册日期 ','用户数量','项目数量','客户数量','联系人数量','id'],
							colModel : [
								{name:'name',index:'name',width:200,fixed:true,search:true,sortable:true,
									searchoptions:{sopt: ["cn"],clearSearch:true}},
								{name:'loginuser',index:'loginuser', width:200,fixed:true,search:false,sortable:true},
								{name:'phone',index:'phone', width:150,fixed:true,search:false,sortable:false},
								{name:'createDate',index:'createDate', width:150,fixed:true,search:false,sortable:false},
								{name:'usernum',index:'usernum', width:180,fixed:true,search:false,sortable:false},
								{name:'pronum',index:'pronum', width:180,fixed:true,search:false,sortable:false},
								{name:'clientnum',index:'clientnum', width:180,fixed:true,search:false,sortable:false},
								{name:'linkmannum',index:'linkmannum', width:150,fixed:true,search:false,sortable:false},
								{name:'id',index:'id', width:100,key:true,hidden:true,hidedlg:true,sortable:false}
							],
							rowNum : 10,
							rowList:[10,20,30],
							pager : '#ajax_companyData_page',
							sortname : 'createDate',

							sortorder : "desc",
							gridComplete : function() {
								/*
								var ids = jQuery("#ajax_companyData_table")
										.jqGrid('getDataIDs');
								for ( var i = 0; i < ids.length; i++) {
									var cl = ids[i];
									se = " <button class='btn btn-xs btn-primary' title='日历查看数据分布' onclick=\"fn_company_data('"
											+ cl
											+ "');\"><i class='fa fa-bar-chart'></i></button> ";
									var name = jQuery("#ajax_companyData_table").jqGrid('getRowData',cl).name;
									jQuery("#ajax_companyData_table").jqGrid('setRowData',ids[i], {name :se + name });

								}
								*/
							},
							caption : "<i class='fa fa-arrow-circle-right'></i> 企业数据监控",
				          	jsonReader: {
				    				root: "dataRows",
				    				page: "page",
				    	 			total: "total",
				    				records: "records",
				    				repeatitems : false
				    		},
							multiselect : false,
				    		rownumbers:true,
				    		gridview:true,
				    		shrinkToFit:true,
				        	viewrecords: true,
				        	autowidth: true,
							loadComplete: function() {

							}
						});

		// update buttons
		$(window).on('resize.jqGrid', function() {
			jQuery("#ajax_companyData_table").jqGrid('setGridWidth', $("#ajax_companyData_list_row").width());
		})
		jQuery("#ajax_companyData_table").jqGrid('navGrid', "#ajax_companyData_page", {
			edit : false,
			add : false,
			del : false,
			search:false
		});

		jQuery("#ajax_companyData_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});
		/*
		 */

		jqGridStyle();
	} // end function
	
	function fn_company_data(id){
		//alert(id)
		loadURL("ajax-company!dashboard.action?keyId=" +id, $('#ajax_product_dialog_message'))
		$('#ajax_product_dialog_message').dialog('open');

		$('.ui-dialog').css("top",10);
	}

	//打开成员的列表框
	function fn_company_members(id){
		gDialog.fCreate({title:'<i class="fa fa-calendar"></i> '+name + ' 成员列表 ',url:'ajax-members!list.action?keyId=' + id,width:1000}).show();
	}

	function fn_members_edit(id){
		gDialog.fCreate({title:'<i class="fa fa-calendar"></i> '+name + ' 设置成员有效期 ',url:'ajax-members!edit.action?keyId=' + id,width:550}).show();
	}

	function fn_members_listEdit(ids){
		gDialog.fCreate({title:'<i class="fa fa-calendar"></i> '+name + ' 设置成员有效期 ',url:'ajax-members!listEdit.action?keyIds=' + ids,width:550}).show();
	}

    function fn_members_listPay(ids){
        gDialog.fCreate({title:'<i class="fa fa-calendar"></i> '+name + ' 初始化成员支付有效期 ',url:'ajax-members!listPay.action?keyIds=' + ids,width:550}).show();
    }

</script>
