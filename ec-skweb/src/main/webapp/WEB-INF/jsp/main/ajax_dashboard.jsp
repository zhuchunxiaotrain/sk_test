<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> 监控仪表盘 <span></span></h1>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8" >
		<ul id="sparks" class="">
			<li class="sparks-info" style="padding-top:10px">
				<a href ="#ajax!company.action"><h5><i class="fa fa-fw fa-lg fa-credit-card"></i> 企业总数 <span class="txt-color-blue">&nbsp;<s:property value="companyNumber"/></span> </h5></a>
			</li>
			<li class="sparks-info" style="padding-top:10px">
				<h5>今日新增 <span class="txt-color-red">&nbsp;<s:property value="companyNumberToday"/></span></h5>
			</li>
			
			<li class="sparks-info" style="padding-top:10px">
				<a href ="#ajax!users.action"><h5> <i class="fa fa-fw fa-lg fa-user"></i> 用户总数 <span class="txt-color-blue">&nbsp;<s:property value="usersNumber"/></span> </h5></a>
			</li>
			<li class="sparks-info" style="padding-top:10px">
				<h5>今日新增<span class="txt-color-red">&nbsp; <s:property value="usersNumberToday"/></span></h5>
			</li>
		</ul>
	</div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">
		<article class="col-sm-12 col-md-12 col-lg-12">
			<!-- new widget -->
			<div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-bar-chart-o txt-color-darken"></i> </span>
					<h2>系统用户数增长情况 </h2>
					<div class="widget-toolbar">
						<!-- add: non-hidden - to disable auto hide -->
						<div class="btn-group">
							<button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
								显示周期 <i class="fa fa-caret-down"></i>
							</button>
							<ul class="dropdown-menu js-status-update pull-right">
								<li>
									<a href="javascript:void(0);" id="mt">30天</a>
								</li>
								<li>
									<a href="javascript:void(0);" id="ag">12个月</a>
								</li>
							</ul>
						</div>
					</div>
				</header>

				<!-- widget div-->
				<div >
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						
					</div>
					<!-- end widget edit box -->
					<div class="widget-body no-padding">
						<div id="userschart" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
							
					</div>

				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->
		</article>
	</div>
	<!-- end row -->
	
	<!-- row -->

	<div class="row">
		<article class="col-sm-12 col-md-12 col-lg-12">
			<!-- new widget jarviswidget-color-blueDark-->
			<div class="jarviswidget " id="wid-id-301" data-widget-colorbutton="false" data-widget-editbutton="false">

				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"
				-->
				<%--<header>--%>
					<%--<span class="widget-icon"> <i class="fa fa-calendar"></i> </span>--%>
					<%--<h2> 销售七步数据监控 </h2>--%>
					<%--<div class="widget-toolbar">--%>
						<%----%>
					<%--</div>--%>
				<%--</header>--%>

				<!-- widget div-->
				<div>
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">

						<input class="form-control" type="text">

					</div>
					<!-- end widget edit box -->

					<%--<div class="widget-body no-padding">--%>
						<%--<!-- content goes here -->--%>
						<%--<div class="widget-body-toolbar">--%>

							<%--<div id="calendar-buttons">--%>

								<%--<div class="btn-group">--%>
									<%--<a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-prev"><i class="fa fa-chevron-left"></i></a>--%>
									<%--<a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-next"><i class="fa fa-chevron-right"></i></a>--%>
								<%--</div>--%>
							<%--</div>--%>
						<%--</div>--%>

						<%--<!-- end content -->--%>
					<%--</div>--%>

				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->

		</article>
	</div>

	<!-- end row -->

<!-- end row -->
<div id="dialog-message">

</div><!-- #dialog-message -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();
	/*
	 * FULL CALENDAR JS
	 */
	
	// calendar month
	$('#mt').click(function () {
		$.getJSON('<%=path %>/main/ajax-users!chart.action?type=month', function(data) {
			data5=data.d1;
			months=data.d2;
			userschart.destroy();
			fn_userschart();
		});
	});
	
	// calendar agenda week
	$('#ag').click(function () {
		$.getJSON('<%=path %>/main/ajax-users!chart.action?type=year', function(data) {
			data5=data.d1;
			months=data.d2;
			userschart.destroy();
			fn_userschart();
		});
	});
	// Load Calendar dependency then setup calendar
	//loadScript("../resource/com/js/plugin/fullcalendar/jquery.fullcalendar.min.js", setupCalendar);

    $(function(){
        setupCalendar();
    })
	function setupCalendar() {
	
	    if ($("#calendar").length) {
	        var date = new Date();
	        var d = date.getDate();
	        var m = date.getMonth();
	        var y = date.getFullYear();
	
	        var calendar = $('#calendar').fullCalendar({
	        	// display
		    	defaultView: 'month',
		    	aspectRatio: 1.35,
		    	weekends: true,
		    	
		    	allDayDefault: true,
		    	ignoreTimezone: true,
		    	
		    	// event ajax
		    	lazyFetching: true,
		    	startParam: 'start',
		    	endParam: 'end',
		    	
		    	// time formats
		    	titleFormat: {
		    		month: 'MMMM yyyy',
		    		week: "MMM d[ yyyy]{ '&#8212;'[ MMM] d yyyy}",
		    		day: 'dddd, MMM d, yyyy'
		    	},
		    	columnFormat: {
		    		month: 'ddd',
		    		week: 'ddd M/d',
		    		day: 'dddd M/d'
		    	},
		    	timeFormat: { // for event elements
		    		'': 'h(:mm)t' // default
		    	},
		    	
		    	// locale
		    	isRTL: false,
		    	firstDay: 0,
		    	monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
		    	monthNamesShort: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
		    	dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
		    	dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
		    	buttonText: {
		    		prev: '&nbsp;&#9668;&nbsp;',
		    		next: '&nbsp;&#9658;&nbsp;',
		    		prevYear: '&nbsp;&lt;&lt;&nbsp;',
		    		nextYear: '&nbsp;&gt;&gt;&nbsp;',
		    		today: '今天',
		    		month: '一月',
		    		week: '一周',
		    		day: '一天',
		    		create:'新建日程'
		    	},
		    	
		    	// jquery-ui theming
		    	theme: false,
		    	
		    	//selectable: false,
		    	unselectAuto: true,
		    	
		    	dropAccept: '*',
		 
	            editable: false,
	            draggable: false,
	            selectable: false,
	            selectHelper: true,
	            unselectAuto: false,
	            disableResizing: false,
	
	            header: {
	                left: 'title', //,today
	                center: 'prev, next, today',
	                right: 'month, agendaWeek, agenDay' //month, agendaDay,
	            },
	
	            select: function (start, end, allDay) {
	                var title = prompt('Event Title:');
	                if (title) {
	                    calendar.fullCalendar('renderEvent', {
	                            title: title,
	                            start: start,
	                            end: end,
	                            allDay: allDay
	                        }, true // make the event "stick"
	                    );
	                }
	                calendar.fullCalendar('unselect');
	            },
	            events: "ajax-company!dashboard7Count.action",
	            eventClick: function(calEvent, jsEvent, view) {

				    // change the border color just for fun
				    $(this).css('border-color', 'red');
				
				},
	            eventRender: function (event, element, icon) {
	                if (!event.description == "") {
	                    element.find('.fc-event-title').append("<br/><span class='ultra-light'>" + event.description +
	                        "</span>");
	                }
	                if (!event.icon == "") {
	                    element.find('.fc-event-title').append("<i class='air air-top-right fa " + event.icon +
	                        " '></i>");
	                }
	            }
	        });
	
	    };
	
	    /* hide default buttons */
	    $('.fc-header-right, .fc-header-center').hide();
	
	}
	
	// calendar prev
	$('#calendar-buttons #btn-prev').click(function () {
	    $('.fc-button-prev').click();
	    return false;
	});
	
	// calendar next
	$('#calendar-buttons #btn-next').click(function () {
	    $('.fc-button-next').click();
	    return false;
	});
	
	// calendar today
	$('#calendar-buttons #btn-today').click(function () {
	    $('.fc-button-today').click();
	    return false;
	});
	
	$.getJSON('<%=path %>/main/ajax-users!chart.action', function(data) {
		data5=data.d1;
		months=data.d2;
		fn_userschart();
	});
	
	//contract_moneychart
	var userschart;
	function fn_userschart(){
		//contract_moneychart
		userschart = new Highcharts.Chart({
	        chart: {
	            renderTo: 'userschart',
	            marginRight: 130,
	            marginBottom: 75,
				events: {
					addSeries: function() {
					}
				}
	        },
	        title: {
	            text: '系统用户增长情况',
	            style: {
					color: '#fff',
					fontSize: '18px',
					fontWeight: 'bold'
				}
	        },
	        subtitle: {
	            //text: '按创建时间',
	            x: 0
	        },
	        xAxis: {
	            categories: months,
	            labels: {
	                rotation: -35,
	                align: 'right',
	                style: {
	                    color:'#fff',fontWeight: ''
	                }
	            }
	        },
	        yAxis: [{
	            title: {
	                text: '单位：（个）'
	            },
	            plotLines: [{
	                value: 0,
	                width: 0,
	                color: '#808080'
	            }],
	            labels: {
	                style: {
	                    color:'#fff',fontWeight: 'bold'
	                }
	            }
	        }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
	        tooltip: {
	            formatter: function() {
	                    return '<b>'+ this.series.name +' 在 '+this.x+' 为 '+
	                   this.y +' </b><br />点击右侧名称可切换显示<em></em>';
	            }
	        },
	        plotOptions: {
	            column: {
	                cursor: 'pointer',
	                point: {
	                    events: {
	                        click: function(){
	                        	 //alert ('月份: '+ this.category +', value: '+ this.y);
	                        	 //loadSupplierMonth(this.category);
	                        }
	                    }
	                }
	             },
	             line: {
	                cursor: 'pointer',
	                point: {
	                    events: {
	                        click: function(){
	                        	 //alert ('月份: '+ this.category +', value: '+ this.y);
	                        	 //loadSupplierMonth(this.category);
	                        }
	                    }
	                }
	             }
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'top',
	            x: 10,
	            y: 150,
	            borderWidth: 0
	        },
			series:data5
	    });
	}
</script>