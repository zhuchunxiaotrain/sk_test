<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<div class="inbox-nav-bar no-content-padding">
	<h1 class="page-title txt-color-blueDark hidden-tablet"><i class="fa fa-fw fa-list"></i> 系统字典 &nbsp;</h1>
	<div class="inbox-checkbox-triggered">
		<div class="btn-group">
			<h5 class=" txt-color-blueDark hidden-tablet">当前字典：<i class="fa fa-fw fa-arrow-right"></i> <span id="ajax_dict_name_selected"> 正在加载...</span>  &nbsp;</h5>
		</div>
	</div>


</div>

<div id="inbox-content" class="inbox-body no-content-padding">
	<div class="inbox-side-bar">
		<a href="javascript:void(0);" id="dict_new_option" class="btn btn-primary btn-block"> <strong>添加新选项</strong> </a>

		<h6> 字典列表 <a href="javascript:loadInbox();" rel="tooltip" title="" data-placement="right" data-original-title="刷新选项" class="pull-right txt-color-darken"><i class="fa fa-refresh"></i></a></h6>

		<ul class="inbox-menu-lg" id="ajax_dict_menulist">
			<li class="active">
				<a key="ClientType"  href="javascript:void(0);">客户性质</a>
			</li>
			<li>
				<a key="ProInfoCategory"  href="javascript:void(0);">项目类别</a>
			</li>
			<li>
				<a key="ProBackCategory"  href="javascript:void(0);">反馈类别</a>
			</li>
            <li>
                <a key="SuperKnowledgeType"  href="javascript:void(0);">知识大类</a>
            </li>
            <li>
                <a key="SubKnowledgeType"  href="javascript:void(0);">知识小类</a>
            </li>
		</ul>
	</div>

	<div class="table-wrap custom-scroll animated fast fadeInRight">
		<!-- ajax will fill this area -->
		

	</div>
</div>

<script type="text/javascript">
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    pageSetUp();


    loadScript("../resource/com/js/plugin/delete-table-row/delete-table-row.js");
    /*
     * Fixed table height
     */
    tableHeightSize()
    $(window).resize(function() {
        tableHeightSize()
    })
    function tableHeightSize() {
        var tableHeight = $(window).height() - 172;
        $('.table-wrap').css('height', tableHeight + 'px');
    }

    //初始化部门显示信息
    var ajax_dictName = "ClientType";
    $("#ajax_dict_name_selected").html("客户性质");
    /*
     * LOAD INBOX MESSAGES
     */
    loadInbox();
    function loadInbox() {
        loadURL("ajax-dict!list.action?dictName="+ajax_dictName, $('#inbox-content > .table-wrap'))
    }
    /*
     * Buttons (compose mail and inbox load)
     */
    $(".inbox-load").click(function() {
        loadInbox();
    });

    $("#dict_new_option").click(function(e) {
        var text = $("ul#ajax_dict_menulist li.active").text();
        $.SmartMessageBox({
            title : "<i class='fa fa-plus-square txt-color-orangeDark'></i>  添加  <span class='txt-color-orangeDark'><strong> "+text+"</strong></span>",
            content : "请输入一个新选项名称",
            buttons : "[取消][确定]",
            input : "text",
            placeholder : "输入名称..."
        }, function(ButtonPress, Value) {
            //alert(ButtonPress + " " + Value);
            if(ButtonPress=="确定"){
                save("","save",Value);
            }
        });
        e.preventDefault();
    })
    $("#ajax_dict_menulist li").click(function() {
        $("#ajax_dict_menulist li.active").removeClass("active");
        $(this).addClass("active");
        $("#ajax_dict_name_selected").html($(this).text());
        ajax_dictName = $("a",$(this)).attr("key");
        loadInbox();
    });

    function fn_create(){

    }
    function fn_edit(id,did){
        $.SmartMessageBox({
            title : "编辑该选项",
            content : "请输入一个新名称",
            buttons : "[取消][确定]",
            input : "text",
            placeholder : "输入名称..."
        }, function(ButtonPress, Value) {
            //alert(ButtonPress + " " + Value);
            if(ButtonPress=="确定"){
                save(id,"edit",Value,did);
            }
        });
    }
    function fn_delete(id,did){
        $.SmartMessageBox({
            title : "删除该选项",
            content : "确定删除该选项吗？",
            buttons : '[取消][确定]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "确定") {
                save(id,"delete","",did);

            }
        });
    }
    function fn_disabled(id,did){
        $.SmartMessageBox({
            title : "禁用该选项",
            content : "确定禁用该选项吗？",
            buttons : '[取消][确定]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "确定") {
                save(id,"disabled","",did);
            }
        });
    }
    function fn_enable(id,did){
        $.SmartMessageBox({
            title : "启用该选项",
            content : "确定启用该选项吗？",
            buttons : '[取消][确定]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "确定") {
                save(id,"enable","",did);
            }
        });
    }

    function save(id,action,name,did){
        //ajax执行返回时触发
        var vActionUrl = "<%=path %>/com/ajax-dict!method.action";
        var data = {"dictName":ajax_dictName, "keyId":id,"type":action,"name":name,"did":did};
        ajax_action(vActionUrl,data,null,function(data){
            _show(data);
            loadInbox();
        })
    }

    //排序操作
    /**
     交换位置：交换sortNo，(下移到第一个置1，其他全部加1)，(或者上移到最后，其他全部减1)，通过Ajax一并JSON {id:sortNo},{id:sortNo}
     */
    function fn_dictSort(before,after,action){
        if(action=="up"){
            //判断，上移判断after是否是系统自带字典，原位置的自定义的邻居从1开始，后续减1，否则交换sortNo
            //alert(beford.html())
            //alert(after.html())
            if(after.attr("class")=="unread"){
                //设置before的sortNo=pre+1，after后面的自定义从1开始
                var beforePre = before.prev();
                if(beforePre.length > 0){
                    var beforePreSortNo = beforePre.attr("sortNo");
                    beforePreSortNo = parseInt(beforePreSortNo)+1;
                    before.attr("sortNo",beforePreSortNo);
                }else{
                    before.attr("sortNo",0);
                }

                var afterNext = after.next();
                var afterSortNo = parseInt(after.attr("sortNo"));
                while(afterNext.length > 0 && afterNext.attr("class")!="unread"){
                    afterSortNo++;
                    afterNext.attr("sortNo",afterSortNo);

                    afterNext = afterNext.next();
                }
            }else{
                //交换sortNo
                var beforeSortNo = before.attr("sortNo");
                var afterSortNo = after.attr("sortNo");
                before.attr("sortNo",afterSortNo);
                after.attr("sortNo",beforeSortNo);
            }
        }else{
            //down
            //判断，下移判断beford是否是系统自带字典，现位置的自定义的邻居从1开始，后续加1，否则交换sortNo

            if(before.attr("class")=="unread"){
                //设置before的sortNo=pre+1，after后面的自定义从1开始

                var beforeSortNo = before.attr("sortNo");
                beforeSortNo = parseInt(beforeSortNo)+1;
                after.attr("sortNo",beforeSortNo);

                var afterNext = after.next();
                var afterSortNo = parseInt(beforeSortNo);
                while(afterNext.length > 0 && afterNext.attr("class")!="unread"){
                    afterSortNo++;
                    afterNext.attr("sortNo",afterSortNo);

                    afterNext = afterNext.next();
                }
            }else{
                //交换sortNo
                var beforeSortNo = before.attr("sortNo");
                var afterSortNo = after.attr("sortNo");
                before.attr("sortNo",afterSortNo);
                after.attr("sortNo",beforeSortNo);
            }
        }

        //重排sortNo = 0 的列表
        var first = $("#ajax_dict_list_table tbody tr:first ");
        if(first.attr("class")!="unread"){
            //alert(first.attr("sortNo"))
            var afterSortNo = 0;
            first.attr("sortNo",0);
            var afterNext = first.next();
            while(afterNext.attr("class")!="unread"){
                afterSortNo++;
                afterNext.attr("sortNo",afterSortNo);

                afterNext = afterNext.next();
            }
        }

        //读取所有的JSON
        //计算提交的内容，读取
        var rowDatas = [];
        $("#ajax_dict_list_table tbody tr").each(function(i,v){
            if($(this).attr("class")!="unread"){
                var rowData = {id:$(this).attr("id"),sortNo:$(this).attr("sortNo")};
                rowDatas.push(rowData);
            }
        });

        var sortList = {sortList:rowDatas};
        if(rowDatas.length>0){
            var vActionUrl = "<%=path %>/com/ajax-dict!sort.action";
            $.ajax({url:vActionUrl,cache:false,dataType:"json",async:false,
                data:{"sortList":JSON.stringify(sortList),"dictName":ajax_dictName},
                success: function(data){
                    if(data.state=="200"){
                        //前台显示出来
                        $.smallBox({
                            title : "提示：",
                            content : "<i class='fa fa-clock-o'></i> <i>"+data.message+"</i>",
                            color : "#659265",
                            iconSmall : "fa fa-thumbs-up bounce animated",
                            timeout : 4000
                        });
                    }else{
                        $.smallBox({
                            title : "操作失败",
                            content : "<i class='fa fa-clock-o'></i> <i>"+data.message+"</i>",
                            color : "#C46A69",
                            iconSmall : "fa fa-times fa-2x fadeInRight animated",
                            timeout : 6000
                        });
                    }
                }
            });
        }

    }
</script>
