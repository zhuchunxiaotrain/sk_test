<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/7
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common2" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>预算调整报表</a>
        <s:if test="modifyYear==null || modifyYear.getProcessState().name()=='Draft'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
        </s:if>
        <s:if test="modifyYear!=null && modifyYear.getProcessState().name()=='Backed'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
        </s:if>
        <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
        <hr class="simple">
        <form id="modifyyear" class="smart-form" novalidate="novalidate" action="" method="post">
          <input type="hidden" name="parentId" id="parentId" value="<s:property value="calculationYear.id" />"/>
          <input type="hidden" name="keyId" id="keyId" value="<s:property value="modifyYear.id" />"/>
          <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
          <header  style="display: block;">
            预算调整报表&nbsp;&nbsp;<span id="title"></span>
          </header>
          <fieldset>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                原全年预算报表
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input  type="text" name="calculationYearText" id="calculationYearText" disabled value="<s:property value="calculationYear.name"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                全年预算报表
              </label>
              <section class="col col-5">
                <label class="input">
                  <input name="uploadify" id="newBudgetfileName" placeholder="" type="file" >
                  <input name="newBudgetfileId" id="newBudgetfileId" style="display: none" value="<s:property value="newBudgetfileId"/>">
                </label>
              </section>
            </div>

          </fieldset>
        </form>
      </div>
    </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#modifyyear #keyId").val(),
      flowName:"modifyyear",
      todo:"1"
    };
    multiDuty(pdata);

  });

  //上传
  inputLoad({
    objId:"newBudgetfileName",
    entityName:"newBudgetIds",
    sourceId:"newBudgetfileId",
    jsessionid:"<%=jsessionid%>"
  });

  //返回视图
  $("#btn-re-common2").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../finance/ajax!modifyyear.action",$('div#s2'));
    }

  });
  //校验
  $("#modifyyear").validate({
    rules : {
      calculationYearId : {
        required : true
      },
      newBudgetIds:{
        required : true
      }
    },
    messages : {
      calculationYearId : {
        required : '请选择原全年预决算报表'
      },
      newBudgetIds : {
        required : '请上传全年预算报表'
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });


  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("modifyyear","../finance/ajax-modifyyear!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../finance/ajax!modifyyear.action",$('div#s2'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(

          function(e) {

            if(!$("#modifyyear").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#modifyyear").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("modifyyear","../finance/ajax-modifyyear!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../finance/ajax!modifyyear.action",$('div#s2'));
                }
              }
            });
          }
  );


</script>
