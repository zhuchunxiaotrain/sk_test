<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/14
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>财务审计</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="">
              <a id="toNotice" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 审计通知书 </a>
            </li>
            <li class="active">
              <a id="toFirst" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计初稿 </a>
            </li>
            <li class="disabled">
              <a id="toLast" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计终稿 </a>
            </li>
            <li class="disabled">
              <a id="toEdit" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计整改通知单 </a>
            </li>
            <li class="disabled">
              <a id="toAnswer" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计答复 </a>
            </li>
          </ul>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
              <div class="row">
                <shiro:hasAnyRoles name="wechat">
                  <s:if test="!auditFirst.auditNotice.invalid">
                    <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(auditfirst.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
                  </s:if>
                </shiro:hasAnyRoles>
              </div>
              <hr class="simple">
              <form id="auditfirst" class="smart-form" novalidate="novalidate" action="" method="post">
                <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
                <input type="hidden" name="keyId" id="keyId" value="<s:property value="auditFirst.id" />"/>
                <input type="hidden" name="auditId" id="auditId" value="<s:property value="auditFirst.auditNotice.id" />"/>
                <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                <header  style="display: block;">
                  审计初稿&nbsp;&nbsp;<span id="title"></span>
                </header>
                <fieldset>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      审计初稿附件
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input name="uploadify" id="filename" style="display: none" placeholder="" type="file" >
                        <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      备注
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input  disabled  id="remarks" name="remarks" type="text" value="<s:property value="auditFirst.remarks"/>">
                      </label>
                    </section>
                  </div>

                </fieldset>

              </form>
              <div class="flow">
                <s:if test="auditFirst.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer"style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
          </div>
        </div>
      </div>
    </div>

  </article>
</div>

<script>

  $(function(){
    //toNotice
    $("a#toNotice").off("click").on("click",function(e) {
      loadURL("../finance/ajax-auditnotice!read.action?keyId="+$("#auditId").val()+"&step=<s:property value="step" />", $('div#content'));
    });

    //toLast
    var firstState = "<s:property value="auditFirst.getProcessState().name()" />";
    var firstCreater="<s:property value="isCreate('auditlast')"/>";
    var lastkeyId="<s:property value="auditFirst.auditNotice.auditLast.id" />";
    if(firstState == "Finished"&&(firstCreater!="style='display:none'"||lastkeyId!=null&&lastkeyId!="")){
      $('#toLast').parent('li').removeClass('disabled');
    }
    $("a#toLast").off("click").on("click",function(e) {
      if (firstState == "Finished"&&(firstCreater!="style='display:none'"||lastkeyId!=null&&lastkeyId!="")) {
        if(lastkeyId==null||lastkeyId==""){
          if(firstCreater!="style='display:none'"){
            loadURL("../finance/ajax-auditlast!input.action?auditId=" + $("#auditId").val()+"&step=<s:property value="step" />", $('div#content'));
          }
        }else{
          loadURL("../finance/ajax-auditlast!read.action?auditId=" + $("#auditId").val()+"&keyId="+lastkeyId+"&step=<s:property value="step" />", $('div#content'));
        }
      } else {
        return false;
      }
    });

    //toEdit
    var lastState = "<s:property value="auditFirst.getAuditNotice().getAuditLast().getProcessState().name()" />";
    var editCreater="<s:property value="isCreate('auditedit')"/>";
    var editkeyId="<s:property value="auditFirst.auditNotice.auditEdit.id" />";
    if(lastState == "Finished"&&(editCreater!="style='display:none'"||editkeyId!=null&&editkeyId!="")){
      $('#toEdit').parent('li').removeClass('disabled');
    }
    $("a#toEdit").off("click").on("click",function(e) {
      if (lastState == "Finished"&&(editCreater!="style='display:none'"||editkeyId!=null&&editkeyId!="")) {
        if(editkeyId==null||editkeyId==""){
          if(editCreater!="style='display:none'"){
            loadURL("../finance/ajax-auditedit!input.action?auditId=" + $("#auditId").val()+"&step=<s:property value="step" />", $('div#content'));
          }
        }else{
          loadURL("../finance/ajax-auditedit!read.action?auditId=" + $("#auditId").val()+"&keyId="+editkeyId+"&step=<s:property value="step" />", $('div#content'));
        }
      } else {
        return false;
      }
    });


    //toAnswer
    var table_global_width=0;
    var editState = "<s:property value="auditFirst.getAuditNotice().getAuditEdit().getProcessState().name()" />";
    if(editState == "Finished"){
      $('#toAnswer').parent('li').removeClass('disabled');
    }
    $("a#toAnswer").off("click").on("click",function(e) {
      if (editState == "Finished") {
        if (table_global_width == 0) {
          table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
        }
        loadURL("../finance/ajax-auditanswer!listAnswer.action?auditId=" + $("#auditId").val()+"&step=<s:property value="step" />"+"&width="+table_global_width, $('div#content'));
      } else {
        return false;
      }
    });

    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "auditfirst"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("auditfirst","../finance/ajax-auditfirst!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("auditfirst","../finance/ajax-auditfirst!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          if($("#auditfirst").valid()){
            form_save("auditfirst","../finance/ajax-auditfirst!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
              $("#btn-re-common").trigger("click");
            })
          }
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("auditfirst", "../finance/ajax-auditfirst!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../finance/ajax-auditfirst!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../finance/ajax-auditfirst!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });
  readLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId"
  });

  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../finance/ajax!auditnotice.action?step=<s:property value="step"/>",$('#content'));
    }

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    var auditId = "<s:property value="auditFirst.auditNotice.id" />";
    loadURL("../finance/ajax-auditfirst!input.action?keyId="+$("input#keyId").val()+"&auditId="+auditId+"&draft="+draft+"&step=<s:property value="step" />",$('#content'));
  });

</script>