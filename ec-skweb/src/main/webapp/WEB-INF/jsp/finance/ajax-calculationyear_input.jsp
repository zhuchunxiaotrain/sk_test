<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/7
  Time: 9:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>全年预决算报表</a>
          <s:if test="calculationYear==null || calculationYear.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="calculationYear!=null && calculationYear.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="calculationyear" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="calculationYear.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              全年预决算报表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  全年预算名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="cname" id="cname" value="<s:property value="calculationYear.name"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  全年预算报表
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="budgetfileName" placeholder="" type="file" >
                    <input name="budgetfileId" id="budgetfileId" style="display: none" value="<s:property value="budgetfileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  全年决算报表
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="finalAccountsfileName" placeholder="" type="file" >
                    <input name="finalAccountsfileId" id="finalAccountsfileId" style="display: none" value="<s:property value="finalAccountsfileId"/>">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";
  function getDefaultDept(data){
    $('#department').val(data.department);
    $('#departmentId').val(data.departmentId);
  }
  var time1 = setInterval("checkDuty()",1000);
  function checkDuty(){
    var curDutyId = $('#curDutyId').val();
    if(curDutyId != ""){
      clearInterval(time1);
      var vActionUrl = '../finance/ajax-calculationyear!getDefaultDept.action?curDutyId='+curDutyId;
      ajax_action(vActionUrl,null,null,getDefaultDept);
    }
  }
  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"calculationyear",
      todo:"1"
    };
    multiDuty(pdata);

  });

  //上传
  inputLoad({
    objId:"budgetfileName",
    entityName:"budgetIds",
    sourceId:"budgetfileId",
    jsessionid:"<%=jsessionid%>"
  });
  //上传
  inputLoad({
    objId:"finalAccountsfileName",
    entityName:"finalAccountsIds",
    sourceId:"finalAccountsfileId",
    jsessionid:"<%=jsessionid%>"
  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../finance/ajax!calculationyear.action",$('#content'));
    }
  });

  //校验
  $("#calculationyear").validate({
    rules : {
      budgetIds : {
        required : true
      },
      finalAccountsIds:{
        required : true
      },
      cname : {
        required : true
      }
    },
    messages : {
      budgetIds : {
        required : '请上传全年预算报表'
      },
      finalAccountsIds : {
        required : '请上传全年决算报表'
      },
      cname : {
        required : '请输入名称'
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("calculationyear","../finance/ajax-calculationyear!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../finance/ajax!calculationyear.action?viewtype=1",$('#content'));
            }
          }
  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#calculationyear").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#calculationyear").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("calculationyear","../finance/ajax-calculationyear!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../finance/ajax!calculationyear.action?viewtype=1",$('#content'));
                }
              }
            });
          }
  );
</script>