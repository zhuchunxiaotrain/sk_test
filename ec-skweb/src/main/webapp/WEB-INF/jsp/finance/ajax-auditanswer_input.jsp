<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/16
  Time: 10:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>审计答复</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="">
              <a id="toNotice" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 审计通知书 </a>
            </li>
            <li class="">
              <a id="toFirst" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计初稿 </a>
            </li>
            <li class="">
              <a id="toLast" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计终稿 </a>
            </li>
            <li class="">
              <a id="toEdit" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计整改通知单 </a>
            </li>
            <li class="active">
              <a id="toAnswer" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计答复 </a>
            </li>
          </ul>
          <hr class="simple">
          <form id="auditanswer" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="auditAnswer.id" />"/>
            <input type="hidden" name="auditId" id="auditId" value="<s:property value="auditId" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <s:if test="auditAnswer==null || auditAnswer.getProcessState().name()=='Draft'">
              <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
            </s:if>
            <s:if test="auditAnswer!=null && auditAnswer.getProcessState().name()=='Backed'">
              <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
            </s:if>
            <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
            <header  style="display: block;">
              审计整改通知单&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  审计答复类型
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="auditType" value="1" <s:property value="auditAnswer.auditType==1?'checked':''"/>>
                      <i></i>审计计划
                    </label>

                    <label class="radio">
                      <input type="radio" name="auditType" value="2" <s:property value="auditAnswer.auditType==2?'checked':''"/>>
                      <i></i>审计结果
                    </label>
                  </div>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  审计答复附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="remarks" id="remarks" type="text" placeholder="请输入备注" value="<s:property value="auditAnswer.remarks"/>"/>
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){

    //toNotice
    $("a#toNotice").off("click").on("click",function(e) {
      loadURL("../finance/ajax-auditnotice!read.action?keyId="+$("#auditId").val()+"&step=<s:property value="step" />", $('div#content'));
    });

    //toFirst
    $("a#toFirst").off("click").on("click",function(e) {
      var firstId="<s:property value="auditNotice.auditFirst.id" />";
      loadURL("../finance/ajax-auditfirst!read.action?keyId="+firstId+"&step=<s:property value="step" />", $('div#content'));
    });

    //toLast
    $("a#toLast").off("click").on("click",function(e) {
      var lastId="<s:property value="auditNotice.auditLast.id" />";
      loadURL("../finance/ajax-auditlast!read.action?keyId="+lastId+"&step=<s:property value="step" />", $('div#content'));
    });

    //toEdit
    $("a#toEdit").off("click").on("click",function(e) {
      var editId="<s:property value="auditNotice.auditEdit.id" />";
      loadURL("../finance/ajax-auditedit!read.action?keyId="+editId+"&step=<s:property value="step" />", $('div#content'));
    });

    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"auditanswer",
      todo:"1"
    };
    multiDuty(pdata);

  });
  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });

  //校验
  $("#auditanswer").validate({
    rules : {
      auditType : {
        required : true
      },
      fileIds : {
        required : true
      }
    },
    messages : {
      auditType : {
        required : '请选择审计答复类型'
      },
      fileIds : {
        required : "请上传审计整改通知单附件"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  //返回视图
  $("#btn-re-common").click(function(){
    loadURL("../finance/ajax-auditanswer!listAnswer.action?auditId="+$("#auditId").val()+"&step=<s:property value="step" />",$('#content'));
  });

  //保存
  $("#btn-save-common").click(
          function(){
            //$("#btn-save-common").attr("disabled", "disabled");
            form_save("auditanswer","../finance/ajax-auditanswer!save.action",null,function(data){
              if(data.state==200){
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../finance/ajax!auditanswer.action?step=1",$('#content'));
                }
              }
            });

          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#auditanswer").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#auditanswer").valid();
                if(!$validForm) return false;
//                $("#btn-confirm-common").attr("disabled", "disabled");
//                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("auditanswer","../finance/ajax-auditanswer!commit.action",null,function(data){
                  if(data.state==200){
                    if(draft == 1){
                      location.href='index.action';
                    }else{
                      loadURL("../finance/ajax!auditanswer.action?step=1",$('#content'));
                    }
                  }
                });
              }
            });
          }
  );

</script>