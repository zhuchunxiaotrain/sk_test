<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/13
  Time: 10:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      财务审计
    </h1>
  </div>
</div>

<div id="auditnotice_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <shiro:hasAnyRoles name="wechat">
            <a id="ajax_auditnotice_btn_add" <s:property value="isCreate('auditnotice')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建财务审计过程套表</a>
            <s:if test="step==1">
              <a id="ajax_auditnotice_invalid" <s:property value="isCreate('auditnotice')"/> class="btn btn-default pull-right" href="javascript:void(0)"> <i class="fa fa-lg fa-hand-o-right"></i> 完成 </a>
            </s:if>
          </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_auditnotice_list_row">
                    <table id="ajax_auditnotice_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_auditnotice_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_auditnotice_jqGrid();
    //完成操作
    $('#ajax_auditnotice_invalid').click(function(){
      var ids=$("#ajax_auditnotice_table").jqGrid('getGridParam','selarrrow');
      if(ids.length != 1){
        alert("请选择一条财务审计");
        return false;
      }
      $.tzDialog({title:"提示",content:"确认要完成此财务审计吗?",callback:function(){
        var vActionUrl = "<%=path%>/finance/ajax-auditnotice!doInvalid.action";
        var data={keyId:ids[0]};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
        });
        jQuery("#ajax_auditnotice_table").trigger("reloadGrid");
      }});
    });

  });


  function load_auditnotice_jqGrid(){
    var step="<s:property value="step" />";
    var isHidden=false;
    if(step==2){
      isHidden=true;
    }
    jQuery("#ajax_auditnotice_table").jqGrid({
      url:'../finance/ajax-auditnotice!list.action?step='+"<s:property value="step" />",
      datatype: "json",
      colNames:['审计类型',"被审计单位",'审计日期',"当前进度","文档状态","操作","id"],
      colModel:[
        {name:'type',index:'type', width:80,sortable:false,search:false},
        {name:'department',index:'department', width:80,search:false,sortable:true},
        {name:'date',index:'date',sortable:false,search:false,width:80},
        {name:'step',index:'step', search:false,sortable:true,width:80},
        {name:'state',index:'state', search:false,sortable:true,width:80,hidden:isHidden},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_auditnotice_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        var ids=$("#ajax_auditnotice_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_auditnotice_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_auditnotice_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_auditnotice_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_auditnotice_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_auditnotice_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 审计通知书",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_auditnotice_table").jqGrid('setGridWidth', $("#ajax_auditnotice_list_row").width());
    })
    jQuery("#ajax_auditnotice_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_auditnotice_table").jqGrid('navGrid', "#ajax_auditnotice_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_auditnotice_read(id){
    loadURL("../finance/ajax-auditnotice!read.action?keyId="+id+"&step="+"<s:property value="step" />",$('#content'));
  }

  $("#ajax_auditnotice_btn_add").off("click").on("click",function(){
    loadURL("../finance/ajax-auditnotice!input.action",$('#content'));
  });
</script>