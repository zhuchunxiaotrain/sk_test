<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/15
  Time: 16:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <form id="searchForm" method="post" action="">
            <input type="hidden" name="auditId" id="auditId" value="<s:property value="auditId" />"/>
            <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>财务审计</a>
            <shiro:hasAnyRoles name="wechat">
              <s:if test="!auditNotice.invalid">
              <a id="ajax_auditanswer_btn_add" <s:property value="isCreate('auditanswer')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建审计答复</a>
              </s:if>
            </shiro:hasAnyRoles>
            <ul id="myTab1" class="nav nav-tabs bordered  ">
              <li class="">
                <a id="toNotice" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 审计通知书 </a>
              </li>
              <li class="">
                <a id="toFirst" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计初稿 </a>
              </li>
              <li class="">
                <a id="toLast" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计终稿 </a>
              </li>
              <li class="">
                <a id="toEdit" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计整改通知单 </a>
              </li>
              <li class="active">
                <a id="toAnswer" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计答复 </a>
              </li>
            </ul>
          </form>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
              <section id="widget-grid" class="">
                <!-- row -->
                <div class="row">
                  <!-- NEW WIDGET START -->
                  <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
                         data-widget-colorbutton="false"
                         data-widget-togglebutton="false"
                         data-widget-deletebutton="false"
                         data-widget-fullscreenbutton="true"
                         data-widget-custombutton="false"
                         data-widget-sortable="false">
                      <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                          <!-- This area used as dropdown edit box -->
                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">
                          <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class=" ">
                              <div class="row" id="ajax_auditanswer_list_row">
                                <table id="ajax_auditanswer_table" class="table table-striped table-bordered table-hover">
                                </table>
                                <div id="ajax_auditanswer_list_page">
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                        <!-- end widget content -->
                      </div>
                      <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                  </article>
                  <!-- WIDGET END -->
                </div>
              </section>
              <!-- end widget grid -->
            </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
          </div>
        </div>
      </div>
    </div>

  </article>
</div>

<script type="text/javascript">
  $(function(){
    load_auditanswer_jqGrid();
    <%--var table_global_width="<s:property value="width" />";--%>
    <%--$("#ajax_auditanswer_list_row").width(table_global_width);--%>
    <%--jQuery("#ajax_auditanswer_table").jqGrid('setGridWidth', table_global_width);--%>

    //toNotice
    $("a#toNotice").off("click").on("click",function(e) {
      loadURL("../finance/ajax-auditnotice!read.action?keyId="+$("#auditId").val()+"&step=<s:property value="step" />", $('div#content'));
    });

    //toFirst
    $("a#toFirst").off("click").on("click",function(e) {
      var firstId= "<s:property value="auditNotice.auditFirst.id" />";
      loadURL("../finance/ajax-auditfirst!read.action?keyId="+firstId+"&step=<s:property value="step" />", $('div#content'));
    });

    //toLast
    $("a#toLast").off("click").on("click",function(e) {
      var lastId= "<s:property value="auditNotice.auditLast.id" />";
      loadURL("../finance/ajax-auditlast!read.action?keyId="+lastId+"&step=<s:property value="step" />", $('div#content'));
    });

    //toEdit
    $("a#toEdit").off("click").on("click",function(e) {
      var editId= "<s:property value="auditNotice.auditEdit.id" />";
      loadURL("../finance/ajax-auditedit!read.action?keyId="+editId+"&step=<s:property value="step" />", $('div#content'));
    });

  });
  function load_auditanswer_jqGrid(){
    jQuery("#ajax_auditanswer_table").jqGrid({
      url:'../finance/ajax-auditanswer!list.action?auditId='+"<s:property value="auditId" />",
      datatype: "json",
      colNames:['审计答复类型','答复时间',"答复人","文档状态","操作","id"],
      colModel:[
        {name:'type',index:'type', width:100,sortable:false,search:false},
        {name:'createDate',index:'createDate',sortable:false,search:false,width:120},
        {name:'creater',index:'creater', search:false,sortable:true,width:80},
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_auditanswer_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        var ids=$("#ajax_auditanswer_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_auditanswer_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_auditanswer_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_auditanswer_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_auditanswer_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_auditanswer_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 审计答复",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_auditanswer_table").jqGrid('setGridWidth', $("#ajax_auditanswer_list_row").width());
    })
    jQuery("#ajax_auditanswer_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_auditanswer_table").jqGrid('navGrid', "#ajax_auditanswer_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_auditanswer_read(id){
    loadURL("../finance/ajax-auditanswer!read.action?keyId="+id+"&step="+"<s:property value="step" />",$('#content'));
  }

  $("#ajax_auditanswer_btn_add").off("click").on("click",function(){
    loadURL("../finance/ajax-auditanswer!input.action?auditId="+$("#auditId").val()+"&step=<s:property value="step" />",$('#content'));
  });

  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../finance/ajax!auditnotice.action?step=<s:property value="step"/>",$('#content'));
    }

  });
</script>