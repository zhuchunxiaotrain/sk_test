<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/9
  Time: 17:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />

<input type="hidden" name="keyId" id="keyId" value="keyId"/>
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      财务分析
    </h1>
  </div>
</div>

<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <shiro:hasAnyRoles name="wechat">
            <a id="ajax_financialanalysis_btn_add"  class="btn btn-default " <s:property value="isCreate('financialanalysis')"/> data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建财务分析</a>
            <s:if test="viewtype==2">
              <a id="ajax_notice_invalid" class="btn btn-default pull-right" href="javascript:void(0)"> <i class="fa fa-lg fa-hand-o-right"></i> 失效 </a>
            </s:if>
          </shiro:hasAnyRoles>
          <%--<input type="text" value="<s:property value="isCreate('financialanalysis')"/>"/>--%>
        </form>
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_financialanalysis_list_row">
                    <table id="ajax_financialanalysis_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_financialanalysis_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script >
  function load_financialanalysis_jqGrid(){
    jQuery("#ajax_financialanalysis_table").jqGrid({
      url:'../finance/ajax-financialanalysis!list.action?viewtype='+"<s:property value="viewtype" />",
      datatype: "json",
      colNames:["所属季度","申请人","提交日期","文档状态","操作","id"],
      colModel:[
        {name:'quarter',index:'quarter', search:false,width:80},
        {name:'creater',index:'creater', search:false,width:80},
        {name:'createDate',index:'createDate', search:false,width:80},
        {name:'state',index:'state', search:false,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_financialanalysis_list_page',
      sortname : 'createDate',
      sortorder : "desc",
      gridComplete:function(){
        var ids=$("#ajax_financialanalysis_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_financialanalysis_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_financialanalysis_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_financialanalysis_table").jqGrid('setRowData',ids[i],{act:de});
        }

        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        /*$(this).gridselect('onecheck',{
         table:"ajax_financialanalysis_table",
         rowId:rowId,
         status:status,
         id:"usersId",
         name:"usersName"
         });*/
        var slt =   $("#ajax_financialanalysis_table").jqGrid('getGridParam','selarrrow');
        if(slt.length > 1){
          alert("请选择一条数据");
          return false;
        }

        var rowId = $("#ajax_financialanalysis_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_financialanalysis_table").jqGrid('getRowData', rowId);
        $("#keyId").val(rowId);

      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_financialanalysis_table").jqGrid('setGridWidth', $("#ajax_financialanalysis_list_row").width());
    })

    jQuery("#ajax_financialanalysis_table").jqGrid('navGrid', "#ajax_financialanalysis_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_financialanalysis_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    load_financialanalysis_jqGrid();

    //失效
    $('#ajax_notice_invalid').click(function(){
      var ids=$("#ajax_financialanalysis_table").jqGrid('getGridParam','selarrrow');
      if(ids.length != 1){
        alert("请选择一条财务分析");
        return false;
      }
      $.tzDialog({title:"提示",content:"确认要将此财务分析失效吗?",callback:function(){
        var vActionUrl = "<%=path%>/finance/ajax-financialanalysis!doInvalid.action";
        var data={keyId:ids[0]};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
        });
        jQuery("#ajax_financialanalysis_table").trigger("reloadGrid");
      }});
    });

  });
  $("#dialog-ok").unbind("click").bind("click",function(){

    gDialog.fClose();
  });

  function fn_financialanalysis_read(id){
    loadURL("../finance/ajax-financialanalysis!read.action?keyId="+id+"&viewtype="+"<s:property value="viewtype" />",$('#content'));
  }




  //完成按钮
  $("#fn_financialanalysis_update").off("click").on("click",function(){
    // loadURL("ajax-financialanalysis!update.action?keyId="+$("#keyId").val(),$('#content'));
    form_save("financialanalysis","../finance/ajax-financialanalysis!update.action?keyId="+$("#keyId").val());
    loadURL("../finance/ajax-financialanalysis.action?viewtype=2",$('#content'));

  });

  //新建
  $("#ajax_financialanalysis_btn_add").off("click").on("click",function(){
    loadURL("../finance/ajax-financialanalysis!input.action",$('#content'));
  });

</script>