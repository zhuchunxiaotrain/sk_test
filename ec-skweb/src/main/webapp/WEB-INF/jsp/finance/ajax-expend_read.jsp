<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/4/1
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<style>
  #detail td{
    text-align: center;
    /*padding: 2px 0px;*/
  }
  tbody td{
    height: 32px;
  }
  #detail{
    margin-bottom: 20px;
  }
</style>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <ul id="myTab1" class="nav nav-tabs bordered  ">
          <li class="active">
            <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 支出单 </a>
          </li>
        </ul>
        <div id="myTabContent1" class="tab-content padding-10 ">
          <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
            <hr class="simple">
            <form id="expend" class="smart-form" novalidate="novalidate" action="" method="post">
              <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
              <input type="hidden" name="keyId" id="keyId" value="<s:property value="expend.id" />"/>
              <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
              <input type="text" name="expendContent" id="expendContent" value="<s:property value="expend.content" />"/>
              <shiro:hasAnyRoles name="wechat">
                <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(expend.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
              </shiro:hasAnyRoles>
              <header  style="display: block;">
                支出单&nbsp;&nbsp;<span id="title"></span>
                <input type="text" value="<s:property value="numStatus" />"/>
              </header>
              <fieldset>

                <div class="row">
                  <label class="label col col-2">
                    支出类别
                  </label>
                  <section class="col col-5">
                    <div class="inline-group state-disabled">
                      <label class="radio">
                        <input type="radio"  disabled name="type" value="1" <s:property value="expend.type==1?'checked':''"/>>
                        <i></i>采购支出</label>
                      <label class="radio">
                        <input type="radio"  disabled name="type" value="2" <s:property value="expend.type==2?'checked':''"/>>
                        <i></i>接待支出</label>
                      <label class="radio">
                        <input type="radio"  disabled name="type" value="3" <s:property value="expend.type==3?'checked':''"/>>
                        <i></i>日常支出</label>
                      <label class="radio">
                        <input type="radio"  disabled name="type" value="4" <s:property value="expend.type==4?'checked':''"/>>
                        <i></i>往来支付</label>
                    </div>
                  </section>
                </div>

                <s:if test="expend.type==1">
                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    <a id="toPur" href="javascript:void(0);">采购申请单</a>
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text"  name="purchaseId" id="purchaseId" disabled value="收款方：<s:property value="expend.purchase.toName"/>" >
                    </label>
                  </section>
                </div>
                </s:if>

                <s:if test="expend.type==2">
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      <a id="toRec" href="javascript:void(0);">接待申请单</a>
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text"  name="receptionId" id="receptionId" disabled value="来宾姓名：<s:property value="expend.reception.guestName"/>" >
                      </label>
                    </section>
                  </div>
                </s:if>

                <div class="row">
                  <label class="label col col-2">
                    &nbsp;
                  </label>
                  <div class="col col-5">
                    <table class="table" border="1" id="detail" style="width: 100%">
                      <thead class="">
                      <tr>
                        <td colspan="3"><button id="add" disabled class="btn btn-primary pull-right" type="button" style="width:100px;">添加</button></td>
                      </tr>
                      <tr>
                        <td>支出内容</td>
                        <td>支出金额</td>
                        <td>备注</td>
                      </tr>
                      </thead>
                      <tbody id="detailContent" class="" >
                      <tr>
                        <td class="detailContent"></td>
                        <td class="detailMoney"></td>
                        <td class="detailRemark"></td>
                      </tr>
                      </tbody>
                      <tr>
                        <td>金额合计</td>
                        <td id="number"><s:property value="expend.num"/></td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </div>
                </div>

              </fieldset>
            </form>
            <div class="flow">
              <s:if test="expend.getProcessState().name()=='Running'">
                <div class="f_title">审批意见</div>
                <div class="chat-footer"style="margin-top:5px">
                  <div class="textarea-div">
                    <div class="typearea">
                      <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                    </div>
                  </div>
                  <span class="textarea-controls"></span>
                </div>
              </s:if>
              <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
              <div class="f_content" style="display:none">
                <div id="showFlow"></div>
              </div>
              <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
              <div class="f_content" style="display:none">
                <div id="showNext"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "expend"
        };
        multiDuty(pdata);
      }

      var expendContent=eval($("#expendContent").val());
      $("#detailContent").empty();
      for(var i=0;i<expendContent.length;i++){
        var j=i+1;
        var newTr=$("<tr><td>"+expendContent[i].content+"</td>" +
        "<td>"+expendContent[i].money+"</td>" +
        "<td>"+expendContent[i].remark+"</td>");
        $("#detailContent").append(newTr);
      }


    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("expend","../finance/ajax-expend!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("expend","../finance/ajax-expend!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("expend","../finance/ajax-expend!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
              $("#btn-re-common").trigger("click");
            })
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("expend", "../finance/ajax-expend!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "4":
        //第四步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("expend", "../finance/ajax-expend!approve4.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../finance/ajax-expend!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../finance/ajax-expend!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });

  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../finance/ajax!expend.action?viewtype=<s:property value="viewtype"/>",$('#content'));
    }

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../finance/ajax-expend!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

  //跳转
  $("#toPur").click(function(){
    loadURL("../manage/ajax-purchase!read.action?keyId=<s:property value="expend.purchase.id"/>&expendId="+$("#keyId").val(),$('#content'));
  });
  $("#toRec").click(function(){
    loadURL("../manage/ajax-reception!read.action?keyId=<s:property value="expend.reception.id"/>&expendId="+$("#keyId").val(),$('#content'));
  });

</script>