<%--
  Created by IntelliJ IDEA. 
  User: ZhuChunXiao
  Date: 2017/3/13
  Time: 10:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>审计通知书</a>
          <s:if test="auditNotice==null || auditNotice.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="auditNotice!=null && auditNotice.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="auditnotice" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="auditNotice.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              审计通知书&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  审计类型
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="typeDictId" id="typeDictId" >
                      <option value="">请选择</option>
                      <s:iterator value="typeDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-department"> 被审计部门</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="department" id="department" placeholder="请选择被审计部门" value="<s:property value="auditNotice.department.name"/>" >
                    <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="auditNotice.department.id"/>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  被审计对象
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="object" value="1" <s:property value='auditNotice.object=="1"?"checked":""'/>>
                      <i></i>部门</label>
                    <label class="radio">
                      <input type="radio"  name="object" value="2" <s:property value='auditNotice.object=="2"?"checked":""'/>>
                      <i></i>个人</label>
                  </div>
                </section>
              </div>

              <div class="row" <s:if test='auditNotice == null || auditNotice.object!="1"'>style="display:none"</s:if> id="showDept">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-noticeDepartment">通知部门</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="noticeDepartment" name="noticeDepartment"
                           value="<s:iterator id="list" value="auditNotice.noticeDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="noticeDepartmentId" name="noticeDepartmentId"
                           value="<s:iterator id="list" value="auditNotice.noticeDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>

              <div class="row" <s:if test='auditNotice == null || auditNotice.object!="2"'>style="display:none"</s:if> id="showPerson">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-noticeUsers">通知个人</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="noticeUsers" name="noticeUsers"
                           value="<s:iterator id="list" value="auditNotice.noticeUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="noticeUsersId" name="noticeUsersId"
                           value="<s:iterator id="list" value="auditNotice.noticeUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  审计日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择审计日期" id="auditDate" name="auditDate"
                            type="text" value="<s:date name="auditNotice.auditDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  审计概况
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="auditSurveyText" id="auditSurveyText" type="text" placeholder="请输入审计概况" value="<s:property value="auditNotice.auditSurveyText"/>"/>
                  </label>
                  <label class="input">
                    <input name="uploadify" id="auditSurveyFilename" placeholder="" type="file" >
                    <input name="auditSurveyFileId" id="auditSurveyFileId" style="display: none" value="<s:property value="auditSurveyFileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  审计事务所
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="officeDictId" id="officeDictId" >
                      <option value="">请选择</option>
                      <s:iterator value="officeDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  审计通知书
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="auditNoticeFilename" placeholder="" type="file" >
                    <input name="auditNoticeFileId" id="auditNoticeFileId" style="display: none" value="<s:property value="auditNoticeFileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="remarks" id="remarks" type="text" placeholder="请输入备注" value="<s:property value="auditNotice.remarks"/>"/>
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"auditnotice",
      todo:"1"
    };
    multiDuty(pdata);

  });
  //上传
  inputLoad({
    objId:"auditSurveyFilename",
    entityName:"auditSurveyFileIds",
    sourceId:"auditSurveyFileId",
    jsessionid:"<%=jsessionid%>"
  });
  //上传
  inputLoad({
    objId:"auditNoticeFilename",
    entityName:"auditNoticeFileIds",
    sourceId:"auditNoticeFileId",
    jsessionid:"<%=jsessionid%>"
  });


  //被审计部门
  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知部门",
      url:"ajax-dialog!dept.action?key=department&more=0",
      width:560
    }).show();
  });
  //通知个人
  $("a[key=btn-choose-noticeUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知个人",
      url:"ajax-dialog!levelUser.action?key=noticeUsers&more=1",
      width:560
    }).show();
  });
  //通知部门
  $("a[key=btn-choose-noticeDepartment]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知部门",
      url:"ajax-dialog!dept.action?key=noticeDepartment&more=1",
      width:560
    }).show();
  });

  $(':radio[name="object"]').click(function(){
    if($(this).val()!="1"&&$(this).val()!="2") {
      $('#showPerson').hide();
      $('#showDept').hide();
    }else if($(this).val()=="1"){
      $('#showPerson').hide();
      $('#showDept').show();
    }else if($(this).val()=="2"){
      $('#showPerson').show();
      $('#showDept').hide();
    }
  });

  $(':radio[name="hasValid"]').click(function(){
    if($(this).val()=="1") {
      $('#showValid').show();
    }else{
      $('#showValid').hide();
    }
  });
  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../finance/ajax!auditnotice.action?step=1",$('#content'));
    }

  });
  //校验
  $("#auditnotice").validate({
    rules : {
      typeDictId : {
        required : true
      },
      departmentId : {
        required : true
      },
      object:{
        required : true
      },
      noticeDepartmentId:{
        required:function(){
          if($(':checked[name="object"]').val() == "1"){
            return true;
          }else{
            return false;
          }
        }
      },
      noticeUsersId:{
        required:function(){
          if($(':checked[name="object"]').val() == "2"){
            return true;
          }else{
            return false;
          }
        }
      },
      auditDate:{
        required : true
      },
      auditSurveyFileIds:{
        required:function(){
          if($("#auditSurveyText").val()!=""&&$("#auditSurveyText").val()!=null){
            return false;
          }else{
            return true;
          }
        }
      },
      officeDictId : {
        required : true
      },
      auditNoticeFileIds:{
        required:true
      }
    },
    messages : {
      typeDictId : {
        required : "请选择审计类型"
      },
      departmentId : {
        required : "请选择被审计部门"
      },
      object : {
        required : "请选择被审批对象"
      },
      noticeDepartmentId:{
        required: "请选择通知部门"
      },
      noticeUsersId:{
        required: "请选择通知个人"
      },
      auditDate:{
        required : "请选择审计日期"
      },
      auditSurveyFileIds:{
        required : "请填写审计概况或者上传附件"
      },
      officeDictId:{
        required : '请选择审计事务所'
      },
      auditNoticeFileIds:{
        required:"请上传审计通知书附件"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#auditDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("auditnotice","../finance/ajax-auditnotice!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../finance/ajax!auditnotice.action?step=1",$('#content'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#auditnotice").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#auditnotice").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("auditnotice","../finance/ajax-auditnotice!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../finance/ajax!auditnotice.action?step=1",$('#content'));
                }
              }
            });
          }
  );
</script>