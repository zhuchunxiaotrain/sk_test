<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/4/1
  Time: 14:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<style>
  #detail td{
    /*width: 80px;*/
    text-align: center;
    padding: 2px 0px;
  }
  table input{
    border:none;
    height: 100%;
    /*width: 80px;*/
    padding: 0 0 0 20px;
  }
  #detail{
    margin-bottom: 20px;
  }
</style>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <s:if test="expend==null || expend.getProcessState().name()=='Draft'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
        </s:if>
        <s:if test="expend!=null && expend.getProcessState().name()=='Backed'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
        </s:if>
        <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
        <hr class="simple">
        <form id="expend" class="smart-form" novalidate="novalidate" action="" method="post">
          <input type="hidden" name="keyId" id="keyId" value="<s:property value="expend.id" />"/>
          <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
          <input type="text" name="expendContent" id="expendContent" value="<s:property value="expend.content" />"/>
          <input type="text" name="expendNum" id="expendNum" value="<s:property value="expend==null?0:expend.num" />"/>
          <header  style="display: block;">
            支出单&nbsp;&nbsp;<span id="title"></span>
          </header>
          <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  支出类别
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="type" value="1" <s:property value="expend.type==1?'checked':''"/>>
                      <i></i>采购支出
                    </label>
                    <label class="radio">
                      <input type="radio" name="type" value="2" <s:property value="expend.type==2?'checked':''"/>>
                      <i></i>接待支出
                    </label>
                    <label class="radio">
                      <input type="radio" name="type" value="3" <s:property value="expend.type==3?'checked':''"/>>
                      <i></i>日常支出
                    </label>
                    <label class="radio">
                      <input type="radio" name="type" value="4" <s:property value="expend.type==4?'checked':''"/>>
                      <i></i>往来支出
                    </label>
                  </div>
                </section>
              </div>

              <div class="row" <s:if test="expend==null || expend.type!=1">style="display:none"</s:if> id="showPurchase">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <s:if test="expend!=null">
                    <span id="toPurchase"><a id='toPur' href='javascript:void(0);'>查看此采购申请单</a></span>
                  </s:if>
                  <s:else>
                    <span id="toPurchase">采购申请单</span>
                  </s:else>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="purchaseId" id="purchaseId" >
                      <option value="">请选择</option>
                      <s:iterator value="purchaseList" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row" <s:if test="expend==null || expend.type!=2">style="display:none"</s:if> id="showReception">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <s:if test="expend!=null">
                    <span id="toReception"><a id='toRec' href='javascript:void(0);'>查看此接待申请单</a></span>
                  </s:if>
                  <s:else>
                    <span id="toReception">接待申请单</span>
                  </s:else>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="receptionId" id="receptionId" >
                      <option value="">请选择</option>
                      <s:iterator value="receptionList" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  &nbsp;
                </label>
                <div class="col col-5">
                  <table class="table" border="1" id="detail" style="width: 100%">
                    <thead class="">
                    <tr>
                      <td colspan="11"><button id="add" class="btn btn-primary pull-right" type="button" style="width:100px;">添加</button></td>
                    </tr>
                    <tr>
                      <td>支出内容</td>
                      <td>支出金额</td>
                      <td>备注</td>
                      <td>操作</td>
                    </tr>
                    </thead>
                    <tbody id="detailContent" class="" style="width: 50%">
                    <tr>
                      <td><input class="detailText detailContent" type="text"></td>
                      <td><input class="detailText detailMoney num" type="text"></td>
                      <td><input class="detailText detailRemark" type="text"></td>
                      <td><button disabled class="del btn btn-primary" type="button" style="width:100px;">删除</button></td>
                    </tr>
                    </tbody>
                    <tr>
                      <td>合计</td>
                      <td id="numMoney"><s:property value="expend==null?0:expend.num" /></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>
              </div>

          </fieldset>
        </form>
      </div>
    </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";
  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"expend",
      todo:"1"
    };
    multiDuty(pdata);

    if($("#expendContent").val()!=null&&$("#expendContent").val()!=""){
      var expendContent=eval($("#expendContent").val());
      $("#detailContent").empty();
      for(var i=0;i<expendContent.length;i++){
        var newTr=$("<tr><td><input class='detailText detailContent' value='"+expendContent[i].content+"' type='text'></td>" +
        "<td><input class='detailText detailMoney num' value='"+expendContent[i].money+"' type='text'></td>" +
        "<td><input class='detailText detailRemark' value='"+expendContent[i].remark+"' type='text'></td>" +
        "<td><button class='del btn btn-primary' type='button' style='width:100px;'>删除</button></td></tr>");
        $("#detailContent").append(newTr);
      }
    }

    $(".del").click(function(){
      $(this).parent().parent().remove();
      if($("#detailContent tr").size()==1){
        $(".del").attr("disabled","disabled");
      }
      calculation();
    });

  });

  //采购，将label变成超链接
  $("#purchaseId").change(function(){
    if($("#purchaseId").val()!=null&&$("#purchaseId").val()!=""){
      $("#toPurchase").empty();
      var newA=$("<a id='toPur' href='javascript:void(0);'>查看此采购申请单</a>");
      $("#toPurchase").append(newA);
      $("#toPur").click(function(){
        loadURL("../manage/ajax-purchase!read.action?keyId="+$("#purchaseId").val()+"&expend=1&expendId="+$("#keyId").val(),$('#content'));
      });
    }else{
      $("#toPurchase").empty();
      $("#toPurchase").text("采购申请单");
    }
  });

  //接待，将label变成超链接
  $("#receptionId").change(function(){
    if($("#receptionId").val()!=null&&$("#receptionId").val()!=""){
      $("#toReception").empty();
      var newA=$("<a id='toRec' href='javascript:void(0);'>查看此接待申请单</a>");
      $("#toReception").append(newA);
      $("#toRec").click(function(){
        loadURL("../manage/ajax-reception!read.action?keyId="+$("#receptionId").val()+"&expend=1&expendId="+$("#keyId").val(),$('#content'));
      });
    }else{
      $("#toPurchase").empty();
      $("#toPurchase").text("接待申请单");
    }
  });

  $("#toPur").click(function(){
    loadURL("../manage/ajax-purchase!read.action?keyId="+$("#purchaseId").val()+"&expend=1&expendId="+$("#keyId").val(),$('#content'));
  });

  $("#toRec").click(function(){
    loadURL("../manage/ajax-reception!read.action?keyId="+$("#receptionId").val()+"&expend=1&expendId="+$("#keyId").val(),$('#content'));
  });

  $(':radio[name="type"]').click(function(){
    if($(this).val()=="1") {
      $('#showPurchase').show();
      $('#showReception').hide();
    }else if($(this).val()=="2"){
      $('#showPurchase').hide();
      $('#showReception').show();
    }else{
      $('#showPurchase').hide();
      $('#showReception').hide();
    }
  });

  //添加表格
  var isAdd=true;
  $("#add").click(function(){
    $(".detailText").each(function(){
      if($(this).val()==null||$(this).val()==""){
        isAdd=false;
      }
    });
    if(isAdd){
      $(".del").removeAttr("disabled");
      var newTr=$("<tr><td><input class='detailText detailContent' type='text'></td>" +
      "<td><input class='detailText detailMoney num' type='text'></td>" +
      "<td><input class='detailText detailRemark' type='text'></td>" +
      "<td><button class='del btn btn-primary' type='button' style='width:100px;'>删除</button></td></tr>");
      $("#detailContent").append(newTr);

      $(".detailMoney").keyup(function(){
        calculation();
      });

      $(".num").keyup(function(){
        $(this).val($(this).val().replace(/[^0-9.]/g,''));
      }).bind("paste",function(){
        $(this).val($(this).val().replace(/[^0-9.]/g,''));
      }).css("ime-mode", "disabled");

      $(".del").click(function(){
        $(this).parent().parent().remove();
        if($("#detailContent tr").size()==1){
          $(".del").attr("disabled","disabled");
        }
        calculation();
      });

    }else{
      alert("请填写完整");
      isAdd=true;
    }
  });


  $(".detailMoney").keyup(function(){
    calculation();
  });
  //计算
  function calculation(){
    var number=0;
    for(var i=0;i<$(".detailMoney").size();i++){
      if(isNaN(parseInt($(".detailMoney")[i].value))){
        number+=0;
      }else{
        number+=parseInt($(".detailMoney")[i].value);
      }
    }
    $("#numMoney").text(number);
  }


  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../finance/ajax!expend.action?viewtype=1",$('#content'));
    }

  });

  //校验
  $("#expend").validate({
    rules : {
      type : {
        required : true
      },
      purchaseId:{
        required : function(){
          if($(':checked[name="type"]').val() == "1"){
            return true;
          }else{
            return false;
          }
        }
      },
      receptionId:{
        required:function(){
          if($(':checked[name="type"]').val() == "2"){
            return true;
          }else{
            return false;
          }
        }
      }
    },
    messages : {
      type : {
        required : "请选择支出类别"
      },
      purchaseId : {
        required : "请选择采购申请单"
      },
      receptionId : {
        required : "请选择接待申请单"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  //设置表格中的数据
  function setDetail(){
    var expendDetail="[";
    for(var i=0;i<$("#detailContent tr").size();i++){
      expendDetail+='{"content":"'+$('.detailContent')[i].value+
      '","money":"'+$('.detailMoney')[i].value+
      '","remark":"'+$('.detailRemark')[i].value+'"},';
    }
    expendDetail=expendDetail.substring(0,expendDetail.length-1);
    expendDetail+="]";
    $("#expendContent").val(expendDetail);
    $("#expendNum").val($("#numMoney").text());
  }

  //保存
  $("#btn-save-common").click(
          function(){
            setDetail();
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("expend","../finance/ajax-expend!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../finance/ajax!expend.action?viewtype=1",$('#content'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            setDetail();
            if(!$("#expend").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#expend").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("expend","../finance/ajax-expend!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../finance/ajax!expend.action?viewtype=1",$('#content'));
                }
              }
            });
          }
  );

  $(".num").keyup(function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).bind("paste",function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).css("ime-mode", "disabled");
</script>
