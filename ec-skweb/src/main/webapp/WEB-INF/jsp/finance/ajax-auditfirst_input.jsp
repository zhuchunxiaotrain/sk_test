<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/14
  Time: 12:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>财务审计</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="">
              <a id="toNotice" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 审计通知书 </a>
            </li>
            <li class="active">
              <a id="toFirst" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计初稿 </a>
            </li>
            <li class="disabled">
              <a id="toLast" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计终稿 </a>
            </li>
            <li class="disabled">
              <a id="toEdit" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计整改通知单 </a>
            </li>
            <li class="disabled">
              <a id="toAnswer" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计答复 </a>
            </li>
          </ul>
          <hr class="simple">
          <form id="auditfirst" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="auditFirst.id" />"/>
            <input type="hidden" name="auditId" id="auditId" value="<s:property value="auditId" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <s:if test="auditFirst==null || auditFirst.getProcessState().name()=='Draft'">
              <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
            </s:if>
            <s:if test="auditFirst!=null && auditFirst.getProcessState().name()=='Backed'">
              <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
            </s:if>
            <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
            <header  style="display: block;">
              审计初稿&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  审计初稿附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="remarks" id="remarks" type="text" placeholder="请输入备注" value="<s:property value="auditFirst.remarks"/>"/>
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){

    //toNotice
    $("a#toNotice").off("click").on("click",function(e) {
      loadURL("../finance/ajax-auditnotice!read.action?keyId="+$("#auditId").val()+"&step=<s:property value="step" />", $('div#content'));
    });

    //toLast
    var firstState = "<s:property value="auditFirst.getProcessState().name()" />";
    var creater="<s:property value="isCreate('auditlast')"/>";
    var lastkeyId="<s:property value="auditFirst.auditNotice.auditLast.id" />";
    if(firstState == "Finished"&&(creater!="style='display:none'"||lastkeyId!=null&&lastkeyId!="")){
      $('#toLast').parent('li').removeClass('disabled');
    }
    $("a#toLast").off("click").on("click",function(e) {
      if (firstState == "Finished"&&(creater!="style='display:none'"||lastkeyId!=null&&lastkeyId!="")) {
        if(lastkeyId==null||lastkeyId==""){
          if(creater!="style='display:none'"){
            loadURL("../finance/ajax-auditlast!input.action?auditId=" + $("#auditId").val()+"&step=<s:property value="step" />", $('div#content'));
          }
        }else{
          loadURL("../finance/ajax-auditlast!read.action?auditId=" + $("#auditId").val()+"&keyId="+lastkeyId+"&step=<s:property value="step" />", $('div#content'));
        }
      } else {
        return false;
      }
    });

    //toEdit
    var lastState = "<s:property value="auditFirst.getAuditNotice().getAuditLast().getProcessState().name()" />";
    var editCreater="<s:property value="isCreate('auditedit')"/>";
    var editkeyId="<s:property value="auditFirst.auditNotice.auditEdit.id" />";
    if(lastState == "Finished"&&(editCreater!="style='display:none'"||editkeyId!=null&&editkeyId!="")){
      $('#toEdit').parent('li').removeClass('disabled');
    }
    $("a#toEdit").off("click").on("click",function(e) {
      if (lastState == "Finished"&&(editCreater!="style='display:none'"||editkeyId!=null&&editkeyId!="")) {
        if(editkeyId==null||editkeyId==""){
          if(editCreater!="style='display:none'"){
            loadURL("../finance/ajax-auditedit!input.action?auditId=" + $("#auditId").val()+"&step=<s:property value="step" />", $('div#content'));
          }
        }else{
          loadURL("../finance/ajax-auditedit!read.action?auditId=" + $("#auditId").val()+"&keyId="+editkeyId+"&step=<s:property value="step" />", $('div#content'));
        }
      } else {
        return false;
      }
    });

    //toAnswer
    var editState = "<s:property value="auditFirst.getAuditNotice().getAuditEdit().getProcessState().name()" />";
    if(editState == "Finished"){
      $('#toAnswer').parent('li').removeClass('disabled');
    }
    $("a#toAnswer").off("click").on("click",function(e) {
      if (editState == "Finished") {
        loadURL("../finance/ajax-auditanswer!listAnswer.action?auditId=" + $("#auditId").val()+"&step=<s:property value="step" />", $('div#content'));
      } else {
        return false;
      }
    });




    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"auditfirst",
      todo:"1"
    };
    multiDuty(pdata);

  });
  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });

  //校验
  $("#auditfirst").validate({
    rules : {
      fileIds : {
        required : true
      }
    },
    messages : {
      fileIds : {
        required : "请上传审计初稿附件"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("auditfirst","../finance/ajax-auditfirst!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../finance/ajax!auditnotice.action?step=1",$('#content'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#auditfirst").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#auditfirst").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("auditfirst","../finance/ajax-auditfirst!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../finance/ajax!auditnotice.action?step=1",$('#content'));
                }
              }
            });
          }
  );

  //返回视图
  $("#btn-re-common").click(function(){
      loadURL("../finance/ajax!auditnotice.action?step=<s:property value="step"/>",$('#content'));
  });

</script>