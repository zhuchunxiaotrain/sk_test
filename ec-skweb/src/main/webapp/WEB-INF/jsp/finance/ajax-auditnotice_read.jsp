<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/13
  Time: 14:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>财务审计</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a id="toNotice" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 审计通知书 </a>
            </li>
            <li class="disabled">
              <a id="toFirst" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计初稿 </a>
            </li>
            <li class="disabled">
              <a id="toLast" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计终稿 </a>
            </li>
            <li class="disabled">
              <a id="toEdit" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计整改通知单 </a>
            </li>
            <li class="disabled">
              <a id="toAnswer" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审计答复 </a>
            </li>
          </ul>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
              <div class="row">
                <shiro:hasAnyRoles name="wechat">
                  <s:if test="!auditNotice.invalid">
                    <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(auditNotice.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
                  </s:if>
                </shiro:hasAnyRoles>
              </div>
              <hr class="simple">
              <form id="auditnotice" class="smart-form" novalidate="novalidate" action="" method="post">
                <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
                <input type="hidden" name="keyId" id="keyId" value="<s:property value="auditNotice.id" />"/>
                <input type="hidden" name="firstId" id="firstId" value="<s:property value="auditNotice.auditFirst.id" />"/>
                <input type="hidden" name="lastId" id="lastId" value="<s:property value="auditNotice.auditLast.id" />"/>
                <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                <header  style="display: block;">
                  审计通知书&nbsp;&nbsp;<span id="title"></span>
                </header>
                <fieldset>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      审计类型
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text" name="typeDictId" id="typeDictId" disabled placeholder="请选择审计类型" value="<s:property value="auditNotice.type.name"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      被审计部门
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text" name="department" id="department" disabled placeholder="请选择被审计部门" value="<s:property value="auditNotice.department.name"/>" >
                        <input type="hidden" id="departmentId" name="departmentId" disabled value="<s:property value="auditNotice.department.id"/>"/>
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      被审计对象
                    </label>
                    <section class="col col-5">
                      <div class="inline-group state-disabled">
                        <label class="radio">
                          <input type="radio" disabled name="object" value="1" <s:property value='auditNotice.object=="1"?"checked":""'/>>
                          <i></i>部门</label>
                        <label class="radio">
                          <input type="radio" disabled name="object" value="2" <s:property value='auditNotice.object=="2"?"checked":""'/>>
                          <i></i>个人</label>
                      </div>
                    </section>
                  </div>

                  <div class="row" <s:if test='auditnotice == null || auditnotice.object!="1"'>style="display:none"</s:if>  id="showDept">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      通知部门
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text" id="noticeDepartment" name="noticeDepartment"
                               value="<s:iterator id="list" value="auditNotice.noticeDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                        <input type="hidden" id="noticeDepartmentId" name="noticeDepartmentId"
                               value="<s:iterator id="list" value="auditNotice.noticeDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                  </div>

                  <div class="row" <s:if test='auditnotice == null || auditnotice.object!="2"'>style="display:none"</s:if>  id="showPerson">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      通知个人
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text" id="noticeUsers" name="noticeUsers"
                               value="<s:iterator id="list" value="auditNotice.noticeUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                        <input type="hidden" id="noticeUsersId" name="noticeUsersId"
                               value="<s:iterator id="list" value="auditNotice.noticeUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      审计日期
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input  disabled  id="auditDate" name="auditDate" type="text" value="<s:date name="auditNotice.auditDate" format="yyyy-MM-dd"/>">
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      审计概况
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input name="auditSurveyText" id="auditSurveyText" type="text" placeholder="请输入审计概况" value="<s:property value="auditNotice.auditSurveyText"/>"/>
                      </label>
                      <label class="input state-disabled">
                        <input name="uploadify" id="auditSurveyFilename" style="display: none" placeholder="" type="file" >
                        <input name="auditSurveyFileId" id="auditSurveyFileId" style="display: none" value="<s:property value="auditSurveyFileId"/>">
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      审计事务所
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input  disabled  id="officeDictId" name="officeDictId" type="text" value="<s:property value="auditNotice.office.name"/>">
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      审计通知书
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input name="uploadify" id="auditNoticeFilename" style="display: none" placeholder="" type="file" >
                        <input name="auditNoticeFileId" id="auditNoticeFileId" style="display: none" value="<s:property value="auditNoticeFileId"/>">
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      备注
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input  disabled  id="remarks" name="remarks" type="text" value="<s:property value="auditNotice.remarks"/>">
                      </label>
                    </section>
                  </div>

                </fieldset>

              </form>
              <div class="flow">
                <s:if test="auditNotice.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer"style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
          </div>
        </div>
      </div>
    </div>

  </article>
</div>

<script>

  $(function(){

    //toFirst
    var noticeState = "<s:property value="auditNotice.getProcessState().name()" />";
    var firstCreater="<s:property value="isCreate('auditfirst')"/>";
    var firstkeyId="<s:property value="auditNotice.auditFirst.id" />";
    if(noticeState == "Finished"&&(firstCreater!="style='display:none'"||firstkeyId!=null&&firstkeyId!="")){
      $('#toFirst').parent('li').removeClass('disabled');
    }
    $("a#toFirst").off("click").on("click",function(e) {
      if (noticeState == "Finished"&&(firstCreater!="style='display:none'"||firstkeyId!=null&&firstkeyId!="")) {
        if(firstkeyId==null||firstkeyId==""){
          if(firstCreater!="style='display:none'"){
            loadURL("../finance/ajax-auditfirst!input.action?auditId=" + $("#keyId").val()+"&step=<s:property value="step" />", $('div#content'));
          }
        }else{
          loadURL("../finance/ajax-auditfirst!read.action?auditId=" + $("#keyId").val()+"&keyId="+firstkeyId+"&step=<s:property value="step" />", $('div#content'));
        }
      } else {
        return false;
      }
    });

    //toLast
    var firstState = "<s:property value="auditNotice.getAuditFirst().getProcessState().name()" />";
    var lastCreater="<s:property value="isCreate('auditlast')"/>";
    var lastkeyId="<s:property value="auditNotice.auditLast.id" />";
    if(firstState == "Finished"&&(lastCreater!="style='display:none'"||lastkeyId!=null&&lastkeyId!="")){
      $('#toLast').parent('li').removeClass('disabled');
    }
    $("a#toLast").off("click").on("click",function(e) {
      if (firstState == "Finished"&&(lastCreater!="style='display:none'"||lastkeyId!=null&&lastkeyId!="")) {
        if(lastkeyId==null||lastkeyId==""){
          if(lastCreater!="style='display:none'"){
            loadURL("../finance/ajax-auditlast!input.action?auditId=" + $("#keyId").val()+"&step=<s:property value="step" />", $('div#content'));
          }
        }else{
          loadURL("../finance/ajax-auditlast!read.action?auditId=" + $("#keyId").val()+"&keyId="+lastkeyId+"&step=<s:property value="step" />", $('div#content'));
        }
      } else {
        return false;
      }
    });

    //toEdit
    var lastState = "<s:property value="auditNotice.getAuditLast().getProcessState().name()" />";
    var editCreater="<s:property value="isCreate('auditedit')"/>";
    var editkeyId="<s:property value="auditNotice.auditEdit.id" />";
    if(lastState == "Finished"&&(editCreater!="style='display:none'"||editkeyId!=null&&editkeyId!="")){
      $('#toEdit').parent('li').removeClass('disabled');
    }
    $("a#toEdit").off("click").on("click",function(e) {
      if (lastState == "Finished"&&(editCreater!="style='display:none'"||editkeyId!=null&&editkeyId!="")) {
        if(editkeyId==null||editkeyId==""){
          if(editCreater!="style='display:none'"){
            loadURL("../finance/ajax-auditedit!input.action?auditId=" + $("#keyId").val()+"&step=<s:property value="step" />", $('div#content'));
          }
        }else{
          loadURL("../finance/ajax-auditedit!read.action?auditId=" + $("#keyId").val()+"&keyId="+editkeyId+"&step=<s:property value="step" />", $('div#content'));
        }
      } else {
        return false;
      }
    });

    //toAnswer
    var table_global_width=0;
    var editState = "<s:property value="auditNotice.getAuditEdit().getProcessState().name()" />";
    if(editState == "Finished"){
      $('#toAnswer').parent('li').removeClass('disabled');
    }
    $("a#toAnswer").off("click").on("click",function(e) {
      if (editState == "Finished") {
        if (table_global_width == 0) {
          table_global_width = $("#s1").width();
        }
        loadURL("../finance/ajax-auditanswer!listAnswer.action?auditId=" + $("#keyId").val()+"&step=<s:property value="step" />"+"&width="+table_global_width, $('div#content'));
      } else {
        return false;
      }
    });


    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "auditnotice"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("auditnotice","../finance/ajax-auditnotice!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("auditnotice","../finance/ajax-auditnotice!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          if($("#auditnotice").valid()){
            form_save("auditnotice","../finance/ajax-auditnotice!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
              $("#btn-re-common").trigger("click");
            })
          }
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("auditnotice", "../finance/ajax-auditnotice!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../finance/ajax-auditnotice!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../finance/ajax-auditnotice!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });
  //上传文件
  readLoad({
    objId:"auditSurveyFilename",
    entityName:"auditSurveyFileIds",
    sourceId:"auditSurveyFileId"
  });

  readLoad({
    objId:"auditNoticeFilename",
    entityName:"auditNoticeFileIds",
    sourceId:"auditNoticeFileId"
  });

  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../finance/ajax!auditnotice.action?step=<s:property value="step"/>",$('#content'));
    }

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../finance/ajax-auditnotice!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

</script>