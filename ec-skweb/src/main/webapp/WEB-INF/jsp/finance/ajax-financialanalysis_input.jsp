<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/10
  Time: 9:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>月度快报</a>
          <s:if test="financialAnalysis==null || financialAnalysis.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="financialAnalysis!=null && financialAnalysis.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="financialanalysis" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="financialAnalysis.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              月度快报&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  所属季度
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="quarter" value="1" <s:property value="financialAnalysis.quarter==1?'checked':''"/>>
                      <i></i>一季度
                    </label>

                    <label class="radio">
                      <input type="radio" name="quarter" value="2" <s:property value="financialAnalysis.quarter==2?'checked':''"/>>
                      <i></i>半年
                    </label>

                    <label class="radio">
                      <input type="radio" name="quarter" value="3" <s:property value="financialAnalysis.quarter==3?'checked':''"/>>
                      <i></i>三季度
                    </label>

                    <label class="radio">
                      <input type="radio" name="quarter" value="4" <s:property value="financialAnalysis.quarter==4?'checked':''"/>>
                      <i></i>全年
                    </label>
                  </div>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  财务分析附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="fileName" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";
  function getDefaultDept(data){
    $('#department').val(data.department);
    $('#departmentId').val(data.departmentId);
  }
  var time1 = setInterval("checkDuty()",1000);
  function checkDuty(){
    var curDutyId = $('#curDutyId').val();
    if(curDutyId != ""){
      clearInterval(time1);
      var vActionUrl = '../finance/ajax-financialanalysis!getDefaultDept.action?curDutyId='+curDutyId;
      ajax_action(vActionUrl,null,null,getDefaultDept);
    }
  }
  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"financialanalysis",
      todo:"1"
    };
    multiDuty(pdata);

  });

  //上传
  inputLoad({
    objId:"fileName",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../finance/ajax!financialanalysis.action",$('#content'));
    }
  });

  //校验
  $("#financialanalysis").validate({
    rules : {
      quarter : {
        required : true
      },
      fileIds : {
        required : true
      }
    },
    messages : {
      quarter : {
        required : '请选择所属季度'
      },
      fileIds : {
        required : '请上传财务分析附件'
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("financialanalysis","../finance/ajax-financialanalysis!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../finance/ajax!financialanalysis.action",$('#content'));
            }
          }
  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#financialanalysis").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#financialanalysis").valid();
                if(!$validForm) return false;
//                $("#btn-confirm-common").attr("disabled", "disabled");
//                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("financialanalysis","../finance/ajax-financialanalysis!commit.action",null,function(data){
                  if(data.state==200){
                    if(draft == 1){
                      location.href='index.action';
                    }else{
                      loadURL("../finance/ajax!financialanalysis.action",$('#content'));
                    }
                  }
                });

              }
            });
          }
  );
</script>