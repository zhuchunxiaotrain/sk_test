<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/9
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>月度快报</a>
          <s:if test="newsMonth==null || newsMonth.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="newsMonth!=null && newsMonth.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="newsmonth" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="newsMonth.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              月度快报&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  年份
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="yearId" id="yearId" >
                      <option value="">请选择</option>
                      <s:iterator begin="2010" end="%{year}" id="now">
                        <s:if test="%{newsMonth.year==#now}">
                          <option value="${now}" selected>${now}</option>
                        </s:if>
                        <s:else>
                          <option value="${now}">${now}</option>
                        </s:else>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  月份
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="monthId" id="monthId" >
                      <option value="">请选择</option>
                      <s:iterator begin="1" end="12" id="month">
                        <s:if test="%{newsMonth.month==#month}">
                          <option value="${month}" selected>${month}</option>
                        </s:if>
                        <s:else>
                          <option value="${month}">${month}</option>
                        </s:else>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-department"> 发布部门</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="department" id="department" placeholder="请选择发布部门" value="<s:property value="newsMonth.department.name"/>" >
                    <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="newsMonth.department.id"/>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  月度快报附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="fileName" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";
  function getDefaultDept(data){
    $('#department').val(data.department);
    $('#departmentId').val(data.departmentId);
  }
  var time1 = setInterval("checkDuty()",1000);
  function checkDuty(){
    var curDutyId = $('#curDutyId').val();
    if(curDutyId != ""){
      clearInterval(time1);
      var vActionUrl = '../finance/ajax-newsmonth!getDefaultDept.action?curDutyId='+curDutyId;
      ajax_action(vActionUrl,null,null,getDefaultDept);
    }
  }
  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"newsmonth",
      todo:"1"
    };
    multiDuty(pdata);
  });

  //上传
  inputLoad({
    objId:"fileName",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../finance/ajax!newsmonth.action",$('#content'));
    }
  });

  //校验
  $("#newsmonth").validate({
    rules : {
      yearId : {
        required : true
      },
      monthId:{
        required : true
      },
      departmentId : {
        required : true
      },
      fileIds : {
        required : true
      }
    },
    messages : {
      yearId : {
        required : '请选择年份'
      },
      monthId : {
        required : '请选择月份'
      },
      departmentId : {
        required : '请选择申报单位'
      },
      fileIds : {
        required : '请上传月度快报附件'
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  //发布部门
  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择发布部门",
      url:"ajax-dialog!dept.action?key=department",
      width:560
    }).show();
  });

  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("newsmonth","../finance/ajax-newsmonth!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../finance/ajax!newsmonth.action",$('#content'));
            }
          }
  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#newsmonth").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#newsmonth").valid();
                if(!$validForm) return false;
//                $("#btn-confirm-common").attr("disabled", "disabled");
//                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("newsmonth","../finance/ajax-newsmonth!commit.action",null,function(data){
                  if(data.state==200){
                    if(draft == 1){
                      location.href='index.action';
                    }else{
                      loadURL("../finance/ajax!newsmonth.action",$('#content'));
                    }
                  }
                });
              }
            });
          }
  );
</script>