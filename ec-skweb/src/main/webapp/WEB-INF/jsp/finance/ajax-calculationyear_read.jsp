<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/7
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <ul id="myTab1" class="nav nav-tabs bordered  ">
          <li class="active">
            <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 全年预决算报表 </a>
          </li>
          <li class="disabled">
            <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 历史记录 </a>
          </li>
        </ul>
        <div id="myTabContent1" class="tab-content padding-10 ">
          <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
            <hr class="simple">
            <form id="calculationyear" class="smart-form" novalidate="novalidate" action="" method="post">
              <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
              <input type="hidden" name="keyId" id="keyId" value="<s:property value="calculationYear.id" />"/>
              <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
              <shiro:hasAnyRoles name="wechat">
                <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(calculationyear.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
              </shiro:hasAnyRoles>
              <header  style="display: block;">
                全年预决算报表&nbsp;&nbsp;<span id="title"></span>
              </header>
              <fieldset>

                <div class="row">
                  <label class="label col col-2">
                    全年预决算名称
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input name="cname" id="cname" disabled value="<s:property value="calculationYear.name"/>">
                    </label>
                  </section>
                </div>


                <div class="row">
                  <label class="label col col-2">
                    全年预算报表
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input name="uploadify" id="budgetfileName" style="display: none" placeholder="" type="file" >
                      <input name="budgetfileId" id="budgetfileId" style="display: none" value="<s:property value="budgetfileId"/>">
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    全年决算报表
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input name="uploadify" id="finalAccountsfileName" style="display: none" placeholder="" type="file" >
                      <input name="finalAccountsfileId" id="finalAccountsfileId" style="display: none" value="<s:property value="finalAccountsfileId"/>">
                    </label>
                  </section>
                </div>

              </fieldset>

            </form>
            <div class="flow">
              <s:if test="calculationYear.getProcessState().name()=='Running'">
                <div class="f_title">审批意见</div>
                <div class="chat-footer"style="margin-top:5px">
                  <div class="textarea-div">
                    <div class="typearea">
                      <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                    </div>
                  </div>
                  <span class="textarea-controls"></span>
                </div>
              </s:if>
              <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
              <div class="f_content" style="display:none">
                <div id="showFlow"></div>
              </div>
              <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
              <div class="f_content" style="display:none">
                <div id="showNext"></div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var state = "<s:property value="calculationYear.getProcessState().name()" />";
  if(state == "Finished"){
    $('#other1').parent('li').removeClass('disabled');
  }
  var table_global_width=0;
  $("a#other1").off("click").on("click",function(e) {
    if(state == "Finished"){
      if (table_global_width == 0) {
        table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
      }
      loadURL("../finance/ajax!modifyyear.action?parentId="+$("#keyId").val(),$('div#s2'));
    }else{
      return false;
    }

  });

  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "calculationyear"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("calculationyear","../finance/ajax-calculationyear!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("calculationyear","../finance/ajax-calculationyear!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("calculationyear","../finance/ajax-calculationyear!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("calculationyear", "../finance/ajax-calculationyear!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "4":
        //第四步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("calculationyear", "../finance/ajax-calculationyear!approve4.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../finance/ajax-calculationyear!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../finance/ajax-calculationyear!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });

  //上传
  readLoad({
    objId:"budgetfileName",
    entityName:"budgetId",
    sourceId:"budgetfileId"
  });
  readLoad({
    objId:"finalAccountsfileName",
    entityName:"finalAccountsId",
    sourceId:"finalAccountsfileId"
  });

  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../finance/ajax!calculationyear.action?viewtype=<s:property value="viewtype"/>",$('#content'));
    }

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../finance/ajax-calculationyear!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

</script>
