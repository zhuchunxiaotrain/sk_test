<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/7
  Time: 17:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common2" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>预算调整报表</a>
        <shiro:hasAnyRoles name="wechat">
          <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(modifyyear.id)"/> key="ajax_edit2" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
        </shiro:hasAnyRoles>

        <hr class="simple">
        <form id="modifyyear" class="smart-form" novalidate="novalidate" action="" method="post">
          <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
          <input type="hidden" name="keyId" id="keyId" value="<s:property value="modifyYear.id" />"/>
          <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
          <input type="hidden" name="parentId" id="parentId" value="<s:property value="modifyYear.calculationYear.id" />"/>
          <header  style="display: block;">
            预算调整报表&nbsp;&nbsp;<span id="title"></span>
          </header>
          <fieldset>

            <div class="row">
              <label class="label col col-2">
                原全年预算报表名称
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input name="calculationYearText" id="calculationYearText" disabled value="<s:property value="calculationYearText"/>">
                  <input name="calculationYearId" id="calculationYearId" disabled type="hidden" value="<s:property value="modifyYear.calculationYear.id"/>">
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                全年预算报表
              </label>
              <section class="col col-5">
                <label class="input">
                  <input name="uploadify" id="newBudgetfileName" placeholder="" type="file" style="display: none">
                  <input name="newBudgetfileId" id="newBudgetfileId" style="display: none" value="<s:property value="newBudgetfileId"/>">
                </label>
              </section>
            </div>

          </fieldset>

        </form>
        <div class="flow">
          <s:if test="modifyYear.getProcessState().name()=='Running'">
            <div class="f_title">审批意见</div>
            <div class="chat-footer"style="margin-top:5px">
              <div class="textarea-div">
                <div class="typearea">
                  <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                </div>
              </div>
              <span class="textarea-controls"></span>
            </div>
          </s:if>
          <div class="f_title"><i class="right" id="flow2"></i>流程信息</div>
          <div class="f_content" style="display:none">
            <div id="showFlow2"></div>
          </div>
          <div class="f_title"><i class="right" id="next2"></i>下一步骤提示</div>
          <div class="f_content" style="display:none">
            <div id="showNext2"></div>
          </div>
        </div>
      </div>
    </div>
  </article>

</div>

<script>
  //返回视图
  $("#btn-re-common2").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../finance/ajax!modifyyear.action",$('div#s2'));
    }

  });
  //编辑
  $("a[key=ajax_edit2]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../finance/ajax-modifyyear!input.action?keyId="+$("#modifyyear #keyId").val()+"&draft="+draft,$('#content'));
  });

  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("#modifyyear input#keyId").val()+"&type=flow",$('#showFlow2'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("#modifyyear input#keyId").val()+"&type=next",$('#showNext2'));
    ajax_action("ajax-config!operateType.action",{keyId: $("#modifyyear input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("#modifyyear input#keyId").val(),
          flowName: "modifyyear"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("#modifyyear input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("modifyyear","../finance/ajax-modifyyear!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common2").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("modifyyear","../finance/ajax-modifyyear!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common2").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("modifyyear","../finance/ajax-modifyyear!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common2").trigger("click");
          })
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("modifyyear", "../finance/ajax-modifyyear!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common2").trigger("click");
          })
        });
        break;
      case "4":
        //第四步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("modifyyear", "../finance/ajax-modifyyear!approve4.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common2").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../finance/ajax-modifyyear!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#modifyyear #curDutyId').val();
      var data={keyId:$("#modifyyear input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common2").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../finance/ajax-modifyyear!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#modifyyear #curDutyId').val();
      var data={keyId:$("#modifyyear input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common2").trigger("click");
      });
    });
    //流程信息展开
    $('#flow2,#next2').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });

  //上传
  readLoad({
    objId:"newBudgetfileName",
    entityName:"newBudgetId",
    sourceId:"newBudgetfileId"
  });

</script>
