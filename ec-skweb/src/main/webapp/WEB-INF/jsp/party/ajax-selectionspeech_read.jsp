<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<s:if test="draft == 1">
  <jsp:include page="../com/ajax-top.jsp" />
</s:if>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-speech-read-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>竞聘演讲返回
          </a>

                <%--<a href="javascript:void(0);" key="btn-selectionspeech-comment">
                      <span style="margin-top: -13px !important;" class="btn btn-default pull-right">
                          <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                      </span>
                </a>

            <shiro:hasAnyRoles name="wechat">
                <a style="margin-top: -13px !important;" <s:property value="isEdit(selectionSpeech.id)"/> class="btn btn-default pull-right pull-right-fix" key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>--%>

          <hr class="simple">
          <form id="selectionSpeech" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="selectionSpeech.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId" />"/>
            <input type="hidden" name="todo" id="todo" value="<s:property value="todo"/>" />
            <input type="hidden" name="index" id="index" value="<s:property value="index" />"/>
            <input type="hidden" name="remind" id="remind" value="<s:property value="remind" />"/>
            <input type="hidden" name="record" id="record" value="<s:property value="record" />"/>
            <input type="hidden" name="draft" id="draft" value="<s:property value="draft" />"/>
            <header  style="display: block;">
              报名基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                   竞聘人员
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text" name="applyUserName" id="applyUserName" placeholder="自动输入" value="<s:property value="applyUser.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  报名日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="createDate" id="createDate" placeholder="" value="<s:date name="selectionApply.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  附件上传
                </label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input type="text" name="attchmentText" id="attchmentText" placeholder="" value="<s:property value="selectionSpeech.attchmentText"/>" >
                  </label>--%>
                  <label class="input">
                    <input name="uploadify" id="speechAttchmentFileId" value="<s:iterator id="list" value="attchmentList"><s:property value="#list.id"/>,</s:iterator>" style="display:none">
                  </label>
                </section>
              </div>
              <%--result=<s:property value="selectionSpeech.result"/>--%>
              <div class="row">
                <label class="label col col-2">
                  结果
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="result" placeholder=""
                           value=" <s:if test="selectionSpeech.result==0">未审批</s:if><s:elseif test="selectionSpeech.result==1">通过</s:elseif><s:elseif test="selectionSpeech.result==2">不通过</s:elseif>" />
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="approverName" id="approverName" placeholder="系统自动" value="<s:property value="selectionSpeech.creater.name"/>" >
                    <input disabled type="hidden" name="approverId" id="approverId" placeholder="" value="<s:property value="selectionSpeech.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="approverDate" id=approverDate" placeholder="系统自动" value="<s:date name="selectionSpeech.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
          <div class="flow" id="speech">
            <s:if test="selectionSpeech.getProcessState().name()=='Running'">
              <div class="f_title">审批意见</div>
              <div class="chat-footer"style="margin-top:5px">
                <div class="textarea-div">
                  <div class="typearea">
                    <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                  </div>
                </div>
                <span class="textarea-controls"></span>
              </div>
            </s:if>
            <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow"></div>
            </div>
            <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">
  readLoad({
      objId:"speechAttchmentFileId",
      entityName:"speechAttchmentId",
      sourceId:"speechAttchmentFileId"
  });

  $(function(){
     $("input").attr("disabled","disabled").parent().addClass(" state-disabled")

      loadURL("ajax-running!workflow.action?bussinessId="+$("#selectionSpeech input#keyId").val()+"&type=flow",$('#speech #showFlow'));
      loadURL("ajax-running!workflow.action?bussinessId="+$("#selectionSpeech input#keyId").val()+"&type=next",$('#speech #showNext'));
      ajax_action("ajax-config!operateType.action",{keyId: $("#selectionSpeech input#keyId").val()},null,function(pdata){
          var showDuty = false;
          var area = $("span.textarea-controls");
          $(pdata.data.datarows).each(function(i,v){
              var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
              $(area).append(str);
              if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                  showDuty = true;
              }
          });
          if(showDuty == true){
              var pdata = {
                  keyId: $("input#keyId").val(),
                  flowName: "selectionspeech"
              };
              multiDuty(pdata);
          }
      });
      var valueObj=$("#selectionSpeech  input#numStatus").val();
      switch(valueObj){
          case "0":
              //保存后再提交
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("selectionSpeech","../party/ajax-selectionspeech!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-apply-read-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
          case "1":
              //第一步审批
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("selectionSpeech","../party/ajax-selectionspeech!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-apply-read-common").trigger("click");
                  });

              });
              break;
          case "2":
              //第二步审批
              $("#left_foot_btn_approve").off("click").on("click",function(){

                  form_save("selectionSpeech","../party/ajax-selectionspeech!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-apply-read-common").trigger("click");
                  })
              });
              break;
              /*case "3":
               //第三步审批
               $("#left_foot_btn_approve").off("click").on("click",function(){
               form_save("selectionSpeech","../party/ajax-selectionspeech!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
               $(this).leftview('leftClose');
               $("#btn-apply-read-common").trigger("click");
               })
               });
               // approveRequest("approve1");
               break;*/
      }
      //退回
      $("#left_foot_btn_sendback").off("click").on("click",function(){
          var vActionUrl="../party/ajax-selectionspeech!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
          var data={keyId:$("#selectionSpeech input#keyId").val()};
          ajax_action(vActionUrl,data,{},function(pdata){
              _show(pdata);
              $("#btn-apply-read-common").trigger("click");
          });
      });
      //否决
      $("#left_foot_btn_deny").off("click").on("click",function(){

          var vActionUrl="../party/ajax-selectionspeech!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
          var data={keyId:$("#selectionSpeech input#keyId").val()};
          ajax_action(vActionUrl,data,{},function(pdata){
              _show(pdata);
              $(this).leftview('leftClose');
              $("#btn-apply-read-common").trigger("click");
          });
      });
      //流程信息展开
      $('#flow,#next').click(function(){
          if($(this).hasClass("right")){
              $(this).removeClass("right").addClass("down");
              $(this).parent(".f_title").next("div.f_content").show();
          }else{
              $(this).removeClass("down").addClass("right");
              $(this).parent(".f_title").next("div.f_content").hide();
          }
      });
  });

  //返回视图
  $("#btn-speech-read-common").click(function(){
      var index = "<s:property value="index" />";
      var todo = "<s:property value="todo" />";
      var remind = "<s:property value="remind" />";
      var record = "<s:property value="record" />";
      var draft = "<s:property value="draft" />";
      if(index==1){
          loadURL("../com/ajax-index!page.action",$('#content'));//主页
      }else if(todo==1){
          loadURL("../com/ajax!toDoList.action",$('#content'));//个人代办
      }else if(remind==1){
          loadURL("../com/ajax!remindList.action",$('#content'));//提醒事项
      }else if(record==1){
          loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));//已办未结
      }else if(record==2){
          loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));//已办已结
      }else if(draft==1){
          loadURL("../com/ajax!draftList.action",$('#content'));//草稿
      }else {
          loadURL("../party/ajax-selectionspeech.action?parentId="+$("#parentId").val(),$('div#s3'));
      }
      /*if($('div#s3').length >=1){

      }else{
          loadURL("../party/ajax-selectionnotice.action?parentId="+$("#parentId").val()+"&todo="+$("#selectSpeech #todo").val(),$('#content'));
      }*/

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../party/ajax-selectionspeech!input.action?keyId="+$("#selectionSpeech input#keyId").val()+"&parentId="+$("#parentId").val()+"&draft="+draft,$('#content'));
  });

  //左侧意见栏
/*
  $("a[key=btn-selectionspeech-comment]").unbind("click").bind("click",function(){
    var keyId = $("#selectionSpeech input#keyId").val();
    var actionUrl = "ajax-running!comment.action?bussinessId="+keyId;/!*bussinessId是文档的ID*!/
    $(this).leftview('init',{
      title:"查看审批意见",
      actionUrl:actionUrl
    });
    var keyId = $("#selectionSpeech input#keyId").val();
    $(this).leftview('foot',{'unid':keyId,'flowName':"selectionspeech",callback:function(){
      var valueObj=$("#selectionSpeech  input#numStatus").val();
      switch(valueObj){
          case "0":
              //保存后再提交
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("selectionSpeech","../party/ajax-selectionspeech!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $(this).leftview('leftClose');
                      $("#btn-apply-read-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
        case "1":
          //第一步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("selectionSpeech","../party/ajax-selectionspeech!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-apply-read-common").trigger("click");
            });

          });
          break;
        case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){

          form_save("selectionSpeech","../party/ajax-selectionspeech!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
            $(this).leftview('leftClose');
            $("#btn-apply-read-common").trigger("click");
          })
        });
        break;
        /!*case "3":
          //第三步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("selectionSpeech","../party/ajax-selectionspeech!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-apply-read-common").trigger("click");
            })
          });
        // approveRequest("approve1");
        break;*!/
      }
      //退回
      $("#left_foot_btn_sendback").off("click").on("click",function(){
        var vActionUrl="../party/ajax-selectionspeech!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("#selectionSpeech input#keyId").val()};

        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-apply-read-common").trigger("click");
        });
      });
      //否决
      $("#left_foot_btn_deny").off("click").on("click",function(){

        var vActionUrl="../party/ajax-selectionspeech!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("#selectionSpeech input#keyId").val()};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-apply-read-common").trigger("click");
        });
      });
    }});
  })
*/

</script>