<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-input-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>年度分摊信息</a>
          <s:if test="moneyAvg!=null || moneyAvg.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="moneyAvg!=null && moneyAvg.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="moneyAvg" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="moneyAvg.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="viewtype" id="viewtype" value="<s:property value="viewtype"/>" />
            <input type="hidden" name="todo" id="todo" value="<s:property value="todo"/>" />
            <input type="hidden" name="detail" id="detail" value="<s:property value="detail"/>" />
            <header  style="display: block;">
              党费分摊基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  总计累计上缴党费金额
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="allDepartMoney" id="allDepartMoney" placeholder="" value="<s:property value="moneyAvg.allDepartMoney" />" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  总计党员人数
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="allPartyHuman" id="allPartyHuman" placeholder="" value="<s:property value="moneyAvg.allPartyHuman" />" >
                  </label>
                </section>
              </div>
                <div class="row">
                  <label class="label col col-2">
                  </label>
                  <section class="col col-2">
                    <label class="input">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      支部名称
                    </label>
                  </section>
                  <section class="col col-2">
                    <label class="input">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      支部党员人数
                    </label>
                  </section>
                  <section class="col col-2">
                    <label class="input">
                      分摊金额
                    </label>
                  </section>
                </div>
                <div class="row">
                  <s:iterator value="moneyAvgDetailList" id="list">
                    <div class="row">
                      <label class="label col col-2">

                      </label>
                      <section class="col col-2">
                        <label class="input">
                          <label class="input state-disabled">
                            <input disabled type="text" id="departName" name="departName" placeholder=""
                                   value="<s:property value="#list.department.name"/>"/>
                            <input type="hidden" id="departId"  name="departId"
                                   value="<s:property value="#list.department.id"/>"/>
                            <input type="hidden" id="moneyAvgDetailId"  name="moneyAvgDetailId"
                                   value="<s:property value="#list.id"/>"/>
                          </label>
                        </label>
                      </section>
                      <section class="col col-2">
                        <label class="input">
                          <label class="input state-disabled">
                            <input disabled type="text" id="departmentHuman" name="departmentHuman" placeholder=""
                                    value="<s:property value="#list.departmentHuman"/>"/>
                          </label>
                        </label>
                      </section>
                      <section class="col col-1">
                        <label class="input">
                          <label class="input state-disabled">
                            <input disabled type="text" id="departAvgMoney" name="departAvgMoney" placeholder=""
                                    value="<s:property value="#list.departAvgMoney"/>"/>
                          </label>
                        </label>
                      </section>
                    </div>
                </s:iterator>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="moneyAvg.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="moneyAvg.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="moneyAvg.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>

    var draft = "<s:property value="draft" />";
  $(function(){
      ajaxMoney();
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"moneyavg",
        todo:"1"
    };
    multiDuty(pdata);
  });
 /* $("a[key=btn-choose-dict-activitybranch]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择支部名称",
          url:"ajax-dialog!dict.action?dictName=ActivityBranch",
          width:340
      }).show();
  });*/

  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择支部名称",
          url:"ajax-dialog!departDepart.action?param=moneyavg",
          width:550
      }).show();


  });
  //返回视图
  $("#btn-apply-input-common").click(function(){
      if(draft == 1){
          location.href="index.action";
      }else{
          loadURL("../party/ajax-moneyavg.action?viewtype="+$("#viewtype").val()+"&detail="+$("#detail").val(),$('#content'));
      }
  });
  //校验
  /*$("#moneyAvg").validate({
    rules : {
        recordDate : {
            required : true
      },
        moneyAvgDepartId:{
            required : true
      },
        everyOneMoney:{
            required : true,
            number:true,
            min:0,

      },

    },
    messages : {
        recordDate : {
            required :  '请输入登记年月'
        },
        moneyAvgDepartId:{
            required : '请输入支部名称'
        },
        everyOneMoney:{
            required : '请输入个人缴纳党费',
            number:'请输入数字',
            min:"请输入正数"
        },

    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });*/

  $('#recordDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(

        function(){
            if(!$("#moneyAvg").valid()){
                $("#areaselect_span").hide();
                return false;
            }
          $("#btn-save-common").attr("disabled", "disabled");
          sum();
          form_save("moneyAvg","../party/ajax-moneyavg!save.action?viewtype="+$('#viewtype').val()+"&detail="+$("#detail").val(),null,
              function (data) {
                  if(data.state == "200" || data.state ==200){
                      if(draft == 1){
                          location.href="index.action";
                      }else{
                          loadURL("../party/ajax-moneyavg.action?viewtype="+$('#viewtype').val()+"&detail="+$("#detail").val(),$("#content"));
                      }
                  }
              }
          );

        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#moneyAvg").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#moneyAvg").valid();
                if(!$validForm) return false;
                sum();
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("moneyAvg","../party/ajax-moneyavg!commit.action?viewtype="+$('#viewtype').val()+"&detail="+$("#detail").val(),null,
                    function (data) {
                        if(data.state == "200" || data.state ==200){
                            if(draft == 1){
                                location.href="index.action";
                            }else{
                                loadURL("../party/ajax-moneyavg.action?viewtype="+$('#viewtype').val()+"&detail="+$("#detail").val(),$("#content"));
                            }
                        }
                    }
                );
              }
            });
          }
  );
  var sum=function(){
      var num=$("#humanNum").val();
      var money=$("#everyOneMoney").val();
      $("#monthPayDues").val(parseInt(num) * parseInt(money));
      $("#cumulativeAmountPartyMoney").val(parseInt($("#cumulativeAmountPartyMoney").val())+parseInt($("#monthPayDues").val()));
  }
  function ajaxMoney() {
      var actionUrl="<%=path%>/party/ajax-moneyavg!count.action";
      var data={};
      ajax_action(actionUrl,data,null,function (pdata) {
          if(pdata.money==0){
              $("#cumulativeAmountPartyMoney").val(0);
          }else{
              $("#cumulativeAmountPartyMoney").val(pdata.money);
          }
      });
  }
</script>
