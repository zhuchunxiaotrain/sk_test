<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="partyMoney==null || partyMoney.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="partyMoney!=null && partyMoney.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="partyMoney" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="partyMoney.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
             党费缴纳登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  缴纳时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="payDate" id="payDate" placeholder="请输入缴纳时间" value="<s:property value="partyMoney.payDate"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否党员
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="isParty" value="0" <s:property value="partyMoney.isParty==0?'checked':''"/> /><i></i>否
                    </label>
                    <label class="radio">
                      <input type="radio" name="isParty" value="1" <s:property value="partyMoney.isParty==1?'checked':''" /> /><i></i>是
                    </label>
                  </div>
                </section>
              </div>
              <div class="row">
                  <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      <a  href="javascript:void(0);" key="btn-choose-users"> 选择用户</a>
                  </label>
                  <section class="col col-5">
                      <label class="input">
                        <label class="input state-disabled">
                           <input disabled type="text" id="usersName" name="usersName"
                             value="<s:iterator id="list" value="users"><s:property value="#list.name"/></s:iterator>"/>
                          <input type="hidden" id="usersId" name="usersId"
                              value="<s:iterator id="list" value="Users"><s:property value="#list.id"/></s:iterator>"/>
                        </label>
                      </label>
                  </section>
              </div>

              <div class="row">
              <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-department"> 选择部门</a>
              </label>
              <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="partyMoneyDepartName" name="partyMoneyDepartName"
                        value="<s:iterator id="list" value="department"><s:property value="#list.name"/></s:iterator>"/>
                      <input type="hidden" id="partyMoneyDepartId" name="partyMoneyDepartId"
                        value="<s:iterator id="list" value="department"><s:property value="#list.id"/></s:iterator>"/>
                  </label>
                  </label>
              </section>
              </div>
            <%--</div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-power"> 选择职权</a>
                </label>
              </div>
              <div class="row">
                <label class="label col col-2">
                </label>
                <section class="col col-2">
                  <label class="input">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    部门名称
                  </label>
                </section>
                <section class="col col-2">
                  <label class="input">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    职权职权
                  </label>
                </section>
                <section class="col col-1">
                  <label class="input">
                    缴纳党费
                  </label>
                </section>
              </div>--%>



              <%--<div id="div_1">
                <div id="payPartyMoney">


                </div>
              </div>--%>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  缴纳党费
                </label>
                <section class="col col-5">
                  <label class="input ">
                    <input  type="text" name="everyPay" id="everyPay" placeholder="请输入要求" value="<s:property value="partyMoney.everyPay"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input disabled style="border: hidden" type="text" name="applyUserName" id="applyUserName" placeholder="" value="<s:property value="partyMoney.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="partyMoney.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden"  disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="" value="<s:date name="partyMoney.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
    inputLoad({
        objId:"attchmentFileName",
        entityName:"attchmentId",
        sourceId:"attchmentFileId"
    });


    $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"partymoney",
        todo:"1"
    };
    multiDuty(pdata);
  });


  $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
          gDialog.fCreate({
              title:"请选择人员",
              url:"ajax-dialog!userParty.action?param=partymoney",
              width:340
          }).show();

  });
  //根据用户查询当前用户所属于的部门
    $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
        if($("#usersId").val()!=""){
          gDialog.fCreate({
          title:"请选择单位",
          url:"ajax-dialog!departByUsers.action?param=partymoney&usersId="+$("#usersId").val(),
          width:500
          }).show();
        }else{
            alert("请先选择用户");
        }



    });
    //根据用户查询当前用户所属于的部门
    $("a[key=btn-choose-power]").unbind("click").bind("click",function(){
        var powerId="";
        $("#payPartyMoney input[name='powerId']").each(function () {
            powerId+=$(this).val()+",";
        })
        if($("#partyMoneyDepartId").val()!=""&&$("#usersId").val()!=""){
            gDialog.fCreate({
                title:"请选择在干部部门的职权",
                url:"ajax-dialog!powerByDepartAndUsers.action?param=partymoney&departId="+$("#partyMoneyDepartId").val()+"&usersId="+$("#usersId").val()+"&powerId="+powerId,
                width:800
            }).show();
        }else{
            alert("请先选择部门");
        }

    });
  //返回视图
  $("#btn-re-common").click(function(){
    loadURL("../party/ajax-partymoney.action?viewtype=<s:property value="viewtype"/>",$('#content'));
  });
  //校验
  $("#partyMoney").validate({
    rules : {
      name : {
        required : true
      },
      record : {
        required : true
      },
      conclusion : {
        required : true
      },
      usersName : {
        required : true
      },
      meetingRecord : {
        required : true
      },
      tellUsersId : {
          required : true
      },
        attchmentId : {
          required : true
      },

    },
    messages : {
      name : {
        required : "请输入登记事宜名称"
      },
      record : {
        required : "请输入备案"
      },
      conclusion : {
        required : "请输入结论"
      },
      usersName : {
        required : "请输入通知对象"
      },
      meetingRecord : {
        required : "请输入相应会议记录"
      },
      tellUsersId : {
          required : "请输入通知对象"
      },
        attchmentId : {
          required : "请输入附件"
      },
    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  $('#payDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(

        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("partyMoney","../party/ajax-partymoney!save.action");
          loadURL("../party/ajax-partymoney.action?viewtype="+"<s:property value="viewtype"/>",$('#content'));
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#partyMoney").valid()) {
              return false;
            }
              $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
              }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                  e.preventDefault();
                  e.stopPropagation();
                  return;
                }
                if (ButtonPressed === "确认") {
                  var $validForm = $("#partyMoney").valid();
                  if(!$validForm) return false;
                  $("#btn-confirm-common").attr("disabled", "disabled");
                  $("#btn-recommit-common").attr("disabled", "disabled");
                  form_save("partyMoney","../party/ajax-partymoney!commit.action?viewtype="+"<s:property value="viewtype"/>");
                  loadURL("../party/ajax-partymoney.action?viewtype="+"<s:property value="viewtype"/>",$('#content'));
                }
              });


          }
  );
</script>
