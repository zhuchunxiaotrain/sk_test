<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-input-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>活动申请信息</a>
          <s:if test="activityManager==null || activityManager.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="activityManager!=null && activityManager.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="activityManager" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="activityManager.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />
            <input type="hidden" name="todo" id="todo" value="<s:property value="todo"/>" />
            <header  style="display: block;">
              活动申请信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-dict-activitytype"> 活动类型</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="activityTypeName" name="activityTypeName" placeholder="请选择活动类型"
                             value="<s:iterator id="list" value="activityType"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden" id="activityTypeId" name="activityTypeId"
                             value="<s:iterator id="list" value="activityType"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否需要使用党费
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="requirdMoneyType" checked="checked" id="yes" value="0" <s:property value="activityManager.requirdMoneyType==0?'checked':''"/> ><i></i>是
                    </label>
                    <label class="radio">
                      <input type="radio" name="requirdMoneyType"  value="1" id="no" <s:property value="activityManager.requirdMoneyType==1?'checked':''"/> ><i></i>否
                    </label>
                  </div>

                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-dict-department">活动支部</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="activityManagerDepartName" name="activityManagerDepartName" placeholder="请选择活动支部"
                             value="<s:iterator id="list" value="department"><s:property value="#list.name"/></s:iterator>"/>
                      <input type="hidden" id="activityManagerDepartId" name="activityManagerDepartId"
                             value="<s:iterator id="list" value="department"><s:property value="#list.id"/></s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>

              <div id="div_1">

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                   活动费用
                  </label>
                  <section class="col col-2">
                    <label class="input">
                      <label class="input ">
                        <input type="text" id="activityMoney" name="activityMoney" placeholder="请输入活动费用"
                               value="<s:property value="activityManager.activityMoney"/>"/>
                      </label>
                    </label>
                  </section>
                  <label class="label col col-1">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    可支配金额
                  </label>
                  <section class="col col-2">
                    <label class="input">
                      <label class="input state-disabled">
                        <input disabled type="text" id="enableMoney" name="enableMoney" placeholder="自动计算" value="<s:property value="activityManager.enableMoney"/>"/>
                       <%-- <input type="hidden" id="moneyAvgId" name="moneyAvgId" value="<s:iterator id="list" value="moneyAvg"><s:property value="#list.id"/>,</s:iterator>"/>--%>
                      </label>
                    </label>
                  </section>
                </div>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-meetinguse">相应会议记录</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="meetingUseName" name="meetingUseName" placeholder="请选择相应会议记录"
                           value="<s:iterator id="list" value="meetingUse"><s:property value="#list.topic"/>,</s:iterator>"/>
                    <input type="hidden" id="meetingUseId" name="meetingUseId"
                           value="<s:iterator id="list" value="meetingUse"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>

            <%--
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  相应会议记录
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="activityManagerRecord" id="activityManagerRecord" placeholder="请输入相应会议记录" value="<s:property value="activityManager.activityManagerRecord"/>" >
                  </label>
                </section>
              </div>
--%>


<%--
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users">选择审核支部书记</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="usersName" name="checkUsersName"
                             value="<s:iterator id="list" value="checkUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden" id="usersId" name="checkUsersId"
                             value="<s:iterator id="list" value="checkUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>--%>


              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="activityManager.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="activityManager.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="activityManager.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
    var draft = "<s:property value="draft" />";
  inputLoad({
      objId:"attchmentFileName",
      entityName:"attchmentId",
      sourceId:"attchmentFileId",
      jsessionid:"<%=jsessionid%>"
  });

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"activitymanager",
        todo:"1"
    };
    multiDuty(pdata);
  });
  //选择相应会议对象
  $("a[key=btn-choose-meetinguse]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择相应会议记录",
          url:"ajax-dialog!meetingUse.action?viewtype=2",
          width:800
      }).show();
  });
$("#yes").click(function () {
    $("#div_1").attr("style","display:block");
})
  $("#no").click(function () {
      $("#div_1").attr("style","display:none");
  })

  $("a[key=btn-choose-dict-activitytype]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择活动类型",
          url:"ajax-dialog!dict.action?dictName=ActivityType",
          width:340
      }).show();
  });
  $("a[key=btn-choose-dict-department]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择单位",
          url:"ajax-dialog!departParty.action?param=activitymanager",
          width:500
      }).show();
      $(".requirdMoneyType").each(function () {
          if($("input[name='requirdMoneyType']:checked").val()==0&&$("#activityManagerDepartId").val()!=""){
              var actionUrl="<%=path%>/party/ajax-moneyavg!checkAvgMoney?departId="+$("#activityManagerDepartId").val();
              var data={departId:$("#activityManagerDepartId").val()};
              ajax_action(actionUrl,data,null,function (pdata) {

              })
          }
      });
  });
  //返回视图
  $("#btn-apply-input-common").click(function(){
      if(draft == 1){
          location.href='index.action';
      }else{
          loadURL("../party/ajax-activitymanager.action?viewtype=<s:property value="viewtype"/>",$('#content'));
      }

  });
  //校验activityManagerRecordactivityManagerDepartId
  $("#activityManager").validate({
      rules : {
          activityTypeId : {
              required : true
          },
          activityBranchId:{
              required : true
          },
          meetingUseId:{
              required : true
          },
          activityManagerDepartId:{
              required : true
          },
      },
      messages : {
          activityTypeId : {
              required :  '请选择活动类型'
          },
          activityBranchId:{
              required : '请选择活动支部'
          },
          meetingUseId:{
              required : '请选择相应会议记录'
          },
          activityManagerDepartId:{
              required : '请选择活动支部'
          },

      },
      ignore: "",
      errorPlacement : function(error, element) {
          error.insertAfter(element.parent());
      }
  });
$("#")

  $('#selectDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });


  //保存
  $("#btn-save-common").click(

        function(){
            if(!$("#activityManager").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            if($("input[name='requirdMoneyType']:checked").val()=="0"){//0==是，1==否
                if($.trim($("#activityMoney").val())==""){
                    alert("活动费用不能为空");
                    return false;
                }
                if(parseInt($("#activityMoney").val())<0){
                    alert("活动费用不能小于0");
                    return false;
                }
                if(isNaN($("#activityMoney").val())){
                    alert("输入的不是数字");
                    return false;
                }
                if(parseInt($("#activityMoney").val())>parseInt($("#enableMoney").val())){
                    alert("活动费用不能大于可支配金额");
                    return false;
                }

            }
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("activityManager","../party/ajax-activitymanager!save.action?viewtype=<s:property value="viewtype"/>",null,
              function (data) {
                  if(data.state == "200" || data.state ==200){
                      if(draft == 1){
                          location.href='index.action';
                      }else{
                          loadURL("../party/ajax-activitymanager.action?viewtype=<s:property value="viewtype"/>",$("#content"));
                      }
                  }
              });

        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {

            if(!$("#activityManager").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            if($("input[name='requirdMoneyType']:checked").val()=="0"){//0==是，1==否
                if($.trim($("#activityMoney").val())==""){
                    alert("活动费用不能为空");
                    return false;
                }
                if(parseInt($("#activityMoney").val())<0){
                 alert("活动费用不能小于0");
                 return false;
                }
                if(isNaN($("#activityMoney").val())){
                    alert("输入的不是数字");
                    return false;
                }
                if(parseInt($("#activityMoney").val())>parseInt($("#enableMoney").val())){
                    alert("活动费用不能大于可支配金额");
                    return false;
                }

            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#activityManager").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("activityManager","../party/ajax-activitymanager!commit.action?viewtype=<s:property value="viewtype"/>",null,
                    function (data) {
                        if(data.state == "200" || data.state ==200){
                            if(draft == 1){
                                location.href='index.action';
                            }else{
                                loadURL("../party/ajax-activitymanager.action?viewtype=<s:property value="viewtype"/>",$("#content"));
                            }
                        }
                    });
              }
            });
          }
  );
 /* function checkMoney() {
      var actionUrl="<%=path%>/party/ajax-moneyavg!count.action";
      var data={};
      ajax_action(actionUrl,data,null,function (pdata) {
              $("#cumulativeAmountPartyMoney").val(pdata.money);
      });
  }*/
</script>
