<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-input-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>党费缴纳基本信息</a>
          <s:if test="moneyManager==null || moneyManager.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="moneyManager!=null && moneyManager.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="moneyManager" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="moneyManager.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="departId" id="departId" value="<s:property value="departId"/>" />
            <input type="hidden" name="viewtype" id="viewtype" value="<s:property value="viewtype"/>" />
            <header  style="display: block;">
              党费登记基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  登记年月
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="recordDate" id="recordDate" placeholder="请选择登记年月" value="<s:date name="moneyManager.recordDate" format="yyyy-MM"/>" >
                  </label>
                </section>
                <section class="col col-1">
                  <label >
                    <a href="javascript:void(0);" id="registerRecord"><span  style="font-size: 20px"> 登记记录</span> </a>
                  </label>
                </section>

              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-department">支部名称</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="moneyManagerDepartName" name="moneyManagerDepartName" placeholder="请选择支部名称"
                             value="<s:iterator id="list" value="department"><s:property value="#list.name"/></s:iterator>"/>
                      <input type="hidden" id="moneyManagerDepartId" name="moneyManagerDepartId"
                             value="<s:iterator id="list" value="department"><s:property value="#list.id"/></s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>
                <div class="row">
                  <label class="label col col-2">
                  </label>
                  <section class="col col-2">
                    <label class="input">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      支部人员
                    </label>
                  </section>
                  <section class="col col-2">
                    <label class="input">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      应缴党费
                    </label>
                  </section>
                  <section class="col col-1">
                    <label class="input">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      实缴党费
                    </label>
                  </section>
                </div>
                <div class="row" id="moneyManagerDiv">
                  <s:iterator value="moneyManagerDetailList" id="list">
                    <div class="row" name="moneyDiv">
                      <label class="label col col-2">

                      </label>
                      <section class="col col-2">
                        <label class="input">
                          <label class="input state-disabled">
                            <input disabled type="text" id="userName" name="userName" placeholder="请输入支部人员"
                                   value="<s:property value="#list.employees.users.name"/>"/>
                            <input type="hidden" id="employessId"  name="employessId"
                                   value="<s:property value="#list.employees.id"/>"/>
                            <input type="hidden" id="moneyManagerDetailId"  name="moneyManagerDetailId"
                                   value="<s:property value="#list.id"/>"/>
                          </label>
                        </label>
                      </section>
                      <section class="col col-2">
                        <label class="input">
                          <label class="input state-disabled">
                            <input disabled type="text" id="baseMoney" name="baseMoney" placeholder="请输入应缴党费"
                                    value="<s:property value="#list.employees.baseMoney"/>"/>
                          </label>
                        </label>
                      </section>
                      <section class="col col-1">
                        <label class="input">
                          <label class="input">
                            <input  type="text" id="monthPayDues" name="monthPayDues" placeholder="请输入实缴党费"
                                    value="<s:property value="#list.monthPayDues"/>"/>
                          </label>
                        </label>
                      </section>
                    </div>
                  </s:iterator>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  本月总上缴党费
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="cumulativeAmountPartyMoney" name="cumulativeAmountPartyMoney" placeholder="自动计算"
                             value="<s:property value="moneyManager.cumulativeAmountPartyMoney"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="moneyManager.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="moneyManager.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="moneyManager.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
    var draft = "<s:property value="draft" />";

  $(function(){
    /*  ajaxMoney();*/
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"moneymanager",
        todo:"1"
    };
    multiDuty(pdata);
  });
 /* $("a[key=btn-choose-dict-activitybranch]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择支部名称",
          url:"ajax-dialog!dict.action?dictName=ActivityBranch",
          width:340
      }).show();
  });*/
 var oldRecordDate="";
 //var newRecordDate="";
 var flag=false;
 var num=0;

//旧的日期
var getOldDate=function () {
    oldRecordDate=$("#recordDate").val();
    //alert("O_1"+oldRecordDate);
};
//选择完成后
  $.fn.getNewDate=function (data) {
      var newRecordDate=$("#recordDate").val();
      //alert("O_2="+newRecordDate);
  }

  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
      if($("#recordDate").val()!=""){
          var recordDate=$("#recordDate").val();
          var record=new Date(recordDate.replace(/-/g,"/"));
          var recordYear=record.getYear();
          var nowDate=new Date();
          var nowYear=nowDate.getYear();
          if(nowYear!=recordYear){
              alert("只能登记本年度的党费");
          }else{
              gDialog.fCreate({
                  title:"请选择支部名称",
                  url:"ajax-dialog!departParty.action?param=moneymanager&payDate="+$("#recordDate").val()+"&keyId="+$("#moneyManager #keyId").val(),
                  width:500
              }).show();
          }
      }else{
          alert("请输入登记月份");
      }
  });
  //查看登记记录
  $("#registerRecord").click(function () {
          gDialog.fCreate({
              title:"本年度党费登记记录",
              url:"ajax-dialog!moneymanager.action?param=moneymanager",
              width:800
          }).show();
  })

  //返回视图
  $("#btn-apply-input-common").click(function(){
      if(draft ==1){
          location.href="index.action";
      }else{
          loadURL("../party/ajax-moneymanager.action?viewtype="+$("#viewtype").val(),$('#content'));
      }
  });
  //校验
  $("#moneyManager").validate({
    rules : {
        recordDate : {
            required : true
      },
        moneyManagerDepartId:{
            required : true
      },
        monthPayDues:{
            required : true,
            number:true,
            min:0,

      },

    },
    messages : {
        recordDate : {
            required :  '请选择登记年月'
        },
        moneyManagerDepartId:{
            required : '请选择支部名称'
        },
        monthPayDues:{
            required : '请输入个人缴纳党费',
            number:'请输入数字',
            min:"请输入正数"
        },

    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  $('#recordDate').datetimepicker({
    format: 'yyyy-mm',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2,
    linkField:"field"
  }).on('changeDate',function () {
      $("#moneyManagerDepartId").val("");
      $("#moneyManagerDepartName").val("");
      $("#moneyManagerDiv div[name='moneyDiv']").remove();
      $("#cumulativeAmountPartyMoney").val("")
  });

  //保存
  $("#btn-save-common").click(

        function(){
            if(!$("#moneyManager").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            var monthPayDues$=$("input[name='monthPayDues']");
            var flag=false;
            monthPayDues$.each(function (i,v) {
                if($.trim($(this).val())==""){
                    alert("活动费用不能为空");
                    flag=true;
                    return false;

                }
                if(parseInt($(this).val())<0){
                    alert("活动费用不能小于0");
                    flag=true;
                    return false;

                }
                if(isNaN($(this).val())){
                    alert("输入的不是数字");
                    flag=true;
                    return false;

                }
              /*if(parseInt($("#activityMoney").val())>parseInt($("#enableMoney").val())){
               alert("活动费用不能大于可支配金额");
               return false;
               }*/
            });
            if(flag){
                return false;
            }
            $("#btn-save-common").attr("disabled", "disabled");
            sum();
            form_save("moneyManager","../party/ajax-moneymanager!save.action?viewtype="+$('#viewtype').val(),null,
                function (data) {
                    if(data.state == "200" || data.state ==200){
                        if(draft == 1){
                            location.href="index.action";
                        }else{
                            loadURL("../party/ajax-moneymanager.action?viewtype="+$('#viewtype').val(),$("#content"));
                        }
                    }
                }
            );
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#moneyManager").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            var monthPayDues$=$("input[name='monthPayDues']");
            var flag=false;
            monthPayDues$.each(function (i,v) {
                if($.trim($(this).val())==""){
                    alert("活动费用不能为空");
                    flag=true;
                    return false;

                }
                if(parseInt($(this).val())<0){
                    alert("活动费用不能小于0");
                    flag=true;
                    return false;

                }
                if(isNaN($(this).val())){
                    alert("输入的不是数字");
                    flag=true;
                    return false;

                }
                /*if(parseInt($("#activityMoney").val())>parseInt($("#enableMoney").val())){
                    alert("活动费用不能大于可支配金额");
                    return false;
                }*/
            });
            if(flag){
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#moneyManager").valid();
                if(!$validForm) return false;
                sum();
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("moneyManager","../party/ajax-moneymanager!commit.action?viewtype="+$('#viewtype').val(),null,
                    function (data) {
                        if(data.state == "200" || data.state ==200){
                            if(draft == 1){
                                location.href="index.action";
                            }else{
                                loadURL("../party/ajax-moneymanager.action?viewtype="+$('#viewtype').val(),$("#content"));
                            }
                        }
                    }
                );
              }
            });
          }
  );
  var sum=function(){
    var monthPayDues$=$("#moneyManager input[name='monthPayDues']");
    var sum=0;
    monthPayDues$.each(function (i) {
        sum+=parseInt($(this).val());
    })
      $("#cumulativeAmountPartyMoney").val(sum);
  };
  function ajaxMoney() {
      var actionUrl="<%=path%>/party/ajax-moneymanager!count.action";
      var data={};
      ajax_action(actionUrl,data,null,function (pdata) {
          if(pdata.money==0){
              $("#cumulativeAmountPartyMoney").val(0);
          }else{
              $("#cumulativeAmountPartyMoney").val(pdata.money);
          }
      });
  }
</script>
