<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>公告信息</a>
          <s:if test="selectionNotice==null || selectionNotice.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="selectionNotice!=null && selectionNotice.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="selectionNotice" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="selectionNotice.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="viewtype" id="viewtype" value="<s:property value="viewtype" />"/>
            <input type="hidden" name="flag" id="flag" value="<s:property value="flag" />"/>
            <input type="hidden" name="todo" id="todo" value="<s:property value="todo" />"/>
            <header  style="display: block;">
              公告基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-dict-postcategory"> 岗位</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="postCategoryName" name="postCategoryName" placeholder="请选择岗位"
                             value="<s:iterator id="list" value="post"><s:property value="#list.name"/></s:iterator>"/>
                      <input type="hidden" id="postCategoryId" name="postCategoryId"
                             value="<s:iterator id="list" value="post"><s:property value="#list.id"/></s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>

            <%--
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  岗位
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="postDictId" id="postDictId">
                      <option value="">请选择</option>
                      <s:iterator value="postArrayList" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>
--%>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  要求
                </label>
                <section class="col col-5">
                  <label class="input ">
                    <input  type="text" name="requires" id="requires" placeholder="请输入要求" value="<s:property value="selectionNotice.requires"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  报名周期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="selectDate" id="selectDate" placeholder="请选择报名周期" value="<s:date name="selectionNotice.selectDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  竞聘方案
                </label>
                <section class="col col-5">
                 <%-- <label class="input">
                    <input type="text" name="makePlanText" id="makePlanText" placeholder="请输入竞聘方案" value="<s:property value="selectionNotice.makePlanText"/>" >
                  </label>--%>
                  <label class="input">
                    <input name="uploadify" id="makePlanFileName" placeholder="" type="file">
                    <input name="makePlanFileId" id="makePlanFileId" style="display:none" value="<s:iterator value="makePlan" id="list"><s:property value="#list.id"/>,</s:iterator>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-role"> 审核小组</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="roleName" id="roleName" placeholder="请选择审核小组" value="<s:iterator id="list" value="role"><s:property value="#list.name"/></s:iterator>" >
                    <input type="hidden" name="roleId" id="roleId" placeholder="" value="<s:iterator id="list" value="role"><s:property value="#list.id"/></s:iterator>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="selectionNotice.remark"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="selectionNotice.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="selectionNotice.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="selectionNotice.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
    var draft = "<s:property value="draft" />";
  inputLoad({
      objId:"makePlanFileName",
      entityName:"makePlanId",
      sourceId:"makePlanFileId",
      jsessionid:"<%=jsessionid%>"
  });
    /*inputLoad_1({
        objId:"makePlanFileName",
        entityName:"makePlanId",
        sourceId:"makePlanFileId",
        processState:"<s:property value="selectionNotice.processState.name"/>",
        pager:"input"
    });*/

  $(function(){
    var pdata= {
      keyId:$("#selectionNotice input#keyId").val(),
      flowName:"selectionnotice",
        todo:"1"
    };
    multiDuty(pdata);
  });

  $("a[key=btn-choose-role]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择角色",
          url:"ajax-dialog!rolecopy1.action",
          width:340
      }).show();
      console.log("btn-choose-role_end");
  });
  $("a[key=btn-choose-dict-postcategory]").unbind("click").bind("click",function(){
      console.log("choose-dict-postcategory_start");
      gDialog.fCreate({
          title:"请选择活动类型",
          url:"ajax-dialog!dict.action?dictName=PostCategory",
          width:340
      }).show();
      console.log("choose-dict-postcategory_end");
  });

  /*$("a[key=btn-choose-users]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择角色",
          url:"ajax-dialog!user.action",
          width:340
      }).show();
  });*/

  //返回视图
  $("#btn-re-common").click(function(){
      if(draft == 1){
          location.href="index.action";
      }else{
          loadURL("../party/ajax-selectionnotice.action?viewtype="+"<s:property value="viewtype"/>"+"&flag="+$("#flag").val(),$('#content'));
      }
  });
  //校验
  $("#selectionNotice").validate({
    rules : {
        postCategoryId : {
            required : true
        },
        postDictId : {
        required : true
      },
        requires:{
        required : true
      },
        selectDate:{
       required : true,
            date:true

      },
        roleId:{
          required : true
      },
        makePlanId:{
      required : true
  },
    },
    messages : {
        postCategoryId : {
            required : '请选择岗位'
        },
        postDictId : {
            required : '请输入岗位'
        },
        requires:{
            required : '请输入需求'
        },
        selectDate:{
            required : '请选择报名日期',
            date:'请输入正确日期格式'
        },
        roleId:{
            required : '请选择审核小组'
        },
        makePlanId:{
            required : '请上传竞聘方案'
        },
    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }

  });
  //自定义验证

  $.validator.addMethod("compareDate",function(value,element){
      console.log("compareDate_start");
      var selectDate = $("#selectDate").val();
      var deadlinetime = new Date();
      alert
      var reg = new RegExp('-','g');
      selectDate = selectDate.replace(reg,'/');//正则替换
      deadlinetime = deadlinetime.replace(reg,'/');
      selectDate = new Date(parseInt(Date.parse(selectDate),10));
      deadlinetime = new Date(parseInt(Date.parse(deadlinetime),10));
      if(selectDate<deadlinetime){
          return false;
      }else{
          return true;
      }
  },"<font color='#E47068'>报名日期大于当前日期</font>");
  $('#selectDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  //保存
  $("#btn-save-common").click(
        function(){
            if(!$("#selectionNotice").valid()){
                $("#areaselect_span").hide();
                return false;
            }
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("selectionNotice","../party/ajax-selectionnotice!save.action?viewtype="+"<s:property value="viewtype"/>"+"&flag="+$("#flag").val(),null,
            function (data) {
                if(data.state =='200' || data.state ==200){
                    if(draft == 1){
                        location.href="index.action";
                    }else {
                        loadURL("../party/ajax-selectionnotice.action?viewtype="+"<s:property value="viewtype"/>"+"&flag="+$("#flag").val(),$('#content'));
                    }
                }
            }
          );
        }
  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#selectionNotice").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            var date=new Date();
            var nowDate=$("#selectDate").val()
            var db=new Date(nowDate.replace(/-/g, "/"));
            if(db<date){
                alert("报名周期必须大于当前时间");
                return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#selectionNotice").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("selectionNotice","../party/ajax-selectionnotice!commit.action?viewtype="+"<s:property value="viewtype"/>"+"&flag="+$("#flag").val(),null,
                  function (data) {
                      if(data.state == '200' || data.state == 200){
                          if(draft == 1){
                             /* location.heft="index.action";*/
                              location.href="index.action";
                          }else{
                              loadURL("../party/ajax-selectionnotice.action?viewtype="+"<s:property value="viewtype"/>"+"&flag="+$("#flag").val(),$('#content'));
                          }
                      }
                  }
                );
              }
            });
          }
  );

</script>
