<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>

<div class="row">
    <%--<jsp:include page="selectCommon.jsp"></jsp:include>--%>
      <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
          <i class="fa fa-table fa-fw "></i>
          竞聘演讲
        </h1>
      </div>
      <%--curDutyId = <s:property value="curDutyId"/>

      com = <s:property value="com" />
      flag = <s:property value="flag"/>--%>
     <%-- <s:if test="curDutyId==''">--%>
      <s:if test="flag==true">
      <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4" style="float: right" id="selectSpeech">
        <div class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px;float: right">
          <%--<a style="font-size: large" id="selectionspeech_back">| 不通过 |</a>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-no-common-speech" href="javascript:void(0);"><i class="fa fa-rocket"></i> 不通过</a>
        </div>

        <div class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px;float:right;">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common-speech" href="javascript:void(0);"><i class="fa fa-rocket"></i>通过</a>
       <%--  <a style="font-size: large"><i class='fa fa-eye'></i>通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
        </div>
      </div>
      </s:if>
      <%--</s:if>--%>
</div>

<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">

            <shiro:hasAnyRoles name="wechat">

                <a id="ajax_selectionspeech_btn_add" <s:property value="isCreate('selectionspeech')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i>新建报名申请表</a>
            </shiro:hasAnyRoles>
          <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />
         <%-- <input type="text" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>--%>
        </form>
      </div>

    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row " id="ajax_selectionspeech_list_row">
                    <input type="hidden" name="keyId" id="keyId" value="<s:property value="keyId"/>">
                    <table id="ajax_selectionspeech_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_selectionspeech_list_page">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">

  $(function(){
     /* var pdata= {
          keyId:$("#keyId").val(),
          flowName:"selectionapply"
      };
      multiDuty(pdata);*/
     /*/!*$("#h1 >a").each(function (index) {

         /!* if(index==backIndex){
              $(this).attr("style","color: red");
              return;
          }*!/
      }*!/
     $("#h1 > a")*!/*/
    load_selectionspeech_jqGrid();
      $("#ajax_selectionspeech_list_row").width(table_global_width);
      jQuery("#ajax_selectionspeech_table").jqGrid('setGridWidth', table_global_width);
  });
  function load_selectionspeech_jqGrid(){
    jQuery("#ajax_selectionspeech_table").jqGrid({
      url:'../party/ajax-selectionspeech!list.action?parentId='+$('#parentId').val(),
      datatype: "json",
      colNames:['姓名','报名日期','文档状态','结果','操作','id'],
      colModel:[
        {name:'creater',index:'creater_name', width:100,search:true,sortable:false},
        {name:'createDate',index:'createDate', width:200,sortable:true,search:true},
        {name:'speechState',index:'speechState', search:false,width:80,sortable:true},
        {name:'result',index:'result', search:true,width:80,sortable:true},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_selectionspeech_list_page',
      sortname : 'createDate',
      sortorder : 'desc',
      gridComplete:function(){
        var ids=$("#ajax_selectionspeech_table").jqGrid('getDataIDs');

        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_selectionspeech_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_selectionspeech_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_selectionspeech_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        curDutyId:"curDutyId",
        repeatitems : false,
          flag:"flag",
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 公告",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_selectionspeech_table").jqGrid('setGridWidth', $("#ajax_selectionspeech_list_row").width());
    })
    jQuery("#ajax_selectionspeech_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_selectionspeech_table").jqGrid('navGrid', "#ajax_selectionspeech_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };
  jQuery("#ajax_selectionspeech_table").jqGrid({
  gridComplete:function(){
      var ids=$("#ajax_selectionspeech_table").jqGrid('getDataIDs');

      for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_selectionspeech_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_selectionspeech_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_selectionspeech_table").jqGrid('setRowData',ids[i],{act:de});
      }
      $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      jqGridStyle();
    },
  });

  //通过
  $("div#selectSpeech a#btn-confirm-common-speech").click(function(e) {
    var rowId = $("#ajax_selectionspeech_table").jqGrid('getGridParam', 'selrow');
    var rowDatas = $("#ajax_selectionspeech_table").jqGrid('getRowData', rowId);
      if(rowDatas['result']=='通过'){
          alert("已经通过，不需要再次通过");
      }else{
          if(rowId==null||rowId==""){
              alert("你没有选择数据");
          }else{
              gDialog.fCreate({
                  title:"请选择上传文件",
                  url:"ajax-dialog!upload.action?keyId="+rowId+"&result=1&parentId="+$("#parentId").val(),
                  width:700
              }).show();
          }
      }

  });
  //不通过
  $("div#selectSpeech a#btn-confirm-no-common-speech").click(function(e) {
      var rowId = $("#ajax_selectionspeech_table").jqGrid('getGridParam', 'selrow');
      var rowDatas = $("#ajax_selectionspeech_table").jqGrid('getRowData', rowId);
     // alert(rowDatas['name']);
      if(rowDatas['result']=='不通过'){
          alert("已经不通过，不需要再次不通过");
      }else {
          if(rowId==null||rowId==""){
              alert("你没有选择数据")
          }else{
              gDialog.fCreate({
                  title:"请选择上传文件",
                  url:"ajax-dialog!upload.action?keyId="+rowId+"&result=2&parentId="+$("#parentId").val(),
                  width:700
              }).show();
          }
      }

  });

  function fn_selectionspeech_read(id){
    loadURL("../party/ajax-selectionspeech!read.action?keyId="+id+"&parentId="+$("#parentId").val(),$('div#s3'));
  }

  $("#ajax_selectionspeech_btn_add").off("click").on("click",function(){
    loadURL("../party/ajax-selectionspeech!input.action?",$('div#s3'));
  })
</script>





















