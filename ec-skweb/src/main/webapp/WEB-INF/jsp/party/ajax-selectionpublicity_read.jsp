<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<s:if test="draft == 1">
  <jsp:include page="../com/ajax-top.jsp" />
</s:if>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-read-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>结果公示返回
          </a>

               <%-- <a href="javascript:void(0);" key="btn-selectionpublicity-comment">
                      <span style="margin-top: -13px !important;" class="btn btn-default pull-right">
                          <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                      </span>
                </a>--%>

            <shiro:hasAnyRoles name="wechat">
                <a style="margin-top: -13px !important;" <s:property value="isEdit(selectionPublicity.id)"/> class="btn btn-default pull-right pull-right-fix" key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>

          <hr class="simple">
          <form id="selectionPublicity" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="selectionPublicity.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />
            <input type="hidden" name="todo" id="todo" value="<s:property value="todo"/>" />
            <input type="hidden" name="index" id="index" value="<s:property value="index" />"/>
            <input type="hidden" name="remind" id="remind" value="<s:property value="remind" />"/>
            <input type="hidden" name="record" id="record" value="<s:property value="record" />"/>
            <input type="hidden" name="draft" id="draft" value="<s:property value="draft" />"/>
            <header  style="display: block;">
              报名基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-selectionspeech"> 选拔人员</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="selectionSpeechApplyName" name="selectionSpeechApplyName"
                             value="<s:property value="selectionSpeech.creater.name"/>  "/>
                      <input type="hidden" id="selectionSpeechId" name="selectionSpeechId"
                             value="<s:property value="selectionSpeech.creater.id"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  结果公示
                </label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input type="text" name="attchmentText" id="attchmentText" placeholder="请输入 结果公示" value="<s:property value="selectionPublicity.attchmentText"/>" >
                  </label>--%>
                  <label class="input">
                    <input name="publicityAttchmentFileId" id="publicityAttchmentFileId" style="display:none" value="<s:iterator id="list" value="attchment"><s:property value="#list.id"/>,</s:iterator>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  党总支人事决策会议
                </label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input type="text" name="meetingDecisionAttchmentText" id="meetingDecisionAttchmentText" placeholder="请输入党总支人事决策会议" value="<s:property value="selectionPublicity.meetingDecisionAttchmentText"/>" >
                  </label>--%>
                  <label class="input">
                    <input name="meetingDecisionAttchmentFileId" id="meetingDecisionAttchmentFileId" style="display:none" value="<s:iterator id="list" value="meetingDecisionAttchment"><s:property value="#list.id"/>,</s:iterator>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  公示开始日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="date" name="publicStartDate" id="publicStartDate" placeholder="请输入公示期" value="<s:date name="selectionPublicity.publicStartDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  公示结束日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="date" name="publicEndDate" id="publicEndDate" placeholder="请输入公示期" value="<s:date name="selectionPublicity.publicEndDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  公示结果
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="result" value="0" <s:property value="selectionPublicity.result==0?'checked':''"/> ><i></i>通过
                    </label>
                    <label class="radio">
                      <input type="radio" name="result" value="1" <s:property value="selectionPublicity.result==1?'checked':''"/> ><i></i>不通过
                    </label>
                  </div>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="" value="<s:property value="selectionPublicity.creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="" value="<s:date name="selectionPublicity.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
          <div class="flow" id="publicity">
            <s:if test="selectionPublicity.getProcessState().name()=='Running'">
              <div class="f_title">审批意见</div>
              <div class="chat-footer"style="margin-top:5px">
                <div class="textarea-div">
                  <div class="typearea">
                    <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                  </div>
                </div>
                <span class="textarea-controls"></span>
              </div>
            </s:if>
            <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow"></div>
            </div>
            <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">

  readLoad({
      objId:"publicityAttchmentFileId",
      entityName:"publicityAttchmentId",
      sourceId:"publicityAttchmentFileId"
  });
  readLoad({
      objId:"meetingDecisionAttchmentFileId",
      entityName:"meetingDecisionAttchmentId",
      sourceId:"meetingDecisionAttchmentFileId"
  });

  $(function(){
     $("input").attr("disabled","disabled").parent().addClass(" state-disabled");
      loadURL("ajax-running!workflow.action?bussinessId="+$("#selectionPublicity input#keyId").val()+"&type=flow",$('#publicity #showFlow'));
      loadURL("ajax-running!workflow.action?bussinessId="+$("#selectionPublicity input#keyId").val()+"&type=next",$('#publicity #showNext'));
      ajax_action("ajax-config!operateType.action",{keyId: $("#selectionPublicity input#keyId").val()},null,function(pdata){
          var showDuty = false;
          var area = $("span.textarea-controls");
          $(pdata.data.datarows).each(function(i,v){
              var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
              $(area).append(str);
              if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                  showDuty = true;
              }
          });
          if(showDuty == true){
              var pdata = {
                  keyId: $("input#keyId").val(),
                  flowName: "selectionpublicity"
              };
              multiDuty(pdata);
          }
      });

      var valueObj=$("#selectionPublicity  input#numStatus").val();
      switch(valueObj){
          case "0":
              //保存后再提交
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("selectionPublicity","../party/ajax-selectionpublicity!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-apply-read-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
          case "1":
              //第一步审批
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("selectionPublicity","../party/ajax-selectionpublicity!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-apply-read-common").trigger("click");
                  });

              });
              break;
          case "2":
              //第二步审批
              $("#left_foot_btn_approve").off("click").on("click",function(){

                  form_save("selectionPublicity","../party/ajax-selectionpublicity!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-apply-read-common").trigger("click");
                  })
              });
              break;
          case "3":
          //第三步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
              form_save("selectionPublicity","../party/ajax-selectionpublicity!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                  $("#btn-apply-read-common").trigger("click");
              })
          });
          // approveRequest("approve1");
          break;
      }
      //退回
      $("#left_foot_btn_sendback").off("click").on("click",function(){
          var vActionUrl="../party/ajax-selectionpublicity!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
          var data={keyId:$("#selectionPublicity input#keyId").val()};

          ajax_action(vActionUrl,data,{},function(pdata){
              _show(pdata);
              $("#btn-apply-read-common").trigger("click");
          });
      });
      //否决
      $("#left_foot_btn_deny").off("click").on("click",function(){

          var vActionUrl="../party/ajax-selectionpublicity!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
          var data={keyId:$("#selectionPublicity input#keyId").val()};
          ajax_action(vActionUrl,data,{},function(pdata){
              _show(pdata);
              $("#btn-apply-read-common").trigger("click");
          });
      });
      //流程信息展开
      $('#flow,#next').click(function(){
          if($(this).hasClass("right")){
              $(this).removeClass("right").addClass("down");
              $(this).parent(".f_title").next("div.f_content").show();
          }else{
              $(this).removeClass("down").addClass("right");
              $(this).parent(".f_title").next("div.f_content").hide();
          }
      });
  });

  //返回视图
  $("#btn-apply-read-common").click(function(){
      var index = "<s:property value="index" />";
      var todo = "<s:property value="todo" />";
      var remind = "<s:property value="remind" />";
      var record = "<s:property value="record" />";
      var draft = "<s:property value="draft" />";
      if(index==1){
          loadURL("../com/ajax-index!page.action",$('#content'));//主页
      }else if(todo==1){
          loadURL("../com/ajax!toDoList.action",$('#content'));//个人代办
      }else if(remind==1){
          loadURL("../com/ajax!remindList.action",$('#content'));//提醒事项
      }else if(record==1){
          loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));//已办未结
      }else if(record==2){
          loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));//已办已结
      }else if(draft==1){
          loadURL("../com/ajax!draftList.action",$('#content'));//草稿
      }else {
          loadURL("../party/ajax-selectionpublicity.action?parentId="+$("#parentId").val(),$('div#s4'));
      }
      /*if($('div#s4').length>=1){
          loadURL("../party/ajax-selectionpublicity.action?parentId="+$("#parentId").val(),$('div#s4'));
      }else {

              loadURL("../party/ajax-selectionnotice.action?parentId="+$("#parentId").val()+"&todo="+$("#selectionPublicity #todo").val(),$('#content'));
          }*/
  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
      var draft = "<s:property value="draft" />";
      var parentId = "<s:property value="selectionNotice.id"/>";
      loadURL("../party/ajax-selectionpublicity!input.action?keyId="+$("#selectionPublicity input#keyId").val()+"&parentId="+parentId+"&draft="+draft,$('#content'));

      /*if($('div#s4').length>=1){
          loadURL("../party/ajax-selectionpublicity!input.action?keyId="+$("#selectionPublicity input#keyId").val()+"&parentId="+$("#parentId").val(),$('div#s4'));
      }else{
          if($("#selectionPublicity #draft").val()=="1") {
              var parentId = "<s:property value="selectionNotice.id"/>";
              loadURL("../party/ajax-selectionpublicity!input.action?keyId="+$("#selectionPublicity input#keyId").val()+"&parentId="+parentId+"&draft="+$("#selectionPublicity #draft").val(),$('#content'));
          }

      }*/

  });

  //左侧意见栏
/*
  $("a[key=btn-selectionpublicity-comment]").unbind("click").bind("click",function(){
    var keyId = $("#selectionPublicity input#keyId").val();
    var actionUrl = "ajax-running!comment.action?bussinessId="+keyId;/!*bussinessId是文档的ID*!/
    $(this).leftview('init',{
      title:"查看审批意见",
      actionUrl:actionUrl
    });
    var keyId = $("#selectionPublicity input#keyId").val();
    $(this).leftview('foot',{'unid':keyId,'flowName':"selectionpublicity",callback:function(){
      var valueObj=$("#selectionPublicity  input#numStatus").val();
      switch(valueObj){
          case "0":
              //保存后再提交
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("selectionPublicity","../party/ajax-selectionpublicity!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $(this).leftview('leftClose');
                      $("#btn-apply-read-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
        case "1":
          //第一步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("selectionPublicity","../party/ajax-selectionpublicity!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-apply-read-common").trigger("click");
            });

          });
          break;
        case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){

          form_save("selectionPublicity","../party/ajax-selectionpublicity!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
            $(this).leftview('leftClose');
            $("#btn-apply-read-common").trigger("click");
          })
        });
        break;
        /!*case "3":
          //第三步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("selectionPublicity","../party/ajax-selectionpublicity!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-apply-read-common").trigger("click");
            })
          });
        // approveRequest("approve1");
        break;*!/
      }
      //退回
      $("#left_foot_btn_sendback").off("click").on("click",function(){
        var vActionUrl="../party/ajax-selectionpublicity!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("#selectionPublicity input#keyId").val()};

        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-apply-read-common").trigger("click");
        });
      });
      //否决
      $("#left_foot_btn_deny").off("click").on("click",function(){

        var vActionUrl="../party/ajax-selectionpublicity!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("#selectionPublicity input#keyId").val()};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-apply-read-common").trigger("click");
        });
      });
    }});
  })
*/

</script>