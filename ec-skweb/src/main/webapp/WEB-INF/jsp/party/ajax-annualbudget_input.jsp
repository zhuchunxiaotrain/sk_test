<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-input-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>年度预算基本信息</a>
          <s:if test="annualBudget==null || annualBudget.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="annualBudget!=null && annualBudget.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="annualBudget" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="annualBudget.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="viewtype" id="viewtype" value="<s:property value="viewtype"/>" />
            <header  style="display: block;">
              年度预算基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-department">所属单位</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="annualBudgetDepartName" name="departName" placeholder="请选择所属单位"
                             value="<s:iterator id="list" value="department"><s:property value="#list.name"/></s:iterator>"/>
                      <input type="hidden" id="annualBudgetDepartId" name="departId"
                             value="<s:iterator id="list" value="department"><s:property value="#list.id"/></s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>


              <div class="row">
                <label class="label col col-2">
                </label>
                <section class="col col-2">
                  <label class="input">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      年度预算科目
                  </label>
                </section>
                <section class="col col-2">
                  <label class="input">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    预算金额
                  </label>
                </section>
                <section class="col col-1">
                  <label class="input">
                    备注
                  </label>
                </section>
                <section class="col col-1">
                  <label class="input">
                    <a id="btn-add-row" href="javascript:void(0);">增加行</a>
                  </label>
                </section>
              </div>
            <div id="add-annoualbudget-row">
              <s:iterator id="list" value="annualBudgetDetailList">
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" class="dict-dialog" id="btn_choose_dict_0" onclick="selectdict(0)">年度预算科目</a>
                </label>
                <section class="col col-2">
                <label class="input">
                  <label class="input state-disabled">
                    <input disabled type="text" id="annualBudgetItemName_0" name="annualBudgetItemName" placeholder="请选择年度预算科目"
                           value="<s:property value="#list.annualBudgetItem.name"/>"/>
                    <input type="hidden" id="annualBudgetItemId_0"  name="annualBudgetItemId"
                           value="<s:property value="#list.annualBudgetItem.id"/>"/>
                    <input type="hidden" id="annualBudgetDetailId_0"  name="annualBudgetDetailId"
                           value="<s:property value="#list.id"/>"/>
                  </label>
                </label>
              </section>
                <section class="col col-2">
                  <label class="input">
                    <label class="input">
                      <input  type="text" id="budgetMoney" name="budgetMoney" placeholder="请输入预算金额"
                              value="<s:property value="#list.budgetMoney"/>"/>
                    </label>
                  </label>
                </section>
                <section class="col col-1">
                  <label class="input">
                    <label class="input">
                      <input  type="text" id="remark" name="remark" placeholder="请输入备注"
                              value="<s:property value="#list.remark"/>"/>
                    </label>
                  </label>
                </section>
                <section class="col col-1">
                  <label class="input">
                    <a class="btn-del-row" href="javascript:void(0);" onclick="check(0)">删除行</a>
                  </label>
                </section>
              </div>
              </s:iterator>
            </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  年度预算金额
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="annualBudgetMoney" name="annualBudgetMoney" placeholder="自动计算"
                             value="<s:property value="annualBudget.annualBudgetMoney"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="annualBudget.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="annualBudget.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="annualBudget.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
var draft="<s:property value="draft"/>";

  $(function(){
     //dialogDict();
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"annualbudget",
        todo:"1"
    };
    multiDuty(pdata);
  });

$("#btn-add-row").bind("click",function(){
    var str='<div class="row"> <label class="label col col-2"> <i class="fa fa-asterisk txt-color-red"></i> <a  href="javascript:void(0);"  class="dict-dialog">年度预算科目</a> </label> <section class="col col-2"> <label class="input"> <label class="input state-disabled"> <input disabled type="text"  name="annualBudgetItemName" placeholder="请选择年度预算科目" value=""/> <input type="hidden" name="annualBudgetItemId"value=""/> </label> </label> </section> <section class="col col-2"> <label class="input"> <label class="input"> <input  type="text" id="budgetMoney" name="budgetMoney" placeholder="请输入预算金额"value=""/> </label> </label> </section> <section class="col col-1"> <label class="input"> <label class="input"> <input  type="text" id="remark" name="remark" placeholder="请输入备注"value=""/> </label> </label> </section> <section class="col col-1"> <label class="input"> <a class="btn-del-row" href="javascript:void(0);" onclick="check()">删除行</a> </label> </section> </div>';
    $("#add-annoualbudget-row").append(str);
    dialogDict();

});
  /*$(document).on("click",".btn-del-row",function () {
      var annualBudgetItemName$=$("input[name='annualBudgetItemName']");
      alert("annualBudgetItemName$:"+annualBudgetItemName$.length);
     // alert("annualBudgetItemName$_length_1 = "+annualBudgetItemName$.length);

      if(annualBudgetItemName$.length==1){
          alert("至少有一项");
      }else{
          $(this).closest("div").remove();
          dialogDict();

      }
  })*/
  function check(i){
      var annualBudgetItemName$=$("input[name='annualBudgetItemName']");
      annualBudgetItemName$.each(function (index) {
          if(index==i){
              $(this).closest("div").remove();
              dialogDict();
              return false;
          }
      })

  }
/*  $(document).on("click","#add-annoualbudget-row .dict-dialog",function () {
    $("#add-annoualbudget-row .dict-dialog").each(  function (i) {
        alert("i_1 = "+i);
    })

  });*/
var dialogDict=function(){
    var array$=$("#add-annoualbudget-row .dict-dialog");
    var delRow$=$("#add-annoualbudget-row .btn-del-row");
    var annualbudgeName$=$("#add-annoualbudget-row input[name='annualBudgetItemName']");
    var annualbudgetId$=$("#add-annoualbudget-row input[name='annualBudgetItemId']");
    array$.each(function (index) {
        //alert("index_"+index);
        $(this).attr("id","btn_choose_dict_"+index);
        $(this).attr("onclick","selectdict("+index+")")
        if(index==array$.length){
            return false;
        }
    });
    delRow$.each(function (index_3) {
        //alert("index_"+index);
        //$(this).attr("id","btn_choose_dict_"+index_3);
        $(this).attr("onclick","check("+index_3+")")
        if(index_3==delRow$.length){
            return false;
        }
    });
    annualbudgeName$.each(function (index_1) {
        //alert("index_1_"+index_1);
        $(this).attr("id","annualBudgetItemName_"+index_1);
        if(index_1==annualbudgeName$.length){
            return false;
        }
    });
    annualbudgetId$.each(function (index_2) {
      //  alert("index_2_"+index_2);
        $(this).attr("id","annualBudgetItemId_"+index_2);
        if(index_2==annualbudgetId$.length){
            return false;
        }
    });
}
  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择单位",
          url:"ajax-dialog!departParty.action?param=annualbudget",
          width:500
      }).show();
  });
function selectdict(index) {
        gDialog.fCreate({
            title:"请选择活动类型",
            url:"ajax-dialog!dict.action?dictName=AnnualBudgetItem&index="+index,
            width:340
        }).show();
}
/* $("#add-annoualbudget-row a.dict-dialog").unbind("click").bind("click",function () {
     alert("aaaaaaa" +
         "" +
         "");
 })*/

//$("a[class='dict-dialog']").each().click(function (i) {
//    alert("i_"+i);

    /*$("a.dict-dialog").each(function (index) {
        if(index==i){
            alert("index = "+index);
            gDialog.fCreate({
                title:"请选择年度预算科目",
                url:"ajax-dialog!dict.action?dictName=AnnualBudgetItem&index="+index,
                width:340
            }).show();
            return false;
        }
    });*/


//});

  //返回视图
  $("#btn-apply-input-common").click(function(){
      if(draft ==1){
          location.href="index.action";
      }else{
         loadURL("../party/ajax-annualbudget.action?viewtype="+$("#viewtype").val(),$('#content'));
      }
  });
  //校验
  $("#annualBudget").validate({
      rules : {
          departId : {
              required : true
          },
          annualBudgetItemId:{
              required : true
          },
          budgetMoney:{
              required : true,
              number:true,
              min:0
          },

      },
      messages : {
          departId : {
              required :  '请选择所属单位'
          },
          annualBudgetItemId:{
              required : '请选择年度预算科目'
          },
          budgetMoney:{
              required : '请输入预算金额',
              number:'请输入数字',
              min:'请输入正数'
          },

      },
      ignore: "",
      errorPlacement : function(error, element) {
          error.insertAfter(element.parent());
      }
  });


  $('#selectDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(

        function(){
            if(!$("#annualBudget").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            if($("input[name='annualBudgetItemId']").length<1){
                alert("请增加行");
                return false;
            }
            var flag=false;
            var annualBudgetItemId$=$("input[name='annualBudgetItemId']");
            annualBudgetItemId$.each(function () {
                if($.trim($(this).val())==""){
                    alert("年度预算科目不能为空");
                    flag=true;
                    return false;
                }
            });
            if(flag){
                return false;
            }

            var budgetMoney$=$("input[name='budgetMoney']");

            budgetMoney$.each(function () {
                if($.trim($(this).val())==""){
                    alert("年度预算科目不能为空");
                    flag=true;
                    return false;
                }
                if(parseInt($(this).val())<0){
                    alert("年度预算科目不能小于0");
                    flag=true;
                    return false;
                }
                if(isNaN($(this).val())){
                    alert("输入的不是数字");
                    flag=true;
                    return false;
                }
            })
            if(flag){
                return false;
            }
          $("#btn-save-common").attr("disabled", "disabled");
            sum();
          form_save("annualBudget","../party/ajax-annualbudget!save.action?viewtype="+$('#viewtype').val(),null,
              function (data) {
                  if(data.state == "200" || data.state ==200){
                      if(draft == 1){
                          location.href='index.action';
                      }else{
                          loadURL("../party/ajax-annualbudget.action?viewtype="+$('#viewtype').val(),$("#content"));
                      }
                  }
              });

        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(

          function(e) {
            if(!$("#annualBudget").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            if($("input[name='annualBudgetItemId']").length<1){
                alert("请增加行");
                return false;
            }
              var flag=false;

              var annualBudgetItemId$=$("input[name='annualBudgetItemId']");
              annualBudgetItemId$.each(function () {
                  if($.trim($(this).val())==""){
                      alert("年度预算科目不能为空");
                      flag=true;
                      return false;
                  }
              });
              if(flag){
                  return false;
              }

            var budgetMoney$=$("input[name='budgetMoney']");

              budgetMoney$.each(function () {
                  if($.trim($(this).val())==""){
                      alert("年度预算科目不能为空");
                      flag=true;
                      return false;
                  }
                  if(parseInt($(this).val())<0){
                      alert("年度预算科目不能小于0");
                      flag=true;
                      return false;
                  }
                  if(isNaN($(this).val())){
                      alert("输入的不是数字");
                      flag=true;
                      return false;
                  }
              })

              if(flag){
                  return false;
              }
              /*var $validForm = $("#annualBudget").valid();
              if(!$validForm) return false;
              var actionUrl="<%=path%>/party/ajax-annualbudget!checkCount.action";
              var data={departId:$("#annualBudgetDepartId").val()};
              ajax_action(actionUrl,data,null,function (pdata) {
                  if($(pdata).length>0){
                      $(pdata).each(function (i,v) {
                          if(v.count>0){
                              alert("一年只能申请一次，或者申请的文档在流转中");
                              flag=true;
                              return false;
                          }
                      });
                  }else{

                  }
              })

              if(flag){
                  return false;
              }*/

            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                sum();
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("annualBudget","../party/ajax-annualbudget!commit.action?viewtype="+$('#viewtype').val(),null,
                    function (data) {
                        if(data.state == "200" || data.state ==200){
                            if(draft == 1){
                                location.href='index.action';
                            }else{
                                loadURL("../party/ajax-annualbudget.action?viewtype="+$('#viewtype').val(),$("#content"));
                            }
                        }
                    }
                );
              }
            });
          }
  );
  var sum=function(){
      var budgetMoney$=$("input[name='budgetMoney']");
      var sum=0;
      budgetMoney$.each(function () {
          sum+=parseInt($(this).val());
      })
      $("#annualBudgetMoney").val(sum);

  }
</script>
