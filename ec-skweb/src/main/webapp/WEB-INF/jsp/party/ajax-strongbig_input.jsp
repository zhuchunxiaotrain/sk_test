<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="strongBig==null || strongBig.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="strongBig!=null && strongBig.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="strongBig" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="strongBig.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              三重一大登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  登记事宜名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="registerName" id="registerName" placeholder="请输入登记事宜名称" value="<s:property value="strongBig.registerName"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  三重一大会议类型
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="strongBigType">
                    <label class="radio">
                      <input type="radio" checked="checked" name="type" value="1" <s:property value="strongBig.type==1?'checked':''"/>>
                      <i></i>部门三重一大会议</label>
                    <label class="radio">
                      <input type="radio"  name="type" value="2" <s:property value="strongBig.type==2?'checked':''"/>>
                      <i></i>中心党政联席会议</label>
                    <label class="radio">
                      <input type="radio"  name="type" value="3" <s:property value="strongBig.type==3?'checked':''"/>>
                      <i></i>党总支会议</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否需要提交中心审核
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="strongBigType">
                    <label class="radio">
                      <input type="radio" checked="checked" name="isCheck" value="1" <s:property value="strongBig.isCheck==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio"  name="isCheck" value="2" <s:property value="strongBig.isCheck==2?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  备案
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="strongBigRecord" id="strongBigRecord" placeholder="请输入备案" value="<s:property value="strongBig.strongBigRecord"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  结论
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="conclusion" id="conclusion" placeholder="请输入结论" value="<s:property value="strongBig.conclusion"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users">通知对象</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="users" name="tellUsersName" placeholder="请选择通知对象"
                             value="<s:iterator id="list" value="tellUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden" id="usersId" name="tellUsersId"
                             value="<s:iterator id="list" value="tellUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-meetinguse">相应会议记录</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="meetingUseName" name="meetingUseName" placeholder="请选择相应会议记录"
                           value="<s:iterator id="list" value="meetingUse"><s:property value="#list.topic"/>,</s:iterator>"/>
                    <input type="hidden" id="meetingUseId" name="meetingUseId"
                           value="<s:iterator id="list" value="meetingUse"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  三重一大相关附件
                </label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input type="text" name="attchmentText" id="attchmentText" placeholder="请输入相关附件" value="<s:property value="strongBig.attchmentText"/>" >
                  </label>--%>
                  <label class="input">
                    <input name="uploadify" id="attchmentFileName" placeholder="" type="file">
                    <input name="attchmentFileId" id="attchmentFileId" style="display:none" value="<s:iterator id="list" value="attchment"><s:property value="#list.id"/>,</s:iterator>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input disabled style="border: hidden" type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="strongBig.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="strongBig.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden"  disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="strongBig.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>

    var draft = "<s:property value="draft" />";
    inputLoad({
        objId:"attchmentFileName",
        entityName:"attchmentId",
        sourceId:"attchmentFileId",
        jsessionid:"<%=jsessionid%>"
    });


    $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"strongbig",
        todo:"1"
    };
    multiDuty(pdata);
  });
    $("a[key=btn-choose-meetinguse]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择相应会议记录",
            url:"ajax-dialog!meetingUse.action?viewtype=2",
            width:800
        }).show();
    });
    //通知个人
    $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择通知个人",
            url:"ajax-dialog!levelUser.action?key=users&more=1",//more==1:多选，more!=1:单选
            width:560
        }).show();
    });
  /*$("a[key=btn-choose-users]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择人员",
          url:"ajax-dialog!user.action",
          width:340
      }).show();
  });*/

  //返回视图
  $("#btn-re-common").click(function(){
      if(draft == 1){
          location.href="index.action";
      }else{
          loadURL("../party/ajax-strongbig.action?viewtype=<s:property value="viewtype"/>",$('#content'));
      }
  });
  //校验
  $("#strongBig").validate({
    rules : {
        registerName : {
        required : true
      },
      strongBigRecord : {
        required : true
      },
      conclusion : {
        required : true
      },
      usersName : {
        required : true
      },
        meetingUseId : {
        required : true
      },
      tellUsersId : {
          required : true
      },
        attchmentId : {
          required : true
      },

    },
    messages : {
        registerName : {
        required : "请输入登记事宜名称"
      },
      strongBigRecord : {
        required : "请输入备案"
      },
      conclusion : {
        required : "请输入结论"
      },
      usersName : {
        required : "请选择通知对象"
      },
        meetingUseId : {
        required : "请选择相应会议记录"
      },
      tellUsersId : {
          required : "请选择通知对象"
      },
        attchmentId : {
          required : "请上传附件"
      },
    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  $('.hasDatepicker').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(

        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("strongBig","../party/ajax-strongbig!save.action",null,
              function (data) {
                  if(data.state == '200' || data.state ==200){
                      if(draft == 1){
                          location.href="index.action";
                      }else {
                          loadURL("../party/ajax-strongbig.action?viewtype="+"<s:property value="viewtype"/>",$('#content'));
                      }
                  }
              }
          );
        }
  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#strongBig").valid()) {
              return false;
            }
              $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
              }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                  e.preventDefault();
                  e.stopPropagation();
                  return;
                }
                if (ButtonPressed === "确认") {
                  var $validForm = $("#strongBig").valid();
                  if(!$validForm) return false;
                  $("#btn-confirm-common").attr("disabled", "disabled");
                  $("#btn-recommit-common").attr("disabled", "disabled");
                  form_save("strongBig","../party/ajax-strongbig!commit.action?viewtype="+"<s:property value="viewtype"/>",null,
                      function (data) {
                          if(data.state == '200' || data.state ==200){
                              if(draft == 1){
                                  location.href="index.action";
                              }else{
                                  loadURL("../party/ajax-strongbig.action?viewtype="+"<s:property value="viewtype"/>",$('#content'));
                              }
                          }
                      }
                  );
                }
              });
          }
  );
</script>
