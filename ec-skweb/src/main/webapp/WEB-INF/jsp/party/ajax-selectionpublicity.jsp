<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>

<div class="row">
    <%--<jsp:include page="selectCommon.jsp"></jsp:include>--%>
      <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
          <i class="fa fa-table fa-fw "></i>
          结果公示
        </h1>
      </div>
</div>

<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">

            <shiro:hasAnyRoles name="wechat">

                <a id="ajax_selectionpublicity_btn_add" <s:property value="isCreate('selectionpublicity')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i>新建结果公示</a>
            </shiro:hasAnyRoles>
          <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />
         <%-- parentId=<s:property value="parentId"/>--%>
        </form>
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_selectionpublicity_list_row">
                    <table id="ajax_selectionpublicity_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_selectionpublicity_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">

  $(function(){
     /*/!*$("#h1 >a").each(function (index) {

         /!* if(index==backIndex){
              $(this).attr("style","color: red");
              return;
          }*!/
      }*!/
     $("#h1 > a")*!/*/
      load_selectionpublicity_jqGrid();
      if(typeof table_global_width !="undefined"){
          $("#ajax_selectionpublicity_list_row").width(table_global_width);
          jQuery("#ajax_selectionpublicity_table").jqGrid('setGridWidth', table_global_width);
      }
  });
  function load_selectionpublicity_jqGrid(){
    jQuery("#ajax_selectionpublicity_table").jqGrid({
      url:'../party/ajax-selectionpublicity!list.action?parentId='+$('#parentId').val(),
      datatype: "json",
      colNames:['选拔人员','公示期','公示结果','文档状态','操作','id'],
      colModel:[
        {name:'applyUsers',index:'applyUsers_name', width:100,search:true,sortable:false},
        {name:'publicDate',index:'publicStartDate', width:200,sortable:true,search:true},
        {name:'result',index:'result', width:200,sortable:true,search:true},
        {name:'processState',index:'processState', width:200,sortable:true,search:false},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_selectionpublicity_list_page',
      sortname : 'createDate',
      sortorder : 'desc',
      gridComplete:function(){
        var ids=$("#ajax_selectionpublicity_table").jqGrid('getDataIDs');

        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_selectionpublicity_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_selectionpublicity_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_selectionpublicity_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 结果公示",
      multiselect : false,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_selectionpublicity_table").jqGrid('setGridWidth', $("#ajax_selectionpublicity_list_row").width());
    })
    jQuery("#ajax_selectionpublicity_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_selectionpublicity_table").jqGrid('navGrid', "#ajax_selectionpublicity_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  /*$("h1 > a").click(function () {
      $("#h1 >a").removeAttr("style");
      var i=$(this).index();
      $("#h1 >a").each(function (backIndex) {
          if(backIndex==0 && backIndex==i){

              loadURL("../party/ajax-selectionnotice.action?viewtype=<s:property value="viewtype"/>"+"&backIndex="+backIndex,$('#content'));
              return;
          }else if(backIndex==1 && backIndex==i){
              loadURL("../party/ajax-selectionpublicity.action?viewtype=<s:property value="viewtype"/>"+"&backIndex="+backIndex,$('#content'));
              return;
          }
      })
  })*/




  function fn_selectionpublicity_read(id){
    if($('div#s4').length>=1){
        loadURL("../party/ajax-selectionpublicity!read.action?keyId="+id+"&parentId="+$("#parentId").val(),$('div#s4'));
    }else{
        loadURL("../party/ajax-selectionpublicity!read.action?keyId="+id+"&parentId="+$("#parentId").val(),$('#content'));
    }
  }

  $("#ajax_selectionpublicity_btn_add").off("click").on("click",function(){
      if($('div#s4').length>=1){
          loadURL("../party/ajax-selectionpublicity!input.action?parentId="+$("#parentId").val(),$('div#s4'));
      }else{
          loadURL("../party/ajax-selectionpublicity!input.action?parentId="+$("#parentId").val(),$('#content'));
      }

  })
</script>





















