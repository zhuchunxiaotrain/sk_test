<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>


<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
   <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      公告
    </h1>
  </div>
  <%--  <jsp:include page="selectCommon.jsp"></jsp:include>--%>
</div>

<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">

          <s:if test="viewtype == 1">
            <shiro:hasAnyRoles name="wechat">

              <a id="ajax_selectionnotice_btn_add" <s:property value="isCreate('selectionnotice')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建公告</a>
            </shiro:hasAnyRoles>
          </s:if>


        </form>
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div id="myTabContent1" class="tab-content padding-10 ">
                  <div class="row tab-pane fade in active" id="ajax_selectionnotice_list_row">

                    <table id="ajax_selectionnotice_table" class="table table-striped table-bordered table-hover">

                    </table>
                    <div id="ajax_selectionnotice_list_page">
                      <input type="hidden" name="flag" id="flag" value="<s:property value="flag" />"/>
                      <input type="hidden" name="todo" id="todo" value="<s:property value="todo" />"/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_selectionnotice_jqGrid();

  });
  function load_selectionnotice_jqGrid(){
    jQuery("#ajax_selectionnotice_table").jqGrid({
      url:'../party/ajax-selectionnotice!list.action?viewtype='+"<s:property value="viewtype"/>"+"&flag="+$("#ajax_selectionnotice_list_page #flag").val()+"&todo="+$("#ajax_selectionnotice_list_page #todo").val(),
      datatype: "json",
      colNames:['岗位','报名周期','公告文档状态','干部选拔文档状态','操作','id'],
      colModel:[
        {name:'post',index:'post_name', width:100,search:true,sortable:false},
        {name:'selectDate',index:'selectDate', width:200,sortable:true,search:true},
        {name:'processState',index:'processState', search:true,width:80,sortable:true},
        {name:'selectionFinished',index:'selectionFinished', search:false,width:80,sortable:true},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_selectionnotice_list_page',
      sortname : 'createDate',
      sortorder : 'desc',
      gridComplete:function(){
        var ids=$("#ajax_selectionnotice_table").jqGrid('getDataIDs');

        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_selectionnotice_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_selectionnotice_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_selectionnotice_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 公告",
        //hiddengrid:true,
      multiselect : false,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_selectionnotice_table").jqGrid('setGridWidth', $("#ajax_selectionnotice_list_row").width());
    })
    jQuery("#ajax_selectionnotice_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_selectionnotice_table").jqGrid('navGrid', "#ajax_selectionnotice_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };
$("#btn-notice").click(function () {
    loadURL("../party/ajax-selectionnotice.action?viewtype=<s:property value="viewtype"/>"+"&flag="+$("#ajax_selectionnotice_list_page #flag").val(),$('#content'));
});
  function fn_selectionnotice_read(id){
    loadURL("../party/ajax-selectionnotice!read.action?keyId="+id+"&viewtype="+"<s:property value="viewtype" />"+"&flag="+$("#ajax_selectionnotice_list_page #flag").val(),$('#content'));
  }

  $("#ajax_selectionnotice_btn_add").off("click").on("click",function(){
    loadURL("../party/ajax-selectionnotice!input.action?viewtype=<s:property value="viewtype"/>"+"&flag="+$("#ajax_selectionnotice_list_page #flag").val(),$('#content'));
  });

</script>





















