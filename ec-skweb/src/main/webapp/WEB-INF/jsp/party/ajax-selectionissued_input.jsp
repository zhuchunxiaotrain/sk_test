<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<s:if test="draft == 1">
  <jsp:include page="../com/ajax-top.jsp" />
</s:if>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-input-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="selectionIssued==null || selectionIssued.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="selectionIssued!=null && selectionIssued.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="selectionIssued" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="selectionIssued.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />
            <input type="hidden" name="todo" id="todo" value="<s:property value="todo"/>" />
            <header  style="display: block;">
              聘书发放基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
<%--
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-selectionpublicity">聘书发放人员</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="selectionPublicityApplyName" name="selectionPublicityApplyName"
                             value="<s:property value="selectionPublicity.selectionSpeech.creater.name"/>"/>
                      <input type="hidden" id="selectionPublicityId" name="selectionPublicityId"
                             value="<s:property value="selectionPublicity.id"/>"/>
                    </label>
                  </label>
                </section>
              </div>
--%>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  聘书发放附件
                </label>
                <section class="col col-5">
                 <%-- <label class="input">
                    <input type="text" name="attchmentText" id="attchmentText" placeholder="请输入聘书发放附件" value="<s:property value="selectionIssued.attchmentText"/>" >
                  </label>--%>
                  <label class="input">
                    <input name="uploadify" id="issuedAttchmentFileName" placeholder="" type="file">
                    <input name="issuedAttchmentFileId" id="issuedAttchmentFileId" style="display:none" value="<s:iterator id="list" value="attchment"><s:property value="#list.id"/>,</s:iterator>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  聘任开始时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="issueStartDate" id="issueStartDate" placeholder="请选择聘任时间" value="<s:date name="selectionIssued.issueStartDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  聘任结束时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="issueEndDate" id="issueEndDate" placeholder="请选择聘任时间" value="<s:date name="selectionIssued.issueEndDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="selectionIssued.creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="selectionIssued.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
    var draft = "<s:property value="draft" />";
  inputLoad({
      objId:"issuedAttchmentFileName",
      entityName:"issuedAttchmentId",
      sourceId:"issuedAttchmentFileId",
      jsessionid:"<%=jsessionid%>"
  });

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"selectionissued",
        todo:"1"
    };
    multiDuty(pdata);
  });



  $("a[key=btn-choose-selectionpublicity]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择结果公示通过人员",
          url:"ajax-dialog!selectionpublicity.action?param=selectionIssued",
          width:600
      }).show();
  });
  $("a[key=btn-choose-selectionnotice]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择公告",
          url:"ajax-dialog!selectionnotice.action",
          width:700
      }).show();
  });

  //返回视图
  $("#btn-apply-input-common").click(function(){
      if(draft == 1){
          location.href="index.action";
      }else{
          loadURL("../party/ajax-selectionissued.action?parentId="+$("#parentId").val(),$('div#s5'));
      }
      /*if($('div#s5').length>=1){
          loadURL("../party/ajax-selectionissued.action?parentId="+$("#parentId").val(),$('div#s5'));
      }else{
          if($("#selectionIssued #todo").val()=="1"){
              var parentId = "<s:property value="selectionNotice.id"/>";
              loadURL("../party/ajax-selectionnotice.action?parentId="+parentId+"&todo="+$("#selectionIssued #todo").val(),$('#content'));
          }

      }*/

  });
  //校验
  $("#selectionIssued").validate({
    rules : {
        selectionPublicityId : {
        required : true
      },
        issuedAttchmentId:{
        required : true
      },
        issueStartDate:{
       required : true,
            date:true
      },
        issueEndDate:{
            required : true,
            min:$("#issueStartDate").val(),
            date:true
        },

    },
    messages : {
        selectionPublicityId : {
            required :  '请聘书发放人员'
        },
        issuedAttchmentId:{
            required : '请上传发放附件'
        },
        issueStartDate:{
            required : '请选择聘任开始时间',
            date:'请输入正确日期格式'
        },
        issueEndDate:{
            required : '请选择聘任结束时间',
            min:'结束时间大于开始时间',
            date:'请输入正确日期格式'
        },

    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  $('#issueStartDate,#issueEndDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(

        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("selectionIssued","../party/ajax-selectionissued!save.action?parentId="+$('#parentId').val(),null,
              function (data) {
                  if(data.state == '200' || data.state == 200){
                      if(draft == 1){
                          location.href="index.action";
                      }else{
                          loadURL("../party/ajax-selectionissued.action?parentId="+$('#parentId').val(),$("div#s5"));
                      }
                      /*if($('div#s5').length>=1){
                          loadURL("../party/ajax-selectionissued.action?parentId="+$('#parentId').val(),$("div#s5"));
                      }else{
                          if($("#selectionIssued #todo").val()=="1") {
                              var parentId = "<s:property value="selectionNotice.id"/>";
                              loadURL("../party/ajax-selectionnotice.action?parentId="+parentId+"&todo="+$("#selectionIssued #todo").val(),$('#content'));
                          }

                      }*/
                  }
              }
          );
        }
  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#selectionIssued").valid()){
              $("#areaselect_span").hide();
              return false;
            }
              var startDate=$("#issueStartDate").val();
              var endDate=$("#issueEndDate").val();
              var nowDate=new Date();
              var dbStart=new Date(startDate.replace(/-/g, "/"));
              var dbEnd=new Date(endDate.replace(/-/g, "/"));
              if(dbStart<nowDate){
                  alert("聘任开始日期大于今天日期");
                  return false;
              }
              if(dbEnd<dbStart){
                  alert("聘任结束日期大于聘任开始日期");
                  return false;
              }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#selectionIssued").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("selectionIssued","../party/ajax-selectionissued!commit.action?parentId="+$('#parentId').val(),null,
                    function (data) {
                        if(data.state == '200' || data.state == 200){
                            if(draft == 1){
                                location.href="index.action";
                            }else{
                                loadURL("../party/ajax-selectionissued.action?parentId="+$('#parentId').val(),$('div#s5'));
                            }
                            /*if($('div#s5').length>=1){
                                loadURL("../party/ajax-selectionissued.action?parentId="+$('#parentId').val(),$('div#s5'));
                            }else{
                                if($("#selectionIssued #todo").val()=="1") {
                                    var parentId = "<s:property value="selectionNotice.id"/>";
                                    loadURL("../party/ajax-selectionnotice.action?parentId="+parentId+"&todo="+$("#selectionIssued #todo").val(),$('#content'));
                                }
                            }*/
                        }
                    }
                );
              }
            });
          }
  );
</script>
