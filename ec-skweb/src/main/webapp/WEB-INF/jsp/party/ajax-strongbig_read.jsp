<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回三重一大</a>


          <%--<ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 三重一大 </a>
            </li>
           &lt;%&ndash; <li class="disabled">
              <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 查看明细</a>
            </li>&ndash;%&gt;
          </ul>--%>
          <shiro:hasAnyRoles name="wechat">
            <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(strongBig.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
          </shiro:hasAnyRoles>
          <%--<div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">--%>

             <%--   <a href="javascript:void(0);" key="btn-comment">
                      <span style="margin-top: -13px !important;" class="btn btn-default pull-right">
                          <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                      </span>
                </a>
--%>
           <%-- <shiro:hasAnyRoles name="wechat">
                <a style="margin-top: -13px !important;" <s:property value="isEdit(strongBig.id)"/> class="btn btn-default pull-right pull-right-fix" key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>--%>

          <hr class="simple">

          <form id="strongBig" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="strongBig.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="viewtype" id="viewtype" value="<s:property value="viewtype" />"/>
            <input type="hidden" name="todo" id="todo" value="<s:property value="todo" />"/>
            <input type="hidden" name="index" id="index" value="<s:property value="index" />"/>
            <input type="hidden" name="remind" id="remind" value="<s:property value="remind" />"/>
            <input type="hidden" name="record" id="record" value="<s:property value="record" />"/>
            <input type="hidden" name="draft" id="draft" value="<s:property value="draft" />"/>
            <header  style="display: block;">
              三重一大登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  登记事宜名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="registerName" id="registerName" placeholder="请输入登记事宜名称" value="<s:property value="strongBig.registerName"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  三重一大会议类型
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="strongBigType">
                    <label class="radio">
                      <input type="radio" checked="checked" name="type" value="1" <s:property value="strongBig.type==1?'checked':''"/>>
                      <i></i>部门三重一大会议</label>
                    <label class="radio">
                      <input type="radio"  name="type" value="2" <s:property value="strongBig.type==2?'checked':''"/>>
                      <i></i>中心党政联席会议</label>
                    <label class="radio">
                      <input type="radio"  name="type" value="3" <s:property value="strongBig.type==3?'checked':''"/>>
                      <i></i>党总支会议</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否需要提交中心审核
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="strongBigType">
                    <label class="radio">
                      <input type="radio" checked="checked" name="isCheck" value="1" <s:property value="strongBig.isCheck==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio"  name="isCheck" value="2" <s:property value="strongBig.isCheck==2?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  备案
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="strongBigRecord" id="strongBigRecord" placeholder="请输入备案" value="<s:property value="strongBig.strongBigRecord"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  结论
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="conclusion" id="conclusion" placeholder="请输入结论" value="<s:property value="strongBig.conclusion"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users"> 通知对象</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="usersName" name="tellUsersName"
                             value="<s:iterator id="list" value="tellUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden" id="usersId" name="tellUsersId"
                             value="<s:iterator id="list" value="tellUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-meetinguse">相应会议记录</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input disabled type="text" id="meetingUseName" name="meetingUseName" placeholder="请选择相应会议记录"
                           value="<s:iterator id="list" value="meetingUse"><s:property value="#list.topic"/>,</s:iterator>"/>
                    <input type="hidden" id="meetingUseId" name="meetingUseId"
                           value="<s:iterator id="list" value="meetingUse"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  三重一大相关附件
                </label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input type="text" name="attchmentText" id="attchmentText" placeholder="请输入相关附件" value="<s:property value="strongBig.attchmentText"/>" >
                  </label>--%>
                  <label class="input">
                    <input name="attchmentFileId" id="attchmentFileId" style="display:none" value="<s:iterator id="list" value="attchment"><s:property value="#list.id"/>,</s:iterator>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input disabled style="border: hidden" type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="strongBig.creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input disabled style="border: hidden" type="text" name="applyUserDate" id="applyUserDate" placeholder="系统自动" value="<s:date name="strongBig.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
          <div class="flow">
            <s:if test="strongBig.getProcessState().name()=='Running'">
              <div class="f_title">审批意见</div>
              <div class="chat-footer"style="margin-top:5px">
                <div class="textarea-div">
                  <div class="typearea">
                    <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                  </div>
                </div>
                <span class="textarea-controls"></span>
              </div>
            </s:if>
            <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow"></div>
            </div>
            <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext"></div>
            </div>
          </div>
       <%-- </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
      </div>--%>
    </div>
   </div>
  </div>
  </article>
</div>

<script type="text/javascript">
    readLoad({
        objId:"attchmentFileId",
        entityName:"attchmentId",
        sourceId:"attchmentFileId"
    });

  $(function(){
     $("input").attr("disabled","disabled").parent().addClass(" state-disabled")
  });

    var table_global_width=0;
    $("a#other1").off("click").on("click",function(e) {
        if (table_global_width == 0) {
            table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
        }
        loadURL("../manage/ajax!noticequery.action?parentId="+$("#keyId").val(),$('div#s2'));
    });

    $(function(){
        loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
        loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
        ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
            var showDuty = false;
            var area = $("span.textarea-controls");
            $(pdata.data.datarows).each(function(i,v){
                var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
                $(area).append(str);
                if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                    showDuty = true;
                }
            });
            if(showDuty == true){
                var pdata = {
                    keyId: $("input#keyId").val(),
                    flowName: "strongbig"
                };
                multiDuty(pdata);
            }
        });
        var valueObj=$("input#numStatus").val();
        switch(valueObj){
            case "0":
                //保存后再提交
                $("#left_foot_btn_approve").off("click").on("click",function(){
                    form_save("strongBig","../party/ajax-strongbig!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                        $("#btn-re-common").trigger("click");
                    })
                });
                break;
            case "1":
                //第一步审批
                $("#left_foot_btn_approve").off("click").on("click",function(){
                    form_save("strongBig","../party/ajax-strongbig!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                        $("#btn-re-common").trigger("click");
                    });
                });
                break;
            case "2":
                //第二步审批
                $("#left_foot_btn_approve").off("click").on("click",function(){
                    form_save("strongBig","../party/ajax-strongbig!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                        $("#btn-re-common").trigger("click");
                    })
                });
                break;
            case "3":
                //第三步审批
                $("#left_foot_btn_approve").off("click").on("click",function(){
                    form_save("strongBig","../party/ajax-strongbig!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                        $("#btn-re-common").trigger("click");
                    })
                });
                break;
        }
        //退回
        $("#left_foot_btn_sendback").off("click").on("click",function(){
            var vActionUrl="../party/ajax-strongbig!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
            var data={keyId:$("input#keyId").val()};
            ajax_action(vActionUrl,data,{},function(pdata){
                _show(pdata);
                $("#btn-re-common").trigger("click");
            });
        });
        //否决
        $("#left_foot_btn_deny").off("click").on("click",function(){
            var vActionUrl="../party/ajax-strongbig!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
            var data={keyId:$("input#keyId").val()};
            ajax_action(vActionUrl,data,{},function(pdata){
                _show(pdata);
                $("#btn-re-common").trigger("click");
            });
        });
        //流程信息展开
        $('#flow,#next').click(function(){
            if($(this).hasClass("right")){
                $(this).removeClass("right").addClass("down");
                $(this).parent(".f_title").next("div.f_content").show();
            }else{
                $(this).removeClass("down").addClass("right");
                $(this).parent(".f_title").next("div.f_content").hide();
            }
        });
    });

    //返回视图
    $("#btn-re-common").click(function(){
        var index = "<s:property value="index" />";
        var todo = "<s:property value="todo" />";
        var remind = "<s:property value="remind" />";
        var record = "<s:property value="record" />";
        var draft = "<s:property value="draft" />";
        if(index==1){
            loadURL("../com/ajax-index!page.action",$('#content'));//主页
        }else if(todo==1){
            loadURL("../com/ajax!toDoList.action",$('#content'));//个人代办
        }else if(remind==1){
            loadURL("../com/ajax!remindList.action",$('#content'));//提醒事项
        }else if(record==1){
            loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));//已办未结
        }else if(record==2){
            loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));//已办已结
        }else if(draft==1){
            loadURL("../com/ajax!draftList.action",$('#content'));//草稿
        }else{
            loadURL("../party/ajax-strongbig.action?viewtype="+$("#viewtype").val(),$('#content'));//zai
        }
        /*if($("#strongBig #todo").val()=="1"){
            loadURL("../party/ajax-strongbig.action?viewtype=1",$('#content'));
        }else{
            loadURL("../party/ajax-strongbig.action?viewtype="+$("#viewtype").val(),$('#content'));
        }*/

    });

    //编辑
    $("a[key=ajax_edit]").click(function(){
        var draft = "<s:property value="draft" />";
        loadURL("../party/ajax-strongbig!input.action?keyId="+$("input#keyId").val()+"&viewtype="+$("#viewtype").val()+"&draft="+draft,$('#content'));
    });
  
  
  
  
/*
  //返回视图
  $("#btn-re-common").click(function(){
    loadURL("../party/ajax-strongbig.action?viewtype,$('#content'));
  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    loadURL("../party/ajax-strongbig!input.action?keyId="+$("input#keyId").val(),$('#content'));
  });

  //左侧意见栏
  $("a[key=btn-comment]").unbind("click").bind("click",function(){
    var keyId = $("input#keyId").val();
    var actionUrl = "ajax-running!comment.action?bussinessId="+keyId;/!*bussinessId是文档的ID*!/
    $(this).leftview('init',{
      title:"查看审批意见",
      actionUrl:actionUrl
    });
    var keyId = $("#keyId").val();
    $(this).leftview('foot',{'unid':keyId,'flowName':"strongbig",callback:function(){
      var valueObj=$("input#numStatus").val();
      switch(valueObj){
          case "0":
              //保存后再提交
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("strongBig","../party/ajax-strongbig!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $(this).leftview('leftClose');
                      $("#btn-re-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
        case "1":
          //第一步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("strongBig","../party/ajax-strongbig!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-re-common").trigger("click");
            });

          });
          break;
        case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("strongBig","../party/ajax-strongbig!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
            $(this).leftview('leftClose');
            $("#btn-re-common").trigger("click");
          })
        });
        break;
        /!*case "3":
          //第三步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("strongBig","../party/ajax-strongbig!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-re-common").trigger("click");
            })
          });
        // approveRequest("approve1");
        break;*!/
      }
      //退回
      $("#left_foot_btn_sendback").off("click").on("click",function(){
        var vActionUrl="../party/ajax-strongbig!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("input#keyId").val()};

        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-re-common").trigger("click");
        });
      });
      //否决
      $("#left_foot_btn_deny").off("click").on("click",function(){
        var vActionUrl="../party/ajax-strongbig!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("input#keyId").val()};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-re-common").trigger("click");
        });
      });
    }});
  })*/

</script>