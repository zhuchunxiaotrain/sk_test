<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-input-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>退管会基本信息</a>
          <s:if test="rebateActivity==null || rebateActivity.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="rebateActivity!=null && rebateActivity.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="rebateActivity" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="rebateActivity.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="viewtype" id="viewtype" value="<s:property value="viewtype"/>" />
            <header  style="display: block;">
              退管会基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row ">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-dict-activitytype"> 活动类型</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="activityTypeName" name="activityTypeName" placeholder="请选择活动类型"
                             value="<s:iterator id="list" value="activityType"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden" id="activityTypeId" name="activityTypeId"
                             value="<s:iterator id="list" value="activityType"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-department">所属单位</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="rebateActivityDepartName" name="departName" placeholder="请选择所属单位"
                             value="<s:iterator id="list" value="department"><s:property value="#list.name"/></s:iterator>"/>
                      <input type="hidden" id="rebateActivityDepartId" name="departId"
                             value="<s:iterator id="list" value="department"><s:property value="#list.id"/></s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>


              <div class="row">
                <label class="label col col-2">
                </label>
                <section class="col col-2">
                  <label class="input">
                      <i class="fa fa-asterisk txt-color-red"></i>
                    活动费用科目
                  </label>
                </section>
                <section class="col col-1">
                  <label class="input">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    预算金额
                  </label>
                </section>
                <section class="col col-1">
                  <label class="input">
                    已用金额
                  </label>
                </section>
                <section class="col col-2">
                  <label class="input">
                    实际金额
                  </label>
                </section>
                <section class="col col-1">
                  <label class="input">
                    剩余金额
                  </label>
                </section>
              <section class="col col-1">
                  <label class="input">
                    <a id="btn-add-row" href="javascript:void(0);"><i class="fa fa-rocket"></i> 增加行</a>
                  </label>
                </section>
              </div>
            <div id="add-annoualbudget-row">
                <s:iterator id="list" value="rebateActivityDetailList" status="st">
                                <div class="row" name="delDiv">
                                  <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" class="dict-dialog" id="btn_choose_dict_0" onclick="selectdict(<s:property value="#st.getIndex()"/>)">活动费用科目</a>
                                  </label>
                                  <section class="col col-2">
                                  <label class="input">
                                    <label class="input state-disabled">
                                      <input disabled type="text" id="annualBudgetName_<s:property value="#st.getIndex()"/>" name="annualBudgetName" placeholder="请输入活动费用科目"
                                             value="<s:property value="#list.annualBudgetDetail.annualBudgetItem.name"/>"/>
                                      <input type="hidden" id="rebateActivityDetailId_<s:property value="#st.getIndex()"/>"  name="rebateActivityDetailId"
                                             value="<s:property value="#list.id"/>"/>
                                      <input type="hidden" id="annualBudgetDetailId_<s:property value="#st.getIndex()"/>"  name="annualBudgetDetailId"
                                             value="<s:property value="#list.annualBudgetDetail.id"/>"/>
                                    </label>
                                  </label>
                                </section>
                                  <section class="col col-1">
                                    <label class="input">
                                      <label class="input state-disabled">
                                        <input disabled  type="text" id="annualBudgetMoney_<s:property value="#st.getIndex()"/>" name="annualBudgetMoney" placeholder="自动计算"
                                                value="<s:property value="#list.annualBudgetDetail.budgetMoney"/>"/>
                                      </label>
                                    </label>
                                  </section>
                                  <section class="col col-1">
                                    <label class="input">
                                      <label class="input state-disabled">
                                        <input disabled  type="text" name="usedAmount" id="usedAmount_<s:property value="#st.getIndex()"/>" placeholder="自动计算"
                                               value="<s:property value="#list.usedAmount"/>"/>
                                      </label>
                                    </label>
                                  </section>
                                  <section class="col col-2">
                                    <label class="input">
                                      <label class="input">
                                        <input  type="text" id="actualAmount_<s:property value="#st.getIndex()"/>" name="actualAmount" placeholder="请输入实际金额"
                                                value="<s:property value="#list.actualAmount"/>"/>
                                      </label>
                                    </label>
                                  </section>
                                  <section class="col col-1">
                                    <label class="input">
                                      <label class="input state-disabled">
                                        <input disabled  type="text" name="remainAmount" id="remainAmount_<s:property value="#st.getIndex()"/>" placeholder="自动计算"
                                                value="<s:property value="#list.remainAmount"/>"/>
                                      </label>
                                    </label>
                                  </section>
                                  <section class="col col-1">
                                    <label class="input">
                                      <a  href="javascript:void(0);" onclick="oncheck(<s:property value="#st.getIndex()"/>)" class="del-div"><i class="fa fa-rocket" ></i>删除行</a>
                                    </label>
                                  </section>
                                </div>
                </s:iterator>
            </div>
               <div class="row">
                  <label class="label col col-2">
                    操作人
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="rebateActivity.creater.name"/>" >
                      <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="rebateActivity.creater.id"/>" >
                    </label>
                  </section>
                    <label class="label col col-1">
                    操作日期
                    </label>
                    <section class="col col-2 ">
                    <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="rebateActivity.createDate" format="yyyy-MM-dd"/>" >
                    </label>
                    </section>
                    </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>

    var draft = "<s:property value="draft" />";
  $(function(){
     //dialogDict();
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"rebateactivity",
        todo:"1"
    };
    multiDuty(pdata);
  });

$("#btn-add-row").bind("click",function(){
    var str='<div class="row" name="delDiv"> <label class="label col col-2"> <i class="fa fa-asterisk txt-color-red"></i> <a  href="javascript:void(0);" class="dict-dialog" id="btn_choose_dict_0" onclick="selectdict(0)">活动费用科目</a> </label> <section class="col col-2"> <label class="input"> <label class="input state-disabled"> <input disabled type="text"  name="annualBudgetName" placeholder="请选择活动费用科目"value=""/> <input type="hidden"  name="annualBudgetDetailId" value=""/> </label> </label> </section> <section class="col col-1"> <label class="input"> <label class="input state-disabled"> <input disabled  type="text" name="annualBudgetMoney" placeholder="自动计算"value=""/> </label> </label> </section> <section class="col col-1"> <label class="input"> <label class="input state-disabled"> <input disabled  type="text" name="usedAmount" placeholder="自动计算" value=""/> </label> </label> </section> <section class="col col-2"> <label class="input"> <label class="input"> <input  type="text" name="actualAmount" placeholder="请输入实际金额"value=""/> </label> </label> </section> <section class="col col-1"> <label class="input"> <label class="input state-disabled"> <input disabled  type="text" name="remainAmount" placeholder="自动计算" value=""/> </label> </label> </section> <section class="col col-1"> <label class="input"> <a onclick="oncheck(0)"  href="javascript:void(0);" class="del-div"><i class="fa fa-rocket"></i>删除行</a> </label> </section> </div>';
    $("#add-annoualbudget-row").append(str);
    dialogDict();

});
/*
  $("#annualBudget").validate({
      rules : {
          departId : {
              required : true
          },
          annualBudgetItemId:{
              required : true
          },
          budgetMoney:{
              required : true,
              number:true,
              min:0
          },

      },
      messages : {
          departId : {
              required :  '请选择所属单位'
          },
          annualBudgetItemId:{
              required : '请选择年度预算科目'
          },
          budgetMoney:{
              required : '请输入预算金额',
              number:'请输入数字',
              min:'请输入正数'
          },

      },
      ignore: "",
      errorPlacement : function(error, element) {
          error.insertAfter(element.parent());
      }
  });
*/


//验证
  $("#rebateActivity").validate({
      rules : {
          departId:{
              required : true
          },
          activityTypeId:{
              required : true
          }
      },
      messages : {
          departId:{
              required : '请选择所属单位'
          },
          activityTypeId:{
              required : '请选择活动类型'
          }
      },
      ignore: "",
      errorPlacement : function(error, element) {
          error.insertAfter(element.parent());
      }
  });

  $("a[key=btn-choose-dict-activitytype]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择活动类型",
          url:"ajax-dialog!dict.action?dictName=ActivityType",
          width:340
      }).show();
  });
  function oncheck(i){
      var annualBudgetName$=$("input[name='annualBudgetName']");
      annualBudgetName$.each(function (index) {
          if(index==i){
              $(this).closest("div").remove();
              dialogDict();
              return false;
          }
      })

  }


var dialogDict=function(){
    var array$=$("#add-annoualbudget-row .dict-dialog");
    var annualBudgeName$=$("#add-annoualbudget-row input[name='annualBudgetName']");
    var annualBudgetDetailId$=$("#add-annoualbudget-row input[name='annualBudgetDetailId']");
    var annualBudgetMoney$=$("#add-annoualbudget-row input[name='annualBudgetMoney']");
    var actualAmount$=$("#add-annoualbudget-row input[name='actualAmount']");//实际金额
    var remainAmount$=$("#add-annoualbudget-row input[name='remainAmount']");//剩余金额
    var usedAmount$=$("#add-annoualbudget-row input[name='usedAmount']");//已用金额
    var delDiv$=$("#add-annoualbudget-row .del-div");
   // alert("array$_length = "+array$.length);
   // alert("annualBudgeName$_length = "+annualBudgeName$.length);
   // alert("annualBudgetId$_length = "+annualBudgetId$.length);
    array$.each(function (index) {
        //alert("index_"+index);
        $(this).attr("id","btn_choose_dict_"+index);
        $(this).attr("onclick","selectdict("+index+")");
        if(index==array$.length){
            return false;
        }
    });
    annualBudgeName$.each(function (index_1) {
        //alert("index_1_"+index_1);
        $(this).attr("id","annualBudgetName_"+index_1);
        if(index_1==annualBudgeName$.length){
            return false;
        }
    });
    annualBudgetDetailId$.each(function (index_2) {
      //  alert("index_2_"+index_2);
        $(this).attr("id","annualBudgetDetailId_"+index_2);
        if(index_2==annualBudgetDetailId$.length){
            return false;
        }
    });
    annualBudgetMoney$.each(function (index_3) {
        //  alert("index_2_"+index_2);
        $(this).attr("id","annualBudgetMoney_"+index_3);
        if(index_3==annualBudgetMoney$.length){
            return false;
        }
    });
    actualAmount$.each(function (index_4) {
        //  alert("index_2_"+index_2);
        $(this).attr("id","actualAmount_"+index_4);
        if(index_4==actualAmount$.length){
            return false;
        }
    });
    remainAmount$.each(function (index_5) {
        //  alert("index_2_"+index_2);
        $(this).attr("id","remainAmount_"+index_5);
        if(index_5==remainAmount$.length){
            return false;
        }
    });
    usedAmount$.each(function (index_6) {
        //  alert("index_2_"+index_2);
        $(this).attr("id","usedAmount_"+index_6);
        if(index_6==usedAmount$.length){
            return false;
        }
    });
    delDiv$.each(function (index_7) {
        //alert("index_"+index);
        $(this).attr("onclick","oncheck("+index_7+")");
        if(index_7==delDiv$.length){
            return false;
        }
    });
}
  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择单位",
          url:"ajax-dialog!departParty.action?param=rebateactivity",
          width:500
      }).show();
  });
function selectdict(index) {
    if($("#rebateActivityDepartId").val()==""){
        alert("请先选中所属单位");
    }else{
            var annualBudgetDetailId$=$("input[name='annualBudgetDetailId']");
            var str='';
            annualBudgetDetailId$.each(function () {
                str+=','+$(this).val();
            });

            gDialog.fCreate({
                title:"请选择活动类型",
                url:"ajax-dialog!annualbudget.action?param=rebateActivity&index="+index+"&departId="+$("#rebateActivityDepartId").val()+"&str="+str,
                width:500
            }).show();
    }

}

  //返回视图
  $("#btn-apply-input-common").click(function(){
      if(draft == 1){
          location.href="index.action";
      }else{
          loadURL("../party/ajax-rebateactivity.action?viewtype="+$("#viewtype").val(),$('#content'));
      }
  });
  //校验

  $("#rebateActivity").validate({
    rules : {
        activityTypeId : {
            required : true
      },
        departId : {
            required : true
        },
        annualBudgetId:{
            required : true
      },
        actualAmount:{
            required : true,
            number:true,
            min:0,

      },

    },
    messages : {
        activityTypeId : {
            required :  '请输入活动类型'
        },
        departId : {
            required :  '请输入所属单位'
        },
        annualBudgetId:{
            required : '请输入年度预算科目'
        },
        actualAmount:{
            required : '请输入实际金额',
            number:'请输入数字',
            min:'请输入正数'
        },

    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  $('#selectDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(

        function(){
          if(!$("#rebateActivity").valid()){
              return false;
          }
            if($("input[name='annualBudgetDetailId']").length<1){
                alert("请增加行");
                return false;
            }
            var flag=false;
            var annualBudgetDetailId$=$("input[name='annualBudgetDetailId']");
            annualBudgetDetailId$.each(function () {
                if($.trim($(this).val())==""){
                    alert(" 活动费用科目不能为空");
                    flag=true;
                    return false;
                }
            });
            if(flag) {
                return false;
            }


            var actualAmount$=$("input[name='actualAmount']");

            actualAmount$.each(function (index) {
                if($.trim($(this).val())==""){
                    alert("实际金额不能为空");
                    flag=true;
                    return false;
                }
                if(parseInt($(this).val())<0){
                    alert("实际金额不能小于0");
                    flag=true;
                    return false;
                }
                if(isNaN($(this).val())){
                    alert("输入的不是数字");
                    flag=true;
                    return false;
                }
                if(parseInt($("#actualAmount_"+index).val())>parseInt($("#remainAmount_"+index).val())){
                    flag=true;
                    alert("实际金额不能大于剩余金额");
                    return false
                }
            });
            if(flag) {
                return false;
            }

            /*actualAmount$.each(function (index) {
                $("#usedAmount_" + index).val(parseInt($("#usedAmount_" + index).val()) + parseInt($("#actualAmount_" + index).val()));
                $("#remainAmount_" + index).val(parseInt($("#annualBudgetMoney_" + index).val()) - parseInt($("#usedAmount_" + index).val()));
            });*/


            $("#btn-save-common").attr("disabled", "disabled");
          form_save("rebateActivity","../party/ajax-rebateactivity!save.action?viewtype="+$('#viewtype').val(),null,
              function (data) {
                  if(data.state == "200" || data.state ==200){
                      if(draft == 1){
                          location.href="index.action";
                      }else{
                          loadURL("../party/ajax-rebateactivity.action?viewtype="+$('#viewtype').val(),$("#content"));
                      }
                  }
              }
          );
        }
  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(

          function(e) {
            if(!$("#rebateActivity").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            sum(e);

          }
  );
  var sum=function(e){
      if($("input[name='annualBudgetDetailId']").length<1){
          alert("请增加行");
          return false;
      }
      var flag=false;
      var annualBudgetDetailId$=$("input[name='annualBudgetDetailId']");
      annualBudgetDetailId$.each(function () {
          if($.trim($(this).val())==""){
              alert(" 活动费用科目不能为空");
              flag=true;
              return false;
          }
      });
      if(flag) {
          return false;
      }
      var actualAmount$=$("input[name='actualAmount']");
      actualAmount$.each(function (index) {
          if($.trim($(this).val())==""){
              alert("实际金额不能为空");
              flag=true;
              return false;
          }
          if(parseInt($(this).val())<0){
              alert("实际金额不能小于0");
              flag=true;
              return false;
          }
          if(isNaN($(this).val())){
              alert("输入的不是数字");
              flag=true;
              return false;
          }
          if(parseInt($("#actualAmount_"+index).val())>parseInt($("#remainAmount_"+index).val())){
              flag=true;
              alert("实际金额不能大于剩余金额");
              return false
          }
      });
      if(flag) {
          return false;
      }




      $.SmartMessageBox({
          title : "提示：",
          content : "确定提交申请吗？",
          buttons : '[取消][确认]'
      }, function(ButtonPressed) {
          if (ButtonPressed === "取消") {
              e.preventDefault();
              e.stopPropagation();
              return;
          }
          if (ButtonPressed === "确认") {
              var $validForm = $("#rebateActivity").valid();
              if(!$validForm) return false;
              /*actualAmount$.each(function (index) {
                  $("#usedAmount_" + index).val(parseInt($("#usedAmount_" + index).val()) + parseInt($("#actualAmount_" + index).val()));
                  $("#remainAmount_" + index).val(parseInt($("#annualBudgetMoney_" + index).val()) - parseInt($("#usedAmount_" + index).val()));
              });
*/
              $("#btn-confirm-common").attr("disabled", "disabled");
              $("#btn-recommit-common").attr("disabled", "disabled");
              form_save("rebateActivity","../party/ajax-rebateactivity!commit.action?viewtype="+$('#viewtype').val(),null,
                  function (data) {
                      if(data.state == "200" || data.state ==200){
                          if(draft == 1){
                              location.href="index.action";
                          }else{
                              loadURL("../party/ajax-rebateactivity.action?viewtype="+$('#viewtype').val(),$("#content"));
                          }
                      }
                  }
              );
          }
      });

  }
</script>
