<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-input-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>信访登记信息</a>
          <s:if test="letterRegister==null || letterRegister.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="letterRegister!=null && letterRegister.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="letterRegister" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="letterRegister.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="viewtype" id="viewtype" value="<s:property value="viewtype"/>" />

            <header  style="display: block;">
              信访登记基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-dict-lettertype">信访类别</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="letterTypeName" name="letterTypeName" placeholder="请选择信访类别"
                             value="<s:iterator id="list" value="letterType"><s:property value="#list.name"/></s:iterator>"/>
                      <input type="hidden" id="letterTypeId" name="letterTypeId"
                             value="<s:iterator id="list" value="letterType"><s:property value="#list.id"/></s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  信访开始日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="letterStartDate" id="letterStartDate" placeholder="请选择信访开始日期" value="<s:date name="letterRegister.letterStartDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  信访处理完毕日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="letterEndDate" id="letterEndDate" placeholder="请选择信访开始日期" value="<s:date name="letterRegister.letterEndDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  信访件处理结果
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" checked="checked" name="workSummary" value="0" <s:if test='letterRegister.workSummary==0'>checked</s:if>><i></i>前期工作
                    </label>
                    <label class="radio">
                      <input type="radio" name="workSummary"  value="1" <s:if test='letterRegister.workSummary==1'>checked</s:if>><i></i>受理登记
                    </label>
                    <label class="radio">
                      <input type="radio" name="workSummary"  value="2" <s:if test='letterRegister.workSummary==2'>checked</s:if>><i></i>分类办理
                    </label>
                    <label class="radio">
                      <input type="radio" name="workSummary"  value="3" <s:if test='letterRegister.workSummary==3'>checked</s:if> ><i></i>回复反馈
                    </label>
                  </div>

                </section>
              </div>
              <%--<div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  信访件处理结果
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="type">
                    <label class="radio">
                      <input type="radio" name="workSummary" value="0" <s:property value="letterRegister.workSummary==0?'checked':''"/> ><i></i>前期工作
                    </label>
                    <label class="radio">
                      <input type="radio" name="workSummary"  value="1" <s:property value="letterRegister.workSummary==1?'checked':''"/> ><i></i>受理登记
                    </label>
                    <label class="radio">
                      <input type="radio" name="workSummary"  value="2" <s:property value="letterRegister.workSummary==2?'checked':''"/> ><i></i>分类办理
                    </label>
                    <label class="radio">
                      <input type="radio" name="workSummary"  value="3" <s:property value="letterRegister.workSummary==3?'checked':''"/> ><i></i>回复反馈
                    </label>
                  </div>

                </section>
              </div>--%>

              <div class="row">
                <label class="label col col-2">
                  信访件处理评价
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="letterEvaluate" id="letterEvaluate" placeholder="请输入信访件处理评价" value="<s:property value="letterRegister.letterEvaluate"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="letterRegister.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="letterRegister.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="letterRegister.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>

    var draft = "<s:property value="draft" />";
  $(function(){
     //dialogDict();
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"letterregister",
        todo:"1"
    };
    multiDuty(pdata);
  });



  $("a[key=btn-choose-dict-lettertype]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择活动类型",
          url:"ajax-dialog!dict.action?dictName=LetterType",
          width:340
      }).show();
  });
  //返回视图
  $("#btn-apply-input-common").click(function(){
      if(draft ==1 ){
          location.href="index.action";
      }else {
          loadURL("../party/ajax-letterregister.action?viewtype="+$("#viewtype").val(),$('#content'));
      }
  });
  //校验
  $("#letterRegister").validate({
    rules : {
        letterTypeId : {
            required : true
      },
        letterStartDate:{
            required : true,
            date:true
      },
        letterEndDate:{
            required : true,
            compareDate:[],
            date:true
        },
        workSummary:{
            required : true
        },


    },
    messages : {
        letterTypeId : {
            required :  '请选择信访类别'
        },
        letterStartDate:{
            required : '请选择信访开始日期',
            date:'请输入正确日期格式'
        },
        letterEndDate:{
            required : '请选择信访处理完毕日期',
            compareDate:'信访处理完毕日期必须大于信访开始日期',
            date:'请输入正确日期格式'
        },
        workSummary:{
            required : '请选择信访件处理结果'
        },


    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });


  //自定义验证
  $.validator.addMethod("compareDate",function(value,element){
      var letterStartDate = $("#letterStartDate").val();
      var letterEndDate = $("#letterEndDate").val();
      var reg = new RegExp('-','g');
      letterStartDate = letterStartDate.replace(reg,'/');//正则替换
      letterEndDate = letterEndDate.replace(reg,'/');
      letterStartDate = new Date(parseInt(Date.parse(letterStartDate),10));
      letterEndDate = new Date(parseInt(Date.parse(letterEndDate),10));
      if(letterStartDate>letterEndDate){
          return false;
      }else{
          return true;
      }
  },"<font color='#E47068'>信访处理完毕日期必须大于信访开始日期</font>");

  $('#letterStartDate,#letterEndDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(

        function(){
            if(!$("#letterRegister").valid()){
                // $("#areaselect_span").hide();
                return false;
            }
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("letterRegister","../party/ajax-letterregister!save.action?viewtype="+$('#viewtype').val(),null,
              function (data) {
                  if(data.state == "200" || data.state ==200){
                      if(draft == 1){
                          location.href="index.action";
                      }else {
                          loadURL("../party/ajax-letterregister.action?viewtype="+$('#viewtype').val(),$("#content"));
                      }
                  }
              }
          );

        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(

          function(e) {
            if(!$("#letterRegister").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#letterRegister").valid();
                if(!$validForm) return false;
                fnSum();
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("letterRegister","../party/ajax-letterregister!commit.action?viewtype="+$('#viewtype').val(),null,
                    function (data) {
                        if(data.state == "200" || data.state ==200){
                            if(draft ==1 ){
                                location.href="index.action";
                            }else{
                                loadURL("../party/ajax-letterregister.action?viewtype="+$('#viewtype').val(),$("#content"));
                            }
                        }
                    }
                );
              }
            });
          }
  );
  var fnSum=function(){
      var budgetMoney$=$("input[name='budgetMoney']");
      var sum=0;
      budgetMoney$.each(function () {
          sum+=parseInt($(this).val());
      })
      $("#letterRegisterMoney").val(sum);

  }
</script>
