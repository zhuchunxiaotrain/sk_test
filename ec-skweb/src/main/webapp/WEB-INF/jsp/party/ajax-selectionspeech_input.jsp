<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>
<s:if test="draft == 1">
  <jsp:include page="../com/ajax-top.jsp" />
</s:if>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-input-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>报名信息</a>
          <s:if test="selectionSpeech==null || selectionSpeech.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="selectionSpeech!=null && selectionSpeech.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="selectionSpeech" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="selectionSpeech.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />
            parentId = <s:property value="parentId"/>
            <header  style="display: block;">
              报名基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text" name="applyUserName" id="applyUserName1" placeholder="自动输入" value="<s:property value="applyUserName"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                 相关资料
                </label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input type="text" name="attchmentText" id="attchmentText" placeholder="请输入相关资料" value="<s:property value="selectionSpeech.attchmentText"/>" >
                  </label>--%>
                  <label class="input">
                    <input name="uploadify" id="attchmentFileName" placeholder="" type="file">
                    <input name="attchmentFileId" id="attchmentFileId" style="display:none" value="<s:property value="attchmentFileId"/>" >
                  </label>
                </section>
              </div>
<%--
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-selectionnotice"> 干部公告</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="postName" name="postName"
                             value="<s:iterator id="list" value="checkUsersList"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden" id="selectionNoticeId" name="selectionNoticeId"
                             value="<s:iterator id="list" value="checkUsersList"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>
--%>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users"> 审核会签小组</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="usersName" name="checkUsersName"
                             value="<s:iterator id="list" value="checkUsersList"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden" id="usersId" name="checkUsersId"
                             value="<s:iterator id="list" value="checkUsersList"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="selectionSpeech.remark"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="selectionSpeech.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="selectionSpeech.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="selectionSpeech.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
    var draft = "<s:property value="draft" />";
  inputLoad({
      objId:"attchmentFileName",
      entityName:"attchmentId",
      sourceId:"attchmentFileId"
  });

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"selectionspeech",
        todo:"1"
    };
    multiDuty(pdata);
  });



  $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择人员",
          url:"ajax-dialog!user.action",
          width:340
      }).show();
  });
  $("a[key=btn-choose-selectionnotice]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择公告",
          url:"ajax-dialog!selectionnotice.action",
          width:700
      }).show();
  });

  //返回视图
  $("#btn-apply-input-common").click(function(){
      if(draft == 1){
          location.href="index.action";
      }else{
          loadURL("../party/ajax-selectionspeech.action?parentId="+$("#parentId").val(),$('div#s2'));
      }
  });
  //校验
  $("#selectionSpeech").validate({
    rules : {
        attchmentText : {
        required : true
      },
        usersId:{
        required : true
      },
        attchmentFileName:{
       required : true
      },
        usersId:{
            required : true
        },

    },
    messages : {
        attchmentText : {
            required :  '请输入相关资料'
        },
        usersId:{
            required : '审核会签小组'
        },
        attchmentFileName:{
            required : '请输入相关资料'
        },

    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  $('#selectDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(

        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("selectionSpeech","../party/ajax-selectionspeech!save.action?parentId="+$('#parentId').val(),null,
              function (data) {
                  if(data.state =='200' || data.state == 200){
                      if(draft == 1){
                          location.href="index.action";
                      }else{
                          loadURL("../party/ajax-selectionspeech.action?parentId="+$('#parentId').val(),$("div#s2"));
                      }
                  }
              }
          );
        }
  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#selectionSpeech").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#selectionSpeech").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("selectionSpeech","../party/ajax-selectionspeech!commit.action?parentId="+$('#parentId').val(),null,
                    function (data) {
                        if(data.state == '200' || data.state == 200){
                            if(draft == 1){
                                location.href="index.action";
                            }else{
                              loadURL("../party/ajax-selectionspeech.action?parentId="+$('#parentId').val(),$('div#s2'));
                            }
                        }
                    }
                );
              }
            });
          }
  );
</script>
