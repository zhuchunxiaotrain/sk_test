<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>--%>

<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>公告返回
          </a>

         <s:if test="selectionNotice!=null && selectionNotice.getProcessState().toString()=='Finished' && selectionNotice.selectionFinished==0">

          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" id="other0" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 公告 </a>
            </li>
            <%--<s:if test="selectionNotice!=null && selectionNotice.selectionFinished==0 && selectionNotice.getProcessState().name()=='Finished'">--%>
            <li >
              <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 报名 </a>
            </li>
            <li >
              <a href="#s3" id="other2" data-toggle="tab"><i class="fa fa-fw fa-lg fa-calendar"></i> 竞聘演讲 </a>
            </li>
            <li>
              <a href="#s4" id="other3" data-toggle="tab"><i class="fa fa-fw fa-lg fa-calendar"></i> 结果公示 </a>
            </li>
            <li>
              <a href="#s5" id="other4" data-toggle="tab"><i class="fa fa-fw fa-lg fa-calendar"></i> 聘书发放 </a>
            </li>
           <%-- </s:if>--%>
          </ul>

        </s:if>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
              <div class="row">
                <%--<a href="javascript:void(0);" key="btn-comment">
                      <span style="margin-top: -13px !important;" class="btn btn-default pull-right">
                          <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                      </span>
                </a>--%>
                <shiro:hasAnyRoles name="wechat">
                  <a style="margin-top: -13px !important;" <s:property value="isEdit(selectionNotice.id)"/> class="btn btn-default pull-right pull-right-fix" key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
                </shiro:hasAnyRoles>
              </div>
        <%--  <s:if test="selectionNotice!=null && selectionNotice.getProcessState().name()=='Finished' &&  selectionNotice.getCreater().getId()==applyUserId">
            <a class="btn btn-default" id="btn-apply" href="javascript:void(0);"><i class="fa fa-rocket"></i> 报名</a>
          </s:if>--%>
          <%--<a class="btn btn-default" id="btn-re-apply" href="javascript:void(0) ">
            <i class="fa fa-lg fa-mail-reply-all"></i>报名--%>
          <hr class="simple">
          <form id="selectionNotice" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="selectionNotice.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="viewtype" id="viewtype" value="<s:property value="viewtype" />"/>
            <input type="hidden" name="flag" id="flag" value="<s:property value="flag" />"/>
            <input type="hidden" name="todo" id="todo" value="<s:property value="todo" />"/>
            <input type="hidden" name="index" id="index" value="<s:property value="index" />"/>
            <input type="hidden" name="remind" id="remind" value="<s:property value="remind" />"/>
            <input type="hidden" name="record" id="record" value="<s:property value="record" />"/>
            <input type="hidden" name="draft" id="draft" value="<s:property value="draft" />"/>
            <header  style="display: block;">
              干部选拔公告&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-dict-postcategory"> 岗位</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="postCategoryName" name="postCategoryName"
                             value="<s:iterator id="list" value="post"><s:property value="#list.name"/></s:iterator>"/>
                      <input type="hidden" id="postCategoryId" name="postCategoryId"
                             value="<s:iterator id="list" value="post"><s:property value="#list.id"/></s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>

            <%--
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  岗位
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" disabled="disabled" name="postDictId" id="postDictId">
                      <s:iterator value="postArrayList" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> disabled="<s:property value="#list.disabled"/>"><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>
--%>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  要求
                </label>
                <section class="col col-5">
                  <label class="input ">
                    <input  type="text" name="requires" id="requires" placeholder="请输入要求" value="<s:property value="selectionNotice.requires"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  报名周期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="date" name="selectDate" id="selectDate" placeholder="请输入要求" value="<s:date name="selectionNotice.selectDate" format="yyyy-MM-dd"/>" >
                  </label>

                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  竞聘方案
                </label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input type="text" name="makePlanText" id="makePlanText" placeholder="请输入要求" value="<s:property value="selectionNotice.makePlanText"/>" >
                  </label>--%>
                  <label class="input">
                    <input name="uploadify" id="makePlanFileId" value="<s:iterator value="makePlan" id="list"><s:property value="#list.id"/>,</s:iterator>" style="display:none">
                    <%--<input name="makePlanFi0...leId" id="makePlanFileId" style="display:none" value="<s:property value="makePlanFileId"/>" >--%>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-users"> 审核小组</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="roleName" id="roleName" placeholder="请输入审核小组" value="<s:iterator id="list" value="role"><s:property value="#list.name"/></s:iterator>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="selectionNotice.remark"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="" value="<s:property value="selectionNotice.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="selectionNotice.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="" value="<s:date name="selectionNotice.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
              <div class="flow">
                <s:if test="selectionNotice.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer"style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
              </div>

            </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
            <div class="tab-pane fade in active" id="s3" style="margin: 10px;"></div>
            <div class="tab-pane fade in active" id="s4" style="margin: 10px;"></div>
            <div class="tab-pane fade in active" id="s5" style="margin: 10px;"></div>
         </div>
        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">
  //添加样式

  readLoad({
      objId:"makePlanFileId",
      entityName:"makePlanId",
      sourceId:"makePlanFileId"
  });
  //返回视图
  $("#btn-re-common").click(function(){
      var index = "<s:property value="index" />";
      var todo = "<s:property value="todo" />";
      var remind = "<s:property value="remind" />";
      var record = "<s:property value="record" />";
      var draft = "<s:property value="draft" />";
      if(index==1){
          loadURL("../com/ajax-index!page.action",$('#content'));//主页
      }else if(todo==1){
          loadURL("../com/ajax!toDoList.action",$('#content'));//个人代办
      }else if(remind==1){
          loadURL("../com/ajax!remindList.action",$('#content'));//提醒事项
      }else if(record==1){
          loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));//已办未结
      }else if(record==2){
          loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));//已办已结
      }else if(draft==1){
          loadURL("../com/ajax!draftList.action",$('#content'));//草稿
      }else {
          loadURL("../party/ajax-selectionnotice.action?viewtype=<s:property value="viewtype"/>" + "&flag=" + $("#flag").val() + "&todo=" + $("#selectionNotice #todo").val(), $('#content'));
      }
  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
      var draft = "<s:property value="draft" />";
      loadURL("../party/ajax-selectionnotice!input.action?keyId="+$("#selectionNotice input#keyId").val()+"&flag="+$("#flag").val()+"&draft="+draft+"&viewtype="+$("#viewtype").val(),$('#content'));

      /*if($("#moneyManager #draft").val()=="1"){
          loadURL("../party/ajax-selectionnotice!input.action?keyId="+$("#selectionNotice input#keyId").val()+"&flag="+$("#flag").val()+"&draft="+$("#selectionNotice #draft").val(),$('#content'));
      }else {
          loadURL("../party/ajax-selectionnotice!input.action?keyId="+$("#selectionNotice input#keyId").val()+"&flag="+$("#flag").val()+"&draft="+$("#selectionNotice #draft").val(),$('#content'));
      }
*/
  });

  $(function(){
      $("input").attr("disabled","disabled").parent().addClass(" state-disabled");
      loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
      loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
      ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
          var showDuty = false;
          var area = $("span.textarea-controls");
          $(pdata.data.datarows).each(function(i,v){
              var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
              $(area).append(str);
              if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                  showDuty = true;
              }
          });
          if(showDuty == true){
              var pdata = {
                  keyId: $("input#keyId").val(),
                  flowName: "selectionnotice"
              };
              multiDuty(pdata);
          }
      });
      var valueObj=$("input#numStatus").val();
      switch(valueObj){
          case "0":
              //保存后再提交
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("selectionNotice","../party/ajax-selectionnotice!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-re-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
          case "1":
              //第一步审批
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("selectionNotice","../party/ajax-selectionnotice!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-re-common").trigger("click");
                  });

              });
              break;

          case "2":
              //第二步审批
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("selectionNotice","../party/ajax-selectionnotice!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-re-common").trigger("click");
                  })
              });
              break;
          case "3":
              //第三步审批
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("selectionNotice","../party/ajax-selectionnotice!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-re-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
      }
      //退回
      $("#left_foot_btn_sendback").off("click").on("click",function(){
          var vActionUrl="../party/ajax-selectionnotice!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
          var data={keyId:$("#selectionNotice input#keyId").val()};

          ajax_action(vActionUrl,data,{},function(pdata){
              _show(pdata);
              $("#btn-re-common").trigger("click");
          });
      });
      //否决
      $("#left_foot_btn_deny").off("click").on("click",function(){
          var vActionUrl="../manage/ajax-selectionnotice!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
          var data={keyId:$("input#keyId").val()};
          ajax_action(vActionUrl,data,{},function(pdata){
              _show(pdata);
              $("#btn-re-common").trigger("click");
          });
      });
      //流程信息展开
      $('#flow,#next').click(function(){
          if($(this).hasClass("right")){
              $(this).removeClass("right").addClass("down");
              $(this).parent(".f_title").next("div.f_content").show();
          }else{
              $(this).removeClass("down").addClass("right");
              $(this).parent(".f_title").next("div.f_content").hide();
          }
      });
  });

  var table_global_width=0;
  $("a#other1").off("click").on("click",function(e){/!*点击报名*!/
      if (table_global_width == 0) {
          table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
      }
      loadURL("../party/ajax-selectionapply.action?parentId="+$("#selectionNotice input#keyId").val(),$('div#s2'));
  });
   $("a#other2").off("click").on("click",function(e){/!*点击竞聘演讲*!/
        if (table_global_width == 0) {
            table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s3").width();
        }
        loadURL("../party/ajax-selectionspeech.action?parentId="+$("#selectionNotice input#keyId").val(),$('div#s3'));
    });
  $("a#other3").off("click").on("click",function(e){/!*点击结果公示*!/
      if (table_global_width == 0) {
          table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s4").width();
      }
      loadURL("../party/ajax-selectionpublicity.action?parentId="+$("#selectionNotice input#keyId").val(),$('div#s4'));
  });
  $("a#other4").off("click").on("click",function(e){/!*点击聘书发放*!/
      if (table_global_width == 0) {
          table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s4").width();
      }
      loadURL("../party/ajax-selectionissued.action?parentId="+$("#selectionNotice input#keyId").val(),$('div#s5'));
  });

</script>