<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-read-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>返回
          </a>

                <%--<a href="javascript:void(0);" key="btn-annualbudget-comment">
                      <span style="margin-top: -13px !important;" class="btn btn-default pull-right">
                          <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                      </span>
                </a>--%>

            <shiro:hasAnyRoles name="wechat">
                <a style="margin-top: -13px !important;" <s:property value="isEdit(annualBudget.id)"/> class="btn btn-default pull-right pull-right-fix" key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>

          <hr class="simple">
          <form id="annualBudget" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="annualBudget.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="viewtype" id="viewtype" value="<s:property value="viewtype" />"/>
            <input type="hidden" name="todo" id="todo" value="<s:property value="todo" />"/>
            <input type="hidden" name="index" id="index" value="<s:property value="index" />"/>
            <input type="hidden" name="remind" id="remind" value="<s:property value="remind" />"/>
            <input type="hidden" name="record" id="record" value="<s:property value="record" />"/>
            <input type="hidden" name="draft" id="draft" value="<s:property value="draft" />"/>
            <header  style="display: block;">
              年度预算基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-department">所属单位</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="annualBudgetDepartName" name="departName"
                             value="<s:iterator id="list" value="department"><s:property value="#list.name"/></s:iterator>"/>
                      <input type="hidden" id="annualBudgetDepartId" name="departId"
                             value="<s:iterator id="list" value="department"><s:property value="#list.id"/></s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>


              <div class="row">
                <label class="label col col-2">
                </label>
                <section class="col col-2">
                  <label class="input">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    年度预算科目
                  </label>
                </section>
                <section class="col col-2">
                  <label class="input">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    预算金额
                  </label>
                </section>
                <section class="col col-1">
                  <label class="input">
                    备注
                  </label>
                </section>
                <%--<section class="col col-1">
                  <label class="input">
                    <a class="btn btn-default pull-right pull-right-fix" id="btn-add-row" href="javascript:void(0);"><i class="fa fa-rocket"></i> 增加行</a>
                  </label>
                </section>--%>
              </div>
              <div id="add-annoualbudget-row">
                <s:iterator id="list" value="annualBudgetDetailList">
                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    <a disabled="" href="javascript:void(0);" class="dict-dialog" id="btn_choose_dict_0" onclick="selectdict(0)">年度预算科目</a>
                  </label>
                  <section class="col col-2">
                    <label class="input">
                      <label class="input state-disabled">
                        <input disabled type="text" id="annualBudgetItemName_0" name="annualBudgetItemName" placeholder="请输入年度预算科目"
                               value="<s:property value="#list.annualBudgetItem.name"/>"/>
                        <input type="hidden" id="annualBudgetItemId_0"  name="annualBudgetItemId"
                               value="<s:property value="#list.annualBudgetItem.id"/>"/>
                      </label>
                    </label>
                  </section>
                  <section class="col col-2">
                    <label class="input">
                      <label class="input">
                        <input  type="text" id="budgetMoney" name="budgetMoney" placeholder="请输入预算金额"
                                value="<s:property value="#list.budgetMoney"/>"/>
                      </label>
                    </label>
                  </section>
                  <section class="col col-1">
                    <label class="input">
                      <label class="input">
                        <input  type="text" id="remark" name="remark" placeholder="请输入备注"
                                value="<s:property value="#list.remark"/>"/>
                      </label>
                    </label>
                  </section>
                  <%--<section class="col col-1">
                    <label class="input">
                      <a class="btn btn-default pull-right pull-right-fix btn-del-row"  href="javascript:void(0);"><i class="fa fa-rocket"></i>删除行</a>
                    </label>
                  </section>--%>
                </div>
                </s:iterator>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users">年度预算金额</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="annualBudgetMoney" name="annualBudgetMoney" placeholder="自动计算"
                             value="<s:property value="annualBudget.annualBudgetMoney"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="" value="<s:property value="annualBudget.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="annualBudget.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="" value="<s:date name="annualBudget.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
          <div class="flow">
            <s:if test="annualBudget.getProcessState().name()=='Running'">
              <div class="f_title">审批意见</div>
              <div class="chat-footer"style="margin-top:5px">
                <div class="textarea-div">
                  <div class="typearea">
                    <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                  </div>
                </div>
                <span class="textarea-controls"></span>
              </div>
            </s:if>
            <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow"></div>
            </div>
            <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">

  readLoad({
      objId:"attchmentFileId",
      entityName:"attchmentId",
      sourceId:"attchmentFileId"
  });

  $(function(){
     $("input").attr("disabled","disabled").parent().addClass(" state-disabled");

      loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
      loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
      ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
          var showDuty = false;
          var area = $("span.textarea-controls");
          $(pdata.data.datarows).each(function(i,v){
              var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
              $(area).append(str);
              if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                  showDuty = true;
              }
          });
          if(showDuty == true){
              var pdata = {
                  keyId: $("input#keyId").val(),
                  flowName: "annualbudget"
              };
              multiDuty(pdata);
          }
      });
      var valueObj=$("#annualBudget  input#numStatus").val();
      switch(valueObj){
          case "0":
              //保存后再提交
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("annualBudget","../party/ajax-annualbudget!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-apply-read-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
          case "1":
              //第一步审批
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("annualBudget","../party/ajax-annualbudget!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-apply-read-common").trigger("click");
                  });

              });
              break;
          case "2":
              //第二步审批
              $("#left_foot_btn_approve").off("click").on("click",function(){

                  form_save("annualBudget","../party/ajax-annualbudget!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $("#btn-apply-read-common").trigger("click");
                  })
              });
              break;
              /*case "3":
               //第三步审批
               $("#left_foot_btn_approve").off("click").on("click",function(){
               form_save("annualBudget","../party/ajax-annualbudget!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
               $(this).leftview('leftClose');
               $("#btn-apply-read-common").trigger("click");
               })
               });
               // approveRequest("approve1");
               break;*/
      }
      //退回
      $("#left_foot_btn_sendback").off("click").on("click",function(){
          var vActionUrl="../party/ajax-annualbudget!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
          var data={keyId:$("#annualBudget input#keyId").val()};

          ajax_action(vActionUrl,data,{},function(pdata){
              _show(pdata);
              $("#btn-apply-read-common").trigger("click");
          });
      });
      //否决
      $("#left_foot_btn_deny").off("click").on("click",function(){

          var vActionUrl="../party/ajax-annualbudget!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
          var data={keyId:$("#annualBudget input#keyId").val()};
          ajax_action(vActionUrl,data,{},function(pdata){
              _show(pdata);
              $("#btn-apply-read-common").trigger("click");
          });
      });
      //流程信息展开
      $('#flow,#next').click(function(){
          if($(this).hasClass("right")){
              $(this).removeClass("right").addClass("down");
              $(this).parent(".f_title").next("div.f_content").show();
          }else{
              $(this).removeClass("down").addClass("right");
              $(this).parent(".f_title").next("div.f_content").hide();
          }
      });
  });

  //返回视图
  $("#btn-apply-read-common").click(function(){
      var index = "<s:property value="index" />";
      var todo = "<s:property value="todo" />";
      var remind = "<s:property value="remind" />";
      var record = "<s:property value="record" />";
      var draft = "<s:property value="draft" />";
      if(index==1){
          loadURL("../com/ajax-index!page.action",$('#content'));//主页
      }else if(todo==1){
          loadURL("../com/ajax!toDoList.action",$('#content'));//个人代办
      }else if(remind==1){
          loadURL("../com/ajax!remindList.action",$('#content'));//提醒事项
      }else if(record==1){
          loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));//已办未结
      }else if(record==2){
          loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));//已办已结
      }else if(draft==1){
          loadURL("../com/ajax!draftList.action",$('#content'));//草稿
      }else{
          loadURL("../party/ajax-annualbudget.action?viewtype="+$("#viewtype").val(),$('#content'));
      }


      /*if($("#annualBudget #todo").val()=="1"){
          loadURL("../party/ajax-annualbudget.action?viewtype=1",$('#content'));
      }else{
          loadURL("../party/ajax-annualbudget.action?viewtype="+$("#viewtype").val(),$('#content'));
      }*/

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
      var draft = "<s:property value="draft" />";
      loadURL("../party/ajax-annualbudget!input.action?keyId="+$("#annualBudget input#keyId").val()+"&draft="+draft+"&viewtype="+$("#annualBudget input#viewtype").val(),$('#content'));
  });

  //左侧意见栏
/*
  $("a[key=btn-annualbudget-comment]").unbind("click").bind("click",function(){
    var keyId = $("#annualBudget input#keyId").val();
    var actionUrl = "ajax-running!comment.action?bussinessId="+keyId;/!*bussinessId是文档的ID*!/
    $(this).leftview('init',{
      title:"查看审批意见",
      actionUrl:actionUrl
    });
    var keyId = $("#annualBudget input#keyId").val();
    $(this).leftview('foot',{'unid':keyId,'flowName':"annualbudget",callback:function(){
      var valueObj=$("#annualBudget  input#numStatus").val();
      switch(valueObj){
          case "0":
              //保存后再提交
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("annualBudget","../party/ajax-annualbudget!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $(this).leftview('leftClose');
                      $("#btn-apply-read-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
        case "1":
          //第一步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("annualBudget","../party/ajax-annualbudget!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-apply-read-common").trigger("click");
            });

          });
          break;
        case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){

          form_save("annualBudget","../party/ajax-annualbudget!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
            $(this).leftview('leftClose');
            $("#btn-apply-read-common").trigger("click");
          })
        });
        break;
        /!*case "3":
          //第三步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("annualBudget","../party/ajax-annualbudget!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-apply-read-common").trigger("click");
            })
          });
        // approveRequest("approve1");
        break;*!/
      }
      //退回
      $("#left_foot_btn_sendback").off("click").on("click",function(){
        var vActionUrl="../party/ajax-annualbudget!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("#annualBudget input#keyId").val()};

        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-apply-read-common").trigger("click");
        });
      });
      //否决
      $("#left_foot_btn_deny").off("click").on("click",function(){

        var vActionUrl="../party/ajax-annualbudget!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("#annualBudget input#keyId").val()};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-apply-read-common").trigger("click");
        });
      });
    }});
  })
*/

</script>