<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<s:if test="draft == 1">
  <jsp:include page="../com/ajax-top.jsp" />
</s:if>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-input-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="selectionPublicity==null || selectionPublicity.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="selectionPublicity!=null && selectionPublicity.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="selectionPublicity" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="selectionPublicity.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />
            <input type="hidden" name="todo" id="todo" value="<s:property value="todo"/>" />
           <%-- parentId=<s:property value="parentId"/>--%>
            <header  style="display: block;">
              结果公示基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-selectionspeech"> 选拔人员</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="selectionSpeechApplyName" name="selectionSpeechApplyName" placeholder="请选择选拔人员"
                             value="<s:property value="selectionSpeech.creater.name"/>"/>
                      <input type="hidden" id="selectionSpeechId" name="selectionSpeechId"
                             value="<s:property value="selectionSpeech.id"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                 结果公示
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="publicityAttchmentFileName" placeholder="" type="file">
                    <input name="publicityAttchmentFileId" id="publicityAttchmentFileId" style="display:none" value="<s:iterator id="list" value="attchment"><s:property value="#list.id"/>,</s:iterator>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  党总支人事决策会议
                </label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input type="text" name="meetingDecisionAttchmentText" id="meetingDecisionAttchmentText" placeholder="请输入党总支人事决策会议" value="<s:property value="selectionPublicity.meetingDecisionAttchmentText"/>" >
                  </label>--%>
                  <label class="input">
                    <input name="uploadify" id="meetingDecisionAttchmentFileName" placeholder="" type="file">
                    <input name="meetingDecisionAttchmentFileId" id="meetingDecisionAttchmentFileId" style="display:none" value="<s:iterator id="list" value="meetingDecisionAttchment"><s:property value="#list.id"/>,</s:iterator>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  公示开始日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="publicStartDate" id="publicStartDate" placeholder="请选择公示开始日期" value="<s:date name="selectionPublicity.publicStartDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  公示结束日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="publicEndDate" id="publicEndDate" placeholder="请选择公示结束日期" value="<s:date name="selectionPublicity.publicEndDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  公示结果
                </label>
                <section class="col col-5">
                  <div class="inline-group" id="result">
                    <label class="radio">
                      <input type="radio" name="result" value="0" <s:property value="selectionPublicity.result==0?'checked':''"/> ><i></i>通过
                    </label>
                    <label class="radio">
                      <input type="radio" name="result" value="1" <s:property value="selectionPublicity.result==1?'checked':''"/> ><i></i>不通过
                    </label>
                    <%--<input type="radio" name="workSummary"  value="3" <s:property value="letterRegister.workSummary==3?'checked':''"/> ><i></i>回复反馈--%>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserName" id="applyUserName" placeholder="系统自动" value="<s:property value="selectionPublicity.creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-1">
                  操作日期
                </label>
                <section class="col col-2 ">
                  <label class="input state-disabled">
                    <input style="border: hidden" disabled type="text" name="applyUserDate" id=applyUserDate" placeholder="系统自动" value="<s:date name="selectionPublicity.createDate" format="yyyy-MM-dd"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
    var draft = "<s:property value="draft" />";
  inputLoad({
      objId:"publicityAttchmentFileName",
      entityName:"publicityAttchmentId",
      sourceId:"publicityAttchmentFileId",
      jsessionid:"<%=jsessionid%>"
  });
  inputLoad({
      objId:"meetingDecisionAttchmentFileName",
      entityName:"meetingDecisionAttchmentId",
      sourceId:"meetingDecisionAttchmentFileId",
      jsessionid:"<%=jsessionid%>"
  });

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"selectionpublicity",
        todo:"1"
    };
    multiDuty(pdata);
  });


  /*var showName=function  showName() {

  }*/

  $("a[key=btn-choose-selectionspeech]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择竞聘演讲通过人员",
          url:"ajax-dialog!selectionspeech.action?param=selectionPublicity&parentId=<s:property value="parentId"/>",
          width:600
      }).show();
  });
  $("a[key=btn-choose-selectionnotice]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择公告",
          url:"ajax-dialog!selectionnotice.action",
          width:700
      }).show();
  });

  //返回视图
  $("#btn-apply-input-common").click(function(){
      if(draft == 1){
          location.href="index.action";
      }else{
          var parentId="<s:property value="selectionNotice.id"/>"
          loadURL("../party/ajax-selectionpublicity.action?parentId="+$("#parentId").val(),$('div#s4'));
      }
      /*if($('div#s4').length>=1){
          loadURL("../party/ajax-selectionpublicity.action?parentId="+$("#parentId").val(),$('div#s4'));
      }else{
          if($("#selectionPublicity #todo").val()=="1"){
              var parentId="<s:property value="selectionNotice.id"/>"
              loadURL("../party/ajax-selectionnotice.action?parentId="+parentId+"&todo="+$("#selectionPublicity #todo").val(),$('#content'));
          }

      }*/

  });
  //检验是否有多个通过的人员
  var checkResult=function(e) {
      var actionUrl="<%=path%>/party/ajax-selectionpublicity!check.action";
     // alert("keyId:"+$("#parentId").val());
      var data={parentId:$("#parentId").val()};
      ajax_action(actionUrl,data,null,function(pdata){
          if(pdata.num>=1 && parseInt($("input[name='result']:checked").val())==0){
              alert("一个岗位只能一个人通过");
          }else {
              $.SmartMessageBox({
                  title : "提示：",
                  content : "确定提交申请吗？",
                  buttons : '[取消][确认]'
              }, function(ButtonPressed) {
                  if (ButtonPressed === "取消") {
                      e.preventDefault();
                      e.stopPropagation();
                      return;
                  }
                  if (ButtonPressed === "确认") {
                      var $validForm = $("#selectionPublicity").valid();
                      if(!$validForm) return false;
                      $("#btn-confirm-common").attr("disabled", "disabled");
                      $("#btn-recommit-common").attr("disabled", "disabled");
                      form_save("selectionPublicity","../party/ajax-selectionpublicity!commit.action?parentId="+$('#parentId').val(),null,
                          function (data) {
                              if(data.state == '200' || data.state == 200){
                                  if(draft == 1){
                                      location.href="index.action";
                                  }else{
                                      loadURL("../party/ajax-selectionpublicity.action?parentId="+$('#parentId').val(),$('div#s4'));
                                  }
                                  /*if($('div#s4').length>=1){
                                      loadURL("../party/ajax-selectionpublicity.action?parentId="+$('#parentId').val(),$('div#s4'));
                                  }else {
                                      if($("#selectionPublicity #todo").val()=="1"){
                                          var parentId="<s:property value="selectionNotice.id"/>"
                                          loadURL("../party/ajax-selectionnotice.action?parentId="+parentId+"&todo="+$("#selectionPublicity #todo").val(),$('#content'));
                                      }
                                  }*/
                              }
                          }
                      );
                  }
              });
          }
      });
  }
  //校验
  $("#selectionPublicity").validate({
    rules : {
        selectionSpeechId : {
        required : true
      },
        publicityAttchmentId:{
        required : true
      },
      meetingDecisionAttchmentId:{
            required : true
        },
      publicStartDate:{
       required : true,
          date:true
      },
     publicEndDate:{
          required : true,
          min:$("#publicStartDate").val(),
         date:true
      },
      result:{
          required : true
      },

    },
    messages : {
        selectionSpeechId : {
            required :  '请选择选拔人员'
        },
        publicityAttchmentId:{
            required : '请上传结果公示'
        },
        meetingDecisionAttchmentId:{
            required : '请上传党总支人事决策会议'
        },
        publicStartDate:{
            required : '请选择公示开始日期',
            date:'请输入正确日期格式'
        },
        publicEndDate:{
            required : '请选择公示结束日期',
            min:'公示结束日期大于公示开始日期',
            date:'请输入正确日期格式'
        },
        result:{
            required : '请选择公示结果'
        },

    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  $('#publicStartDate,#publicEndDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  //自定义验证
  $.validator.addMethod("compareDate",function(value,element){
      var letterStartDate = $("#letterStartDate").val();
      var letterEndDate = $("#letterEndDate").val();
      var reg = new RegExp('-','g');
      letterStartDate = letterStartDate.replace(reg,'/');//正则替换
      letterEndDate = letterEndDate.replace(reg,'/');
      letterStartDate = new Date(parseInt(Date.parse(letterStartDate),10));
      letterEndDate = new Date(parseInt(Date.parse(letterEndDate),10));
      if(letterStartDate>letterEndDate){
          return false;
      }else{
          return true;
      }
  },"<font color='#E47068'>公示结束日期大于公示开始日期</font>");

  //保存
  $("#btn-save-common").click(

        function(){
            if(!$("#selectionPublicity").valid()){
                $("#areaselect_span").hide();
                return false;
            }
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("selectionPublicity","../party/ajax-selectionpublicity!save.action?parentId="+$('#parentId').val(),null,
              function (data) {
                  if(data.state == '200' || data.state ==200){
                      if(draft == 1){
                          location.href="index.action";
                      }else{
                          loadURL("../party/ajax-selectionpublicity.action?parentId="+$('#parentId').val(),$("div#s4"));
                      }
                      /*if($('div#s4').length>=1){
                          loadURL("../party/ajax-selectionpublicity.action?parentId="+$('#parentId').val(),$("div#s4"));
                      }else{
                          if($("#selectionPublicity #todo").val()=="1"){
                              var parentId="<s:property value="selectionNotice.id"/>"
                              loadURL("../party/ajax-selectionnotice.action?parentId="+parentId+"&todo="+$("#selectionPublicity #todo").val(),$("#content"));
                          }

                      }*/
                  }
              }
          );

        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#selectionPublicity").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            var startDate=$("#publicStartDate").val();
            var endDate=$("#publicEndDate").val();
            var nowDate=new Date();
            var dbStart=new Date(startDate.replace(/-/g, "/"));
            var dbEnd=new Date(endDate.replace(/-/g, "/"));
            if(dbStart<nowDate){
                alert("公示开始日期大于今天日期");
                return false;
            }
            if(dbEnd<dbStart){
                alert("公示结束日期大于公示开始日期");
                return false;
            }
            checkResult(e);
          }
  );
</script>
