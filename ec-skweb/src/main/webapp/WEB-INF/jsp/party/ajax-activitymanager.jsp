<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>

<div class="row">
    <%--<jsp:include page="selectCommon.jsp"></jsp:include>--%>
      <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
          <i class="fa fa-table fa-fw "></i>
         活动管理
        </h1>
      </div>
</div>

<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <s:if test="viewtype == 1">
            <shiro:hasAnyRoles name="wechat">

                <a id="ajax_activitymanager_btn_add" <s:property value="isCreate('activitymanager')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i>新建活动申请表</a>
            </shiro:hasAnyRoles>
          <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />
          </s:if>
        </form>
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_activitymanager_list_row">
                    <table id="ajax_activitymanager_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_activitymanager_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">

  $(function(){
     /*/!*$("#h1 >a").each(function (index) {

         /!* if(index==backIndex){
              $(this).attr("style","color: red");
              return;
          }*!/
      }*!/
     $("#h1 > a")*!/*/
      load_activitymanager_jqGrid();
  });
  function load_activitymanager_jqGrid(){
    jQuery("#ajax_activitymanager_table").jqGrid({
      url:'../party/ajax-activitymanager!list.action?viewtype='+"<s:property value="viewtype"/>",
      datatype: "json",
      colNames:['年份','月','活动类型','活动支部','活动费用','文档状态','操作','id'],
      colModel:[
        {name:'year',index:'year', width:100,search:true,sortable:true},
        {name:'month',index:'month', width:100,search:true,sortable:true},
        {name:'activityType',index:'activityType_name', width:100,search:true,sortable:false},
        {name:'department',index:'department_name', width:100,search:true,sortable:true},
        {name:'activityMoney',index:'activityMoney', width:100,search:true,sortable:true},
        {name:'state',index:'state', search:false,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_activitymanager_list_page',
      sortname : 'createDate',
      sortorder : 'desc',
      gridComplete:function(){
        var ids=$("#ajax_activitymanager_table").jqGrid('getDataIDs');

        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_activitymanager_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_activitymanager_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_activitymanager_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 活动管理",
      multiselect : false,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_activitymanager_table").jqGrid('setGridWidth', $("#ajax_activitymanager_list_row").width());
    })
    jQuery("#ajax_activitymanager_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_activitymanager_table").jqGrid('navGrid', "#ajax_activitymanager_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  /*$("h1 > a").click(function () {
      $("#h1 >a").removeAttr("style");
      var i=$(this).index();
      $("#h1 >a").each(function (backIndex) {
          if(backIndex==0 && backIndex==i){

              loadURL("../party/ajax-selectionnotice.action?viewtype=<s:property value="viewtype"/>"+"&backIndex="+backIndex,$('#content'));
              return;
          }else if(backIndex==1 && backIndex==i){
              loadURL("../party/ajax-activitymanager.action?viewtype=<s:property value="viewtype"/>"+"&backIndex="+backIndex,$('#content'));
              return;
          }
      })
  })*/




  function fn_activitymanager_read(id){
    loadURL("../party/ajax-activitymanager!read.action?keyId="+id+"&viewtype="+"<s:property value="viewtype" />",$('#content'));
  }

  $("#ajax_activitymanager_btn_add").off("click").on("click",function(){
    loadURL("../party/ajax-activitymanager!input.action?viewtype=<s:property value="viewtype"/>",$('#content'));
  })
</script>





















