<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26 
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common2" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="dispatchrevise==null || dispatchrevise.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="dispatchrevise!=null && dispatchrevise.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="dispatchrevise" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="dispatchrevise.dispatch.id" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="dispatchrevise.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              附件修订记录表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  修订原因
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="reason" id="reason" placeholder="请输入修订原因" value="<s:property value="dispatchrevise.reason"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否具有有效期
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="hasValid" value="1" <s:property value="dispatchrevise.hasValid==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio"  name="hasValid" value="0" <s:property value="dispatchrevise.hasValid==0?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>
              <div class="row" <s:if test="dispatchrevise==null || dispatchrevise.hasValid!=1">style="display:none"</s:if> id="showValid">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  有效期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择有效期" id="validDate" name="validDate"
                            type="text" value="<s:date name="dispatchrevise.validDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  发文对象
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="object" value="1" <s:property value='dispatchrevise == null || dispatchrevise.object == null || dispatchrevise.object=="1"?"checked":""'/>>
                      <i></i>全体员工</label>
                    <label class="radio">
                      <input type="radio"  name="object" value="2" <s:property value='dispatchrevise.object=="2"?"checked":""'/>>
                      <i></i>部门</label>
                    <label class="radio">
                      <input type="radio"  name="object" value="3" <s:property value='dispatchrevise.object=="3"?"checked":""'/>>
                      <i></i>个人</label>
                  </div>
                </section>
              </div>


              <div class="row" <s:if test='dispatchrevise == null || dispatchrevise.object!="2"'>style="display:none"</s:if> id="showDept">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-noticeDepartment">通知部门</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="noticeDepartment2" name="noticeDepartment"
                           value="<s:iterator id="list" value="dispatchrevise.noticeDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="noticeDepartment2Id" name="noticeDepartmentId"
                           value="<s:iterator id="list" value="dispatchrevise.noticeDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
                <div class="row" <s:if test='dispatchrevise == null || dispatchrevise.object!="3"'>style="display:none"</s:if> id="showPerson">
                    <label class="label col col-2">
                        <i class="fa fa-asterisk txt-color-red"></i>
                        <a href="javascript:void(0);" key="btn-choose-noticeUsers">通知个人</a>
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text" id="noticeUsers2" name="noticeUsers"
                               value="<s:iterator id="list" value="dispatchrevise.noticeUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                        <input type="hidden" id="noticeUsers2Id" name="noticeUsersId"
                               value="<s:iterator id="list" value="dispatchrevise.noticeUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  原附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="oldfilename" placeholder="" type="file" style="display: none">
                    <input name="oldfileId" id="oldfileId" style="display: none" value="<s:property value="oldfileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  修订附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filerevisename" placeholder="" type="file" >
                    <input name="filereviseId" id="filereviseId" style="display: none" value="<s:property value="filereviseId"/>">
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#dispatchrevise #keyId").val(),
      flowName:"dispatchrevise",
      todo:"1"
    };
    multiDuty(pdata);

  });
  //上传文件
  readLoad({
    objId:"oldfilename",
    entityName:"oldfileIds",
    sourceId:"oldfileId"
  });
  //上传
  inputLoad({
    objId:"filerevisename",
    entityName:"filereviseIds",
    sourceId:"filereviseId",
    jsessionid:"<%=jsessionid%>"
  });

  //通知个人
  $("a[key=btn-choose-noticeUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知个人",
      url:"ajax-dialog!levelUser.action?key=noticeUsers2&more=1",
      width:560
    }).show();
  });
  //通知部门
  $("a[key=btn-choose-noticeDepartment]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知部门",
      url:"ajax-dialog!dept.action?key=noticeDepartment2&more=1",
      width:560
    }).show();
  });
  $(':radio[name="object"]').click(function(){
    if($(this).val()=="1") {
      $('#dispatchrevise #showPerson').hide();
      $('#dispatchrevise #showDept').hide();
    }else if($(this).val()=="2"){
        $('#dispatchrevise #showPerson').hide();
        $('#dispatchrevise #showDept').show();
    }else if($(this).val()=="3"){
       $('#dispatchrevise #showPerson').show();
       $('#dispatchrevise #showDept').hide();
    }
  });

  $(':radio[name="hasValid"]').click(function(){
    if($(this).val()=="1") {
      $('#dispatchrevise #showValid').show();
    }else{
      $('#dispatchrevise #showValid').hide();
    }
  });
  //返回视图
  $("#btn-re-common2").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!dispatchrevise.action",$('div#s2'));
    }

  });
  //校验
  $("#dispatchrevise").validate({
    rules : {
      reason : {
        required : true
      },
      hasValid: {
        required : true
      },
      validDate:{
        required : function(){
          if($('#dispatchrevise :checked[name="hasValid"]').val() == "1"){
            return true;
          }else{
            return false;
          }
        }
      },
      object:{
        required : true
      },
      noticeDepartmentId:{
        required:function(){
          if($('#dispatchrevise :checked[name="object"]').val() == "2"){
            return true;
          }else{
            return false;
          }
        }
      },
      noticeUsersId:{
        required:function(){
          if($(':checked[name="object"]').val() == "3"){
            return true;
          }else{
            return false;
          }
        }
      },
      filereviseIds:{
        required:true
      }
    },
    messages : {
      reason : {
        required : "请填写修订原因"
      },
      hasValid : {
        required : "请选择是否具有有效期"
      },
      validDate: {
        required : "请选择有效期"
      },
      object : {
        required : "请选择发文对象"
      },
      noticeDepartmentId:{
        required: "请选择通知部门"
      },
      noticeUsersId:{
        required: "请选择通知个人"
      },
      filereviseIds:{
        required:"请选择修订附件"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#dispatchrevise #validDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("dispatchrevise","../manage/ajax-dispatchrevise!save.action");
          if(draft == 1){
            location.href='index.action';
          }else{
            loadURL("../manage/ajax!dispatchrevise.action",$('div#s2'));
          }
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(

          function(e) {

            if(!$("#dispatchrevise").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#dispatchrevise").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("dispatchrevise","../manage/ajax-dispatchrevise!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!dispatchrevise.action",$('div#s2'));
                }
              }
            });
          }
  );


</script>
