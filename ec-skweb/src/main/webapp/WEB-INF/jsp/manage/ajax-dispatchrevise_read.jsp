<%-- 
  Created by IntelliJ IDEA.  
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common2" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <shiro:hasAnyRoles name="wechat">
            <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(dispatchrevise.id)"/> key="ajax_edit2" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
          </shiro:hasAnyRoles>

          <hr class="simple">
          <form id="dispatchrevise" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="dispatchrevise.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="dispatchrevise.dispatch.id" />"/>
            <header  style="display: block;">
              发文申请表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  修订原因
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  type="text" name="reason" id="reason" disabled placeholder="请输入修订原因" value="<s:property value="dispatchrevise.reason"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否具有有效期
                </label>
                <section class="col col-5">
                  <div class="inline-group state-disabled">
                    <label class="radio">
                      <input type="radio" disabled  name="hasValid" value="1" <s:property value="dispatchrevise.hasValid==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio" disabled  name="hasValid" value="0" <s:property value="dispatchrevise.hasValid==0?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>

              <div class="row" <s:if test="dispatchrevise==null || dispatchrevise.hasValid!=1">style="display:none"</s:if> id="showValid">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  有效期
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  disabled  id="validDate" name="validDate"
                            type="text" value="<s:date name="dispatchrevise.validDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  发文对象
                </label>
                <section class="col col-5">
                  <div class="inline-group state-disabled">
                    <label class="radio">
                      <input type="radio" disabled name="object" value="1" <s:property value='dispatchrevise.object=="1"?"checked":""'/>>
                      <i></i>全体员工</label>
                    <label class="radio">
                      <input type="radio" disabled name="object" value="2" <s:property value='dispatchrevise.object=="2"?"checked":""'/>>
                      <i></i>部门</label>
                    <label class="radio">
                      <input type="radio" disabled name="object" value="3" <s:property value='dispatchrevise.object=="3"?"checked":""'/>>
                      <i></i>个人</label>
                  </div>
                </section>
              </div>



              <div class="row" <s:if test='dispatchrevise == null || dispatchrevise.object!="2"'>style="display:none"</s:if>  id="showDept">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  通知部门
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="noticeDepartment" name="noticeDepartment"
                           value="<s:iterator id="list" value="dispatchrevise.noticeDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="noticeDepartmentId" name="noticeDepartmentId"
                           value="<s:iterator id="list" value="dispatchrevise.noticeDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row" <s:if test='dispatchrevise == null || dispatchrevise.object!="3"'>style="display:none"</s:if>  id="showPerson">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  通知个人
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="noticeUsers" name="noticeUsers"
                           value="<s:iterator id="list" value="dispatchrevise.noticeUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="noticeUsersId" name="noticeUsersId"
                           value="<s:iterator id="list" value="dispatchrevise.noticeUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  原附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="oldfilename" placeholder="" type="file" style="display: none">
                    <input name="oldfileId" id="oldfileId" style="display: none" value="<s:property value="oldfileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  修订附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filerevisename" placeholder="" type="file" style="display: none">
                    <input name="filereviseId" id="filereviseId" style="display: none" value="<s:property value="filereviseId"/>">
                  </label>
                </section>
              </div>
              <s:if test="flowNumStatus==2">
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否需要会签
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="mulExam" value="1" >
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio"  name="mulExam" value="0" >
                      <i></i>否</label>
                  </div>
                </section>
              </div>

              <div class="row" <s:if test='dispatchrevise== null || dispatchrevise.mulExam!="1"'>style="display:none"</s:if> id="showExamUsers">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-examUsers">会签人员</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="examUsers2" name="examUsers"
                           value="<s:iterator id="list" value="dispatchrevise.examUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="examUsers2Id" name="examUsersId"
                           value="<s:iterator id="list" value="dispatchrevise.examUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    是否需要中心领导审核
                  </label>
                  <section class="col col-5">
                    <div class="inline-group">
                      <label class="radio">
                        <input type="radio"  name="leaderExam" value="1" >
                        <i></i>是</label>
                      <label class="radio">
                        <input type="radio"  name="leaderExam" value="0" >
                        <i></i>否</label>
                    </div>
                  </section>
                </div>

                <div class="row" <s:if test='dispatchrevise == null || dispatchrevise.leaderExam!="1"'>style="display:none"</s:if> id="showExamObject">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    中心审核对象
                  </label>
                  <section class="col col-5">
                    <div class="inline-group">
                      <s:iterator value="examObject" id="list">
                        <label class="checkbox">
                          <input type="checkbox"  name="examObjectId" value="<s:property value="#list.id"/>" <s:property value="#list.checked"/> >
                          <i></i><span><s:property value="#list.name"/></span>
                        </label>
                      </s:iterator>
                    </div>
                  </section>
                </div>
              </s:if>
              <s:if test="flowNumStatus>2">
                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    是否需要会签
                  </label>
                  <section class="col col-5">
                    <div class="inline-group state-disabled">
                      <label class="radio">
                        <input type="radio" disabled  value="1" <s:property value="dispatchrevise.mulExam==1?'checked':''"/>>
                        <i></i>是</label>
                      <label class="radio">
                        <input type="radio" disabled  value="0" <s:property value="dispatchrevise.mulExam==0?'checked':''"/>>
                        <i></i>否</label>
                    </div>
                  </section>
                </div>

                <div class="row" <s:if test='dispatchrevise == null || dispatchrevise.mulExam!="1"'>style="display:none"</s:if>>
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    会签人员
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input disabled type="text"  name="examUsers"
                             value="<s:iterator id="list" value="dispatchrevise.examUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden"  name="examUsersId"
                             value="<s:iterator id="list" value="dispatchrevise.examUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    是否需要中心领导审核
                  </label>
                  <section class="col col-5">
                    <div class="inline-group state-disabled">
                      <label class="radio">
                        <input type="radio"  disabled name="leaderExam" value="1" <s:property value="dispatchrevise.leaderExam==1?'checked':''"/>>
                        <i></i>是</label>
                      <label class="radio">
                        <input type="radio"  disabled name="leaderExam" value="0" <s:property value="dispatchrevise.leaderExam==0?'checked':''"/>>
                        <i></i>否</label>
                    </div>
                  </section>
                </div>

                <div class="row" <s:if test='dispatchrevise == null || dispatchrevise.leaderExam!="1"'>style="display:none"</s:if> >
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    中心审核对象
                  </label>
                  <section class="col col-5">
                    <div class="inline-group ">
                      <s:iterator value="examObject" id="list">
                        <label class="checkbox  state-disabled">
                          <input disabled type="checkbox" name="examObjectId" value="<s:property value="#list.id"/>" <s:property value="#list.checked"/> >
                          <i></i><span><s:property value="#list.name"/></span>
                        </label>
                      </s:iterator>
                    </div>
                  </section>
                </div>
              </s:if>


            </fieldset>

          </form>
          <div class="flow">
            <s:if test="dispatchrevise.getProcessState().name()=='Running'">
            <div class="f_title">审批意见</div>
            <div class="chat-footer"style="margin-top:5px">
              <div class="textarea-div">
                <div class="typearea">
                  <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                </div>
              </div>
              <span class="textarea-controls"></span>
            </div>
            </s:if>
            <div class="f_title"><i class="right" id="flow2"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow2"></div>
            </div>
            <div class="f_title"><i class="right" id="next2"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext2"></div>
            </div>
          </div>
        </div>
    </div>
  </article>

</div>

<script>
  //返回视图
  $("#btn-re-common2").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!dispatchrevise.action",$('div#s2'));
    }

  });
  //编辑
  $("a[key=ajax_edit2]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-dispatchrevise!input.action?keyId="+$("#dispatchrevise #keyId").val()+"&draft="+draft,$('#content'));
  });

  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("#dispatchrevise input#keyId").val()+"&type=flow",$('#showFlow2'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("#dispatchrevise input#keyId").val()+"&type=next",$('#showNext2'));
    ajax_action("ajax-config!operateType.action",{keyId: $("#dispatchrevise input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("#dispatchrevise input#keyId").val(),
          flowName: "dispatchrevise"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("#dispatchrevise input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("dispatchrevise","../manage/ajax-dispatchrevise!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common2").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("dispatchrevise","../manage/ajax-dispatchrevise!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common2").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          if($("#dispatchrevise").valid()){
            form_save("dispatchrevise","../manage/ajax-dispatchrevise!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
              $("#btn-re-common2").trigger("click");
            })
          }
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("dispatchrevise", "../manage/ajax-dispatchrevise!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
              $("#btn-re-common2").trigger("click");
            })
        });
        break;
      case "4":
        //第四步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("dispatchrevise", "../manage/ajax-dispatchrevise!approve4.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common2").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-dispatchrevise!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#dispatchrevise #curDutyId').val();
      var data={keyId:$("#dispatchrevise input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common2").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-dispatchrevise!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#dispatchrevise #curDutyId').val();
      var data={keyId:$("#dispatchrevise input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common2").trigger("click");
      });
    });
    //流程信息展开
    $('#flow2,#next2').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });

  readLoad({
    objId:"oldfilename",
    entityName:"oldfileIds",
    sourceId:"oldfileId"
  });
  //上传
  readLoad({
    objId:"filerevisename",
    entityName:"filereviseIds",
    sourceId:"filereviseId"
  });

  //是否需要会签
  $(":radio[name='mulExam']").click(function(){
    if($(this).val() == "1"){
      $("#showExamUsers").show();
    }else{
      $("#showExamUsers").hide();
    }
  });
  //是否需要中心领导审核
  $(":radio[name='leaderExam']").click(function(){
    if($(this).val() == "1"){
      $("#showExamObject").show();
    }else{
      $("#showExamObject").hide();
    }
  });


  //校验
  $("#dispatchrevise").validate({
    rules : {
      mulExam : {
        required : true
      },
      examUsersId:{
        required : function(){
          if($('#dispatchrevise :checked[name="mulExam"]').val() == "1"){
            return true;
          }else{
            return false;
          }
        }
      },
      leaderExam:{
        required : true
      },
      examObjectId:{
        required : function(){
          if($('#dispatchrevise :checked[name="leaderExam"]').val() == "1"){
            return true;
          }else{
            return false;
          }
        }
      }
    },
    messages : {
      mulExam : {
        required : "请选择是否需要会签"
      },
      examUsersId:{
        required : "请选择会签人员"
      },
      leaderExam:{
        required : "请选择是否需要中心领导审核"
      },
      examObjectId:{
        required : "请选择中心审核对象"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });


  //会签人员
  $("a[key=btn-choose-examUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择会签人员",
      url:"ajax-dialog!levelUser.action?key=examUsers2&more=1",
      width:560
    }).show();
  });
</script>
