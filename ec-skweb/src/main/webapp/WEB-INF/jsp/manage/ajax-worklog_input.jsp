<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/21
  Time: 13:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>工作日志</a>
          <s:if test="worklog==null || worklog.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="worklog" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="workLog.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              工作日志&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  日志时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择日志时间" id="logDate" name="logDate"
                            type="text" value="<s:date name="workLog.logDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  日志名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="name" id="name" placeholder="请输入日志名称" value="<s:property value="workLog.name"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  工作日志内容
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="content" id="content" placeholder="请输入工作日志内容" value="<s:property value="workLog.content"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  相关附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"worklog",
      todo:"1"
    };
    multiDuty(pdata);

  });
  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });


  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!worklog.action",$('#content'));
    }

  });
  //校验
  $("#worklog").validate({
    rules : {
      logDate : {
        required : true
      },
      name : {
        required : true
      },
      content : {
        required : true
      },
      fileIds:{
        required : true
      }
    },
    messages : {
      logDate : {
        required : "请选择日志时间"
      },
      name : {
        required : "请输入日志名称"
      },
      content : {
        required : "请输入工作日志内容"
      },
      fileIds : {
        required : "请选择相关附件"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#logDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("worklog","../manage/ajax-worklog!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!worklog.action",$('#content'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#worklog").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#worklog").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("worklog","../manage/ajax-worklog!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!worklog.action",$('#content'));
                }
              }
            });
          }
  );
</script>