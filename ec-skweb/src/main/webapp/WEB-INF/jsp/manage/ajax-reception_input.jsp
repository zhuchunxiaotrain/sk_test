<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/31
  Time: 9:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<style>
  #dataTable{
    margin-bottom: 20px;
  }
  #data td{
    height: 30px;
    text-align: center;
  }
  #data td input[type="text"]{
    width: 100%;
    height: 100%;
    border: none;
  }
  #data td input[type="button"]{
    margin-left: 4px;
    width: 100%;
  }
</style>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <s:if test="reception==null || reception.getProcessState().name()=='Draft'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
        </s:if>
        <s:if test="reception!=null && reception.getProcessState().name()=='Backed'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
        </s:if>
        <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
        <hr class="simple">
        <form id="reception" class="smart-form" novalidate="novalidate" action="" method="post">
          <input type="hidden" name="keyId" id="keyId" value="<s:property value="reception.id" />"/>
          <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
          <input type="hidden" name="object" id="object" value="<s:property value="reception.object" />" />
          <header  style="display: block;">
            接待登记表&nbsp;&nbsp;<span id="title"></span>
          </header>
          <fieldset>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                公函附件
              </label>
              <section class="col col-5">
                <label class="input">
                  <input name="uploadify" id="filename" placeholder="" type="file" >
                  <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                公函号
              </label>
              <section class="col col-5">
                <label class="input">
                  <input class="num" type="text" name="letterNo" id="letterNo" placeholder="请输入公函号" value="<s:property value="reception.letterNo"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                来宾姓名
              </label>
              <section class="col col-2">
                <label class="input">
                  <input type="text" name="guestName" id="guestName" placeholder="请输入来宾姓名" value="<s:property value="reception.guestName"/>" >
                </label>
              </section>

              <label class="label col col-1">
                <i class="fa fa-asterisk txt-color-red"></i>
                性别
              </label>
              <section class="col col-2">
                <div class="inline-group">
                  <label class="radio">
                    <input type="radio"  name="sex" value="1" <s:property value="reception.sex==1?'checked':''"/>>
                    <i></i>男</label>
                  <label class="radio">
                    <input type="radio"  name="sex" value="0" <s:property value="reception.sex==0?'checked':''"/>>
                    <i></i>女</label>
                </div>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                来访单位
              </label>
              <section class="col col-2">
                <label class="input">
                  <input  type="text" name="department" id="department" placeholder="请输入来访单位" value="<s:property value="reception.department"/>" >
                </label>
              </section>

              <label class="label col col-1">
                <i class="fa fa-asterisk txt-color-red"></i>
                职务
              </label>
              <section class="col col-2">
                <label class="input">
                  <input  type="text" name="post" id="post" placeholder="请输入职务" value="<s:property value="reception.post"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                主要随员
              </label>
              <section class="col col-2">
                <label class="input">
                  <input  type="text" name="mainAttache" id="mainAttache" placeholder="请输入主要随员" value="<s:property value="reception.mainAttache"/>" >
                </label>
              </section>

              <label class="label col col-1">
                <i class="fa fa-asterisk txt-color-red"></i>
                人数
              </label>
              <section class="col col-2">
                <label class="input">
                  <input class="num" type="text" name="count" id="count" placeholder="请输入人数" value="<s:property value="reception.count"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                <a href="javascript:void(0);" key="btn-choose-mainName">主陪姓名</a>
              </label>
              <section class="col col-2">
                <label class="input state-disabled">
                  <input disabled type="text" id="mainName" name="mainName" placeholder="请选择主陪姓名"
                         value="<s:property value="reception.mainName.name"/>"/>
                  <input type="hidden" id="mainNameId" name="mainNameId"
                         value="<s:property value="reception.mainName.id"/>"/>
                </label>
              </section>

              <label class="label col col-1">
                其他陪同人员
              </label>
              <section class="col col-2">
                <label class="input">
                  <input  type="text" name="otherPerson" id="otherPerson" placeholder="请输其他陪同人员" value="<s:property value="reception.otherPerson"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                接待事由
              </label>
              <section class="col col-5">
                <label class="input">
                  <input type="text" name="receptionReason" id="receptionReason" placeholder="请输入接待事由" value="<s:property value="reception.receptionReason"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                接待时间
              </label>
              <section class="col col-5">
                <label class="input">
                  <input  placeholder="请选择接待时间" id="date" name="date" type="text" value="<s:date name="reception.date" format="yyyy-MM-dd"/>">
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                公务内容
              </label>
              <section class="col col-5">
                <label class="input">
                  <input type="text" name="content" id="content" placeholder="请输入公务内容" value="<s:property value="reception.content"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                就餐地点
              </label>
              <section class="col col-2">
                <label class="input">
                  <input  type="text" name="eatPlace" id="eatPlace" placeholder="请输入就餐地点" value="<s:property value="reception.eatPlace"/>" >
                </label>
              </section>

              <label class="label col col-1">
                <i class="fa fa-asterisk txt-color-red"></i>
                就餐标准
              </label>
              <section class="col col-2">
                <label class="input">
                  <input  type="text" name="eatStandard" id="eatStandard" placeholder="请输入就餐标准" value="<s:property value="reception.eatStandard"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                住宿地点
              </label>
              <section class="col col-2">
                <label class="input">
                  <input  type="text" name="livePlace" id="livePlace" placeholder="请输入住宿地点" value="<s:property value="reception.livePlace"/>" >
                </label>
              </section>

              <label class="label col col-1">
                <i class="fa fa-asterisk txt-color-red"></i>
                住宿标准
              </label>
              <section class="col col-2">
                <label class="input">
                  <input  type="text" name="liveStandard" id="liveStandard" placeholder="请输入住宿标准" value="<s:property value="reception.liveStandard"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                本次费用合计
              </label>
              <section class="col col-5">
                <label class="input">
                  <input class="num" type="text" name="money" id="money" placeholder="请输入本次费用合计" value="<s:property value="reception.money"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                其他项目
              </label>
              <div class="col col-5">
                <table id="dataTable" class="table" border="1" width="100%">
                  <thead>
                    <tr>
                      <td colspan="4"><input id="add" type="button" class="btn btn-primary pull-right" value="增加行" /></td>
                    </tr>
                    <tr>
                      <td>其他项目</td>
                      <td>项目内容</td>
                      <td>金额</td>
                      <td>操作</td>
                    </tr>
                  </thead>
                  <tbody id="data">
                    <tr>
                      <td>
                        <input class="rowData dataName" type="text" style="padding-left: 5px"/>
                      </td>
                      <td>
                        <input class="rowData dataContent" type="text" style="padding-left: 5px"/>
                      </td>
                      <td>
                        <input class="rowData dataMoney num" type="text" style="padding-left: 5px"/>
                      </td>
                      <td>
                        <input disabled class="del btn btn-primary" type="button" value="删除" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div class="row">
              <label class="label col col-2">
                备注
              </label>
              <section class="col col-5">
                <label class="input">
                  <input type="text" name="remarks" id="remarks" placeholder="请输入备注" value="<s:property value="reception.remarks"/>" >
                </label>
              </section>
            </div>


          </fieldset>
        </form>
      </div>
    </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"reception",
      todo:"1"
    };
    multiDuty(pdata);

    if($("#object").val()!=null&&$("#object").val()!=""){
      var object=eval($("#object").val());
      $("#data").empty();
      for(var i=0;i<object.length;i++){
        var newTr=$("<tr>" +
        "<td><input class='rowData dataName' type='text' value='"+object[i].name+"' style='padding-left: 5px'/></td>" +
        "<td><input class='rowData dataContent' type='text' value='"+object[i].content+"' style='padding-left: 5px'/></td>" +
        "<td><input class='rowData dataMoney num' type='text' value='"+object[i].money+"' style='padding-left: 5px'/></td>" +
        "<td><input class='del btn btn-primary' type='button' value='删除' /></td>" +
        "</tr>");
        $("#data").append(newTr);
      }
      if(object.length==1){
        $(".del").attr("disabled","disabled");
      }
    }

  });

  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });

  //选择主陪姓名
  $("a[key=btn-choose-mainName]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知个人",
      url:"ajax-dialog!levelUser.action?key=mainName&more=0",
      width:560
    }).show();
  });

  //增加行
  var add=true;
  $("#add").click(function(){
    $(".rowData").each(function(){
      if($(this).val()==null||$(this).val()==""){
        add=false;
      }
    });
    if(add){
      $(".del").removeAttr("disabled");
      var newTr=$("<tr>" +
      "<td><input class='rowData dataName' type='text' style='padding-left: 5px'/></td>" +
      "<td><input class='rowData dataContent' type='text' style='padding-left: 5px'/></td>" +
      "<td><input class='rowData dataMoney num' type='text' style='padding-left: 5px'/></td>" +
      "<td><input class='del btn btn-primary' type='button' value='删除' /></td>" +
      "</tr>");
      $("#data").append(newTr);

      $(".del").click(function(){
        $(this).parent().parent().remove();
        if($("#data tr").size()==1){
          $(".del").attr("disabled","disabled");
        }
      });

      //数字禁止输入其他字符
      $(".num").keyup(function(){
        $(this).val($(this).val().replace(/[^0-9.]/g,''));
      }).bind("paste",function(){
        $(this).val($(this).val().replace(/[^0-9.]/g,''));
      }).css("ime-mode", "disabled");

    }else{
      alert("请填写完整");
      add=true;
    }

  });

  $(".del").click(function(){
    $(this).parent().parent().remove();
    if($("#data tr").size()==1){
      $(".del").attr("disabled","disabled");
    }
  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!reception.action?viewtype=1",$('#content'));
    }
  });

  //校验
  $("#reception").validate({
    rules : {
      fileIds : {
        required : true
      },
      letterNo : {
        required : true
      },
      guestName : {
        required : true
      },
      sex : {
        required : true
      },
      department : {
        required : true
      },
      post : {
        required : true
      },
      mainAttache : {
        required : true
      },
      count : {
        required : true
      },
      mainNameId : {
        required : true
      },
      otherPerson : {
        required : true
      },
      receptionReason : {
        required : true
      },
      date : {
        required : true
      },
      content : {
        required : true
      },
      eatPlace : {
        required : true
      },
      eatStandard : {
        required : true
      },
      livePlace : {
        required : true
      },
      liveStandard : {
        required : true
      },
      money : {
        required : true
      }
    },
    messages : {
      fileIds : {
        required : "请上传公函附件"
      },
      letterNo : {
        required : "请输入公函号"
      },
      guestName : {
        required : "请输入来宾姓名"
      },
      sex : {
        required : "请选择性别"
      },
      department : {
        required : "请输入来访单位"
      },
      post : {
        required : "请输入职务"
      },
      mainAttache : {
        required : "请输入主要随员"
      },
      count : {
        required : "请输入人数"
      },
      mainNameId : {
        required : "请选择主陪姓名"
      },
      otherPerson : {
        required : "请输入其他陪同人员"
      },
      receptionReason : {
        required : "请输入接待事由"
      },
      date : {
        required : "请选择接待时间"
      },
      content : {
        required : "请输入公务内容"
      },
      eatPlace : {
        required : "请输入就餐地点"
      },
      eatStandard : {
        required : "请输入就餐标准"
      },
      livePlace : {
        required : "请输入住宿地点"
      },
      liveStandard : {
        required : "请输入住宿标准"
      },
      money : {
        required : "请输入本次费用合计"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#date').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //设置object
  function setObject(){
    var object="[";
    for(var i=0;i<$("#data tr").size();i++){
      object+='{"name":"'+$('.dataName')[i].value+
              '","content":"'+$('.dataContent')[i].value+
              '","money":"'+$('.dataMoney')[i].value+'"},';
    }
    object=object.substring(0,object.length-1);
    object+="]";
    $("#object").val(object);
  }

  //保存
  $("#btn-save-common").click(
          function(){
            setObject();
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("reception","../manage/ajax-reception!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!reception.action?viewtype=1",$('#content'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            setObject();
            if(!$("#reception").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#reception").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("reception","../manage/ajax-reception!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!reception.action?viewtype=1",$('#content'));
                }
              }
            });
          }
  );

  //数字禁止输入其他字符
  $(".num").keyup(function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).bind("paste",function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).css("ime-mode", "disabled");
</script>