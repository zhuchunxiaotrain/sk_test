<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article  class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>

          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 公告通知 </a>
            </li>
            <li class="disabled">
              <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 查看明细 </a>
            </li>
          </ul>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
          <hr class="simple">
          <form id="notice" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="notice.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <shiro:hasAnyRoles name="wechat">
              <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(notice.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>
            <header  style="display: block;">
              公告通知&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  通知主题
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="topic" id="topic" disabled placeholder="请输入通知主题" value="<s:property value="notice.topic"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  是否具有有效期
                </label>
                <section class="col col-5">
                  <div class="inline-group state-disabled">
                    <label class="radio">
                      <input type="radio"  disabled name="hasValid" value="1" <s:property value="notice.hasValid==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio"  disabled name="hasValid" value="0" <s:property value="notice.hasValid==0?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>
              <div class="row" <s:if test="notice==null  || notice.hasValid!=1">style="display:none"</s:if> id="showValid">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  有效期
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  disabled  id="validDate" name="validDate"
                            type="text" value="<s:date name="notice.validDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  是否置顶
                </label>
                <section class="col col-5">
                  <div class="inline-group state-disabled">
                    <label class="radio">
                      <input type="radio" disabled name="top" value="1"  <s:property value="notice.top==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio" disabled name="top" value="0" <s:property value="notice == null || notice.top==null || notice.top==0?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                   公告类型
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text"  disabled  value="<s:property value="notice.type.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                 发布部门
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="department" id="department" value="<s:property value="notice.department.name"/>" >
                    <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="notice.department.id"/>"/>
                  </label>
                </section>
              </div>
              <s:if test="notice.getProcessState().name()!='Finished' || loginUser.id == notice.creater.id">
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                   通知对象
                </label>
                <section class="col col-5">
                  <div class="inline-group state-disabled">
                    <label class="radio">
                      <input type="radio" disabled name="object" value="1"  <s:property value='notice.object=="1"  ?"checked":""'/>>
                      <i></i>全体员工</label>
                    <label class="radio">
                      <input type="radio" disabled name="object" value="2" <s:property value='notice.object=="2"?"checked":""'/>>
                      <i></i>部门</label>
                    <label class="radio">
                      <input type="radio" disabled name="object" value="3" <s:property value='notice.object=="3"?"checked":""'/>>
                      <i></i>个人</label>
                  </div>
                </section>
              </div>
              <div class="row" <s:if test='notice == null || notice.object!="2"'>style="display:none"</s:if>  id="showDept">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                 通知部门
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="noticeDepartment" name="noticeDepartment"
                           value="<s:iterator id="list" value="notice.noticeDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="noticeDepartmentId" name="noticeDepartmentId"
                           value="<s:iterator id="list" value="notice.noticeDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
                <div class="row" <s:if test='notice == null || notice.object!="3"'>style="display:none"</s:if>  id="showPerson">
                    <label class="label col col-2">
                        <i class="fa fa-asterisk txt-color-red"></i>
                        通知个人
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text" id="noticeUsers" name="noticeUsers"
                               value="<s:iterator id="list" value="notice.noticeUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                        <input type="hidden" id="noticeUsersId" name="noticeUsersId"
                               value="<s:iterator id="list" value="notice.noticeUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                </div>
              </s:if>

              <div class="row">
                <label class="label col col-2">
                  内容
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="content" id="content"  value="<s:property value="notice.content"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                 附件上传
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input name="uploadify" id="filename" style="display: none" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>
            </fieldset>

          </form>
          <div class="flow">
            <s:if test="notice.getProcessState().name()=='Running'">
            <div class="f_title">审批意见</div>
            <div class="chat-footer"style="margin-top:5px">
              <div class="textarea-div">
                <div class="typearea">
                  <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                </div>
              </div>
              <span class="textarea-controls"></span>
            </div>
            </s:if>
            <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow"></div>
            </div>
            <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext"></div>
            </div>
          </div>
        </div>
          <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
        </div>
      </div>
    </div>
   </article>

</div>

<script>
  var state = "<s:property value="notice.getProcessState().name()" />";
  if(state == "Finished"){
    $('#other1').parent('li').removeClass('disabled');
  }
  var table_global_width=0;
  $("a#other1").off("click").on("click",function(e) {
    if(state == "Finished"){
      if (table_global_width == 0) {
        table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
      }
      loadURL("../manage/ajax!noticequery.action?parentId="+$("#keyId").val(),$('div#s2'));
    }else{
      return false;
    }

  });

  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "notice"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("notice","../manage/ajax-notice!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("notice","../manage/ajax-notice!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("notice","../manage/ajax-notice!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("notice","../manage/ajax-notice!approve3.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-notice!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-notice!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });
  //上传文件
  readLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId"
  });
  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!notice.action?viewtype=<s:property value="viewtype"/>",$('#content'));
    }

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-notice!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

</script>
