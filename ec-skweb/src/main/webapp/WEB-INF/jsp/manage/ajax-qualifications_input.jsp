<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/20
  Time: 11:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>公司资质登记表</a>
          <s:if test="qualifications==null || qualifications.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="qualifications!=null && qualifications.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="qualifications" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="qualifications.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              公司资质登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-department">所属单位</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="department" id="department" placeholder="请选择发布部门" value="<s:property value="qualifications.department.name"/>" >
                    <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="qualifications.department.id"/>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  资质类别
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="type" value="1" <s:property value="qualifications.type==1?'checked':''"/>>
                      <i></i>企业证照
                    </label>

                    <label class="radio">
                      <input type="radio" name="type" value="2" <s:property value="qualifications.type==2?'checked':''"/>>
                      <i></i>企业资质
                    </label>
                  </div>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  资质名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="qualificationsDictId" id="qualificationsDictId" >
                      <option value="">请选择</option>
                      <s:iterator value="qualificationsDictList" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  资质号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" id="qualificationsNo" disabled placeholder="资质号" value="<s:property value="qualifications.qualificationsDict.no"/>" >
                  </label>
                </section>
              </div>

              <div class="row" id="showPerson">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-handler">办理人员</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="handler" name="handler" placeholder="请选择办理人员"
                           value="<s:property value="qualifications.handler.name"/>"/>
                    <input type="hidden" id="handlerId" name="handlerId"
                           value="<s:property value="qualifications.handler.id"/>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  使用年限
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择使用年限" id="useDate" name="useDate"
                            type="text" value="<s:date name="qualifications.useDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";
  function getDefaultDept(data){
    $('#department').val(data.department);
    $('#departmentId').val(data.departmentId);
  }
  var time1 = setInterval("checkDuty()",1000);
  function checkDuty(){
    var curDutyId = $('#curDutyId').val();
    if(curDutyId != ""){
      clearInterval(time1)
      var vActionUrl = '../manage/ajax-qualifications!getDefaultDept.action?curDutyId='+curDutyId;
      ajax_action(vActionUrl,null,null,getDefaultDept);
    }
  }
  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"qualifications",
      todo:"1"
    };
    multiDuty(pdata);
  });

  //所属单位
  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择发布部门",
      url:"ajax-dialog!dept.action?key=department",
      width:560
    }).show();
  });

  //办理人员
  $("a[key=btn-choose-handler]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知个人",
      url:"ajax-dialog!levelUser.action?key=handler",
      width:560
    }).show();
  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!qualifications.action?step=1",$('#content'));
    }

  });
  //校验
  $("#qualifications").validate({
    rules : {
      departmentId : {
        required : true
      },
      type: {
        required : true
      },
      qualificationsDictId:{
        required : true
      },
      handlerId:{
        required : true
      },
      useDate:{
        required : true
      }
    },
    messages : {
      departmentId : {
        required : "请选择所属单位"
      },
      type : {
        required : "请选择资质类别"
      },
      qualificationsDictId : {
        required : "请选择资质名称"
      },
      handlerId:{
        required : "请选择办理人员"
      },
      useDate:{
        required : "请选择使用年限"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  //选择资质名称之后获取数据
  $("#qualificationsDictId").change(function(){
    var qualificationsDictId=$(this).children('option:selected').val();
    if(qualificationsDictId!=null){
      $.ajax({
        url:"../manage/ajax-qualifications!getInfo.action",
        type:"post",
        cache:false,dataType:"json",async:true,
        data:{"qualificationsDictId":qualificationsDictId},
        success:function(data){
          if(data.result.errorCode == 1){
            $("#qualificationsNo").val(data.data.no);
          }
        }
      });
    }
  });

  $('#useDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
          function(){
//            $("#btn-save-common").attr("disabled", "disabled");
            form_save("qualifications","../manage/ajax-qualifications!save.action",null,function(data){
              if(data.state==200){
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!qualifications.action?step=1",$('#content'));
                }
              }
            });
          }
  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#qualifications").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#qualifications").valid();
                if(!$validForm) return false;
//                $("#btn-confirm-common").attr("disabled", "disabled");
//                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("qualifications","../manage/ajax-qualifications!commit.action",null,function(data){
                  if(data.state==200){
                    if(draft == 1){
                      location.href='index.action';
                    }else{
                      loadURL("../manage/ajax!qualifications.action?step=1",$('#content'));
                    }
                  }
                });
              }
            });
          }
  );

  //数字禁止输入其他字符
  $(".num").keyup(function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).bind("paste",function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).css("ime-mode", "disabled");
</script>