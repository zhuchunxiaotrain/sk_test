<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<div id="business_data">

  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_noticequery_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_noticequery_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
      load_notice_jqGrid();
      $("#ajax_noticequery_list_row").width(table_global_width);
      jQuery("#ajax_noticequery_table").jqGrid('setGridWidth', table_global_width);

  });
  function load_notice_jqGrid(){
    jQuery("#ajax_noticequery_table").jqGrid({
      url:'../manage/ajax-noticequery!list.action?parentId='+"<s:property value="parentId" />",
      datatype: "json",
      colNames:['查看人','查看日期',"id"],
      colModel:[
        {name:'users',index:'users_name',sortable:true,search:true,width:80},
        {name:'createDate',index:'createDate', width:100,search:true,sortable:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_noticequery_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        var ids=$("#ajax_noticequery_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_noticequery_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_notice_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_noticequery_table").jqGrid('setRowData',ids[i],{act:""});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 查看明细",
      multiselect : false,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_noticequery_table").jqGrid('setGridWidth', $("#ajax_noticequery_list_row").width());
    })
    jQuery("#ajax_noticequery_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_noticequery_table").jqGrid('navGrid', "#ajax_noticequery_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };


</script>





















