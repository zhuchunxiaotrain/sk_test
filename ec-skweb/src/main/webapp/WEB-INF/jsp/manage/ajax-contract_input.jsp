<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/23
  Time: 13:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>合同管理</a>
          <s:if test="contract==null || contract.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="contract!=null && contract.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="contract" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="contract.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              合同登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同编号
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" class="num" name="no" id="no" placeholder="请输入合同编号" value="<s:property value="contract.no"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="name" id="name" placeholder="请输入合同名称" value="<s:property value="contract.name"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-department"> 经办部门</a>
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input disabled type="text" name="department" id="department" placeholder="请选择经办部门" value="<s:property value="contract.department.name"/>" >
                    <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="contract.department.id"/>"/>
                  </label>
                </section>

                <label class="label col col-1">
                  经办人
                </label>
                <section class="col col-2">
                  <label class="input">
                    <input  type="text" name="manager" id="manager" placeholder="请输入经办人" value="<s:property value="contract.manager"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  经办人职务
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="managerPost" id="managerPost" placeholder="请输入经办人职务" value="<s:property value="contract.managerPost"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同甲方
                </label>
                <section class="col col-2">
                  <label class="input">
                    <input  type="text" name="contractA" id="contractA" placeholder="请输入合同甲方" value="<s:property value="contract.contractA"/>" >
                  </label>
                </section>

                <label class="label col col-1">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同乙方
                </label>
                <section class="col col-2">
                  <label class="input">
                    <input  type="text" name="contractB" id="contractB" placeholder="请输入合同乙方" value="<s:property value="contract.contractB"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  合同第三方
                </label>
                <section class="col col-2">
                  <label class="input">
                    <input  type="text" name="contractC" id="contractC" placeholder="请输入合同第三方" value="<s:property value="contract.contractC"/>" >
                  </label>
                </section>

                <label class="label col col-1">
                  合同其他方
                </label>
                <section class="col col-2">
                  <label class="input">
                    <input  type="text" name="contractD" id="contractD" placeholder="请输入合同其他方" value="<s:property value="contract.contractD"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  预算编号
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" class="num" name="budgetNo" id="budgetNo" placeholder="请输入预算编号" value="<s:property value="contract.budgetNo"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同标的（小写）
                </label>
                <section class="col col-2">
                  <label class="input">
                    <input  type="text" class="num" name="subjectSmall" id="subjectSmall" placeholder="请输入合同标的（小写）" value="<s:property value="contract.subjectSmall"/>" >
                  </label>
                </section>

                <label class="label col col-1">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同标的（大写）
                </label>
                <section class="col col-2">
                  <label class="input">
                    <input  type="text" name="subjectBig" id="subjectBig" placeholder="请输入合同标的（大写）" value="<s:property value="contract.subjectBig"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择合同日期" id="date" name="date" type="text" value="<s:date name="contract.date" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同主要事宜
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="mainThing" id="mainThing" placeholder="请输入合同主要事宜" value="<s:property value="contract.mainThing"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  相关附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"contract",
      todo:"1"
    };
    multiDuty(pdata);

  });
  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });


  //选择经办部门
  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知部门",
      url:"ajax-dialog!dept.action?key=department&more=0",
      width:560
    }).show();
  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!contract.action?viewtype=1",$('#content'));
    }

  });
  //校验
  $("#contract").validate({
    rules : {
      no : {
        required : true
      },
      name : {
        required : true
      },
      departmentId : {
        required : true
      },
      contractA : {
        required : true
      },
      contractB:{
        required : true
      },
      budgetNo: {
        required : true
      },
      subjectSmall: {
        required : true
      },
      subjectBig: {
        required : true
      },
      date: {
        required : true
      },
      mainThing: {
        required : true
      },
      fileIds:{
        required:true
      }
    },
    messages : {
      no : {
        required : "请输入合同编号"
      },
      name : {
        required : "请输入合同名称"
      },
      departmentId : {
        required : "请选择经办部门"
      },
      contractA : {
        required : "请输入合同甲方"
      },
      contractB : {
        required : "请输入合同乙方"
      },
      budgetNo : {
        required : "请输入预算编号"
      },
      subjectSmall: {
        required : "请输入合同标的（小写）"
      },
      subjectBig:{
        required : "请输入合同标的（大写）"
      },
      date:{
        required: "请选择合同日期"
      },
      mainThing : {
        required : "请输入合同主要事宜"
      },
      fileIds:{
        required:"请选择相关附件"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#date').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("contract","../manage/ajax-contract!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!contract.action?viewtype=1",$('#content'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#contract").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#contract").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("contract","../manage/ajax-contract!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!contract.action?viewtype=1",$('#content'));
                }
              }
            });
          }
  );

  //数字禁止输入其他字符
  $(".num").keyup(function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).bind("paste",function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).css("ime-mode", "disabled");
</script>
