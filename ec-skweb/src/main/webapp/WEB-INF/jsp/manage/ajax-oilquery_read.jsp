<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/23
  Time: 9:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<s:if test="draft == 1">
  <jsp:include page="../com/ajax-top.jsp" />
</s:if>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-read-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>加油明细返回
          </a>

          <shiro:hasAnyRoles name="wechat">
            <a style="margin-top: -13px !important;" <s:property value="isEdit(oilquery.id)"/> class="btn btn-default pull-right pull-right-fix" key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
          </shiro:hasAnyRoles>

          <hr class="simple">
          <form id="oilquery" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="oilQuery.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />
            <header  style="display: block;">
              车辆加油明细表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  当前公里数
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text" name="oilQueryKilometre" id="oilQueryKilometre" placeholder="当前公里数" value="<s:property value="oilQuery.kilometre"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  加油时间
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  placeholder="请选择加油时间" id="oilQueryDate" name="oilQueryDate" disabled type="text" value="<s:date name="oilQuery.oilDate" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  加油型号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text" name="typeText" id="typeText" placeholder="加油型号" value="<s:property value="typeText"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  加油金额
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text" name="oilQueryMoney" id="oilQueryMoney" placeholder="加油金额" value="<s:property value="oilQuery.oilMoney"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  累计至本次加油金额
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text" name="oilQueryMoneyNum" id="oilQueryMoneyNum" placeholder="累计至本次加油金额" value="<s:property value="oilQuery.oilMoneyNum"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  备注
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text" name="oilQueryRemarks" id="oilQueryRemarks" placeholder="备注" value="<s:property value="oilQuery.remarks"/>" >
                  </label>
                </section>
              </div>
              
            </fieldset>
          </form>
          <div class="flow" id="apply">
            <s:if test="oilQuery.getProcessState().name()=='Running'">
              <div class="f_title">审批意见</div>
              <div class="chat-footer"style="margin-top:5px">
                <div class="textarea-div">
                  <div class="typearea">
                    <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                  </div>
                </div>
                <span class="textarea-controls"></span>
              </div>
            </s:if>
            <div class="f_title" name="apply"><i class="right" id="flow"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow"></div>
            </div>
            <div class="f_title" name="apply"><i class="right" id="next"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">
  $(function(){
    $("input").attr("disabled","disabled").parent().addClass(" state-disabled");
    loadURL("ajax-running!workflow.action?bussinessId="+$("#oilquery input#keyId").val()+"&type=flow",$('#apply #showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("#oilquery input#keyId").val()+"&type=next",$('#apply #showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("#oilquery input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "oilquery"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("#oilquery  input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("oilquery","../manage/ajax-oilquery!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
            $("#btn-apply-read-common").trigger("click");
          })
        });
        break;
    }
   
    //流程信息展开
    $('#apply #flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });

  });

  //返回视图
  $("#btn-apply-read-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));//主页
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));//个人代办
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));//提醒事项
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));//已办未结
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));//已办已结
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));//草稿
    }else{
      loadURL("../manage/ajax!oilquery.action?parentId="+$("#parentId").val(),$('div#s2'));
    }
  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    var parentId="<s:property value="oilQuery.oil.id"/>";
    loadURL("../manage/ajax-oilquery!input.action?keyId="+$("#oilquery input#keyId").val()+"&parentId="+parentId+"&draft="+draft,$('#content'));
  });

</script>