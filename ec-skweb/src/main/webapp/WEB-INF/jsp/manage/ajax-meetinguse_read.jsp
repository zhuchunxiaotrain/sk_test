<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i>会议使用</a>
            </li>
            <li class="disabled">
              <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 会议记录 </a>
            </li>
            <li class="disabled">
              <a href="#s3" id="other2" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 会议签到 </a>
            </li>
            <li class="disabled">
              <a href="#s4" id="other3" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 会议请假 </a>
            </li>
          </ul>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
          <hr class="simple">
          <form id="meetinguse" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="meetinguse.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <shiro:hasAnyRoles name="wechat">
              <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(meetinguse.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>
            <header  style="display: block;">
              会议室使用登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请会议室
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text"  name="meetnamedictId" id="meetnamedictId" disabled placeholder="请输入申请会议室" value="<s:property value="meetinguse.meetname.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  会议类型
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="typedictId" id="typedictId" disabled placeholder="请输入会议类型" value="<s:property value="meetinguse.type.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  会议议题
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="topic" id="topic" disabled placeholder="请输入会议议题" value="<s:property value="meetinguse.topic"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  预计开始时间
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input  placeholder="预计开始时间" id="begintime" name="begintime"  disabled
                            type="text" value="<s:date name="meetinguse.begintime" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  预计结束时间
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input  placeholder="预计结束时间" id="endtime" name="endtime" disabled
                            type="text" value="<s:date name="meetinguse.endtime" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                  <label class="label col col-2">
                    参会人员
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input disabled type="text" id="joinUsers" name="joinUsers"
                             value="<s:iterator id="list" value="meetinguse.joinUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden" id="joinUsersId" name="joinUsersId"
                             value="<s:iterator id="list" value="meetinguse.joinUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="meetinguse.remark"/>" >
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
              <div class="flow">
                <s:if test="meetinguse.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer"style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
            <div class="tab-pane fade in active" id="s3" style="margin: 10px;"></div>
            <div class="tab-pane fade in active" id="s4" style="margin: 10px;"></div>
          </div>
        </div>
    </div>
  </article>
</div>

<script>
  var state = "<s:property value="meetinguse.getProcessState().name()" />";
  if(state == "Finished"){
    $('#other1').parent('li').removeClass('disabled');
    $('#other2').parent('li').removeClass('disabled');
    $('#other3').parent('li').removeClass('disabled');
  }
  var table_global_width=0;
  //会议记录
  $("a#other1").off("click").on("click",function(e) {
    if(state == "Finished"){
      if (table_global_width == 0) {
        table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
      }
      loadURL("../manage/ajax!meetingrecord.action?parentId="+$("#keyId").val(),$('div#s2'));
    }else{
      return false;
    }
  });
  //会议签到
  $("a#other2").off("click").on("click",function(e) {
    if(state == "Finished"){
      if (table_global_width == 0) {
        table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s3").width();
      }
      loadURL("../manage/ajax!meetingsign.action?parentId="+$("#keyId").val(),$('div#s3'));
    }else{
      return false;
    }
  });
  //会议请假
  $("a#other3").off("click").on("click",function(e) {
    if(state == "Finished"){
      if (table_global_width == 0) {
        table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s4").width();
      }
      loadURL("../manage/ajax!meetingleave.action?parentId="+$("#keyId").val(),$('div#s4'));
    }else{
      return false;
    }
  });
  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-meetinguse!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });
  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    var calandar = "<s:property value="calandar" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else if(calandar==1){
      loadURL("../manage/ajax!meetingusecalandar.action",$('#content'));
    }else{
      loadURL("../manage/ajax!meetinguse.action?viewtype=<s:property value="viewtype"/>",$('#content'));
    }

  });
  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });
</script>
