<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/29
  Time: 12:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <ul id="myTab1" class="nav nav-tabs bordered  ">
          <li class="active">
            <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i>资产登记表</a>
          </li>
          <li class="disabled">
            <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 资产使用明细 </a>
          </li>
        </ul>
        <div id="myTabContent1" class="tab-content padding-10 ">
          <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
            <hr class="simple">
            <form id="assetuse" class="smart-form" novalidate="novalidate" action="" method="post">
              <input type="hidden" name="keyId" id="keyId" value="<s:property value="assetUse.id" />"/>
              <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
              <shiro:hasAnyRoles name="wechat">
                <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(assetuse.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
              </shiro:hasAnyRoles>
              <header  style="display: block;">
                资产登记表&nbsp;&nbsp;<span id="title"></span>
              </header>
              <fieldset>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    <a id="toPur" href="javascript:void(0);">采购登记表</a>
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text"  name="purchaseId" id="purchaseId" disabled placeholder="请选择采购登记表" value="收款方：<s:property value="assetUse.purchase.toName"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    固定资产名称
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="name" id="name" disabled placeholder="请输入固定资产名称" value="<s:property value="assetUse.name"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    固定资产型号
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="type" id="type" disabled placeholder="请输入固定资产型号" value="<s:property value="assetUse.type"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    固定资产编号
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="no" id="no" disabled placeholder="请输入固定资产名称" value="<s:property value="assetUse.no"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    采购金额
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="money" id="money" disabled placeholder="请输入采购金额" value="<s:property value="assetUse.money"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    采购日期
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input  placeholder="采购日期" id="date" name="date"  disabled type="text" value="<s:date name="assetUse.date" format="yyyy-MM-dd"/>">
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    使用部门
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input disabled type="text" name="department" id="department" value="<s:property value="assetUse.department.name"/>" >
                      <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="assetUse.department.id"/>"/>
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    使用人员
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input disabled type="text" name="user" id="user" value="<s:property value="assetUse.user.name"/>" >
                      <input type="hidden" id="userId" name="userId" value="<s:property value="assetUse.user.id"/>"/>
                    </label>
                  </section>
                </div>

              </fieldset>
            </form>
            <div class="flow">
              <s:if test="assetuse.getProcessState().name()=='Running'">
                <div class="f_title">审批意见</div>
                <div class="chat-footer"style="margin-top:5px">
                  <div class="textarea-div">
                    <div class="typearea">
                      <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                    </div>
                  </div>
                  <span class="textarea-controls"></span>
                </div>
              </s:if>
              <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
              <div class="f_content" style="display:none">
                <div id="showFlow"></div>
              </div>
              <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
              <div class="f_content" style="display:none">
                <div id="showNext"></div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var state = "<s:property value="assetUse.getProcessState().name()" />";
  if(state == "Finished"){
    $('#other1').parent('li').removeClass('disabled');
  }
  var table_global_width=0;
  //资产使用明细
  $("a#other1").off("click").on("click",function(e) {
    if(state == "Finished"){
      if (table_global_width == 0) {
        table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
      }
      loadURL("../manage/ajax!usedetail.action?parentId="+$("#keyId").val(),$('div#s2'));
    }else{
      return false;
    }
  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-assetuse!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!assetuse.action?viewtype=<s:property value="viewtype"/>",$('#content'));
    }

  });
  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });

  $("#toPur").click(function(){
    loadURL("../manage/ajax-purchase!read.action?keyId=<s:property value="assetUse.purchase.id"/>&assetuseId="+$("#keyId").val(),$('#content'));
  });
</script>