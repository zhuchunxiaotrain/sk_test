<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/22
  Time: 14:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>车辆加油登记</a>
          <s:if test="oil==null || oil.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="oil!=null && oil.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="oil" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="oil.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" id="isUse" value=""/>
            <header  style="display: block;">
              车辆加油登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  车辆品牌
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="brandId" id="brandId" >
                      <option value="">请选择</option>
                      <s:iterator value="brandDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  车辆型号
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="typeId" id="typeId" >
                      <option value="">请选择</option>
                      <s:iterator value="typeDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  车牌号码
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="carNoId" id="carNoId" >
                      <option value="">请选择</option>
                      <s:iterator value="carNoDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  当前里程数
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input class="num" type="text" name="kilometre" id="kilometre" placeholder="请输入当前里程数" value="<s:property value="oil.kilometre"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  加油时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input placeholder="请选择加油时间" id="oilDate" name="oilDate" type="text" value="<s:date name="oil.oilDate" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  加油金额
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input class="num" type="text" name="oilMoney" id="oilMoney" placeholder="请输入加油金额" value="<s:property value="oil.oilMoney"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  累计加油金额
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input disabled type="text" name="oilMoneyNum" id="oilMoneyNum" value="<s:property value="oil.oilMoneyNum"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="remarks" id="remarks" placeholder="请输入备注" value="<s:property value="oil.remarks"/>" >
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>

</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"oil",
      todo:"1"
    };
    multiDuty(pdata);
  });

  $("#oilMoney").keyup(function(){
    $("#oilMoneyNum").val($("#oilMoney").val());
  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!oil.action",$('#content'));
    }

  });
  //校验
  $("#oil").validate({
    rules : {
      brandId : {
        required : true
      },
      typeId : {
        required : true
      },
      carNoId : {
        required : true
      },
      kilometre : {
        required : true
      },
      oilDate:{
        required : true
      },
      oilMoney: {
        required : true
      }
    },
    messages : {
      brandId : {
        required : "请选择车辆品牌"
      },
      typeId : {
        required : "请选择车辆型号"
      },
      carNoId : {
        required : "请选择车牌号码"
      },
      kilometre : {
        required : "请输入当前里程数"
      },
      oilDate : {
        required : "请选择加油时间"
      },
      oilMoney : {
        required : "请输入加油金额"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });


  $('#oilDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("oil","../manage/ajax-oil!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!oil.action",$('#content'));
            }
          }
  );


  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#oil").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#oil").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("oil","../manage/ajax-oil!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!oil.action",$('#content'));
                }
              }
            });
          }
  );

  //数字禁止输入其他字符
  $(".num").keyup(function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).bind("paste",function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).css("ime-mode", "disabled");
</script>