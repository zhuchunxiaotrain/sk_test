<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/28
  Time: 9:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<style>
  #detail td{
    text-align: center;
    padding: 2px 0px;
  }
  tbody td{
    height: 32px;
  }
  #detail{
    margin-bottom: 20px;
  }
  .detailText{
    border: none;
    height: 100%;
    width: 80px;
    padding: 0 0 0 15px;
  }
</style>
<div class="row">
  <!-- NEW WIDGET START -->
  <div class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <ul id="myTab1" class="nav nav-tabs bordered  ">
          <li class="active">
            <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 资产采购登记表 </a>
          </li>
        </ul>
        <div id="myTabContent1" class="tab-content padding-10 ">
          <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
            <hr class="simple">
            <form id="purchase" class="smart-form" novalidate="novalidate" action="" method="post">
              <input type="hidden" name="purchaseDetail" id="purchaseDetail" value="<s:property value="purchase.purchaseDetail" />"/>
              <input type="hidden" name="detailNum" id="detailNum" value="<s:property value="purchase.detailNum" />"/>
              <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
              <input type="hidden" name="keyId" id="keyId" value="<s:property value="purchase.id" />"/>
              <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
              <%--<input type="text" name="purchaseDetail1" id="purchaseDetail1" />--%>
              <%--<input type="text" name="detailNum1" id="detailNum1" />--%>
              <shiro:hasAnyRoles name="wechat">
                <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(purchase.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
              </shiro:hasAnyRoles>
              <header  style="display: block;">
                资产采购登记表&nbsp;&nbsp;<span id="title"></span>
                <input type="hidden" value="<s:property value="numStatus" />"/>
                <input type="hidden" value="<s:property value="flowNumStatus" />"/>
              </header>
              <fieldset>

                <div class="row">
                  <label class="label col col-2">
                    资产类别
                  </label>
                  <section class="col col-5">
                    <div class="inline-group state-disabled">
                      <label class="radio">
                        <input type="radio" disabled name="assetType" value="1" <s:property value="purchase.assetType==1?'checked':''"/>>
                        <i></i>低值易耗品
                      </label>
                      <label class="radio">
                        <input type="radio" disabled name="assetType" value="2" <s:property value="purchase.assetType==2?'checked':''"/>>
                        <i></i>固定资产
                      </label>
                    </div>
                  </section>
                </div>

                  <div class="row">
                    <label class="label col col-2">
                      采购类别
                    </label>
                    <section class="col col-5">
                      <div class="inline-group state-disabled">
                        <label class="radio">
                          <input type="radio" disabled name="purchaseType" value="1" <s:property value="purchase.purchaseType==1?'checked':''"/>>
                          <i></i>公开招标
                        </label>
                        <label class="radio">
                          <input type="radio" disabled name="purchaseType" value="2" <s:property value="purchase.purchaseType==2?'checked':''"/>>
                          <i></i>邀请招标
                        </label>
                        <label class="radio">
                          <input type="radio" disabled name="purchaseType" value="3" <s:property value="purchase.purchaseType==3?'checked':''"/>>
                          <i></i>内部招标
                        </label>
                        <label class="radio">
                          <input type="radio" disabled name="purchaseType" value="4" <s:property value="purchase.purchaseType==4?'checked':''"/>>
                          <i></i>竞争性磋商
                        </label>
                        <label class="radio">
                          <input type="radio" disabled name="purchaseType" value="5" <s:property value="purchase.purchaseType==5?'checked':''"/>>
                          <i></i>单一来源
                        </label>
                        <label class="radio">
                          <input type="radio" disabled name="purchaseType" value="6" <s:property value="purchase.purchaseType==6?'checked':''"/>>
                          <i></i>询价
                        </label>
                      </div>
                    </section>
                  </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    申请部门
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input disabled type="text" name="department" id="department" value="<s:property value="purchase.department.name"/>" >
                      <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="purchase.department.id"/>"/>
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    &nbsp;
                  </label>
                  <div class="col col-9">
                    <table class="table" border="1" id="detail" style="width: 100%">
                      <thead class="">
                      <tr>
                        <td colspan="<s:property value="flowNumStatus>=5?11:8" />"></td>
                      </tr>
                      <tr>
                        <td>序号</td>
                        <td>项目名称</td>
                        <td>数量</td>
                        <td>预算编号</td>
                        <td>预算单价</td>
                        <td>预算金额</td>
                        <s:if test="flowNumStatus>=5">
                        <td>采购单价</td>
                        <td>实际金额</td>
                        <td>采购日期</td>
                        </s:if>
                        <td>备注</td>
                      </tr>
                      </thead>
                      <tbody id="detailContent" class="" style="width: 50%">
                      <tr>
                        <td style="padding: 0 20px">1</td>
                        <td class="detailName"></td>
                        <td class="number"></td>
                        <td class="detailNo"></td>
                        <td class="budgetPrice"></td>
                        <td class="budgetMoney"></td>
                        <%--<s:if test="flowNumStatus==5">--%>
                        <%--<td class="purchasingPrice"></td>--%>
                        <%--<td class="ActualMoney"></td>--%>
                        <%--<td class="detailDate"></td>--%>
                        <%--</s:if>--%>
                        <td class="detailRemarks"></td>
                      </tr>
                      </tbody>
                      <tr>
                        <td colspan="2" style="padding: 8px 0;text-align: center">金额合计</td>
                        <td id="number">0</td>
                        <td>&nbsp;</td>
                        <td id="budgetPrice">0</td>
                        <td id="budgetMoney">0</td>
                        <s:if test="flowNumStatus>=5">
                        <td id="purchasingPrice">0</td>
                        <td id="ActualMoney">0</td>
                        <td>&nbsp;</td>
                        </s:if>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </div>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    其他要说明的情况
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="other" id="other" disabled placeholder="请输入其他要说明的情况" value="<s:property value="purchase.other"/>" >
                    </label>
                  </section>
                </div>

                  <div class="row">
                    <label class="label col col-2">
                      支付方式
                    </label>
                    <section class="col col-5">
                      <div class="inline-group state-disabled">
                        <label class="radio">
                          <input type="radio" disabled name="pay" value="1" <s:property value="purchase.pay==1?'checked':''"/>>
                          <i></i>转账
                        </label>
                        <label class="radio">
                          <input type="radio" disabled name="pay" value="2" <s:property value="purchase.pay==2?'checked':''"/>>
                          <i></i>现金
                        </label>
                      </div>
                    </section>
                  </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    收款方名称
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="toName" id="toName" disabled placeholder="请输入收款方名称" value="<s:property value="purchase.toName"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    收款方账号
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="toNo" id="toNo" disabled placeholder="请输入收款方账号" value="<s:property value="purchase.toNo"/>" >
                    </label>
                  </section>
                </div>

                <s:if test="flowNumStatus==2">
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要上报中心
                    </label>
                    <section class="col col-5">
                      <div class="inline-group">
                        <label class="radio">
                          <input type="radio"  name="leaderExam" value="1" >
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  name="leaderExam" value="0" >
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>
                </s:if>
                <s:if test="flowNumStatus>2">
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要上报中心
                    </label>
                    <section class="col col-5">
                      <div class="inline-group state-disabled">
                        <label class="radio">
                          <input type="radio"  disabled name="leaderExam" value="1" <s:property value="purchase.leaderExam==1?'checked':''"/>>
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  disabled name="leaderExam" value="0" <s:property value="purchase.leaderExam==0?'checked':''"/>>
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>
                </s:if>
              </fieldset>

            </form>
            <div class="flow">
              <s:if test="purchase.getProcessState().name()=='Running'">
                <div class="f_title">审批意见</div>
                <div class="chat-footer"style="margin-top:5px">
                  <div class="textarea-div">
                    <div class="typearea">
                      <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                    </div>
                  </div>
                  <span class="textarea-controls"></span>
                </div>
              </s:if>
              <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
              <div class="f_content" style="display:none">
                <div id="showFlow"></div>
              </div>
              <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
              <div class="f_content" style="display:none">
                <div id="showNext"></div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>


</div>

<script>
  var state = "<s:property value="purchase.getProcessState().name()" />";
  if(state == "Finished"){
    $('#other1').parent('li').removeClass('disabled');
  }
  var table_global_width=0;
  $("a#other1").off("click").on("click",function(e) {
    if(state == "Finished"){
      if (table_global_width == 0) {
        table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
      }
      loadURL("../manage/ajax!purchaserevise.action?parentId="+$("#keyId").val(),$('div#s2'));
    }else{
      return false;
    }

  });

  $(function(){

    //表格内的内容
    var step="<s:property value="flowNumStatus" />";
    var purchaseDetail=eval($("#purchaseDetail").val());
    $("#detailContent").empty();
    if(step<5){
      for(var i=0;i<purchaseDetail.length;i++){
        var j=i+1;
        var newTr=$("<tr><td style='padding: 0 20px'>"+j+"</td>" +
        "<td>"+purchaseDetail[i].name+"</td>" +
        "<td>"+purchaseDetail[i].number+"</td>" +
        "<td>"+purchaseDetail[i].detailNo+"</td>" +
        "<td>"+purchaseDetail[i].budgetPrice+"</td>" +
        "<td>"+purchaseDetail[i].budgetMoney+"</td>" +
//        "<td>"+purchaseDetail[i].purchasingPrice+"</td>" +
//        "<td>"+purchaseDetail[i].ActualMoney+"</td>" +
//        "<td>"+purchaseDetail[i].detailDate+"</td>" +
        "<td>"+purchaseDetail[i].detailRemarks+"</td>");
        $("#detailContent").append(newTr);
      }

      var detailNum=eval($("#detailNum").val());
      $("#number").text(detailNum[0].number);
      $("#budgetPrice").text(detailNum[0].budgetPrice);
      $("#budgetMoney").text(detailNum[0].budgetMoney);
//      $("#purchasingPrice").text(detailNum[0].purchasingPrice);
//      $("#ActualMoney").text(detailNum[0].ActualMoney);
    }else if(step==5){
      for(var i=0;i<purchaseDetail.length;i++){
        var j=i+1;
        var newTr=$("<tr><td style='padding: 0 20px'>"+j+"</td>" +
        "<td class='detailName'>"+purchaseDetail[i].name+"</td>" +
        "<td class='number'>"+purchaseDetail[i].number+"</td>" +
        "<td class='detailNo'>"+purchaseDetail[i].detailNo+"</td>" +
        "<td class='budgetMoney'>"+purchaseDetail[i].budgetPrice+"</td>" +
        "<td class='budgetPrice'>"+purchaseDetail[i].budgetMoney+"</td>" +
        "<td><input type='text' placeholder='请输入' class='detailText purchasingPrice num'/></td>" +
        "<td><input type='text' placeholder='请输入' class='detailText ActualMoney num'/></td>" +
        "<td><input type='text' placeholder='请选择' class='detailText detailDate'/></td>" +
        "<td class='detailRemarks'>"+purchaseDetail[i].detailRemarks+"</td>");
        $("#detailContent").append(newTr);
      }

      var detailNum=eval($("#detailNum").val());
      $("#number").text(detailNum[0].number);
      $("#budgetPrice").text(detailNum[0].budgetPrice);
      $("#budgetMoney").text(detailNum[0].budgetMoney);
      $("#purchasingPrice").text(detailNum[0].purchasingPrice);
      $("#ActualMoney").text(detailNum[0].ActualMoney);
    }else if(step>5){
      for(var i=0;i<purchaseDetail.length;i++){
        var j=i+1;
        var newTr=$("<tr><td style='padding: 0 20px'>"+j+"</td>" +
        "<td>"+purchaseDetail[i].name+"</td>" +
        "<td>"+purchaseDetail[i].number+"</td>" +
        "<td>"+purchaseDetail[i].detailNo+"</td>" +
        "<td>"+purchaseDetail[i].budgetPrice+"</td>" +
        "<td>"+purchaseDetail[i].budgetMoney+"</td>" +
        "<td>"+purchaseDetail[i].purchasingPrice+"</td>" +
        "<td>"+purchaseDetail[i].ActualMoney+"</td>" +
        "<td>"+purchaseDetail[i].detailDate+"</td>" +
        "<td>"+purchaseDetail[i].detailRemarks+"</td>");
        $("#detailContent").append(newTr);
      }

      var detailNum=eval($("#detailNum").val());
      $("#number").text(detailNum[0].number);
      $("#budgetPrice").text(detailNum[0].budgetPrice);
      $("#budgetMoney").text(detailNum[0].budgetMoney);
      $("#purchasingPrice").text(detailNum[0].purchasingPrice);
      $("#ActualMoney").text(detailNum[0].ActualMoney);
    }



    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "purchase"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("purchase","../manage/ajax-purchase!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("purchase","../manage/ajax-purchase!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          if($("#purchase").valid()){
            form_save("purchase","../manage/ajax-purchase!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
              $("#btn-re-common").trigger("click");
            })
          }
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("purchase", "../manage/ajax-purchase!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "4":
        //第四步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("purchase", "../manage/ajax-purchase!approve4.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "5":
        //第五步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          if(setTable()) {
            form_save("purchase", "../manage/ajax-purchase!approve5.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
              $("#btn-re-common").trigger("click");
            })
          }
        });
        break;
      case "6":
        //第六步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("purchase", "../manage/ajax-purchase!approve6.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "7":
        //第七步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("purchase", "../manage/ajax-purchase!approve7.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "8":
        //第八步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("purchase", "../manage/ajax-purchase!approve8.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-purchase!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-purchase!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });

  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    var assetuse = "<s:property value="assetuse" />";
    var assetuseId = "<s:property value="assetuseId" />";
    var expend = "<s:property value="expend" />";
    var expendId = "<s:property value="expendId" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else if(assetuse==1){
      loadURL("../manage/ajax-assetuse!input.action",$('#content'));
    }else if(assetuseId!=null&&assetuseId!=""){
      loadURL("../manage/ajax-assetuse!read.action?keyId="+assetuseId,$('#content'));
    }else if(expend==1&&expendId==""){
      loadURL("../finance/ajax-expend!input.action",$('#content'));
    }else if(expendId!=null&&expendId!=""){
      loadURL("../finance/ajax-expend!read.action?keyId="+expendId,$('#content'));
    }else{
      loadURL("../manage/ajax!purchase.action?step=<s:property value="step"/>",$('#content'));
    }

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-purchase!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

  //校验
  $("#purchase").validate({
    rules : {
      leaderExam:{
        required : true
      }
    },
    messages : {
      leaderExam:{
        required : "请选择是否需要中心领导审核"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  //计算
  $(".detailText").keyup(function(){
    var purchasingPrice=0;
    for(var i=0;i<$(".purchasingPrice").size();i++){
      if(isNaN(parseInt($(".purchasingPrice")[i].value))){
        purchasingPrice+=0;
      }else{
        purchasingPrice+=parseInt($(".purchasingPrice")[i].value);
      }
    }
    $("#purchasingPrice").text(purchasingPrice);

    var ActualMoney=0;
    for(var i=0;i<$(".ActualMoney").size();i++){
      if(isNaN(parseInt($(".ActualMoney")[i].value))){
        ActualMoney+=0;
      }else{
        ActualMoney+=parseInt($(".ActualMoney")[i].value);
      }
    }
    $("#ActualMoney").text(ActualMoney);
  });

  //设置表格的数据
  function setTable(){
    var isTable=true;
    $(".detailText").each(function(){
      if($(this).val()==null||$(this).val()==""){
        isTable=false;
      }
    });
    if(isTable){
      var purchaseDetail="[";
      for(var i=0;i<$("#detailContent tr").size();i++){
        purchaseDetail+='{"name":"'+$('.detailName')[i].innerText+
        '","number":"'+$('.number')[i].innerText+
        '","detailNo":"'+$('.detailNo')[i].innerText+
        '","budgetMoney":"'+$('.budgetMoney')[i].innerText+
        '","budgetPrice":"'+$('.budgetPrice')[i].innerText+
        '","purchasingPrice":"'+$('.purchasingPrice')[i].value+
        '","ActualMoney":"'+$('.ActualMoney')[i].value+
        '","detailDate":"'+$('.detailDate')[i].value+
        '","detailRemarks":"'+$('.detailRemarks')[i].innerText+'"},';
      }
      purchaseDetail=purchaseDetail.substring(0,purchaseDetail.length-1);
      purchaseDetail+="]";
      $("#purchaseDetail").val(purchaseDetail);
      var detailNum='[{"number":"'+$("#number").text()+
              '","budgetPrice":"'+$("#budgetPrice").text()+
              '","budgetMoney":"'+$("#budgetMoney").text()+
              '","purchasingPrice":"'+$("#purchasingPrice").text()+
              '","ActualMoney":"'+$("#ActualMoney").text()+'"}]';
      $("#detailNum").val(detailNum);
      return true;
    }else{
      alert("您有未填写的数据,请填写完整");
      isTable=true;
      return false;
    }

  }

  $(".detailDate").each(function(){
    $(this).datetimepicker({
      format: 'yyyy-mm-dd',
      weekStart: 1,
      autoclose: true,
      todayBtn: 'linked',
      language: 'zh-CN',
      minView:2
    });
  });


  //数字禁止输入其他字符
  $(".num").keyup(function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).bind("paste",function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).css("ime-mode", "disabled");

</script>