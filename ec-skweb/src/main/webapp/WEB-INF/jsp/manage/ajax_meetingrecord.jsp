<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>



<div id="meetingrecord_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
            <shiro:hasAnyRoles name="wechat">
                <a id="ajax_meetingrecord_btn_add" <s:property value="isCreate('meetingrecord')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建会议记录登记</a>
            </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_meetingrecord_list_row">
                    <table id="ajax_meetingrecord_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_meetingrecord_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_meetingrecord_jqGrid();
    $("#ajax_meetingrecord_list_row").width(table_global_width);
    jQuery("#ajax_meetingrecord_table").jqGrid('setGridWidth', table_global_width);

  });


  function load_meetingrecord_jqGrid(){
    jQuery("#ajax_meetingrecord_table").jqGrid({
      url:'../manage/ajax-meetingrecord!list.action?parentId='+$('#meetinguse #keyId').val(),
      datatype: "json",
      colNames:['会议记录上传日期',"会议记录上传人员","文档状态","操作","id"],
      colModel:[
        {name:'createDate',index:'createDate', width:100,sortable:true,search:true},
        {name:'creater',index:'creater_name',sortable:false,search:true,width:80},
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_meetingrecord_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        var ids=$("#ajax_meetingrecord_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_meetingrecord_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_meetingrecord_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_meetingrecord_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_meetingrecord_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_meetingrecord_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 会议记录登记",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_meetingrecord_table").jqGrid('setGridWidth', $("#ajax_meetingrecord_list_row").width());
    })
    jQuery("#ajax_meetingrecord_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_meetingrecord_table").jqGrid('navGrid', "#ajax_meetingrecord_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_meetingrecord_read(id){
    loadURL("../manage/ajax-meetingrecord!read.action?keyId="+id,$('div#s2'));
  }

  //新建会议记录登记
  $("#ajax_meetingrecord_btn_add").off("click").on("click",function(){
    var vActionUrl = "<%=path%>/manage/ajax-meetingrecord!checkUser.action";
    var data={parentId:$('#meetinguse #keyId').val()};
    ajax_action(vActionUrl,data,{},function(pdata){
      if(pdata.state == "200"){
        loadURL("../manage/ajax-meetingrecord!input.action?parentId="+$('#meetinguse #keyId').val(),$('div#s2'));
      }else{
        alert("您没有权限新建");
      }
    });

  });
</script>





















