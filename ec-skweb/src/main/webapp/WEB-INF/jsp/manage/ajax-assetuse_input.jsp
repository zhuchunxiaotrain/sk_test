<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/29
  Time: 10:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <s:if test="assetUse==null || assetUse.getProcessState().name()=='Draft'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
        </s:if>
        <s:if test="assetUse!=null && assetUse.getProcessState().name()=='Backed'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
        </s:if>
        <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
        <hr class="simple">
        <form id="assetuse" class="smart-form" novalidate="novalidate" action="" method="post">
          <input type="hidden" name="keyId" id="keyId" value="<s:property value="assetUse.id" />"/>
          <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
          <header  style="display: block;">
            资产使用登记表&nbsp;&nbsp;<span id="title"></span>
          </header>
          <fieldset>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                <span id="toPurchase">采购登记表</span>
              </label>
              <section class="col col-5">
                <label class="input">
                  <select class="form-control" name="purchaseId" id="purchaseId" >
                    <option value="">请选择</option>
                    <s:iterator value="purchaseList" id="list">
                      <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                    </s:iterator>
                  </select>
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                固定资产名称
              </label>
              <section class="col col-5">
                <label class="input">
                  <input type="text" name="name" id="name" placeholder="请输入固定资产名称" value="<s:property value="assetUse.name"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                固定资产型号
              </label>
              <section class="col col-5">
                <label class="input">
                  <input type="text" name="type" id="type" placeholder="请输入固定资产型号" value="<s:property value="assetUse.type"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                固定资产编号
              </label>
              <section class="col col-5">
                <label class="input">
                  <input class="num" type="text" name="no" id="no" placeholder="请输入固定资产编号" value="<s:property value="assetUse.no"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                采购金额
              </label>
              <section class="col col-5">
                <label class="input">
                  <input class="num" type="text" name="money" id="money" placeholder="请输入采购金额" value="<s:property value="assetUse.money"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                采购日期
              </label>
              <section class="col col-5">
                <label class="input">
                  <input type="text" name="date" id="date" placeholder="请选择采购日期" value="<s:date name="assetUse.date" format="yyyy-MM-dd"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                <a href="javascript:void(0);" key="btn-choose-department"> 使用部门</a>
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input disabled type="text" name="department" id="department" placeholder="请选择使用部门" value="<s:property value="assetUse.department.name"/>" >
                  <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="assetUse.department.id"/>"/>
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                <a href="javascript:void(0);" key="btn-choose-user">使用人员</a>
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input disabled type="text" id="user" name="user" placeholder="请选择使用人员" value="<s:property value="assetUse.user.name"/>"/>
                  <input type="hidden" id="userId" name="userId" value="<s:property value="assetUse.user.id"/>"/>
                </label>
              </section>
            </div>

          </fieldset>
        </form>
      </div>
    </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"assetuse",
      todo:"1"
    };
    multiDuty(pdata);

    //将label变成超链接
    $("#purchaseId").change(function(){
      if($("#purchaseId").val()!=null&&$("#purchaseId").val()!=""){
        $("#toPurchase").empty();
        var newA=$("<a id='toPur' href='javascript:void(0);'>查看此采购登记表</a>");
        $("#toPurchase").append(newA);
        $("#toPur").click(function(){
          loadURL("../manage/ajax-purchase!read.action?keyId="+$("#purchaseId").val()+"&assetuse=1&assetuseId="+$("#keyId").val(),$('#content'));
        });
      }else{
        $("#toPurchase").empty();
        $("#toPurchase").text("采购登记表");
      }
    });

  });

  //申请部门
  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知部门",
      url:"ajax-dialog!dept.action?key=department&more=0",
      width:560
    }).show();
  });

  //使用人员
  $("a[key=btn-choose-user]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择使用人员",
      url:"ajax-dialog!levelUser.action?key=user&more=0",
      width:560
    }).show();
  });


  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!assetuse.action",$('#content'));
    }

  });
  //校验
  $("#assetuse").validate({
    rules : {
      purchaseId : {
        required : true
      },
      name : {
        required : true
      },
      type : {
        required : true
      },
      no: {
        required : true
      },
      money: {
        required : true
      },
      date: {
        required : true
      },
      departmentId: {
        required : true
      },
      userId: {
        required : true
      }
    },
    messages : {
      purchaseId : {
        required : "请选择采购登记表"
      },
      name : {
        required : "请输入固定资产名称"
      },
      type : {
        required : "请输入固定资产型号"
      },
      no : {
        required : "请输入固定资产编号"
      },
      money:{
        required : "请输入采购金额"
      },
      date: {
        required : "请选择采购日期"
      },
      departmentId: {
        required : "请选择使用部门"
      },
      userId: {
        required : "请选择使用人员"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#date').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("assetuse","../manage/ajax-assetuse!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!assetuse.action",$('#content'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#assetuse").valid()){
              $("#areaselect_span").hide();
              return false;
            }

            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#assetuse").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("assetuse","../manage/ajax-assetuse!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!assetuse.action?viewtype=1",$('#content'));
                }
              }
            });
          }
  );

  //数字禁止输入其他字符
  $(".num").keyup(function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).bind("paste",function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).css("ime-mode", "disabled");
</script>