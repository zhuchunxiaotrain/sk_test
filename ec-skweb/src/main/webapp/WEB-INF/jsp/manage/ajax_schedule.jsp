<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
    <shiro:hasAnyRoles name="wechat">
        <a id="ajax_schedule_btn_add" <s:property value="isCreate('schedule')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建日程安排</a>
    </shiro:hasAnyRoles>
</div>
<div class="row"  id='calendar'>

</div>

<script type="text/javascript">
  $('#calendar').fullCalendar({
    theme: false,
    lang: 'zh-cn',
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultDate: '<s:date name="today" format="yyyy-MM-dd"/> ',
    editable: false,
    eventLimit: true, // allow "more" link when too many events
    events: {
      url: '<%=path%>/manage/ajax-schedule!getCalendarJson.action',
      error: function() {
      //  $('#script-warning').show();
      }
    },
    eventClick: function(calEvent, jsEvent, view) {
      loadURL("../manage/ajax-schedule!read.action?keyId="+calEvent.id,$('#content'));

    },

    loading: function(bool) {
      //$('#loading').toggle(bool);
    }
  });

  $("#ajax_schedule_btn_add").off("click").on("click",function(){
    loadURL("../manage/ajax-schedule!input.action",$('#content'));
  });
</script>

















