<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/20
  Time: 16:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>印章使用管理</a>
          <s:if test="seal==null || seal.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="seal!=null && seal.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="seal" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="seal.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              印章使用申请表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  印章使用类型
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="type" value="1" <s:property value="seal.type==1?'checked':''"/>>
                      <i></i>内用
                    </label>

                    <label class="radio">
                      <input type="radio" name="type" value="2" <s:property value="seal.type==2?'checked':''"/>>
                      <i></i>外借
                    </label>
                  </div>
                </section>
              </div>

              <div class="row" <s:if test="seal==null || seal.type==1">style="display:none"</s:if> id="showReturn">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  归还日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择归还日期" id="returnDate" name="returnDate"
                            type="text" value="<s:date name="seal.returnDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  印章名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="sealNameId" id="sealNameId" >
                      <option value="">请选择</option>
                      <s:iterator value="nameDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  盖章事由
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="reason" id="reason" placeholder="请输入盖章事由" value="<s:property value="seal.reason"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  盖章内容/份数
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="content" id="content" placeholder="请输入盖章内容/份数" value="<s:property value="seal.content"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  相关附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"seal",
      todo:"1"
    };
    multiDuty(pdata);

  });
  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });


  $(':radio[name="type"]').click(function(){
    if($(this).val()=="1") {
      $('#showReturn').hide();
    }else if($(this).val()=="2"){
      $('#showReturn').show();
    }
  });

  $(':radio[name="hasValid"]').click(function(){
    if($(this).val()=="1") {
      $('#showValid').show();
    }else{
      $('#showValid').hide();
    }
  });
  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!seal.action?step=1",$('#content'));
    }

  });
  //校验
  $("#seal").validate({
    rules : {
      type : {
        required : true
      },
      returnDate : {
        required : function(){
          if($(':checked[name="type"]').val() == "2"){
            return true;
          }else{
            return false;
          }
        }
      },
      sealNameId : {
        required : true
      },
      content : {
        required : true
      },
      reason : {
        required : true
      },
      fileIds:{
        required : true
      }
    },
    messages : {
      type : {
        required : "请选择印章使用类型"
      },
      returnDate : {
        required : "请选择归还日期"
      },
      sealNameId : {
        required : "请选择印章名称"
      },
      content : {
        required : "请输入盖章内容/份数"
      },
      reason : {
        required : "请输入盖章事由"
      },
      fileIds : {
        required : "请选择相关附件"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#returnDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("seal","../manage/ajax-seal!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!seal.action?step=1",$('#content'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#seal").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#seal").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("seal","../manage/ajax-seal!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!seal.action?step=1",$('#content'));
                }
              }
            });
          }
  );
</script>