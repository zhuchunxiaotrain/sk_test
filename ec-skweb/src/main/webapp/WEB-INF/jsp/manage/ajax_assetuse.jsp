<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/29
  Time: 9:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      资产使用登记
    </h1>
  </div>
</div>

<div id="assetuse_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <shiro:hasAnyRoles name="wechat">
              <a id="ajax_assetuse_btn_add" <s:property value="isCreate('assetuse')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建资产使用登记表</a>
              <a id="ajax_download_assetuse" <s:property value="isCreate('assetuse')"/> class="btn btn-default pull-right" href="javascript:void(0)"> <i class="fa fa-lg fa-hand-o-right"></i> 导出Excel </a>
          </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_assetuse_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_assetuse_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_assetuse_jqGrid();
  });


  function load_assetuse_jqGrid(){
    jQuery("#ajax_assetuse_table").jqGrid({
      url:'../manage/ajax-assetuse!list.action',
      datatype: "json",
      colNames:['年',"月",'资产编号',"使用人","使用部门","登记日期","文档状态","操作","id"],
      colModel:[
        {name:'year',index:'year', width:100,sortable:true,search:true,searchoptions:{sopt:['cn']}},
        {name:'month',index:'month', width:150,search:true,sortable:true,searchoptions:{sopt:['cn']}},
        {name:'no',index:'no', width:120,search:true,sortable:true,searchoptions:{sopt:['cn']}},
        {name:'creater',index:'creater',sortable:true,search:true,width:120,searchoptions:{sopt:['cn']}},
        {name:'department',index:'department',sortable:true,search:true,width:120,searchoptions:{sopt:['cn']}},
        {name:'createDate',index:'createDate',sortable:true,search:true,width:120,searchoptions:{sopt:['cn']}},
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      loadonce: true,
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_assetuse_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        $('.ui-search-oper').hide();
        var ids=$("#ajax_assetuse_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_assetuse_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_assetuse_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_assetuse_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_assetuse_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_assetuse_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 资产使用登记",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_assetuse_table").jqGrid('setGridWidth', $("#ajax_assetuse_list_row").width());
    })
    jQuery("#ajax_assetuse_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_assetuse_table").jqGrid('navGrid', "#ajax_assetuse_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_assetuse_read(id){
    loadURL("../manage/ajax-assetuse!read.action?keyId="+id,$('#content'));
  }

  $("#ajax_assetuse_btn_add").off("click").on("click",function(){
    loadURL("../manage/ajax-assetuse!input.action",$('#content'));
  });

  $('#ajax_download_assetuse').click(function(){
    location.href = "../manage/ajax-assetuse!exportExcel.action";
  });
</script>
