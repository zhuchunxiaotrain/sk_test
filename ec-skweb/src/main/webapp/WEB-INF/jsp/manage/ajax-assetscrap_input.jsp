<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/30
  Time: 10:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <s:if test="assetScrap==null || assetScrap.getProcessState().name()=='Draft'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
        </s:if>
        <s:if test="assetScrap!=null && assetScrap.getProcessState().name()=='Backed'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
        </s:if>
        <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
        <hr class="simple">
        <form id="assetscrap" class="smart-form" novalidate="novalidate" action="" method="post">
          <input type="hidden" name="keyId" id="keyId" value="<s:property value="assetScrap.id" />"/>
          <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
          <header  style="display: block;">
            资产报废登记表&nbsp;&nbsp;<span id="title"></span>
          </header>
          <fieldset>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                <span id="toPurchase">固定资产名称</span>
              </label>
              <section class="col col-5">
                <label class="input">
                  <select class="form-control" name="assetUseId" id="assetUseId" >
                    <option value="">请选择</option>
                    <s:iterator value="assetUseList" id="list">
                      <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                    </s:iterator>
                  </select>
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                资产编号
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input type="text" name="no" id="no" disabled value="<s:property value="assetScrap.assetUse.no"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                资产型号
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input type="text" name="type" id="type" disabled value="<s:property value="assetScrap.assetUse.type"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                购买日期
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input  id="date" name="date"  disabled type="text" value="<s:date name="assetScrap.assetUse.date" format="yyyy-MM-dd"/>">
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                购买金额
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input type="text" name="money" id="money" disabled value="<s:property value="assetScrap.assetUse.money"/>" >
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                报废原因
              </label>
              <section class="col col-5">
                <label class="input">
                  <select class="form-control" name="scrapReasonId" id="scrapReasonId" >
                    <option value="">请选择</option>
                    <s:iterator value="scrapReasonDict" id="list">
                      <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                    </s:iterator>
                  </select>
                </label>
              </section>
            </div>

          </fieldset>
        </form>
      </div>
    </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"assetscrap",
      todo:"1"
    };
    multiDuty(pdata);
  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!assetscrap.action?viewtype=1",$('#content'));
    }

  });
  //校验
  $("#assetscrap").validate({
    rules : {
      assetUseId : {
        required : true
      },
      scrapReasonId : {
        required : true
      }
    },
    messages : {
      assetUseId : {
        required : "请选择固定资产名称"
      },
      scrapReasonId : {
        required : "请选择报废原因"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  //选择固定资产名称后获取数据
  $("#assetUseId").change(function(){
    var useId=$(this).children('option:selected').val();
    if(useId!=null){
      $.ajax({
        url:"../manage/ajax-assetscrap!getAssetUseInfo.action",
        type:"post",
        cache:false,dataType:"json",async:true,
        data:{"assetUseId":useId},
        success:function(data){
          if(data.result.errorCode == 1){
            $("#no").val(data.data.no);
            $("#type").val(data.data.type);
            $("#date").val(data.data.date);
            $("#money").val(data.data.money);
          }
        }
      });
    }else{
      $("#no","#type","#date","#money").empty();
    }

  });

  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("assetscrap","../manage/ajax-assetscrap!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!assetscrap.action?viewtype=1",$('#content'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#assetscrap").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#assetscrap").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("assetscrap","../manage/ajax-assetscrap!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!assetscrap.action?viewtype=1",$('#content'));
                }
              }
            });
          }
  );
</script>