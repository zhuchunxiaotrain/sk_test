<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/21
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>车辆管理</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 车辆使用申请 </a>
            </li>
          </ul>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
              <div class="row">
                <shiro:hasAnyRoles name="wechat">
                  <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(car.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
                </shiro:hasAnyRoles>
              </div>
              <hr class="simple">
              <form id="car" class="smart-form" novalidate="novalidate" action="" method="post">
                <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
                <input type="hidden" name="keyId" id="keyId" value="<s:property value="car.id" />"/>
                <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                <header  style="display: block;">
                  车辆管理&nbsp;&nbsp;<span id="title"></span>
                </header>
                <fieldset>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      车辆品牌
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text" disabled placeholder="请输入车辆品牌" value="<s:property value="car.carConfig.brand"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      车辆型号
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text" disabled placeholder="请输入车辆型号" value="<s:property value="car.carConfig.type"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      车辆号牌
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text" disabled placeholder="请输入车辆号牌" value="<s:property value="car.carConfig.carNo"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      申请用途
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text" name="purpose" id="purpose" disabled placeholder="请输入申请用途" value="<s:property value="car.purpose"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      申请详细说明
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text" name="description" id="description" disabled placeholder="请输入申请详细说明" value="<s:property value="car.description"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      相关附件
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input name="uploadify" id="filename" style="display: none" placeholder="" type="file" >
                        <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      有效期
                    </label>
                    <section class="col col-2">
                      <label class="input state-disabled">
                        <input  disabled  id="dateStart" name="dateStart" type="text" value="<s:date name="car.dateStart" format="yyyy-MM-dd"/>">
                      </label>
                    </section>
                    <%--<label class="label col col-1"></label>--%>
                    <section class="col col-2">
                      <label class="input state-disabled">
                        <input  disabled  id="dateEnd" name="dateEnd" type="text" value="<s:date name="car.dateEnd" format="yyyy-MM-dd"/>">
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      当前公里数
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text" name="kilometre" id="kilometre" disabled value="<s:property value="car.kilometre"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      备注
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text" name="remarks" id="remarks" disabled value="<s:property value="car.remarks"/>" >
                      </label>
                    </section>
                  </div>

                </fieldset>

              </form>
              <div class="flow">
                <s:if test="car.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer"style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
          </div>
        </div>
      </div>
    </div>

  </article>
</div>

<script>

  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "car"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("car","../manage/ajax-car!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("car","../manage/ajax-car!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          if($("#car").valid()){
            form_save("car","../manage/ajax-car!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
              $("#btn-re-common").trigger("click");
            })
          }
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("car", "../manage/ajax-car!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "4":
        //第四步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("car", "../manage/ajax-car!approve4.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-car!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-car!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });

  //上传文件
  readLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId"
  });

  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!car.action?step=<s:property value="step"/>",$('#content'));
    }

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-car!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

</script>