<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/31
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<style>
  #dataTable{
    margin-bottom: 20px;
  }
  #data td{
    height: 30px;
    text-align: center;
  }
  #data td input[type="text"]{
    width: 100%;
    height: 100%;
    border: none;
  }
  #data td input[type="button"]{
    margin-left: 4px;
    width: 100%;
  }
</style>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <ul id="myTab1" class="nav nav-tabs bordered  ">
          <li class="active">
            <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 接待登记表 </a>
          </li>
        </ul>
        <div id="myTabContent1" class="tab-content padding-10 ">
          <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
            <hr class="simple">
            <form id="reception" class="smart-form" novalidate="novalidate" action="" method="post">
              <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
              <input type="hidden" name="keyId" id="keyId" value="<s:property value="reception.id" />"/>
              <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
              <input type="hidden" name="object" id="object" value="<s:property value="reception.object" />"/>
              <shiro:hasAnyRoles name="wechat">
                <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(reception.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
              </shiro:hasAnyRoles>
              <header  style="display: block;">
                接待登记表&nbsp;&nbsp;<span id="title"></span>
              </header>
              <fieldset>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    公函上传
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input name="uploadify" id="filename" style="display: none" placeholder="" type="file" >
                      <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    公函号
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input  type="text" name="letterNo" id="letterNo" disabled placeholder="请输入公函号" value="<s:property value="reception.letterNo"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    来宾姓名
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input  type="text" name="guestName" id="guestName" disabled placeholder="请输入来宾姓名" value="<s:property value="reception.guestName"/>" >
                    </label>
                  </section>

                  <label class="label col col-1">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    性别
                  </label>
                  <section class="col col-2">
                    <div class="inline-group state-disabled">
                      <label class="radio">
                        <input type="radio" disabled name="sex" value="1" <s:property value="reception.sex==1?'checked':''"/>>
                        <i></i>男</label>
                      <label class="radio">
                        <input type="radio" disabled name="sex" value="0" <s:property value="reception.sex==0?'checked':''"/>>
                        <i></i>女</label>
                    </div>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    来访单位
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input  type="text" name="department" id="department" disabled placeholder="请输入来访单位" value="<s:property value="reception.department"/>" >
                    </label>
                  </section>

                  <label class="label col col-1">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    职务
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="post" id="post" disabled placeholder="请输入职务" value="<s:property value="reception.post"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    主要随员
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input  type="text" name="mainAttache" id="mainAttache" disabled placeholder="请输入主要随员" value="<s:property value="reception.mainAttache"/>" >
                    </label>
                  </section>

                  <label class="label col col-1">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    人数
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="count" id="count" disabled placeholder="请输入人数" value="<s:property value="reception.count"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    主陪姓名
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input  type="text" name="mainName" id="mainName" disabled placeholder="请输入主陪姓名" value="<s:property value="reception.mainName.name"/>" >
                      <input  type="hidden" name="mainNameId" id="mainNameId" disabled value="<s:property value="reception.mainName.id"/>" >
                    </label>
                  </section>

                  <label class="label col col-1">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    其他陪同人员
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="otherPerson" id="otherPerson" disabled placeholder="请输入其他陪同人员" value="<s:property value="reception.otherPerson"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    接待事由
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="receptionReason" id="receptionReason" disabled placeholder="请输入接待事由" value="<s:property value="reception.receptionReason"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    接待时间
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input  placeholder="请选择接待时间" id="date" name="date" disabled type="text" value="<s:date name="reception.date" format="yyyy-MM-dd"/>">
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    公务内容
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="content" id="content" disabled placeholder="请输入公务内容" value="<s:property value="reception.content"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    就餐地点
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input  type="text" name="eatPlace" id="eatPlace" disabled placeholder="请输入就餐地点" value="<s:property value="reception.eatPlace"/>" >
                    </label>
                  </section>

                  <label class="label col col-1">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    就餐标准
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="eatStandard" id="eatStandard" disabled placeholder="请输入就餐标准" value="<s:property value="reception.eatStandard"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    住宿地点
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input  type="text" name="livePlace" id="livePlace" disabled placeholder="请输入住宿地点" value="<s:property value="reception.livePlace"/>" >
                    </label>
                  </section>

                  <label class="label col col-1">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    住宿标准
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="liveStandard" id="liveStandard" disabled placeholder="请输入住宿标准" value="<s:property value="reception.liveStandard"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    本次费用合计
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="money" id="money" disabled placeholder="请输入本次费用合计" value="<s:property value="reception.money"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    其他项目
                  </label>
                  <div class="col col-5">
                    <table id="dataTable" class="table" border="1" width="100%">
                      <thead>
                      <%--<tr>--%>
                        <%--<td colspan="3"><input disabled id="add" type="button" class="btn btn-primary pull-right" value="增加行" /></td>--%>
                      <%--</tr>--%>
                      <tr>
                        <td>其他项目</td>
                        <td>项目内容</td>
                        <td>金额</td>
                      </tr>
                      </thead>
                      <tbody id="data">
                      <tr>
                        <td>
                          <input class="rowData dataName" type="text" style="padding-left: 5px"/>
                        </td>
                        <td>
                          <input class="rowData dataContent" type="text" style="padding-left: 5px"/>
                        </td>
                        <td>
                          <input class="rowData dataMoney" type="text" style="padding-left: 5px"/>
                        </td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <s:if test="flowNumStatus==1">
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要上报中心
                    </label>
                    <section class="col col-5">
                      <div class="inline-group">
                        <label class="radio">
                          <input type="radio"  name="leaderExam" value="1" <s:property value="reception.ifCentralStaff==1?'checked':''"/>>
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  name="leaderExam" value="0" >
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>
                </s:if>
                <s:if test="flowNumStatus>1">
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要上报中心
                    </label>
                    <section class="col col-5">
                      <div class="inline-group state-disabled">
                        <label class="radio">
                          <input type="radio"  disabled name="leaderExam" value="1" <s:property value="reception.leaderExam==1?'checked':''"/>>
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  disabled name="leaderExam" value="0" <s:property value="reception.leaderExam==0?'checked':''"/>>
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>
                </s:if>

              </fieldset>

            </form>
            <div class="flow">
              <s:if test="reception.getProcessState().name()=='Running'">
                <div class="f_title">审批意见</div>
                <div class="chat-footer"style="margin-top:5px">
                  <div class="textarea-div">
                    <div class="typearea">
                      <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                    </div>
                  </div>
                  <span class="textarea-controls"></span>
                </div>
              </s:if>
              <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
              <div class="f_content" style="display:none">
                <div id="showFlow"></div>
              </div>
              <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
              <div class="f_content" style="display:none">
                <div id="showNext"></div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "reception"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("reception","../manage/ajax-reception!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          var $validForm = $("#reception").valid();
          if(!$validForm) return false;
          form_save("reception","../manage/ajax-reception!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("reception","../manage/ajax-reception!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
              $("#btn-re-common").trigger("click");
            })
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("reception", "../manage/ajax-reception!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-reception!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-reception!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });

    //设置表格中的内容
    var object=eval($("#object").val());
    if(object.length>0){
      $("#data").empty();
      for(var i=0;i<object.length;i++){
        var newTr=$("<tr><td>"+object[i].name+"</td>" +
        "<td>"+object[i].content+"</td>" +
        "<td>"+object[i].money+"</td></tr>");
        $("#data").append(newTr);
      }
    }

  });

  //上传文件
  readLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId"
  });

  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    var expend = "<s:property value="expend" />";
    var expendId = "<s:property value="expendId" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else if(expend==1&&expendId==""){
      loadURL("../finance/ajax-expend!input.action",$('#content'));
    }else if(expendId!=null&&expendId!=""){
      loadURL("../finance/ajax-expend!read.action?keyId="+expendId,$('#content'));
    }else{
      loadURL("../manage/ajax!reception.action?viewtype=<s:property value="viewtype"/>",$('#content'));
    }

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-reception!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

  //校验
  $("#reception").validate({
    rules : {
      leaderExam:{
        required : true
      }
    },
    messages : {
      leaderExam:{
        required : "请选择是否需要上报中心"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

</script>