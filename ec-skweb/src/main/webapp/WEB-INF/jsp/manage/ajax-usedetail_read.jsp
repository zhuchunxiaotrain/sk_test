<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/29
  Time: 16:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<s:if test="draft == 1">
  <jsp:include page="../com/ajax-top.jsp" />
</s:if>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-apply-read-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>返回
          </a>

          <shiro:hasAnyRoles name="wechat">
            <a style="margin-top: -13px !important;" <s:property value="isEdit(useDetail.id)"/> class="btn btn-default pull-right pull-right-fix" key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
          </shiro:hasAnyRoles>

          <hr class="simple">
          <form id="useDetail" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="useDetail.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="useDetail.assetUse.id" />"/>
            <header  style="display: block;">
              资产使用调拨登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  原使用部门
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" value="<s:property value="useDetail.assetUse.department.name"/>" >
                    <%--<input type="hidden" value="<s:property value="assetUse.department.id"/>"/>--%>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  原使用人员
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" value="<s:property value="useDetail.assetUse.user.name"/>" >
                    <%--<input type="hidden"  value="<s:property value="assetUse.user.id"/>"/>--%>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  现使用部门
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="department" id="department" value="<s:property value="useDetail.newDepartment.name"/>" >
                    <input type="hidden" name="departmentId" id="departmentId" value="<s:property value="useDetail.newDepartment.id"/>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  现使用人员
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="user" id="user" value="<s:property value="useDetail.newUser.name"/>" >
                    <input type="hidden" name="userId" id="userId"  value="<s:property value="useDetail.newUser.id"/>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  会签人员
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text"  name="examUsers"
                           value="<s:iterator id="list" value="useDetail.examUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden"  name="examUsersId"
                           value="<s:iterator id="list" value="useDetail.examUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
          <div class="flow" id="apply">
            <s:if test="useDetail.getProcessState().name()=='Running'">
              <div class="f_title">审批意见</div>
              <div class="chat-footer"style="margin-top:5px">
                <div class="textarea-div">
                  <div class="typearea">
                    <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                  </div>
                </div>
                <span class="textarea-controls"></span>
              </div>
            </s:if>
            <div class="f_title" name="apply"><i class="right" id="flow"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow"></div>
            </div>
            <div class="f_title" name="apply"><i class="right" id="next"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">
  $(function(){
    $("input").attr("disabled","disabled").parent().addClass(" state-disabled");
    loadURL("ajax-running!workflow.action?bussinessId="+$("#useDetail input#keyId").val()+"&type=flow",$('#apply #showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("#useDetail input#keyId").val()+"&type=next",$('#apply #showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("#useDetail input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("#useDetail input#keyId").val(),
          flowName: "usedetail"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("#useDetail  input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("useDetail","../manage/ajax-usedetail!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
            $("#btn-apply-read-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("useDetail","../manage/ajax-usedetail!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-apply-read-common").trigger("click");
          });
        });
        break;
    }

    //流程信息展开
    $('#apply #flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });

  });

  //返回视图
  $("#btn-apply-read-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));//主页
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));//个人代办
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));//提醒事项
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));//已办未结
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));//已办已结
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));//草稿
    }else{
      loadURL("../manage/ajax!usedetail.action?parentId="+$("#parentId").val(),$('div#s2'));
    }
  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-usedetail!input.action?keyId="+$("#keyId").val()+"&parentId="+$("#parentId").val()+"&draft="+draft,$('#content'));
  });

</script>