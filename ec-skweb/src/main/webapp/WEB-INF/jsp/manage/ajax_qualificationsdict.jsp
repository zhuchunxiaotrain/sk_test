<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/4/7
  Time: 16:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      资质系统字典
    </h1>
  </div>
</div>

<div id="qualificationsdict_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <shiro:hasAnyRoles name="wechat">
              <a id="ajax_qualificationsdict_btn_add" <s:property value="isCreate('qualificationsdict')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建资质信息</a>
          </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_qualificationsdict_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_qualificationsdict_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_qualificationsdict_jqGrid();
  });


  function load_qualificationsdict_jqGrid(){
    jQuery("#ajax_qualificationsdict_table").jqGrid({
      url:'../manage/ajax-qualificationsdict!list.action',
      datatype: "json",
      colNames:["资质名称","资质号","操作","id"],
      colModel:[
        {name:'name',index:'name', width:100,sortable:false,search:true},
        {name:'no',index:'no', width:100,search:true,sortable:true},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_qualificationsdict_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        var ids=$("#ajax_qualificationsdict_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_qualificationsdict_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_qualificationsdict_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_qualificationsdict_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_qualificationsdict_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_qualificationsdict_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 资质系统字典",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_qualificationsdict_table").jqGrid('setGridWidth', $("#ajax_qualificationsdict_list_row").width());
    })
    jQuery("#ajax_qualificationsdict_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_qualificationsdict_table").jqGrid('navGrid', "#ajax_qualificationsdict_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_qualificationsdict_read(id){
    loadURL("../manage/ajax-qualificationsdict!read.action?keyId="+id,$('#content'));
  }

  $("#ajax_qualificationsdict_btn_add").off("click").on("click",function(){
    loadURL("../manage/ajax-qualificationsdict!input.action",$('#content'));
  });
</script>