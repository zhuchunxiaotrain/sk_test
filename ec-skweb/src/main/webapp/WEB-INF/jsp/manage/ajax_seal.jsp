<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/20
  Time: 16:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      印章使用管理
    </h1>
  </div>
</div>

<div id="seal_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <shiro:hasAnyRoles name="wechat">
            <s:if test="step==1">
              <a id="ajax_seal_btn_add" <s:property value="isCreate('seal')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建印章使用申请表</a>
            </s:if>
            <s:if test="step==2">
              <a id="ajax_seal_btn_return" <s:property value="isCreate('seal')"/> class="btn btn-default pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-lg fa-hand-o-right"></i> 归还确认</a>
            </s:if>
          </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_seal_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_seal_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->

  <!-- 延续操作模态框 -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="width: auto">
    <div class="modal-dialog" role="document" style="width: auto">
      <div class="modal-content" style="box-shadow: none;border: none">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">归还确认</h4>
        </div>
        <div class="modal-body">
          <form id="return" class="smart-form" novalidate="novalidate" action="" method="post">

            <input type="hidden" name="keyId" id="keyId"/>

            <div class="row">
              <label class="label col col-4">
                归还日期
              </label>
              <section class="col col-7">
                <label class="input">
                  <input  placeholder="请选择归还日期" id="returnDate" name="returnDate" type="text">
                </label>
              </section>
            </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="return_close">关闭</button>
          <button type="button" class="btn btn-primary" id="return_save">保存</button>
        </div>
      </div>
    </div>
  </div>


</div>

<script type="text/javascript">

  $(function(){
    load_seal_jqGrid();
  });


  function load_seal_jqGrid(){
    jQuery("#ajax_seal_table").jqGrid({
      url:'../manage/ajax-seal!list.action?step='+"<s:property value="step" />",
      datatype: "json",
      colNames:['印章使用类型',"印章名称","申请时间","申请人"<s:if test="step==2||step==3">,"归还状态"</s:if>,"状态","操作","id"],
      colModel:[
        {name:'type',index:'type', width:100,sortable:true,search:true,searchoptions:{sopt:['cn']}},
        {name:'name',index:'name', width:150,search:true,sortable:true,searchoptions:{sopt:['cn']}},
        {name:'createDate',index:'createDate',sortable:true,search:true,width:120,searchoptions:{sopt:['cn']}},
        {name:'creater',index:'creater',sortable:true,search:true,width:120,searchoptions:{sopt:['cn']}},
        <s:if test="step==2||step==3">
        {name:'giveBack',index:'giveBack',sortable:true,search:true,width:120,searchoptions:{sopt:['cn']}},
        </s:if>
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      loadonce: true,
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_seal_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        $('.ui-search-oper').hide();
        var ids=$("#ajax_seal_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_seal_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_seal_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_seal_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_seal_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_seal_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 印章使用管理",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_seal_table").jqGrid('setGridWidth', $("#ajax_seal_list_row").width());
    })
    jQuery("#ajax_seal_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_seal_table").jqGrid('navGrid', "#ajax_seal_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  $('#returnDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //校验
  $("#return").validate({
    rules : {
      returnDate : {
        required : true
      }
    },
    messages : {
      returnDate : {
        required : "请选择归还日期"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  function fn_seal_read(id){
    loadURL("../manage/ajax-seal!read.action?keyId="+id+"&step="+"<s:property value="step" />",$('#content'));
  }

  $("#ajax_seal_btn_add").off("click").on("click",function(){
    loadURL("../manage/ajax-seal!input.action",$('#content'));
  });

  //显示归还确认模态框
  $("#ajax_seal_btn_return").off("click").on("click",function(){
    var ids=$("#ajax_seal_table").jqGrid('getGridParam','selarrrow');
    if(ids.length != 1){
      alert("请选择一条印章使用登记表");
      return false;
    }
    $("#keyId").val(ids);
  });

  //执行归还确认操作
  $("#return_save").off("click").on("click",function(){
    var $validForm = $("#return").valid();
    if(!$validForm) return false;
    form_save("return","../manage/ajax-seal!giveBack.action",null,function(data){
      if(data.state==200){
        $("#return_close").trigger('click');
        jQuery("#ajax_seal_table").trigger("reloadGrid");
      }
    });
  });
</script>