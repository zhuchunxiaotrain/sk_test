
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="taskcontrol==null || taskcontrol.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="taskcontrol!=null && taskcontrol.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="taskcontrol" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="taskcontrol.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              任务督办单&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  任务编号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="taskNo" id="taskNo" placeholder="请输入任务编号" value="<s:property value="taskcontrol.taskNo"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  任务主题
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="topic" id="topic" placeholder="请输入任务主题" value="<s:property value="taskcontrol.topic"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  详细要求
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="demand" id="demand" placeholder="请输入详细要求" value="<s:property value="taskcontrol.demand"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  任务难度系数
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="degree" value="一星" <s:property value='taskcontrol.degree=="一星"?"checked":""'/>>
                      <i></i>一星</label>
                    <label class="radio">
                      <input type="radio"  name="degree" value="二星" <s:property value='taskcontrol.degree=="二星"?"checked":""'/>>
                      <i></i>二星</label>
                    <label class="radio">
                      <input type="radio"  name="degree" value="三星" <s:property value='taskcontrol.degree=="三星"?"checked":""'/>>
                      <i></i>三星</label>
                    <span style="color:red;line-height:24px">提示：星数越大难度越高</span>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  相关附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-dutyMans">任务责任人</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="dutyMans" name="dutyMans"
                           value="<s:iterator id="list" value="taskcontrol.dutyMans"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="dutyMansId" name="dutyMansId"
                           value="<s:iterator id="list" value="taskcontrol.dutyMans"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-dealDepartment">会办部门</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="dealDepartment" name="dealDepartment"
                           value="<s:iterator id="list" value="taskcontrol.dealDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="dealDepartmentId" name="dealDepartmentId"
                           value="<s:iterator id="list" value="taskcontrol.dealDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  要求完成时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择要求完成时间" id="planFinishDate" name="planFinishDate"
                            type="text" value="<s:date name="taskcontrol.planFinishDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="taskcontrol.remark"/>" >
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"taskcontrol",
      todo:"1"
    };
    multiDuty(pdata);

  });
  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });



  //任务责任人
  $("a[key=btn-choose-dutyMans]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择任务责任人",
      url:"ajax-dialog!levelUser.action?key=dutyMans&more=1",
      width:560
    }).show();
  });
  //会办部门
  $("a[key=btn-choose-dealDepartment]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择会办部门",
      url:"ajax-dialog!dept.action?key=dealDepartment&more=1",
      width:560
    }).show();
  });


  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!taskcontrol.action?viewtype=1",$('#content'));
    }

  });
  //校验
  $("#taskcontrol").validate({
    rules : {
      topic : {
        required : true
      },
      demand : {
        required : true
      },
      degree : {
        required : true
      },
      fileIds:{
        required:true
      },
      dutyMansId:{
        required:true
      },
      dealDepartmentId:{
        required:true
      },
      planFinishDate:{
        required:true
      }
    },
    messages : {
      topic : {
        required : "请输入主题"
      },
      demand : {
        required : "请输入详细要求"
      },
      degree : {
        required : "请选择任务难度系数"
      },
      fileIds:{
        required: "请上传相关附件"
      },
      dutyMansId:{
        required: "请选择任务责任人"
      },
      dealDepartmentId:{
        required:"请选择会办部门"
      },
      planFinishDate:{
        required:"请选择要求完成时间"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#planFinishDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("taskcontrol","../manage/ajax-taskcontrol!save.action");
          if(draft == 1){
            location.href='index.action';
          }else{
            loadURL("../manage/ajax!taskcontrol.action?viewtype=1",$('#content'));
          }
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#taskcontrol").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#taskcontrol").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("taskcontrol","../manage/ajax-taskcontrol!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!taskcontrol.action?viewtype=1",$('#content'));
                }
              }
            });
          }
  );
</script>
