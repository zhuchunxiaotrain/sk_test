<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58  
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="meetinguse==null || meetinguse.getProcessState().name()=='Draft' || update == \"1\"">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="meetinguse!=null && meetinguse.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <s:if test='update != "1"'>
            <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          </s:if>
          <hr class="simple">
          <form id="meetinguse" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="meetinguse.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              会议室使用登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请会议室
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="meetnamedictId" id="meetnamedictId" >
                      <option value="">请选择</option>
                      <s:iterator value="meetnameDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  会议类型
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="typedictId" id="typedictId" >
                      <option value="">请选择</option>
                      <s:iterator value="typeDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  会议议题
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="topic" id="topic" placeholder="请输入会议议题" value="<s:property value="meetinguse.topic"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  预计开始时间
                </label>
                <section class="col col-2">
                  <label class="input">
                    <input  placeholder="预计开始时间" id="begintime" name="begintime"
                            type="text" value="<s:date name="meetinguse.begintime" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  预计结束时间
                </label>
                <section class="col col-2">
                  <label class="input">
                    <input  placeholder="预计结束时间" id="endtime" name="endtime"
                            type="text" value="<s:date name="meetinguse.endtime" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                  <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      <a href="javascript:void(0);" key="btn-choose-joinUsers">参会人员</a>
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input disabled type="text" id="joinUsers" name="joinUsers"
                             value="<s:iterator id="list" value="meetinguse.joinUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden" id="joinUsersId" name="joinUsersId"
                             value="<s:iterator id="list" value="meetinguse.joinUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="meetinguse.remark"/>" >
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"meetinguse",
      todo:"1"
    };
    multiDuty(pdata);

  });



  //参会人员
  $("a[key=btn-choose-joinUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择参会人员",
      url:"ajax-dialog!levelUser.action?key=joinUsers&more=1",
      width:560
    }).show();
  });


  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!meetinguse.action?viewtype=1",$('#content'));
    }

  });
  //校验
  $("#meetinguse").validate({
    rules : {
      meetnamedictId : {
        required : true
      },
      typedictId : {
        required : true
      },
      title : {
        required : true
      },
      begintime : {
        required : true,
        remote:{type:"post",
          url:"../manage/ajax-meetinguse!checkBeginTime.action",
          data:{
            begintime:function(){return $("#begintime").val();},
            keyId:function(){return $("#keyId").val();}
          }
        }
      },
      endtime:{
        required : true,
        remote:{type:"post",
          url:"../manage/ajax-meetinguse!checkEndTime.action",
          data:{
            endtime:function(){return $("#endtime").val();},
            keyId:function(){return $("#keyId").val();}
          }
        }
      },
      joinUsersId: {
        required : true
      }
    },
    messages : {
      meetnamedictId : {
        required : "请选择申请会议室"
      },
      typedictId : {
        required : "请选择会议类型"
      },
      title : {
        required : "请选择会议议题"
      },
      begintime : {
        required : "请选择预计开始时间",
        remote: '预计开始时间与待使用中的会议有冲突'
      },
      endtime:{
        required : "请选择预计结束时间",
        remote: '预计结束时间与待使用中的会议有冲突'
      },
      joinUsersId: {
        required : "请选择参会人员"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#begintime').datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN'
  });
  $('#endtime').datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN'
  });
  //保存
  $("#btn-save-common").click(
        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("meetinguse","../manage/ajax-meetinguse!save.action");
          if(draft == 1){
            location.href='index.action';
          }else{
            loadURL("../manage/ajax!meetinguse.action?viewtype=1",$('#content'));
          }
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#meetinguse").valid()){
              $("#areaselect_span").hide();
              return false;
            }

            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#meetinguse").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                if("<s:property value="update"/>" == "1"){
                  form_save("meetinguse","../manage/ajax-meetinguse!update.action");
                }else{
                  form_save("meetinguse","../manage/ajax-meetinguse!commit.action");
                }
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!meetinguse.action?viewtype=1",$('#content'));
                }
              }
            });
          }
  );
</script>
