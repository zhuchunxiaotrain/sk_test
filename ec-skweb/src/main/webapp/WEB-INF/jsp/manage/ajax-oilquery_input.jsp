<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/22
  Time: 17:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common-query" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>加油明细</a>
          <s:if test="oilQuery==null || oilQuery.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="oilquery" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="oilQuery.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="text" name="parentId" id="parentId" value="<s:property value="parentId"/>" />
            <input type="hidden" id="isUse" value=""/>
            <header  style="display: block;">
              车辆加油明细登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  当前里程数
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input class="num" type="text" name="oilQueryKilometre" id="oilQueryKilometre" placeholder="请输入当前里程数" value="<s:property value="oilQuery.kilometre"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  加油时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择加油时间" id="oilQueryDate" name="oilQueryDate" type="text" value="<s:date name="oilQuery.oilDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  加油型号
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="oilQueryType" value="1" <s:property value="oilQuery.type==1?'checked':''"/>>
                      <i></i>#92
                    </label>
                    <label class="radio">
                      <input type="radio" name="oilQueryType" value="2" <s:property value="oilQuery.type==2?'checked':''"/>>
                      <i></i>#95
                    </label>
                    <label class="radio">
                      <input type="radio" name="oilQueryType" value="2" <s:property value="oilQuery.type==2?'checked':''"/>>
                      <i></i>#98
                    </label>
                  </div>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  加油金额
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input class="num" type="text" name="oilQueryMoney" id="oilQueryMoney" placeholder="请输入加油金额" value="<s:property value="oilQuery.oilMoney"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  累计至本次加油金额
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input disabled type="text" name="oilQueryMoneyNum" id="oilQueryMoneyNum" >
                    <input disabled type="hidden" name="moneyHelp" id="moneyHelp" value="<s:property value="moneyHelp"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="oilQueryRemarks" id="oilQueryRemarks" placeholder="请输入备注" value="<s:property value="oilQuery.remarks"/>" >
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>

</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"oilquery",
      todo:"1"
    };
    multiDuty(pdata);

    var oilThisMoney=0;
    if($("#oilQueryMoney").val()!=""){
      var oilThisMoney=$("#oilQueryMoney").val();
    }

    $("#oilQueryMoneyNum").val(parseFloat(oilThisMoney)+parseFloat($("#moneyHelp").val()));
    var thisMoney=$("#oilQueryMoney").val();
    if(thisMoney==null||thisMoney==""){
      thisMoney=0;
    }
    var moneyHelp=thisMoney+$("#moneyHelp").val();
//    $("#moneyHelp").val(moneyHelp);
//    $("#oilQueryMoneyNum").val($("#moneyHelp").val());
  });

  $("#oilQueryMoney").keyup(function(){
    var nowMoney=parseFloat($("#oilQueryMoney").val())+parseFloat($("#moneyHelp").val());
    $("#oilQueryMoneyNum").val(nowMoney);
    if($("#oilQueryMoney").val()==""){
      <%--var oldMoney="<s:property value="oilQuery.oilMoneyNum" />";--%>
      <%--if(oldMoney==null){--%>
        <%--oldMoney=0;--%>
      <%--}--%>
      $("#oilQueryMoneyNum").val($("#moneyHelp").val());
    }
  });

  //返回视图
  $("#btn-re-common-query").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!oilquery.action?parentId="+$("#parentId").val(),$('#s2'));
    }

  });
  //校验
  $("#oilquery").validate({
    rules : {
      oilQueryKilometre : {
        required : true
      },
      oilQueryDate : {
        required : true
      },
      oilQueryType : {
        required : true
      },
      oilQueryMoney : {
        required : true
      }
    },
    messages : {
      oilQueryKilometre : {
        required : "请输入当前里程数"
      },
      oilQueryDate : {
        required : "请选择加油时间"
      },
      oilQueryType : {
        required : "请选择加油型号"
      },
      oilQueryMoney : {
        required : "请输入加油金额"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#oilQueryDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("oilquery","../manage/ajax-oilquery!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!oilquery.action?parentId="+$("#parentId").val(),$('#s2'));
            }
          }
  );


  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#oilquery").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#oilquery").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("oilquery","../manage/ajax-oilquery!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!oilquery.action?parentId="+$("#parentId").val(),$('#s2'));
                }
              }
            });
          }
  );

  //数字禁止输入其他字符
  $(".num").keyup(function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).bind("paste",function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).css("ime-mode", "disabled");
</script>