<%-- 
  Created by IntelliJ IDEA.
  User: dqf 
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      会议室使用管理
    </h1>
  </div>
</div>

<div id="meetinguse_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
            <shiro:hasAnyRoles name="wechat">
              <s:if test="viewtype==1">
                <a id="ajax_meetinguse_btn_add" <s:property value="isCreate('meetinguse')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建会议室使用登记表</a>
                <a id="ajax_meetingsign_btn_add" <s:property value="isCreate('meetingsign')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 会议签到</a>
                <a id="ajax_meetingleave_btn_add" <s:property value="isCreate('meetingleave')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 会议请假</a>
                <a id="ajax_meetinguse_update"  class="btn btn-default pull-right" href="javascript:void(0)"> <i class="fa fa-lg fa-hand-o-right"></i> 会议调整 </a>
                <a id="ajax_meetinguse_cancel"  <s:property value="isCreate('meetingcancel')"/> class="btn btn-default pull-right" href="javascript:void(0)"> <i class="fa fa-lg fa-hand-o-right"></i> 会议取消 </a>
              </s:if>
            </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_meetinguse_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_meetinguse_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->

    <div class="smart-form row" id="dialog-form1" title="会议请假" style="display:none;margin-top:10px;overflow-y:hidden">
      <label class="label col col-3">
        <i class="fa fa-asterisk txt-color-red"></i>
        请假事由
      </label>
      <section class="col col-8">
        <label class="input">
          <input type="text" name="reason" id="reason" placeholder="请输入请假事由"  >
        </label>
      </section>
     </div>
<input type="hidden" id="leaveids" />
</div>

<script type="text/javascript">
  $(function(){
    load_meetinguse_jqGrid();
    //新建会议室使用登记表
    $("#ajax_meetinguse_btn_add").off("click").on("click",function(){
      loadURL("../manage/ajax-meetinguse!input.action",$('#content'));
    });
    //会议签到
    $("#ajax_meetingsign_btn_add").off("click").on("click",function(){
      var ids=$("#ajax_meetinguse_table").jqGrid('getGridParam','selarrrow');
      if(ids.length != 1){
        alert("请选择一条会议室使用登记表");
        return false;
      }
      $.tzDialog({title:"提示",content:"已经达到会议现场，准备签到?",callback:function(){
          var vActionUrl = "<%=path%>/manage/ajax-meetingsign!checkJoinUsers.action";
          var data={parentId:ids[0]};
          ajax_action(vActionUrl,data,{},function(pdata){
            if(pdata.state == "200"){
              var url = "<%=path%>/manage/ajax-meetingsign!commit.action";
              var data={parentId:ids[0]};
              ajax_action(url,data,{},function(pdata){
                _show(pdata);
              });
            }else{
              alert(pdata.message);
            }
          });
      }});

    });

    //会议请假
    $("#ajax_meetingleave_btn_add").off("click").on("click",function(){
      var leaveids = $("#ajax_meetinguse_table").jqGrid('getGridParam','selarrrow');
      if(leaveids.length != 1){
        alert("请选择一条会议室使用登记表");
        return false;
      }
      $('#leaveids').val(leaveids[0]);
      $( "#dialog-form1" ).dialog( "open" );
      // loadURL("../manage/ajax-meetingsign!commit.action?parentId="+$('#meetinguse #keyId').val(),$('#content'));
    });

    $( "#dialog-form1" ).dialog({
      autoOpen: false,
      height: 170,
      width: 450,
      modal: true,
      buttons: {
        "确定": function() {
          var reason = $('#reason').val();
          var parentId = $('#leaveids').val();
          var vActionUrl = "<%=path%>/manage/ajax-meetingleave!checkJoinUsers.action";
          var data={parentId:parentId};
          ajax_action(vActionUrl,data,{},function(pdata){
              if(pdata.state == "200"){
                var url = "<%=path%>/manage/ajax-meetingleave!commit.action";
                var data={parentId:parentId, reason:reason};
                ajax_action(url,data,{},function(pdata){
                  _show(pdata);
                });
              }else{
                alert(pdata.message);
              }
          });
          $( this ).dialog( "close" );
        },
        "取消": function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
      }
    });
    //会议取消
    $('#ajax_meetinguse_cancel').click(function(){
      var ids=$("#ajax_meetinguse_table").jqGrid('getGridParam','selarrrow');
      if(ids.length != 1){
        alert("请选择一条会议室使用登记表");
        return false;
      }
      $.tzDialog({title:"提示",content:"确认要将此会议室使用登记表取消吗?",callback:function(){
        var vActionUrl = "<%=path%>/manage/ajax-meetinguse!doInvalid.action";
        var data={keyId:ids[0]};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          jQuery("#ajax_meetinguse_table").trigger("reloadGrid");
        });
      }});
    });

    //会议调整
    $('#ajax_meetinguse_update').click(function(){
      var ids=$("#ajax_meetinguse_table").jqGrid('getGridParam','selarrrow');
      if(ids.length != 1){
        alert("请选择一条会议室使用登记表");
        return false;
      }
      var url = "<%=path%>/manage/ajax-meetinguse!checkCreater.action";
      var data={keyId:ids[0]};
      ajax_action(url,data,{},function(pdata){
          if(pdata.state == "200"){
            loadURL("../manage/ajax-meetinguse!input.action?keyId="+ids[0]+"&update=1",$('#content'));
          }else{
            alert(pdata.message);
          }
      });

    });
  });


  function load_meetinguse_jqGrid(){
    jQuery("#ajax_meetinguse_table").jqGrid({
      url:'../manage/ajax-meetinguse!list.action?viewtype='+"<s:property value="viewtype" />",
      datatype: "json",
      colNames:['会议类型',"会议议题",'申请会议室',"会议时间","文档状态","操作","id"],
      colModel:[
        {name:'type',index:'type_name', width:100,sortable:false,search:true},
        {name:'topic',index:'topic', width:150,search:true,sortable:true},
        {name:'meetname',index:'meetname_name', width:250,search:true,sortable:false},
        {name:'begintime',index:'begintime',sortable:false,search:true,width:120},
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      loadonce: true,
      pager : '#ajax_meetinguse_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        var ids=$("#ajax_meetinguse_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_meetinguse_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_meetinguse_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_meetinguse_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_meetinguse_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_meetinguse_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 会议室使用管理",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_meetinguse_table").jqGrid('setGridWidth', $("#ajax_meetinguse_list_row").width());
    })
    jQuery("#ajax_meetinguse_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_meetinguse_table").jqGrid('navGrid', "#ajax_meetinguse_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_meetinguse_read(id){
    loadURL("../manage/ajax-meetinguse!read.action?keyId="+id+"&viewtype="+"<s:property value="viewtype" />",$('#content'));
  }






</script>





















