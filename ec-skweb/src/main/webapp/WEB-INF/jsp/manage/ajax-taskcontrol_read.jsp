
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%> 
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 任务督办单 </a>
            </li>
            <li class="disabled">
              <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 任务反馈 </a>
            </li>
          </ul>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
          <hr class="simple">
          <form id="taskcontrol" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="taskcontrol.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <shiro:hasAnyRoles name="wechat">
              <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(taskcontrol.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>
            <header  style="display: block;">
              任务督办单&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  任务编号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="taskNo" id="taskNo" placeholder="请输入任务编号" value="<s:property value="taskcontrol.taskNo"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  任务主题
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="topic" id="topic" placeholder="请输入任务主题" value="<s:property value="taskcontrol.topic"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  详细要求
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text" name="demand" id="demand" placeholder="请输入详细要求" value="<s:property value="taskcontrol.demand"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  任务难度系数
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text" name="degree"  placeholder="请输入任务难度系数" value="<s:property value="taskcontrol.degree"/>" >
                  </label>

                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  相关附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" style="display: none">
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  任务责任人
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="dutyMans" name="dutyMans"
                           value="<s:iterator id="list" value="taskcontrol.dutyMans"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="dutyMansId" name="dutyMansId"
                           value="<s:iterator id="list" value="taskcontrol.dutyMans"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  会办部门
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="dealDepartment" name="dealDepartment"
                           value="<s:iterator id="list" value="taskcontrol.dealDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="dealDepartmentId" name="dealDepartmentId"
                           value="<s:iterator id="list" value="taskcontrol.dealDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  要求完成时间
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled placeholder="请选择要求完成时间" id="planFinishDate" name="planFinishDate"
                            type="text" value="<s:date name="taskcontrol.planFinishDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  disabled type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="taskcontrol.remark"/>" >
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
              <div class="flow">
                <s:if test="taskcontrol.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer" style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
          </div>
        </div>
    </div>

  </article>
</div>

<script>
  var state = "<s:property value="taskcontrol.getProcessState().name()" />";
  if(state == "Finished"){
    $('#other1').parent('li').removeClass('disabled');
  }
  var table_global_width=0;
  $("a#other1").off("click").on("click",function(e) {
    if(state == "Finished"){
      if (table_global_width == 0) {
        table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
      }
      loadURL("../manage/ajax!taskback.action?parentId="+$("#keyId").val(),$('div#s2'));
    }else{
      return false;
    }

  });

  //上传文件
  readLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId"
  });


  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "taskcontrol"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("taskcontrol","../manage/ajax-taskcontrol!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("taskcontrol","../manage/ajax-taskcontrol!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;

    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-taskcontrol!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-taskcontrol!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });
  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!taskcontrol.action?viewtype=<s:property value="viewtype"/>",$('#content'));
    }

  });
  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-taskcontrol!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });
</script>
