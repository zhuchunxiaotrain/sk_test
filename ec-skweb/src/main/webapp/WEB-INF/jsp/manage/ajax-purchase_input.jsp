<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/27
  Time: 13:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<style>
  #detail td{
    width: 80px;
    text-align: center;
    padding: 2px 0px;
  }
  table input{
    border:none;
    height: 100%;
    width: 80px;
    padding: 0 0 0 20px;
  }
  #detail{
    margin-bottom: 20px;
  }
</style>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>资产采购</a>
          <s:if test="purchase==null || purchase.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="purchase!=null && purchase.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="purchase" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="purchase.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="purchaseDetail" id="purchaseDetail" value="<s:property value="purchase.purchaseDetail" />"/>
            <input type="hidden" name="detailNum" id="detailNum" value="<s:property value="purchase.detailNum" />"/>
            <header  style="display: block;">
              资产采购登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  资产类别
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="assetType" value="1" <s:property value="purchase.assetType==1?'checked':''"/>>
                      <i></i>低值易耗品
                    </label>
                    <label class="radio">
                      <input type="radio" name="assetType" value="2" <s:property value="purchase.assetType==2?'checked':''"/>>
                      <i></i>固定资产
                    </label>
                  </div>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  采购类别
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="purchaseType" value="1" <s:property value="purchase.purchaseType==1?'checked':''"/>>
                      <i></i>公开招标
                    </label>
                    <label class="radio">
                      <input type="radio" name="purchaseType" value="2" <s:property value="purchase.purchaseType==2?'checked':''"/>>
                      <i></i>邀请招标
                    </label>
                    <label class="radio">
                      <input type="radio" name="purchaseType" value="3" <s:property value="purchase.purchaseType==3?'checked':''"/>>
                      <i></i>内部招标
                    </label>
                    <label class="radio">
                      <input type="radio" name="purchaseType" value="4" <s:property value="purchase.purchaseType==4?'checked':''"/>>
                      <i></i>竞争性磋商
                    </label>
                    <label class="radio">
                      <input type="radio" name="purchaseType" value="5" <s:property value="purchase.purchaseType==5?'checked':''"/>>
                      <i></i>单一来源
                    </label>
                    <label class="radio">
                      <input type="radio" name="purchaseType" value="6" <s:property value="purchase.purchaseType==6?'checked':''"/>>
                      <i></i>询价
                    </label>
                  </div>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-department"> 申请部门</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="department" id="department" placeholder="请选择申请部门" value="<s:property value="purchase.department.name"/>" >
                    <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="purchase.department.id"/>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  &nbsp;
                </label>
                <div class="col col-9">
                  <table class="table" border="1" id="detail" style="width: 100%">
                    <thead class="">
                      <tr>
                        <td colspan="11"><button id="add" class="btn btn-primary pull-right" type="button" style="width:100px;">添加</button></td>
                      </tr>
                      <tr>
                        <td>序号</td>
                        <td>项目名称</td>
                        <td>数量</td>
                        <td>预算编号</td>
                        <td>预算单价</td>
                        <td>预算金额</td>
                        <%--<td>采购单价</td>--%>
                        <%--<td>实际金额</td>--%>
                        <%--<td>采购日期</td>--%>
                        <td>备注</td>
                        <td>操作</td>
                      </tr>
                    </thead>
                    <tbody id="detailContent" class="" style="width: 50%">
                      <tr>
                        <td class="no" style="padding: 0 20px">1</td>
                        <td><input class="detailText detailName" type="text"></td>
                        <td><input class="detailText number num" type="text"></td>
                        <td><input class="detailText detailNo num" type="text"></td>
                        <td><input class="detailText budgetPrice num" type="text"></td>
                        <td><input class="detailText budgetMoney num" type="text"></td>
                        <%--<td><input class="detailText purchasingPrice num" type="text"></td>--%>
                        <%--<td><input class="detailText ActualMoney num" type="text"></td>--%>
                        <%--<td><input class="detailText detailDate" type="text"></td>--%>
                        <td><input class="detailText detailRemarks" type="text"></td>
                        <td><button disabled class="del btn btn-primary" type="button" style="width:100px;">删除</button></td>
                      </tr>
                    </tbody>
                    <tr>
                      <td colspan="2" style="padding: 8px 0;text-align: center">金额合计</td>
                      <td id="number">0</td>
                      <td>&nbsp;</td>
                      <td id="budgetPrice">0</td>
                      <td id="budgetMoney">0</td>
                      <%--<td id="purchasingPrice">0</td>--%>
                      <%--<td id="ActualMoney">0</td>--%>
                      <%--<td>&nbsp;</td>--%>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>
              </div>

              <div class="row">
                <label class="label col col-2">
                  其他要说明的情况
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="other" id="other" placeholder="请输入其他要说明的情况" value="<s:property value="purchase.other"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  支付方式
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio" name="pay" value="1" <s:property value="purchase.pay==1?'checked':''"/>>
                      <i></i>转账
                    </label>
                    <label class="radio">
                      <input type="radio" name="pay" value="2" <s:property value="purchase.pay==2?'checked':''"/>>
                      <i></i>现金
                    </label>
                  </div>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  收款方名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="toName" id="toName" placeholder="请输入收款方名称" value="<s:property value="purchase.toName"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  收款方账号
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input class="num" type="text" name="toNo" id="toNo" placeholder="请输入收款方账号" value="<s:property value="purchase.toNo"/>" >
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var isAdd=true;
  $("#add").click(function(){
    $(".detailText").each(function(){
      if($(this).val()==null||$(this).val()==""){
        isAdd=false;
      }
    });
   if(isAdd){
     var no=parseInt($(".detailName").size())+1;
     $(".del").removeAttr("disabled");
     var newTr=$("<tr><td class='no' style='padding: 0 20px'>"+no+"</td>" +
     "<td><input class='detailText detailName' type='text'></td>" +
     "<td><input class='detailText number num' type='text'></td>" +
     "<td><input class='detailText detailNo' type='text'></td>" +
     "<td><input class='detailText budgetPrice num ' type='text'></td>" +
     "<td><input class='detailText budgetMoney num' type='text'></td>" +
//     "<td><input class='detailText purchasingPrice num' type='text'></td>" +
//     "<td><input class='detailText ActualMoney num' type='text'></td>" +
//     "<td><input class='detailText detailDate' type='text'></td>" +
     "<td><input class='detailText detailRemarks' type='text'></td>" +
     "<td><button class='del btn btn-primary' type='button' style='width:100px;'>删除</button></td></tr>");
     $("#detailContent").append(newTr);

     $(".number,.budgetPrice,.budgetMoney,.purchasingPrice,.ActualMoney").keyup(function(){
       calculation();
     });

     $(".num").keyup(function(){
       $(this).val($(this).val().replace(/[^0-9.]/g,''));
     }).bind("paste",function(){
       $(this).val($(this).val().replace(/[^0-9.]/g,''));
     }).css("ime-mode", "disabled");

     $(".detailDate").each(function(){
       $(this).datetimepicker({
         format: 'yyyy-mm-dd',
         weekStart: 1,
         autoclose: true,
         todayBtn: 'linked',
         language: 'zh-CN',
         minView:2
       });
     });

     $(".del").click(function(){
       $(this).parent().parent().remove();
       if($("#detailContent tr").size()==1){
         $(".del").attr("disabled","disabled");
       }
       calculation();
     });

   }else{
     alert("请填写完整");
     isAdd=true;
   }
  });

  $(".number,.budgetPrice,.budgetMoney,.purchasingPrice,.ActualMoney").keyup(function(){
    calculation();
  });

  //计算
  function calculation(){
    var number=0;
    for(var i=0;i<$(".number").size();i++){
      if(isNaN(parseInt($(".number")[i].value))){
        number+=0;
      }else{
        number+=parseInt($(".number")[i].value);
      }
    }
    $("#number").text(number);

    var budgetPrice=0;
    for(var i=0;i<$(".budgetPrice").size();i++){
      if(isNaN(parseInt($(".budgetPrice")[i].value))){
        budgetPrice+=0;
      }else{
        budgetPrice+=parseInt($(".budgetPrice")[i].value);
      }

    }
    $("#budgetPrice").text(budgetPrice);

    var budgetMoney=0;
    for(var i=0;i<$(".budgetMoney").size();i++){
      if(isNaN(parseInt($(".budgetMoney")[i].value))){
        budgetMoney+=0;
      }else{
        budgetMoney+=parseInt($(".budgetMoney")[i].value);
      }
    }
    $("#budgetMoney").text(budgetMoney);

//    var purchasingPrice=0;
//    for(var i=0;i<$(".purchasingPrice").size();i++){
//      if(isNaN(parseInt($(".purchasingPrice")[i].value))){
//        purchasingPrice+=0;
//      }else{
//        purchasingPrice+=parseInt($(".purchasingPrice")[i].value);
//      }
//    }
//    $("#purchasingPrice").text(purchasingPrice);
//
//    var ActualMoney=0;
//    for(var i=0;i<$(".ActualMoney").size();i++){
//      if(isNaN(parseInt($(".ActualMoney")[i].value))){
//        ActualMoney+=0;
//      }else{
//        ActualMoney+=parseInt($(".ActualMoney")[i].value);
//      }
//    }
//    $("#ActualMoney").text(ActualMoney);

    for(var i=0;i<$(".detailName").size();i++){
      $(".no")[i].innerText=i+1;
    }
  }





  var draft = "<s:property value="draft" />";
  function getDefaultDept(data){
    $('#department').val(data.department);
    $('#departmentId').val(data.departmentId);
  }
  var time1 = setInterval("checkDuty()",1000);
  function checkDuty(){
    var curDutyId = $('#curDutyId').val();
    if(curDutyId != ""){
      clearInterval(time1);
      var vActionUrl = '../manage/ajax-purchase!getDefaultDept.action?curDutyId='+curDutyId;
      ajax_action(vActionUrl,null,null,getDefaultDept);
    }
  }

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"purchase",
      todo:"1"
    };
    multiDuty(pdata);
    $("#detail").width($("#other").width());

    if($("#purchaseDetail").val()!=null&&$("#purchaseDetail").val()!=""){
      var purchaseDetail=eval($("#purchaseDetail").val());
      $("#detailContent").empty();
      for(var i=0;i<purchaseDetail.length;i++){
        var j=i+1;
        var newTr=$("<tr><td class='no' style='padding: 0 20px'>"+j+"</td>" +
        "<td><input class='detailText detailName' value='"+purchaseDetail[i].name+"' type='text'></td>" +
        "<td><input class='detailText number num' value='"+purchaseDetail[i].number+"' type='text'></td>" +
        "<td><input class='detailText detailNo' value='"+purchaseDetail[i].detailNo+"' type='text'></td>" +
        "<td><input class='detailText budgetPrice num' value='"+purchaseDetail[i].budgetPrice+"' type='text'></td>" +
        "<td><input class='detailText budgetMoney num' value='"+purchaseDetail[i].budgetMoney+"'  type='text'></td>" +
//        "<td><input class='detailText purchasingPrice num' value='"+purchaseDetail[i].purchasingPrice+"'  type='text'></td>" +
//        "<td><input class='detailText ActualMoney num'  value='"+purchaseDetail[i].ActualMoney+"' type='text'></td>" +
//        "<td><input class='detailText detailDate' value='"+purchaseDetail[i].detailDate+"' type='text'></td>" +
        "<td><input class='detailText detailRemarks' value='"+purchaseDetail[i].detailRemarks+"' type='text'></td>" +
        "<td><button class='del btn btn-primary' type='button' style='width:100px;'>删除</button></td></tr>");
        $("#detailContent").append(newTr);
      }
    }
    if($("#detailNum").val()!=null&&$("#detailNum").val()!=""){
      var detailNum=eval($("#detailNum").val());
      $("#number").text(detailNum[0].number);
      $("#budgetPrice").text(detailNum[0].budgetPrice);
      $("#budgetMoney").text(detailNum[0].budgetMoney);
//      $("#purchasingPrice").text(detailNum[0].purchasingPrice);
//      $("#ActualMoney").text(detailNum[0].ActualMoney);
    }

    $(".del").click(function(){
      $(this).parent().parent().remove();
      if($("#detailContent tr").size()==1){
        $(".del").attr("disabled","disabled");
      }
      calculation();
    });
  });

  //申请部门
  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知部门",
      url:"ajax-dialog!dept.action?key=department&more=0",
      width:560
    }).show();
  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!purchase.action?step=1",$('#content'));
    }
  });

  //校验
  $("#purchase").validate({
    rules : {
      assetType : {
        required : true
      },
      purchaseType : {
        required : true
      },
      departmentId : {
        required : true
      },
      pay : {
        required : true
      },
      toName:{
        required : true
      },
      toNo: {
        required : true
      }
    },
    messages : {
      assetType : {
        required : "请选择资产类别"
      },
      purchaseType : {
        required : "请选择采购类别"
      },
      departmentId : {
        required : "请选择申请部门"
      },
      pay : {
        required : "请选择支付方式"
      },
      toName : {
        required : "请输入收款方名称"
      },
      toNo : {
        required : "请输入收款人账号"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('.detailDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
          function(){
            setDetail();
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("purchase","../manage/ajax-purchase!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!purchase.action?step=1",$('#content'));
            }
          }

  );

  function setDetail(){
    var purchaseDetail="[";
    for(var i=0;i<$("#detailContent tr").size();i++){
      purchaseDetail+='{"name":"'+$('.detailName')[i].value+
      '","number":"'+$('.number')[i].value+
      '","detailNo":"'+$('.detailNo')[i].value+
      '","budgetMoney":"'+$('.budgetMoney')[i].value+
      '","budgetPrice":"'+$('.budgetPrice')[i].value+
//      '","purchasingPrice":"'+$('.purchasingPrice')[i].value+
//      '","ActualMoney":"'+$('.ActualMoney')[i].value+
//      '","detailDate":"'+$('.detailDate')[i].value+
      '","detailRemarks":"'+$('.detailRemarks')[i].value+'"},';
    }
    purchaseDetail=purchaseDetail.substring(0,purchaseDetail.length-1);
    purchaseDetail+="]";
    $("#purchaseDetail").val(purchaseDetail);
    var detailNum='[{"number":"'+$("#number").text()+
            '","budgetPrice":"'+$("#budgetPrice").text()+
            '","budgetMoney":"'+$("#budgetMoney").text()+
            '","purchasingPrice":"'+$("#purchasingPrice").text()+
            '","ActualMoney":"'+$("#ActualMoney").text()+'"}]';
    $("#detailNum").val(detailNum);
  }

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            setDetail();
            if (!$("#purchase").valid()) {
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title: "提示：",
              content: "确定提交申请吗？",
              buttons: '[取消][确认]'
            }, function (ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#purchase").valid();
                if (!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("purchase", "../manage/ajax-purchase!commit.action");
                if (draft == 1) {
                  location.href = 'index.action';
                } else {
                  loadURL("../manage/ajax!purchase.action?step=1", $('#content'));
                }
              }
            });
          }
  );

  //数字禁止输入其他字符
  $(".num").keyup(function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).bind("paste",function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).css("ime-mode", "disabled");
</script>