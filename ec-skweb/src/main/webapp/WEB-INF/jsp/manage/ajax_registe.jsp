<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      资料管理
    </h1>
  </div>
</div>

<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
            <shiro:hasAnyRoles name="wechat">
              <s:if test="viewtype==1">
                <a id="ajax_registe_btn_add" <s:property value="isCreate('registe')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建资料登记表</a>
              </s:if>
              <s:if test="viewtype==2">
               <a id="ajax_registe_invalid" <s:property value="isCreate('registe')"/> class="btn btn-default pull-right" href="javascript:void(0)"> <i class="fa fa-lg fa-hand-o-right"></i> 失效 </a>
             </s:if>
            </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_registe_list_row">
                    <table id="ajax_registe_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_registe_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_registe_jqGrid();
    //失效操作
    $('#ajax_registe_invalid').click(function(){
      var ids=$("#ajax_registe_table").jqGrid('getGridParam','selarrrow');
      if(ids.length != 1){
        alert("请选择一条资料登记表");
        return false;
      }
      $.tzDialog({title:"提示",content:"确认要将此资料登记表失效吗?",callback:function(){
        var vActionUrl = "<%=path%>/manage/ajax-registe!doInvalid.action";
        var data={keyId:ids[0]};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          jQuery("#ajax_registe_table").jqGrid().setGridParam({datatype:'json'}).trigger("reloadGrid");
        });

      }});
    });



  });


  function load_registe_jqGrid(){
    jQuery("#ajax_registe_table").jqGrid({
      url:'../manage/ajax-registe!list.action?viewtype='+"<s:property value="viewtype" />",
      datatype: "json",
      colNames:['年份','月',"资料类别",'资料名称','上传日期',"上传人","文档状态","操作","id"],
      colModel:[
        {name:'year',index:'year', width:60,sortable:false,search:true},
        {name:'month',index:'month', width:60,sortable:false,search:true},
        {name:'type',index:'type_name',sortable:false,search:true,width:120},
        {name:'dataName',index:'dataName', width:100,search:true,sortable:true},
        {name:'createDate',index:'createDate', width:100,search:true,sortable:true},
        {name:'creater',index:'creater_name',sortable:false,search:true,width:80},
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_registe_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        var ids=$("#ajax_registe_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_registe_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_registe_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_registe_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_registe_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_registe_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i>资料管理",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_registe_table").jqGrid('setGridWidth', $("#ajax_registe_list_row").width());
    })
    jQuery("#ajax_registe_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_registe_table").jqGrid('navGrid', "#ajax_registe_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_registe_read(id){
    loadURL("../manage/ajax-registe!read.action?keyId="+id+"&viewtype="+"<s:property value="viewtype" />",$('#content'));
  }

  $("#ajax_registe_btn_add").off("click").on("click",function(){
    loadURL("../manage/ajax-registe!input.action",$('#content'));
  });
</script>





















