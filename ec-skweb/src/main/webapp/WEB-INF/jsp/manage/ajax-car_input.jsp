<%--
  Created by IntelliJ IDEA. 
  User: ZhuChunXiao
  Date: 2017/3/21
  Time: 16:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>车辆管理</a>
          <s:if test="car==null || car.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="car!=null && car.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="car" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="car.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="isUse" id="isUse" value="<s:property value="car.isUse" />"/>
            <header  style="display: block;">
              车辆使用登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  车牌号码
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="carConfigId" id="carConfigId" >
                      <option value="">请选择</option>
                      <s:iterator value="carConfigList" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  车辆品牌
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="brand" id="brand" disabled placeholder="车辆品牌" value="<s:property value="car.carConfig.brand"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  车辆型号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="type" id="type" disabled placeholder="车辆型号" value="<s:property value="car.carConfig.type"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请用途
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="purpose" id="purpose" placeholder="请输入申请用途" value="<s:property value="car.purpose"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请详细说明
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="description" id="description" placeholder="请输入申请详细说明" value="<s:property value="car.description"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请相关附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  使用时间
                </label>
                <section class="col col-2">
                  <label class="input">
                    <input placeholder="请选择使用开始时间" id="dateStart" name="dateStart" type="text" value="<s:date name="car.dateStart" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <section class="col col-2">
                  <label class="input">
                    <input placeholder="请选择使用结束时间" id="dateEnd" name="dateEnd" type="text" value="<s:date name="car.dateEnd" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <section class="col col-2" style="margin-top: -5px">
                  <input class="btn btn-default" type="button" id="useInfo" value="查看使用明细" style="display: none"/>
                </section>
              </div>

              <%--<s:if test="car!=null&&car.getProcessState().name()=='Draft'">--%>
                <%--<div class="row">--%>
                  <%--<label class="label col col-2">--%>
                    <%--<i class="fa fa-asterisk txt-color-red"></i>--%>
                    <%--当前公里数--%>
                  <%--</label>--%>
                  <%--<section class="col col-5">--%>
                    <%--<label class="input">--%>
                      <%--<input disabled  type="text" name="kilometre" id="kilometre"  value="<s:property value="car.kilometre"/>" >--%>
                    <%--</label>--%>
                  <%--</section>--%>
                <%--</div>--%>
              <%--</s:if>--%>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="remarks" id="remarks"  value="<s:property value="car.remarks"/>" >
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>

</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"car",
      todo:"1"
    };
    multiDuty(pdata);
  });

  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!car.action?step=1",$('#content'));
    }
  });

  //选择车牌号之后获取数据
  $("#carConfigId").change(function(){
    var carConfigId=$(this).children('option:selected').val();
    if(carConfigId!=null){
      $.ajax({
        url:"../manage/ajax-car!getInfo.action",
        type:"post",
        cache:false,dataType:"json",async:true,
        data:{"carConfigId":carConfigId},
        success:function(data){
          if(data.result.errorCode == 1){
            $("#brand").val(data.data.brand);
            $("#type").val(data.data.type);
          }
        }
      });
    }
  });

  //校验
  $("#car").validate({
    rules : {
      carConfigId : {
        required : true
      },
      purpose : {
        required : true
      },
      description:{
        required : true
      },
      fileIds: {
        required : true
      },
      dateStart:{
        required : true
      },
      dateEnd:{
        required : true
      }
    },
    messages : {
      carConfigId : {
        required : "请选择车辆号牌"
      },
      purpose : {
        required : "请输入申请用途"
      },
      description : {
        required : "请输入申请详细说明"
      },
      fileIds : {
        required : "请上传相关附件"
      },
      dateStart: {
        required : "请选择使用开始时间"
      },
      dateEnd:{
        required : "请选择使用结束时间"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  //判断是否有人正在用车
  function check(){
    var carConfigId= $("#carConfigId").children('option:selected').val();
    var dateStart=$("#dateStart").val();
    $.ajax({
      type: "POST",
      url: "../manage/ajax-car!checkuse.action",
      cache:false,dataType:"json",async:true,
      data:{"carConfigId":carConfigId,"dateStart1":dateStart},
      success: function(result){
        var isUse=result.data.data[0].check;
        if(isUse!=1){
          $("#isUse").val("0");
          $("#useInfo").show();
        }else{
          $("#isUse").val("1");
          $("#useInfo").hide();
        }
      }
    });
  }

  $('#dateStart').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  $('#dateEnd').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("car","../manage/ajax-car!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!car.action?step=1",$('#content'));
            }
          }
  );

  //条件符合后开始判断用车信息
  $("#carConfigId,#dateStart").change(function(){
    if($("#carConfigId").val()!=""&&$("#dateStart").val()!=""){
      check();
    }
  });

  $("#useInfo").click(function(){
    loadURL("../manage/ajax!car.action?carNoId3="+$("#carConfigId").children('option:selected').val(),$('#content'));
  });

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#car").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            check();
            if($("#isUse").val()==0){
              alert("您选择的时间段该车已经有人使用");
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#car").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("car","../manage/ajax-car!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!car.action?step=1",$('#content'));
                }
              }
            });
          }
  );
</script>