<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26 
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common2" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <shiro:hasAnyRoles name="wechat">
            <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(meetingrecord.id)"/> key="ajax_edit2" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
          </shiro:hasAnyRoles>
          <hr class="simple">
          <form id="meetingrecord" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="meetingrecord.meetingUse.id" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="meetingrecord.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              会议记录登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  会议纪要上传
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file"  style="display: none">
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  备注
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="meetingrecord.remark"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>

          <div class="flow">
            <s:if test="meetingrecord.getProcessState().name()=='Running'">
              <div class="f_title">审批意见</div>
              <div class="chat-footer"style="margin-top:5px">
                <div class="textarea-div">
                  <div class="typearea">
                    <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                  </div>
                </div>
                <span class="textarea-controls"></span>
              </div>
            </s:if>
            <div class="f_title"><i class="right" id="flow2"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow2"></div>
            </div>
            <div class="f_title"><i class="right" id="next2"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext2"></div>
            </div>
          </div>
        </div>
      </div>

  </article>
</div>

<script>
  //返回视图
  $("#btn-re-common2").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!meetingrecord.action",$('div#s2'));
    }

  });
  //编辑
  $("a[key=ajax_edit2]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-meetingrecord!input.action?keyId="+$("#meetingrecord #keyId").val()+"&draft="+draft,$('#content'));
  });

  //返回视图
  $("#btn-re-common2").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!meetingrecord.action",$('div#s2'));
    }

  });
  //上传
  readLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId"
  });

  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("#meetingrecord input#keyId").val()+"&type=flow",$('#showFlow2'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("#meetingrecord input#keyId").val()+"&type=next",$('#showNext2'));

    //流程信息展开
    $('#flow2,#next2').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });

</script>
