<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />

<div class="row" >
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
        <div class="widget-body">
            <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>返回
            </a>
            <shiro:hasAnyRoles name="wechat">
              <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(schedule.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 编辑</a>
            </shiro:hasAnyRoles>

              <hr class="simple">
              <form id="schedule" class="smart-form" novalidate="novalidate" action="" method="post">
                <input type="hidden" name="numStatus" id="numStatus"  value="<s:property value="numStatus" />"/>
                <input type="hidden" name="keyId" id="keyId" value="<s:property value="schedule.id" />"/>
                <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId"/>"/>
                <header  style="display: block;">
                  日程安排&nbsp;&nbsp;<span id="title"></span>
                </header>
                <fieldset>
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      日程类型
                    </label>
                    <section class="col col-5">
                      <div class="inline-group state-disabled">
                        <label class="radio">
                          <input disabled type="radio"  name="type" value="约会客户" <s:property value="schedule.type==\"约会客户\"?'checked':''"/>>
                          <i></i>约会客户</label>
                        <label class="radio">
                          <input disabled type="radio"  name="type" value="外出业务" <s:property value="schedule.type==\"外出业务\"?'checked':''"/>>
                          <i></i>外出业务</label>
                        <label class="radio">
                          <input disabled type="radio"  name="type" value="备忘" <s:property value="schedule.type==\"备忘\"?'checked':''"/>>
                          <i></i>备忘</label>
                      </div>
                    </section>
                  </div>
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      主题
                    </label>
                    <section class="col col-5">
                      <label class="input  state-disabled">
                        <input disabled type="text" name="topic" id="topic" placeholder="请输入主题" value="<s:property value="schedule.topic"/>" >
                      </label>
                    </section>
                  </div>
                  <div class="row" >
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      开始时间
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled placeholder="请输入开始时间" id="beginDate" name="beginDate"
                                type="text" value="<s:date name="schedule.beginDate" format="yyyy-MM-dd HH:mm"/>">
                      </label>
                    </section>
                  </div>
                  <div class="row" >
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      结束时间
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled  placeholder="请输入结束时间" id="endDate" name="endDate"
                                type="text" value="<s:date name="schedule.endDate" format="yyyy-MM-dd HH:mm"/>">
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      参与人员
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text" id="joinUsers" name="joinUsers"
                               value="<s:iterator id="list" value="schedule.joinUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                        <input type="hidden" id="joinUsersId" name="joinUsersId"
                               value="<s:iterator id="list" value="schedule.joinUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      是否公开
                    </label>
                    <section class="col col-5">
                      <div class="inline-group state-disabled">
                        <label class="radio">
                          <input disabled type="radio"  name="open" value="1"  <s:property value="schedule.open==1?'checked':''"/>>
                          <i></i>是</label>
                        <label class="radio">
                          <input disabled type="radio"  name="open" value="0" <s:property value="schedule == null || schedule.open==null || schedule.open==0?'checked':''"/>>
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      具体内容
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text" name="content" id="content" placeholder="请输入内容" value="<s:property value="schedule.content"/>" >
                      </label>
                    </section>
                  </div>

                </fieldset>
              </form>
              <div class="flow">
                <s:if test="schedule.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer"style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
              </div>
        </div>
      </div>
    </article>

</div>

<script type="text/javascript">


  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!schedule.action",$('#content'));
    }

  });
  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-schedule!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });


  //左侧意见栏
  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "schedule"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("schedule","ajax-schedule!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;



    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="ajax-schedule!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="ajax-schedule!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });

</script>