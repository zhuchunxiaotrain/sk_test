<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/23
  Time: 12:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      合同管理
    </h1>
  </div>
</div>

<div id="contract_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <shiro:hasAnyRoles name="wechat">
            <s:if test="viewtype==1">
              <a id="ajax_contract_btn_add" <s:property value="isCreate('contract')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建发文申请表</a>
            </s:if>
          </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_contract_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_contract_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_contract_jqGrid();
  });


  function load_contract_jqGrid(){
    jQuery("#ajax_contract_table").jqGrid({
      url:'../manage/ajax-contract!list.action?viewtype='+"<s:property value="viewtype" />",
      datatype: "json",
      colNames:['合同编号',"合同名称",'合同甲方',"合同乙方","合同标的（元）","状态","操作","id"],
      colModel:[
        {name:'no',index:'no', width:100,sortable:true,search:true,searchoptions:{sopt:['cn']}},
        {name:'name',index:'name', width:150,search:true,sortable:true,searchoptions:{sopt:['cn']}},
        {name:'contracta',index:'contracta', width:100,search:true,sortable:true,searchoptions:{sopt:['cn']}},
        {name:'contractb',index:'contractb', width:100,search:true,sortable:true,searchoptions:{sopt:['cn']}},
        {name:'subject',index:'subject',sortable:true,search:true,width:120,searchoptions:{sopt:['cn']}},
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      loadonce: true,
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_contract_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        $('.ui-search-oper').hide();
        var ids=$("#ajax_contract_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_contract_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_contract_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_contract_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_contract_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_contract_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 合同管理",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_contract_table").jqGrid('setGridWidth', $("#ajax_contract_list_row").width());
    })
    jQuery("#ajax_contract_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_contract_table").jqGrid('navGrid', "#ajax_contract_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_contract_read(id){
    loadURL("../manage/ajax-contract!read.action?keyId="+id+"&viewtype="+"<s:property value="viewtype" />",$('#content'));
  }

  $("#ajax_contract_btn_add").off("click").on("click",function(){
    loadURL("../manage/ajax-contract!input.action",$('#content'));
  });
</script>