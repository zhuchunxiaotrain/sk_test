<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="notice==null || notice.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="notice!=null && notice.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="notice" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="notice.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              公告通知&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  通知主题
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="topic" id="topic" placeholder="请输入通知主题" value="<s:property value="notice.topic"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否具有有效期
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="hasValid" value="1" <s:property value="notice.hasValid==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio"  name="hasValid" value="0" <s:property value="notice.hasValid==0?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>
              <div class="row" <s:if test="notice==null || notice.hasValid!=1">style="display:none"</s:if> id="showValid">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  有效期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择有效期" id="validDate" name="validDate"
                            type="text" value="<s:date name="notice.validDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  是否置顶
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="top" value="1"  <s:property value="notice.top==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio"  name="top" value="0" <s:property value="notice == null || notice.top==null || notice.top==0?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                   公告类型
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="noticedictId" id="noticedictId" >
                      <option value="">请选择</option>
                      <s:iterator value="noticeDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-department"> 发布部门</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="department" id="department" placeholder="请选择发布部门" value="<s:property value="notice.department.name"/>" >
                    <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="notice.department.id"/>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                   通知对象
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="object" value="1"  <s:property value='notice == null || notice.object == null || notice.object=="1"  ?"checked":""'/>>
                      <i></i>全体员工</label>
                    <label class="radio">
                      <input type="radio"  name="object" value="2" <s:property value='notice.object=="2"?"checked":""'/>>
                      <i></i>部门</label>
                    <label class="radio">
                      <input type="radio"  name="object" value="3" <s:property value='notice.object=="3"?"checked":""'/>>
                      <i></i>个人</label>
                  </div>
                </section>
              </div>
              <div class="row" <s:if test='notice == null || notice.object!="2"'>style="display:none"</s:if> id="showDept">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-noticeDepartment">通知部门</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="noticeDepartment" name="noticeDepartment"
                           value="<s:iterator id="list" value="notice.noticeDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="noticeDepartmentId" name="noticeDepartmentId"
                           value="<s:iterator id="list" value="notice.noticeDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
                <div class="row" <s:if test='notice == null || notice.object!="3"'>style="display:none"</s:if> id="showPerson">
                    <label class="label col col-2">
                        <i class="fa fa-asterisk txt-color-red"></i>
                        <a href="javascript:void(0);" key="btn-choose-noticeUsers">通知个人</a>
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text" id="noticeUsers" name="noticeUsers"
                               value="<s:iterator id="list" value="notice.noticeUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                        <input type="hidden" id="noticeUsersId" name="noticeUsersId"
                               value="<s:iterator id="list" value="notice.noticeUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                </div>
              <div class="row">
                <label class="label col col-2">
                  内容
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="content" id="content" placeholder="请输入内容" value="<s:property value="notice.content"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                 附件上传
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">

                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>

  </article>
</div>
 
<script>
  var draft = "<s:property value="draft" />";
  function getDefaultDept(data){
      $('#department').val(data.department);
      $('#departmentId').val(data.departmentId);
  }
  var time1 = setInterval("checkDuty()",1000);
  function checkDuty(){
    var curDutyId = $('#curDutyId').val();
    if(curDutyId != ""){
      clearInterval(time1)
      var vActionUrl = '../manage/ajax-notice!getDefaultDept.action?curDutyId='+curDutyId;
      ajax_action(vActionUrl,null,null,getDefaultDept);
    }
  }
  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"notice",
      todo:"1"
    };
    multiDuty(pdata);
  });
  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });

  //发布部门
  $("a[key=btn-choose-department]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择发布部门",
          url:"ajax-dialog!dept.action?key=department",
          width:560
      }).show();
  });

  //通知个人
  $("a[key=btn-choose-noticeUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知个人",
      url:"ajax-dialog!levelUser.action?key=noticeUsers&more=1",
      width:560
    }).show();
  });
  //通知部门
  $("a[key=btn-choose-noticeDepartment]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知部门",
      url:"ajax-dialog!dept.action?key=noticeDepartment&more=1",
      width:560
    }).show();
  });
  $(':radio[name="object"]').click(function(){
    if($(this).val()=="1") {
      $('#showPerson').hide();
      $('#showDept').hide();
    }else if($(this).val()=="2"){
        $('#showPerson').hide();
        $('#showDept').show();
    }else if($(this).val()=="3"){
       $('#showPerson').show();
       $('#showDept').hide();
    }
  });

  $(':radio[name="hasValid"]').click(function(){
    if($(this).val()=="1") {
      $('#showValid').show();
    }else{
      $('#showValid').hide();
    }
  });
  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!notice.action?viewtype=1",$('#content'));
    }

  });
  //校验
  $("#notice").validate({
    rules : {
      topic : {
        required : true
      },
      hasValid: {
        required : true
      },
      validDate:{
        required : function(){
          if($(':checked[name="hasValid"]').val() == "1"){
            return true;
          }else{
            return false;
          }
        }
      },
      top:{
        required : true
      },
      noticedictId:{
        required : true
      },
      departmentId:{
        required : true
      },
      object:{
        required : true
      },
      noticeDepartmentId:{
        required:function(){
          if($(':checked[name="object"]').val() == "2"){
            return true;
          }else{
            return false;
          }
        }
      },
      noticeUsersId:{
        required:function(){
          if($(':checked[name="object"]').val() == "3"){
            return true;
          }else{
            return false;
          }
        }
      },
      content:{
        required:true
      },
      fileIds:{
        required:true
      }
    },
    messages : {
      topic : {
        required : "请输入通知主题"
      },
      hasValid : {
        required : "请选择是否具有有效期"
      },
      validDate : {
        required : "请选择有效期"
      },
      top:{
        required : "请选择是否置顶"
      },
      noticedictId:{
        required : "请选择公告类型"
      },
      departmentId:{
        required : "请选择发布部门"
      },
      object:{
        required : "请选择通知对象"
      },
      noticeDepartmentId:{
        required: "请选择通知部门"
      },
      noticeUsersId:{
        required: "请选择通知个人"
      },
      content:{
        required:"请输入内容"
      },
      fileIds:{
        required:"请上传附件"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#validDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("notice","../manage/ajax-notice!save.action");
          if(draft == 1){
            location.href='index.action';
          }else{
            loadURL("../manage/ajax!notice.action?viewtype=1",$('#content'));
          }
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#notice").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#notice").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("notice","../manage/ajax-notice!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!notice.action?viewtype=1",$('#content'));
                }
              }
            });
          }
  );
</script>
