<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/21
  Time: 15:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      车辆使用登记
    </h1>
  </div>
</div>

<div id="car_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <shiro:hasAnyRoles name="wechat">
            <s:if test="step==1||step==null">
              <a id="ajax_car_btn_add" <s:property value="isCreate('car')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建车辆使用申请表</a>
            </s:if>
            <s:if test="step==2">
              <a id="ajax_car_kilometre" <s:property value="isCreate('car')"/> class="btn btn-default pull-right" data-toggle="modal" data-target="#myModal"> <i class="fa fa-lg fa-hand-o-right"></i> 当前公里数登记 </a>
            </s:if>
          </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_car_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_car_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->

  <!-- 公里数登记模态框 -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="width: auto">
    <div class="modal-dialog" role="document" style="width: auto">
      <div class="modal-content" style="box-shadow: none;border: none">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">当前公里数登记</h4>
        </div>
        <div class="modal-body">
          <form id="kilometre" class="smart-form" novalidate="novalidate" action="" method="post">

            <input type="hidden" name="keyId" id="keyId"/>

            <div class="row">
              <label class="label col col-4">
                当前公里数
              </label>
              <section class="col col-7">
                <label class="input">
                  <input  placeholder="请输入当前公里数" id="kilometreNum" name="kilometreNum" type="text">
                </label>
              </section>
            </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="kilometre_close">关闭</button>
          <button type="button" class="btn btn-primary" id="kilometre_save">保存</button>
        </div>
      </div>
    </div>
  </div>


</div>

<script type="text/javascript">
  $(function(){
    load_car_jqGrid();
  });


  function load_car_jqGrid(){
    jQuery("#ajax_car_table").jqGrid({
      url:'../manage/ajax-car!list.action?step='+"<s:property value="step" />"+"&carNoId3="+"<s:property value="carNoId3" />",
      datatype: "json",
      colNames:['申请人',"申请用途",'车辆品牌',"车辆型号","车牌号码","使用时间","状态","操作","id"],
      colModel:[
        {name:'creater',index:'creater', width:100,sortable:true,search:true,searchoptions:{sopt:['cn']}},
        {name:'use',index:'purpose', width:80,search:true,sortable:true,searchoptions:{sopt:['cn']}},
        {name:'brand',index:'brand', width:80,search:true,sortable:true,searchoptions:{sopt:['cn']}},
        {name:'type',index:'type',sortable:true,search:true,width:80,searchoptions:{sopt:['cn']}},
        {name:'carNo',index:'carNo', search:true,sortable:true,width:120,searchoptions:{sopt:['cn']}},
        {name:'time',index:'time', search:true,sortable:true,width:80,searchoptions:{sopt:['cn']}},
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      loadonce: true,
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_car_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        $('.ui-search-oper').hide();
        var ids=$("#ajax_car_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_car_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_car_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_car_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_car_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_car_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 车辆管理",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_car_table").jqGrid('setGridWidth', $("#ajax_car_list_row").width());
    })
    jQuery("#ajax_car_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_car_table").jqGrid('navGrid', "#ajax_car_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  //校验
  $("#kilometre").validate({
    rules : {
      kilometreNum : {
        required : true
      }
    },
    messages : {
      kilometreNum : {
        required : "请输入当前公里数"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  function fn_car_read(id){
    loadURL("../manage/ajax-car!read.action?keyId="+id+"&step="+"<s:property value="step" />",$('#content'));
  }

  $("#ajax_car_btn_add").off("click").on("click",function(){
    loadURL("../manage/ajax-car!input.action",$('#content'));
  });

  //显示公里数模态框
  $("#ajax_car_kilometre").off("click").on("click",function(){
    var ids=$("#ajax_car_table").jqGrid('getGridParam','selarrrow');
    if(ids.length != 1){
      alert("请选择一条车辆申请表");
      return false;
    }
    $("#keyId").val(ids);
  });

  //执行公里数操作
  $("#kilometre_save").off("click").on("click",function(){
    var $validForm = $("#kilometre").valid();
    if(!$validForm) return false;
    form_save("kilometre","../manage/ajax-car!qkilometre.action",null,function(data){
      if(data.state==200){
        $("#kilometre_close").trigger('click');
        jQuery("#ajax_car_table").trigger("reloadGrid");
      }
    });
  });

  //公里数禁止输入其他字符
  $("#kilometreNum").keyup(function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).bind("paste",function(){
    $(this).val($(this).val().replace(/[^0-9.]/g,''));
  }).css("ime-mode", "disabled");
  
</script>