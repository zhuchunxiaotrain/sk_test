<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26 
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common2" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="meetingrecord==null || meetingrecord.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="meetingrecord!=null && meetingrecord.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="meetingrecord" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="meetingrecord.meetingUse.id" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="meetingrecord.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              会议记录登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  会议纪要上传
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="meetingrecord.remark"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#meetingrecord #keyId").val(),
      flowName:"meetingrecord",
      todo:"1"
    };
    multiDuty(pdata);
    if($('#parentId').val() == ""){
      $('#parentId').val("<s:property value="parentId" />");
    }
  });
  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });


  //返回视图
  $("#btn-re-common2").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!meetingrecord.action",$('div#s2'));
    }

  });
  //校验
  $("#meetingrecord").validate({
    rules : {
      fileIds:{
        required:true
      }
    },
    messages : {
      fileIds:{
        required:"请选择会议纪要上传"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });



  //保存
  $("#btn-save-common").click(
        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("meetingrecord","../manage/ajax-meetingrecord!save.action");
          if(draft == 1){
            location.href='index.action';
          }else{
            loadURL("../manage/ajax!meetingrecord.action",$('div#s2'));
          }
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(

          function(e) {

            if(!$("#meetingrecord").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#meetingrecord").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("meetingrecord","../manage/ajax-meetingrecord!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!meetingrecord.action",$('div#s2'));
                }
              }
            });
          }
  );


</script>
