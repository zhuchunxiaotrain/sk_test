<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="receive==null || receive.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="receive!=null && receive.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="receive" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="receive.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              收文登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  收文类别
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="typedictId" id="typedictId" >
                      <option value="">请选择</option>
                      <s:iterator value="typeDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  收文编号
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="receiveNo" id="receiveNo" placeholder="请输入收文编号" value="<s:property value="receive.receiveNo"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  收文标题
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="title" id="title1" placeholder="请输入收文标题" value="<s:property value="receive.title"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  收文时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择收文时间" id="time" name="time"
                            type="text" value="<s:date name="receive.time" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  有效期限
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择有效期限" id="validDate" name="validDate"
                            type="text" value="<s:date name="receive.validDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>


                <div class="row">
                    <label class="label col col-2">
                        <i class="fa fa-asterisk txt-color-red"></i>
                        <a href="javascript:void(0);" key="btn-choose-passObject">传阅对象</a>
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text" id="passObject" name="passObject"
                               value="<s:iterator id="list" value="receive.passObject"><s:property value="#list.name"/>,</s:iterator>"/>
                        <input type="hidden" id="passObjectId" name="passObjectId"
                               value="<s:iterator id="list" value="receive.passObjectId"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  正文附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"receive",
      todo:"1"
    };
    multiDuty(pdata);

  });
  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });

  //传阅对象
  $("a[key=btn-choose-passObject]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择传阅对象",
      url:"ajax-dialog!levelUser.action?key=passObject&more=1",
      width:560
    }).show();
  });



  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!receive.action?viewtype=1",$('#content'));
    }

  });
  //校验
  $("#receive").validate({
    rules : {
      typedictId : {
        required : true
      },
      title : {
        required : true
      },
      receiveNo : {
        required : true
      },
      time : {
        required : true
      },
      validDate:{
        required : true
      },
      passObjectId:{
        required : true
      },
      fileIds:{
        required:true
      }
    },
    messages : {
      typedictId : {
        required : "请选择收文类别"
      },
      title : {
        required : "请输入收文标题"
      },
      receiveNo : {
        required : "请输入收文编号"
      },
      time: {
        required : "请选择发文时间"
      },
      validDate: {
        required : "请选择有效期限"
      },
      passObjectId:{
        required : "请选择传阅对象"
      },
      fileIds:{
        required: "请选择正文附件"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#validDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  $('#time').datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  //保存
  $("#btn-save-common").click(
        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("receive","../manage/ajax-receive!save.action");
          if(draft == 1){
            location.href='index.action';
          }else{
            loadURL("../manage/ajax!receive.action?viewtype=1",$('#content'));
          }
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#receive").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#receive").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("receive","../manage/ajax-receive!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!receive.action",$('#content'));
                }
              }
            });
          }
  );
</script>
