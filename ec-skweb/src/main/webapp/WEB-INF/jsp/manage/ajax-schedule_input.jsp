<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="schedule==null || schedule.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="schedule!=null && schedule.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="schedule" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="schedule.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              日程安排&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  日程类型
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="type" value="约会客户" <s:property value="schedule.type==\"约会客户\"?'checked':''"/>>
                      <i></i>约会客户</label>
                    <label class="radio">
                      <input type="radio"  name="type" value="外出业务" <s:property value="schedule.type==\"外出业务\"?'checked':''"/>>
                      <i></i>外出业务</label>
                    <label class="radio">
                      <input type="radio"  name="type" value="备忘" <s:property value="schedule.type==\"备忘\"?'checked':''"/>>
                      <i></i>备忘</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  主题
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="topic" id="topic" placeholder="请输入主题" value="<s:property value="schedule.topic"/>" >
                  </label>
                </section>
              </div>

              <div class="row" >
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  开始时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请输入开始时间" id="beginDate" name="beginDate"
                            type="text" value="<s:date name="schedule.beginDate" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
              </div>
              <div class="row" >
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  结束时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请输入结束时间" id="endDate" name="endDate"
                            type="text" value="<s:date name="schedule.endDate" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                  <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      <a href="javascript:void(0);" key="btn-choose-joinUsers">参与人员</a>
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input disabled type="text" id="joinUsers" name="joinUsers"
                             value="<s:if test="schedule == null"><s:property value="loginUser.name"/></s:if><s:else><s:iterator id="list" value="schedule.joinUsers"><s:property value="#list.name"/>,</s:iterator></s:else>"/>
                      <input type="hidden" id="joinUsersId" name="joinUsersId"
                             value="<s:if test="schedule == null"><s:property value="loginUser.id"/></s:if><s:else><s:iterator id="list" value="schedule.joinUsers"><s:property value="#list.id"/>,</s:iterator></s:else>"/>
                    </label>
                  </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否公开
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="open" value="1" <s:property value="schedule.open==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio"  name="open" value="0" <s:property value="schedule == null || schedule.open==null || schedule.open==0?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>


              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  公开范围
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="object" value="1" <s:property value='schedule == null || schedule.object == null || schedule.object=="1"  ?"checked":""'/>>
                      <i></i>全体员工</label>
                    <label class="radio">
                      <input type="radio"  name="object" value="2" <s:property value='schedule.object=="2"?"checked":""'/>>
                      <i></i>部门</label>
                    <label class="radio">
                      <input type="radio"  name="object" value="3" <s:property value='schedule.object=="3"?"checked":""'/>>
                      <i></i>个人</label>
                  </div>
                </section>
              </div>
              <div class="row" <s:if test='schedule == null || schedule.object!="2"'>style="display:none"</s:if> id="showDept">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-openDepartment">部门</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="openDepartment" name="openDepartment"
                           value="<s:iterator id="list" value="schedule.openDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="openDepartmentId" name="openDepartmentId"
                           value="<s:iterator id="list" value="schedule.openDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row" <s:if test='schedule == null || schedule.object!="3"'>style="display:none"</s:if> id="showPerson">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-openUsers">个人</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="openUsers" name="openUsers"
                           value="<s:iterator id="list" value="schedule.openUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="openUsersId" name="openUsersId"
                           value="<s:iterator id="list" value="schedule.openUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  具体内容
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="content" id="content" placeholder="请输入内容" value="<s:property value="schedule.content"/>" >
                  </label>
                </section>
              </div>


            </fieldset>
          </form>
        </div>
      </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";
  function getDefaultDept(data){
    $('#openDepartment').val(data.department);
    $('#openDepartmentId').val(data.departmentId);
  }
  var time1 = setInterval("checkDuty()",1000);
  function checkDuty(){
    var curDutyId = $('#curDutyId').val();
    if(curDutyId != ""){
      clearInterval(time1)
      var vActionUrl = '../manage/ajax-schedule!getDefaultDept.action?curDutyId='+curDutyId;
      ajax_action(vActionUrl,null,null,getDefaultDept);
    }
  }
  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"schedule",
      todo:"1"
    };
    multiDuty(pdata);

  });

  //公开个人
  $("a[key=btn-choose-openUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知个人",
      url:"ajax-dialog!levelUser.action?key=openUsers&more=1",
      width:560
    }).show();
  });
  //公开部门
  $("a[key=btn-choose-openDepartment]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知部门",
      url:"ajax-dialog!dept.action?key=openDepartment&more=1",
      width:560
    }).show();
  });
  //公开范围
  $(':radio[name="object"]').click(function(){
    if($(this).val()=="1") {
      $('#showPerson').hide();
      $('#showDept').hide();
    }else if($(this).val()=="2"){
      $('#showPerson').hide();
      $('#showDept').show();
    }else if($(this).val()=="3"){
      $('#showPerson').show();
      $('#showDept').hide();
    }
  });

  //参与人员
  $("a[key=btn-choose-joinUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择参与人员",
      url:"ajax-dialog!levelUser.action?key=joinUsers",
      width:560
    }).show();
  });




  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else {
      loadURL("../manage/ajax!schedule.action", $('#content'));
    }
  });
  //校验
  $("#schedule").validate({
    rules : {
      type : {
        required : true
      },
      topic : {
        required : true
      },
      beginDate: {
        required : true
      },
      endDate:{
        required : true
      },
      joinUsersId:{
        required : true
      },
      open:{
        required : true
      },
      object:{
        required : true
      },
      openDepartmentId:{
        required:function(){
          if($(':checked[name="object"]').val() == "2"){
            return true;
          }else{
            return false;
          }
        }
      },
      openUsersId:{
        required:function(){
          if($(':checked[name="object"]').val() == "3"){
            return true;
          }else{
            return false;
          }
        }
      }
    },
    messages : {
      type : {
        required : "请选择日期类型"
      },
      topic : {
        required : "请输入通知主题"
      },
      beginDate:{
        required : "请选择开始时间"
      },
      endDate:{
        required : "请选择结束时间"
      },
      joinUsersId:{
        required : "请选择参与人员"
      },
      open:{
        required : "请选择是否公开"
      },
      object:{
        required : "请选择公开范围"
      },
      openDepartmentId:{
        required: "请选择部门"
      },
      openUsersId:{
        required: "请选择个人"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#beginDate').datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  $('#endDate').datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  //保存
  $("#btn-save-common").click(
        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("schedule","../manage/ajax-schedule!save.action");
          if(draft == 1){
            location.href='index.action';
          }else {
            loadURL("../manage/ajax!schedule.action", $('#content'));
          }
        }
  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#schedule").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#schedule").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("schedule","../manage/ajax-schedule!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else {
                  loadURL("../manage/ajax!schedule.action", $('#content'));
                }
              }
            });
          }
  );
</script>
