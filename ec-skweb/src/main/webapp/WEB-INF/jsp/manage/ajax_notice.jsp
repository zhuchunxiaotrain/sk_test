<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      公告通知
    </h1>
  </div>
</div>

<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
            <shiro:hasAnyRoles name="wechat">
              <s:if test="viewtype==1">
                <a id="ajax_notice_btn_add" <s:property value="isCreate('notice')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建公告通知</a>
              </s:if>
              <s:if test="viewtype==2">
               <a id="ajax_notice_invalid" <s:property value="isCreate('notice')"/> class="btn btn-default pull-right" href="javascript:void(0)"> <i class="fa fa-lg fa-hand-o-right"></i> 失效 </a>
               <a id="ajax_cancel_notice_top" <s:property value="isCreate('notice')"/> class="btn btn-default pull-right" href="javascript:void(0)"> <i class="fa fa-lg fa-hand-o-right"></i> 取消置顶 </a>
               <a id="ajax_notice_top" <s:property value="isCreate('notice')"/> class="btn btn-default pull-right" href="javascript:void(0)"> <i class="fa fa-lg fa-hand-o-right"></i> 公告置顶 </a>
             </s:if>
            </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_notice_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_notice_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_notice_jqGrid();
    //失效操作
    $('#ajax_notice_invalid').click(function(){
      var ids=$("#ajax_notice_table").jqGrid('getGridParam','selarrrow');
      if(ids.length != 1){
        alert("请选择一条公告通知");
        return false;
      }
      $.tzDialog({title:"提示",content:"确认要将此公告通知失效吗?",callback:function(){
        var vActionUrl = "<%=path%>/manage/ajax-notice!doInvalid.action";
        var data={keyId:ids[0]};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          jQuery("#ajax_notice_table").trigger("reloadGrid");
        });

      }});
    });
    //公告置顶操作
    $('#ajax_notice_top').click(function(){
      var ids=$("#ajax_notice_table").jqGrid('getGridParam','selarrrow');
      if(ids.length != 1){
        alert("请选择一条公告通知");
        return false;
      }
      var checkUrl = "<%=path%>/manage/ajax-notice!isTop.action";
      var data={keyId:ids[0]};
      ajax_action(checkUrl,data,{},function(pdata){
          if(pdata.rs == "true"){
            alert("此公告已经置顶，无法重复操作");
          }else{
            $.tzDialog({title:"提示",content:"确认要将此公告通知置顶吗?",callback:function(){
              var vActionUrl = "<%=path%>/manage/ajax-notice!doTop.action";
              var data={keyId:ids[0]};
              ajax_action(vActionUrl,data,{},function(pdata){
                _show(pdata);
                jQuery("#ajax_notice_table").trigger("reloadGrid");
              });
            }});
          }
      });
    });
    //取消置顶操作
    $('#ajax_cancel_notice_top').click(function(){
      var id = $('#chooseId').val();
      if(!id){
        alert("请选择一条公告通知");
        return false;
      }
      var checkUrl = "<%=path%>/manage/ajax-notice!isTop.action";
      var data={keyId:id};
      ajax_action(checkUrl,data,{},function(pdata){
        if(pdata.rs == "false"){
          alert("此公告不是置顶公告，无法进行操作");
        }else{
          $.tzDialog({title:"提示",content:"确认要将此公告通知取消置顶吗?",callback:function(){
            var vActionUrl = "<%=path%>/manage/ajax-notice!doCancelTop.action";
            var data={keyId:id};
            ajax_action(vActionUrl,data,{},function(pdata){
              _show(pdata);
              jQuery("#ajax_notice_table").trigger("reloadGrid");
            });

          }});
        }
      });
    });
  });


  function load_notice_jqGrid(){
    jQuery("#ajax_notice_table").jqGrid({
      url:'../manage/ajax-notice!list.action?viewtype='+"<s:property value="viewtype" />",
      datatype: "json",
      colNames:['日期',"公告类型",'通知主题',"创建人","文档状态","操作","id"],
      colModel:[
        {name:'createDate',index:'createDate', width:100,search:true,sortable:true},
        {name:'type',index:'type_name', width:200,sortable:false,search:true},
        {name:'topic',index:'topic', width:250,search:true,sortable:true},
        {name:'creater',index:'creater_name',sortable:false,search:true,width:80},
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_notice_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        var ids=$("#ajax_notice_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_notice_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_notice_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_notice_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_notice_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_notice_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 公告通知",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_notice_table").jqGrid('setGridWidth', $("#ajax_notice_list_row").width());
    })
    jQuery("#ajax_notice_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_notice_table").jqGrid('navGrid', "#ajax_notice_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_notice_read(id){
    loadURL("../manage/ajax-notice!read.action?keyId="+id+"&viewtype="+"<s:property value="viewtype" />",$('#content'));
  }

  $("#ajax_notice_btn_add").off("click").on("click",function(){
    loadURL("../manage/ajax-notice!input.action",$('#content'));
  });
</script>





















