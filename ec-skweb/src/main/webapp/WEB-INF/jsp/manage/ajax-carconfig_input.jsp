<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/4/7
  Time: 10:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>车辆配置</a>
          <s:if test="carConfig==null || carConfig.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="carConfig!=null && carConfig.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="carconfig" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="carConfig.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              车辆配置表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  车辆品牌
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="brand" id="brand" placeholder="请输入车辆品牌" value="<s:property value="carConfig.brand"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  车辆型号
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="type" id="type" placeholder="请输入车辆型号" value="<s:property value="carConfig.type"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  车辆号牌
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="carNo" id="carNo" placeholder="请输入车辆号牌" value="<s:property value="carConfig.carNo"/>" >
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"carconfig",
      todo:"1"
    };
    multiDuty(pdata);

  });

  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!carconfig.action",$('#content'));
    }

  });
  //校验
  $("#carconfig").validate({
    rules : {
      brand : {
        required : true
      },
      type : {
        required : true
      },
      carNo : {
        required : true
      }
    },
    messages : {
      brand : {
        required : "请输入车辆品牌"
      },
      type : {
        required : "请输入车辆型号"
      },
      carNo : {
        required : "请选择车辆号牌"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });


  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("carconfig","../manage/ajax-carconfig!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!carconfig.action",$('#content'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#carconfig").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#carconfig").valid();
                if(!$validForm) return false;
//                $("#btn-confirm-common").attr("disabled", "disabled");
//                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("carconfig","../manage/ajax-carconfig!commit.action",null,function(data){
                  if(data.state==200){
                    if(draft == 1){
                      location.href='index.action';
                    }else{
                      loadURL("../manage/ajax!carconfig.action",$('#content'));
                    }
                  }
                });
              }
            });
          }
  );

</script>