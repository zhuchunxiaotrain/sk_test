<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/29
  Time: 15:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common2" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <s:if test="useDetail==null || useDetail.getProcessState().name()=='Draft'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
        </s:if>
        <s:if test="useDetail!=null && useDetail.getProcessState().name()=='Backed'">
          <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
        </s:if>
        <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
        <hr class="simple">
        <form id="usedetail" class="smart-form" novalidate="novalidate" action="" method="post">
          <input type="hidden" name="parentId" id="parentId" value="<s:property value="useDetail.assetUse.id" />"/>
          <input type="hidden" name="keyId" id="keyId" value="<s:property value="useDetail.id" />"/>
          <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
          <header  style="display: block;">
            资产使用调拨登记表&nbsp;&nbsp;<span id="title"></span>
          </header>
          <fieldset>

            <div class="row">
              <label class="label col col-2">
                原使用部门
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input disabled type="text" value="<s:property value="oldDepartment"/>" >
                  <%--<input type="hidden" value="<s:property value="assetUse.department.id"/>"/>--%>
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                原使用人员
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input disabled type="text" value="<s:property value="oldUser"/>" >
                  <%--<input type="hidden"  value="<s:property value="assetUse.user.id"/>"/>--%>
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                <a href="javascript:void(0);" key="btn-choose-newDepartment"> 现使用部门</a>
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input disabled type="text" name="newDepartment" id="newDepartment" placeholder="请选择现使用部门" value="<s:property value="useDetail.newDepartment.name"/>" >
                  <input type="hidden" id="newDepartmentId" name="newDepartmentId" value="<s:property value="useDetail.newDepartment.id"/>"/>
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                <a href="javascript:void(0);" key="btn-choose-newUser">现使用人员</a>
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input disabled type="text" id="newUser" name="newUser" placeholder="请选择现使用人员" value="<s:property value="useDetail.newUser.name"/>"/>
                  <input type="hidden" id="newUserId" name="newUserId" value="<s:property value="useDetail.newUser.id"/>"/>
                </label>
              </section>
            </div>

            <div class="row" >
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                <a href="javascript:void(0);" key="btn-choose-examUsers">会签人员</a>
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input disabled type="text" id="examUsers" name="examUsers" placeholder="请选择会签人员"
                         value="<s:iterator id="list" value="useDetail.examUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                  <input type="hidden" id="examUsersId" name="examUsersId"
                         value="<s:iterator id="list" value="useDetail.examUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                </label>
              </section>
            </div>

          </fieldset>
        </form>
      </div>
    </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#usedetail #keyId").val(),
      flowName:"usedetail",
      todo:"1"
    };
    multiDuty(pdata);
    if($('#parentId').val() == ""){
      $('#parentId').val("<s:property value="parentId" />");
    }
  });


  //现使用部门
  $("a[key=btn-choose-newDepartment]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择现通知部门",
      url:"ajax-dialog!dept.action?key=newDepartment&more=0",
      width:560
    }).show();
  });

  //现使用人员
  $("a[key=btn-choose-newUser]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择现使用人员",
      url:"ajax-dialog!levelUser.action?key=newUser&more=0",
      width:560
    }).show();
  });

  //会签人员
  $("a[key=btn-choose-examUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择会签人员",
      url:"ajax-dialog!levelUser.action?key=examUsers&more=1",
      width:560
    }).show();
  });


  //返回视图
  $("#btn-re-common2").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!usedetail.action",$('div#s2'));
    }

  });
  //校验
  $("#usedetail").validate({
    rules : {
      newDepartmentId:{
        required:true
      },
      newUserId:{
        required:true
      },
      examUsersId:{
        required:true
      }
    },
    messages : {
      newDepartmentId:{
        required:"请选择现使用部门"
      },
      newUserId:{
        required:"请选择现使用人员"
      },
      examUsersId:{
        required:"请选择会签人员"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });



  //保存
  $("#btn-save-common").click(
          function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("usedetail","../manage/ajax-usedetail!save.action");
            if(draft == 1){
              location.href='index.action';
            }else{
              loadURL("../manage/ajax!usedetail.action",$('div#s2'));
            }
          }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(

          function(e) {

            if(!$("#usedetail").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#usedetail").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("usedetail","../manage/ajax-usedetail!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!usedetail.action",$('div#s2'));
                }
              }
            });
          }
  );


</script>