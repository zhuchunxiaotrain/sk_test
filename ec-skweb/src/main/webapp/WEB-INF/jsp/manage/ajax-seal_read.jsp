<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/20
  Time: 17:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>印章使用管理</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 印章使用申请表 </a>
            </li>
          </ul>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
              <div class="row">
                <shiro:hasAnyRoles name="wechat">
                  <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(seal.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
                </shiro:hasAnyRoles>
              </div>
              <hr class="simple">
              <form id="seal" class="smart-form" novalidate="novalidate" action="" method="post">
                <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
                <input type="hidden" name="keyId" id="keyId" value="<s:property value="seal.id" />"/>
                <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                <header  style="display: block;">
                  印章使用申请表&nbsp;&nbsp;<span id="title"></span>
                </header>
                <fieldset>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      印章使用类型
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input type="text" name="typeText" id="typeText" disabled placeholder="请选择印章使用类型" value="<s:property value="typeText"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row" <s:if test="seal==null || seal.type==1">style="display:none"</s:if> id="showReturn">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      归还日期
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input  disabled  id="returnDate" name="returnDate"
                                type="text" value="<s:date name="seal.returnDate" format="yyyy-MM-dd"/>">
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      印章名称
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input  type="text" name="sealNameId" id="sealNameId" disabled placeholder="请选择印章名称" value="<s:property value="seal.sealName.name"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      盖章事由
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input  type="text" name="reason" id="reason" disabled placeholder="请输入盖章事由" value="<s:property value="seal.reason"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      盖章内容/份数
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input  type="text" name="content" id="content" disabled placeholder="请输入盖章内容/份数" value="<s:property value="seal.content"/>" >
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      附件上传
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input name="uploadify" id="filename" style="display: none" placeholder="" type="file" >
                        <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                      </label>
                    </section>
                  </div>

                  <s:if test="flowNumStatus==2">
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要上报中心
                    </label>
                    <section class="col col-5">
                      <div class="inline-group">
                        <label class="radio">
                          <input type="radio"  name="reportCenter" value="1" >
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  name="reportCenter" value="0" >
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>
                  </s:if>
                  <s:if test="flowNumstarts>2">
                    <div class="row">
                      <label class="label col col-2">
                        <i class="fa fa-asterisk txt-color-red"></i>
                        是否需要上报中心
                      </label>
                      <section class="col col-5">
                        <div class="inline-group state-disabled">
                          <label class="radio">
                            <input type="radio"  disabled name="reportCenter" value="1" <s:property value="seal.reportCenter==1?'checked':''"/>>
                            <i></i>是</label>
                          <label class="radio">
                            <input type="radio"  disabled name="reportCenter" value="0" <s:property value="seal.reportCenter==0?'checked':''"/>>
                            <i></i>否</label>
                        </div>
                      </section>
                    </div>
                  </s:if>

                </fieldset>

              </form>
              <div class="flow">
                <s:if test="seal.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer"style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
          </div>
        </div>
      </div>
    </div>

  </article>
</div>

<script>
  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "seal"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("seal","../manage/ajax-seal!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("seal","../manage/ajax-seal!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          if($("#seal").valid()){
            form_save("seal","../manage/ajax-seal!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
              $("#btn-re-common").trigger("click");
            })
          }
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("seal", "../manage/ajax-seal!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "4":
        //第四步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("seal", "../manage/ajax-seal!approve4.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-seal!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-seal!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });
  //上传文件
  readLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId"
  });
  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!seal.action?step=<s:property value="step"/>",$('#content'));
    }

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-seal!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

  //校验
  $("#seal").validate({
    rules : {
      reportCenter : {
        required : true
      }
    },
    messages : {
      reportCenter : {
        required : "请选择是否需要上报中心"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

</script>