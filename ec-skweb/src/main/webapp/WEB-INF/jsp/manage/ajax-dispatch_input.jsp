<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="dispatch==null || dispatch.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="dispatch!=null && dispatch.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="dispatch" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="dispatch.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              发文申请表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  发文类别
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="typedictId" id="typedictId" >
                      <option value="">请选择</option>
                      <s:iterator value="typeDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  发文标题
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text" name="title" id="title1" placeholder="请输入发文标题" value="<s:property value="dispatch.title"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  文件编号
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="fileNo" id="fileNo" placeholder="请输入文件编号" value="<s:property value="dispatch.fileNo"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  发文时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择发文时间" id="time" name="time"
                            type="text" value="<s:date name="dispatch.time" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  发文对象
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="object" value="1" <s:property value='dispatch == null || dispatch.object == null || dispatch.object=="1"  ?"checked":""'/>>
                      <i></i>全体员工</label>
                    <label class="radio">
                      <input type="radio"  name="object" value="2" <s:property value='dispatch.object=="2"?"checked":""'/>>
                      <i></i>部门</label>
                    <label class="radio">
                      <input type="radio"  name="object" value="3" <s:property value='dispatch.object=="3"?"checked":""'/>>
                      <i></i>个人</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否具有有效期
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="hasValid" value="1" <s:property value="dispatch.hasValid==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio"  name="hasValid" value="0" <s:property value="dispatch.hasValid==0?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>
              <div class="row" <s:if test="dispatch==null || dispatch.hasValid!=1">style="display:none"</s:if> id="showValid">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  有效期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择有效期" id="validDate" name="validDate"
                            type="text" value="<s:date name="dispatch.validDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

              <div class="row" <s:if test='dispatch == null || dispatch.object!="2"'>style="display:none"</s:if> id="showDept">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-noticeDepartment">通知部门</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="noticeDepartment" name="noticeDepartment"
                           value="<s:iterator id="list" value="dispatch.noticeDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="noticeDepartmentId" name="noticeDepartmentId"
                           value="<s:iterator id="list" value="dispatch.noticeDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
                <div class="row" <s:if test='dispatch == null || dispatch.object!="3"'>style="display:none"</s:if> id="showPerson">
                    <label class="label col col-2">
                        <i class="fa fa-asterisk txt-color-red"></i>
                        <a href="javascript:void(0);" key="btn-choose-noticeUsers">通知个人</a>
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text" id="noticeUsers" name="noticeUsers"
                               value="<s:iterator id="list" value="dispatch.noticeUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                        <input type="hidden" id="noticeUsersId" name="noticeUsersId"
                               value="<s:iterator id="list" value="dispatch.noticeUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  相关附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"dispatch",
      todo:"1"
    };
    multiDuty(pdata);

  });
  //上传
  inputLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId",
    jsessionid:"<%=jsessionid%>"
  });



  //通知个人
  $("a[key=btn-choose-noticeUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知个人",
      url:"ajax-dialog!levelUser.action?key=noticeUsers&more=1",
      width:560
    }).show();
  });
  //通知部门
  $("a[key=btn-choose-noticeDepartment]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择通知部门",
      url:"ajax-dialog!dept.action?key=noticeDepartment&more=1",
      width:560
    }).show();
  });
  $(':radio[name="object"]').click(function(){
    if($(this).val()=="1") {
      $('#showPerson').hide();
      $('#showDept').hide();
    }else if($(this).val()=="2"){
        $('#showPerson').hide();
        $('#showDept').show();
    }else if($(this).val()=="3"){
       $('#showPerson').show();
       $('#showDept').hide();
    }
  });

  $(':radio[name="hasValid"]').click(function(){
    if($(this).val()=="1") {
      $('#showValid').show();
    }else{
      $('#showValid').hide();
    }
  });
  //返回视图
  $("#btn-re-common").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!dispatch.action?viewtype=1",$('#content'));
    }

  });
  //校验
  $("#dispatch").validate({
    rules : {
      typedictId : {
        required : true
      },
      title : {
        required : true
      },
      fileNo : {
        required : true
      },
      time : {
        required : true
      },
      object:{
        required : true
      },
      hasValid: {
        required : true
      },
      validDate:{
        required : function(){
          if($(':checked[name="hasValid"]').val() == "1"){
            return true;
          }else{
            return false;
          }
        }
      },
      noticeDepartmentId:{
        required:function(){
          if($(':checked[name="object"]').val() == "2"){
            return true;
          }else{
            return false;
          }
        }
      },
      noticeUsersId:{
        required:function(){
          if($(':checked[name="object"]').val() == "3"){
            return true;
          }else{
            return false;
          }
        }
      },
      fileIds:{
        required:true
      }
    },
    messages : {
      typedictId : {
        required : "请选择发文类别"
      },
      title : {
        required : "请输入发文标题"
      },
      fileNo : {
        required : "请输入文件编号"
      },
      time : {
        required : "请选择发文时间"
      },
      object : {
        required : "请选择发文对象"
      },
      hasValid : {
        required : "请选择是否具有有效期"
      },
      validDate: {
        required : "请选择有效期"
      },
      noticeDepartmentId:{
        required: "请选择通知部门"
      },
      noticeUsersId:{
        required: "请选择通知个人"
      },
      fileIds:{
        required:"请选择相关附件"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#validDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  $('#time').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });
  //保存
  $("#btn-save-common").click(
        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("dispatch","../manage/ajax-dispatch!save.action");
          if(draft == 1){
            location.href='index.action';
          }else{
            loadURL("../manage/ajax!dispatch.action?viewtype=1",$('#content'));
          }
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#dispatch").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#dispatch").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("dispatch","../manage/ajax-dispatch!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!dispatch.action?viewtype=1",$('#content'));
                }
              }
            });
          }
  );
</script>
