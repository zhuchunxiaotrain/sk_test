<%-- 
  Created by IntelliJ IDEA. 
  User: dqf
  Date: 2015/8/26 
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 发文申请表 </a>
            </li>
            <li class="disabled">
              <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 附件修订 </a>
            </li>
          </ul>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
          <hr class="simple">
          <form id="dispatch" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="dispatch.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <shiro:hasAnyRoles name="wechat">
              <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(dispatch.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>
            <header  style="display: block;">
              发文申请表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  发文类别
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="typedictId" id="typedictId" disabled placeholder="请输入发文类别" value="<s:property value="dispatch.type.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  发文标题
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  type="text" name="title" id="title1" disabled placeholder="请输入发文标题" value="<s:property value="dispatch.title"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  文件编号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="fileNo" id="fileNo" disabled placeholder="请输入文件编号" value="<s:property value="dispatch.fileNo"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  发文时间
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  placeholder="请选择发文时间" id="time" name="time" disabled
                            type="text" value="<s:date name="dispatch.time" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  发文对象
                </label>
                <section class="col col-5">
                  <div class="inline-group state-disabled">
                    <label class="radio">
                      <input type="radio" disabled name="object" value="1" <s:property value='dispatch.object=="1"?"checked":""'/>>
                      <i></i>全体员工</label>
                    <label class="radio">
                      <input type="radio" disabled name="object" value="2" <s:property value='dispatch.object=="2"?"checked":""'/>>
                      <i></i>部门</label>
                    <label class="radio">
                      <input type="radio" disabled name="object" value="3" <s:property value='dispatch.object=="3"?"checked":""'/>>
                      <i></i>个人</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否具有有效期
                </label>
                <section class="col col-5">
                  <div class="inline-group state-disabled">
                    <label class="radio">
                      <input type="radio" disabled  name="hasValid" value="1" <s:property value="dispatch.hasValid==1?'checked':''"/>>
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio" disabled  name="hasValid" value="0" <s:property value="dispatch.hasValid==0?'checked':''"/>>
                      <i></i>否</label>
                  </div>
                </section>
              </div>
              <div class="row" <s:if test="dispatch==null || dispatch.hasValid!=1">style="display:none"</s:if> id="showValid">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  有效期
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  disabled  id="validDate" name="validDate"
                            type="text" value="<s:date name="dispatch.validDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

              <div class="row" <s:if test='dispatch == null || dispatch.object!="2"'>style="display:none"</s:if>  id="showDept">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  通知部门
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="noticeDepartment" name="noticeDepartment"
                           value="<s:iterator id="list" value="dispatch.noticeDepartment"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="noticeDepartmentId" name="noticeDepartmentId"
                           value="<s:iterator id="list" value="dispatch.noticeDepartment"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row" <s:if test='dispatch == null || dispatch.object!="3"'>style="display:none"</s:if>  id="showPerson">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  通知个人
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="noticeUsers" name="noticeUsers"
                           value="<s:iterator id="list" value="dispatch.noticeUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="noticeUsersId" name="noticeUsersId"
                           value="<s:iterator id="list" value="dispatch.noticeUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                 附件上传
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input name="uploadify" id="filename" style="display: none" placeholder="" type="file" >
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>
              <s:if test="flowNumStatus==2">
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  是否需要会签
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="mulExam" value="1" >
                      <i></i>是</label>
                    <label class="radio">
                      <input type="radio"  name="mulExam" value="0" >
                      <i></i>否</label>
                  </div>
                </section>
              </div>

              <div class="row" <s:if test='dispatch == null || dispatch.mulExam!="1"'>style="display:none"</s:if> id="showExamUsers">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a href="javascript:void(0);" key="btn-choose-examUsers">会签人员</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="examUsers" name="examUsers"
                           value="<s:iterator id="list" value="dispatch.examUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="examUsersId" name="examUsersId"
                           value="<s:iterator id="list" value="dispatch.examUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    是否需要中心领导审核
                  </label>
                  <section class="col col-5">
                    <div class="inline-group">
                      <label class="radio">
                        <input type="radio"  name="leaderExam" value="1" >
                        <i></i>是</label>
                      <label class="radio">
                        <input type="radio"  name="leaderExam" value="0" >
                        <i></i>否</label>
                    </div>
                  </section>
                </div>

                <div class="row" <s:if test='dispatch == null || dispatch.leaderExam!="1"'>style="display:none"</s:if> id="showExamObject">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    中心审核对象
                  </label>
                  <section class="col col-5">
                    <div class="inline-group">
                      <s:iterator value="examObject" id="list">
                        <label class="checkbox">
                          <input type="checkbox"  name="examObjectId" value="<s:property value="#list.id"/>" <s:property value="#list.checked"/> >
                          <i></i><span><s:property value="#list.name"/></span>
                        </label>
                      </s:iterator>
                    </div>
                  </section>
                </div>
              </s:if>
              <s:if test="flowNumStatus>2">
                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    是否需要会签
                  </label>
                  <section class="col col-5">
                    <div class="inline-group state-disabled">
                      <label class="radio">
                        <input type="radio" disabled  value="1" <s:property value="dispatch.mulExam==1?'checked':''"/>>
                        <i></i>是</label>
                      <label class="radio">
                        <input type="radio" disabled  value="0" <s:property value="dispatch.mulExam==0?'checked':''"/>>
                        <i></i>否</label>
                    </div>
                  </section>
                </div>

                <div class="row" <s:if test='dispatch == null || dispatch.mulExam!="1"'>style="display:none"</s:if>>
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    会签人员
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input disabled type="text"  name="examUsers"
                             value="<s:iterator id="list" value="dispatch.examUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden"  name="examUsersId"
                             value="<s:iterator id="list" value="dispatch.examUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    是否需要中心领导审核
                  </label>
                  <section class="col col-5">
                    <div class="inline-group state-disabled">
                      <label class="radio">
                        <input type="radio"  disabled name="leaderExam" value="1" <s:property value="dispatch.leaderExam==1?'checked':''"/>>
                        <i></i>是</label>
                      <label class="radio">
                        <input type="radio"  disabled name="leaderExam" value="0" <s:property value="dispatch.leaderExam==0?'checked':''"/>>
                        <i></i>否</label>
                    </div>
                  </section>
                </div>

                <div class="row" <s:if test='dispatch == null || dispatch.leaderExam!="1"'>style="display:none"</s:if> >
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    中心审核对象
                  </label>
                  <section class="col col-5">
                    <div class="inline-group ">
                      <s:iterator value="examObject" id="list">
                        <label class="checkbox  state-disabled">
                          <input disabled type="checkbox" name="examObjectId" value="<s:property value="#list.id"/>" <s:property value="#list.checked"/> >
                          <i></i><span><s:property value="#list.name"/></span>
                        </label>
                      </s:iterator>
                    </div>
                  </section>
                </div>
              </s:if>


            </fieldset>

          </form>
          <div class="flow">
            <s:if test="dispatch.getProcessState().name()=='Running'">
            <div class="f_title">审批意见</div>
            <div class="chat-footer"style="margin-top:5px">
              <div class="textarea-div">
                <div class="typearea">
                  <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                </div>
              </div>
              <span class="textarea-controls"></span>
            </div>
            </s:if>
            <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow"></div>
            </div>
            <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext"></div>
            </div>
          </div>
        </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
          </div>
        </div>
    </div>
  </article>
</div>

<script>
  var state = "<s:property value="dispatch.getProcessState().name()" />";
  if(state == "Finished"){
    $('#other1').parent('li').removeClass('disabled');
  }
  var table_global_width=0;
  $("a#other1").off("click").on("click",function(e) {
    if(state == "Finished"){
      if (table_global_width == 0) {
        table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
      }
      loadURL("../manage/ajax!dispatchrevise.action?parentId="+$("#keyId").val(),$('div#s2'));
    }else{
      return false;
    }

  });

  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "dispatch"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("dispatch","../manage/ajax-dispatch!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("dispatch","../manage/ajax-dispatch!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          if($("#dispatch").valid()){
            form_save("dispatch","../manage/ajax-dispatch!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
              $("#btn-re-common").trigger("click");
            })
          }
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("dispatch", "../manage/ajax-dispatch!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
              $("#btn-re-common").trigger("click");
            })
        });
        break;
      case "4":
        //第四步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("dispatch", "../manage/ajax-dispatch!approve4.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-dispatch!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-dispatch!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });
  //上传文件
  readLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId"
  });
  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!dispatch.action?viewtype=<s:property value="viewtype"/>",$('#content'));
    }

  });
  //是否需要会签
  $(":radio[name='mulExam']").click(function(){
    if($(this).val() == "1"){
      $("#showExamUsers").show();
    }else{
      $("#showExamUsers").hide();
    }
  });
  //是否需要中心领导审核
  $(":radio[name='leaderExam']").click(function(){
    if($(this).val() == "1"){
      $("#showExamObject").show();
    }else{
      $("#showExamObject").hide();
    }
  });
  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-dispatch!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

  //校验
  $("#dispatch").validate({
    rules : {
      mulExam : {
        required : true
      },
      examUsersId:{
        required : function(){
          if($(':checked[name="mulExam"]').val() == "1"){
            return true;
          }else{
            return false;
          }
        }
      },
      leaderExam:{
        required : true
      },
      examObjectId:{
        required : function(){
          if($(':checked[name="leaderExam"]').val() == "1"){
            return true;
          }else{
            return false;
          }
        }
      }
    },
    messages : {
      mulExam : {
        required : "请选择是否需要会签"
      },
      examUsersId:{
        required : "请选择会签人员"
      },
      leaderExam:{
        required : "请选择是否需要中心领导审核"
      },
      examObjectId:{
        required : "请选择中心审核对象"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });


  //会签人员
  $("a[key=btn-choose-examUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择会签人员",
      url:"ajax-dialog!levelUser.action?key=examUsers&more=1",
      width:560
    }).show();
  });
</script>
