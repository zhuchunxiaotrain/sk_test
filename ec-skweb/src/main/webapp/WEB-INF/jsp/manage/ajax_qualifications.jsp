<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/20
  Time: 10:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      公司资质管理
    </h1>
  </div>
</div>

<div id="qualifications_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <shiro:hasAnyRoles name="wechat">
            <s:if test="step==1||step==null">
              <a id="ajax_qualifications_btn_add" <s:property value="isCreate('qualifications')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建公司资质</a>
            </s:if>
            <s:if test="step==2||step==3">
              <a id="ajax_qualifications_btn_continue" <s:property value="isCreate('qualifications')"/> class="btn btn-default " data-toggle="modal" data-target="#myModal"><i class="fa fa-lg fa-plus"></i> 资质延续</a>
              <%--<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Small modal</button>--%>
            </s:if>
          </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_qualifications_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_qualifications_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->

  <!-- 延续操作模态框 -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="width: auto">
    <div class="modal-dialog" role="document" style="width: auto">
      <div class="modal-content" style="box-shadow: none;border: none">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">资质延续</h4>
        </div>
        <div class="modal-body">
          <form id="continue" class="smart-form" novalidate="novalidate" action="" method="post">

            <input type="hidden" name="keyId" id="keyId"/>

            <div class="row">
              <label class="label col col-4">
                评定日期
              </label>
              <section class="col col-7">
                <label class="input">
                  <input  placeholder="请选择评定日期" id="evaluateDate" name="evaluateDate" type="text">
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-4">
                <i class="fa fa-asterisk txt-color-red"></i>
                使用年限
              </label>
              <section class="col col-7">
                <label class="input">
                  <input  placeholder="请选择使用年限" id="useDate" name="useDate" type="text">
                </label>
              </section>
            </div>

            <div class="row">
              <label class="label col col-4">
                <i class="fa fa-asterisk txt-color-red"></i>
                提前提醒周期
              </label>
              <section class="col col-7">
                <label class="input">
                  <input  placeholder="请选择提前提醒周期" id="remindDate" name="remindDate" type="text">
                </label>
              </section>
            </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="continue_close">关闭</button>
          <button type="button" class="btn btn-primary" id="continue_save">保存</button>
        </div>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
  $(function(){
    load_qualifications_jqGrid();
  });


  function load_qualifications_jqGrid(){
    jQuery("#ajax_qualifications_table").jqGrid({
      url:'../manage/ajax-qualifications!list.action?step='+"<s:property value="step" />",
      datatype: "json",
      colNames:['资质类别',"资质名称",'资质号',"评定日期","使用期限","文档状态","操作","id"],
      colModel:[
        {name:'type',index:'type', width:100,sortable:true,search:true,searchoptions:{sopt:['cn']}},
        {name:'qualificationsName',index:'qualificationsDict', width:150,search:true,sortable:true,searchoptions:{sopt:['cn']}},
        {name:'qualificationsNo',index:'qualificationsDict', width:150,search:true,sortable:true,searchoptions:{sopt:['cn']}},
        {name:'evaluateDate',index:'evaluateDate',sortable:true,search:true,width:120,searchoptions:{sopt:['cn']}},
        {name:'useDate',index:'useDate',sortable:true,search:true,width:120,searchoptions:{sopt:['cn']}},
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      loadonce: true,
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_qualifications_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        $('.ui-search-oper').hide();
        var ids=$("#ajax_qualifications_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_qualifications_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_qualifications_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_qualifications_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_qualifications_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_qualifications_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 公司资质管理",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_qualifications_table").jqGrid('setGridWidth', $("#ajax_qualifications_list_row").width());
    })
    jQuery("#ajax_qualifications_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_qualifications_table").jqGrid('navGrid', "#ajax_qualifications_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  $('#evaluateDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  $('#useDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  $('#remindDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //校验
  $("#continue").validate({
    rules : {
      useDate : {
        required : true
      },
      remindDate: {
        required : true
      }
    },
    messages : {
      useDate : {
        required : "请选择使用年限"
      },
      remindDate : {
        required : "请选择提前提醒周期"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  function fn_qualifications_read(id){
    loadURL("../manage/ajax-qualifications!read.action?keyId="+id+"&step="+"<s:property value="step" />",$('#content'));
  }

  $("#ajax_qualifications_btn_add").off("click").on("click",function(){
    loadURL("../manage/ajax-qualifications!input.action",$('#content'));
  });

  //显示资质延续模态框
  $("#ajax_qualifications_btn_continue").off("click").on("click",function(){
    var ids=$("#ajax_qualifications_table").jqGrid('getGridParam','selarrrow');
    if(ids.length != 1){
      alert("请选择一条公司资质登记表");
      return false;
    }
    $("#keyId").val(ids);
  });

  //执行资质延续操作
  $("#continue_save").off("click").on("click",function(){
    var $validForm = $("#continue").valid();
    if(!$validForm) return false;
    form_save("continue","../manage/ajax-qualifications!qContinue.action",null,function(data){
      if(data.state==200){
        $("#continue_close").trigger('click');
        jQuery("#ajax_qualifications_table").trigger("reloadGrid");
      }
    });
  });

</script>