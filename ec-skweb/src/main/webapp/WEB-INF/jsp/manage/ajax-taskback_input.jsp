<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26 
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common2" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
          <s:if test="taskback==null || taskback.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="taskback!=null && taskback.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="taskback" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="taskback.taskControl.id" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="taskback.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              任务督办反馈单&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  完成程度
                </label>
                <section class="col col-5">
                  <div class="inline-group">
                    <label class="radio">
                      <input type="radio"  name="degree" value="全部完成" <s:property value='taskback.degree=="全部完成"?"checked":""'/>>
                      <i></i>全部完成</label>
                    <label class="radio">
                      <input type="radio"  name="degree" value="部分完成" <s:property value='taskback.degree=="部分完成"?"checked":""'/>>
                      <i></i>部分完成</label>
                  </div>
                </section>
              </div>

              <div class="row" <s:if test="taskback==null || taskback.degree!=\"全部完成\"">style="display:none"</s:if> id="showValid">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  实际完成时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择实际完成时间" id="finishTime" name="finishTime"
                            type="text" value="<s:date name="taskback.finishTime" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  任务反馈内容
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请填写任务反馈内容" id="content" name="content"
                            type="text" value="<s:property value="taskback.content"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  相关附件
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="filename2" placeholder="" type="file" >
                    <input name="fileId2" id="fileId2" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  placeholder="请选择备注" id="remark" name="remark"
                            type="text" value="<s:property value="taskback.remark"/>">
                  </label>
                </section>
              </div>


            </fieldset>
          </form>
        </div>
      </div>

  </article>
</div>

<script>
  var draft = "<s:property value="draft" />";

  $(function(){
    var pdata= {
      keyId:$("#taskback #keyId").val(),
      flowName:"taskback",
      todo:"1"
    };
    multiDuty(pdata);

  });

  //上传
  inputLoad({
    objId:"filename2",
    entityName:"fileIds2",
    sourceId:"fileId2",
    jsessionid:"<%=jsessionid%>"
  });


  $(':radio[name="degree"]').click(function(){
    if($(this).val()=="全部完成") {
      $('#taskback #showValid').show();
    }else{
      $('#taskback #showValid').hide();
    }
  });
  //返回视图
  $("#btn-re-common2").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!taskback.action",$('div#s2'));
    }

  });
  //校验
  $("#taskback").validate({
    rules : {
      degree : {
        required : true
      },
      finishTime:{
        required : function(){
          if($('#taskback :checked[name="degree"]').val() == "全部完成"){
            return true;
          }else{
            return false;
          }
        }
      },
      content:{
        required: true
      },
      fileIds:{
        required:true
      },
      remark:{
        required:true
      }
    },
    messages : {
      degree : {
        required : "请选择完成程度"
      },
      finishTime:{
        required :"请选择实际完成时间"
      },
      content:{
        required: "请填写任务反馈内容"
      },
      fileIds:{
        required:"请上传相关附件"
      },
      remark:{
        required:"请填写备注"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });

  $('#taskback #finishTime').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(
        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("taskback","../manage/ajax-taskback!save.action");
          if(draft == 1){
            location.href='index.action';
          }else{
            loadURL("../manage/ajax!taskback.action",$('div#s2'));
          }
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(

          function(e) {

            if(!$("#taskback").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#taskback").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("taskback","../manage/ajax-taskback!commit.action");
                if(draft == 1){
                  location.href='index.action';
                }else{
                  loadURL("../manage/ajax!taskback.action",$('div#s2'));
                }
              }
            });
          }
  );


</script>
