<%--
  Created by IntelliJ IDEA.
  User: ZhuChunXiao
  Date: 2017/3/23
  Time: 13:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <div class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

      <div class="widget-body">
        <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>合同管理</a>
        <ul id="myTab1" class="nav nav-tabs bordered  ">
          <li class="active">
            <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i>合同登记表</a>
          </li>
          <%--<li class="disabled">--%>
            <%--<a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 审批信息 </a>--%>
          <%--</li>--%>
        </ul>
        <div id="myTabContent1" class="tab-content padding-10 ">
          <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
            <hr class="simple">
            <form id="contract" class="smart-form" novalidate="novalidate" action="" method="post">
              <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
              <input type="hidden" name="keyId" id="keyId" value="<s:property value="contract.id" />"/>
              <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
              <shiro:hasAnyRoles name="wechat">
                <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(contract.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
              </shiro:hasAnyRoles>
              <header  style="display: block;">
                合同登记表&nbsp;&nbsp;<span id="title"></span>
              </header>
              <fieldset>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    合同编号
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" class="num" name="no" id="no" disabled placeholder="合同编号" value="<s:property value="contract.no"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    合同名称
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="name" id="name" disabled placeholder="合同名称" value="<s:property value="contract.name"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    经办部门
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input disabled type="text" name="department" id="department" placeholder="经办部门" value="<s:property value="contract.department.name"/>" >
                      <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="contract.department.id"/>"/>
                    </label>
                  </section>

                  <label class="label col col-1">
                    经办人
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="manager" id="manager" disabled placeholder="经办人" value="<s:property value="contract.manager"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    经办人职务
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="managerPost" id="managerPost" disabled placeholder="经办人职务" value="<s:property value="contract.managerPost"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    合同甲方
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="contractA" id="contractA" disabled placeholder="合同甲方" value="<s:property value="contract.contractA"/>" >
                    </label>
                  </section>

                  <label class="label col col-1">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    合同乙方
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="contractB" id="contractB" disabled placeholder="合同乙方" value="<s:property value="contract.contractB"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    合同第三方
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="contractC" id="contractC" disabled placeholder="合同第三方" value="<s:property value="contract.contractC"/>" >
                    </label>
                  </section>

                  <label class="label col col-1">
                    合同其他方
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="contractD" id="contractD" disabled placeholder="合同其他方" value="<s:property value="contract.contractD"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    预算编号
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" class="num" name="budgetNo" id="budgetNo" disabled placeholder="预算编号" value="<s:property value="contract.budgetNo"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    合同标的（小写）
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" class="num" name="subjectSmall" id="subjectSmall" disabled placeholder="合同标的（小写）" value="<s:property value="contract.subjectSmall"/>" >
                    </label>
                  </section>

                  <label class="label col col-1">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    合同标的（大写）
                  </label>
                  <section class="col col-2">
                    <label class="input state-disabled">
                      <input type="text" name="subjectBig" id="subjectBig" disabled placeholder="合同标的（大写）" value="<s:property value="contract.subjectBig"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    合同日期
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input  placeholder="合同日期" id="date" name="date" disabled type="text" value="<s:date name="contract.date" format="yyyy-MM-dd"/>">
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    合同主要事宜
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input type="text" name="mainThing" id="mainThing" disabled placeholder="合同主要事宜" value="<s:property value="contract.mainThing"/>" >
                    </label>
                  </section>
                </div>

                <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    附件上传
                  </label>
                  <section class="col col-5">
                    <label class="input state-disabled">
                      <input name="uploadify" id="filename" style="display: none" placeholder="" type="file" >
                      <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                    </label>
                  </section>
                </div>

                <s:if test="flowNumStatus==2">
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要会签
                    </label>
                    <section class="col col-5">
                      <div class="inline-group">
                        <label class="radio">
                          <input type="radio"  name="mulExam" value="1" >
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  name="mulExam" value="0" >
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>

                  <div class="row" <s:if test='contract == null || contract.mulExam!="1"'>style="display:none"</s:if> id="showExamUsers">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      <a href="javascript:void(0);" key="btn-choose-examUsers">会签人员</a>
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text" id="examUsers" name="examUsers"
                               value="<s:iterator id="list" value="contract.examUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                        <input type="hidden" id="examUsersId" name="examUsersId"
                               value="<s:iterator id="list" value="contract.examUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要党政联合会议
                    </label>
                    <section class="col col-5">
                      <div class="inline-group">
                        <label class="radio">
                          <input type="radio"  name="partyExam" value="1" >
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  name="partyExam" value="0" >
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>

                </s:if>

                <s:if test="flowNumStatus==4">
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要上报中心
                    </label>
                    <section class="col col-5">
                      <div class="inline-group">
                        <label class="radio">
                          <input type="radio"  name="leaderExam" value="1" >
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  name="leaderExam" value="0" >
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>
                </s:if>

                <s:if test="flowNumStatus>2">
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要会签
                    </label>
                    <section class="col col-5">
                      <div class="inline-group state-disabled">
                        <label class="radio">
                          <input type="radio" disabled  value="1" <s:property value="contract.mulExam==1?'checked':''"/>>
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio" disabled  value="0" <s:property value="contract.mulExam==0?'checked':''"/>>
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>

                  <div class="row" <s:if test='contract == null || contract.mulExam!="1"'>style="display:none"</s:if>>
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      会签人员
                    </label>
                    <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled type="text"  name="examUsers"
                               value="<s:iterator id="list" value="contract.examUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                        <input type="hidden"  name="examUsersId"
                               value="<s:iterator id="list" value="contract.examUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                  </div>

                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要党政联合会议
                    </label>
                    <section class="col col-5">
                      <div class="inline-group state-disabled">
                        <label class="radio">
                          <input type="radio"  disabled name="partyExam" value="1" <s:property value="contract.partyExam==1?'checked':''"/>>
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  disabled name="partyExam" value="0" <s:property value="contract.partyExam==0?'checked':''"/>>
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>

                </s:if>
                <s:if test="flowNumStatus>4">
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否需要上报中心
                    </label>
                    <section class="col col-5">
                      <div class="inline-group state-disabled">
                        <label class="radio">
                          <input type="radio"  disabled name="leaderExam" value="1" <s:property value="contract.leaderExam==1?'checked':''"/>>
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  disabled name="leaderExam" value="0" <s:property value="contract.leaderExam==0?'checked':''"/>>
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>
                </s:if>


              </fieldset>

            </form>
            <div class="flow">
              <s:if test="contract.getProcessState().name()=='Running'">
                <div class="f_title">审批意见</div>
                <div class="chat-footer"style="margin-top:5px">
                  <div class="textarea-div">
                    <div class="typearea">
                      <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                    </div>
                  </div>
                  <span class="textarea-controls"></span>
                </div>
              </s:if>
              <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
              <div class="f_content" style="display:none">
                <div id="showFlow"></div>
              </div>
              <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
              <div class="f_content" style="display:none">
                <div id="showNext"></div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>


</div>

<script>
  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "contract"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("contract","../manage/ajax-contract!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("contract","../manage/ajax-contract!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          if($("#contract").valid()){
            form_save("contract","../manage/ajax-contract!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
              $("#btn-re-common").trigger("click");
            })
          }
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("contract", "../manage/ajax-contract!approve3.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "4":
        //第四步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("contract", "../manage/ajax-contract!approve4.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "5":
        //第五步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("contract", "../manage/ajax-contract!approve5.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "6":
        //第六步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("contract", "../manage/ajax-contract!approve6.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "7":
        //第七步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("contract", "../manage/ajax-contract!approve7.action?comment=" + encodeURIComponent($("textarea#chat_textarea-expand").val()), null, function () {
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-contract!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
      var vActionUrl="../manage/ajax-contract!deny.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });
  //上传文件
  readLoad({
    objId:"filename",
    entityName:"fileIds",
    sourceId:"fileId"
  });
  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!contract.action?viewtype=<s:property value="viewtype"/>",$('#content'));
    }

  });
  //是否需要会签
  $(":radio[name='mulExam']").click(function(){
    if($(this).val() == "1"){
      $("#showExamUsers").show();
    }else{
      $("#showExamUsers").hide();
    }
  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("../manage/ajax-contract!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

  //校验
  $("#contract").validate({
    rules : {
      mulExam : {
        required : true
      },
      examUsersId:{
        required : function(){
          if($(':checked[name="mulExam"]').val() == "1"){
            return true;
          }else{
            return false;
          }
        }
      },
      partyExam : {
        required : true
      },
      leaderExam:{
        required : true
      }
    },
    messages : {
      mulExam : {
        required : "请选择是否需要会签"
      },
      examUsersId:{
        required : "请选择会签人员"
      },
      leaderExam:{
        required : "请选择是否需要上报中心"
      },
      partyExam : {
        required : "请选择是否需要党政联合会议"
      }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      if(element.is(":radio")){
        element.parents("div.inline-group").append(error);
      }else {
        error.insertAfter(element.parent());
      }
    }
  });


  //会签人员
  $("a[key=btn-choose-examUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择会签人员",
      url:"ajax-dialog!levelUser.action?key=examUsers&more=1",
      width:560
    }).show();
  });
</script>