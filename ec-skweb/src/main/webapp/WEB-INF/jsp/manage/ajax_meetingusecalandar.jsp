<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />

<div class="row"  id='calendar'>

</div>

<script type="text/javascript">
  $('#calendar').fullCalendar({
    theme: false,
    lang: 'zh-cn',
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultDate: '<s:date name="today" format="yyyy-MM-dd"/> ',
    editable: false,
    eventLimit: true, // allow "more" link when too many events
    events: {
      url: '<%=path%>/manage/ajax-meetinguse!getCalendarJson.action',
      error: function() {
      //  $('#script-warning').show();
      }
    },
    eventClick: function(calEvent, jsEvent, view) {
      loadURL("../manage/ajax-meetinguse!read.action?keyId="+calEvent+"&calandar=1".id,$('#content'));

    },

    loading: function(bool) {
      //$('#loading').toggle(bool);
    }
  });


</script>

















