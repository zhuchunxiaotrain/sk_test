<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      发文管理
    </h1>
  </div>
</div>

<div id="dispatch_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
            <shiro:hasAnyRoles name="wechat">
              <s:if test="viewtype==1">
                <a id="ajax_dispatch_btn_add" <s:property value="isCreate('dispatch')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建发文申请表</a>
              </s:if>
              <s:if test="viewtype==2">
               <a id="ajax_dispatch_invalid" <s:property value="isCreate('dispatch')"/> class="btn btn-default pull-right" href="javascript:void(0)"> <i class="fa fa-lg fa-hand-o-right"></i> 失效 </a>
             </s:if>
            </shiro:hasAnyRoles>
        </form>
        <input type="hidden"  id="chooseId" />
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_dispatch_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_dispatch_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_dispatch_jqGrid();
    //失效操作
    $('#ajax_dispatch_invalid').click(function(){
      var ids=$("#ajax_dispatch_table").jqGrid('getGridParam','selarrrow');
      if(ids.length != 1){
        alert("请选择一条发文申请表");
        return false;
      }
      $.tzDialog({title:"提示",content:"确认要将此发文申请表失效吗?",callback:function(){
        var vActionUrl = "<%=path%>/manage/ajax-dispatch!doInvalid.action";
        var data={keyId:ids[0]};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          jQuery("#ajax_dispatch_table").trigger("reloadGrid");
        });

      }});
    });

  });


  function load_dispatch_jqGrid(){
    jQuery("#ajax_dispatch_table").jqGrid({
      url:'../manage/ajax-dispatch!list.action?viewtype='+"<s:property value="viewtype" />",
      datatype: "json",
      colNames:['发文类别',"文件编号",'发文标题',"发文时间","文档状态","操作","id"],
      colModel:[
        {name:'type',index:'type_name', width:100,sortable:false,search:true},
        {name:'fileNo',index:'fileNo', width:150,search:true,sortable:true},
        {name:'title',index:'title', width:250,search:true,sortable:true},
        {name:'time',index:'time',sortable:false,search:true,width:120},
        {name:'state',index:'state', search:false,sortable:true,width:80},
        {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
        {name:'id',index:'id',search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_dispatch_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        var ids=$("#ajax_dispatch_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_dispatch_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_dispatch_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_dispatch_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_dispatch_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_dispatch_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 发文管理",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_dispatch_table").jqGrid('setGridWidth', $("#ajax_dispatch_list_row").width());
    })
    jQuery("#ajax_dispatch_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_dispatch_table").jqGrid('navGrid', "#ajax_dispatch_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_dispatch_read(id){
    loadURL("../manage/ajax-dispatch!read.action?keyId="+id+"&viewtype="+"<s:property value="viewtype" />",$('#content'));
  }

  $("#ajax_dispatch_btn_add").off("click").on("click",function(){
    loadURL("../manage/ajax-dispatch!input.action",$('#content'));
  });
</script>





















