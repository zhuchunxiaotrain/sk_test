<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26 
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid = session.getId();
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->

        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common4" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>

          <hr class="simple">
          <form id="meetingleave" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="meetingleave.meetingUse.id" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="meetingleave.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              会议请假登记表&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  请假人员
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="creater" id="creater" placeholder="请输入请假人员" value="<s:property value="meetingleave.creater.name"/>" >
                  </label>
                </section>
              </div>


              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  请假原因
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  id="reason" name="reason"  disabled
                            type="text" value="<s:property value="meetingleave.reason"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  请假时间
                </label>
                <section class="col col-2">
                  <label class="input state-disabled">
                    <input  id="createDate" name="createDate"  disabled
                            type="text" value="<s:date name="meetingleave.createDate" format="yyyy-MM-dd HH:mm"/>">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>


        </div>
      </div>

  </article>
</div>

<script>
  //返回视图
  $("#btn-re-common3").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("../com/ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("../com/ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("../com/ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("../com/ajax!draftList.action",$('#content'));
    }else{
      loadURL("../manage/ajax!meetingleave.action",$('div#s3'));
    }

  });


  //返回视图
  $("#btn-re-common4").click(function(){
    if(draft == 1){
      location.href='index.action';
    }else{
      loadURL("../manage/ajax!meetingleave.action",$('div#s4'));
    }

  });




</script>
