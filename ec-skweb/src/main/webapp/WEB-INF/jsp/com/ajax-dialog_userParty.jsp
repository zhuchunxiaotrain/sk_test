<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="selectedIds" value=""/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_user_list_row">
      <table id="ajax_user_list_table">
      </table>
      <div id="ajax_user_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>
  function run_jqgrid_function(){
    jQuery("#ajax_user_list_table").jqGrid({
      url:'ajax-users!list.action',
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['用户名称','Id'],
      colModel : [
        {name:'name',index:'name', width:240,sortable:false,search:true,sorttype:'string'},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_user_list_page',
      sortname : 'createDate',
      sortorder : "desc",
      gridComplete:function(){
          var ids=$("#ajax_user_list_table").jqGrid('getDataIDs');
          var param="<s:property value="param"/>";
          var usersId=$("#usersId").val();
          for(var i=0;i<ids.length;i++) {
              var cl = ids[i];
              if (usersId != "" && usersId != null) {
                  var idsArr = usersId.split(",");
                  for (var s = 0; s < idsArr.length; s++) {
                      if (idsArr[s] == cl) {
                         // alert("s"+s);
                          $("#ajax_user_list_table").jqGrid("setSelection", cl);
                      }
                  }
              }
          }
        jqGridStyle();
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      },
        onSelectRow: function (rowId, status, e) {
            $(this).gridselect('onecheck',{
                table:"ajax_user_list_table",
                rowId:rowId,
                status:status,
                id:"usersId",
                name:"usersName"
            });
            /*var usersId="";
            var usersName="";
            var flag=false;
            var param="<s:property value="param"/>";
            if(param=="partymoney"){
                alert("param="+param);
                usersId="partyMoneyUsersId";
                usersName="partyMoneyUsersName11";
                flag=true;
            }*/



            /*if(flag==true){
                $(this).gridselect('onecheck',{
                    table:"ajax_power_list_table",
                    rowId:rowId,
                    status:status,
                    id:usersId,
                    name:usersName,
                    type:"radio"
                });
            }else {
                $(this).gridselect('onecheck',{
                    table:"ajax_user_list_table",
                    rowId:rowId,
                    status:status,
                    id:usersId,
                    name:usersName,
                    type:"checkbox",
                });

            }*/

        },

        onSelectAll:function(aRowids, status, e){
            $(this).gridselect('morecheck',{
                table:"ajax_user_list_table",
                rowId:aRowids,
                status:status,
                id:"usersId",
                name:"usersName",
            });
        },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_user_list_table").jqGrid('setGridWidth', $("#ajax_user_list_row").width()-10);
    })

    jQuery("#ajax_user_list_table").jqGrid('navGrid', "#ajax_user_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_user_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
//      var slt =   $("#ajax_user_list_table").jqGrid('getGridParam','selarrrow');
//      var usersName = "";
//      for(var i=0; i<slt.length; i++){
//            var rowDatas = $("#ajax_user_list_table").jqGrid('getRowData', slt[i]);
//            usersName = usersName + rowDatas['name'] + ",";
//      }
//       usersName = usersName.substring(0,usersName.length-1);
//       $("#usersName").val(usersName);
       gDialog.fClose();
      if($("#usersId").val()==""){
          $("#partyMoneyDepartName").val("");
          $("#payPartyMoney").remove();
          alert(222);
      }
  });
</script>
