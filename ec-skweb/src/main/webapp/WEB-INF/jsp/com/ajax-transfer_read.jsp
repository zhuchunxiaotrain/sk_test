<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-sm-12 col-md-12 col-lg-12">
        <div class="jarviswidget well" id="wid-id-3"
             data-widget-colorbutton="false"
             data-widget-editbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-custombutton="false"
             data-widget-sortable="false">
            <!-- widget div-->
            <div>
                <div class="widget-body">
                    <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i> 调动申请</a>
                    <div id="myTabContent1" class="tab-content padding-10 ">
                            <div class="row">
                                <shiro:hasAnyRoles name="wechat">
                                    <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(change.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
                                </shiro:hasAnyRoles>
                            </div>
                            <hr class="simple">
                            <form id="transfer" class="smart-form" novalidate="novalidate" action="" method="post">
                                <input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
                                <input type="hidden" name="keyId" id="keyId" value="<s:property value="change.id" />"/>
                                <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                                <header  style="display: block;">
                                    调动申请&nbsp;&nbsp;<span id="title"></span>
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <label class="label col col-2">
                                            <i class="fa fa-asterisk txt-color-red"></i>
                                             调动人员
                                        </label>
                                        <section class="col col-5">
                                            <label class="input">
                                                <label class="input state-disabled">
                                                    <input disabled type="text" id="name" name="name" placeholder="请选择调动人员"
                                                           value="<s:property value="change.name"/>"/>
                                                </label>

                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <label class="label col col-2">
                                            <i class="fa fa-asterisk txt-color-red"></i>
                                            用工性质
                                        </label>
                                        <section class="col col-5">
                                            <label class="input state-disabled">
                                                <input disabled type="text" name="natureValue" id="natureValue" placeholder="请输入用工性质" value="<s:property value="natureValue"/>" >
                                            </label>
                                        </section>
                                    </div>

                                    <div class="row">
                                        <label class="label col col-2">
                                            <i class="fa fa-asterisk txt-color-red"></i>
                                            调往部门
                                        </label>
                                        <section class="col col-5">
                                            <label class="input">
                                                <label class="input state-disabled">
                                                    <input disabled type="text" id="transferDepartName" name="departName" placeholder="请选择调往部门"
                                                           value="<s:property value="change.department.name"/>"/>
                                                    <input type="hidden" id="transferDepartId" name="departId"
                                                           value="<s:property value="change.department.id"/>"/>
                                                </label>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <label class="label col col-2">
                                            <i class="fa fa-asterisk txt-color-red"></i>
                                            岗位
                                        </label>
                                        <section class="col col-5">
                                            <label class="input">
                                                <label class="input state-disabled">
                                                    <input disabled type="text" id="postName" name="postName" placeholder="请选择调往岗位"
                                                           value="<s:property value="change.post.name"/>"/>
                                                    <input type="hidden" id="postId" name="postId"
                                                           value="<s:property value="change.post.id"/>"/>
                                                </label>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <label class="label col col-2">
                                            调动原因
                                        </label>
                                        <section class="col col-5">
                                            <label class="input state-disabled">
                                                <input type="text" disabled name="reason" id="reason" placeholder="请输入调动原因" value="<s:property value="change.reason"/>" >
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <label class="label col col-2">
                                            <i class="fa fa-asterisk txt-color-red"></i>
                                            原薪资
                                        </label>
                                        <section class="col col-5">
                                            <label class="input state-disabled">
                                                <input type="text" disabled name="oldMoney" id="oldMoney" placeholder="请输入原薪资" value="<s:property value="change.oldMoney"/>" >
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <label class="label col col-2">
                                            调用薪资
                                        </label>
                                        <section class="col col-5">
                                            <label class="input state-disabled">
                                                <input type="text" disabled name="money" id="money" placeholder="请输入调用薪资" value="<s:property value="change.money"/>" >
                                            </label>
                                        </section>
                                    </div>

                                    <div class="row">
                                        <label class="label col col-2">
                                            <i class="fa fa-asterisk txt-color-red"></i>
                                            会签人
                                        </label>
                                        <section class="col col-5">
                                            <label class="input">
                                                <label class="input state-disabled">
                                                    <input disabled type="text" id="usersName" name="nextStepApproversName"
                                                           value="<s:property value="change.nextStepApproversName"/>"/>
                                                    <input type="hidden" id="usersId" name="nextStepApproversId"
                                                           value="<s:property value="change.nextStepApproversId"/>"/>
                                                </label>
                                            </label>
                                        </section>
                                    </div>

                                    <div class="row">
                                        <label class="label col col-2">
                                            操作人
                                        </label>
                                        <section class="col col-4">
                                            <label class="input state-disabled">
                                                <input  disabled type="text"  id="creater" placeholder="" value="<s:property value="change.creater.name"/>" >
                                            </label>
                                        </section>
                                        <label class="label col col-2">
                                            操作日期
                                        </label>
                                        <section class="col col-4 ">
                                            <label class="input state-disabled">

                                                <input  disabled type="text" name="createDate" id=createDate" placeholder="" value="<s:date name="change.createDate" format="yyyy-MM-dd"/>" >
                                            </label>
                                        </section>
                                    </div>

                                </fieldset>

                            </form>
                            <div class="flow">
                                <s:if test="change.getProcessState().name()=='Running'">
                                    <div class="f_title">审批意见</div>
                                    <div class="chat-footer"style="margin-top:5px">
                                        <div class="textarea-div">
                                            <div class="typearea">
                                                <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                                            </div>
                                        </div>
                                        <span class="textarea-controls"></span>
                                    </div>
                                </s:if>
                                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                                <div class="f_content" style="display:none">
                                    <div id="showFlow"></div>
                                </div>
                                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                                <div class="f_content" style="display:none">
                                    <div id="showNext"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </article>
</div>

<script>

    //返回视图
    $("#btn-re-common").click(function(){
        var index = "<s:property value="index" />";
        var todo = "<s:property value="todo" />";
        var remind = "<s:property value="remind" />";
        var record = "<s:property value="record" />";
        var draft = "<s:property value="draft" />";
        if(index==1){
            loadURL("ajax-index!page.action",$('#content'));
        }else if(todo==1){
            loadURL("ajax!toDoList.action",$('#content'));
        }else if(remind==1){
            loadURL("ajax!remindList.action",$('#content'));
        }else if(record==1){
            loadURL("ajax!taskRecordList.action?type=1",$('#content'));
        }else if(record==2){
            loadURL("ajax!taskRecordList.action?type=2",$('#content'));
        }else if(draft==1){
            loadURL("ajax!draftList.action",$('#content'));
        }else{
            loadURL("ajax-change.action?viewtype=<s:property value="viewtype"/>",$('#content'));
        }

    });

    $(function(){
        loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
        loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
        ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
            var showDuty = false;
            var area = $("span.textarea-controls");
            $(pdata.data.datarows).each(function(i,v){
                var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
                $(area).append(str);
                if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                    showDuty = true;
                }
            });
            if(showDuty == true){
                var pdata = {
                    keyId: $("input#keyId").val(),
                    flowName: "transfer"
                };
                multiDuty(pdata);
            }
        });
        var valueObj=$("input#numStatus").val();
        switch(valueObj){
            case "0":
                //保存后再提交
                $("#left_foot_btn_approve").off("click").on("click",function(){
                    form_save("transfer","ajax-transfer!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                        $("#btn-re-common").trigger("click");
                    })
                });
                break;
            case "1":
                //第一步审批
                $("#left_foot_btn_approve").off("click").on("click",function(){
                    form_save("transfer","ajax-transfer!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                        $("#btn-re-common").trigger("click");
                    });
                });
                break;
            case "2":
                //第二步审批
                $("#left_foot_btn_approve").off("click").on("click",function(){
                    form_save("transfer","ajax-transfer!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                        $("#btn-re-common").trigger("click");
                    })
                });
                break;
            case "3":
                //第三步审批
                $("#left_foot_btn_approve").off("click").on("click",function(){
                    form_save("transfer","ajax-transfer!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                        $("#btn-re-common").trigger("click");
                    })
                });
                break;
        }
        //退回
        $("#left_foot_btn_sendback").off("click").on("click",function(){
            var vActionUrl="ajax-transfer!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
            var data={keyId:$("input#keyId").val()};
            ajax_action(vActionUrl,data,{},function(pdata){
                _show(pdata);
                $("#btn-re-common").trigger("click");
            });
        });
        //否决
        $("#left_foot_btn_deny").off("click").on("click",function(){
            var vActionUrl="ajax-transfer!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
            var data={keyId:$("input#keyId").val()};
            ajax_action(vActionUrl,data,{},function(pdata){
                _show(pdata);
                $("#btn-re-common").trigger("click");
            });
        });
        //流程信息展开
        $('#flow,#next').click(function(){
            if($(this).hasClass("right")){
                $(this).removeClass("right").addClass("down");
                $(this).parent(".f_title").next("div.f_content").show();
            }else{
                $(this).removeClass("down").addClass("right");
                $(this).parent(".f_title").next("div.f_content").hide();
            }
        });
    });

    //编辑
    $("a[key=ajax_edit]").click(function(){
        var draft = "<s:property value="draft" />";
        loadURL("ajax-transfer!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
    });
    
</script>
