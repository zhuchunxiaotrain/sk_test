
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<div class="modal-body">
<input type="hidden" name="keyId" id="userId" value="<s:property value="keyId" />">
<fieldset>
	<legend>
	  <span class="label  label-warning "> [请选择部门岗位信息，只做添加操作]</span>
	</legend>
	<table>
	    <tr>
	        <td>
	            <ul class="list-group td-ul" id="ajax_users_config_depart">
                        <s:iterator value="departArrayList" id="list">
                           <li id="<s:property value="#list.departId"/>" class="list-group-item td-li">
                                <div class="checkbox">
                                    <label>
                                      <input id="<s:property value="#list.departId"/>" type="checkbox" class="checkbox style-0" <s:property value="#list.checked"/>>
                                      <span><s:property value="#list.departName" /></span>
                                    </label>
                                </div>

                            </li>
                        </s:iterator>
                </ul>
	        </td>
	        <td>
                <ul class="list-group td-ul" id="ajax_users_config_post">
                    <s:iterator value="postArrayList" id="list">
                       <li class='list-group-item td-li' id="<s:property value="#list.id"/>">
                            <div class='checkbox'>
                               <label>
                                   <input id="<s:property value="#list.id"/>" type='checkbox' class='checkbox style-0' <s:property value="#list.checked"/>>
                                   <span id="<s:property value="#list.name"/>"><s:property value="#list.name"/></span>
                                </label>
                            </div>
                       </li>
                    </s:iterator>
                </ul>

	        </td>
	    </tr>
	</table>
</fieldset>
</div>
<div class="modal-footer">
	<a href="#../com/ajax!users.action" class="btn btn-primary" id="dialog-ok">确定</a>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>
    //loadDataTableScripts();
	var actionUrl = "../com/ajax-users!configPost.action";
	var keyId = $("input#userId").val();
	multList("ajax_users_config_depart","ajax_users_config_post",actionUrl);
    $("#dialog-ok").unbind("click").bind("click",function(){
        var actionUrl = "<%=path%>/com/ajax-users!config.action";
        var rowData = getRowIds("ajax_users_config_post");
        var data={rowIds:rowData,keyId:keyId};
        ajax_action(actionUrl,data,{},function(pdata){
            _show(pdata);
        });
        gDialog.fClose();
        jQuery("#ajax_users_table").trigger("reloadGrid");
    });

</script>
