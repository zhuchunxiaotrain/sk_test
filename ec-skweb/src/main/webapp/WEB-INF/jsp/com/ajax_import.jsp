<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/3/2
  Time: 15:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<div>
    <!-- rows -->
    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                    <a class="btn btn-default btn-sm" id="btn_back" href="javascript:void(0);"><i class="fa fa-lg fa-mail-reply-all"></i> 返回</a>
                </div>
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-togglebutton="false"
                     data-widget-deletebutton="false"
                     data-widget-fullscreenbutton="true"
                     data-widget-custombutton="false"
                     data-widget-sortable="false">
                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                    -->
                    <%--<header>--%>
                        <%--<span class="widget-icon"> <i class="fa fa-table"></i> </span>--%>
                        <%--<h2>导出执行列表</h2>--%>
                    <%--</header>--%>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body no-padding">

                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class=" ">
                                    <div class="row" id="ajax_import_list_row">

                                        <table id="ajax_import_table" class="table table-striped table-bordered table-hover">
                                        </table>
                                        <div id="ajax_import_list_page">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->

        <!-- end row -->

    </section>
<!-- end widget grid -->
</div>

<script type="text/javascript">
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    pageSetUp();


    function import_reload(){
        loadURL("ajax!import.action",$('#content'));
    }


</script>
<script type="text/javascript">
    $(function(){
        load_import_jqGrid();
    });
    function load_import_jqGrid(){
        jQuery("#ajax_import_table").jqGrid({
            url:'ajax-import!datalist.action',
            datatype: "json",
            colNames:['操作时间',"操作名称","操作","id"],
            colModel:[
                {name:'createDate',index:'createDate', width:80},
                {name:'name',index:'name', width:80,sortable:false},
                {name:'act',index:'act', width:210,sortable:false,fixed:true},
                {name:'id',index:'id',hidden:true},
            ],
            rowNum : 10,
            rowList:[10,20,30],
            pager : '#ajax_import_list_page',
            sortname : 'id',
            sortorder : "asc",
            gridComplete:function(){
                var ids=$("#ajax_import_table").jqGrid('getDataIDs');
                for(var i=0;i<ids.length;i++){
                    var cl=ids[i];
                    var rowData = $("#ajax_import_table").jqGrid("getRowData",cl);
                    var de="<a class='btn btn-default pull-right pull-right-fix' key='ajax_edit' href='file.action?gridId="+cl+"'><i class='fa fa-lg fa-edit'></i> 下载</a>";
                    jQuery("#ajax_import_table").jqGrid('setRowData',ids[i],{act:de});
                }

                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");

                jqGridStyle();
            },
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },
            caption : "<i class='fa fa-arrow-circle-right'></i>下载中心",
            multiselect : false,
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            viewrecords: true,
            autowidth: true,
            height:'auto',
            forceFit:true,
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_import_table").jqGrid('setGridWidth', $("#ajax_import_list_row").width());
        })

        jQuery("#ajax_import_table").jqGrid('navGrid', "#ajax_import_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });
    };
    //删除
    function fn_import_actions_delete(id){
        fn_delete(id);
    };

    function fn_down_file(id){
        loadURL("ajax-import!download.action?keyId="+id,$('#content'));
    }

    function fn_import_destroy(id){
        console.debug(id);
        var vActionUrl = "<%=path%>/com/ajax-import!delete.action";
        data={keyId:id};
        ajax_action(vActionUrl,data,{},function(pdata){
            _show(pdata);
        });
        jQuery("#ajax_import_table").trigger("reloadGrid");
    }

    $("#btn_back").click(function(){
       loadURL("ajax!client.action",$("#content"));
    });
</script>