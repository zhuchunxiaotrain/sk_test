
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<input type="hidden" name="keyId" id="keyId" value="<s:property value="keyId" />">
<fieldset>
    <ul class="list-group" id="ajax_query">
        <s:iterator value="dutyArrayList" id="list">
           <li id="<s:property value="#list.id"/>" class="list-group-item">
                <div class="checkbox" style="display:inline">
                    <label>
                      <span>
                        <input type="checkbox" id="<s:property value="#list.id"/>" <s:property value="#list.checked"/> name="ck_duty_info"/>
                        <s:property value="#list.department.name" /><s:if test="#list.post==null || #list.post.isEmpty()"></s:if><s:else>(<s:property value="#list.post.name" />)</s:else>
                      </span>
                    </label>
                    <a title="删除" key="ajax_duty_actions_delete" class="btn bg-color-red txt-color-white btn-xs pull-right"
                                                                                      href="javascript:void(0);"><i class="fa fa-times"></i>删除</a>
                </div>

            </li>
        </s:iterator>

    </ul>
</fieldset>
<script>
	//loadDataTableScripts();
	$("input[name='ck_duty_info']").unbind("click").bind("click",function(){
	    if($(this).prop("checked")){
	        $("input[name='ck_duty_info']:checked").attr("checked",false);
            $(this).prop('checked',true);
	    }

	    var keyId=$(this).attr("id");
	    var vActionUrl = "<%=path%>/com/ajax-users!defaultDuty.action";
        data={keyId:keyId, userId:$('#keyId').val()};
        ajax_action(vActionUrl,data,{},function(pdata){
            _show(pdata);
        });
        jQuery("#ajax_users_table").trigger("reloadGrid");
	});

    $("a[key=ajax_duty_actions_delete]").click(function(){
        var id=$(this).parent().parent();
        var vActionUrl = "<%=path%>/com/ajax-users!deleteDuty.action";
		data={keyId:id.attr("id") };
		ajax_action(vActionUrl,data,{},function(pdata){
            _show(pdata);
        });
		loadURL("../com/ajax-users!leftView.action?keyId="+$("input#keyId").val(),$('#ajax_query'));
        jQuery("#ajax_users_table").trigger("reloadGrid");
    });
</script>
