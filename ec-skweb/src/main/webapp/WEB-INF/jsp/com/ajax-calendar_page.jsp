<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<jsp:include page="ajax-top.jsp" />

<hr style="height:1px;border-top:1px solid #ccc;"/>

<div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
            <a id="ajax_btn_add"  class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建日程安排</a>
        </form>
      </div>
    </div>
  </div>
<div class="row"  id='calendar'>

</div>

<script type="text/javascript">
    $('#calendar').fullCalendar({
        theme: false,
        lang: 'zh-cn',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },

        defaultDate: '<s:date name="today" format="yyyy-MM-dd"/> ',
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        events: {
            url: '<%=path%>/com/ajax-leave!getLeaveJson.action',
            error: function() {
                $('#script-warning').show();
            }
        },

        loading: function(bool) {
            //$('#loading').toggle(bool);
        }
    });

</script>






















