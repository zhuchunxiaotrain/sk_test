<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<ul class="notification-body">
	<s:if test="taskArrayList.size() > 0">
		<s:iterator value="taskArrayList" id="list" status="u">
			<li>
				<span class="">
					<a key="ajax_todo_action_edit" href="javascript:void(0);" mpUrl="<s:property value="#list.url"/>" class="msg">
						<img src="../resource/com/img/avatars/male.png" alt="" class="air air-top-left margin-top-5" width="40" height="40" />
						<span class="from">
						    <i class="fa fa-user txt-color-green"></i>
						    <s:property value="#list.toUser"/>
						</span>
						<time><s:property value="#list.time"  /></time>
						<span class="subject">
							<i class="fa fa-flag-checkered"></i>
							<s:property value="#list.module"/> -  <s:property value="#list.pname"/>
						</span>
						<span class="msg-body">
							<i class="fa fa-paperclip"></i>
							<s:property value="#list.msg"/>
						</span>
					</a>
				</span>
			</li>
		</s:iterator>
	</s:if>
	<s:else>
		<h4> <i class="fa fa-warning"></i> 没有@我的待办信息！</h4>
	</s:else>
</ul>
<script>
    $("a[key=ajax_todo_action_edit]").click(function(){
        $("div.ajax-dropdown").hide();
        var url = $(this).attr("mpUrl");
        loadURL(url,$('#content'));
    });

    $("a[key=ajax_draft_action_edit]").click(function(){
        $("div.ajax-dropdown").hide();
        var url = $(this).attr("mpUrl");
        loadURL(url,$('#content'));
    });

    $("a[key=ajax_draft_delete]").click(function(){
        var vActionUrl = "/ec-skweb/com/ajax-flow!draftDelete.action";
        data={keyId:$("#keyId1").val(),key:$(this).attr("data")};
        ajax_action(vActionUrl,data,{},function(pdata){
            _showResult(pdata);
        });
        loadURL("../com/ajax-flow!draftList.action",$(".ajax-notifications.custom-scroll"));
    });
</script>