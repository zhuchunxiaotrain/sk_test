<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="ajax-top.jsp" />
<input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>项目信息
          </a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 基本情况 </a>
            </li>
            <li class="disabled">
              <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 政治面貌 </a>
            </li>
            <li>
              <a href="#s3" id="other2" data-toggle="tab"><i class="fa fa-fw fa-lg fa-calendar"></i> 培训记录 </a>
            </li>
            <li>
              <a href="#s4" id="other3" data-toggle="tab"><i class="fa fa-fw fa-lg fa-calendar"></i> 人员履历 </a>
            </li>
            <li>
              <a href="#s5" id="other4" data-toggle="tab"><i class="fa fa-fw fa-lg fa-calendar"></i> 奖惩记录 </a>
            </li>
            <li>
              <a href="#s6" id="other5" data-toggle="tab"><i class="fa fa-fw fa-lg fa-calendar"></i> 考核记录 </a>
            </li>
          </ul>

          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
              <div class="row">
                  <a class="btn btn-default pull-right pull-right-fix"  key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
              </div>

          <hr class="simple">
          <form id="employees" class="smart-form" novalidate="novalidate" action="" method="post">
            <input  type="hidden" name="keyId" id="keyId" value="<s:property value="employees.id" />"/>
            <input  type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              项目基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled id="name" name="name" type="text" value="<s:property value="employees.name"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  性别
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text"  name="sex" id="gender" placeholder="根据身份证识别" value="<s:property value="employees.gender"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  年假
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="annuaLeaveNum" id="annuaLeaveNum" placeholder="请输入年假" value="<s:property value="leaveDetail.annuaLeave"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  籍贯
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled name="nativePlace" id="nativePlace" placeholder="请输入籍贯" value="<s:property value="employees.nativePlace"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  民族
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled name="nation" id="nation" placeholder="请输入民族" value="<s:property value="employees.nation"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  用人性质

                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled name="nature" id="nature"  value="<s:property value="nature"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同开始日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="startDate" name="startDate" disabled placeholder="请选择合同开始日期"
                           type="text" value="<s:date name="employees.startDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>

                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同结束日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="endDate" name="endDate" disabled placeholder="请选择合同结束日期"
                           type="text" value="<s:date name="employees.endDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  试用开始日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  id="trialStart" name="trialStart" disabled placeholder="请选择试用期开始日期"
                            type="text" value="<s:date name="employees.trialStart" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  试用结束日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  id="trialEnd" name="trialEnd" disabled placeholder="请选择试用结束日期"
                            type="text" value="<s:date name="employees.trialEnd" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  所属部门
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text"  disabled name="depName" id="depName"  value="<s:property value="employees.dep.name"/>" >
                    <input type="hidden"  name="depId" id="depId" value="<s:property value="employees.dep.id"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  岗位
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="postName" id="postName"  value="<s:property value="employees.empPost.name"/>" >
                    <input type="hidden"  name="postId" id="postId" value="<s:property value="employees.empPost.id"/>" >

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  基本薪资
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled name="money" id="money" placeholder="请输入基本薪资(元)" value="<s:property value="employees.money"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  出生日期
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  id="birthDate" name="birthDate" disabled placeholder="请选择出生日期"
                            type="text" value="<s:date name="employees.birthDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  户口所在地
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="houseLocation">
                    <label class="radio ">
                      <input type="radio" id="yes" disabled checked="checked" name="houseLocation" value="0" <s:property value="employees.houseLocation==0?'checked':''"/>>
                      <i></i>上海户口</label>
                    <label class="radio ">
                      <input type="radio" id="no" disabled name="houseLocation" value="1" <s:property value="employees.houseLocation==1?'checked':''"/>>
                      <i></i>非上海户口</label>
                  </div>
                </section>
              </div>
                <div class="row" <s:if test="employees.houseLocation==1">style="display:none"</s:if> id="showValid">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  居住证登记
                </label>
                <section class="col col-10">
                  <div class="inline-group" name="cardRegister">
                    <label class="radio">
                      <input type="radio" disabled checked="checked" name="cardRegister"  value="0" <s:property value="employees.cardRegister==0?'checked':''"/>>
                      <i></i>有</label>
                    <label class="radio">
                      <input type="radio" disabled name="cardRegister" value="1" <s:property value="employees.cardRegister==1?'checked':''"/>>
                      <i></i>无</label>
                  </div>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  居住证开始日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input   id="cardStart" name="cardStart" disabled placeholder="请选择居住证开始日期"
                             type="text" value="<s:date name="employees.cardStart" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  居住证结束日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  id="cardEnd" name="cardEnd" disabled placeholder="请选择居住证结束日期"
                            type="text" value="<s:date name="employees.cardEnd" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  身份证号
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="cardID" id="cardID" value="<s:property value="employees.cardID"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  婚姻情况
                </label>
                <section class="col col-4">
                  <div class="inline-group" name="maritalStatus">
                    <label class="radio">
                      <input type="radio" disabled checked="checked" name="maritalStatus" value="0" <s:property value="employees.maritalStatus==0?'checked':''"/>>
                      <i></i>已婚</label>
                    <label class="radio ">
                      <input type="radio" disabled name="maritalStatus" value="1" <s:property value="employees.maritalStatus==1?'checked':''"/>>
                      <i></i>未婚</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  移动电话
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="mobile" id="mobile"  placeholder="请输入移动电话" value="<s:property value="employees.mobile"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  电子邮件
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="email" id="email" placeholder="请输入电子邮件" value="<s:property value="employees.email"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  最高学历
                </label>
                    <section class="col col-4">
                      <label class="input state-disabled">
                        <input type="text"  disabled  value="<s:property value="employees.highestDegree.name"/>" >
                      </label>
                    </section>
                <label class="label col col-2">
                  毕业院校
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input disabled type="text" placeholder="请输入毕业院校" value="<s:property value="employees.school"/>" />
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  所学专业
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  id="profession" disabled type="text"  value="<s:property value="employees.profession"/>">

                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  参加工作时间
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input   id="workTime" name="workTime" disabled placeholder="请选择参加工作时间"
                             type="text" value="<s:date name="employees.workTime" format="yyyy-MM"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  户籍地址
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled id="permanentAddress" placeholder="请输入户籍地址" type="permanentAddress" value="<s:property value="employees.permanentAddress"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  联系电话
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled placeholder="请输入联系电话" name="tel" id="tel"  value="<s:property value="employees.tel"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  邮政编码
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" id="postcode" disabled name="postcode" placeholder="请输入邮政编码"  value="<s:property value="employees.postcode"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  居住地址
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  type="text" disabled id="address" name="address" placeholder="请输入居住地址" value="<s:property value="employees.address"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  紧急联络人
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  type="text"  disabled id="contactLinkman" name="contactLinkman" placeholder="请输入紧急联络人" value="<s:property value="employees.contactLinkman"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  紧急联络电话
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  type="text" disabled id="contactTel" name="contactTel" value="<s:property value="employees.contactTel"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  邮政编码
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  type="text" disabled id="postcode1" name="postcode1" placeholder="请输入邮政编码" value="<s:property value="employees.postcode1"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">身份证复印件</label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input name="section.talktext" id="talktext" placeholder="请输入会前2次沟通记录"
                           type="text" value="<s:property value="section.talktext"/>">
                  </label>--%>
                  <label class="input state-disabled">
                    <input  name="uploadify" id="copyfileName" style="display: none" type="file" disabled>
                    <input  name="copyfileId"  id="copyfileId" style="display: none" value="<s:property value="copyfileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input state-disabledt">
                    <input type="text" disabled name="remark" id="remark" placeholder="请输入备注" value="<s:property value="employees.remark"/>" >
                  </label>
                </section>
              </div>
              <s:iterator value="employees.familySet" id="list">

              <div class="row">
                <label class="label col col-1">
                  称谓
                </label>
                <label class="label col col-2">
                  姓名
                </label>
                <label class="label col col-1">
                  年龄
                </label>
                <label class="label col col-2">
                  政治面貌
                </label>
                <label class="label col col-2">
                  工作单位
                </label>
                <label class="label col col-1">
                  职务
                </label>
                <label class="label col col-2">
                  联系电话
                </label>
              </div>
              <div id="add">
                <div class="row">
                  <section class="col col-1">
                    <label class="input">
                      <input type="text" disabled name="appellation" id="appellation" placeholder="请输入称谓" value="<s:property value="#list.appellation"/>" >
                    </label>
                  </section>
                  <section class="col col-2">
                    <label class="input">
                      <input type="text" disabled name="name1" id="name1" placeholder="请输入姓名" value="<s:property value="#list.name"/>" >
                    </label>
                  </section>
                  <section class="col col-1">
                    <label class="input">
                      <input type="text" disabled name="age1" id="age1" placeholder="请输入年龄" value="<s:property value="#list.age"/>" >
                    </label>
                  </section>
                  <section class="col col-2">
                    <label class="input">
                      <input type="text" disabled name="politicalStatus" id="politicalStatus" placeholder="请输入政治面貌" value="<s:property value="#list.politicalStatus"/>" >
                    </label>
                  </section>
                  <section class="col col-2">
                    <label class="input">
                      <input type="text" disabled name="workAdd" id="workAdd" placeholder="请输入" value="<s:property value="#list.workAdd"/>" >
                    </label>
                  </section>
                  <section class="col col-2">
                    <label class="input">
                      <input type="text" disabled name="post1" id="post1" placeholder="请输入备注" value="<s:property value="#list.post"/>" >
                    </label>
                  </section>
                  <section class="col col-2">
                    <label class="input">
                      <input type="text" disabled name="tel1" id="tel1" placeholder="请输入联系电话" value="<s:property value="#list.tel"/>" >
                    </label>
                  </section>
                </div>
              </div>
             </s:iterator>
            </fieldset>
          </form>

        </div>
        <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
        <div class="tab-pane fade in active" id="s3" style="margin: 10px;"></div>
        <div class="tab-pane fade in active" id="s4" style="margin: 10px;"></div>
        <div class="tab-pane fade in active" id="s5" style="margin: 10px;"></div>
        <div class="tab-pane fade in active" id="s6" style="margin: 10px;"></div>
       </div>
    </div>
  </div>
      </div>
  </article>
</div>

<script type="text/javascript">
/*
      $('li.disabled').off("click").on("click", function () {
          return false;
      })
*/


    //上传
    inputLoad({
        objId:"copyfileId",
        entityName:"copyIDCardIds",
        sourceId:"copyfileId"
    });

    $("a[key='add-accept-common']").bind("click",function() {
        $("#add").append('<div class="row"><section class="col col-1"><label class="input"><input type="text" name="appellation" id="appellation" placeholder="请输入称谓" value="<s:property value="familySet.appellation"/>" ></label></section><section class="col col-2"><label class="input"><input type="text" name="name1" id="name1" placeholder="请输入姓名" value="<s:property value="familySet.name"/>" ></label></section><section class="col col-1"><label class="input"><input type="text" name="age" id="age" placeholder="请输入年龄" value="<s:property value="familySet.age"/>" ></label></section><section class="col col-2"><label class="input"><input type="text" name="politicalStatus" id="politicalStatus" placeholder="请输入政治面貌" value="<s:property value="familySet.politicalStatus"/>" ></label></section><section class="col col-2"><label class="input"><input type="text" name="workAdd" id="workAdd" placeholder="请输入" value="<s:property value="familySet.workAdd"/>" ></label></section><section class="col col-2"><label class="input"><input type="text" name="post1" id="post1" placeholder="请输入备注" value="<s:property value="familySet.post"/>" ></label></section><section class="col col-2"><label class="input"><input type="text" name="tel1" id="tel1" placeholder="请输入联系电话" value="<s:property value="familySet.tel"/>" ></label></section></div><a href="javascript:void(0)" style="font-size:16px" key="del-accept-common" class="del" > 删除</a> </label>')

    });


    $(document).on("click", '.del', function(){

        var div$=$("a.del");
        div$.attr("style","border: 1px solid red")
        //var acceptMoneys$= $("input[name='acceptMoneys']");
        //if(acceptMoneys$.length==1){
        //alert("至少有一个收款人")
        //  }else{
      /*$(this).parents('div[data-row="1"]').remove();*/
        $(this).closest("div").remove();
        //}
    });

    $('#startDate,#endDate,#trialStart,#trialEnd,#birthDate,#cardStart,#cardEnd,#workTime').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN',
        minView:2
    });

    var state = "<s:property value="employees.getProcessState().name()" />";
    if(state == "Finished"){
        $('#other1').parent('li').removeClass('disabled');
    }
    var table_global_width=0;
/*
    $("a#other1").off("click").on("click",function(e) {
        if(state == "Running"){
            if (table_global_width == 0) {
                table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
            }
            loadURL("ajax-political!read.action?keyId="+$("#keyId").val(),$('div#s2'));
        }else{
            return false;
        }

    });
*/

    $("a#other1").off("click").on("click",function(e){
        if (table_global_width == 0) {
            table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
        }
        loadURL("ajax-political!read.action?keyId="+$("#keyId").val(),$('div#s2'));
    });


  $("a#other2").off("click").on("click",function(e){
        if (table_global_width == 0) {
            table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s3").width();
        }
        loadURL("ajax-trainrecord.action?parentId="+$("#keyId").val(),$('div#s3'));
    });
  $("a#other3").off("click").on("click",function(e){
      if (table_global_width == 0) {
          table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s4").width();
      }
      loadURL("ajax-personnelresume.action?parentId="+$("#keyId").val(),$('div#s4'));
  });
  $("a#other4").off("click").on("click",function(e){
      if (table_global_width == 0) {
          table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s5").width();
      }
      loadURL("ajax-records.action?parentId="+$("#keyId").val(),$('div#s5'));
  });
  $("a#other5").off("click").on("click",function(e){
      if (table_global_width == 0) {
          table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s5").width();
      }
      loadURL("ajax-records.action?parentId="+$("#keyId").val(),$('div#s5'));
  });

    //返回视图
    $("#btn-re-common").click(function(){
        var index = "<s:property value="index" />";
        var todo = "<s:property value="todo" />";
        var remind = "<s:property value="remind" />";
        var record = "<s:property value="record" />";
        var draft = "<s:property value="draft" />";
        if(index==1){
            loadURL("ajax-index!page.action",$('#content'));
        }else if(todo==1){
            loadURL("ajax!toDoList.action",$('#content'));
        }else if(remind==1){
            loadURL("ajax!remindList.action",$('#content'));
        }else if(record==1){
            loadURL("ajax!taskRecordList.action?type=1",$('#content'));
        }else if(record==2){
            loadURL("ajax!taskRecordList.action?type=2",$('#content'));
        }else if(draft==1){
            loadURL("ajax!draftList.action",$('#content'));
        }else{
            loadURL("ajax-employees.action?viewtype=<s:property value="viewtype"/>",$('#content'));
        }

    });

  //编辑
  $("a[key=ajax_edit]").click(function(){

      $.ajax({
          type: "get",
          url:"ajax-employees!checkOneself.action?keyId="+$("input#keyId").val(),
          cache:false,
          dataType:"json",
          async:false,
          success: function(data){
              if(data.ifOneself=="1"){
                  loadURL("ajax-employees!input.action?keyId="+$("input#keyId").val(),$('#content'));
              }else {
                  alert("需要本人操作!")
              }

          }
      });
  });


</script>