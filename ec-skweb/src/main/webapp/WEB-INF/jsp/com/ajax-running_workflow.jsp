
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<input type="hidden" name="keyId" id="keyId" value="<s:property value="keyId" />">
    <ul  style="list-style:none;margin-top:10px">
        <s:if test="dataRows.size() > 0">
            <s:iterator value="dataRows" id="list" status="u">
               <li id="<s:property value="#list.id"/>" style="margin-left:-15px;margin-bottom:10px;font-size:14px;border-bottom: 1px dashed #ccc">
                    <span>
                        <span style="color:#03a9f4">
                            <i class="fa fa-user txt-color-green"></i>
                            <s:property value="#list.assignee"/>
                            (<s:property value="#list.curDuty.department.name"/>-<s:property value="#list.curDuty.post.name"/>)
                        </span>
                         <time>
                             <s:property value="#list.startTime"/>
                         </time>
                         <s:if test="#u.first">创建流程</s:if><s:else><s:property value="#list.type"/></s:else>
                    </span>
                   <s:if test="!#u.first">
                    <div class="subject">
                        <i class="fa fa-comments"></i>
                        审批意见: <s:property value="#list.commentMsg"/>
					</div>
                   </s:if>
                </li>
            </s:iterator>
        </s:if>
        <s:else>
            <h4> <i class="fa fa-warning"></i> 没有任何审批信息！</h4>
        </s:else>
    </ul>

    <ul>

    </ul>
<script>
$("span.msg-body a").unbind("click").bind("click",function(e){
    var keyId = $(this).attr("id");
    var taskId= $(this).attr("tId");
    var proId = $(this).attr("pId");
    var data = {keyId:keyId,taskId:taskId,proId:proId};
    var actionUrl = "../com/ajax-running!openDoc.action";
    gDialog.fCreate({
        title:'查看预览',
        url:actionUrl,
        param:data,
        width:1000
    }).show();
})

</script>
