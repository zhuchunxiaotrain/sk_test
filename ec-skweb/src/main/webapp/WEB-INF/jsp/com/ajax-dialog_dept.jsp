<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>



<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_dept_list_row">
      <table id="ajax_dept_list_table">
      </table>
      <div id="ajax_dept_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-serach" style="float:left;cursor:pointer">搜索</span>
  <span class="btn btn-primary" id="dialog-ok" style="cursor:pointer">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>
  function run_jqgrid_function(){
      var key="<s:property value="key"/>";
      var more="<s:property value="more"/>";
    jQuery("#ajax_dept_list_table").jqGrid({
      url:'ajax-depart!list.action',
      mtype:"post",
      datatype: 'json',
        colNames:['上级部门','部门名称','Id'],
        colModel : [
            {name:'parent',index:'parent_name', width:100,sortable:false,search:true,sorttype:'string'},
            {name:'name',index:'name', width:500,sortable:true,search:true,sorttype:'string'},
            {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
        ],
      page : 1,
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_dept_list_page',
      sortname:"parent",
      sortorder : "asc",
      gridComplete:function(){
          var ids=$("#ajax_dept_list_table").jqGrid('getDataIDs');
          var deptId = $("#"+key+"Id").val();
          for(var i=0;i<ids.length;i++) {
              var cl = ids[i];
              if (deptId != "" && deptId != null) {
                  var idsArr = deptId.split(",");
                  for (var s = 0; s < idsArr.length; s++) {
                      if (idsArr[s] == cl) {
                          $("#ajax_dept_list_table").jqGrid("setSelection", cl);
                      }
                  }
              }
          }

        jqGridStyle();
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        $('#ajax_dept_list_table').find('[type="checkbox"]').click(function(){
       //   $('#ajax_dept_list_table').find('[type="checkbox"]').removeAttr('checked');
        })
      },
      onSelectRow: function (rowId, status, e) {
          /* var rowId = $("#ajax_dept_list_table").jqGrid('getGridParam','selrow');
          var rowDatas = $("#ajax_dept_list_table").jqGrid('getRowData', rowId);
          $('#department').val(rowDatas['name']);
          $('#noticedictId').val(rowDatas['id']);*/
          if(more !="1"){
              $(this).gridselect('onecheck',{
                  table:"ajax_dept_list_table",
                  rowId:rowId,
                  status:status,
                  id:key+"Id",
                  name:key,
                  type:"radio"
              });
          }else{
              $(this).gridselect('onecheck',{
                  table:"ajax_dept_list_table",
                  rowId:rowId,
                  status:status,
                  id:key+"Id",
                  name:key,
                  type:"checkbox"
              });
          }

      },
        onSelectAll:function(aRowids, status, e){
            if(more =="1"){
                $(this).gridselect('morecheck',{
                    table:"ajax_dept_list_table",
                    rowId:aRowids,
                    status:status,
                    id:key+"Id",
                    name:key,
                    type:"checkbox"
                });
            }
        },
      jsonReader: {
          root: "dataRows",
          page: "page",
          total: "total",
          records: "records",
          repeatitems : false
      },
        multiselect: true,
        rownumbers:true,
        gridview:true,
        shrinkToFit:true,
        forceFit:true,
        viewrecords: true,
        autowidth: true,
        height : 'auto',
        grouping:true,
        groupingView : {
          groupField : ['parent'],
          groupColumnShow : [false]
        },
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_dept_list_table").jqGrid('setGridWidth', $("#ajax_dept_list_row").width()-10);
    })

    jQuery("#ajax_dept_list_table").jqGrid('navGrid', "#ajax_dept_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_dept_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();

  });
  $("#dialog-ok").unbind("click").bind("click",function(){
    if($("#show_varietyDict").length ==1){
      var vActionUrl = "<%=path%>/com/ajax-dict!getVarietyDict.action";
      var data={nursedictId: $("#nursedictId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        //console.log(pdata)
        if(pdata.rs){
          var html = "";
          for(var i=0; i<pdata.rs.length; i++){
            html += '<label class="checkbox"><input type="checkbox"  name="varietydictId" value="'+pdata.rs[i].id+'" > <i></i><span>'+pdata.rs[i].name+'</span></label>';
          }
          $("#show_varietyDict").html(html);
        }
      });
    }
    gDialog.fClose();
  });

  $('#dialog-serach').unbind("click").bind("click",function(){

    var e = jQuery.Event("keypress");//模拟一个键盘事件
    e.keyCode = 13;//keyCode=13是回车

    $("#gs_name").trigger(e);//模拟按下回车

  })



</script>
