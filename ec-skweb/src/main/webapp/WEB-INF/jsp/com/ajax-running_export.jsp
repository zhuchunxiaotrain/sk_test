<%@ page language="java" import="java.util.*" pageEncoding="utf-8"
	contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>	
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String keyId = request.getParameter("keyId");
String link = request.getParameter("link");	//对象是instance还是definition
%>
<html lang="en">
<head>

<link href="<%=path %>/resource/flow/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<%=path %>/resource/flow/js/jui/css/cupertino/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" media="screen"/>


<!-- 通用JS -->
<!-- jquery & ui -->
<script type="text/javascript" src="<%=path %>/resource/jquery-1.11.1.js"></script>

<!-- qtip -->
<script src="<%=path %>/resource/flow/js/qtip/jquery.qtip.min.js" type="text/javascript"></script>

<script src="<%=path %>/resource/flow/js/jui/js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>

<script src="<%=path %>/resource/flow/js/jquery.outerhtml.js" type="text/javascript"></script>

<script type="text/javascript">

function StringBuffer() {
    this.__strings__ = new Array;
}

StringBuffer.prototype.append = function (str) {
    this.__strings__.push(str);
};

StringBuffer.prototype.toString = function () {
    return this.__strings__.join("");
};

var pkey = "<s:property value="processDefinition.key"/>";
var pversion = "<s:property value="processDefinition.version"/>";
var prname = "<s:property value="processDefinition.diagramResourceName"/>";
var definitionUrl = "/"+pkey+"/"+pversion+"/"+prname;
var imageUrl = "<%=path %>/resource/bpm/images" + definitionUrl;


var infos;
function graphTrace(infos) {
	var options;
    var _defaults = {
        srcEle: this,
        pid: $(this).attr('pid')
    };
    var opts = $.extend(true, _defaults, options);
    // 获取图片资源
    var positionHtml = "";
    var min_x = 0;
    var min_y = 0;
    // 生成图片
    var varsArray = new Array();
    $.each(infos, function(i, v) {
        //获取最小的y
        if(v.y < min_y || min_y == 0){
            min_y = v.y;
        }
        //获取最小x
        if(!v.left){
            if(v.x < min_x || min_x == 0){
                min_x = v.x;
            }
        }else{
            min_x = v.left;
        }
        var $positionDiv = $('<div/>', {
            'class': 'activity-attr'
        }).css({
            position: 'absolute',
            left: (v.x - 1),
            top: (v.y - 1),
            width: (v.width - 2),
            height: (v.height - 2),
            backgroundColor: 'black',
            opacity: 0,
            zIndex: $.fn.qtip.zindex - 1
        });

            // 节点边框
        var $border = $('<div/>', {
            'class': 'activity-attr-border'
        }).css({
            position: 'absolute',
            left: (v.x +4),
            top: (v.y + 1),
            width: (v.width - 4),
            height: (v.height - 3),
            zIndex: $.fn.qtip.zindex - 2
        });
            if (v.currentActiviti) {
                $border.addClass('ui-corner-all-12').css({
                    border: '3px solid red'
                });
            }
            positionHtml += $positionDiv.outerHTML() + $border.outerHTML();
            varsArray[varsArray.length] = v.vars;
        });

        if ($('#workflowTraceDialog').length == 0) {
            $('<div/>', {
                id: 'workflowTraceDialog',
                title: '',
                html: "<div><img src='<s:property value="filePath"/>' style='position:absolute; left:"+min_x+"px; top:"+ min_y+"px;' />" +
                "<div id='processImageBorder'>" +
                positionHtml +
                "</div>" +
                "</div>"
            }).appendTo('body');
        } else {
            $('#workflowTraceDialog img').attr('src', "<s:property value="filePath"/>");
            $('#workflowTraceDialog #processImageBorder').html(positionHtml);
        }

        // 设置每个节点的data
        $('#workflowTraceDialog .activity-attr').each(function(i, v) {
            $(this).data('vars', varsArray[i]);
        });

        // 打开对话框
		$('#workflowTraceDialog').css('padding', '0.2em');
                $('#workflowTraceDialog').css('padding', '0.2em').height(400);

                // 此处用于显示每个节点的信息，如果不需要可以删除
                $('.activity-attr').qtip({
                    content:{
                    	text:function() {
	                        var vars = $(this).data('vars');
	                        var tipContent = "<div><table class='table-task' cellpadding='0' cellspacing='0' border='0'>";
	                        $.each(vars, function(varKey, varValue) {
	                            if (varValue) {
	                                tipContent += "<tr><th  width='30%'>" + varKey + "</th><td>" + varValue + "</td></tr>";
	                            }
	                        });
	                        tipContent += "</table></br></div>";
	                        return tipContent;
                    	},
						title:{
							text: '任务执行情况'				
						}
                    },
                    position: {
                        at: 'bottom left',
                        adjust: {
                            x: 3
                        },
   		   				viewport: $(window)
                    },
   			        show:{   			        	
	   			     	effect: function(offset) {
	   						$(this).slideDown(200);
	   					}
   			        },   			     	
   			        hide:{
   			        	event:'click mouseleave',
   			        	leave: false,
   			        	fixed:true,
   			        	delay:300
   			        	}, 
   			        style: {
   			       	  classes:'ui-tooltip-light ui-tooltip-shadow'
   			        } 	
                });
}

function closedialog(param) {
	alert(param.msg);
	return true;
}

</script>
<style>
  body {
    margin: 0;
  }
  #container {
    overflow: hidden;
    position: relative;
    height: 400px;
  }
 
  img {
    position: absolute;
  }
  </style>
  <script>
function animate( to ) {
      $( this ).stop( true, false ).animate( to );
    }
function next(event) {
    event.preventDefault();
    //下一步动作
    if(index<infos.length){
        $( "#user" ).goto(infos[index].x,infos[index].y,infos[index].width,infos[index].height,animate);
      	//闪烁两下
      	$( "#user" ).fadeOut(100);
      	$( "#user" ).fadeIn(100);
      	$( "#user" ).fadeOut(100);
      	$( "#user" ).fadeIn(100);
      	index++;
    }
}
$(function() {
	$.fn.goto = function( x,y,w,h,using ) {
		var my = "left+" + (x+w/2-20) +" top+" + (y);
	    return this.position({
	       	my: my,
	        at: "left top",
	        of: "body",
	        collision: "none",
	        using: using
	    });
	};
    var data = {keyId:<%=keyId%>}
	$.ajax({
        url : "../com/ajax-running!activitis.action",
        method:"post",
        cache : false,
        dataType : "json",
        async : false,
        data : data,
        success : function(data) {
            infos = data;
        	graphTrace(data);
        	$( "#user" ).click( next );
            goon();
        }
    });
});
  
function goon(){
	$( "#user" ).click();
	setTimeout(goon,2000)
}
var index = 0;
</script>
</head>
<body  style="background-color:#fff;width:100%;height:100%;overflow:auto">
<img id="user" src="<%=path %>/resource/flow/images/User.png" style="z-index:10000"/>

</body>

</html>