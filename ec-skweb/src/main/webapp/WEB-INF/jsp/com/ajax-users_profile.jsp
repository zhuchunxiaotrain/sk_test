<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!-- Bread crumb is created dynamically -->
<!-- row -->
<div class="row">

	<!-- col -->
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark"><!-- PAGE HEADER --><i class="fa-fw fa fa-user"></i> 用户信息</h1>
	</div>
	<!-- end col -->

	<!-- right side of the page with the sparkline graphs -->
	<!-- col -->
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
	</div>
	<!-- end col -->

</div>
<!-- end row -->

<!-- row -->

<div class="row">
	<div class="col-sm-12">
			<div class="well well-sm">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-6">
						<div class="well well-light well-sm no-margin no-padding">
							<div class="row">
								<div class="col-sm-12">
									<div class="row">
                                        <input type="hidden" name="keyId" id="keyId" value="<s:property value="user.id" />">
										<div  class="col-sm-3 profile-pic">
											<img id="users_image" style="top:0;margin-bottom:0" src="../com/file.action?keyId=<s:property value="user.id" />" />
										</div>
										<div class="col-sm-6">
											<h1><s:property value="user.name" />
											<br>
											<small><s:property value="admin.usermobile" /></small></h1>

											<ul class="list-unstyled">
												<li>
													<p class="text-muted">
														<i class="fa fa-envelope"></i> 手机号：<s:property value="user.mobile" />
													</p>
                                                    <p class="text-muted">
                                                    <input  name="uploadify" id="ajax_user_file" placeholder="" type="file" >
                                                    </p>
												</li>
											</ul>
											<br>
											
										</div>
									</div>

								</div>

							</div>
							

							<!-- end row -->


                        </div>
					</div>
					<div class="col-sm-12 col-md-12 col-lg-6">
						<div class="well well-light well-sm ">
							<div class="row">
								<div class="col-sm-12">
									<div class="row">

										<div class="col-sm-12">
											<h2>所属企业：<s:property value="user.company.name"/>
											<br>
											</h2>
											<ul class="list-unstyled">
												<li>
													<p class="text-muted">
														<i class="fa fa-envelope"></i> 简称：<s:property value="user.company.subname"/>
													</p>
												</li>
											</ul>
											<br>
										</div>

									</div>

								</div>

							</div>
						</div>
					</div>
				</div>

			</div>


	</div>

</div>
<!-- end row -->


<script type="text/javascript">

	pageSetUp();
	
	var pagefunction = function() {
		
	};
	pagefunction();

    $(function(){
        $("#ajax_user_file").uploadify({
            height        	: 30,
            multi : false,
            fileSizeLimit : '1MB',
            fileDataName : 'uploadify',
            progressData : 'percentage',
            'fileTypeDesc' : '格式:jpg',     //描述
            fileTypeExts : '*.jpg',
            buttonText : '选择图片上传',
            swf           : '../resource/com/js/plugin/uploadify/uploadify.swf',
            uploader      : 'ajax-users!uploadImg.action?keyId='+$("input#keyId").val(),
            width         : 140,
            onUploadStart:function(file){
                $("#import_message_alert").empty();
                $('<div class="alert alert-info fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-info"></i><strong>正在努力为您处理！</strong> 导入过程分校验和导入两部分，请耐心等待...</div>').appendTo($("#import_message_alert"));

            },
            onUploadSuccess : function(file, data, response) {
                $("#import_message_alert").empty();
                var json = jQuery.parseJSON(data);
                if(json && json.status == "200"){
                    $('<div class="alert alert-success fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-check"></i><strong>上传完毕</strong> 相片上传成功！</div>').appendTo($("#import_excel_message_alert"));
                    $(json.rows).each(function(index,map){
                        $('<div class="alert alert-info fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-info"></i><strong>提醒！</strong> '+map.id+'</div>').appendTo($("#import_excel_message_alert"));
                    });
                    $("#users_image").attr("src",json.fileUrl);
                    //loadURL("ajax-users!profile.action?keyId="+$("input#keyId").val(),$("#content"));
                }else{
                    $('<div class="alert alert-info fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-info"></i><strong>上传失败！</strong> </div>').appendTo($("#import_message_alert"));
                }
            }
        });

    });
</script>
