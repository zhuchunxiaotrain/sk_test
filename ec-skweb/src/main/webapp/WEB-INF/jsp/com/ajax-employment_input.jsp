<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-sm-12 col-md-12 col-lg-12">
        <div class="jarviswidget well" id="wid-id-3"
             data-widget-colorbutton="false"
             data-widget-editbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-custombutton="false"
             data-widget-sortable="false">
            <!-- widget div-->
            <div>
                <div class="widget-body">
                    <a class="btn btn-default" id="btn-employment-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>录用审核信息</a>
                    <s:if test="employment==null || employment.getProcessState().name()=='Draft'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
                    </s:if>
                    <s:if test="employment!=null && employment.getProcessState().name()=='Backed'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
                    </s:if>
                    <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
                    <hr class="simple">
                    <hr class="simple">
                    <form id="employment" class="smart-form" novalidate="novalidate" action="" method="post">
                        <input type="hidden" name="keyId" id="keyId" value="<s:property value="employment.id" />"/>
                        <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                        <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />

                        <header  style="display: block;">
                            录用审核信息&nbsp;&nbsp;<span id="title"></span>
                        </header>
                        <fieldset>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-users"> 姓名</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="name1" name="name" placeholder="请输入录用人姓名"
                                                   value="<s:property value="employment.name"/>"/>
                                            <input type="hidden" id="cardID1" name="cardID"
                                                   value="<s:property value="employment.cardID"/>"/>
                                        </label>
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    员工工号
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="staffid" id="staffid" disabled placeholder="自动生成员工编号" value="<s:property value="employment.staffid"/>" >
                                    </label>
                                </section>
                            </div><div class="row">
                                <label class="label col col-2">
                                    性别
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="sexValue" id="sexValue" placeholder="请输入性别" value="<s:property value="sexValue"/>" >
                                        <input type="hidden" name="sex" id="sex"  value="<s:property value="sex"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    用工性质
                                </label>
                                <section class="col col-5">
                                    <div class="inline-group" name="nature">
                                        <label class="radio">
                                            <input type="radio" checked="checked" name="nature" value="0" <s:property value="employment.nature==0?'checked':''"/>>
                                            <i></i>正式</label>
                                        <label class="radio">
                                            <input type="radio"  name="nature" value="1" <s:property value="employment.nature==1?'checked':''"/>>
                                            <i></i>试用</label>
                                        <label class="radio">
                                            <input type="radio"  name="nature" value="2" <s:property value="employment.nature==2?'checked':''"/>>
                                            <i></i>聘用</label>
                                    </div>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    手机号码
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="phone" id="phone" placeholder="请输入手机号码" value="<s:property value="employment.phone"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    录用岗位
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="postName" id="postName" placeholder="请输入录用岗位" value="<s:property value="post.name"/>" >
                                        <input type="hidden"  name="postId" id="postId" value="<s:property value="post.id"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    应聘部门
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="departmentName" id="departmentName" placeholder="请输入应聘部门" value="<s:property value="department.name"/>" >
                                        <input type="hidden"  name="departmentId" id="departmentId" value="<s:property value="department.id"/>" >

                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    面试记录
                                </label>
                                <label class="label col col-2">
                                    <jsp:include page="ajax-interview1.jsp"></jsp:include>

                                </label>
                            </div>


                            <div class="row">
                                <label class="label col col-2">
                                    备注
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="employment.remark"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    操作人
                                </label>
                                <section class="col col-4">
                                    <label class="input state-disabled">
                                        <input  disabled type="text"  placeholder="" value="<s:property value="creater.name"/>" >
                                    </label>
                                </section>
                                <label class="label col col-2">
                                    操作日期
                                </label>
                                <section class="col col-4 ">
                                    <label class="input state-disabled">
                                        <input  disabled type="text" name="createDate" id=createDate"  value="<s:property value="createDate" />">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </article>
</div>

<script>

    var draft = "<s:property value="draft" />";
    $(function(){
        var pdata= {
            keyId:$("#keyId").val(),
            flowName:"employment",
            todo:"1"
        };
        multiDuty(pdata);
    });


    $('#term').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN',
        minView:2
    });

    $("a[key=btn-choose-post]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择岗位",
            url:"ajax-dialog!mpost.action",
            width:340
        }).show();
    });


    $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择面试人",
            url:"ajax-dialog!emp.action?parentId="+$("input#parentId").val(),
            width:400
        }).show();
    });

    //校验
    $("#employment").validate({
        rules : {
            name:{
                required : true
            },
            phone:{
                required:true,
                isPhone:true
            },
        },
        messages : {
            name: {
                required : '请输入录用人',
            },
            phone:{
                required: "请输入您的联系电话",
                isPhone: "请输入一个有效的联系电话"
            },
        },
        ignore: "",
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });

    // 联系电话(手机/电话皆可)验证
    jQuery.validator.addMethod("isPhone", function(value,element) {
        var length = value.length;
        var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;
        var tel = /^\d{3,4}-?\d{7,9}$/;
        return this.optional(element) || (tel.test(value) || mobile.test(value));
    }, "请正确填写您的联系电话");

    //保存
    $("#btn-save-common").click(

        function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("employment","ajax-employment!save.action",null,function (data) {

                if(data.state == "200" || data.state ==200) {
                    if(draft == 1){
                        location.href='index.action';
                    }else{
                        loadURL("ajax-employment.action?parentId="+$("#parentId").val(),$('div#s3'));
                    }
                }
            });
        }

    );
    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#employment").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#employment").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("employment","ajax-employment!commit.action",null,function (data) {
                        if(data.state == "200" || data.state ==200) {
                            if(draft == 1){
                                location.href='index.action';
                            }else {
                                loadURL("ajax-employment.action?parentId=" + $("#parentId").val(), $('div#s3'));
                            }
                        }
                    });
                }
            });
        }
    );

    //返回视图
    $("#btn-employment-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-employment.action?parentId="+$("input#parentId").val(), $('div#s3'));
        }

    });

</script>