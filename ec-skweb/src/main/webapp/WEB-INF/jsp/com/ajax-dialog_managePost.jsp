<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<input type="hidden" id="departId" name="depId" value="<s:property value="depId"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_post_list_row">
      <table id="ajax_post_list_table">
      </table>
      <div id="ajax_post_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script tex>
  function run_jqgrid_function(){
    jQuery("#ajax_post_list_table").jqGrid({
      url:'ajax-post!postList.action?depId='+$("#departId").val(),
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['岗位名称','Id'],
      colModel : [
        {name:'name',index:'name', width:240,sortable:false,search:true,sorttype:'string'},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_post_list_page',
      sortname : 'id',
      sortorder : "desc",
      gridComplete:function(){
        jqGridStyle();
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      },
      onSelectRow: function (rowId, status, e) {
          var slt =   $("#ajax_post_list_table").jqGrid('getGridParam','selarrrow');
          if(slt.length > 1){
                alert("请选择一条数据");
                return false;
          }

      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_post_list_table").jqGrid('setGridWidth', $("#ajax_post_list_row").width()-10);
    })

    jQuery("#ajax_post_list_table").jqGrid('navGrid', "#ajax_post_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_post_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
      var rowId = $("#ajax_post_list_table").jqGrid('getGridParam','selrow');
      var rowDatas = $("#ajax_post_list_table").jqGrid('getRowData', rowId);
      $("#postName").val(rowDatas['name']);
      $("#postId").val(rowId);
      gDialog.fClose();
  });
</script>
