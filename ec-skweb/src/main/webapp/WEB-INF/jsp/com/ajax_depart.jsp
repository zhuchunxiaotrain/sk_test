<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="row" style="padding:2px">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<div>
			<form id="headForm" method="post" action="">
				<h5>
					<label style="width:100%" > <span class="label  label-success"> 提示：查看人员内可以设置部门负责人</span> </label>
				</h5>
				<a id="ajax_depart_btn_add" class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 添加新部门</a>
				<a id="ajax_search_organization_info" class="btn btn-default" href="javascript:void(0);"> 查看组织架构  <i class="fa fa-eye"></i></a>
				<%--<a id="ajax_export_dept_info" class="btn btn-default btn-primary pull-right" href="javascript:void(0);"> 部门导出  <i class="fa fa-lg fa-external-link"></i></a>--%>
				<%--<a id="ajax_import_btn_upload" class="btn btn-default btn-primary pull-right" href="javascript:void(0);"> 部门导入 <i class="fa fa-lg fa-cloud-upload"></i></a>--%>
			</form>
		</div>
	</div>
</div>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">
	<!-- row -->
	<div class="row">
		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
				data-widget-colorbutton="false" 
				data-widget-togglebutton="false" 
				data-widget-deletebutton="false" 
				data-widget-fullscreenbutton="true" 
				data-widget-custombutton="false" 
				data-widget-sortable="false">

				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<div style="margin-left: 30px;margin-top: -5px;position: absolute;">部门信息 </div>
				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class=" ">
                                <div class="row" id="ajax_depart_list_row">
						            <table id="ajax_depart_table" class="table table-striped table-bordered table-hover">
						            </table>
                                    <div id="ajax_depart_list_page">
						            </div>
                                </div>

						    </div>
                        </div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<div id="dialog-depart_message">

</div><!-- #dialog-message -->
<div id="dialog-message">

</div><!-- #dialog-message -->
<div id="dialog-depart_lead">

</div><!-- #dialog-message -->
<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();
	
	// PAGE RELATED SCRIPTS
	function loadDataTableScripts() {
		loadScript("../resource/com/js/plugin/datatables/jquery.dataTables-cust.min.js", dt_2);

		function dt_2() {
			loadScript("../resource/com/js/plugin/datatables/ColReorder.min.js", dt_3);
		}

		function dt_3() {
			loadScript("../resource/com/js/plugin/datatables/FixedColumns.min.js", dt_4);
		}

		function dt_4() {
			loadScript("../resource/com/js/plugin/datatables/ColVis.min.js", dt_5);
		}

		function dt_5() {
			loadScript("../resource/com/js/plugin/datatables/ZeroClipboard.js", dt_6);
		}

		function dt_6() {
			loadScript("../resource/com/js/plugin/datatables/media/js/TableTools.min.js", dt_7);
		}

		function dt_7() {
			loadScript("../resource/com/js/plugin/datatables/DT_bootstrap.js", runDataTables);
		}

	}
	function runDataTables() {
    /*
    * BASIC
    $('#ajax_members_table').dataTable({
    		"sPaginationType" : "bootstrap_full"
    });*/
    /* END BASIC */
    }
	
	function reload(){
		loadURL("../com/ajax!depart.action",$('#content'));
	}
	
	//添加新部门
	$('#ajax_depart_btn_add').click(function() {
		gDialog.fCreate({
		    title:'新建部门信息',
		    url:"../com/ajax-depart!read.action?keyId=",
		    width:500
		}).show();	});

	$("#ajax_export_dept_info").click(function(){
		var content = "";
		var title = "确定导出业务数据吗？";

		$.SmartMessageBox({
			title : title,
			content : content,
			buttons : '[取消][确定]'
		}, function(ButtonPressed) {
			if (ButtonPressed === "确定") {
				$("#headForm").attr("action","../com/ajax-import!exportFileDept.action").submit();
			}
		});
	});
	$("#ajax_import_btn_upload").click(function(){
		gDialog.fCreate({
			title:'请上传excel文件',
			url:"../com/ajax-import!dialogDept.action",
			width:500
		}).show();
	});



//删除部门
function fn_delete_depart(id){
	$.SmartMessageBox({
		title : "<i class='fa fa-minus-square txt-color-orangeDark'></i> <span class='txt-color-orangeDark'><strong>删除部门 【请确保该部门没有关联职权】</strong></span>",
		content : "确定删除该部门吗？",
		buttons : '[取消][确定]'
	}, function(ButtonPressed) {
		if (ButtonPressed === "确定") {
			var vActionUrl = "<%=path%>/com/ajax-depart!delete.action";
			data={keyId:id};
			ajax_action(vActionUrl,data,{},function(pdata){
				_show(pdata);
			});
			jQuery("#ajax_depart_table").trigger("reloadGrid");
		}
	});
}

</script>
<script type="text/javascript">
    $(function(){
        load_depart_jqGrid();
    });
	function btnUserInfo(cellvalue, options, cell){
		if(cellvalue!=""){
			var cl=cell["sprincipal"];
			var id=cell["id"];
			var clObj=cell["powerId"];
			if(cl!=""){
				var btn="<button class='btn bg-color-blueLight txt-color-white' data-original-title='查看成员'  onclick=\"fn_depart_actions_query('"+id+"');\"><i class='fa fa-eye'></i> 查看成员</button>"+" ";
				var btns=btn+btnDefaultPower(clObj,id);
			}
			else{
				var btn="<button class='btn btn-warning' data-original-title='查看成员'  onclick=\"fn_depart_actions_query('"+id+"');\"><i class='fa fa-eye'></i> 查看成员</button>"+" ";
				var btns=btn+btnDefaultPower(clObj,id);
			}
			return btns;
		}
		return false;
	}
	function btnDefaultPower(clObj,id){
		if(clObj!=""){
		    var btn="<button class='btn bg-color-blueLight txt-color-white' data-original-title='查看职权'  onclick=\"fn_post_actions_query('"+id+"');\"><i class='fa fa-eye'></i>查看职权</button>";
		}
		else{
			var btn="<button class='btn btn-warning' data-original-title='查看职权'  onclick=\"fn_post_actions_query('"+id+"');\"><i class='fa fa-eye'></i>查看职权</button>"
		}
		return btn;
	}
    function load_depart_jqGrid(){
        jQuery("#ajax_depart_table").jqGrid({
            	url:'../com/ajax-depart!list.action',
        	    datatype: "json",
            	colNames:["部门名称",'部门描述',"部门负责人","上级部门","创建日期","部门成员","操作","id"],
            	colModel:[
                    {name:'name',index:'name', width:60,sortable:true,search:true},
           		    {name:'description',index:'description', width:70},
           		    {name:'principal',index:'principal', width:120,sortable:false,search:false},
           		    {name:'upParent',index:'upParent', width:60,sortable:false,search:true},
           		    {name:'createDate',index:'createDate', width:110,sortable:true,search:true},
           		    {name:'employee',index:'employee', width:150,sortable:false,formatter:btnUserInfo,search:false},
           		    {name:'act',index:'act', width:150,sortable:false,fixed:true,search:false},
           		    {name:'id',index:'id', width:50,hidden:true}
             	],
                rowNum : 10,
                rowList:[10,20,30],
                pager : '#ajax_depart_list_page',
                sortname : 'id',
                sortorder : "asc",
                gridComplete:function(){
                    var ids=$("#ajax_depart_table").jqGrid('getDataIDs');
                    for(var i=0;i<ids.length;i++){
                        var cl=ids[i];
                        se="<button class='btn btn-default' data-original-title='编辑' onclick=\"fn_depart_actions_edit('"+cl+"');\"><i class='fa fa-edit'></i>编辑</button>"+" ";
                        de="<button class='btn btn-default' data-original-title='删除' onclick=\"fn_depart_actions_delete('"+cl+"');\"><i class='fa fa-times'></i>删除</button>"+" ";
                        jQuery("#ajax_depart_table").jqGrid('setRowData',ids[i],{act:se+de});
                    }
                    $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                    jqGridStyle();
                },
    	        jsonReader: {
    			    root: "dataRows",
    			    page: "page",
    			    total: "total",
    			    records: "records",
    			    repeatitems : false
    			},
    			multiselect : false,
                rownumbers:true,
                gridview:true,
                shrinkToFit:true,
                viewrecords: true,
                autowidth: true,
                height:'auto',
                forceFit:true,
                loadComplete: function() {
                }
            });
            $(window).on('resize.jqGrid', function() {
                jQuery("#ajax_depart_table").jqGrid('setGridWidth', $("#ajax_depart_list_row").width());
            });
			jQuery("#ajax_depart_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

            jQuery("#ajax_depart_table").jqGrid('navGrid', "#ajax_depart_list_page", {
                edit : false,
            	add : false,
            	del : false,
            	search:false
            });
	};

	function fn_depart_actions_query(id){
		var actionUrl = "../com/ajax-depart!leftView.action?keyId="+id;
		$(this).leftview('init',{
			title:"查看部门成员【勾选设置部门负责人】",
			actionUrl:actionUrl
		});
	};

	function fn_post_actions_query(id){
		var actionUrl = "../com/ajax-depart!postQuery.action?keyId="+id;
        $(this).leftview('init',{
			title:"岗位信息【勾选设置默认职权】",
			actionUrl:actionUrl
		});
	};

	function fn_depart_actions_edit(id){
		gDialog.fCreate({
		    title:'部门信息',
		    url:"../com/ajax-depart!read.action?keyId=" +id,
		    width:500
		}).show();
	};

	function fn_depart_actions_delete(id){
        fn_delete_depart(id);
       // jQuery("#ajax_depart_table").trigger("reloadGrid");
	}

	$("#ajax_depart_btn_export").click(function(){
		$.ajax({
			url : "",
            method:"post",
            cache : false,
            dataType : "json",
            async : false,
            success:function(){

            }
		})
	});

	$("#ajax_search_organization_info").click(function(){
		loadURL("../com/ajax!organization.action",$("#content"));
	});
</script>
