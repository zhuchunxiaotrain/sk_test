<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="ajax-top.jsp" />

<input  type="hidden" name="numStatus" id="numStatus"  value="<s:property value="numStatus" />"/>
<div class="row" >
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>离职管理
          </a>
          <!--
                <a href="javascript:void(0);" key="btn-comment">
                      <span style="margin-top: -13px !important;" class="btn btn-default pull-right">
                          <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                      </span>
                </a>
                -->
          
          <div id="myTabContent1" class="tab-content padding-10 ">
              <div class="row">
            <shiro:hasAnyRoles name="wechat">
                <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(dimission.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>
            </div>

          <hr class="simple">
          <form id="dimission" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="dimission.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId"/>"/>
            <header  style="display: block;">
              离职员工&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  用人性质
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="hidden"  name="nature" id="nature"  value="<s:property value="dimission.nature"/>" >
                    <input type="text" disabled name="natureName" id="natureName"  value="<s:property value="natureName"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled id="name"  name="name" type="text" value="<s:property value="dimission.name"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  身份证号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input id="cardID" disabled name="cardID" type="text" value="<s:property value="dimission.cardID"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  部门
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input disabled type="text" name="depName" id="depName"  value="<s:property value="dimission.department.name"/>" >
                    <input disabled type="hidden"  name="depId" id="depId" value="<s:property value="dimission.department.id"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  政治面貌
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="politicalStatusName" id="politicalStatusName"  value="<s:property value="politicalStatusName"/>" >
                    <input type="hidden"  name="politicalStatus" id="politicalStatus"  value="<s:property value="politicalStatus"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同开始日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="startDate" disabled name="startDate"
                           type="text" value="<s:date name="dimission.startDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>

                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同结束日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="endDate" name="endDate" disabled
                           type="text" value="<s:date name="dimission.endDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  离职性质
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="dimissionNature">
                    <label class="radio">
                      <input type="radio" disabled checked="checked" name="dimissionNature" value="0" <s:property value="dimission.dimissionNature==0?'checked':''"/>>
                      <i></i>辞职</label>
                    <label class="radio">
                      <input type="radio" disabled name="dimissionNature" value="1" <s:property value="dimission.dimissionNature==1?'checked':''"/>>
                      <i></i>辞退</label>
                    <label class="radio">
                      <input type="radio" disabled name="dimissionNature" value="2" <s:property value="dimission.dimissionNature==2?'checked':''"/>>
                      <i></i>退休</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  离职原因
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input id="reasons" disabled name="reasons" type="text"  placeholder="请输入离职原因" value="<s:property value="dimission.reasons"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">辞职报告</label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input name="uploadify" id="reportfileId" style="display: none" value="<s:property value="reportfileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">

                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  预计离职日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="leaveDate" name="leaveDate"
                           type="text" disabled value="<s:date name="dimission.leaveDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input  type="text" id="usersName" name="nextStepApproversName" placeholder="请选择会签人"
                              value="<s:property value="dimission.nextStepApproversName"/>"/>
                      <input type="hidden" id="usersId" name="nextStepApproversId"
                             value="<s:property value="dimission.nextStepApproversId"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input id="remark" name="remark" type="text" disabled placeholder="请输入备注" value="<s:property value="dimission.remark"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text"  id="applyUserName" placeholder="" value="<s:property value="dimission.creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input  disabled type="text" name="applyDate" id=applyDate"  value="<s:date name="dimission.createDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
          <div class="flow">
                <s:if test="dimission.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer"style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
              </div>

          </div>
      </div>
    </div>
 </div>
  </article>
</div>

<script type="text/javascript">

 /* $('li.disabled').off("click").on("click",function(){
    return false;
  })*/

  /*$(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"dimission"
    };
    multiDuty(pdata);
  });*/

   //上传文件
   readLoad({
       objId:"reportfileId",
       entityName:"reportId",
       sourceId:"reportfileId"
   });

  $('#startDate,#endDate,#dimissionalEnd,#dimissionalStart').datetimepicker({
      format: 'yyyy-mm-dd',
      weekStart: 1,
      autoclose: true,
      todayBtn: 'linked',
      language: 'zh-CN',
      minView:2
  });

 //返回视图
 $("#btn-re-common").click(function(){
     var index = "<s:property value="index" />";
     var todo = "<s:property value="todo" />";
     var remind = "<s:property value="remind" />";
     var record = "<s:property value="record" />";
     var draft = "<s:property value="draft" />";
     if(index==1){
         loadURL("ajax-index!page.action",$('#content'));
     }else if(todo==1){
         loadURL("ajax!toDoList.action",$('#content'));
     }else if(remind==1){
         loadURL("ajax!remindList.action",$('#content'));
     }else if(record==1){
         loadURL("ajax!taskRecordList.action?type=1",$('#content'));
     }else if(record==2){
         loadURL("ajax!taskRecordList.action?type=2",$('#content'));
     }else if(draft==1){
         loadURL("ajax!draftList.action",$('#content'));
     }else{
         loadURL("ajax-dimission.action",$('#content'));
     }

 });

 $(function(){
     loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
     loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
     ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
         var showDuty = false;
         var area = $("span.textarea-controls");
         $(pdata.data.datarows).each(function(i,v){
             var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
             $(area).append(str);
             if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                 showDuty = true;
             }
         });
         if(showDuty == true){
             var pdata = {
                 keyId: $("input#keyId").val(),
                 flowName: "dimission"
             };
             multiDuty(pdata);
         }
     });
     var valueObj=$("input#numStatus").val();
     switch(valueObj){
         case "0":
             //保存后再提交
             $("#left_foot_btn_approve").off("click").on("click",function(){
                 form_save("dimission","ajax-dimission!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                     $("#btn-re-common").trigger("click");
                 })
             });
             break;
         case "1":
             //第一步审批
             $("#left_foot_btn_approve").off("click").on("click",function(){
                 form_save("dimission","ajax-dimission!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                     $("#btn-re-common").trigger("click");
                 });
             });
             break;
         case "2":
             //第二步审批
             $("#left_foot_btn_approve").off("click").on("click",function(){
                 form_save("dimission","ajax-dimission!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                     $("#btn-re-common").trigger("click");
                 })
             });
             break;
         case "3":
             //第三步审批
             $("#left_foot_btn_approve").off("click").on("click",function(){
                 form_save("dimission","ajax-dimission!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                     $("#btn-re-common").trigger("click");
                 })
             });
             break;
     }
     //退回
     $("#left_foot_btn_sendback").off("click").on("click",function(){
         var vActionUrl="ajax-dimission!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
         var data={keyId:$("input#keyId").val()};
         ajax_action(vActionUrl,data,{},function(pdata){
             _show(pdata);
             $("#btn-re-common").trigger("click");
         });
     });
     //否决
     $("#left_foot_btn_deny").off("click").on("click",function(){
         var vActionUrl="ajax-dimission!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
         var data={keyId:$("input#keyId").val()};
         ajax_action(vActionUrl,data,{},function(pdata){
             _show(pdata);
             $("#btn-re-common").trigger("click");
         });
     });
     //流程信息展开
     $('#flow,#next').click(function(){
         if($(this).hasClass("right")){
             $(this).removeClass("right").addClass("down");
             $(this).parent(".f_title").next("div.f_content").show();
         }else{
             $(this).removeClass("down").addClass("right");
             $(this).parent(".f_title").next("div.f_content").hide();
         }
     });
 });

 //编辑
 $("a[key=ajax_edit]").click(function(){
     var draft = "<s:property value="draft" />";
     loadURL("ajax-dimission!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
 });





</script>