
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<div class="row" style="margin:0px 0 20px 0">


    <div class="col-lg-2 col-sm-2 col-xs-6">
        <div id="ajax_index-list" class="main-box infographic-box colored yellow-bg" onclick="location.href='index.action'">
            <span class="headline" style="text-align: center" >主页</span>
        </div>
    </div>

    <div class="col-lg-2 col-sm-2 col-xs-6">
        <div id="ajax_manage-list" class="main-box infographic-box colored emerald-bg"  onclick="location.href='index.action?_t=manage'">
            <span class="headline" style="text-align: center">行政管理模块</span>
        </div>
    </div>
    <div class="col-lg-2 col-sm-2 col-xs-6">
        <div id="ajax_com-list" class="main-box infographic-box colored green-bg" onclick="location.href='index.action?_t=web'">
            <span class="headline" style="text-align: center" >人事管理模块</span>
        </div>
    </div>
    <div class="col-lg-2 col-sm-2 col-xs-6">
        <div id="ajax_finance-list" class="main-box infographic-box colored gray-bg" onclick="location.href='index.action?_t=finance'">
            <span class="headline" style="text-align: center" >财务管理模块</span>
        </div>
    </div>
    <div class="col-lg-2 col-sm-2 col-xs-6">
        <div id="ajax_party-list" class="main-box infographic-box colored red-bg" onclick="location.href='index.action?_t=party'">
            <span class="headline" style="text-align: center" >党政管理模块</span>
        </div>
    </div>
    <div class="col-lg-2 col-sm-2 col-xs-6">
        <div id="ajax_study-list" class="main-box infographic-box colored purple-bg">
            <span class="headline" style="text-align: center">学习园地</span>
        </div>
    </div>

</div>