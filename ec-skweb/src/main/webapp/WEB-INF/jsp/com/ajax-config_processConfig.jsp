<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- row -->
<div class="modal-body" style="height: 500px ">
    <div class="row">
        <input type="hidden" id="dealType" value="<s:property value="dealType"/>">
        <input type="hidden" id="doType" value="<s:property value="doType"/>">
        <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
            <div class="jarviswidget well" id="wid-id-3"
                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-sortable="false">
                 <div>
                    <div class="widget-body">
                        <form id="form_process" class="form-horizontal">
                            <input type="hidden" name="processDefinitionId" id="processDefinitionId" value="<s:property value="processDefinition.id" />">
                            <input type="hidden" name="activityId" id="activityId" value="<s:property value="activityId"/>">
                            <fieldset>

                                <legend>节点信息</legend>
                                <div class="form-group ">
                                    <label class="col-md-2 control-label">节点类型</label>
                                    <div class="col-md-8">
                                        <input class="form-control" disabled="" type="text"  value="<s:property value="nodeType"/>">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-2 control-label">节点名称</label>
                                    <div class="col-md-8">
                                        <input class="form-control" disabled="" type="text" value="<s:property value="nodeName"/>">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-2 control-label">节点描述</label>
                                    <div class="col-md-8">
                                        <input class="form-control" disabled="" type="text"  value="<s:property value="properties.documentation"/>">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-2 control-label">任务变量Id</label>
                                    <div class="col-md-8">
                                        <input class="form-control" disabled="" type="text"  value="<s:property value="activityId"/>">
                                    </div>
                                </div>
                            </fieldset>
                            <s:if test="multiBehavior != null ">
                                <fieldset>
                                    <legend>多实例设置</legend>
                                    <div class="form-group ">
                                        <label class="col-md-2 control-label">执行群组变量</label>
                                        <div class="col-md-8">
                                            <input class="form-control" disabled="" type="text"  value="<s:property value="multiBehavior.getCollectionExpression().getExpressionText()"/>">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label class="col-md-2 control-label">结束条件</label>
                                        <div class="col-md-8">
                                            <input class="form-control" disabled="" type="text"  value="<s:property value="multiBehavior.getCompletionConditionExpression().getExpressionText()"/>">
                                        </div>
                                    </div>
                                </fieldset>
                            </s:if>
                            <%--<s:if test="properties.type != 'exclusiveGateway' && assExpre ==null">--%>
                                <fieldset>
                                    <legend>用户任务信息</legend>
                                    <%--<div class="form-group ">--%>
                                        <%--<label class="col-md-2 control-label">执行者</label>--%>
                                        <%--<div class="col-md-8">--%>
                                            <%--<input class="form-control" disabled="" type="text" value="<s:property value="taskDefinition.getAssigneeExpression().getExpressionText()"/>">--%>
                                        <%--</div>--%>
                                    <%--</div>--%>
                                    <div class="form-group ">
                                        <label class="col-md-2 control-label">可执行操作</label>
                                        <div class="col-md-8">
                                            <div class="col-md-8">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" id="deny" name="actionType" value="001"  class="checkbox style-0">
                                                    <span>否决</span>
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" id="back" name="actionType" value="010" class="checkbox style-0">
                                                    <span>退回</span>
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" id="pass" name="actionType" value="100" class="checkbox style-0">
                                                    <span>通过</span>
                                                </label>
                                                <%--<label class="checkbox-inline">--%>
                                                    <%--<input type="checkbox" id="require" name="actionType" value="1000" class="checkbox style-0">--%>
                                                    <%--<span>请求</span>--%>
                                                <%--</label>--%>
                                            </div>
                                        </div>
                                    </div>

                                    <%--<div class="form-group ">--%>
                                        <%--<label class="col-md-2 control-label">持续时间</label>--%>
                                        <%--<div class="col-md-8">--%>
                                            <%--<input class="form-control" disabled="" type="text"  value="<s:property value="taskDefinition.getDueDateExpression().getExpressionText()"/>">--%>
                                        <%--</div>--%>
                                    <%--</div>--%>
                                </fieldset>
                                <%--<s:if test="!taskDefinition.getAssigneeExpression().getExpressionText().contains('applyUserId')">--%>
                                    <fieldset>
                                        <legend>节点配置</legend>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">群组： </label>
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <input class="form-control" type="text" id="roleName" value="<s:iterator id="list" value="processConfig.commonConfig.roleSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>"/>
                                                    <input type="hidden" type="text" name="roleId" id="roleId" value="<s:iterator id="list" value="processConfig.commonConfig.roleSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                                                    <div class="input-group-btn">
                                                        <button class="btn" id="btn-choose-role"  type="button" style="font-size: 12px;">选择群组</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">部门： </label>
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <input class="form-control" type="text"  id="departName" value="<s:iterator id="list" value="processConfig.commonConfig.departmentSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>"/>
                                                    <input type="hidden" type="text" name="departId" id="departId"
                                                    value="<s:iterator id="list" value="processConfig.commonConfig.departmentSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                                                    <div class="input-group-btn">
                                                        <button class="btn" id="btn-choose-depart" type="button" style="font-size: 12px;">选择部门</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">岗位： </label>
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <input class="form-control" type="text" id="postName"
                                                           value="<s:iterator id="list" value="processConfig.commonConfig.postSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>"/>
                                                    <input type="hidden" type="text" name="postId" id="postId"
                                                            value="<s:iterator id="list" value="processConfig.commonConfig.postSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                                                    <div class="input-group-btn">
                                                        <button class="btn" id="btn-choose-post" type="button" style="font-size: 12px;">选择岗位</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">职权： </label>
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <input class="form-control" type="text"  id="powerName"
                                                           value="<s:iterator id="list" value="processConfig.commonConfig.powerSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>"/>
                                                    <input type="hidden" type="text" name="powerId" id="powerId"
                                                           value="<s:iterator id="list" value="processConfig.commonConfig.powerSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                                                    <div class="input-group-btn">
                                                        <button class="btn" id="btn-choose-power" type="button" style="font-size: 12px;">选择职权</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <legend>域名配置</legend>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">域名：</label>
                                            <div class="col-md-8">
                                                <div class="input">
                                                    <input class="form-control" type="text" name="fields" id="fields" value="<s:property value="processConfig.variable"/>"
                                                    placeholder="请输入一个自定义审批人的域名"/>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <%--<fieldset>--%>
                                        <%--<legend>域名配置</legend>--%>
                                        <%--<div class="form-group">--%>
                                            <%--<label class="col-md-2 control-label">域名： </label>--%>
                                            <%--<div class="col-md-8">--%>
                                                <%--<div class="input-group">--%>
                                                    <%--<input class="form-control" type="text"  id="fieldName" name="fieldName" value="<s:property value="fieldName"/>"/>--%>
                                                    <%--<input type="hidden" name="fields" id="fields" value="<s:property value="processConfig.variable"/>"/>--%>
                                                    <%--<div class="input-group-btn">--%>
                                                        <%--<button class="btn" id="btn-choose-field" type="button" style="font-size: 12px;">选择域名</button>--%>
                                                    <%--</div>--%>
                                                <%--</div>--%>
                                            <%--</div>--%>
                                        <%--</div>--%>
                                    <%--</fieldset>--%>
                                    <fieldset>
                                        <legend>特殊设定</legend>
                                        <div class="form-group ">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-8">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="power" name="type" value="001"  class="checkbox style-0">
                                                            <span>上一步操作用户职权上级</span>
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="header" name="type" value="010" class="checkbox style-0">
                                                            <span>上一步操作用户所属部门负责人</span>
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="upheader" name="type" value="100" class="checkbox style-0">
                                                            <span>上一步操作用户所属部门的上级部门负责人</span>
                                                        </label>
                                                    </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                <%--</s:if>--%>
                            <%--</s:if>--%>
                        </form>
                    </div>
                 </div>
            </div>
        </article>
    </div>
</div>
<div class="modal-footer">
	<a href="javascript:void(0);" class="btn btn-primary" id="a-dialog-ok">确定</a>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script type="text/javascript">
pageSetUp();

function initCheckBox(key,selectObj){
    if($("input#"+key).val()!=null){
        var checkType=$("input#"+key).val();
    }
    var handleType=$("input[name="+selectObj+"]");
    $(handleType).each(function(i,v){
        var selectVal=$(v).val();
        var idx=selectVal.indexOf("1");
        if(checkType!=null && checkType.charAt(idx)=="1"){
            $(v).prop("checked",true);
        }
        else{
            $(v).prop("checked",false);
        }
    });
}
$(function(){
    initCheckBox("dealType","type");
    initCheckBox("doType","actionType");
});

$("#btn-choose-role").unbind("click").bind("click",function(){
      gDialog.fCreate({
           title:"请选择群组",
           url:"../com/ajax-dialog!role.action",
           width:340
        }).show();
});
$("#btn-choose-depart").unbind("click").bind("click",function(){
    gDialog.fCreate({
               title:"请选择部门",
               url:"../com/ajax-dialog!mdepart.action",
               width:500
            }).show();
});
$("#btn-choose-post").unbind("click").bind("click",function(){
     gDialog.fCreate({
           title:"请选择岗位",
           url:"../com/ajax-dialog!mpost.action",
           width:340
        }).show();
});
$("#btn-choose-power").unbind("click").bind("click",function(){
    gDialog.fCreate({
           title:"请选择职权",
           url:"../com/ajax-dialog!power.action",
           width:340
        }).show();
});
$("#btn-choose-field").unbind("click").bind("click",function(){
    gDialog.fCreate({
        title:"请选择域",
        url:"../com/ajax-dialog!field.action?keyId="+"<s:property value="businessConfig.id"/>",
        width:340
    }).show();
});
$("#a-dialog-ok").unbind("click").bind("click",function(){
        var actionUrl = "../com/ajax-config!processSave.action";
        form_save("form_process",actionUrl);
        gDialog.fClose();
    });
</script>