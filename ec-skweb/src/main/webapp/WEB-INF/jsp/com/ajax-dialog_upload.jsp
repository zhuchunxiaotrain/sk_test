<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String jsessionid = session.getId();
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<input type="hidden" id="keyId" value="<s:property value="keyId"/>"/>
<input type="hidden" id="result" value="<s:property value="result"/>"/>
<input type="hidden" id="parentId" value="<s:property value="parentId"/>"/>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">

                <h5><label style="width:100%"> 提示：请上传附件
                </label>
                </h5>
            </div>
        </div>
        <div class="col-md-12">
            <form class="form-horizontal" action="" method="post">
                <fieldset>
                    <div class="row" id="upload-div">
                        <label class="label col col-2">
                           <%-- <i class="fa fa-asterisk txt-color-red"></i>--%>
                            附件上传
                        </label>
                        <section class="col col-5">
                          <%--  <label class="input">
                                <input type="text" name="attchmentText" id="attchmentText" placeholder="请输入附件上传" value="<s:property value="selectionSpeech.attchmentText"/>" >
                            </label>--%>
                            <label class="input">
                                <input name="uploadify" id="speechAttchmentFileName" placeholder="" type="file">
                                <input name="speechAttchmentFileId" id="speechAttchmentFileId" style="display:none" value="<s:property value="speechAttchmentFileId"/>" >
                            </label>
                        </section>
                    </div>


                   <%-- <div class="form-group">
                        <label class="col-md-3 control-label" >请上传附件</label>
                        <div class="col-md-9">
                            <input class="form-control" name="uploadify" id="file_upload_1" placeholder=""
                                   type="file" >
                        </div>
                    </div>--%>
                </fieldset>
            </form>
            <div id="import_message_alert">

            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <span class="btn btn-primary" id="dialog-ok">确定</span>
    <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>

    inputLoad({
        objId:"speechAttchmentFileName",
        entityName:"speechAttchmentId",
        sourceId:"speechAttchmentFileId",
        jsessionid:"<%=jsessionid%>"
    });
    function run_jqgrid_function(){
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_upload_list_table").jqGrid('setGridWidth', $("#ajax_upload_list_row").width()-10);
        })

        jQuery("#ajax_upload_list_table").jqGrid('navGrid', "#ajax_upload_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });

        jQuery("#ajax_upload_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
    }

    $(function(){
        run_jqgrid_function();
    });
    $("#dialog-ok").unbind("click").bind("click",function(){

        var rowId="<s:property value="keyId"/>";
        var result="<s:property value="result"/>";
        var speechAttchmentId=$("#speechAttchmentId").val();
        var attchmentText=$("div#upload-div input#attchmentText").val();
        if(speechAttchmentId==""){
            alert("请上传附件");
        }else{
            var actionUrl="<%=path %>/party/ajax-selectionspeech!update.action";
            var data={result :result,keyId :rowId,speechAttchmentId :speechAttchmentId,parentId:$("#parentId").val()};
            ajax_action(actionUrl,data,null,function (pdata) {
                _show(pdata);
            });
            loadURL("../party/ajax-selectionspeech.action?parentId="+$("#parentId").val(),$('div#s3'));
            gDialog.fClose();
        }


       /* var actionUrl="<%=path %>/party/ajax-selectionspeech!update.action";
        var data={result :"2",keyId :rowId,speechAttchmentId :speechAttchmentId,attchmentText :attchmentText};
        ajax_action(actionUrl,data,null,function (pdata) {
            _show(pdata);
        });
        loadURL("../party/ajax-selectionspeech.action?keyId="+rowId,$('div#s3'));*/
    });
</script>
