<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
<div class="row" >
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>项目信息
          </a>
          <!--
                <a href="javascript:void(0);" key="btn-comment">
                      <span style="margin-top: -13px !important;" class="btn btn-default pull-right">
                          <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                      </span>
                </a>
                -->
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 招标信息 </a>
            </li>
            <li class="disabled">
              <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 交易金支付 </a>
            </li>
            <li>
              <a href="#s3" id="other2" data-toggle="tab"><i class="fa fa-fw fa-lg fa-calendar"></i> 中标通知书登记 </a>
            </li>
          </ul>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
                <div class="row">
                  <a href="javascript:void(0);" key="btn-comment">
                    <span class="btn btn-default pull-right">
                      <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                    </span>
                  </a>
            <shiro:hasAnyRoles name="wechat">
                <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(manageProInfo.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 编辑</a>
            </shiro:hasAnyRoles>
              </div>


          <hr class="simple">
          <form id="manageProInfo" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="manageProInfo.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              项目基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  项目名称
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="name" id="name" disabled value="<s:property value="manageProInfo.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  项目编号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="projectNo" id="projectNo" value="<s:property value="manageProInfo.projectNo"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  建设单位
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="clientName"
                           value="<s:property value="manageProInfo.client.name"/>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  建设单位地址
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="buildAddress" id="buildAddress" value="<s:property value="manageProInfo.buildAddress"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                   联系人
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="buildAddress" id="linkmanName" value="<s:property value="manageProInfo.linkman.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  项目概况
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="projectInfo" id="projectInfo" disabled value="<s:property value="manageProInfo.projectInfo"/>" >
                  </label>
                </section>
              </div>
                <div class="row">
                    <label class="label col col-2">
                        <i class="fa fa-asterisk txt-color-red"></i>
                        计划投资额（万元）
                    </label>
                    <section class="col col-5">
                        <label class="input">
                            <input type="text" name="numInventMoney" id="numInventMoney" disabled value="<s:property value="manageProInfo.numInventMoney"/>" >
                        </label>
                    </section>
                </div>
              <div class="row">
                <label class="label col col-2">
                  资金来源
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="moneyFrom" id="moneyFrom" disabled value="<s:property value="manageProInfo.moneyFrom"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  招标信息来源
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="bidFrom" id="bidFrom" disabled value="<s:property value="manageProInfo.bidFrom"/>" >
                  </label>
                </section>
              </div>
                <div class="row">
                    <label class="label col col-2">
                        <i class="fa fa-asterisk txt-color-red"></i>
                         会签人
                    </label>
                    <section class="col col-5">
                        <label class="input">
                            <label class="input state-disabled">
                                <input disabled type="text" id="usersName" name="nextStepApproversName" disabled
                                       value="<s:property value="manageProInfo.nextStepApproversName"/>"/>
                            </label>
                        </label>
                    </section>
                </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="remark" id="remark" disabled value="<s:property value="manageProInfo.remark"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  新建人
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" value="<s:property value="manageProInfo.creater.name"/>" />
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
        <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
        <div class="tab-pane fade in active" id="s3" style="margin: 10px;"></div>
      </div>
    </div>
 </div>
      </div>
    </article>
</div>

<script type="text/javascript">
  $('li.disabled').off("click").on("click",function(){
    return false;
  })
  /*
  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"manageproinfo"
    };
    multiDuty(pdata);
  });*/

  var table_global_width=0;
  $("a#other1").off("click").on("click",function(e) {
    if (table_global_width == 0) {
      table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
    }
    loadURL("ajax-manageproinfo!view.action?parentId="+$("#keyId").val(),$('div#s2'));
  });

  $("a#other2").off("click").on("click",function(e){
    if (table_global_width == 0) {
      table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s3").width();
    }
    loadURL("ajax-manageproinfo!view2.action?parentId="+$("#keyId").val(),$('div#s3'));
  });
  //返回视图
  $("#btn-re-common").click(function(){
    loadURL("ajax-manageproinfo.action?viewtype=<s:property value="viewtype"/>",$('#content'));
  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    loadURL("ajax-manageproinfo!input.action?keyId="+$("input#keyId").val(),$('#content'));
  });

  //左侧意见栏
  $("a[key=btn-comment]").unbind("click").bind("click",function(){
    var keyId = $("input#keyId").val();
    var actionUrl = "ajax-running!comment.action?bussinessId="+keyId;
    $(this).leftview('init',{
      title:"查看审批意见",
      actionUrl:actionUrl
    });
    var keyId = $("#keyId").val();
    $(this).leftview('foot',{'unid':keyId,'flowName':"manageproinfo",callback:function(){
      var valueObj=$("input#numStatus").val();
      switch(valueObj){
          case "0":
              //保存后再提交
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("manageProInfo","ajax-manageproinfo!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $(this).leftview('leftClose');
                      $("#btn-re-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
        case "1":
          //第一步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("manageProInfo","ajax-manageproinfo!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-re-common").trigger("click");
            });

          });
          break;
        case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("manageProInfo","ajax-manageproinfo!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
            $(this).leftview('leftClose');
            $("#btn-re-common").trigger("click");
          })
        });
        break;
        case "3":
          //第三步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("manageProInfo","ajax-manageproinfo!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-re-common").trigger("click");
            })
          });
        // approveRequest("approve1");
        break;
      }
      //退回
      $("#left_foot_btn_sendback").off("click").on("click",function(){
        var vActionUrl="ajax-manageproinfo!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("input#keyId").val()};

        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-re-common").trigger("click");
        });
      });
      //否决
      $("#left_foot_btn_deny").off("click").on("click",function(){
        var vActionUrl="ajax-manageproinfo!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("input#keyId").val()};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-re-common").trigger("click");
        });
      });
    }});
  })

</script>