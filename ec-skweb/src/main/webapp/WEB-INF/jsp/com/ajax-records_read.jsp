<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-records-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>奖惩记录
          </a>
          <div class="row">
            <%--<a href="javascript:void(0);" key="btn-comment">
                    <span class="btn btn-default pull-right">
                      <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                    </span>
            </a>--%>
            <shiro:hasAnyRoles name="wechat">
              <a style="margin-top: -13px !important;" <s:property value="isEdit(records.id)"/> class="btn btn-default pull-right pull-right-fix" key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>
          </div>
          <hr class="simple">
          <form id="records" class="smart-form" novalidate="novalidate" action="" method="post">
            <input  name="keyId" id="recordsKeyId" value="<s:property value="keyId" />"/>
            <input type="hidden" name="parentId" id="parentId"  value="<s:property value="parentId" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              奖惩记录&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  奖惩类型
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="types">
                    <label class="radio">
                      <input type="radio" disabled checked="checked" name="types" value="0" <s:property value="records.types==0?'checked':''"/>>
                      <i></i>奖励</label>
                    <label class="radio">
                      <input type="radio" disabled name="types" value="1" <s:property value="records.types==1?'checked':''"/>>
                      <i></i>惩罚</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  奖惩名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="name" id="name" disabled value="<s:property value="records.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  奖惩时间
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  disabled id="rpDate" name="rpDate"
                            type="text" value="<s:date name="records.rpDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">相关附件</label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  name="uploadify" id="accessoriesFileId" placeholder="" style="display: none" value="<s:property value="accessoriesFileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  提交人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text"  id="creater" placeholder="" value="<s:property value="records.creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  提交日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input disabled type="text" placeholder="操作时间" value="<s:date name="records.createDate" format="yyyy-MM-dd"/>">

                  </label>
                </section>
              </div>

            </fieldset>

          </form>
        </div>
  </div>
      </div>
  </article>
</div>

<script>
    //上传文件
    readLoad({
        objId:"accessoriesFileId",
        entityName:"accessoriesId",
        sourceId:"accessoriesFileId"
    });

    //返回视图
    $("#btn-records-common").click(function(){
        var index = "<s:property value="index" />";
        var todo = "<s:property value="todo" />";
        var remind = "<s:property value="remind" />";
        var record = "<s:property value="record" />";
        var draft = "<s:property value="draft" />";
        if(index==1){
            loadURL("ajax-index!page.action",$('#content'));
        }else if(todo==1){
            loadURL("ajax!toDoList.action",$('#content'));
        }else if(remind==1){
            loadURL("ajax!remindList.action",$('#content'));
        }else if(record==1){
            loadURL("ajax!taskRecordList.action?type=1",$('#content'));
        }else if(record==2){
            loadURL("ajax!taskRecordList.action?type=2",$('#content'));
        }else if(draft==1){
            loadURL("ajax!draftList.action",$('#content'));
        }else{
            loadURL("ajax-records.action?parentId="+$("#parentId").val(),$('div#s5'));
        }

    });

    //编辑
    $("a[key=ajax_edit]").click(function(){
        var draft = "<s:property value="draft" />";
        loadURL("ajax-records!input.action?keyId="+$("input#recordsKeyId").val()+"&draft="+draft+"&parentId="+$("#parentId").val(),$('#content'));
    });

</script>
