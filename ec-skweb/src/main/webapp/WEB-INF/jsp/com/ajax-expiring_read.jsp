<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<input type="text" name="numStatus" id="numStatus"  value="<s:property value="numStatus" />"/>
<div class="row" >
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>续约申请
          </a>
          <!--
                <a href="javascript:void(0);" key="btn-comment">
                      <span style="margin-top: -13px !important;" class="btn btn-default pull-right">
                          <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                      </span>
                </a>
                -->
          
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
                <div class="row">
                  <a href="javascript:void(0);" key="btn-comment">
                    <span class="btn btn-default pull-right">
                      <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                    </span>
                  </a>
            <shiro:hasAnyRoles name="wechat">
                <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(employees.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 编辑</a>
            </shiro:hasAnyRoles>
              </div>


          <hr class="simple">
          <form id="employees" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="employees.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              续约申请&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  用人性质
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="hidden"  name="nature" id="nature"  value="<s:property value="employees.nature"/>" >
                    <input type="text"  name="natureName" id="natureName"  value="<s:property value="natureName"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input disabled id="name" name="name" type="text" value="<s:property value="employees.name"/>">

                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  部门
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input type="text" name="depName" id="depName"  value="<s:property value="employees.dep.name"/>" >
                    <input type="hidden"  name="depId" id="depId" value="<s:property value="employees.dep.id"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  岗位
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input type="text" name="postName" id="postName"  value="<s:property value="employees.post.name"/>" >
                    <input type="hidden"  name="postId" id="postId" value="<s:property value="employees.post.id"/>" >

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同开始日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input id="startDate" name="startDate"
                           type="text" value="<s:date name="employees.startDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>

                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同结束日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input id="endDate" name="endDate"
                           type="text" value="<s:date name="employees.endDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  续签合同开始日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="renewalStart" name="renewalStart"
                            type="text" value="<s:date name="employees.renewalStart" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  续签合同结束日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="renewalEnd" name="renewalEnd"
                            type="text" value="<s:date name="employees.renewalEnd" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <%--<div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-post">续签职位</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="postName"
                           value="<s:property value=""/>"/>
                    <input type="hidden" id="postId" name="postId"
                           value="<s:property value=""/>"/>
                  </label>
                </section>
              </div>--%>
              <div class="row">
                <label class="label col col-2">
                  续签薪资(元)
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="renewMoney" id="renewMoney" placeholder="请输入续签薪资(元)" value="<s:property value="employees.renewMoney"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="usersName" name="nextStepApproversName"
                             value="<s:property value="employees.nextStepApproversName"/>"/>
                      <input type="hidden" id="usersId" name="nextStepApproversId"
                             value="<s:property value="employees.nextStepApproversId"/>"/>
                    </label>
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
        <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
        <div class="tab-pane fade in active" id="s3" style="margin: 10px;"></div>
      </div>
    </div>
 </div>
      </div>
    </article>
</div>

<script type="text/javascript">

 /* $('li.disabled').off("click").on("click",function(){
    return false;
  })*/

  /*$(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"employees"
    };
    multiDuty(pdata);
  });*/

  $('#startDate,#endDate,#renewalEnd,#renewalStart').datetimepicker({
      format: 'yyyy-mm-dd',
      weekStart: 1,
      autoclose: true,
      todayBtn: 'linked',
      language: 'zh-CN',
      minView:2
  });
  //返回视图
  $("#btn-re-common").click(function(){
    loadURL("ajax-expiring.action?viewtype=<s:property value="viewtype"/>",$('#content'));
  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    loadURL("ajax-expiring!input.action?keyId="+$("input#keyId").val(),$('#content'));
  });

  //左侧意见栏
  $("a[key=btn-comment]").unbind("click").bind("click",function(){
    var keyId = $("input#keyId").val();
    var actionUrl = "ajax-running!comment.action?bussinessId="+keyId;
    $(this).leftview('init',{
      title:"查看审批意见",
      actionUrl:actionUrl
    });
    var keyId = $("#keyId").val();
    $(this).leftview('foot',{'unid':keyId,'flowName':"expiring",callback:function(){
      var valueObj=$("input#numStatus").val();
      switch(valueObj){
          case "0":
              //保存后再提交
              $("#left_foot_btn_approve").off("click").on("click",function(){
                  form_save("employees","ajax-expiring!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                      $(this).leftview('leftClose');
                      $("#btn-re-common").trigger("click");
                  })
              });
              // approveRequest("approve1");
              break;
        case "1":
          //第一步审批
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("employees","ajax-expiring!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-re-common").trigger("click");
            });

          });
          break;
        case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("employees","ajax-expiring!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
            $(this).leftview('leftClose');
            $("#btn-re-common").trigger("click");
          })
        });
        break;
        case "3":
          //第三步会签
          $("#left_foot_btn_approve").off("click").on("click",function(){
            form_save("employees","ajax-expiring!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
              $(this).leftview('leftClose');
              $("#btn-re-common").trigger("click");
            })
          });
        // approveRequest("approve1");
        break;
      }
      //退回
      $("#left_foot_btn_sendback").off("click").on("click",function(){
        var vActionUrl="ajax-expiring!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("input#keyId").val()};

        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-re-common").trigger("click");
        });
      });
      //否决
      $("#left_foot_btn_deny").off("click").on("click",function(){
        var vActionUrl="ajax-expiring!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("input#keyId").val()};
        ajax_action(vActionUrl,data,{},function(pdata){
          _show(pdata);
          $(this).leftview('leftClose');
          $("#btn-re-common").trigger("click");
        });
      });
    }});
  })

</script>