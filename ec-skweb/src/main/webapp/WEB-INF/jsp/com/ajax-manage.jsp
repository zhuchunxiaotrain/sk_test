<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />

<input type="hidden" name="keyId" id="keyId" value="keyId"/>
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      招聘管理
    </h1>
  </div>
</div>
<%--<div class="row">
  <a id="ajax_payDues_btn_add"  class="btn btn-default pull-right pull-right-fix" ><i class="fa fa-lg fa-plus"></i>党费缴纳</a>

</div>--%>
<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <s:if test="viewtype==1">
          <shiro:hasAnyRoles name="wechat">
            <a id="ajax_manage_btn_add" <s:property value="isCreate('manage')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建招聘申请表</a>
          </shiro:hasAnyRoles>
          </s:if>
          <s:if test="viewtype==2">
          <shiro:hasAnyRoles name="wechat">
            <a id="fn_manage_update" <s:property value="isCreate('manage')"/> class="btn btn-default pull-right pull-right-fix" data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 招聘完成</a>
          </shiro:hasAnyRoles>
          </s:if>
        </form>
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_manage_list_row">
                    <table id="ajax_manage_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_manage_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script >
    function load_manage_jqGrid(){
        jQuery("#ajax_manage_table").jqGrid({
            url:'ajax-manage!list.action?viewtype='+"<s:property value="viewtype" />",
            datatype: "json",
            colNames:["招聘岗位","招聘部门","招聘人数","申请人","提交日期","文档状态","操作","id"],
            colModel:[
                {name:'department',index:'department', search:false,width:80},
                {name:'post',index:'post', search:false,width:80},
                {name:'recruitNum',index:'recruitNum', search:false,width:80},
                {name:'creater',index:'creater', search:false,width:80},
                {name:'createDate',index:'createDate', search:false,width:80},
                {name:'state',index:'state', search:false,width:80},
                {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
                {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
            ],
            rowNum : 10,
            rowList:[10,20,30],
            pager : '#ajax_manage_list_page',
            sortname : 'createDate',
            sortorder : "desc",
            gridComplete:function(){
                var ids=$("#ajax_manage_table").jqGrid('getDataIDs');
                for(var i=0;i<ids.length;i++){
                    var cl=ids[i];
                    var rowData = $("#ajax_manage_table").jqGrid("getRowData",cl);
                    var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_manage_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
                    jQuery("#ajax_manage_table").jqGrid('setRowData',ids[i],{act:de});
                }

                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                jqGridStyle();
            },
            onSelectRow: function (rowId, status, e) {
              /*$(this).gridselect('onecheck',{
               table:"ajax_manage_table",
               rowId:rowId,
               status:status,
               id:"usersId",
               name:"usersName"
               });*/
                var slt =   $("#ajax_manage_table").jqGrid('getGridParam','selarrrow');
                if(slt.length > 1){
                    alert("请选择一条数据");
                    return false;
                }

                var rowId = $("#ajax_manage_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#ajax_manage_table").jqGrid('getRowData', rowId);
                $("#keyId").val(rowId);

            },
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },

            multiselect: true,
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            forceFit:true,
            viewrecords: true,
            autowidth: true,
            height : 'auto',
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_manage_table").jqGrid('setGridWidth', $("#ajax_expiring_list_row").width());
        })

        jQuery("#ajax_manage_table").jqGrid('navGrid', "#ajax_manage_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });

        jQuery("#ajax_manage_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
    }

    $(function(){
        load_manage_jqGrid();
    });
    $("#dialog-ok").unbind("click").bind("click",function(){

        gDialog.fClose();
    });

    function fn_manage_read(id){
        loadURL("ajax-manage!read.action?keyId="+id+"&viewtype="+"<s:property value="viewtype" />",$('#content'));
    }


    //完成按钮
    $("#fn_manage_update").off("click").on("click",function(){
       // loadURL("ajax-manage!update.action?keyId="+$("#keyId").val(),$('#content'));

        form_save("manage","ajax-manage!update.action?keyId="+$("#keyId").val());
        loadURL("ajax-manage.action?viewtype=2",$('#content'));

    });

    //新建
    $("#ajax_manage_btn_add").off("click").on("click",function(){

        loadURL("ajax-manage!input.action",$('#content'));
    });

</script>




















