<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/9/21
  Time: 15:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<input type="hidden" id="dictName" name="dictName" value="<s:property value="dictName"/> " />
<input type="hidden" id="superNode" name="superNode" value="" />
<table id="tree"></table>
<div id="pager"></div>

<script type="text/javascript">

  jQuery(document).ready(function($) {
    jQuery('#tree').jqGrid({
      "url":"<%=path%>/com/ajax-dict!listtree.action?dictName="+ $.trim($("#dictName").val()),
      "colModel":[{"name":"id", "index":"id", "sorttype":"string", "key":true, "hidden":true, "width":50},
                    {"name":"did", "index":"did", "sorttype":"string", "hidden":true, "width":50},
                    {"name":"check","index":"check","width":15,"label":' '},
                  {"name":"name", "index":"name", "sorttype":"string", "label":"类别名称", "width":170},
                  {"name":"state", "index":"state", "sorttype":"string", "label":"状态","align":"center" ,"width":150},
                  {"name":"opt","index":"opt","label":"操作","align":"center"},
//                  {"name":"date", "index":"date", "sorttype":"string", "label":"创建日期", "width":90, "align":"right"},
                  {"name":"lft", "hidden":true},
                  {"name":"rgt", "hidden":true},
                  {"name":"level", "hidden":true},
                  {"name":"uiicon", "hidden":true}],
      "width":""+0.7*$(document).width(),
      "hoverrows":false,
      "viewrecords":false,
      "gridview":true,
      "height":"auto",
      "sortname":"lft",
      "loadonce":true,
      "rowNum":100,
      "scrollrows":true,
      // enable tree grid
      "treeGrid":true,
      // which column is expandable
      "ExpandColumn":"name",
      // datatype
      "treedatatype":"json",
      // the model used
      "treeGridModel":"nested",
      // configuration of the data comming from server
      "treeReader":{
        "left_field":"lft",
        "right_field":"rgt",
        "level_field":"level",
        "leaf_field":"isLeaf",
        "expanded_field":"expanded",
        "loaded":"loaded",
        "icon_field":"icon"
      },
      "sortorder":"asc",
        "gridComplete":function(){
            var ids=$("#tree").jqGrid('getDataIDs');
            for(var i=0;i<ids.length;i++){
                var cl=ids[i];
                var rowData = $("#tree").jqGrid("getRowData",cl);
                var did = $.trim(rowData['did']);
                var de="<button class='btn btn-default' data-original-title='编辑' onclick=\"fn_dict_edit('"+cl+"','"+did+"');\"><i class='fa fa-eye'></i>编辑</button>"+" ";
                var qy="<button class='btn btn-default bg-color-green' data-original-title='启用' onclick=\"fn_dict_enable('"+cl+"','"+did+"');\"><i class='fa fa-lg fa-check-circle-o'></i>启用</button>"+" ";
                var jy="<button class='btn btn-default bg-color-red' data-original-title='禁用' onclick=\"fn_dict_disable('"+cl+"','"+did+"');\"><i class='fa fa-lg fa-ban'></i>禁用</button>"+" ";
                if ($.trim(rowData['state']) == "启用"){
                    jQuery("#tree").jqGrid('setRowData',ids[i],{opt:de+jy});
                } else{
                    jQuery("#tree").jqGrid('setRowData',ids[i],{opt:de+qy});
                }
                var ch = "<input style='width:16px;height:16px;' type='checkbox' id='item_"+cl+"' key='tree_item_checkbox' onchange='tree_item_checked(\""+cl+"\");' />";
                jQuery("#tree").jqGrid('setRowData',ids[i],{check:ch});
            }
            $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
            jqGridStyle();
        },
        "onSelectRow":function(rowId, status, e){
            if (!$("#item_"+rowId).is(":checked")){
                $("#item_"+rowId).prop("checked",true);
                $("#superNode").val(rowId);
                $("input[key='tree_item_checkbox']").each(function(){
                    if($.trim($(this).attr("id"))!=("item_"+rowId))
                        $(this).prop("checked",false);
                });
            } else {
                $("#superNode").val("");
                $("#item_"+rowId).prop("checked",false);
            }
        },
      "datatype":"json",
      "pager":"#pager"
    });
  });
  $(window).on('resize.jqGrid', function() {
      jQuery("#tree").jqGrid('setGridWidth', 0.7*$(document).width());
  })
    function fn_dict_edit(cl,did){
        fn_edit(cl, did);
    }

    function fn_dict_enable(cl,did){
        fn_enable(cl, did);
    }

    function fn_dict_disable(cl,did){
        fn_disabled(cl,did);
    }

    function tree_item_checked(cl,type){
        if($("#item_"+cl).is(":checked")) {
            $("input[key='tree_item_checkbox']").each(function(){
                if($.trim($(this).attr("id"))!=("item_"+cl)){
                    $(this).prop("checked",false);
                }
            });
            $("#superNode").val(cl);
        } else{
            $("#superNode").val("");
        }
    }

</script>
