
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<input type="hidden" name="keyId" id="keyId" value="<s:property value="keyId" />">
<fieldset>

    <ul class="list-group" id="ajax_query">
        <s:if test="dataRows.size() > 0">
            <s:iterator value="dataRows" id="list">
               <li id="<s:property value="#list.id"/>" class="message">
                    <img src="<s:property value="#list.url"/>" class="online" style="width:45px;height:45px">
                    <span class="message-text">
                        <time>
                            <s:property value="#list.startTime"/>
                        </time>
                        <a href="javascript:void(0);" class="username">
                            <i class="fa fa-user txt-color-green"></i>
                            <s:property value="#list.assignee"/>
                            (<s:property value="#list.curDuty.department.name"/>-<s:property value="#list.curDuty.post.name"/>)
                        </a>
                        <span class="subject">
                            <i class="fa fa-comments"></i>
                            <s:property value="#list.commentMsg"/>
						</span>
                        <br/>
                        <span class="msg-body">
                            <s:iterator value="file" id="flist">
                                <i class="fa fa-paperclip"></i>
                                <a id="<s:property value="#flist.keyId"></s:property>" tId="<s:property value="#list.tId"/>" pId="<s:property value="#list.pId"/>"
                                   href="javascript:void(0);"><s:property value="#flist.filename"></s:property></a>
                            </s:iterator>
						</span>
                    </span>
                </li>
            </s:iterator>
        </s:if>
        <s:else>
            <h4> <i class="fa fa-warning"></i> 没有任何审批信息！</h4>
        </s:else>
    </ul>
</fieldset>
<script>
$("span.msg-body a").unbind("click").bind("click",function(e){
    var keyId = $(this).attr("id");
    var taskId= $(this).attr("tId");
    var proId = $(this).attr("pId");
    var data = {keyId:keyId,taskId:taskId,proId:proId};
    var actionUrl = "../com/ajax-running!openDoc.action";
    gDialog.fCreate({
        title:'查看预览',
        url:actionUrl,
        param:data,
        width:1000
    }).show();
})

</script>
