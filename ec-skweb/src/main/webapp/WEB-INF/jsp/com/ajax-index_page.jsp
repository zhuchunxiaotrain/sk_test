
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<style>
    .ui-jqgrid-sortable{text-align: left !important; padding-left:5px}
    .ui-jqgrid-title{display: block;text-align: center}
    /*#notice_table_topic{padding: 2px 0}*/
    .ui-jqgrid{border:1px solid #ccc !important}
    .ui-search-toolbar div{padding: 0px !important;}
    .ui-jqgrid-title more{float: right;color:#4B93B3}
    .ui-jqgrid-title more:hover{cursor:pointer;text-decoration: none;color:#03a9f4}
</style>
<jsp:include page="ajax-top.jsp" />

<div class="row" style="margin-bottom:20px;">
    <div class="col-lg-6 col-sm-6 col-xs-12" id="notice_list" style="padding-right:25px">
        <div class="row" id="notice_list_row">
            <table id="notice_table" class="table table-striped table-bordered table-hover">
            </table>

        </div>
    </div>

    <div class="col-lg-6 col-sm-6 col-xs-12" id="remind_list" style="padding-left:25px">
        <div class="row" id="remind_list_row">
            <table id="remind_table" class="table table-striped table-bordered table-hover">
            </table>

        </div>
    </div>
</div>
<hr style="height:1px;border-top:1px solid #ccc;"/>

<div class="row" style="margin-bottom:20px;">
    <div class="col-lg-4 col-sm-4 col-xs-12" id="todo_list" style="padding-right:30px">
        <div class="row" id="todo_list_row">
            <table id="todo_table" class="table table-striped table-bordered table-hover">
            </table>

        </div>
    </div>


    <div class="col-lg-4 col-sm-4 col-xs-12" id="norecord_list" style="padding-left:15px;padding-right:15px">
        <div class="row" id="norecord_list_row">
            <table id="norecord_table" class="table table-striped table-bordered table-hover">
            </table>

        </div>
    </div>

    <div class="col-lg-4 col-sm-4 col-xs-12" id="record_list" style="padding-left:30px">
        <div class="row" id="record_list_row">
            <table id="record_table" class="table table-striped table-bordered table-hover">
            </table>

        </div>
    </div>
</div>

<hr style="height:1px;border-top:1px solid #ccc;"/>

  <div class="row"  id='calendar'>


  </div>









<script type="text/javascript">
    $(function(){
        load_notice();
        load_remind();
        load_todo();
        load_norecord();
        load_record();
        $('#content').on("click",'#gview_notice_table .ui-jqgrid-title more',function(){
            loadURL("../manage/ajax!notice.action?viewtype=2",$('#content'));
        });
        $('#content').on("click",'#gview_remind_table .ui-jqgrid-title more',function(){
            loadURL("../com/ajax!remindList.action",$('#content'));
        });
        $('#content').on("click",'#gview_todo_table .ui-jqgrid-title more',function(){
            loadURL("../com/ajax!toDoList.action",$('#content'));
        });
        $('#content').on("click",'#gview_norecord_table .ui-jqgrid-title more',function(){
           loadURL("../com/ajax!taskRecordList.action?type=1",$('#content'));
        });
        $('#content').on("click",'#gview_record_table .ui-jqgrid-title more',function(){
            loadURL("../com/ajax!taskRecordList.action?type=2",$('#content'));
        });
    })
    $('#calendar').fullCalendar({
                theme: false,
                lang: 'zh-cn',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },

                defaultDate: '<s:date name="today" format="yyyy-MM-dd"/> ',
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                events: {
                    url: '<%=path%>/com/ajax-index!getNoticeJson.action',
                    error: function() {
                        $('#script-warning').show();
                    }
                 },
                eventClick: function(calEvent, jsEvent, view) {
                    loadURL("../manage/ajax-notice!read.action?keyId="+calEvent.id+"&viewtype=2&index=1",$('#content'));

                },

                loading: function(bool) {
                    //$('#loading').toggle(bool);
                }
    });

    //公告通知
    function load_notice(){
        jQuery("#notice_table").jqGrid({
            url:'../manage/ajax-notice!list.action?viewtype=2',
            datatype: "json",
            colNames:['公告主题',"id"],
            colModel:[
                {name:'topic',index:'topic', width:200,search:false,sortable:false},
                {name:'id',index:'id',search:false,hidden:true,key:true}
            ],
            rowNum :5,
           // rowList:[10,20,30],
          //  pager : '#notice_list_page',
            sortname : '',
            sortorder : "",
            gridComplete:function(){
                $('#gview_notice_table a.ui-jqgrid-titlebar-close').hide();
                $('#gview_notice_table span.ui-jqgrid-title more').remove();
                $('#gview_notice_table span.ui-jqgrid-title').append('<more>More</more>') ;
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                jqGridStyle();
            },
            onSelectRow: function (rowId, status, e) {
                var rowId = $("#notice_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#notice_table").jqGrid('getRowData', rowId);
            },
            ondblClickRow:function(rowid, iRow, iCol, e){
                var rowId = $("#notice_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#notice_table").jqGrid('getRowData', rowId);
                loadURL("../manage/ajax-notice!read.action?keyId="+rowId+"&viewtype=2&index=1",$('#content'));
            },
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },
            caption : "公告通知",
            multiselect : false,
            rownumbers:false,
            gridview:true,
            shrinkToFit:true,
            viewrecords: false,
            autowidth: true,
            height:'auto',
            forceFit:true,
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#notice_table").jqGrid('setGridWidth', $("#notice_list_row").width());
        })
        jQuery("#notice_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});


    };

    //提醒事项
    function load_remind(){
        jQuery("#remind_table").jqGrid({
            url:'../com/ajax-remind!list.action',
            datatype: "json",
            colNames:['提醒内容',"id",'url'],
            colModel:[
                {name:'content',index:'content', width:200,search:false,sortable:false},
                {name:'id',index:'id',search:false,hidden:true,key:true},
                {name:'url',index:'url',search:false,hidden:true}
            ],
            rowNum :5,
            // rowList:[10,20,30],
            //  pager : '#notice_list_page',
            sortname : '',
            sortorder : "",
            gridComplete:function(){
                $('#gview_remind_table a.ui-jqgrid-titlebar-close').hide();
                $('#gview_remind_table span.ui-jqgrid-title more').remove();
                $('#gview_remind_table span.ui-jqgrid-title').append('<more>More</more>') ;
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                jqGridStyle();
            },
            onSelectRow: function (rowId, status, e) {
                var rowId = $("#remind_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#remind_table").jqGrid('getRowData', rowId);
            },
            ondblClickRow:function(rowid, iRow, iCol, e){
                var rowId = $("#remind_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#remind_table").jqGrid('getRowData', rowId);
                loadURL(rowDatas.url+"&index=1",$('#content'));
            },
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },
            caption : "提醒事项",
            multiselect : false,
            rownumbers:false,
            gridview:true,
            shrinkToFit:true,
            viewrecords: false,
            autowidth: true,
            height:'auto',
            forceFit:true,
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#remind_table").jqGrid('setGridWidth', $("#remind_list_row").width());
        })
        jQuery("#remind_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});


    };

    //个人待办事项目
    function load_todo(){
        jQuery("#todo_table").jqGrid({
            url:'../com/ajax-flow!toDoList.action?key=index',
            datatype: "json",
            colNames:['审核内容','所属模块','申请人','url',"id"],
            colModel:[
                {name:'pname',index:'pname', width:200,search:false,sortable:false},
                {name:'module',index:'module', width:100,search:false,sortable:false},
                {name:'toUser',index:'toUser', width:100,search:false,sortable:false},
                {name:'url',index:'url', width:0,search:false,sortable:false,hidden:true},
                {name:'docId',index:'docId',search:false,hidden:true,key:true}
            ],
            rowNum :5,
            // rowList:[10,20,30],
            //  pager : '#notice_list_page',
            sortname : '',
            sortorder : "",
            gridComplete:function(){
                $('#gview_todo_table a.ui-jqgrid-titlebar-close').hide();
                $('#gview_todo_table span.ui-jqgrid-title more').remove();
                $('#gview_todo_table span.ui-jqgrid-title').append('<more>More</more>') ;
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                jqGridStyle();
            },
            onSelectRow: function (rowId, status, e) {
                var rowId = $("#todo_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#todo_table").jqGrid('getRowData', rowId);
            },
            ondblClickRow:function(rowid, iRow, iCol, e){
                var rowId = $("#todo_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#todo_table").jqGrid('getRowData', rowId);
                loadURL(rowDatas.url+"&index=1",$('#content'));
            },
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },
            caption : "个人待办事项",
            multiselect : false,
            rownumbers:false,
            gridview:true,
            shrinkToFit:true,
            viewrecords: false,
            autowidth: true,
            height:'auto',
            forceFit:true,
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#todo_table").jqGrid('setGridWidth', $("#todo_list_row").width());
        })
        jQuery("#todo_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});


    };


    //已办未结
    function load_norecord(){
        jQuery("#norecord_table").jqGrid({
            url:'../com/ajax-taskrecord!list.action?type=1',
            datatype: "json",
            colNames:['审核内容','所属模块','当前操作人','url',"id"],
            colModel:[
                {name:'pname',index:'pname', width:200,search:false,sortable:false},
                {name:'module',index:'module', width:100,search:false,sortable:false},
                {name:'usernames',index:'usernames', width:100,search:false,sortable:false},
                {name:'url',index:'url', width:0,search:false,sortable:false,hidden:true},
                {name:'id',index:'id',search:false,hidden:true,key:true}
            ],
            rowNum :5,
            // rowList:[10,20,30],
            //  pager : '#notice_list_page',
            sortname : '',
            sortorder : "",
            gridComplete:function(){
                $('#gview_norecord_table a.ui-jqgrid-titlebar-close').hide();
                $('#gview_norecord_table span.ui-jqgrid-title more').remove();
                $('#gview_norecord_table span.ui-jqgrid-title').append('<more>More</more>') ;
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                jqGridStyle();
            },
            onSelectRow: function (rowId, status, e) {
                var rowId = $("#norecord_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#norecord_table").jqGrid('getRowData', rowId);
            },
            ondblClickRow:function(rowid, iRow, iCol, e){
                var rowId = $("#norecord_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#norecord_table").jqGrid('getRowData', rowId);
                loadURL(rowDatas.url+"&index=1",$('#content'));
            },
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },
            caption : "已办未结",
            multiselect : false,
            rownumbers:false,
            gridview:true,
            shrinkToFit:true,
            viewrecords: false,
            autowidth: true,
            height:'auto',
            forceFit:true,
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#norecord_table").jqGrid('setGridWidth', $("#norecord_list_row").width());
        })
        jQuery("#norecord_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});


    };

    //已办已结
    function load_record(){
        jQuery("#record_table").jqGrid({
            url:'../com/ajax-taskrecord!list.action?type=2',
            datatype: "json",
            colNames:['审核内容','所属模块','url',"id"],
            colModel:[
                {name:'pname',index:'pname', width:200,search:false,sortable:false},
                {name:'module',index:'module', width:100,search:false,sortable:false},
                {name:'url',index:'url', width:0,search:false,sortable:false,hidden:true},
                {name:'id',index:'id',search:false,hidden:true,key:true}
            ],
            rowNum :5,
            // rowList:[10,20,30],
            //  pager : '#notice_list_page',
            sortname : '',
            sortorder : "",
            gridComplete:function(){
                $('#gview_record_table a.ui-jqgrid-titlebar-close').hide();
                $('#gview_record_table span.ui-jqgrid-title more').remove();
                $('#gview_record_table span.ui-jqgrid-title').append('<more>More</more>') ;
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                jqGridStyle();
            },
            onSelectRow: function (rowId, status, e) {
                var rowId = $("#record_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#record_table").jqGrid('getRowData', rowId);
            },
            ondblClickRow:function(rowid, iRow, iCol, e){
                var rowId = $("#record_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#record_table").jqGrid('getRowData', rowId);
                loadURL(rowDatas.url+"&index=1",$('#content'));
            },
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },
            caption : "已办已结",
            multiselect : false,
            rownumbers:false,
            gridview:true,
            shrinkToFit:true,
            viewrecords: false,
            autowidth: true,
            height:'auto',
            forceFit:true,
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#record_table").jqGrid('setGridWidth', $("#record_list_row").width());
        })
        jQuery("#record_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});


    };
</script>
