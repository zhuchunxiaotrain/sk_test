<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/2/4
  Time: 13:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<div class="row" style="padding:2px">
  <div class="col-sm-12 col-md-12 col-lg-12">
    <div>
      <form id="searchForm" method="post" action="">
        <a id="ajax_flow_back" class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-mail-reply-all"></i>返回</a>
        <input id="keyId" name="keyId" hidden value="<s:property value="keyId"></s:property> "/>
      </form>
    </div>
  </div>
</div>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

  <!-- row -->
  <div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <!-- Widget ID (each widget will need unique ID)-->
      <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
           data-widget-colorbutton="false"
           data-widget-togglebutton="false"
           data-widget-deletebutton="false"
           data-widget-fullscreenbutton="true"
           data-widget-custombutton="false"
           data-widget-sortable="false">

        <!-- widget div-->
        <div>

          <!-- widget edit box -->
          <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->

          </div>
          <!-- end widget edit box -->

          <!-- widget content -->
          <div class="widget-body no-padding">
            <div class="col-sm-12 col-md-12 col-lg-12">
              <div class=" ">
                <div class="row" id="ajax_flow_list_row">
                  <table id="ajax_flow_table" class="table table-striped table-bordered table-hover">
                  </table>
                  <div id="ajax_flow_list_page">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end widget content -->

        </div>
        <!-- end widget div -->

      </div>
      <!-- end widget -->

    </article>
    <!-- WIDGET END -->

  </div>

  <!-- end row -->
</section>
<!-- end widget grid -->


<script type="text/javascript">

  function loadInbox() {
    loadURL("../com/ajax!flow.action?keyId=", $('#content'));
  }

  function reload(){
    loadURL("../com/ajax!flow.action",$('#content'));
  }

  function hrefFormat(cellvalue, options, cell){
    var keyId = cell["id"];
    var href = "<%=path%>/com/ajax-running!list.action?ketId="+cell["id"];
    return "<a href='javascript:void(0);' onclick=openRunning('"+keyId+"') id="+keyId+" key='running_list'>"+cellvalue+"</a>";
  }

  $(function(){
    jQuery("#ajax_flow_table").jqGrid({
      url:'../com/ajax-flow!listHistory.action?keyId='+$("#keyId").val(),
      datatype: "json",
      colNames:['流程定义编号','流程名称','创建时间',"id","version"],
      colModel:[
        {name:'pid',index:'pid', width:50},
        {name:'name',index:'name', width:50},
        {name:'time',index:'time', width:50},
//        {name:'act',index:'act', width:200,sortable:false,fixed:true},
        {name:'id',index:'id', width:50,hidden:true},
        {name:'version',index:'version', width:50}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_flow_list_page',
      sortname : 'id',
      sortorder : "asc",
      gridComplete:function(){
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");//暂时这样以后再想办法(浏览器问题chrome)
        jqGridStyle();
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 历史信息",
      multiselect : false,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_flow_table").jqGrid('setGridWidth', $("#ajax_flow_list_row").width());
    })

    jQuery("#ajax_flow_table").jqGrid('navGrid', "#ajax_flow_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  });

  function openRunning(id){
    loadURL("../com/ajax-running!list.action?keyId="+id,$('#content'));
  }
  function fn_flow_history(id){
    gDialog.fCreate({
      title:'查看流程图',
      url:"ajax-flow!export.action?keyId="+id,
      width:1000
    }).show();
  }

  $("#ajax_flow_back").click(function(){
    loadURL("../com/ajax!flow.action",$('#content'));
  });
  function fn_flow_histroy(id){
    loadURL("../com/ajax-config!input1.action?keyId="+id,$('#content'));
  }
</script>


