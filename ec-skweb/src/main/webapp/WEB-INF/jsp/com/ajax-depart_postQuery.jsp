<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

    <fieldset>
        <ul class="list-group">
            <s:iterator value="postArrayList" id="list">
               <li class="list-group-item td-li">
                    <div class="checkbox">
                        <label>
                          <input name="power_set" id="<s:property value="#list.id"/>" type="checkbox" class="checkbox style-0" <s:property value="#list.checked"/>>
                          <span><s:property value="#list.name" /></span>
                        </label>
                    </div>
                </li>
            </s:iterator>
        </ul>
    </fieldset>

<script>
    $("input[name='power_set']").unbind("click").bind("click",function(){
       if($(this).prop("checked")){
           $("input[name='power_set']:checked").attr("checked",false);
           $(this).prop('checked',true);
       }
        var keyId=$(this).attr("id");
        var vActionUrl = "<%=path%>/com/ajax-power!setPowerDefault.action";
        data={keyId:keyId, departId:$('#keyId').val()};
        ajax_action(vActionUrl,data,{},function(pdata){
            _show(pdata);
        });
        jQuery("#ajax_depart_table").trigger("reloadGrid");
    });
</script>