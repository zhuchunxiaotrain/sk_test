<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>招聘申请</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 招聘申请表 </a>
            </li>
            <li class="disabled">
              <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 面试登记表 </a>
            </li>
            <li class="disabled">
              <a href="#s3" id="other2" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 录用审核</a>
            </li>
          </ul>

          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
              <div class="row">
                <shiro:hasAnyRoles name="wechat">
                  <a style="margin-top: -13px !important;" <s:property value="isEdit(manage.id)"/> class="btn btn-default pull-right pull-right-fix" key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
                </shiro:hasAnyRoles>
              </div>
          <hr class="simple">
          <form id="manage" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="manage.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="numStatus" id="numStatus"  value="<s:property value="numStatus" />"/>
            <header  style="display: block;">
              招聘申请&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  招聘部门
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="departName" name="departName"  value="<s:property value="manage.recruitDepart.name"/>"/>
                      <input type="hidden" id="departId" name="departId" value="<s:property value="manage.recruitDepart.id"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  招聘岗位
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="postName" name="postName" value="<s:property value="manage.post.name"/>"/>
                      <input type="hidden" id="postId" name="postId" value="<s:property value="manage.post.id"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  招聘人数
                </label>
                <section class="col col-5">
                  <label class="input state-disabled" >
                    <input type="text" disabled name="recruitNum" id="recruitNum"  value="<s:property value="manage.recruitNum"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  性别
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled  name="sex" id="sex"  value="<s:property value="manage.sex.value()"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  年龄
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="age" id="age" disabled value="<s:property value="manage.age"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  学历
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="edu" id="edu" disabled value="<s:property value="manage.edu"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  专业
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="profession" id="profession" disabled value="<s:property value="manage.profession"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  工作经验
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="experience" id="experience" disabled value="<s:property value="manage.experience"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                  <label class="label col col-2">
                    <i class="fa fa-asterisk txt-color-red"></i>
                    面试负责人
                  </label>
                  <section class="col col-5">
                      <label class="input state-disabled">
                        <input disabled  type="text" name="chargeUsersName" value="<s:iterator id="list" value="manage.chargeUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                      </label>
                    <label class="input state-disabled">
                        <input   type="hidden" id="chargeUsersId" name="chargeUsersId" value="<s:iterator id="list" value="manage.chargeUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                      </label>
                  </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  其他要求
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="others" id="others" disabled value="<s:property value="manage.others"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  会签人
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <label class="input state-disabled">
                      <input disabled type="text" id="usersName" name="nextStepApproversName" disabled
                             value="<s:property value="manage.nextStepApproversName"/>"/>
                    </label>
                    <label class="input state-disabled">
                      <input  type="hidden" id="usersId" name="nextStepApproversId" disabled
                             value="<s:property value="manage.nextStepApproversId"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text" name="applyUserName" id="applyUserName" placeholder="" value="<s:property value="manage.creater.name"/>" >
                    <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="manage.creater.id"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  操作日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input  disabled type="text" name="applyDate" id=applyDate"  value="<s:date name="manage.createDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
          <div class="flow">
                <s:if test="manage.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer"style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
          </div>
        </div>
        <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
        <div class="tab-pane fade in active" id="s3" style="margin: 10px;"></div>
       </div>
    </div>
  </div>
      </div>
  </article>
</div>

<script type="text/javascript">
/*
       $('li.disabled').off("click").on("click", function () {
           return false;
       })
*/

    var state = "<s:property value="manage.getProcessState().name()" />";
    if(state == "Finished"){
        $('#other1').parent('li').removeClass('disabled');
    }
    var table_global_width=0;
    $("a#other1").off("click").on("click",function(e) {
        if(state == "Finished"){
            if (table_global_width == 0) {
                table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
            }
            loadURL("ajax-interview.action?parentId="+$("#keyId").val(),$('div#s2'));
        }else{
            return false;
        }

    });

    var state = "<s:property value="manage.getProcessState().name()" />";
    if(state == "Finished"){
        $('#other2').parent('li').removeClass('disabled');
    }

    var table_global_width=0;
    $("a#other2").off("click").on("click",function(e) {
        if(state == "Finished"){
            if (table_global_width == 0) {
                table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s3").width();
            }
            loadURL("ajax-employment.action?parentId="+$("#keyId").val(),$('div#s3'));
        }else{
            return false;
        }

    });

    //返回视图
    $("#btn-re-common").click(function(){
        var index = "<s:property value="index" />";
        var todo = "<s:property value="todo" />";
        var remind = "<s:property value="remind" />";
        var record = "<s:property value="record" />";
        var draft = "<s:property value="draft" />";
        if(index==1){
            loadURL("ajax-index!page.action",$('#content'));
        }else if(todo==1){
            loadURL("ajax!toDoList.action",$('#content'));
        }else if(remind==1){
            loadURL("ajax!remindList.action",$('#content'));
        }else if(record==1){
            loadURL("ajax!taskRecordList.action?type=1",$('#content'));
        }else if(record==2){
            loadURL("ajax!taskRecordList.action?type=2",$('#content'));
        }else if(draft==1){
            loadURL("ajax!draftList.action",$('#content'));
        }else{
            loadURL("ajax-manage.action?viewtype=<s:property value="viewtype"/>",$('#content'));
        }

    });

    $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
        var showDuty = false;
        var area = $("span.textarea-controls");
        $(pdata.data.datarows).each(function(i,v){
            var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
            $(area).append(str);
            if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                showDuty = true;
            }
        });
        if(showDuty == true){
            var pdata = {
                keyId: $("input#keyId").val(),
                flowName: "manage"
            };
            multiDuty(pdata);
        }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
        case "0":
            //保存后再提交
            $("#left_foot_btn_approve").off("click").on("click",function(){
                form_save("manage","ajax-manage!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                    $("#btn-re-common").trigger("click");
                })
            });
            break;
        case "1":
            //第一步审批
            $("#left_foot_btn_approve").off("click").on("click",function(){
                form_save("manage","ajax-manage!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                    $("#btn-re-common").trigger("click");
                });
            });
            break;
        case "2":
            //第二步审批
            $("#left_foot_btn_approve").off("click").on("click",function(){
                form_save("manage","ajax-manage!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                    $("#btn-re-common").trigger("click");
                })
            });
            break;
        case "3":
            //第三步审批
            $("#left_foot_btn_approve").off("click").on("click",function(){
                form_save("manage","ajax-manage!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                    $("#btn-re-common").trigger("click");
                })
            });
            break;
    }
    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
        var vActionUrl="ajax-manage!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("input#keyId").val()};
        ajax_action(vActionUrl,data,{},function(pdata){
            _show(pdata);
            $("#btn-re-common").trigger("click");
        });
    });
    //否决
    $("#left_foot_btn_deny").off("click").on("click",function(){
        var vActionUrl="ajax-manage!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
        var data={keyId:$("input#keyId").val()};
        ajax_action(vActionUrl,data,{},function(pdata){
            _show(pdata);
            $("#btn-re-common").trigger("click");
        });
    });
    //流程信息展开
    $('#flow,#next').click(function(){
        if($(this).hasClass("right")){
            $(this).removeClass("right").addClass("down");
            $(this).parent(".f_title").next("div.f_content").show();
        }else{
            $(this).removeClass("down").addClass("right");
            $(this).parent(".f_title").next("div.f_content").hide();
        }
    });
  });

    //编辑
    $("a[key=ajax_edit]").click(function(){
        var draft = "<s:property value="draft" />";
        loadURL("ajax-manage!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
    });


</script>