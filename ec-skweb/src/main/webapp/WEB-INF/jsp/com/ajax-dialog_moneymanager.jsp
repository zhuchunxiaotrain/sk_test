<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<input type="hidden" id="payDate" value="<s:property value="payDate"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_moneymanager_list_row">
      <table id="ajax_moneymanager_list_table">
      </table>
      <div id="ajax_moneymanager_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script tex>
  function run_jqgrid_function(){
    jQuery("#ajax_moneymanager_list_table").jqGrid({
      url:'../party/ajax-moneymanager!listfordialog.action?payDate='+$("#payDate").val(),
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['年份','月份','部门','当月上缴总金额','创建者','文档状态','Id'],
      colModel : [
        {name:'registerYear',index:'registerYear', width:100,sortable:false,search:true,sorttype:'string'},
        {name:'registerMonth',index:'registerMonth', width:100,sortable:false,search:true,sorttype:'string'},
        {name:'department',index:'department_name', width:150,sortable:false,search:true,sorttype:'string'},
        {name:'cumulativeAmountPartyMoney',index:'cumulativeAmountPartyMoney', width:150,sortable:false,search:true,sorttype:'string'},
        {name:'creater',index:'creater_name', width:100,sortable:false,search:true,sorttype:'string'},
         {name:'state',index:'state', width:100,sortable:false,search:true,sorttype:'string'},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_moneymanager_list_page',
      sortname : 'createDate',
      sortorder : "desc",
      gridComplete:function(){
        var param=$("#param").val();
        var moneyManagerId=$("#moneyManagerId").val();
        $("#ajax_moneymanager_list_table").jqGrid('setSelection',moneyManagerId);
        jqGridStyle();
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      },
      onSelectRow: function (rowId, status, e) {
          var rowId = $("#ajax_moneymanager_list_table").jqGrid('getGridParam','selrow');
          var rowDatas = $("#ajax_moneymanager_list_table").jqGrid('getRowData', rowId);
          $("#postName").val(rowDatas["post"]);
          $("#moneyManagerId").val(rowId);

      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect:false,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_moneymanager_list_table").jqGrid('setGridWidth', $("#ajax_moneymanager_list_row").width()-10);
    })

    jQuery("#ajax_moneymanager_list_table").jqGrid('navGrid', "#ajax_moneymanager_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_moneymanager_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
    gDialog.fClose();
  });
</script>
