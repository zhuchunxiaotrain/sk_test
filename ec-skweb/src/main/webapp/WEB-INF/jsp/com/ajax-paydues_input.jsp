<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-sm-12 col-md-12 col-lg-12">
        <div class="jarviswidget well" id="wid-id-3"
             data-widget-colorbutton="false"
             data-widget-editbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-custombutton="false"
             data-widget-sortable="false">
            <!-- widget div-->
            <div>
                <div class="widget-body">

                    <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>党费缴纳信息</a>
                    <s:if test="payDues==null || payDues.getProcessState().name()=='Draft'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
                    </s:if>
                    <s:if test="payDues!=null && payDues.getProcessState().name()=='Backed'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
                    </s:if>
                    <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>

                    <hr class="simple">
                    <form id="payDues" class="smart-form" novalidate="novalidate" action="" method="post">
                        <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId" />"/>
                        <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>

                        <header  style="display: block;">
                            党费缴纳信息&nbsp;&nbsp;<span id="title"></span>
                        </header>
                        <fieldset>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    缴纳金额
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="money" id="money" placeholder="请输入缴纳金额" value="<s:property value="payDues.money"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    缴纳日期
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input  placeholder="请选择缴纳日期" id="payDate" name="payDate"
                                                type="text" value="<s:date name="payDues.payDate" format="yyyy-MM-dd"/>">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </article>
</div>

<script>
    
    $('#payDate').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN',
        minView:2
    });

    //校验
    $("#payDues").validate({
        rules : {
            money:{
                required : true,
                number : true,
                min:0
            },
            payDate:{
                required : true
            }
        },
        messages : {
            money: {
                required : '请输入缴纳金额',
                number :  '请输入数字',
                min : '请输入一个大于0的数字'
            },
            payDate : {
                required : '请输入缴纳日期'
            }
        },
        ignore: "",
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });

    //保存
    $("#btn-save-common").click(

        function(){
            if(!$("#payDues").valid()){
                return false;
            }
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("payDues","ajax-paydues!save.action");
            loadURL("ajax-employees.action",$('#content'));

        }

    );
    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#payDues").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#payDues").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("payDues","ajax-paydues!commit.action");
                    loadURL("ajax-employees.action",$('#content'));
                }
            });
        }
    );

    $("#btn-re-common").click(function(){
     loadURL("ajax-employees.action",$('#content'));
    });
    
    

</script>