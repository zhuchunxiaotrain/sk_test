<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String jsessionid=session.getId();
%>
<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-sm-12 col-md-12 col-lg-12">
        <div class="jarviswidget well" id="wid-id-3"
             data-widget-colorbutton="false"
             data-widget-editbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-custombutton="false"
             data-widget-sortable="false">
            <!-- widget div-->
            <div>
                <div class="widget-body">

                    <a class="btn btn-default" id="btn-re-records" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>奖惩记录</a>
                    <s:if test="records==null || records.getProcessState().name()=='Draft'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
                    </s:if>
                    <s:if test="records!=null && records.getProcessState().name()=='Backed'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
                    </s:if>
                    <a class="btn btn-default pull-right pull-right-fix" id="btn-save-records" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>

                    <hr class="simple">
                    <form id="records" class="smart-form" novalidate="novalidate" action="" method="post">
                        <input  type="hidden" name="parentId" id="parentId" value="<s:property value="parentId" />"/>
                        <input  type="hidden" name="keyId" id="keyId" value="<s:property value="records.id" />"/>

                        <header  style="display: block;">
                            奖惩记录&nbsp;&nbsp;<span id="title"></span>
                        </header>
                        <fieldset>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    奖惩类型
                                </label>
                                <section class="col col-5">
                                    <div class="inline-group" name="types">
                                        <label class="radio">
                                            <input type="radio" checked="checked" name="types" value="0" <s:property value="records.types==0?'checked':''"/>>
                                            <i></i>奖励</label>
                                        <label class="radio">
                                            <input type="radio" name="types" value="1" <s:property value="records.types==1?'checked':''"/>>
                                            <i></i>惩罚</label>
                                    </div>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    奖惩名称
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="name" id="name" placeholder="请输入奖惩名称" value="<s:property value="records.name"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    奖惩时间
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input  placeholder="请选择入奖惩时间" id="rpDate" name="rpDate"
                                                type="text" value="<s:date name="records.rpDate" format="yyyy-MM-dd"/>">
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">相关附件</label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input  name="uploadify" id="accessoriesFileName" placeholder="" type="file" >
                                        <input name="accessoriesFileId" id="accessoriesFileId" style="display: none" value="<s:property value="accessoriesFileId"/>">
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    提交人
                                </label>
                                <section class="col col-4">
                                    <label class="input state-disabled">
                                        <input  disabled type="text" name="creater" id="creater" placeholder="" value="<s:property value="creater"/>" >
                                    </label>
                                </section>
                                <label class="label col col-2">
                                    提交日期
                                </label>
                                <section class="col col-4 ">
                                    <label class="input state-disabled">
                                        <input  disabled type="text" name="createDate" id=createDate" placeholder="" value="<s:property value="createDate"/>" >
                                    </label>
                                </section>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </article>
</div>

<script>
    var draft = "<s:property value="draft" />";

    //上传
    inputLoad({
        objId:"accessoriesFileName",
        entityName:"accessoriesId",
        sourceId:"accessoriesFileId",
        jsessionid:"<%=jsessionid%>"
    });

    $('#rpDate').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN',
        minView:2
    });

    //保存
    $("#btn-save-records").click(

        function(){
            $("#btn-save-records").attr("disabled", "disabled");
            form_save("records","ajax-records!save.action",null,function (data) {

                if(data.state == "200" || data.state ==200) {
                    if(draft== 1){
                        location.href='index.action';
                    }else {
                        loadURL("ajax-records.action?parentId=" + $("input#parentId").val(), $('div#s5'));
                    }
                }
            });

        }

    );

    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#records").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#records").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("records","ajax-records!commit.action",null,function (data) {

                        if(data.state == "200" || data.state ==200) {
                            if(draft== 1){
                                location.href='index.action';
                            }else {
                                loadURL("ajax-records.action?parentId=" + $("input#parentId").val(), $('div#s5'));
                            }
                        }

                    });

                }
            });
        }
    );

    $("#btn-re-records").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-records.action?parentId="+$("#parentId").val(),$('div#s5'));
        }
    });

</script>