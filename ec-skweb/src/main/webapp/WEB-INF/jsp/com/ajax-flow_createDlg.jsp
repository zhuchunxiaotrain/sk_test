<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<div class="modal-body">
    <div class="row">
            <div class="col-md-12">

            <form id="flow_input" class="smart-form" novalidate="novalidate" action="" method="post">
                <header>
                   [创建流程]
                </header>
                <fieldset>
                    <div class="row">
                        <label class="label col col-3">流程名称</label>
                        <section class="col col-9">
                            <label class="input">
                                <input type="text" name="name" id="name" placeholder="请输入流程名称">
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <label class="label col col-3">流程关键字</label>
                        <section class="col col-9">
                            <label class="input">
                                <input name="key" id="key" placeholder="请输入关键字" type="text">
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <label class="label col col-3">流程描述</label>
                        <section class="col col-9">
                            <label class="input">
                                <input name="description" id="description" placeholder="流程描述"
                                type="text">
                            </label>
                        </section>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" id="dialog-ok">确定</button>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>
      //校验
        $("#flow_input").validate({
            rules : {
                name: {
                    required : true
                },
                key:{
                    required : true
                }
            },
            messages : {
                 name : {
                     required : '请输入流程名称'
                 },
                 key:{
                     required : '请输入流程关键字'
                 }
            },
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });
    $("#dialog-ok").unbind("click").bind("click",function(){
        var $valid = $("#flow_input").valid();
        if(!$valid) return false;
        var actionUrl = "<%=path%>/com/ajax-modeler!save.action";
        gDialog.fClose();
        form_save("flow_input",actionUrl,{msg:false},function(pdata){
            var modelerId = pdata.data.modelerId;
            var href = "<%=path%>/modeler.html?modelId="+modelerId;
            var width = window.innerWidth-100;
            var iframe = "<iframe class='dframe' src='"+href+"'></iframe>";
//            gDialog.fCreate({
//                title:'创建流程模型',
//                content:iframe,
//                width:width
//            }).show();
            window.open(href);
        });

        jQuery("#ajax_users_table").trigger("reloadGrid");
    });
</script>