<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<div class="modal-body">
<div class="row">
	<div class="col-md-12">
		<form id="post_read" class="smart-form" novalidate="novalidate" action="" method="post">
			<input type="hidden" name="keyId" id="keyId" value="<s:property value="post.id" />">
			<s:if test="post == null">
				<header>
					[新增岗位]
				</header>
			</s:if>
			<s:else>
				<header>
					[编辑岗位]
				</header>
			</s:else>
			<fieldset>

				<div class="row">
					<label class="label col col-3" >岗位名称</label>
					<section class="col col-9">
						<label class="input">
							<input  name="name" id="name" placeholder="请输入岗位名称"
							type="text" value="<s:property value="post.name" />">
						</label>
					</section>
				</div>
				<s:if test="post==null">
					<div class="row">
						<label class="label col col-3" >岗位标识</label>
						<section class="col col-9">
							<label class="input">
								<input name="pname" id="pname" placeholder="请输入岗位标识"
										type="text" value="<s:property value="post.pname" />">
							</label>
						</section>
					</div>
				</s:if>
				<s:else>
					<div class="row">
						<label class="label col col-3" >岗位标识</label>
						<section class="col col-9">
							<label class="input">
								<input  disabled name="pname" id="pname" placeholder="请输入岗位标识"
										type="text" value="<s:property value="post.pname" />">
							</label>
						</section>
					</div>
				</s:else>
				<div class="row">
					<label class="label col col-3" >岗位简介</label>
					<section class="col col-9">
						<label class="input">
							<input  name="description" id="description" placeholder="请输入岗位描述信息"
							type="text" value="<s:property value="post.description" />">
						</label>
					</section>
				</div>

			</fieldset>
		</form>
	</div>
 </div>
</div>
<div class="modal-footer">
	<a href="#../com/ajax!post.action" class="btn btn-primary" id="dialog-ok">确定</a>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>
    $("#dialog-ok").unbind("click").bind("click",function(){
		var $valid = $("#post_read").valid();
		if(!$valid) return false;
        var actionUrl = "<%=path%>/com/ajax-post!save.action";
        var opt={dialogId:'dialog-post_message'};
        form_save("post_read",actionUrl,opt);
        gDialog.fClose();
        jQuery("#ajax_post_table").trigger("reloadGrid");
    });
</script>
<script>
	//校验
	$("#post_read").validate({
		rules : {
		    name:{
		        required : true
		    },
			pname:{
				required : true,
				remote:{                                          //验证标识是否存在
					type:"POST",
					url:"../com/ajax-post!isUnique.action",             //servlet
					data:{
						keyId:$("#keyId").val(),
						validatePro:"pname",
						newValue:function(){return $("#pname").val();}
					}
				}
			}
		},
		messages : {
		    name:{
                required:'请输入岗位名称'
            },
			pname:{
				required:'请输入岗位标识',
				remote:jQuery.format("该岗位已经存在")
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
</script>