<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_selectionnotice_list_row">
      <table id="ajax_selectionnotice_list_table">
      </table>
      <div id="ajax_selectionnotice_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script tex>
  function run_jqgrid_function(){
    jQuery("#ajax_selectionnotice_list_table").jqGrid({
      url:'../party/ajax-selectionnotice!listfordialog.action',
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['岗位','报名周期','公告创建者','Id'],
      colModel : [
        {name:'post',index:'post', width:200,sortable:false,search:true,sorttype:'string'},
        {name:'selectDate',index:'selectDate', width:200,sortable:false,search:true,sorttype:'string'},
        {name:'creater',index:'creater', width:200,sortable:false,search:true,sorttype:'string'},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_selectionnotice_list_page',
      sortname : 'createDate',
      sortorder : "desc",
      gridComplete:function(){
        var param=$("#param").val();
        var selectionNoticeId=$("#selectionNoticeId").val();
        $("#ajax_selectionnotice_list_table").jqGrid('setSelection',selectionNoticeId);
        jqGridStyle();
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      },
      onSelectRow: function (rowId, status, e) {
          var rowId = $("#ajax_selectionnotice_list_table").jqGrid('getGridParam','selrow');
          var rowDatas = $("#ajax_selectionnotice_list_table").jqGrid('getRowData', rowId);
          $("#postName").val(rowDatas["post"]);
          $("#selectionNoticeId").val(rowId);

      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_selectionnotice_list_table").jqGrid('setGridWidth', $("#ajax_selectionnotice_list_row").width()-10);
    })

    jQuery("#ajax_selectionnotice_list_table").jqGrid('navGrid', "#ajax_selectionnotice_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_selectionnotice_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
    gDialog.fClose();
  });
</script>
