<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<style>
    .ui-search-toolbar div{padding: 0px !important;}
</style>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
        <i class="fa fa-table fa-fw "></i>
        草稿
    </h1>
  </div>
</div>
<input type="hidden" name="type" value='<s:property value="type" />' id="type" />
<div id="draft_data">

  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_draft_table" class="table table-striped table-bordered table-hover">
                    </table>
                      <div id="ajax_draft_list_page">
                      </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function() {

      load_draft_jqGrid();
       
  });

  function load_draft_jqGrid(){
    jQuery("#ajax_draft_table").jqGrid({
        url:'../com/ajax-flow!draftList.action?key=index',
        datatype: "json",
        colNames:['审核内容','所属模块','创建时间','操作','key','url',"docId"],
        colModel:[
            {name:'pName',index:'pName', width:200,search:false,sortable:false},
            {name:'module',index:'module', width:100,search:false,sortable:false},
            {name:'time',index:'time', width:100,sortable:false,search:false},
            {name:'act',index:'act', width:100,sortable:false,search:false,fixed:true},
            {name:'key',index:'key', width:0,search:false,sortable:false,hidden:true},
            {name:'url',index:'url', width:0,search:false,sortable:false,hidden:true},
            {name:'docId',index:'docId',search:false,hidden:true,key:true}
        ],
        rowNum : 10,
        rowList:[10,20,30],
      pager : '#ajax_draft_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
          var ids=$("#ajax_draft_table").jqGrid('getDataIDs');
          for(var i=0;i<ids.length;i++){
              var cl=ids[i];
              var rowData = $("#ajax_draft_table").jqGrid("getRowData",cl);
              var key = rowData.key;
              var de="<button class='btn btn-default' data-original-title='删除' onclick=\"fn_draft_destroy('"+cl+"','"+key+"' );\"><i class='fa fa-eye'></i>删除</button>"+" ";
              jQuery("#ajax_draft_table").jqGrid('setRowData',ids[i],{act:de});
          }
          $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
          jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_draft_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_draft_table").jqGrid('getRowData', rowId);
      },
        ondblClickRow:function(rowid, iRow, iCol, e){
            var rowId = $("#ajax_draft_table").jqGrid('getGridParam','selrow');
            var rowDatas = $("#ajax_draft_table").jqGrid('getRowData', rowId);
            loadURL(rowDatas.url,$('#content'));
        },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
        caption : "草稿",
        multiselect : false,
        rownumbers:false,
        gridview:true,
        shrinkToFit:true,
        viewrecords: false,
        autowidth: true,
        height:'auto',
        forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_draft_table").jqGrid('setGridWidth', $("#ajax_draft_list_row").width());
    })
    jQuery("#ajax_draft_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

      jQuery("#ajax_draft_table").jqGrid('navGrid', "#ajax_draft_list_page", {
          edit : false,
          add : false,
          del : false,
          search:false
      });

  };


  function fn_draft_destroy(id,key){
      ajax_action("../com/ajax-flow!draftDelete.action?keyId="+id+"&key="+key,{},{},function(pdata){
          if(pdata){
              _showResult(pdata);
          }});
      loadURL("ajax!draftList.action",$('#content'));
     // loadURL("../com/ajax-flow!draftDelete.action?keyId="+id+"&key="+key,$('#content'));
  }

</script>





















