<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<input type="hidden" id="parentId" value="<s:property value="parentId"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_selectionspeech_list_row">
      <table id="ajax_selectionspeech_list_table">
      </table>
      <div id="ajax_selectionspeech_list_page_1">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script tex>
  function run_jqgrid_function(){
    jQuery("#ajax_selectionspeech_list_table").jqGrid({
      url:'../party/ajax-selectionspeech!listfordialog.action?parentId=<s:property value="parentId"/>',
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['选拔人员','应聘岗位','竞聘演讲结果','Id'],
      colModel : [
        {name:'name',index:'name', width:150,sortable:false,search:true,sorttype:'string'},
          {name:'post',index:'post', width:150,sortable:false,search:true,sorttype:'string'},
        {name:'result',index:'result', width:150,sortable:false,search:true,sorttype:'string'},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_selectionspeech_list_page_1',
      sortname : 'createDate',
      sortorder : "desc",
        gridComplete:function(){
            var ids=$("#ajax_selectionspeech_list_table").jqGrid('getDataIDs');
             var selectionSpeechsId;
             var param="<s:property value="param"/>"
            if(param=="selectionPublicity"){
                var selectionSpeechsId=$("#selectionSpeechId").val();
            }
            for(var i=0;i<ids.length;i++) {
                var cl = ids[i];
                if (selectionSpeechsId != "" && selectionSpeechsId != null) {
                    var idsArr = selectionSpeechsId.split(",");
                    for (var s = 0; s < idsArr.length; s++) {
                        if (idsArr[s] == cl) {
                            $("#ajax_selectionspeech_list_table").jqGrid("setSelection", cl);
                        }
                    }
                }
            }
            jqGridStyle();
            $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        },
        onSelectRow: function (rowId, status, e) {
            var selectionSpeechId="";
            var selectionSpeechApplyName="";
            var flag=false;
            var param="<s:property value="param"/>"
            if(param=="selectionPublicity"){
                selectionSpeechId="selectionSpeechId";
                selectionSpeechApplyName="selectionSpeechApplyName";
                flag=true;
            }
            if(flag==true){
                $(this).gridselect('onecheck',{
                    table:"ajax_selectionspeech_list_table",
                    rowId:rowId,
                    status:status,
                    id:selectionSpeechId,/*dictId是中间值*/
                    name:selectionSpeechApplyName,
                    type:"radio"/*单选按钮*/
                });
            }else{
                $(this).gridselect('morecheck',{
                    table:"ajax_selectionspeech_list_table",
                    rowId:rowId,
                    status:status,
                    id:selectionSpeechId,/*dictId是中间值*/
                    name:selectionSpeechApplyName,
                    type:"checkbox"/*多选按钮*/
                });
            }
        },
        /*onSelectAll:function(rowId, status, e){
            var selectionSpeechId="";
            var selectionSpeechApplyName="";
            selectionSpeechId="selectionSpeechId";
            selectionSpeechApplyName="selectionSpeechApplyName";
            $(this).gridselect('morecheck',{
                table:"ajax_selectionspeech_list_table",
                rowId:rowId,
                status:status,
                id:"selectionSpeechId",
                name:"selectionSpeechApplyName",
            });
        },*/

      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_selectionspeech_list_table").jqGrid('setGridWidth', $("#ajax_selectionspeech_list_row").width()-10);
    })

    jQuery("#ajax_selectionspeech_list_table").jqGrid('navGrid', "#ajax_selectionspeech_list_page_1", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_selectionspeech_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
    gDialog.fClose();
  });
</script>
