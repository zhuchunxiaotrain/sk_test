<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h5>
            <label style="width:100%"> <span class="label  label-success"> 提示：点击任务节点可以查看节点信息</span> </label>
        </h5>
        <h1>
            <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)" style="font-size: 15px;"><i class="fa fa-lg fa-mail-reply-all"></i> 业务配置信息</a>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="keyId" />">
            <input type="hidden" name="id" id="id" value="">
        </h1>
    </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">
    <!-- row -->
	<div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0" data-widget-editbutton="false">
                <!-- widget header
                <header>
                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <h2>流程配置</h2>
                </header>-->
                <!-- widget div-->
                <div>
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <!-- widget content -->
                    <div class="widget-body">
                        <div class="col-sm-12" style="overflow:auto">
                            <img key="ajax-flow_export" src="<s:property value="filePath" />" >
                        </div>

                    </div>
                </div>
            </div>
        </article>
    </div>
</section>


</div>
		
<script type="text/javascript">

pageSetUp();

</script>
<script type="text/javascript">
//生成对应的div
$(function(){
    var top = $("img[key=ajax-flow_export]").position().top;
    var data = {keyId:$("input[name='keyId']").val()};
    var positionHtml = "";
    $.ajax({
        url : "../com/ajax-running!allActivity.action",
        method:"post",
        cache : false,
        dataType : "json",
        async : false,
        data : data,
        success : function(data) {
            $.each(data,function(i,v){
                var id= $("input#id").val(v.id);
                if(v.type != "endEvent" && v.type != "startEvent" && v.type !="exclusiveGateway"){
                    var $border = $('<div/>', {
                        'class': 'activity-attr-border'
                    }).css({
                        position: 'absolute',
                        left: (v.x - v.left),
                        top: (v.y - v.top+top-11),
                        width: (v.width + 10),
                        height: (v.height + 12)
                    }).attr("id",v.id);
                    $("img[key=ajax-flow_export]").after($border);
                }

            })
        }
    });
})
</script>
<script type="text/javascript">
    $(".activity-attr-border").unbind("click").bind("click",function(){
        var activityId = $(this).attr("id");
        var proDefId = $("input[name='keyId']").val();
        var actionUrl = "../com/ajax-config!processConfig.action?keyId="+proDefId+"&activityId="+activityId;
        gDialog.fCreate({
            title:'节点配置',
            url:actionUrl,
            width:800
        }).show();
    })
    $("a#btn-re-common").unbind("click").bind("click",function(){
        var id = $("input[name='keyId']").val();
        loadURL("../com/ajax-config!input.action?keyId="+id,$('#content'));
    });
</script>