<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<script src="../resource/com/js/plugin/dropzone/dropzone.js"></script>

<span class="editable-container editable-inline" style="">
    <div>
        <div class="editableform-loading" style="display: none;">
            <i class="ace-icon fa fa-spinner fa-spin fa-2x light-blue"></i>
        </div>
        <form class="form-inline editableform" style="" action="/file-upload" id="my-dropzone" class="dropzone">
            <div class="control-group form-group">
                <div>
                    <div class="editable-input editable-image">
                        <span>
                            <input type="hidden" name="avatar-hidden" value="">
                        </span>
                        <span>
                            <label class="ace-file-input ace-file-multiple" style="width: 150px;">
                                <input type="file" name="avatar">
                                <span class="ace-file-container" data-title="Change Avatar">
                                    <span class="ace-file-name" data-title="No File ...">
                                        <i class=" ace-icon fa fa-picture-o"></i>
                                    </span>
                                </span>
                                <a class="remove" href="#">
                                    <i class=" ace-icon fa fa-times"></i>
                                </a>
                            </label>
                        </span>
                    </div>
                    <div class="editable-buttons">
                        <button type="submit" class="btn btn-info editable-submit">
                            <i class="ace-icon fa fa-check"></i>
                        </button>
                        <button type="button" class="btn editable-cancel">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="editable-error-block help-block" style="display: none;">
                </div>
            </div>
        </form>
    </div>
</span>

<script>
$("#remove").unbind("click").bind("click",function(){
    $("#avatar").show();
    $("users_img").empty();
})

</script>