<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      人员履历
    </h1>
  </div>
</div>

<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <%--<form id="searchForm" method="post" action="">
          <s:if test="viewtype==1">
            <shiro:hasAnyRoles name="wechat">
              <a id="ajax_personnelresume_btn_add" <s:property value="isCreate('personnelresume')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建培训申请</a>
            </shiro:hasAnyRoles>
          </s:if>
          <s:if test="viewtype==2">
            <shiro:hasAnyRoles name="wechat">
              <a id="fn_personnelresume_update" <s:property value="isCreate('personnelresume')"/> class="btn btn-default pull-right pull-right-fix" data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 培训完成</a>
            </shiro:hasAnyRoles>
          </s:if>
          <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />

        </form>--%>
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_personnelresume_list_row">
                    <table id="ajax_personnelresume_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_personnelresume_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">

    $(function(){
        load_personnelresume_jqGrid();
    });

  function load_personnelresume_jqGrid(){
    jQuery("#ajax_personnelresume_table").jqGrid({
      url:'ajax-personnelresume!list.action?parentId=<s:property value="parentId"/>',
      datatype: "json",
      colNames:['异动类型',"异动时间","id"],
      colModel:[
        {name:'type',index:'type_name', width:200,sortable:false,search:true},
        {name:'time',index:'time',sortable:false,search:true,width:80},
        {name:'id',index:'id',search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_personnelresume_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){
        var ids=$("#ajax_personnelresume_table").jqGrid('getDataIDs');
        for(var i=0;i<ids.length;i++){
          var cl=ids[i];
          var rowData = $("#ajax_personnelresume_table").jqGrid("getRowData",cl);
          var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_personnelresume_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" ";
          jQuery("#ajax_personnelresume_table").jqGrid('setRowData',ids[i],{act:de});
        }
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_personnelresume_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_personnelresume_table").jqGrid('getRowData', rowId);
        $('#chooseId').val(rowId);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
      caption : "<i class='fa fa-arrow-circle-right'></i> 人员履历",
      multiselect : true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      viewrecords: true,
      autowidth: true,
      height:'auto',
      forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_personnelresume_table").jqGrid('setGridWidth', $("#ajax_personnelresume_list_row").width());
    })
    jQuery("#ajax_personnelresume_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

    jQuery("#ajax_personnelresume_table").jqGrid('navGrid', "#ajax_personnelresume_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };
  

</script>





















