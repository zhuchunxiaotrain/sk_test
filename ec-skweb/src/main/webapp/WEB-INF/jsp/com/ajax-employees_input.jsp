<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid=session.getId();
%>
<jsp:include page="ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">

          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all">员工基本情况</i></a>

            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          <s:if test="employees!=null && employees.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
<%--
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-download"></i> 保存</a>
--%>
          <hr class="simple">
          <form id="employees" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="employees.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              项目基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input disabled id="name" name="name" type="text" value="<s:property value="employees.name"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  性别
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" disabled name="gender" id="gender"  value="<s:property value="gender"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  年假
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="annuaLeaveNum" id="annuaLeaveNum" placeholder="请输入年假" value="<s:property value="leaveDetail.annuaLeave"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  籍贯
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="nativePlace" id="nativePlace" placeholder="请输入籍贯" value="<s:property value="employees.nativePlace"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  民族
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="nation" id="nation" placeholder="请输入民族" value="<s:property value="employees.nation"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  用人性质
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" disabled name="natureValue" id="natureValue"  value="<s:property value="natureValue"/>" >
                    <input type="hidden" name="nature" id="nature"  value="<s:property value="employees.nature"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同开始日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input id="startDate" name="startDate" placeholder="请选择合同开始日期"
                           type="text" value="<s:date name="employees.startDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>

                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同结束日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input id="endDate" name="endDate" placeholder="请选择合同结束日期"
                           type="text" value="<s:date name="employees.endDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  试用开始日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="trialStart" name="trialStart" placeholder="请选择试用期开始日期"
                            type="text" value="<s:date name="employees.trialStart" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  试用结束日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="trialEnd" name="trialEnd" placeholder="请选择试用期开始日期"
                            type="text" value="<s:date name="employees.trialEnd" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  所属部门
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input type="text" disabled name="depName" id="depName"  value="<s:property value="employees.dep.name"/>" >
                    <input type="hidden"  name="depId" id="depId" value="<s:property value="employees.dep.id"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  岗位
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input type="text" disabled name="postName" id="postName"  value="<s:property value="employees.empPost.name"/>" >
                    <input type="hidden"  name="postId" id="postId" value="<s:property value="employees.empPost.id"/>" >

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  基本薪资
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="money" id="money" placeholder="请输入基本薪资(元)" value="<s:property value="employees.money"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  出生日期
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  id="birthDate" name="birthDate" placeholder="请选择出生日期"
                            type="text" value="<s:date name="employees.birthDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  户口所在地
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="houseLocation">
                    <label class="radio">
                      <input type="radio"  checked="checked" name="houseLocation" value="0" <s:property value="employees.houseLocation==0?'checked':''"/>>
                      <i></i>上海户口</label>
                    <label class="radio">
                      <input type="radio"  name="houseLocation" value="1" <s:property value="employees.houseLocation==1?'checked':''"/>>
                      <i></i>非上海户口</label>
                  </div>
                </section>
              </div>
              <div class="row" <s:if test="employees.houseLocation==1||employees==null">style="display:none"</s:if> id="showValid">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  居住证登记
                </label>
                <section class="col col-10">
                  <div class="inline-group" name="cardRegister">
                    <label class="radio">
                      <input type="radio" checked="checked" name="cardRegister" value="0" <s:property value="employees.cardRegister==0?'checked':''"/>>
                      <i></i>有</label>
                    <label class="radio">
                      <input type="radio"  name="cardRegister" value="1" <s:property value="employees.cardRegister==1?'checked':''"/>>
                      <i></i>无</label>
                  </div>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  居住证开始日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input   id="cardStart" name="cardStart" placeholder="请选择居住证开始日期"
                             type="text" value="<s:date name="employees.cardStart" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  居住证结束日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="cardEnd" name="cardEnd" placeholder="请选择居住证结束日期"
                            type="text" value="<s:date name="employees.cardEnd" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  身份证号
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input type="text" disabled name="cardID" id="cardID" value="<s:property value="employees.cardID"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  婚姻情况
                </label>
                <section class="col col-4">
                  <div class="inline-group" name="maritalStatus">
                    <label class="radio">
                      <input type="radio" checked="checked" name="maritalStatus" value="0" <s:property value="employees.maritalStatus==0?'checked':''"/>>
                      <i></i>已婚</label>
                    <label class="radio">
                      <input type="radio"  name="maritalStatus" value="1" <s:property value="employees.maritalStatus==1?'checked':''"/>>
                      <i></i>未婚</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  移动电话
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input type="text" name="mobile" id="mobile" placeholder="请输入移动电话" value="<s:property value="employees.mobile"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  电子邮件
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input type="text" name="email" id="email" placeholder="请输入电子邮件" value="<s:property value="employees.email"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  最高学历
                </label>
                <section class="col col-4">
                  <label class="input">
                    <select class="form-control" name="highestDegreeDictId" id="highestDegreeDictId">
                      <option value="">请选择</option>
                      <s:iterator value="eduArrayList" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  毕业院校
                </label>
                <section class="col col-4">
                  <label class="input ">
                    <input  type="text" name="school" id="school" placeholder="请输入毕业院校" value="<s:property value="employees.school"/>" />
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  所学专业
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input type="text" disabled id="profession" name="profession" placeholder="请输入所学专业" value="<s:property value="employees.profession"/>">

                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  参见工作时间
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="workTime" name="workTime" type="text" placeholder="请选择参加工作时间" value="<s:date name="employees.workTime" format="yyyy-MM"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  户籍地址
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" id="permanentAddress" name="permanentAddress" placeholder="请输入户籍地址" value="<s:property value="employees.permanentAddress"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  联系电话
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input type="text" placeholder="请输入联系电话" name="tel" id="tel"  value="<s:property value="employees.tel"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  邮政编码
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input type="text" id="postcode" name="postcode" placeholder="请输入邮政编码"  value="<s:property value="employees.postcode"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  居住地址
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text"  id="address" name="address" placeholder="请输入居住地址" value="<s:property value="employees.address"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  紧急联络人
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  type="text"  id="contactLinkman" name="contactLinkman" placeholder="请输入紧急联络人" value="<s:property value="employees.contactLinkman"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  紧急联络电话
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  type="text" id="contactTel" name="contactTel" placeholder="请输入紧急联络电话" value="<s:property value="employees.contactTel"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  邮政编码
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  type="text"  id="postcode1" name="postcode1" placeholder="请输入邮政编码" value="<s:property value="employees.postcode1"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  身份证复印件
                </label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input name="section.talktext" id="talktext" placeholder="请输入会前2次沟通记录"
                           type="text" value="<s:property value="section.talktext"/>">
                  </label>--%>
                  <label class="input">
                    <input  id="copyfilename" placeholder="" type="file" >
                    <input name="copyfileId" id="copyfileId" style="display: none" value="<s:property value="copyfileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="employees.remark"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-1">
                  称谓
                </label>
                <label class="label col col-2">
                  姓名
                </label>
                <label class="label col col-1">
                  年龄
                </label>
                <label class="label col col-1">
                  政治面貌
                </label>
                <label class="label col col-2">
                  工作单位
                </label>
                <label class="label col col-1">
                  职务
                </label>
                <label class="label col col-2">
                  联系电话
                </label>
                <section class="col col-1">
                  <label class="label col col-1" >
                    <a id="btn-add-row" href="javascript:void(0)"><i class="fa fa-rocket"></i>增加行</a>
                  </label>
                </section>

              </div>

                <div id="add-employees-row" >
                  <s:iterator value="familySet" id="list">
                  <div class="row">
                    <section class="col col-1">
                      <label class="input">
                        <input type="text" name="appellation" id="appellation" placeholder="请输入称谓" value="<s:property value="#list.appellation"/>" >
                      </label>
                    </section>
                    <section class="col col-2">
                      <label class="input">
                        <input type="text" name="name1" id="name1" placeholder="请输入姓名" value="<s:property value="#list.name"/>" >
                      </label>
                    </section>
                    <section class="col col-1">
                      <label class="input">
                        <input type="text" name="age1" id="age1" placeholder="请输入年龄" value="<s:property value="#list.age"/>" >
                      </label>
                    </section>
                    <section class="col col-1">
                      <label class="input">
                        <input type="text" name="politicalStatus" id="politicalStatus" placeholder="请输入政治面貌" value="<s:property value="#list.politicalStatus"/>" >
                      </label>
                    </section>
                    <section class="col col-2">
                      <label class="input">
                        <input type="text" name="workAdd" id="workAdd" placeholder="请输入" value="<s:property value="#list.workAdd"/>" >
                      </label>
                    </section>
                    <section class="col col-2">
                      <label class="input">
                        <input type="text" name="post1" id="post1" placeholder="请输入职务" value="<s:property value="#list.post"/>" >
                      </label>
                    </section>
                    <section class="col col-2">
                      <label class="input">
                        <input type="text" name="tel1" id="tel1" placeholder="请输入联系电话" value="<s:property value="#list.tel"/>" >
                      </label>
                    </section>
                    <section class="col col-1"> <label class="input">
                      <a  href="javascript:void(0);" class="del"><i class="fa fa-rocket"></i>删除行</a> </label>
                    </section>

                  </div>
                  </s:iterator>

                </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">
/*
       $('li.disabled').off("click").on("click", function () {
           return false;
       })

*/
    //上传
    inputLoad({
        objId:"copyfilename",
        entityName:"copyIDCardIds",
        sourceId:"copyfileId",
        jsessionid:"<%=jsessionid%>"
    });

    $(function(){
        var pdata= {
            keyId:$("#keyId").val(),
            flowName:"employees",
            todo:"1"
        };
        multiDuty(pdata);
    });
    //校验
    $("#employees").validate({
        rules : {
            startDate : {
                required : true
            },
            endDate: {
                required : true
            },
            trialStart:{
                required : true
            },
            trialEnd:{
                required : true
            },
            money:{
                required : true,
                number:true,
                min:0

            },
            nativePlace:{
                required : true
            },

            nation:{
                required : true
            },
            birthDate:{
                required:true
            },
            school:{
                required : true
            },
            email:{
                required:true,
                email:true
            },
            permanentAddress:{
                required:true
            },
            workTime:{
                required : true
            },
            profession:{
                required : true
            },
            postcode:{
                required : true,
                number:true,
                digits:true
            },
            postcode1:{
                number:true,
                digits:true
            },
            contactLinkman:{
                required : true
            },
            address:{
                required : true
            },
            copyIDCardIds:{
                required : true
            }
        },

        messages : {
            startDate : {
                required :"请选择合同开始日期"
            },
            endDate: {
                required : "请选择合同结束日期"
            },
            trialStart:{
                required : "请选择试用开始日期"
            },
            trialEnd:{
                required : "请选择试用结束提前"
            },
            money:{
                required : "请输入基本薪资",
                number:"请输入数字",
                min: "不能小于0"

            },
            email:{
                required:"请输入邮箱地址",
                email:"请输入正确的邮箱"
            },
            nativePlace:{
                required : "请输入籍贯"
            },

            nation:{
                required : "请输入民族"
            },
            birthDate:{
                required:"请选择出生日期"
            },
            school:{
                required : "请输入毕业院校"
            },
            email:{
                email:"请输入正确的电子邮件"
            },
            permanentAddress:{
                required:"请输入户籍地址"
            },
            workTime:{
                required : "请选择参见工作时间"
            },
            profession:{
                required : "请输入所学专业"
            },
            postcode:{
                required : "请输入邮政编码",
                number:"请输入合法数字",
                digits:"只能输入整数"
            },
            postcode1:{
                number:"请输入合法数字",
                digits:"只能输入整数"
            },
            contactLinkman:{
                required : "请输入紧急联络人"
            },
            address:{
                required : "请输入居住地址"
            },
            copyIDCardIds:{
                required : "请输入身份证复印件"
            }

        },
        ignore: "",
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }

    });


//增加行
    $("#btn-add-row").bind("click",function () {
        var str='<div class="row"> <section class="col col-1"> <label class="input"> <input type="text" name="appellation"  placeholder="请输入称谓"  > </label> </section> <section class="col col-2"> <label class="input"> <input type="text" name="name1"  placeholder="请输入姓名" > </label> </section> <section class="col col-1"> <label class="input"> <input type="text" name="age1"  placeholder="请输入年龄" > </label> </section> <section class="col col-1"> <label class="input"> <input type="text" name="politicalStatus"  placeholder="请输入政治面貌" > </label> </section> <section class="col col-2"> <label class="input"> <input type="text" name="workAdd"  placeholder="请输入" > </label> </section> <section class="col col-2"> <label class="input"> <input type="text" name="post1"  > </label> </section> <section class="col col-2"> <label class="input"> <input type="text" name="tel1"  placeholder="请输入联系电话"> </label> </section><section class="col col-1"> <label class="input"> <a  href="javascript:void(0);" class="del"><i class="fa fa-rocket"></i>删除行</a> </label></section></div>';
        $("#add-employees-row").append(str);
    });

    $(document).on("click", '.del', function(){

        var div$=$("a.del");
        //div$.attr("style","border: 1px solid red")
        //var acceptMoneys$= $("input[name='acceptMoneys']");
        //if(acceptMoneys$.length==1){
            //alert("至少有一个收款人")
      //  }else{
          /*$(this).parents('div[data-row="1"]').remove();*/
            $(this).closest("div").remove();
        //}
    });

    $(':radio[name="houseLocation"]').click(function(){
        if($(this).val()=="0") {
            $('#showValid').show();
        }else{
            $('#showValid').hide();
        }
    });

    $(function () {

        $('#startDate').datetimepicker({
            format: 'yyyy-mm-dd',
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked',
            language: 'zh-CN',
            minView:2
        }).on('changeDate',function () {
            var startDate=$('#startDate').val();
            $('#endDate').datetimepicker('setStartDate',startDate);

        });

        $('#trialStart').datetimepicker({
            format: 'yyyy-mm-dd',
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked',
            language: 'zh-CN',
            minView:2
        }).on('changeDate',function () {
            var trialStart=$('#trialStart').val();
            $('#trialEnd').datetimepicker('setStartDate',trialStart);

        });

        $('#cardStart').datetimepicker({
            format: 'yyyy-mm-dd',
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked',
            language: 'zh-CN',
            minView:2
        }).on('changeDate',function () {
            var cardStart=$('#cardStart').val();
            $('#cardEnd').datetimepicker('setStartDate',cardStart);

        });

        $('#endDate,#trialEnd,#birthDate,#cardEnd').datetimepicker({
            format: 'yyyy-mm-dd',
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked',
            language: 'zh-CN',
            minView:2
        });

        $('#workTime').datetimepicker({
            format: 'yyyy-mm',
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked',
            language: 'zh-CN',
            startView: 3,
            forceParse: false,
            minView: 3
        });

    });

    //保存
    $("#btn-save-common").click(

        function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("employees","ajax-employees!save.action",null,function (data) {
                if(data.state == "200" || data.state ==200) {
                    if(draft == 1){
                        location.href='index.action';
                    }else{
                        loadURL("ajax-employees.action",$('#content'));
                    }
                }
            });

        }

    );

    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#employees").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#employees").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("employees","ajax-employees!commit.action",null,function (data) {
                        if(data.state == "200" || data.state ==200) {
                            if(draft == 1){
                                location.href='index.action';
                            }else{
                                loadURL("ajax-employees.action",$('#content'));
                            }
                        }
                    });
                }
            });
        }
    );

    //返回视图
    $("#btn-re-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-employees.action",$('#content'));
        }

    });
</script>