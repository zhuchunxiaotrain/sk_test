<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_role_list_row">
      <table id="ajax_role_list_table">
      </table>
      <div id="ajax_role_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>
  var isModal = "<s:property value="isModal" />";
  function run_jqgrid_function(){
    jQuery("#ajax_role_list_table").jqGrid({
      url:'ajax-role!list.action',
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['群组名称','Id'],
      colModel : [
        {name:'name',index:'name', width:240,sortable:false,search:true,sorttype:'string'},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_role_list_page',
      sortname : 'createDate',
      sortorder : "desc",
      gridComplete:function(){
          var roleId=$("#roleId").val();
            var ids=$("#ajax_role_list_table").jqGrid('getDataIDs');
            for(var i=0;i<ids.length;i++) {
                var cl = ids[i];
                if (roleId != "" && roleId != null) {
                    var idsArr = roleId.split(",");
                    for (var s = 0; s < idsArr.length; s++) {
                        if (idsArr[s] == cl) {
                            $("#ajax_role_list_table").jqGrid("setSelection", cl);
                        }
                    }
                }
            }
        jqGridStyle();
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      },
        onSelectRow: function (rowId, status, e) {
            $(this).gridselect('onecheck',{
                table:"ajax_role_list_table",
                rowId:rowId,
                status:status,
                id:"roleId",
                name:"roleName"
            });
        },
        onSelectAll:function(aRowids, status, e){
            $(this).gridselect('morecheck',{
                table:"ajax_role_list_table",
                rowId:aRowids,
                status:status,
                id:"roleId",
                name:"roleName",
            });
        },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_role_list_table").jqGrid('setGridWidth', $("#ajax_role_list_row").width()-10);
    })

    jQuery("#ajax_role_list_table").jqGrid('navGrid', "#ajax_role_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_role_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
       gDialog.fClose();
  });
</script>
