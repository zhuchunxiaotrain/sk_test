<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<div class="modal-body">

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-8 ">
			<form id="save_imgCrop">
				<input type="text" name="keyId"  value="<s:property value="keyId"/>">
				<input type="hidden" name="x" id="x" value="">
				<input type="hidden" name="y" id="y" value="">
				<input type="hidden" name="w" id="w" value="">
				<input type="hidden" name="h" id="h" value="">
				<img src="<s:property value="imgUrl"></s:property>" id="imgCrop" />
			</form>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" id="dialog-ok">确定</button>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>
	$('#imgCrop').Jcrop({
		aspectRatio:1,
		bgFade:true,
		onChange:showCords,
		onSelect:showCords,
		keySupport:true,
		bgFade:true,
		minSize :[100,100]
	});
	function showCords(obj) {
		$("#x").val(obj.x);
		$("#y").val(obj.y);
		$("#w").val(obj.w);
		$("#h").val(obj.h);
	}
	$("#dialog-ok").unbind("click").bind("click",function(){
		form_save("save_imgCrop","<%=path%>/com/ajax-users!cropSave.action");
		gDialog.fClose();
	});
</script>