<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input  name="parentId" id="parentId"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_project_list_row">
      <table id="ajax_emp_list_table">
      </table>
      <div id="ajax_project_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script >
  function run_jqgrid_function(){
    jQuery("#ajax_emp_list_table").jqGrid({
      url:'ajax-interview!listfordialog.action?parentId='+$("#parentId").val(),
      mtype:"POST",
      datatype: 'json',
      page : 1,
        colNames:["面试人","身份证号","id"],
        colModel:[
            {name:'name',index:'name', search:false,width:100},
            {name:'cardID',index:'cardID', search:false,width:200},
            {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
        ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_project_list_page',
      sortname : 'createDate',
      sortorder : "desc",
      gridComplete:function(){
        var param=$("#param").val();
        jqGridStyle();
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      },
        onSelectRow: function (rowId, status, e) {
            var slt =   $("#ajax_emp_list_table").jqGrid('getGridParam','selarrrow');
            if(slt.length > 1){
                alert("请选择一条数据");
                return false;
            }

        },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
          jqGridStyle();
          $(".ui-jqgrid-bdiv").css("overflow-x","hidden");

      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_emp_list_table").jqGrid('setGridWidth', $("#ajax_project_list_row").width()-10);
    })

    jQuery("#ajax_emp_list_table").jqGrid('navGrid', "#ajax_project_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_emp_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
      var rowId = $("#ajax_emp_list_table").jqGrid('getGridParam','selrow');
      var rowDatas = $("#ajax_emp_list_table").jqGrid('getRowData', rowId);
      $("#name1").val(rowDatas['name']);
      $("#cardID1").val(rowDatas['cardID']);
      $("#id1").val(rowId);
    gDialog.fClose();
  });
</script>
