<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this tsummarylate use File | Settings | File Tsummarylates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common_summary" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i> 培训总结</a>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="row">
                <shiro:hasAnyRoles name="wechat">
                  <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(summary.id)"/> key="ajax_summary_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
                </shiro:hasAnyRoles>
              </div>
          <hr class="simple">
          <form id="summary" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="summaryKeyId" value="<s:property value="summary.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input  type="hidden" name="numStatus" id="summary_numStatus" value="<s:property value="numStatus" />"/>
            <input type="hidden" name="parentId" id="parentId"  value="<s:property value="parentId" />"/>
            <header  style="display: block;">
              培训总结&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  所属培训
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="hidden"  name="typeDictId" id="typeDictId"  value="<s:property value="summary.type.id"/>" >
                    <input type="text"  name="typeDictName" id="typeDictName"  value="<s:property value="summary.type.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  课程名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input disabled id="name" name="name" type="text" value="<s:property value="summary.name"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  课程讲师
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text"  value="<s:iterator id="list" value="summary.teacher"><s:property value="#list.name"/>,</s:iterator>"/>
                  </label>
                  <label class="input state-disabled">
                    <input   type="hidden" id="teacherId" name="teacherId" value="<s:iterator id="list" value="summary.teacher"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  培训评估
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input name="assessmentfileId" id="assessmentfileId" style="display: none" value="<s:property value="assessmentfileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  提交人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text" id="creater" placeholder="" value="<s:property value="summary.creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  提交日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input disabled type="text" placeholder="操作时间" value="<s:date name="summary.createDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
            </fieldset>

          </form>
            <div class="flow">
              <s:if test="summary.getProcessState().name()=='Running'">
                <div class="f_title">审批意见</div>
                <div class="chat-footer"style="margin-top:5px">
                  <div class="textarea-div">
                    <div class="typearea">
                      <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                    </div>
                  </div>
                  <span class="textarea-controls"></span>
                </div>
              </s:if>
              <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
              <div class="f_content" style="display:none">
                <div id="summary_showFlow"></div>
              </div>
              <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
              <div class="f_content" style="display:none">
                <div id="summary_showNext"></div>
              </div>
            </div>
            </div>
          </div>
        </div>
  </div>
  </article>
</div>

<script type="text/javascript">
/*
       $('li.disabled').off("click").on("click", function () {
           return false;
       })
*/
/*
    var table_global_width=0;
    $("a#other1").off("click").on("click",function(e) {
        if (table_global_width == 0) {
            table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
        }
        loadURL("ajax-summary.action?parentId="+$("#keyId").val(),$('div#s2'));
    });
    */
    //上传
    inputLoad({
        objId:"assessmentfileId",
        entityName:"assessmentfileIds",
        sourceId:"assessmentfileId"
    });
    
    //审批
    $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#summaryKeyId").val()+"&type=flow",$('#summary_showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#summaryKeyId").val()+"&type=next",$('#summary_showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#summaryKeyId").val()},null,function(pdata){
        var showDuty = false;
        var area = $("span.textarea-controls");
        $(pdata.data.datarows).each(function(i,v){
            var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
            $(area).append(str);
            if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                showDuty = true;
            }
        });
        if(showDuty == true){
            var pdata = {
                keyId: $("input#summaryKeyId").val(),
                flowName: "summary"
            };
            multiDuty(pdata);
        }
    });
    var valueObj=$("input#summary_numStatus").val();
    switch(valueObj){
        case "0":
            //保存后再提交
            $("#left_foot_btn_approve").off("click").on("click",function(){
                form_save("summary","ajax-summary!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                    $("#btn-re-common_summary").trigger("click");
                })
            });
            break;
        case "1":
            //第一步审批
            $("#left_foot_btn_approve").off("click").on("click",function(){
                form_save("summary","ajax-summary!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                    $("#btn-re-common_summary").trigger("click");
                });

            });
            break;
    }

    //流程信息展开
    $('#flow,#next').click(function(){
        if($(this).hasClass("right")){
            $(this).removeClass("right").addClass("down");
            $(this).parent(".f_title").next("div.f_content").show();
        }else{
            $(this).removeClass("down").addClass("right");
            $(this).parent(".f_title").next("div.f_content").hide();
        }
    });
   });

    //返回视图
    $("#btn-re-common_summary").click(function(){
        var index = "<s:property value="index" />";
        var todo = "<s:property value="todo" />";
        var remind = "<s:property value="remind" />";
        var record = "<s:property value="record" />";
        var draft = "<s:property value="draft" />";
        if(index==1){
            loadURL("ajax-index!page.action",$('#content'));
        }else if(todo==1){
            loadURL("ajax!toDoList.action",$('#content'));
        }else if(remind==1){
            loadURL("ajax!remindList.action",$('#content'));
        }else if(record==1){
            loadURL("ajax!taskRecordList.action?type=1",$('#content'));
        }else if(record==2){
            loadURL("ajax!taskRecordList.action?type=2",$('#content'));
        }else if(draft==1){
            loadURL("ajax!draftList.action",$('#content'));
        }else{
            loadURL("ajax-summary.action?parentId="+$("#parentId").val(),$('div#s2'));
        }

    });

    //编辑
    $("a[key=ajax_summary_edit]").click(function(){
        var draft = "<s:property value="draft" />";
        loadURL("ajax-summary!input.action?keyId="+$("input#summaryKeyId").val()+"&draft="+draft+"&parentId="+$("#parentId").val(),$('#content'));
    });


</script>