<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>
<jsp:include page="ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i> 请假申请</a>

          <div id="myTabContent1" class="tab-content padding-10 ">
              <div class="row">
              <shiro:hasAnyRoles name="wechat">
                <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(leave.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
              </shiro:hasAnyRoles>
              </div>
          <hr class="simple">
          <form id="leave" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="numStatus" id="numStatus" value="<s:property value="numStatus" />"/>
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="leave.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="viewtype" id="viewtype"  value="<s:property value="viewtype" />"/>

            <header  style="display: block;">
              请假申请&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  请假类型
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="result">
                    <label class="radio">
                      <input type="radio" disabled checked="checked" name="type" value="0" <s:property value="leave.type==0?'checked':''"/>>
                      <i></i>年假</label>
                    <label class="radio">
                      <input type="radio" disabled name="type" value="1" <s:property value="leave.type==1?'checked':''"/>>
                      <i></i>病假</label>
                    <label class="radio">
                      <input type="radio" disabled name="type" value="2" <s:property value="leave.type==2?'checked':''"/>>
                      <i></i>事假</label>
                    <label class="radio">
                      <input type="radio" disabled name="type" value="3" <s:property value="leave.type==3?'checked':''"/>>
                      <i></i>其他</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  职员姓名
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="name" disabled id="name" value="<s:property value="leave.creater.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  所属年份
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  placeholder="请选择所属年份" disabled id="time" name="time" type="text" value="<s:property value="leave.time"/>">
                  </label>
                </section>

              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  请假天数
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled name="dayNum" id="dayNum" placeholder="请输入请假天数" value="<s:property value="leave.dayNum"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  工作交接安排
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
<%--
                    <input name="uploadify" id="filename" style="display: none" placeholder="" type="file" >
--%>
                    <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  今年年假
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled name="annuaLeave" id="annuaLeave" value="<s:property value="leaveDetail.annuaLeave"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  今年年假(已用)
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled name="usedAnnuaLeave" id="usedAnnuaLeave" value="<s:property value="leaveDetail.usedAnnuaLeave"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  今年年假(可用)
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled name="unusedAnnuaLeave" id="unusedAnnuaLeave" value="<s:property value="leaveDetail.unusedAnnuaLeave"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  已申请病假
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled name="sickLeave" id="sickLeave" value="<s:property value="leaveDetail.sickLeave"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  已申请事假
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled name="thingLeave" id="thingLeave" value="<s:property value="leaveDetail.thingLeave"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                已申请其他假期
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input type="text" disabled name="othersLeave" id="othersLeave" value="<s:property value="leaveDetail.othersLeave"/>" >
                </label>
              </section>
            </div>
            <div class="row">
              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                当年已申请天数
              </label>
              <section class="col col-5">
                <label class="input state-disabled">
                  <input type="text" disabled name="usedDayNum" id="usedDayNum" value="<s:property value="leaveDetail.usedDayNum"/>" >
                </label>
              </section>
            </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="usersName" name="nextStepApproversName"
                             value="<s:property value="leave.nextStepApproversName"/>"/>
                      <input type="hidden" id="usersId" name="nextStepApproversId"
                             value="<s:property value="leave.nextStepApproversId"/>"/>
                    </label>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  申请人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text"  placeholder="" value="<s:property value="leave.creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  操作日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input  disabled type="text"  value="<s:date name="leave.createDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

            </fieldset>

          </form>
          <div class="flow">
            <s:if test="leave.getProcessState().name()=='Running'">
            <div class="f_title">审批意见</div>
            <div class="chat-footer"style="margin-top:5px">
              <div class="textarea-div">
                <div class="typearea">
                  <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                </div>
              </div>
              <span class="textarea-controls"></span>
            </div>
            </s:if>
            <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="showFlow"></div>
            </div>
            <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="showNext"></div>
            </div>
          </div>
        </div>
        </div>
      </div>
   </div>

  </article>
</div>

<script>


  $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
      var showDuty = false;
      var area = $("span.textarea-controls");
      $(pdata.data.datarows).each(function(i,v){
        var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
        $(area).append(str);
        if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
          showDuty = true;
        }
      });
      if(showDuty == true){
        var pdata = {
          keyId: $("input#keyId").val(),
          flowName: "leave"
        };
        multiDuty(pdata);
      }
    });
    var valueObj=$("input#numStatus").val();
    switch(valueObj){
      case "0":
        //保存后再提交
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("leave","ajax-leave!commit.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "1":
        //第一步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("leave","ajax-leave!approve1.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          });
        });
        break;
      case "2":
        //第二步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("leave","ajax-leave!approve2.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
      case "3":
        //第三步审批
        $("#left_foot_btn_approve").off("click").on("click",function(){
          form_save("leave","ajax-leave!approve3.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val()),null,function(){
            $("#btn-re-common").trigger("click");
          })
        });
        break;
    }

    //退回
    $("#left_foot_btn_sendback").off("click").on("click",function(){
      var vActionUrl="ajax-leave!reject.action?comment="+encodeURIComponent($("textarea#chat_textarea-expand").val())+"&curDutyId="+$('#curDutyId').val();
      var data={keyId:$("input#keyId").val()};
      ajax_action(vActionUrl,data,{},function(pdata){
        _show(pdata);
        $("#btn-re-common").trigger("click");
      });
    });

    //流程信息展开
    $('#flow,#next').click(function(){
      if($(this).hasClass("right")){
        $(this).removeClass("right").addClass("down");
        $(this).parent(".f_title").next("div.f_content").show();
      }else{
        $(this).removeClass("down").addClass("right");
        $(this).parent(".f_title").next("div.f_content").hide();
      }
    });
  });
  //上传文件
  readLoad({
    objId:"fileId",
    entityName:"fileIds",
    sourceId:"fileId"
  });
  //返回视图
  $("#btn-re-common").click(function(){
    var index = "<s:property value="index" />";
    var todo = "<s:property value="todo" />";
    var remind = "<s:property value="remind" />";
    var record = "<s:property value="record" />";
    var draft = "<s:property value="draft" />";
    if(index==1){
      loadURL("ajax-index!page.action",$('#content'));
    }else if(todo==1){
      loadURL("ajax!toDoList.action",$('#content'));
    }else if(remind==1){
      loadURL("ajax!remindList.action",$('#content'));
    }else if(record==1){
      loadURL("ajax!taskRecordList.action?type=1",$('#content'));
    }else if(record==2){
      loadURL("ajax!taskRecordList.action?type=2",$('#content'));
    }else if(draft==1){
      loadURL("ajax!draftList.action",$('#content'));
    }else{
      loadURL("ajax-leave.action?viewtype=<s:property value="viewtype"/>",$('#content'));
    }

  });

  //编辑
  $("a[key=ajax_edit]").click(function(){
    var draft = "<s:property value="draft" />";
    loadURL("ajax-leave!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
  });

</script>
