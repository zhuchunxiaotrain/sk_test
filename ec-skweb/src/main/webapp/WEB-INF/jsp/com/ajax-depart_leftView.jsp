<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<input type="hidden" name="keyId" id="keyId" value="<s:property value="keyId" />">
<fieldset>
    <ul class="list-group" id="ajax_depart_query">
        <s:iterator value="dutyArrayList" id="list">
           <li id="<s:property value="#list.id"/>" class="list-group-item">
                <div class="checkbox">
                    <label>
                      <span>
                          <input type="checkbox"  id="<s:property value="#list.id"/>" <s:property value="#list.checked"/> name="ck_depart_info"/>
                          <s:property value="#list.user" /><s:if test="#list.post!=''">(<s:property value="#list.post" />)</s:if>
                      </span>
                    </label>
                    <a title="删除" key="ajax_dutyAgain_actions_delete" id="<s:property value="#list.id" />"class="btn bg-color-red txt-color-white btn-xs pull-right"
                        href="javascript:void(0);"><i class="fa fa-times"></i>删除</a>
                </div>
            </li>
        </s:iterator>

    </ul>
</fieldset>
<script>
	//loadDataTableScripts();
    $("a[key=ajax_dutyAgain_actions_delete]").click(function(){
        var vActionUrl = "<%=path%>/com/ajax-depart!deleteUser.action";
		data={keyId:$(this).attr("id")};
		ajax_action(vActionUrl,data,{},function(pdata){
            _show(pdata);
        });
		var id=$("input#keyId").val();
		loadURL("ajax-depart!leftView.action?keyId="+id,$('#ajax_depart_query'));
		jQuery("#ajax_depart_table").trigger("reloadGrid");
    });

	$("input[name='ck_depart_info']").unbind("click").bind("click",function(){
	    if($(this).prop("checked")){
            $("input[name='ck_depart_info']:checked").prop("checked",false);
            $(this).prop('checked',true);
        }
	    var rowId=$(this).attr("id");
        var actionUrl = "<%=path%>/com/ajax-depart!lead.action";
        var data={rowIds:rowId,keyId:$("input#keyId").val()};
        ajax_action(actionUrl,data,{},function(pdata){
            _show(pdata);
        });
        jQuery("#ajax_depart_table").trigger("reloadGrid");
	});
</script>
