<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />

<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      请假管理
    </h1>
  </div>
</div>
<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">

          <s:if test="viewtype==1">
              <a id="ajax_leave_btn_add" class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新建请假申请表</a>

          </s:if>
        </form>
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_leave_list_row">
                    <table id="ajax_leave_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_leave_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_leave_jqGrid();
  });
  function load_leave_jqGrid(){
      jQuery("#ajax_leave_table").jqGrid({ 
          url:'ajax-leave!list.action?viewtype='+"<s:property value="viewtype" />", 
          datatype: "json", 
          colNames:["请假类型","职员姓名","请假天数","请假日期","文档状态","操作","id"], 
          colModel:[ 
              {name:'type',index:'type', search:false,width:80}, 
              {name:'creater',index:'creater', search:false,width:80}, 
              {name:'dayNum',index:'dayNum', search:false,width:80}, 
              {name:'createDate',index:'createDate', search:false,width:80},
              {name:'state',index:'state', search:false,width:80},
              {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true},
              {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
          ], 
          rowNum : 10, 
          rowList:[10,20,30], 
          pager : '#ajax_leave_list_page', 
          sortname : '', 
          sortorder : "", 
          gridComplete:function(){ 
              var ids=$("#ajax_leave_table").jqGrid('getDataIDs'); 
              for(var i=0;i<ids.length;i++){ 
                   cl=ids[i]; 
                  var rowData = $("#ajax_leave_table").jqGrid("getRowData",cl); 
                  var de="<button class='btn btn-default' data-original-title='查看' onclick=\"fn_leave_read('"+cl+"');\"><i class='fa fa-eye'></i>查看</button>"+" "; 

                  jQuery("#ajax_leave_table").jqGrid('setRowData',ids[i],{act:de}); 
              } 

              $(".ui-jqgrid-bdiv").css("overflow-x","hidden"); 
              jqGridStyle(); 
          }, 
          jsonReader: { 
              root: "dataRows", 
              page: "page", 
              total: "total", 
              records: "records", 
              repeatitems : false 
          }, 
          caption : "<i class='fa fa-arrow-circle-right'></i> 信息", 
          multiselect : false, 
          rownumbers:true, 
          gridview:true, 
          shrinkToFit:true, 
          viewrecords: true, 
          autowidth: true, 
          height:'auto', 
          forceFit:true, 
          loadComplete: function() {     } });

      $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_leave_table").jqGrid('setGridWidth', $("#ajax_leave_list_row").width());
       })
       jQuery("#ajax_leave_table").jqGrid('filterToolbar',{
        searchOperators:false,
        stringResult:true});

    jQuery("#ajax_leave_table").jqGrid('navGrid', "#ajax_leave_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };

  function fn_leave_read(id){
      loadURL("ajax-leave!read.action?keyId="+id,$('#content'));
  }

  //新建
  $("#ajax_leave_btn_add").off("click").on("click",function(){
      loadURL("ajax-leave!input.action",$('#content'));
  });

</script>





















