<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i> 培训申请</a>
          <ul id="myTab1" class="nav nav-tabs bordered  ">
            <li class="active">
              <a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 培训申请表 </a>
            </li>
            <li class="disabled">
              <a href="#s2" id="other1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-th-list"></i> 培训总结 </a>
            </li>
          </ul>
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">

            <div class="row">
                <shiro:hasAnyRoles name="wechat">
                  <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(train.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
                </shiro:hasAnyRoles>
              </div>
          <hr class="simple">
          <form id="train" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="train.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input  type="hidden" name="numStatus" id="emp_numStatus" value="<s:property value="numStatus" />"/>
            <header  style="display: block;">
              培训申请&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  培训主题
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="name" id="name" placeholder="请输入培训主题" value="<s:property value="train.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  培训类型
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" disabled id="type"  value="<s:property value="train.type.name"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  培训内容
                </label>

                <section class="col col-5">
                  <label class="input">
                    <input name="uploadify" id="contentfileId" style="display: none" value="<s:property value="contentfileId"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-trainUsers"> 培训讲师</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="trainUsersName"
                           value="<s:iterator id="list" value="train.trainUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="trainUsersId" name="trainUsersId"
                           value="<s:iterator id="list" value="train.trainUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  培训周期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled" >
                    <input  id="startDate" disabled name="startDate" type="text" placeholder="请选择培训周期" value="<s:date name="train.startDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <label class="label col col-2">

                </label>
                <section class="col col-4">
                  <label class="input state-disabled" >
                    <input  id="endDate" name="endDate" disabled type="text" placeholder="请选择培训周期" value="<s:date name="train.endDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <s:iterator id="list" value="train.participantSet">
              <div class="row">
                <label class="label col col-2">部门,参与人</label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input disabled type="text" id="departmentName" value="<s:property value="#list.department.name"/>"/>
                    <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="#list.department.id"/>"/>
                  </label>
                </section>
                <label class="label col col-2">
                </label>
                <section class="col col-4">

                <label class="input state-disabled">
                  <input disabled type="text" id="participantName"
                         value="<s:iterator id="list1" value="#list.participantUsers"><s:property value="#list1.name"/>,</s:iterator>"/>
                  <input type="hidden" id="participantId" name="participantId"
                         value="<s:iterator id="list1" value="#list.participantUsers"><s:property value="#list1.id"/>,</s:iterator>"/>
                </label>
                </section>
<%--
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input disabled type="text" id="participantName" value="<s:property value="#list.participantUsers.name"/>"/>
                    <input type="hidden" id="participantId" name="participantId" value="<s:property value="#list.participantUsers.id"/>"/>
                  </label>
                </section>--%>
              </div>
              </s:iterator>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="train.remark"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="creater" value="<s:property value="train.creater.name"/>" />
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  操作日期
                </label>
                <section class="col col-5">
                  <label class="input state-disabled" >
                    <input disabled type="text" placeholder="操作时间" value="<s:date name="train.createDate" format="yyyy-MM-dd"/>">

                  </label>
                </section>
              </div>
            </fieldset>

          </form>
            <div class="flow">
              <s:if test="train.getProcessState().name()=='Running'">
                <div class="f_title">审批意见</div>
                <div class="chat-footer"style="margin-top:5px">
                  <div class="textarea-div">
                    <div class="typearea">
                      <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                    </div>
                  </div>
                  <span class="textarea-controls"></span>
                </div>
              </s:if>
              <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
              <div class="f_content" style="display:none">
                <div id="emp_showFlow"></div>
              </div>
              <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
              <div class="f_content" style="display:none">
                <div id="emp_showNext"></div>
              </div>
            </div>
            </div>
            <div class="tab-pane fade in active" id="s2" style="margin: 10px;"></div>
          </div>
        </div>
  </div>
    </div>
  </article>
</div>

<script type="text/javascript">
/*
       $('li.disabled').off("click").on("click", function () {
           return false;
       })
*/
    var state = "<s:property value="train.getProcessState().name()" />";
    if(state == "Finished"){
        $('#other1').parent('li').removeClass('disabled');
    }
    var table_global_width=0;
    $("a#other1").off("click").on("click",function(e) {
        if(state == "Finished"){
            if (table_global_width == 0) {
                table_global_width = $("#s1").is(":visible") ? $("#s1").width() : $("#s2").width();
            }
            loadURL("ajax-summary.action?parentId="+$("#keyId").val(),$('div#s2'));
        }else{
            return false;
        }

    });

    //上传
    inputLoad({
        objId:"contentfileId",
        entityName:"contentfileIds",
        sourceId:"contentfileId"
    });
    
    //审批
    $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#emp_showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#emp_showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
        var showDuty = false;
        var area = $("span.textarea-controls");
        $(pdata.data.datarows).each(function(i,v){
            var str = '<button id="emp_left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
            $(area).append(str);
            if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                showDuty = true;
            }
        });
        if(showDuty == true){
            var pdata = {
                keyId: $("input#keyId").val(),
                flowName: "train"
            };
            multiDuty(pdata);
        }
    });
    var valueObj=$("input#emp_numStatus").val();
    switch(valueObj){
        case "0":
            //保存后再提交
            $("#emp_left_foot_btn_approve").off("click").on("click",function(){
                form_save("train","ajax-train!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                    $("#btn-re-common").trigger("click");
                })
            });
            break;
        case "1":
            //第一步审批
            $("#emp_left_foot_btn_approve").off("click").on("click",function(){
                form_save("train","ajax-train!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                    $("#btn-re-common").trigger("click");
                });


            });
            break;
    }

    //流程信息展开
    $('#flow,#next').click(function(){
        if($(this).hasClass("right")){
            $(this).removeClass("right").addClass("down");
            $(this).parent(".f_title").next("div.f_content").show();
        }else{
            $(this).removeClass("down").addClass("right");
            $(this).parent(".f_title").next("div.f_content").hide();
        }
    });
   });

    //编辑
    $("a[key=ajax_edit]").click(function(){
        var draft = "<s:property value="draft" />";
        loadURL("ajax-train!input.action?keyId="+$("input#keyId").val()+"&draft="+draft,$('#content'));
    });

    //返回视图
    $("#btn-re-common").click(function(){
        var index = "<s:property value="index" />";
        var todo = "<s:property value="todo" />";
        var remind = "<s:property value="remind" />";
        var record = "<s:property value="record" />";
        var draft = "<s:property value="draft" />";
        if(index==1){
            loadURL("ajax-index!page.action",$('#content'));
        }else if(todo==1){
            loadURL("ajax!toDoList.action",$('#content'));
        }else if(remind==1){
            loadURL("ajax!remindList.action",$('#content'));
        }else if(record==1){
            loadURL("ajax!taskRecordList.action?type=1",$('#content'));
        }else if(record==2){
            loadURL("ajax!taskRecordList.action?type=2",$('#content'));
        }else if(draft==1){
            loadURL("ajax!draftList.action",$('#content'));
        }else{
            loadURL("ajax-train.action?viewtype=<s:property value="viewtype"/>",$('#content'));
        }

    });


</script>