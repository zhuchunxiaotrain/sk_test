<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_selectionpublicity_list_row">
      <table id="ajax_selectionpublicity_list_table">
      </table>
      <div id="ajax_selectionpublicity_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script tex>
  function run_jqgrid_function(){
    jQuery("#ajax_selectionpublicity_list_table").jqGrid({
      url:'../party/ajax-selectionpublicity!listfordialog.action',
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['选拔人员','应聘岗位','Id'],
      colModel : [
        {name:'name',index:'name', width:250,sortable:false,search:true,sorttype:'string'},
        {name:'post',index:'post', width:250,sortable:false,search:true,sorttype:'string'},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true},
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_selectionpublicity_list_page',
      sortname : 'createDate',
      sortorder : "desc",
        gridComplete:function(){
            var ids=$("#ajax_selectionpublicity_list_table").jqGrid('getDataIDs');
             var selectionPublicitysId;
             var param="<s:property value="param"/>"
            if(param=="selectionIssued"){
                selectionPublicitysId=$("#selectionPublicityId").val();
            }
            for(var i=0;i<ids.length;i++) {
                var cl = ids[i];
                if (selectionPublicitysId != "" && selectionPublicitysId != null) {
                    var idsArr = selectionPublicitysId.split(",");
                    for (var s = 0; s < idsArr.length; s++) {
                        if (idsArr[s] == cl) {
                            $("#ajax_selectionpublicity_list_table").jqGrid("setSelection", cl);
                        }
                    }
                }
            }
            jqGridStyle();
            $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
        },
        onSelectRow: function (rowId, status, e) {
            var selectionPublicityId="";
            var selectionPublicityApplyName="";
            var flag=false;
            var param="<s:property value="param"/>"
            if(param=="selectionIssued"){
                selectionPublicityId="selectionPublicityId";
                selectionPublicityApplyName="selectionPublicityApplyName";
                flag=true;
            }
            if(flag==true){
                $(this).gridselect('onecheck',{
                    table:"ajax_selectionpublicity_list_table",
                    rowId:rowId,
                    status:status,
                    id:selectionPublicityId,/*dictId是中间值*/
                    name:selectionPublicityApplyName,
                    type:"radio"/*单选按钮*/
                });
            }else {
                $(this).gridselect('morecheck',{
                    table:"ajax_selectionpublicity_list_table",
                    rowId:rowId,
                    status:status,
                    id:selectionPublicityId,/*dictId是中间值*/
                    name:selectionPublicityApplyName,
                    type:"checkbox"/*单选按钮*/
                });
            }

        },
        /*onSelectAll:function(rowId, status, e){
            var selectionPublicityId="";
            var selectionPublicityApplyName="";
            var param="<s:property value="param"/>"
            if(param=="selectionIssued"){
                selectionPublicityId="selectionPublicityId";
                selectionPublicityApplyName="selectionPublicityApplyName";
            }
            $(this).gridselect('morecheck',{
                table:"ajax_selectionpublicity_list_table",
                rowId:rowId,
                status:status,
                id:selectionPublicityId,
                name:selectionPublicityApplyName,
            });
        },*/

      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_selectionpublicity_list_table").jqGrid('setGridWidth', $("#ajax_selectionpublicity_list_row").width()-10);
    })

    jQuery("#ajax_selectionpublicity_list_table").jqGrid('navGrid', "#ajax_selectionpublicity_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_selectionpublicity_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
    gDialog.fClose();
  });
</script>
