<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<input type="hidden" id="departId" value="<s:property value="departId"/>"/>
<input type="hidden" id="usersId" value="<s:property value="usersId"/>"/>
<input type="hidden" id="powerId" value="<s:property value="powerId"/>"/>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12" id="ajax_power_list_row">
            <table id="ajax_power_list_table">
            </table>
            <div id="ajax_power_list_page">
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <span class="btn btn-primary" id="dialog-ok">确定</span>
    <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>
    function run_jqgrid_function(){
        jQuery("#ajax_power_list_table").jqGrid({
            url:'../com/ajax-power!powerByDepartAndUsers.action?departId='+$("#departId").val()+"&usersId="+$("#usersId").val()+"&powerId="+$("#powerId").val(),
            mtype:"POST",
            datatype: 'json',
            page : 1,
            colNames:['用户名','部门','职权','Id'],
            colModel : [
                {name:'user',index:'user', width:240,sortable:false,search:true,sorttype:'string'},
                {name:'department',index:'department', width:240,sortable:false,search:true,sorttype:'string'},
                {name:'power',index:'power', width:240,sortable:false,search:true,sorttype:'string'},
                {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
            ],
            rowNum : 10,
            rowList:[10,20,30],
            pager : '#ajax_power_list_page',
            sortname : 'createDate',
            sortorder : "desc",
            gridComplete:function(){
                var param="<s:property value="param"/>";
                if(param=="partymoney"){
                    var powersId=$("#partyMoneyPowerId").val();
                }

                var ids=$("#ajax_power_list_table").jqGrid('getDataIDs');
                for(var i=0;i<ids.length;i++) {
                    var cl = ids[i];
                    if (powersId != "" && powersId != null) {
                        var idsArr = powersId.split(",");
                        for (var s = 0; s < idsArr.length; s++) {
                            if (idsArr[s] == cl) {
                                $("#ajax_power_list_table").jqGrid("setSelection", cl);
                            }
                        }
                    }
                }
                jqGridStyle();
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
            },
            onSelectRow: function (rowId, status, e) {
                var param="<s:property value="param"/>";
                var powerId="";
                var powerName="";
                var flag=false;
                if(param=="partymoney"){
                    powerId="partyMoneyPowerId";
                    powerName="partyMoneyPowerName";
                    flag=true;
                }
                if(flag==true){
                    $(this).gridselect('onecheck',{
                        table:"ajax_power_list_table",
                        rowId:rowId,
                        status:status,
                        id:powerId,
                        name:powerName,
                        type:"radio"
                    });
                }else {
                    $(this).gridselect('onecheck',{
                        table:"ajax_power_list_table",
                        rowId:rowId,
                        status:status,
                        id:powerId,
                        name:powerName,
                        type:"checkbox"
                    });
                }

            },
            /*onSelectAll:function(aRowids, status, e){
                $(this).gridselect('morecheck',{
                    table:"ajax_power_list_table",
                    rowId:aRowids,
                    status:status,
                    id:powerId,
                    name:powerName
                });
            },*/
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },

            multiselect: true,
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            forceFit:true,
            viewrecords: true,
            autowidth: true,
            height : 'auto',
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_power_list_table").jqGrid('setGridWidth', $("#ajax_power_list_row").width()-10);
        })

        jQuery("#ajax_power_list_table").jqGrid('navGrid', "#ajax_power_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });

        jQuery("#ajax_power_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
    }

    $(function(){
        run_jqgrid_function();
    });
    $("#dialog-ok").unbind("click").bind("click",function(){
        var div="";
        var rowIdArr = $("#ajax_power_list_table").jqGrid('getGridParam','selarrrow');
        for(var i in rowIdArr){
            var rowData = $("#ajax_power_list_table").jqGrid('getRowData', rowIdArr[i]);
            var str1='<div class="row"> <label class="label col col-2"> </label> <section class="col col-2"> <label class="input"> <label class="input state-disabled"> <input disabled type="text"  name="departmentName" value="'+rowData["department"]+'"/> <input disabled type="hidden"  name="powerId" value="'+rowData["id"]+'"/>';
            var str2='</label> </label> </section> <section class="col col-2"> <label class="input"> <label class="input state-disabled"> <input disabled type="text"  name="powerName" value="'+rowData["power"]+'"/>';
            var str3='</label> </label> </section> <section class="col col-1"> <label class="input"> <label class="input"> <input  type="text" name="everyPay" value=""/> </label> </label> </section> </div>';
            div+=str1+str2+str3;
        }
        $("#payPartyMoney").append(div);
        gDialog.fClose();
    });
</script>
