<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<input type="hidden" id="participantDepartmentName" value="<s:property value="participantDepartmentName"/>"/>
<input type="hidden" id="participantDepartmentId" value="<s:property value="participantDepartmentId"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_depart_list_row">
      <table id="ajax_depart_list_table">
      </table>
      <div id="ajax_depart_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script tex>
  function run_jqgrid_function(){
    jQuery("#ajax_depart_list_table").jqGrid({
      url:'ajax-depart!list.action',
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['部门名称','上级部门','Id'],
      colModel : [
        {name:'name',index:'name', width:240,sortable:false,search:true,sorttype:'string'},
        {name:'upParent',index:'upParent', width:170,sortable:false,search:true,sorttype:'string'},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_depart_list_page',
      sortname : 'createDate',
      sortorder : "desc",
      gridComplete:function(){
          var ids=$("#ajax_depart_list_table").jqGrid('getDataIDs');
          var param=$("#param").val();
          var departmentId1=$("#participantDepartmentId").val();
         // alert("#"+departmentId1)
          var departsId;
          if(param=="annualbudget") {
              departsId=$("#annualBudgetDepartId").val();
             // alert("deppartsId = "+departsId);
          }else if(param=="rebateactivity"){
              departsId=$("#rebateActivityDepartId").val();
          }else if(param="moneymanager"){
              departsId=$("#moneyManagerDepartId").val();
          }else if(param="activitymanager"){
              departsId=$("#activityManagerDepartId").val();
          }else if(param="transfer"){
              departsId=$("#transferDepartId").val();
          }else if(param="train"){
              departsId=$("#"+departmentId1).val();
          }
        for (var i=0;i<ids.length;i++){
             // alert("start_"+i);
            var cl=ids[i];
            if(departsId!=null && departsId != ""){
                var idsArr= departsId.split(",");
                for(var s=0;s<idsArr.length;s++){
                    //alert("idsArrLength = "+idsArr.length);
                    //alert("center_"+s);
                    if(idsArr[s] == cl){
                        //alert("equerl_"+s);
                        $("#ajax_depart_list_table").jqGrid("setSelection",cl);
                    }
                }
            }
          }
        jqGridStyle();
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      },
      onSelectRow: function (rowId, status, e) {
              var param=$("#param").val();
              var departmentName=$("#participantDepartmentName").val();
              var departmentId=$("#participantDepartmentId").val();
              var departId="";
              var departName="";
              var flag=false;
              if(param=="annualbudget"){
                  departId="annualBudgetDepartId";
                  departName="annualBudgetDepartName";
                  flag=true;
              }else if(param=="rebateactivity"){
                  departId="rebateActivityDepartId";
                  departName="rebateActivityDepartName";
                  flag=true;
              }else if(param=="moneymanager"){
                  departId="moneyManagerDepartId";
                  departName="moneyManagerDepartName";
                  flag=true;
              }else if(param=="activitymanager"){
                  departId="activityManagerDepartId";
                  departName="activityManagerDepartName";
                  flag=true;
              }else if(param=="manage"){
                  departId="departId";
                  departName="departName";
                  flag=true;

              }else if(param=="interview"){
                  departId="interviewDepartId";
                  departName="interviewDepartName";
                  flag=true;

              }else if(param=="partymoney"){
                  departId="partyMoneyDepartId";
                  departName="partyMoneyDepartName";
                  flag=true;
              }else if(param=="transfer"){
                  departId="transferDepartId";
                  departName="transferDepartName";
                  flag=true;
              }else if(param=="train"){
                  departId=departmentId;
                  departName=departmentName;
                  flag=true;
              }


              if(flag==true){
                  $(this).gridselect('onecheck',{
                      table:"ajax_depart_list_table",
                      rowId:rowId,
                      status:status,
                      id:departId,
                      name:departName,
                      type:"radio",
                  });
              }else {
                  $(this).gridselect('onecheck',{
                      table:"ajax_depart_list_table",
                      rowId:rowId,
                      status:status,
                      id:departId,
                      name:departName,
                      type:"checkbox",
                  });
              }
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_depart_list_table").jqGrid('setGridWidth', $("#ajax_depart_list_row").width()-10);
    })

    jQuery("#ajax_depart_list_table").jqGrid('navGrid', "#ajax_depart_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_depart_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
      var rowId = $("#ajax_depart_list_table").jqGrid('getGridParam','selrow');
      var rowDatas = $("#ajax_depart_list_table").jqGrid('getRowData', rowId);
      var param="<s:property value="param"/>"
      if(param=="moneymanager"){
          var actionUrl="<%=path %>/com/ajax-duty!count.action";
          var data={departId :rowId}
          ajax_action(actionUrl,data,null,function (pdata) {
              var count=pdata.count;
              $("#humanNum").val(count);
          })
      }
      $("#d_name").val(rowDatas['name']);
      $("#d_id").val(rowId);
    gDialog.fClose();
  });
</script>