<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

  <!-- rows -->
  <!-- widget grid -->

            <div class="modal-body">
              <div class="row">
                <div class="col-md-12" id="ajax_interview1_list_row">
                  <table id="ajax_interview1_table" >
                  </table>
                  <div id="ajax_interview1_list_page">
                  </div>
                </div>
              </div>
            </div>
<script type="text/javascript">
    $(function(){
        load_interview_jqGrid();
    });

  function load_interview_jqGrid() {
      jQuery("#ajax_interview1_table").jqGrid({
          url:'ajax-interview!list.action?parentId='+$('#parentId').val(),
          datatype: "json", 
          colNames:["年","月","姓名","身份证号","面试结果","文档状态"], 
          colModel:[ 
              {name:'year',index:'year', search:false,width:80}, 
              {name:'month',index:'month', search:false,width:80}, 
              {name:'name',index:'name', search:false,width:80}, 
              {name:'cardID',index:'cardID', search:false,width:180}, 
              {name:'result',index:'result', search:false,width:80}, 
              {name:'state',index:'state', search:false,width:80}, 
          ], 
          sortname : '', 
          sortorder : "", 
          jsonReader: { 
              root: "dataRows", 
              records: "records", 
              repeatitems : false 
          }, 
          multiselect : false, 
          rownumbers:true, 
          gridview:true, 
          shrinkToFit:true, 
          viewrecords: true, 
          autowidth: 'auto', 
          height:'auto', 
          forceFit:true, 
          loadComplete: function() {     } });

      $(window).on('resize.jqGrid', function() {
          jQuery("#ajax_interview1_table").jqGrid('setGridWidth', $("#ajax_interview1_list_row").width());
      })
      jQuery("#ajax_interview1_table").jqGrid('filterToolbar',{
          searchOperators:false,
          stringResult:true});

      jQuery("#ajax_interview1_table").jqGrid('navGrid', "#ajax_interview1_list_page", {
          edit : false,
          add : false,
          del : false,
          search:false
      });


  }

</script>





















