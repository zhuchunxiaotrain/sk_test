<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-sm-12 col-md-12 col-lg-12">
        <div class="jarviswidget well" id="wid-id-3"
             data-widget-colorbutton="false"
             data-widget-editbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-custombutton="false"
             data-widget-sortable="false">
            <!-- widget div-->
            <div>
                <div class="widget-body">
                    <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>员工调动</a>
                    <s:if test="transfer==null || transfer.getProcessState().name()=='Draft'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
                    </s:if>
                    <s:if test="transfer!=null && transfer.getProcessState().name()=='Backed'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
                    </s:if>
                    <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
                    <hr class="simple">
                    <form id="transfer" class="smart-form" novalidate="novalidate" action="" method="post">
                        <input type="hidden" name="keyId" id="keyId" value="<s:property value="change.id" />"/>
                        <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                        <header  style="display: block;">
                            调动申请&nbsp;&nbsp;<span id="title"></span>
                        </header>
                        <fieldset>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-removeUsers"> 调动人员</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="removeUsers" name="name" placeholder="请选择调动人员"
                                                   value="<s:property value="change.name"/>"/>
                                            <input type="hidden" id="parentId" name="parentId"
                                                   value="<s:property value=""/>"/>

                                        </label>

                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    用工性质
                                </label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="natureValue" id="natureValue" placeholder="请输入用工性质" value="<s:property value="natureValue"/>" >
                                        <input type="hidden" name="nature" id="nature"  value="<s:property value="change.nature"/>" >

                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-depart"> 调往部门</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="transferDepartName" name="departName" placeholder="请选择调往部门"
                                                   value="<s:property value="change.department.name"/>"/>
                                            <input type="hidden" id="transferDepartId" name="departId"
                                                   value="<s:property value="change.department.id"/>"/>
                                        </label>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-post"> 岗位</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="postName" name="postName" placeholder="请选择调往岗位"
                                                   value="<s:property value="change.post.name"/>"/>
                                            <input type="hidden" id="postId" name="postId"
                                                   value="<s:property value="change.post.id"/>"/>
                                        </label>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    调动原因
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="reason" id="reason" placeholder="请输入调动原因" value="<s:property value="change.reason"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                     原薪资
                                </label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="oldMoney" id="oldMoney" placeholder="请输入原薪资" value="<s:property value="change.oldMoney"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    调用薪资
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="money" id="money" placeholder="请输入调用薪资" value="<s:property value="change.money"/>" >
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="usersName" name="nextStepApproversName"
                                                   value="<s:property value="change.nextStepApproversName"/>"/>
                                            <input type="hidden" id="usersId" name="nextStepApproversId"
                                                   value="<s:property value="change.nextStepApproversId"/>"/>
                                        </label>
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    操作人
                                </label>
                                <section class="col col-4">
                                    <label class="input state-disabled">
                                        <input  disabled type="text"  id="creater" placeholder="" value="<s:property value="creater.name"/>" >
                                    </label>
                                </section>
                                <label class="label col col-2">
                                    操作日期
                                </label>
                                <section class="col col-4 ">
                                    <label class="input state-disabled">
                                        <input  disabled type="text" name="createDate" id=createDate" placeholder="" value="<s:property value="createDate"/>" >
                                    </label>
                                </section>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </article>
</div>

<script>
    var draft = "<s:property value="draft" />";

    $(function(){
        var pdata= {
            keyId:$("#keyId").val(),
            flowName:"transfer"
        };
        multiDuty(pdata);
    });

    //校验
    $("#transfer").validate({
        rules : {
            name : {
                required : true
            },
            transferDepartId: {
                required : true
            },
            postId:{
                required : true
            },
            reason:{
                required : true
            },
            money:{
                required : true,
                min:0
            },

        },
        messages : {
            name : {
                required : '请选择调动人员'
            },
            transferDepartId: {
                required : '请选择调往部门'
            },
            postId:{
                required : '请选择岗位'
            },
            reason:{
                required : '请输入调动原因'
            },
            money:{
                required : '请输入调用薪资',
                min:'请输入大于0的数字'
            },


        },
        ignore: "",

    });

    //调动部门
    $("a[key=btn-choose-depart]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择调动部门",
            url:"ajax-dialog!depart.action?param=transfer",
            width:560
        }).show();
    });

    //调动岗位
    $("a[key=btn-choose-post]").unbind("click").bind("click",function(){
        var dep=$("#transferDepartId").val();
        if(dep.length<1){
            alert("请先选择调动部门！");
            return false;
        }
        gDialog.fCreate({
            title:"请选择调动岗位",
            url:"ajax-dialog!managePost.action?depId="+$("#transferDepartId").val(),
            width:340
        }).show();
    });

    $("a[key=btn-choose-removeUsers]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择调动员工",
            url:"ajax-dialog!transferUser.action",
            width:340
        }).show();
    });

    //责任人
    $("a[key=btn-choose-chargeUsers]").unbind("click").bind("click",function(){
        gDialog.fCreate({
           title:"请选择责任人",
           url:"ajax-dialog!users.action",
           width:340
        }).show();
    });

    //会签人
    $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择人员",
            url:"ajax-dialog!user.action",
            width:340
        }).show();
    });

    //保存
    $("#btn-save-common").click(

        function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("transfer","ajax-transfer!save.action",null,function (data) {

                if(data.state == "200" || data.state ==200) {
                    if(draft == 1){
                        location.href='index.action';
                    }else{
                        loadURL("ajax-change.action?viewtype=1",$('#content'));
                    }
                }
            });
        }

    );

    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#transfer").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#transfer").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("transfer","ajax-transfer!commit.action",null,function (data) {

                        if(data.state == "200" || data.state ==200) {
                            if(draft == 1){
                                location.href='index.action';
                            }else{
                                loadURL("ajax-change.action?viewtype=1",$('#content'));
                            }
                        }
                    });
                }
            });
        }
    );

    //返回视图
    $("#btn-re-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-change.action?viewtype=1",$('#content'));
        }

    });

</script>