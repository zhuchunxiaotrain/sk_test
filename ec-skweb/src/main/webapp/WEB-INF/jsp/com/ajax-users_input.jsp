<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<div class="modal-body">
    <div class="row">
            <div class="col-md-12">

            <form id="users_input" class="smart-form" novalidate="novalidate" action="" method="post">
                <header>
                   [人员添加]
                </header>
                <fieldset>

                    <div class="row">
                        <label class="label col col-3">编号</label>
                        <section class="col col-9">
                            <label class="input">
                                <input name="no" id="no" placeholder="请输入编号"
                                type="text">
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <label class="label col col-3">成员电话</label>
                        <section class="col col-9">
                            <label class="input">
                                <input name="mobile" id="mobile" placeholder="请输入手机号码"
                                type="text">
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <label class="label col col-3">姓名</label>
                        <section class="col col-9">
                            <label class="input">
                                <input name="name" id="name" placeholder="请输入姓名"
                                type="text">
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <label class="label col col-3">帐号类型</label>
                        <section class="col col-6" style="margin-left: 20px;">
                            <label  class="checkbox-inline">
                                <input type="checkbox" id="publicUser" class="checkbox style-0" name="adminType" value="1">
                                <span>前台账户</span>
                                </input>

                            </label>
                            <label  class="checkbox-inline" style="margin-left: 45px;">
                                <input type="checkbox" id="adminUser" class="checkbox style-0" name="adminType" value="2">
                                <span>后台账户</span>
                                </input>
                            </label>
                            <input type="text" id="hideType" hidden value="<s:property value="user.adminType"/> "/>
                        </section>

                    </div>
                    <div class="row">
                        <label class="label col col-3">密码</label>
                        <section class="col col-9">
                            <label class="input">
                                <input name="password" id="password" placeholder="请输入密码"
                                type="password">
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <label class="label col col-3">确认密码</label>
                        <section class="col col-9">
                            <label class="input">
                                <input name="repassword" id="repassword" placeholder="请确认密码"
                                type="password">
                            </label>
                        </section>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
	<a href="#../com/ajax!users.action" class="btn btn-primary" id="dialog-ok">确定</a>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>

    $("#users_input").validate({
        rules : {
            name : {
                required : true,
                remote:{
                    type:"POST",
                    url:"../com/ajax-users!isUniqueByName.action",
                    data:{
                        keyId:"",
                        validatePro:"name",
                        newValue:function(){return $.trim($("#name").val());}
                    }
                }
            },
            mobile : {
                required :  true,
                remote:{
                      type:"POST",
                      url:"../com/ajax-users!isUniqueByMobile.action",
                      data:{
                          keyId:"",
                          validatePro:"mobile",
                          newValue:function(){return $.trim($("#mobile").val());}
                      }
                  }
            },
            password : {
                required : true
            },
            repassword : {
                required : true,
                equalTo : '#password'
            }
        },
        messages : {
            name : {
                required : '请输入用户名',
                remote:jQuery.format("该姓名已经被注册")
            },
            mobile : {
                required : '请输入您的登陆手机号',
                remote:jQuery.format("该登陆手机号已经被注册")
            },
            password : {
                required : '请输入您的密码'
            },
            repassword : {
                required : '请再输入一次密码',
                equalTo : '两次密码输入应一致'
            }
        },
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });

    $("#dialog-ok").unbind("click").bind("click",function(){
        var $valid = $("#users_input").valid();
        if(!$valid) return false;
        var actionUrl = "<%=path%>/com/ajax-users!save.action";
        form_save("users_input",actionUrl);
        gDialog.fClose();
        jQuery("#ajax_users_table").trigger("reloadGrid");
    });
</script>