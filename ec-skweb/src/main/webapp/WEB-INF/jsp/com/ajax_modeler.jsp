<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="row" style="padding:2px">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<div>
			<form id="searchForm" method="post" action="">
				<a id="ajax_flow_createDlg" class="btn btn-default" data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 创建流程</a>
			</form>
		</div>
	</div>
</div>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">
		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
				data-widget-colorbutton="false" 
				data-widget-togglebutton="false" 
				data-widget-deletebutton="false" 
				data-widget-fullscreenbutton="true" 
				data-widget-custombutton="false" 
				data-widget-sortable="false">

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class=" ">
                                <div class="row" id="ajax_modeler_list_row">
                                    <table id="ajax_modeler_table" class="table table-striped table-bordered table-hover">
                                    </table>
                                    <div id="ajax_modeler_list_page">
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->
</section>
<!-- end widget grid -->


<script type="text/javascript">


	function loadInbox() {
		loadURL("../com/ajax!modeler.action?keyId=", $('#content'));
	}
	
	function reload(){
		loadURL("../com/ajax!modeler.action",$('#content'));
	}
    function hrefFormat(cellvalue, options, cell){
        var keyId = cell["id"];
        var href = "<%=path%>/com/ajax-running!list.action?ketId="+cell["id"];
        return "<a href='javascript:void(0);' onclick=openRunning('"+keyId+"') id="+keyId+" key='running_list'>"+cellvalue+"</a>";
    }
	function xmlFormat(cellvalue, options, cell){
		var keyId = cell["id"];
		var href = "<%=path%>/com/ajax-flow!readXml.action?ketId="+cell["id"];
		return "<a href='"+href+"' target='_blank' id="+keyId+" key='running_list'>"+cellvalue+"</a>";
	}
    $(function(){
            jQuery("#ajax_modeler_table").jqGrid({
            	url:'../com/ajax-modeler!list.action',
        	    datatype: "json",
            	colNames:['编号','名称',"Key","创建日期","操作","id"],
            	colModel:[
           		    {name:'id',index:'id', width:50,sortable:true,search:true,searchoptions:{sopt:['cn']}},
           		    {name:'name',index:'name', width:50,formatter:hrefFormat,sortable:true,search:true,searchoptions:{sopt:['cn']}},
           		    {name:'key',index:'key', width:50,sortable:true,search:true,searchoptions:{sopt:['cn']}},
           		    {name:'time',index:'time', width:50,sortable:true,search:true,searchoptions:{sopt:['cn']}},
           		    {name:'act',index:'act', width:200,sortable:false,fixed:true},
           		    {name:'id',index:'id', width:50,hidden:true},
             	],
				loadonce: true,
                rowNum : 10,
                rowList:[10,20,30],
                pager : '#ajax_modeler_list_page',
                sortname : 'id',
                sortorder : "asc",
                gridComplete:function(){
					$('.ui-search-oper').hide();
                    var ids=$("#ajax_modeler_table").jqGrid('getDataIDs');
                    for(var i=0;i<ids.length;i++){
                        var cl=ids[i];
                        de="<button class='btn btn-default' onclick=\"fn_modeler_actions_delete('"+cl+"');\"><i class='fa fa-eye'></i>删除</button>";
                        se="<button class='btn btn-default' onclick=\"fn_modeler_actions_deploy('"+cl+"');\"><i class='fa fa-eye'></i>部署</button>";
                        cf="<button class='btn btn-default' onclick=\"fn_modeler_actions_edit('"+cl+"');\"><i class='fa fa-gear'></i>编辑</button>";
                        jQuery("#ajax_modeler_table").jqGrid('setRowData',ids[i],{act:se+cf+de});
                    }
                    $(".ui-jqgrid-bdiv").css("overflow-x","hidden");//暂时这样以后再想办法(浏览器问题chrome)
                    jqGridStyle();
                },
    	        jsonReader: {
    			    root: "dataRows",
    			    page: "page",
    			    total: "total",
    			    records: "records",
    			    repeatitems : false
    			},
				caption : "<i class='fa fa-arrow-circle-right'></i> 模型列表",
    			multiselect : false,
                rownumbers:true,
                gridview:true,
                shrinkToFit:true,
                viewrecords: true,
                autowidth: true,
                height:'auto',
                forceFit:true,
                loadComplete: function() {
                }
            });
            $(window).on('resize.jqGrid', function() {
                jQuery("#ajax_modeler_table").jqGrid('setGridWidth', $("#ajax_modeler_list_row").width());
            });
		    jQuery("#ajax_modeler_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});
            jQuery("#ajax_modeler_table").jqGrid('navGrid', "#ajax_modeler_list_page", {
                edit : false,
            	add : false,
            	del : false,
            	search:false
            });
    });

    function openRunning(id){
        loadURL("../com/ajax-running!list.action?keyId="+id,$('#content'));
    }
    function fn_modeler_actions_delete(id){
        ajax_action("../com/ajax-modeler!delete.action",{modelerId:id},null,function(pdata){
            _show(pdata);
            jQuery("#ajax_modeler_table").trigger("reloadGrid");
        })
    }
    function fn_modeler_actions_deploy(id){
        ajax_action("../com/ajax-modeler!deploy.action",{modelerId:id},null,function(pdata){
            _show(pdata);
            jQuery("#ajax_modeler_table").trigger("reloadGrid");
        })
    }
    function fn_modeler_actions_edit(id){
        var href = "<%=path%>/modeler.html?modelId="+id;
        var iframe = "<iframe class='dframe' src='"+href+"'></iframe>";
        var width = window.innerWidth-100;
        window.open(href);
    }

	$("#ajax_flow_createDlg").click(function(){
		gDialog.fCreate({
			title:'创建流程',
			url:"../com/ajax-flow!createDlg.action?keyId=",
			width:500
		}).show();
	});
</script>

