<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
 <div class="row">
     <article class="col-sm-12 col-md-12 col-lg-12">
         <div class="jarviswidget well" id="wid-id-3"
              data-widget-colorbutton="false"
              data-widget-editbutton="false"
              data-widget-togglebutton="false"
              data-widget-deletebutton="false"
              data-widget-fullscreenbutton="false"
              data-widget-custombutton="false"
              data-widget-sortable="false">
              <div>
                <div class="widget-body">
                    <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i> 人员列表</a>
                    <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
                    <hr class="simple">
                    <form id="users_read" class="form-horizontal" action="" method="post">
                        <input type="hidden" name="keyId" id="keyId" value="<s:property value="user.id" />">
                        <input type="hidden" name="password" id="password" value="<s:property value="user.password" />">
                        <input type="hidden" name="repassword" id="repassword" value="<s:property value="user.password" />">
                        <fieldset>
                            <legend>
                                成员信息表
                            </legend>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email">成员邮箱</label>
                                <div class="col-md-7 state-disabled">
                                    <input class="form-control" disabled
                                           name="email" id="email"
                                        type="text" value="<s:property value="user.email" />" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">姓名</label>
                                <div class="col-md-7 state-disabled">
                                    <input class="form-control" disabled name="name" id="name"
                                        type="text" value="<s:property value="user.name" />">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="no">编号</label>
                                <div class="col-md-7">
                                    <input class="form-control" name="no" id="no" placeholder="请输入编号"
                                        type="text" value="<s:property value="user.no" />">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="idCard">身份证号</label>
                                <div class="col-md-7">
                                    <input class="form-control" name="idCard" id="idCard" placeholder="请输入身份证号"
                                        type="text" value="<s:property value="user.idCard" />">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-3 control-label" for="address">家庭住址</label>
                                <div class="col-md-7">
                                    <input class="form-control" name="address" id="address" placeholder="请输入家庭住址"
                                        type="text" value="<s:property value="user.address" />">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="areaselect_text">所在地区</label>
                                <div class="col-md-7">
                                    <a class="btn btn-success btn-sm" id="areaselect_text" href="javascript:void(0);">请选择城市</a>
                                    <span id="areaselect_span">
                                        <input name="province" id="province" type="text" placeholder="省份" value="<s:property value="user.province"/>"/>
                                        <input name="city" id="city" type="text" placeholder="城市" value="<s:property value="user.city"/>"/>
                                        <input name="county" id="county" type="text" placeholder="县级" value="<s:property value="user.county"/>"/>
                                        <a class="btn btn-success btn-sm" id="areaselect_check" href="javascript:void(0);"><i class="fa fa-check"></i> </a>
                                        <a class="btn btn-warning btn-sm" id="areaselect_cancel" href="javascript:void(0);"><i class="fa fa-ban"></i> </a>
									 </span>
                                    <!--弹出省省市-->


                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="mobile">移动电话</label>
                                <div class="col-md-7">
                                    <input class="form-control" name="mobile" id="mobile" placeholder="请输入移动电话"
                                        type="text" value="<s:property value="user.mobile" />">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="phone">固定电话</label>
                                <div class="col-md-7">
                                    <input class="form-control" name="phone" id="phone" placeholder="请输入固定电话"
                                        type="text" value="<s:property value="user.phone" />">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="terminal">终端电话</label>
                                <div class="col-md-7">
                                    <input class="form-control" name="terminal" id="terminal" placeholder="请输入终端电话"
                                        type="text" value="<s:property value="user.terminal" />">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="personalSign">个人签名</label>
                                <div class="col-md-7">
                                    <input class="form-control" name="personalSign" id="personalSign" placeholder="请输入个人签名"
                                        type="text" value="<s:property value="user.personalSign" />">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">职责</label>
                                <div class="col-md-7">
                                    <select multiple="multiple" disabled="disabled" class="form-control custom-scroll">
                                        <s:if test="dutyList!=null">
                                            <s:iterator value="dutyList" id="list">
                                                <option><s:property value="#list.department.name"/>-<s:property value="#list.post.name"/></option>
                                            </s:iterator>
                                        </s:if>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
              </div>
         </div>
   </article>
</div>

<script type="text/javascript">
    function loadCard(){
        area_save($("#province").val(),$("#city").val(),$("#county").val());
        $("a#areaselect_text").show();
    }
    loadCard();
    loadScript("../resource/com/js/data.js", loadSelect);
    function loadSelect(){
        loadScript("../resource/com/js/areaselect.js", loadLocation);
    }
    function loadLocation(){
        var locObj ;
        $("#areaselect_text").click(function(){
			$("#areaselect_span").show();
            locObj = new locationCard({
                ids: ['province', 'city', 'county']
            });
            locObj.init();
			$("#areaselect_text").hide();
		});
		$("#areaselect_span").hide();
		$("#areaselect_check").click(function(){

			if($("#province").val()!=null){
				area_save($("#province").val(),$("#city").val(),$("#county").val());
				$("#areaselect_span").hide();
				$("#areaselect_text").show();
                locObj.destoryCards();
			}else{
				$.smallBox({
					title : "操作失败",
					content : "<i class='fa fa-clock-o'></i> <i> 请选择地区，地区不能为空！</i>",
					color : "#C46A69",
					iconSmall : "fa fa-times fa-2x fadeInRight animated",
					timeout : 6000
				});
                locObj.destoryCards();
			}
		})
		$("#areaselect_cancel").click(function(){

			$("#areaselect_span").hide();
			$("#areaselect_text").show();
            locObj.destoryCards();
		})
	}
	function area_save(p,city,county){
        $("a#areaselect_text").text(p+city+county);
        if(p == "") {
            $("a#areaselect_text").text("请选择地址");
        }
	}
</script>
<script>
    $("#btn-save-common").click(
          function(e) {
    	    $.SmartMessageBox({
    		    title : "提示：",
    			content : "确定更新该用户吗？",
    			buttons : '[取消][确认]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "取消") {
    			    e.preventDefault();
    				e.stopPropagation();
    				return;
    			}
    			if (ButtonPressed === "确认") {
    			    $("#btn-save-common").attr("disabled", "disabled");
    				form_save("users_read","<%=path%>/com/ajax-users!save.action");
                    loadURL("../com/ajax!users.action",$('#content'));
    			}
    		});
    });
    $("#btn-re-common").click(function(){
        loadURL("../com/ajax!users.action",$('#content'));
    });
</script>