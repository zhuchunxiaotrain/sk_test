<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<div class="modal-body">
<input type="hidden" name="keyId" id="keyId" value="<s:property value="keyId" />">
<fieldset>
	<legend>
	  <span class="label  label-warning "> [请选择部门岗位信息]</span>
	</legend>
	<table>
	    <tr>
	        <td>
	            <ul class="list-group td-ul" id="ajax_depart_config_depart">
                        <s:iterator value="dutyArrayList" id="list">
                           <li id="<s:property value="#list.postId"/>" class="list-group-item td-li">
                                <div class="checkbox">
                                    <label>
                                      <input id="<s:property value="#list.postId"/>" type="checkbox" class="checkbox style-0" <s:property value="#list.checked"/>>
                                      <span><s:property value="#list.postName" /></span>
                                    </label>
                                </div>

                            </li>
                        </s:iterator>
                </ul>
	        </td>
	        <td>
                <ul class="list-group td-ul" id="ajax_depart_config_post">

                </ul>

	        </td>
	    </tr>
	</table>
</fieldset>
</div>
<div class="modal-footer">
	<a href="#ajax!depart.action" class="btn btn-primary" id="dialog-ok">确定</a>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>
	loadDataTableScripts();
	var actionUrl = "ajax-depart!configPost.action";
	var keyId = $("input#keyId").val();
	multList("ajax_depart_config_depart","ajax_depart_config_post",actionUrl);

	$("#dialog-ok").unbind("click").bind("click",function(){
            var actionUrl = "<%=path%>/com/ajax-depart!lead.action";
            var rowData = getRowIds("ajax_depart_config_post");
            var data={rowIds:rowData,keyId:keyId};
            ajax_action(actionUrl,data);
            gDialog.fClose();
            jQuery("#ajax_depart_table").trigger("reloadGrid");
        });
</script>
