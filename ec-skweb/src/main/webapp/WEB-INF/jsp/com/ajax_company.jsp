<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<input type="hidden" id="keyId" value="<s:property value="org.id"/>"/>
<input type="hidden" id="adminId" value="<s:property value="admin.id"/>"/>
<section>
    <row>
        <article>
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0"
                 data-widget-colorbutton="false" data-widget-editbutton="false"
                 data-widget-custombutton="false">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                    -->
                <header>
				<span class="widget-icon"> <i class="fa fa-edit"></i>
				</span>
                    <h2>企业信息设置</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">
                        <div class="widget-body-toolbar">
                            <div class="alert alert-info fade in">
                                <i class="fa-fw fa fa-info"></i> <strong>提示!</strong> 请点击下划线项即可编辑

                            </div>
                        </div>

                        <table id="user" class="table table-bordered table-striped"
                               style="clear: both">
                            <tbody>
                            <tr>
                                <td width="30%">企业账号</td>
                                <td width="70%"><s:property value="admin.username"/> &nbsp;
                                    [ 成员数量：<s:property value="org.number"/> （个）]

                                </td>
                            </tr>
                            <tr>
                                <td width="30%">企业账号注册日期</td>
                                <td width="70%"><s:date name="org.createDate" format="yyyy-MM-dd" /> &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">企业或组织全称</td>
                                <td width="70%"><a href="ajax-company!accountEdit.action#"
                                                   id="name"  data-type="text" data-pk="1"
                                                   data-original-title="请输入名称"><s:property value="org.name"/></a></td>
                            </tr>
                            <tr>
                                <td>公司或组织简称</td>
                                <td><a
                                        href="ajax-company!accountEdit.action#"
                                        id="subname" data-type="text" data-pk="1"
                                        data-placement="right" data-placeholder="小于4个字"
                                        data-original-title="简称"><s:property value="org.subname"/></a></td>
                            </tr>

                            <tr>
                                <td>所在地区</td>
                                <td>
                                    <!-- <a
                                    href="ajax-company!accountEdit.action#"
                                    id="sex" data-type="select" data-pk="1" data-value=""
                                    data-original-title="选择区域"></a> -->
                                    <a class="btn btn-success btn-sm" id="areaselect_text" href="javascript:void(0);">请选择地址</a>
									<span id="areaselect_span">
									 	<input name="province" id="province" type="text" placeholder="省份" value="<s:property value="org.province"/>"/>
							            <input name="city" id="city" type="text" placeholder="城市" value="<s:property value="org.city"/>"/>
							            <input name="county" id="county" type="text" placeholder="县级" value="<s:property value="org.county"/>"/>
										<a class="btn btn-success btn-sm" id="areaselect_check" href="javascript:void(0);"><i class="fa fa-check"></i> </a>
										<a class="btn btn-warning btn-sm" id="areaselect_cancel" href="javascript:void(0);"><i class="fa fa-ban"></i> </a>
									</span>
                                </td>
                            </tr>
                            <tr>
                                <td>联系人</td>
                                <td><a href="ajax-company!accountEdit.action#" id="manager" data-type="text" data-pk="1"  data-original-title="选择行业"><s:property value="admin.username"/></a></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->
        </article>
    </row>

</section>


<!-- SCRIPTS ON PAGE EVENT -->
<script type="text/javascript">
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    pageSetUp();
    var p = $("#province").val();
    var city = $("#city").val();
    var county = $("#county").val();

    function loadCard(){
        if($("#province").val() !=""){
            $("a#areaselect_text").text(p+city+county);
        }

        $("a#areaselect_text").show();
    }
    loadCard();
    // PAGE RELATED SCRIPTS
    loadScript("../resource/com/js/data.js", loadSelect);
    function loadSelect(){
        loadScript("../resource/com/js/areaselect.js", loadLocation);
    }
    function loadLocation(){
        $("#areaselect_text").click(function(){
            $("#areaselect_span").show();
            $("#areaselect_text").hide();
        });

        new locationCard({
            ids: ['province', 'city', 'county']
        }).init();
        $("#areaselect_span").hide();
        $("#areaselect_check").click(function(){
            if($("#province").val()!=null){
                area_save($("#province").val(),$("#city").val(),$("#county").val());

                $("#areaselect_span").hide();
                $("#areaselect_text").show();
            }else{
                $.smallBox({
                    title : "操作失败",
                    content : "<i class='fa fa-clock-o'></i> <i> 请选择地区，地区不能为空！</i>",
                    color : "#C46A69",
                    iconSmall : "fa fa-times fa-2x fadeInRight animated",
                    timeout : 6000
                });
            }
        })
        $("#areaselect_cancel").click(function(){

            $("#areaselect_span").hide();
            $("#areaselect_text").show();
        })
        //部门共享设置
        var _departmentShare_checked = <s:if test="company.departmentShare == 1">true</s:if><s:else>false</s:else>;
        $("#departmentShare").click(function(){
            _departmentShare_checked = _departmentShare_checked==true?false:true;
            keyId = _departmentShare_checked==true?"1":"0";
            //ajax
            var vActionUrl = "<%=path %>/com/ajax-company!accountEdit.action";
            $.ajax({url:vActionUrl,cache:false,dataType:"text",async:true,
                data:{ "keyId":keyId,"name":"departmentShare"},
                success: function(data){
                    if(data=="1"){
                        //前台显示出来
                        $.smallBox({
                            title : "提示：",
                            content : "<i class='fa fa-clock-o'></i> <i>操作成功！</i>",
                            color : "#659265",
                            iconSmall : "fa fa-thumbs-up bounce animated",
                            timeout : 4000
                        });
                    }else{
                        $.smallBox({
                            title : "操作失败",
                            content : "<i class='fa fa-clock-o'></i> <i>操作失败！</i>",
                            color : "#C46A69",
                            iconSmall : "fa fa-times fa-2x fadeInRight animated",
                            timeout : 6000
                        });
                    }
                }
            });
        });
        //系统提醒设置
        var _companyHelp_checked = <s:if test="company.help == 0">true</s:if><s:else>false</s:else>;
        $("#companyHelp").click(function(){
            _companyHelp_checked = _companyHelp_checked==true?false:true;
            keyId = _companyHelp_checked==true?"0":"1";
            //ajax
            var vActionUrl = "<%=path %>/com/ajax-company!accountEdit.action";
            $.ajax({url:vActionUrl,cache:false,dataType:"text",async:true,
                data:{ "keyId":keyId,"name":"help"},
                success: function(data){
                    if(data=="1"){
                        //前台显示出来
                        $.smallBox({
                            title : "提示：",
                            content : "<i class='fa fa-clock-o'></i> <i>操作成功！</i>",
                            color : "#659265",
                            iconSmall : "fa fa-thumbs-up bounce animated",
                            timeout : 4000
                        });
                    }else{
                        $.smallBox({
                            title : "操作失败",
                            content : "<i class='fa fa-clock-o'></i> <i>操作失败！</i>",
                            color : "#C46A69",
                            iconSmall : "fa fa-times fa-2x fadeInRight animated",
                            timeout : 6000
                        });
                    }
                }
            });
        });

        //业务审批开启
        var _companyifApprove_checked = <s:if test="company.ifApprove == 1">true</s:if><s:else>false</s:else>;
        $("#ifApprove").click(function(){
            _companyifApprove_checked = _companyifApprove_checked==true?false:true;
            keyId = _companyifApprove_checked==true?"1":"0";
            //ajax
            var vActionUrl = "<%=path %>/com/ajax-company!accountEdit.action";
            $.ajax({url:vActionUrl,cache:false,dataType:"text",async:true,
                data:{ "keyId":keyId,"name":"ifApprove"},
                success: function(data){
                    if(data=="1"){
                        //前台显示出来
                        $.smallBox({
                            title : "提示：",
                            content : "<i class='fa fa-clock-o'></i> <i>操作成功！</i>",
                            color : "#659265",
                            iconSmall : "fa fa-thumbs-up bounce animated",
                            timeout : 4000
                        });
                    }else{
                        $.smallBox({
                            title : "操作失败",
                            content : "<i class='fa fa-clock-o'></i> <i>操作失败！</i>",
                            color : "#C46A69",
                            iconSmall : "fa fa-times fa-2x fadeInRight animated",
                            timeout : 6000
                        });
                    }
                }
            });
        });
    }

    function area_save(province,city,county){

        //ajax执行返回时触发
        var vActionUrl = "<%=path %>../com/ajax-company!accountEdit.action";
        var data = { "province":province,"city":city,"county":county,"name":"area","keyId":$("#keyId").val()};
        ajax_action(vActionUrl,data,null,function(data){
            _show(data);
            $("#areaselect_text").text(province + city + county);
        });
    }
    /*
     * X-Ediable
     */

    loadScript("../resource/com/js/plugin/x-editable/moment.min.js", loadMockJax);

    function loadMockJax() {
        loadScript("../resource/com/js/plugin/x-editable/jquery.mockjax.min.js", loadXeditable);
    }

    function loadXeditable() {
        loadScript("../resource/com/js/plugin/x-editable/x-editable.min.js", loadTypeHead);
    }

    function loadTypeHead() {
        loadScript("../resource/com/js/plugin/typeahead/typeahead.min.js", loadTypeaheadjs);
    }

    function loadTypeaheadjs() {
        loadScript("../resource/com/js/plugin/typeahead/typeaheadjs.min.js", runXEditDemo);
    }

    function runXEditDemo() {

        (function (e) {
            "use strict";
            var t = function (e) {
                this.init("address", e, t.defaults)
            };
            e.fn.editableutils.inherit(t, e.fn.editabletypes.abstractinput);
            e.extend(t.prototype, {
                render: function () {
                    this.$input = this.$tpl.find("input")
                },
                value2html: function (t, n) {
                    if (!t) {
                        e(n).empty();
                        return
                    }
                    var r = e("<div>").text(t.city).html() + ", " + e("<div>").text(t.street).html() +
                            " st., bld. " + e("<div>").text(t.building).html();
                    e(n).html(r)
                },
                html2value: function (e) {
                    return null
                },
                value2str: function (e) {
                    var t = "";
                    if (e)
                        for (var n in e)
                            t = t + n + ":" + e[n] + ";";
                    return t
                },
                str2value: function (e) {
                    return e
                },
                value2input: function (e) {
                    if (!e)
                        return;
                    this.$input.filter('[name="password"]').val(e.password);
                    this.$input.filter('[name="confirmPassword"]').val(e.confirmPassword)
                },
                input2value: function () {
                    return {
                        password: this.$input.filter('[name="password"]').val(),
                        confirmPassword: this.$input.filter('[name="confirmPassword"]').val()
                    }
                },
                activate: function () {
                    this.$input.filter('[name="password"]').focus()
                },
                autosubmit: function () {
                    this.$input.keydown(function (t) {
                        t.which === 13 && e(this).closest("form").submit()
                    })
                }
            });
            t.defaults = e.extend({}, e.fn.editabletypes.abstractinput.defaults, {
                tpl: '<div class="editable-address"><label><span>新密码</span><input type="password" name="password" class="input-small"></label></div><div class="editable-address"><label><span>再输一次</span><input type="password" name="confirmPassword" class="input-small"></label></div>',
                inputclass: ""
            });
            e.fn.editabletypes.address = t
        })(window.jQuery);

        //ajax mocks
        $.mockjaxSettings.responseTime = 500;
        $.mockjax({
            url: '/post',
            response: function (settings) {
                log(settings, this);
            }
        });
        $.mockjax({
            url: '/error',
            status: 400,
            statusText: 'Bad Request',
            response: function (settings) {
                this.responseText = 'Please input correct value';
                log(settings, this);
            }
        });
        $.mockjax({
            url: '/status',
            status: 500,
            response: function (settings) {
                this.responseText = 'Internal Server Error';
                log(settings, this);
            }
        });

        /*  $.mockjax({
         url: '/smartsales/com/ajax-company!getIndustry.action',
         response: function (settings) {

         this.responseText = [{
         value: 0,
         text: 'Guest'
         }, {
         value: 1,
         text: 'Service'
         }, {
         value: 2,
         text: 'Customer'
         }, {
         value: 3,
         text: 'Operator'
         }, {
         value: 4,
         text: 'Support'
         }, {
         value: 5,
         text: 'Admin'
         }];
         log(settings, this);
         }
         }); */

        //TODO: add this div to page
        function log(settings, response) {
            var s = [],
                    str;
            s.push(settings.type.toUpperCase() + ' url = "' + settings.url + '"');
            for (var a in settings.data) {
                if (settings.data[a] && typeof settings.data[a] === 'object') {
                    str = [];
                    for (var j in settings.data[a]) {
                        str.push(j + ': "' + settings.data[a][j] + '"');
                    }
                    str = '{ ' + str.join(', ') + ' }';
                } else {
                    str = '"' + settings.data[a] + '"';
                }
                s.push(a + ' = ' + str);
            }
            s.push('RESPONSE: status = ' + response.status);

            if (response.responseText) {
                if ($.isArray(response.responseText)) {
                    s.push('[');
                    $.each(response.responseText, function (i, v) {
                        s.push('{value: ' + v.value + ', text: "' + v.text + '"}');
                    });
                    s.push(']');
                } else {
                    s.push($.trim(response.responseText));
                }
            }
            s.push('--------------------------------------\n');
            $('#console').val(s.join('\n') + $('#console').val());
        }

        /*
         * X-EDITABLES
         */
        /*
         $('#inline').on('change', function (e) {
         if ($(this).prop('checked')) {
         window.location.href = 'http://192.241.236.31/test3.smartadmin/ajax/?mode=inline#ajax/plugins.html';
         } else {
         window.location.href = 'http://192.241.236.31/test3.smartadmin/ajax/?#ajax/plugins.html';
         }
         });

         if (window.location.href.indexOf("?mode=inline") > -1) {
         $('#inline').prop('checked', true);
         $.fn.editable.defaults.mode = 'inline';
         } else {
         $('#inline').prop('checked', false);
         $.fn.editable.defaults.mode = 'popup';
         }
         */
        //defaults
       $.fn.editable.defaults.url = 'ajax-company!accountEdit.action';
       $.fn.editable.defaults.mode = 'inline'; //use this to edit inline

        //enable / disable
        $('#enable').click(function () {
            $('#user .editable').editable('toggleDisabled');
        });

        //editables
        $('#name').editable({
            url: 'ajax-company!accountEdit.action?keyId='+$("#keyId").val(),
            type: 'text',
            pk: "1",
            name: 'name',
            title: '请输入组织名称'
        });

        $('#subname').editable({
            url: 'ajax-company!accountEdit.action?keyId='+$("#keyId").val(),
            type: 'text',
            pk: "1",
            name: 'subname',
            title: '请输入组织名称'
        });
        $('#manager').editable({
            url: 'ajax-company!accountEdit.action?adminId='+$("#adminId").val(),
            type: 'text',
            pk: "1",
            name: 'manager',
            title: '请输入组织名称'
        });

        $('#mphone').editable({
            url: 'ajax-company!accountEdit.action?adminId='+$("#adminId").val(),
            type: 'text',
            pk: "1",
            name: 'mphone',
            title: '请输入组织名称'
        });

        $('#sex').editable({
            prepend: "请选择",
            source: [{
                value: 1,
                text: '上海'
            }, {
                value: 2,
                text: '北京'
            }],
            display: function (value, sourceData) {
                var colors = {
                    "": "gray",
                    1: "green",
                    2: "blue"
                }, elem = $.grep(sourceData, function (o) {
                    return o.value == value;
                });

                if (elem.length) {
                    $(this).text(elem[0].text).css("color", colors[value]);
                } else {
                    $(this).empty();
                }
            }
        });
        $('#status').editable();



        $('#vacation').editable({
            datepicker: {
                todayBtn: 'linked'
            }
        });

        $('#dob').editable();

        $('#event').editable({
            placement: 'right',
            combodate: {
                firstItem: 'name'
            }
        });

        $('#meeting_start').editable({
            format: 'yyyy-mm-dd hh:ii',
            viewformat: 'dd/mm/yyyy hh:ii',
            validate: function (v) {
                if (v && v.getDate() == 10)
                    return 'Day cant be 10!';
            },
            datetimepicker: {
                todayBtn: 'linked',
                weekStart: 1
            }
        });

        $('#comments').editable({
            showbuttons: 'bottom'
        });

        $('#note').editable();
        $('#pencil').click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            $('#note').editable('toggle');
        });

        $('#state').editable({
            source: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
                "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas",
                "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota",
                "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey",
                "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon",
                "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas",
                "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"
            ]
        });

        $('#state2').editable({
            value: 'California',
            typeahead: {
                name: 'state',
                local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
                    "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
                    "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan",
                    "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire",
                    "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio",
                    "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota",
                    "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia",
                    "Wisconsin", "Wyoming"
                ]
            }
        });

        $('#fruits').editable({
            pk: 1,
            limit: 3,
            source: [{
                value: 1,
                text: 'banana'
            }, {
                value: 2,
                text: 'peach'
            }, {
                value: 3,
                text: 'apple'
            }, {
                value: 4,
                text: 'watermelon'
            }, {
                value: 5,
                text: 'orange'
            }]
        });

        $('#tags').editable({
            inputclass: 'input-large',
            select2: {
                tags: ['html', 'javascript', 'css', 'ajax'],
                tokenSeparators: [",", " "]
            }
        });

        //所属行业
        var industry = [];
        $.each({
            "BD": "Bangladesh",
            "BE": "Belgium"
        }, function (k, v) {
            industry.push({
                id: k,
                text: v
            });
        });
        $('#industry').editable({
            showbuttons: false
        });
        $('#industry').editable({
            source: industry,
            select2: {
                width: 200
            }
        });

        //地址
        $('#address').editable({
            value: {
                password: "",
                confirmPassword: ""
            },
            validate: function (value) {
                if (value.password == ''){return '密码不能为空';}
                if (value.password != value.confirmPassword){return '两次输入不一致';}
            },
            display: function (value) {
                /* if (!value) {
                 $(this).empty();
                 return;
                 }
                 var html = '<b>' + $('<div>').text(value.city).html() + '</b>, ' + $('<div>').text(value.street)
                 .html() + ' st., bld. ' + $('<div>').text(value.building).html();
                 */
                var html = "修改密码";
                $(this).html(html);
            }
        });

        $('#user .editable').on('hidden', function (e, reason) {
            if (reason === 'save' || reason === 'nochange') {
                var $next = $(this).closest('tr').next().find('.editable');
                if ($('#autoopen').is(':checked')) {
                    setTimeout(function () {
                        $next.editable('show');
                    }, 300);
                } else {
                    $next.focus();
                }
            }
        });

    }



</script>
