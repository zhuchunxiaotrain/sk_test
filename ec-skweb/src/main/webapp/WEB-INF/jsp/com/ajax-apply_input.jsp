<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all">员工转正</i></a>

          <s:if test="apply==null || apply.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="apply!=null && apply.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="apply" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="apply.id" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              员工转正&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <section class="col col-5">
                  <label class="input">
                    <input type="hidden"  name="nature" id="nature"  value="<s:property value="employees.nature"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input state-disabled ">
                    <input disabled id="name" name="name" type="text" value="<s:property value="employees.name"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  性别
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="gender" name="gender"  value="<s:property value="employees.gender"/>">

                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  部门
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="depName" id="depName"  value="<s:property value="department.name"/>" >
                    <input type="hidden"  name="depId" id="depId" value="<s:property value="department.id"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  岗位
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="postName" id="postName"  value="<s:property value="post.name"/>" >
                    <input type="hidden"  name="postId" id="postId" value="<s:property value="post.id"/>" >

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同开始日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input id="startDate" name="startDate"
                           type="text" value="<s:date name="employees.startDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>

                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同结束日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input id="endDate" name="endDate"
                           type="text" value="<s:date name="employees.endDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  试用期开始日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="trialStart" name="trialStart" type="text" value="<s:date name="employees.trialStart" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  试用期结束日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="trialEnd" name="trialEnd" type="text" value="<s:date name="employees.trialEnd" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                </label>
                <a href="javascript:void(0);" > 详细信息</a>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="usersName" name="nextStepApproversName"
                             value="<s:property value="apply.nextStepApproversName"/>"/>
                      <input type="hidden" id="usersId" name="nextStepApproversId"
                             value="<s:property value="apply.nextStepApproversId"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text"  id="creater" placeholder="" value="<s:property value="creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input  disabled type="text" name="createDate" id=createDate" placeholder="" value="<s:property value="createDate"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">
    var draft = "<s:property value="draft" />";

    /*
           $('li.disabled').off("click").on("click", function () {
               return false;
           })

    */
    $(function(){
        var pdata= {
            keyId:$("#keyId").val(),
            flowName:"apply",
            todo:"1"

        };
        multiDuty(pdata);
    });

    $('#trialStart').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN',
        minView:2
    }).on('changeDate',function () {
        var trialStart=$('#trialStart').val();
        $('#trialEnd').datetimepicker('setStartDate',trialStart);

    });

   $('#startDate,#endDate,#trialEnd').datetimepicker({
       format: 'yyyy-mm-dd',
       weekStart: 1,
       autoclose: true,
       todayBtn: 'linked',
       language: 'zh-CN',
       minView:2
    });

    $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择人员",
            url:"ajax-dialog!user.action",
            width:340
        }).show();
    });
    //员工详细信息

    //保存
    $("#btn-save-common").click(

        function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("apply","ajax-apply!save.action",null,function (data) {

                if(data.state == "200" || data.state ==200) {
                    if(draft == 1){
                        location.href='index.action';
                    }else{
                        loadURL("ajax-probation.action",$('#content'));
                    }
                }
            });

        }

    );

    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#apply").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#apply").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("apply","ajax-apply!commit.action",null,function (data) {
                        if(data.state == "200" || data.state ==200) {
                            if(draft == 1){
                                location.href='index.action';
                            }else{
                                loadURL("ajax-probation.action",$('#content'));
                            }
                        }

                    });
                }
            });
        }
    );

    //返回视图
    $("#btn-re-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-probation.action",$('#content'));
        }

    });

</script>