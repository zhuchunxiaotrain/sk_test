<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="row" style="padding:2px">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<form id="searchForm" method="post" action="">
			<h5><label style="width:100%"> <span class="label  label-success"> 提示：可以添加、删除、修改职权信息,职权搜索只支持部门搜索</span> </label>
			</h5><a id="ajax_power_btn_add" class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 添加职权</a>
		<%--<a id="ajax_export_power_info" class="btn btn-default btn-primary pull-right" href="javascript:void(0);"> 职权信息导出  <i class="fa fa-lg fa-external-link"></i></a>--%>
		<%--<a id="ajax_import_power_upload" class="btn btn-default btn-primary pull-right" href="javascript:void(0);"> 职权信息导入 <i class="fa fa-lg fa-cloud-upload"></i></a>--%>
		</form>
	</div>
</div>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false"
				data-widget-colorbutton="false" 
				data-widget-togglebutton="false" 
				data-widget-deletebutton="false" 
				data-widget-fullscreenbutton="true" 
				data-widget-custombutton="false" 
				data-widget-sortable="false">

				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<div style="margin-left: 30px;margin-top: -5px;position: absolute;">职权信息 </div>
				</header>
				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="col-sm-12 col-md-12 col-lg-12">
                            <div class=" ">
                                <div class="row" id="ajax_power_list_row">
						            <table id="ajax_power_table" class="table table-striped table-bordered table-hover">
						            </table>
						            <div id="ajax_power_list_page">
						            </div>
						         </div>
						    </div>
                        </div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<div id="dialog-power_message">

</div><!-- #dialog-message -->

<div id="dialog-message">

</div><!-- #dialog-message -->

<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();
	

	function reload(){
		loadURL("../com/ajax!power.action",$('#content'));
	}
	
	//添加职权
	$('#ajax_power_btn_add').click(function() {
        gDialog.fCreate({
		    title:'添加职权信息',
		    url:"../com/ajax-power!input.action?keyId=",
		    width:500
		}).show();
	});

	$("#dialog-power_message").dialog({
		autoOpen : false,
		modal : true,
		title : "职权信息框",
		width:500,
		buttons : [{
			html : "取消",
			"class" : "btn btn-default",
			click : function() {
				$(this).dialog("close");
			}
		}, {
			html : "<i class='fa fa-check'></i>&nbsp; 提交",
			"class" : "btn btn-primary",
			click : function() {
			    var actionUrl = "<%=path%>/com/ajax-power!save.action";
			    var opt={dialogId:'dialog-power_message'};
			    form_save("power_read",actionUrl,opt);
			    loadInbox();

			}
		}]

	});

</script>
<script type="text/javascript">
	pageSetUp();

	$(function(){
		load_jqGrid();
	});

	function load_jqGrid(){
        jQuery("#ajax_power_table").jqGrid({
            	url:'../com/ajax-power!list.action',
        	    datatype: "json",
            	colNames:["描述","职权","上级职权","操作","id"],
            	colModel:[
           		    {name:'description',index:'description', width:50,searchoptions:{sopt:['cn']}},
					{name:'name',index:'name', width:80,searchoptions:{sopt:['cn']}},
					{name:'upParent',index:'upParent', width:80,sortable:true,search:true,searchoptions:{sopt:['cn']}},
           		    {name:'act',index:'act', width:150,sortable:false,search:false,fixed:true},
           		    {name:'id',index:'id', width:50,hidden:true}
             	],
			    loadonce:true,
                rowNum : 10,
                rowList:[10,20,30],
                pager : '#ajax_power_list_page',
                sortname : 'id',
                sortorder : "asc",
                gridComplete:function(){
					$('.ui-search-oper').hide();
                    var ids=$("#ajax_power_table").jqGrid('getDataIDs');
                    for(var i=0;i<ids.length;i++){
                        var cl=ids[i];
                        se="<button class='btn btn-default' data-original-title='编辑' onclick=\"fn_power_actions_edit('"+cl+"');\"><i class='fa fa-eye'></i>编辑</button>"+" ";
                        de="<button class='btn btn-default' data-original-title='删除' onclick=\"fn_power_actions_delete('"+cl+"');\"><i class='fa fa-times'></i>删除</button>"+" ";
                        jQuery("#ajax_power_table").jqGrid('setRowData',ids[i],{act:se+de});
                    }
                    $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                    jqGridStyle();
                },
    	        jsonReader: {
    			    root: "dataRows",
    			    page: "page",
    			    total: "total",
    			    records: "records",
    			    repeatitems : false
    			},
    			multiselect : false,
                rownumbers:true,
                gridview:true,
                shrinkToFit:true,
                viewrecords: true,
                autowidth: true,
                height:'auto',
                forceFit:true,
                loadComplete: function() {
                }
            });
		jQuery("#ajax_power_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});
		jQuery("#ajax_power_table").jqGrid('navGrid', "#ajax_power_list_page", {
			edit : false,
			add : false,
			del : false,
			search:false
		});
	}
    $(window).on('resize.jqGrid', function() {
        jQuery("#ajax_power_table").jqGrid('setGridWidth', $("#ajax_power_list_row").width());
    })


    //编辑职权
    function fn_power_actions_edit(id){
        gDialog.fCreate({
		    title:'编辑职权信息',
		    url:"../com/ajax-power!input.action?keyId=" +id,
		    width:500
		}).show();
    }
    //删除职权
    function fn_power_actions_delete(id){
    	$.SmartMessageBox({
            title : "<i class='fa fa-minus-square txt-color-orangeDark'></i> <span class='txt-color-orangeDark'><strong>删除职权 【请确该职权没有下级职权】</strong></span>",
     		content : "确定删除该职权吗？",
    		buttons : '[取消][确定]'
    	}, function(ButtonPressed) {
    		if (ButtonPressed === "确定") {
    			var vActionUrl = "<%=path%>/com/ajax-power!delete.action";
    			data={keyId:id};
    			ajax_action(vActionUrl,data,{},function(pdata){
                    _show(pdata);
                });
    			jQuery("#ajax_power_table").trigger("reloadGrid");
    		}
    	});
    }
	$("#ajax_export_power_info").click(function(){
		var content = "";
		var title = "确定导出业务数据吗？";

		$.SmartMessageBox({
			title : title,
			content : content,
			buttons : '[取消][确定]'
		}, function(ButtonPressed) {
			if (ButtonPressed === "确定") {
				$("#searchForm").attr("action","../com/ajax-import!exportPowerFile.action").submit();
			}
		});
	});
	$("#ajax_import_power_upload").click(function(){
		gDialog.fCreate({
			title:'请上传excel文件',
			url:"../com/ajax-import!powerDialog.action",
			width:500
		}).show();
	});
</script>