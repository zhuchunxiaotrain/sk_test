<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<input type="hidden" id="keyId" value="<s:property value="org.id"/>"/>
<input type="hidden" id="adminId" value="<s:property value="admin.id"/>"/>
<!-- rows -->
<!-- widget grid -->
<jsp:include page="ajax-top.jsp" />
<section>

	<!-- row -->
	<div class="row">
		<!-- NEW WIDGET START -->
		<article>

			<!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0"
                 data-widget-colorbutton="false" data-widget-editbutton="false"
                 data-widget-custombutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->

                <header>
				<span class="widget-icon"> <i class="fa fa-edit"></i>
				</span>
                    <%--<h2>企业信息设置</h2>--%>
                </header>
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">
                        <div class="widget-body-toolbar">
                            <div class="alert alert-info fade in">
                                <i class="fa-fw fa fa-info"></i> <strong>提示!</strong> 请点击下划线项即可编辑

                            </div>
                        </div>

                        <table id="user" class="table table-bordered table-striped"
                               style="clear: both">
                            <tbody>
                            <tr>
                                <td width="30%">企业账号</td>
                                <td width="70%"><s:property value="admin.username"/> &nbsp;
                                    [ 成员数量：<s:property value="number"/> （个）]

                                </td>
                            </tr>
                            <tr>
                                <td width="30%">企业账号注册日期</td>
                                <td width="70%"><s:date name="org.createDate" format="yyyy-MM-dd" /> &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">企业或组织全称</td>
                                <td width="70%"><a href="ajax-company!accountEdit.action#"
                                                   id="name"  data-type="text" data-pk="1"
                                                   data-original-title="请输入名称"><s:property value="org.name"/></a></td>
                            </tr>
                            <tr>
                                <td>公司或组织简称</td>
                                <td><a
                                        href="ajax-company!accountEdit.action#"
                                        id="subname" data-type="text" data-pk="1"
                                        data-placement="right" data-placeholder="小于4个字"
                                        data-original-title="简称"><s:property value="org.subname"/></a></td>
                            </tr>

                            <tr>
                                <td>所在地区</td>
                                <td>
                                    <!-- <a
                                    href="ajax-company!accountEdit.action#"
                                    id="sex" data-type="select" data-pk="1" data-value=""
                                    data-original-title="选择区域"></a> -->
                                    <a class="btn btn-success btn-sm" id="areaselect_text" href="javascript:void(0);">请选择地址</a>
									<span id="areaselect_span">
									 	<input name="province" id="province" type="text" placeholder="省份" value="<s:property value="org.province"/>"/>
							            <input name="city" id="city" type="text" placeholder="城市" value="<s:property value="org.city"/>"/>
							            <input name="county" id="county" type="text" placeholder="县级" value="<s:property value="org.county"/>"/>
										<a class="btn btn-success btn-sm" id="areaselect_check" href="javascript:void(0);"><i class="fa fa-check"></i> </a>
										<a class="btn btn-warning btn-sm" id="areaselect_cancel" href="javascript:void(0);"><i class="fa fa-ban"></i> </a>
									</span>
                                </td>
                            </tr>
                            <tr>
                                <td>联系人</td>
                                <td><span ><s:property value="admin.username"/></span></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>
				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body ">
                        <div class="col-sm-12 col-md-12 col-lg-12">
								<%--<div id="org" style="display:none">--%>
								<%--</div>--%>
							<ul id="org" style="display:none">
							</ul>
							<div id="chart" class="orgChart"></div>
                        </div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();
	
	// PAGE RELATED SCRIPTS
	function loadDataTableScripts() {
		loadScript("../resource/newframejs/plugin/datatables/jquery.dataTables-cust.min.js", dt_2);

		function dt_2() {
			loadScript("../resource/newframejs/plugin/datatables/ColReorder.min.js", dt_3);
		}

		function dt_3() {
			loadScript("../resource/newframejs/plugin/datatables/FixedColumns.min.js", dt_4);
		}

		function dt_4() {
			loadScript("../resource/newframejs/plugin/datatables/ColVis.min.js", dt_5);
		}

		function dt_5() {
			loadScript("../resource/newframejs/plugin/datatables/ZeroClipboard.js", dt_6);
		}

		function dt_6() {
			loadScript("../resource/newframejs/plugin/datatables/media/js/TableTools.min.js", dt_7);
		}

		function dt_7() {
			loadScript("../resource/newframejs/plugin/datatables/DT_bootstrap.js", runDataTables);
		}

	}
	function runDataTables() {
    /*
    * BASIC
    $('#ajax_members_table').dataTable({
    		"sPaginationType" : "bootstrap_full"
    });*/
    /* END BASIC */
    }
	
	function reload(){
		loadURL("ajax!organization.action",$('#content'));
	}

</script>
<script type="text/javascript">
	function orgInfo(data){
		var liObj = "<li id='"+data.id+"'>"+data.name;
		if(data.child){
			liObj = liObj + "<ul>";
			$.each(data.child,function(i,v){
				liObj = liObj + orgInfo(v);
			})
			liObj = liObj + "</ul>";
		}
		liObj = liObj + "</li>";

		return liObj;
	}
	$(function(){
		$.ajax({
			url : "ajax-depart!organization.action",
			method:"post",
			cache : false,
			dataType : "json",
			async : false,
			success : function(data) {
				var obj = orgInfo(data.rows);
				$("#org").append(obj);
				$("#org").jOrgChart({
					chartElement : '#chart'
				});
			}
		});

	});
</script>
<script>
    var p = $("#province").val();
    var city = $("#city").val();
    var county = $("#county").val();

    function loadCard(){
        if($("#province").val() !=""){
            $("a#areaselect_text").text(p+city+county);
        }

        $("a#areaselect_text").show();
    }
    loadCard();
    // PAGE RELATED SCRIPTS
    loadScript("../resource/com/js/data.js", loadSelect);
    function loadSelect(){
        loadScript("../resource/com/js/areaselect.js", loadLocation);
    }
    function loadLocation() {
        $("#areaselect_text").click(function () {
            $("#areaselect_span").show();
            $("#areaselect_text").hide();
        });

        new locationCard({
            ids: ['province', 'city', 'county']
        }).init();
        $("#areaselect_span").hide();
        $("#areaselect_check").click(function () {
            if ($("#province").val() != null) {
                area_save($("#province").val(), $("#city").val(), $("#county").val());

                $("#areaselect_span").hide();
                $("#areaselect_text").show();
            } else {
                $.smallBox({
                    title: "操作失败",
                    content: "<i class='fa fa-clock-o'></i> <i> 请选择地区，地区不能为空！</i>",
                    color: "#C46A69",
                    iconSmall: "fa fa-times fa-2x fadeInRight animated",
                    timeout: 6000
                });
            }
        })
        $("#areaselect_cancel").click(function () {

            $("#areaselect_span").hide();
            $("#areaselect_text").show();
        });
    }

    function area_save(province,city,county){

        //ajax执行返回时触发
        var vActionUrl = "ajax-company!accountEdit.action";
        var data = { "province":province,"city":city,"county":county,"name":"area","keyId":$("#keyId").val()};
        ajax_action(vActionUrl,data,null,function(data){
            _show(data);
            $("#areaselect_text").text(province + city + county);
        });
    }
</script>
<script>
    loadScript("../resource/newframejs/plugin/x-editable/moment.min.js", loadMockJax);

    function loadMockJax() {
        loadScript("../resource/newframejs/plugin/x-editable/jquery.mockjax.min.js", loadXeditable);
    }

    function loadXeditable() {
        loadScript("../resource/newframejs/plugin/x-editable/x-editable.min.js", loadTypeHead);
    }

    function loadTypeHead() {
        loadScript("../resource/newframejs/plugin/typeahead/typeahead.min.js", loadTypeaheadjs);
    }

    function loadTypeaheadjs() {
        loadScript("../resource/newframejs/plugin/typeahead/typeaheadjs.min.js", runXEditDemo);
    }

    function runXEditDemo() {

        (function (e) {
            "use strict";
            var t = function (e) {
                this.init("address", e, t.defaults)
            };
            e.fn.editableutils.inherit(t, e.fn.editabletypes.abstractinput);
            e.extend(t.prototype, {
                render: function () {
                    this.$input = this.$tpl.find("input");
                },
                value2html: function (t, n) {
                    if (!t) {
                        e(n).empty();
                        return
                    }
                    var r = e("<div>").text(t.city).html() + ", " + e("<div>").text(t.street).html() +
                            " st., bld. " + e("<div>").text(t.building).html();
                    e(n).html(r)
                },
                html2value: function (e) {
                    return null
                },
                value2str: function (e) {
                    var t = "";
                    if (e)
                        for (var n in e)
                            t = t + n + ":" + e[n] + ";";
                    return t
                },
                str2value: function (e) {
                    return e
                },
                value2input: function (e) {
                    if (!e)
                        return;
                    this.$input.filter('[name="password"]').val(e.password);
                    this.$input.filter('[name="confirmPassword"]').val(e.confirmPassword)
                },
                input2value: function () {
                    return {
                        password: this.$input.filter('[name="password"]').val(),
                        confirmPassword: this.$input.filter('[name="confirmPassword"]').val()
                    }
                },
                activate: function () {
                    this.$input.filter('[name="password"]').focus()
                },
                autosubmit: function () {
                    this.$input.keydown(function (t) {
                        t.which === 13 && e(this).closest("form").submit()
                    })
                }
            });
            t.defaults = e.extend({}, e.fn.editabletypes.abstractinput.defaults, {
                tpl: '<div class="editable-address"><label><span>新密码</span><input type="password" name="password" class="input-small"></label></div><div class="editable-address"><label><span>再输一次</span><input type="password" name="confirmPassword" class="input-small"></label></div>',
                inputclass: ""
            });
            e.fn.editabletypes.address = t;



            $.fn.editable.defaults.url = 'ajax-company!accountEdit.action';
            $.fn.editable.defaults.mode = 'inline';
            $('#name').editable({
                url: 'ajax-company!accountEdit.action?keyId='+$("#keyId").val(),
                type: 'text',
                pk: "1",
                name: 'name',
                title: '请输入组织全称',
                success:function(){
                    var data={state:200,title:"操作状态",message:"保存成功"};
                    _show(data);
                }
            });

            $('#subname').editable({
                url: 'ajax-company!accountEdit.action?keyId='+$("#keyId").val(),
                type: 'text',
                pk: "1",
                name: 'subname',
                title: '请输入组织简称',
                success:function(){
                    var data={state:200,title:"操作状态",message:"保存成功"};
                    _show(data);
                }
            })

        })(window.jQuery);
    }


</script>