<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title> 上海市卫生产业开发中心-协同运营平台 </title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Use the correct meta names below for your web application

    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/smartadmin-production.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/smartadmin-skins.css">

    <!-- SmartAdmin RTL Support is under construction
        <link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/demo.css">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="../resource/com/img/favicon/favicon-t.ico" type="image/x-icon">
    <link rel="icon" href="../resource/com/img/favicon/favicon-t.ico" type="image/x-icon">
    <script type="text/javascript">



    </script>
<style>
    body{

    }
    #main{
            z-index:-9;
            background:url('../resource/com/img/loginbg9005.jpg')  no-repeat;
            background-size:100%;
            min-height:800px;
            width:100%;
            height:100%;

    }

    .clear{clear:both}
    .oWindowBody{ position:absolute; left:0; right:0; bottom:0; top:0px;}
    .noframe .oWindowBody{ top:0; }
    .osWindowBodyContent{ position:absolute; width:100%; height:100%; top:0; left:0; }
    .oWindowMax .oWinRestore{ display:block; }
    .noframe{ border:none; background-color:transparent; box-shadow:none; border-radius:0; overflow:visible; }
    .noframe .oWindowTop, .noframe .oWinToolBar, .oWindowMax .oWinMax{ display:none; }
    .oLoadingWrap{ position:absolute; z-index:2; left:0; right:0; bottom:0; top:0; background:#09aafd url('../resource/com/img/appLoadingBg.png') center center no-repeat; }
    .oAppLoadingBox{ position:absolute; width:280px; height:190px; top:50%; left:50%; margin:-95px 0 0 -140px; text-align:center; }
    .oAppLoadingIco{ width:256px; height:124px; background-image:url('../resource/com/img/appLoading.gif'); }
    .osLoginWrap {
      width: 519px;
      position: absolute;
      margin: auto;
      border: 1px solid #273442;
      background: #4393f9;
      box-shadow: 0 12px  #5aaefc inset;
      opacity:0.6;
      filter:alpha(opacity=60);
      -moz-opacity:0.6;
      -khtml-opacity: 0.6;
        /*min-height:352px; */
      /*border-radius: 6px 6px 0 0;*/
      padding-bottom: 12px;
      left:80%;
      top:20%;
    }

    .osLoginHeader {
      height: 43px;
      background: url('../resource/com/img/osLoginHeaderBg.png') repeat;
      position: relative;
      border-radius: 4px 4px 0 0;
    }
    .osLoginAvatar {
      width: 136px;
      height: 136px;
      border-radius: 70px;
      padding: 5px;
      box-shadow: 0 3px 5px rgba(0, 0, 0, 0.2);
      background: #ffffff ;
      position: absolute;
      left: 50%;
      top: 50%;
      margin: -68px 0 0 -68px;
    }
    .osLoginAvatar img {
    display: block;
    height: 126px;
    width: 126px;
    border-radius: 70px;
    border: none;

    }
    .osLoginInner {
      padding-bottom: 24px;
      box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
      position: relative;
    }
    .osLoginEName {
      color: #4f5255;
      font-size: 18px;
      width: 322px;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      margin: 54px auto 12px;
      text-align: center;
      line-height: 1.2em;
    }
    .osLoginIco01:before {
      background: url('../resource/com/img/osLoginIco.png') no-repeat;
      background-position: 0 -3px;
    }
    .osLoginIco02:before {
      background: url('../resource/com/img/osLoginIco.png') no-repeat no-repeat 0 -39px;
    }
    .osLoginIco01:before,
    .osLoginIco02:before {
      content: '';
      position: absolute;
      left: 12px;
      top: 12px;
      width: 20px;
      height: 17px;

    }
    .osLoginIptBox:after {
      content: "";
      position: absolute;
      top: 0;
      bottom: 0;
      left: 42px;
      width: 4px;
      background: url('../resource/com/img/osLoginIco.png') no-repeat -7px -126px;
    }
    .osLoginIptBox {
      background-image: -webkit-linear-gradient(-90deg, #fff,#fff);
      background-image: -moz-linear-gradient(-90deg, #fff,#fff);
      background-image: -o-linear-gradient(-90deg, #fff,#fff);
      background-image: linear-gradient(180deg, #fff,#fff);
      position: relative;
      width: 400px;
      height: 39px;
      border: 1px solid #bdbdbd;
      border-radius: 4px;
      margin: 0 auto 19px;
      padding: 0 0 0 43px;
      box-shadow: 0 2px 3px rgba(82, 85, 90, 0.21) inset;
    }
    .osLoginIpt {
      height: 39px;
      margin: 0;
      width: 393px;
      padding-left:7px;
      border: 0;
      outline: 0;
      background: none;
    }
    .osLoginIptBtn {
      height: 40px;
      line-height: 40px;
      text-align: center;
      width: 120px;
      margin: 30px auto 0px;

      cursor: pointer;
      font-size: 24px;
      border-radius: 4px;
      font-family: 'Microsoft YaHei';
      background-image: -webkit-linear-gradient(-90deg, #5aaefc,#4393f9);
      background-image: -moz-linear-gradient(-90deg, #5aaefc,#4393f9);
      background-image: -o-linear-gradient(-90deg, #5aaefc,#4393f9);
      background-image: linear-gradient(180deg, #5aaefc,#4393f9);
      box-shadow: 0 1px 0 rgba(255, 255, 255, 0.34) inset, 0 2px 2px 0 rgba(0, 0, 0, 0.19);
      border: 1px solid #4186db;
      color: #fff;
    }
    .osLoginTip {
      position: absolute;
      z-index: 9999;
      background: rgba(49, 51, 58, 0.8);
      border-radius: 4px;
      color: #fff;
      padding: 6px 10px;
      line-height: 18px;
    }
    .osLoginRow {
      width: 330px;
      margin:0 auto 5px;
      line-height: 16px;
      height: 16px;
    }
    .fl{ float:left; }
    .fr{ float:right !important; }
    .f_desc{color:#fff;font-size:14px;position:absolute;right:5%;top:160px;width:392px}
    .f_desc p.last{float:right;margin-top:20px;margin-right:6px }
     div.bottom{width:394px;position:absolute;left:50%;bottom:24%;color:#fff}
     div.bottom p.end{margin-top:-8px}
    p.last{position:absolute;bottom:9%;left:50%;width:657px;color:#fff;font-size:16px}
    .logo{width:500px;position:absolute;left:50%;top:20%}
     .logo img{width:60%;height:60%}
    /*footer start*/
    .footer{width:100%;background:#F7F7F7;font-size:15px;border-top:1px solid #B4B5B5;border:2px solid red}
    .footer .f_con{width:1180px;float:right}
    .footer .f_con .f_desc{width:600px;float:left;margin-top:34px}
    .footer .f_con .f_desc p{line-height:20px;font-size:20px}
    .footer .f_con .f_img{width:370px;float:left;margin-top:10px}
    .footer .f_con .f_img .f_service{width:130px;float:left}
    .footer .f_con .f_img .f_service img{width:100%;height:100%}
    .footer .f_con .f_img .f_service span{line-height:30px;display:block;height:30px;text-align:center}
    .footer .f_con .f_img .f_sub{width:130px;float:left;margin-left:20px}
    .footer .f_con .f_img .f_sub img{width:100%;height:100%}
    .footer .f_con .f_img .f_sub span{line-height:30px;display:block;height:30px;text-align:center}
    #login-form .state-error{border-color: #F1B283;background-color: #FFE292;color: #DE0000;border: 1px solid #fb7272;
         box-shadow: 1px 1px 1px #fb7272 inset, -1px -1px 1px #fb7272 inset;}
    #login-form .state-error input{background:none !important}
    #login-form .state-error + em{margin-bottom:8px !important;margin-top:-12px !important;margin-left:5px !important}
    .smart-form .state-success input{background:none}
    .smart-form .state-success input:focus{border:1px solid #66afe9}

    /*end footer*/
    .btn-primary {
        color: #fff;
        background-color: #0066C8;
    }
    .top{
        padding:20px 0;
        background: #fff;
    }

    .top .t_con{
        text-align:center;
        width:800px;
        margin:0 auto;
        font-size:45px;
        color:#036EB8;
		font-weight:700
    }
    /*
    @media screen and (max-width:954px){

      .f_desc {width:300px}

    }
     @media screen and (max-width:776px){

          .f_desc {width:200px}

        }
     @media screen and (max-width:670px){

          .f_desc {display:none}

        }
        */
@media screen and (max-height:590px){
                p.last {display:none}

              }

            @media screen and (max-height:834px){
                     p.last{bottom:7%}

            }

              @media screen and (max-height:768px){
                                 p.last{bottom:5%}

                        }
</style>
</head>
<body id="login" class="animated fadeInDown" style="min-height:100px;" onscroll="no">
<div class="top">
    <div class="t_con">上海市卫生产业开发中心-协同运营平台</div>
    <%--<div class="t_con">Shanghai health industry development center platform</div>--%>
</div>
<div id="main" role="main" style="">
<div class="oWindowBody">

<div class="osLoginWrap">
    <!--
    <div class="osLoginHeader">
        <div class="osLoginAvatar">
            <img class="loginAvatar"  src="../resource/com/img/osLoginAvatar.png" alt="">
        </div>
    </div>
    -->
    <div class="osLoginInner">
        <h3 class="osLoginEName loginName"></h3>

        <div style="width:450px;margin:0 auto">
         <form action="login.action" id="login-form" method="post" class="smart-form client-form">
            <!--
                <div>

                             <label class="label col col-2">手机号 </label>
                             <section class="col col-10">
                                 <label class="input"> <i class="icon-append fa fa-user"></i>
                                     <input type="text" name="username" placeholder="请输入手机号"
                                            value="<s:property value="username"/>">
                                     <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i>
                                         请输入手机号</b></label>
                             </section>
                               <div class="clear"></div>
                         </div>
                         <div>
                             <label class="label col col-2">密&nbsp&nbsp&nbsp码</label>
                             <section class="col col-10">
                                 <label class="input"> <i class="icon-append fa fa-lock"></i>
                                     <input type="password" name="password" placeholder="请输入您的密码">
                                     <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i>
                                         请输入您的密码</b> </label>

                                 <div class="note">

                                 </div>

                             </section>
                              <div class="clear"></div>
                         </div>

          -->

        <div class="osLoginIptBox osLoginIco01">

            <input value="<s:property value="username"/>" placeholder="请输入手机号" name="username" type="text" spellcheck="false" class="osLoginIpt" maxlength="50" tabindex="1">

        </div>



        <div class="osLoginIptBox osLoginIco02">
            <input type="password" name="password" placeholder="请输入您的密码" class="osLoginIpt passwordField" tabindex="2" maxlength="14">

        </div>

                                 <div style="margin-top:-5px">

                                             <s:if test="error != null && error != ''">
                                                 <div class="alert alert-danger fade in">
                                                     <button class="close" data-dismiss="alert">×</button>
                                                     <i class="fa-fw fa fa-warning"></i>
                                                     <strong style="padding-top:-10px">
                                                     <s:property value="error"/>
                                                     </strong>
                                                 </div>
                                             </s:if>


                                        <div class="clear"></div>
                                    </div>
        </form>
            <div class="osLoginIptBtn okBtn">登   录</div>
        </div>

        <%--
        <div class="osLoginRow">
            <div class="fr">
                <a  href="javascript:;" id="experience_btn-Register" style="margin-right:11px">体验注册</a>
                <a  href="javascript:;" id="btn-Register" style="margin-right:11px">注 册</a>
                <!--
                <span> | </span>
                <a id="findPasswd" href="javascript:;">忘记密码</a>
                -->
            </div>

        </div>
        --%>
    </div>

</div>
            <%--
<div class="bottom">
                         <p>"工程泛联客"由上海玖达信息技术有限公司与2015年自主研发</p>
                                    	     <p class="end">的针对工程建设领域项目现场管理所定制的移动终端服务软件。</p>


                    </div>
                    --%>
                  <p class="last">&copy;Copyright 2014-2015 上海玖达信息技术有限公司/上海泛在企业管理有限公司</p>
</div>

</div>
</body>

    <!--<span id="logo"></span>-->
    <!-- MAIN CONTENT -->


<!--
<div class="logo">
    <img src="../resource/com/img/logo_cst.png" />
</div>
-->


<%--<img src="../resource/com/img/loginbg.png" style="max-width: 100%">--%>





<!--
<div class="footer">
	<div class="f_con">
		<div class="f_img">
			<div class="f_service">
				<img src="../resource/com/img/image-32.png" alt="泛销服务号">
				<span>泛销服务号</span>
			</div>
			<div class="f_sub">
				<img src="../resource/com/img/image-33.png" alt="泛在订阅号">
				<span>泛在订阅号</span>
			</div>
			<div class="clear"></div>
		</div>

        <div class="clear"></div>
	</div>
<div class="clear"></div>
</div>
-->
</body>

<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel" style="font-family:微软雅黑">
                    企业账号注册（个人用户无需在此注册，请在企业后台由管理员进行注册）
                </h4>
            </div>
            <div class="modal-body">
                <form action="#" method="post" id="smart-form-register" class="smart-form client-form">

                    <fieldset>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-phone"></i>
                                <input type="text" id="username" name="username" placeholder="请输入手机号">
                                <b class="tooltip tooltip-bottom-right">输入正确的手机号</b> </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                <input type="password" name="password" placeholder="请输入密码" id="password">
                                <b class="tooltip tooltip-bottom-right">不要忘记您的密码</b> </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                <input type="password" name="passwordConfirm" placeholder="请再输入一次密码">
                                <b class="tooltip tooltip-bottom-right">不要忘记您的密码</b> </label>
                        </section>
                    </fieldset>
                    <fieldset>
                        <div class="row">
                            <section class="col col-8">
                                <label class="input"><i class="icon-append fa fa-unlock-alt"></i>
                                    <input type="text" name="authcode" placeholder="请输入验证码">
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="input">
                                    <button type="button" id="register_page_send" class="btn btn-warning btn-lg" style="font-family:微软雅黑;font-size:14px">
                                        获取验证码
                                    </button>
                                </label>
                            </section>
                            <section class="col col-12" id="message_reg_alert">

                            </section>
                        </div>
                    </fieldset>
                    <footer>
                        <input type="hidden" name="terms" id="terms" value="1">
                        <button type="button" id="register_page_submit" class="btn btn-primary btn-lg"  style="font-family:微软雅黑;">
                            注册 <i class="fa fa-sign-in fa-lg"></i>
                        </button>
                    </footer>

                </form>
            </div>
            <%--<div class="modal-footer">--%>
            <%--<button type="button" class="btn btn-default"--%>
            <%--data-dismiss="modal">关闭--%>
            <%--</button>--%>
            <%--<button type="button" class="btn btn-primary">--%>
            <%--提交--%>
            <%--</button>--%>
            <%--</div>--%>
        </div>
        <!-- /.modal-content -->
    </div>

    </div>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog"
     aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="ModalLabel" style="font-family:微软雅黑">
                    体验账号注册
                </h4>
            </div>
            <div class="modal-body">
                <form action="#" method="post" id="cst-form-register" class="smart-form client-form">

                    <fieldset>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-phone"></i>
                                <input type="text" id="experience_username" name="username" placeholder="请输入手机号">
                                <b class="tooltip tooltip-bottom-right">输入正确的手机号</b> </label>
                        </section>
                    </fieldset>
                    <fieldset>
                        <div class="row">
                            <section class="col col-8">
                                <label class="input"><i class="icon-append fa fa-unlock-alt"></i>
                                    <input type="text" id="experience_authcode" name="authcode" placeholder="请输入验证码">
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="input">
                                    <button type="button" id="experience_register_page_send" class="btn btn-warning btn-lg" style="font-family:微软雅黑;font-size:14px">
                                        获取验证码
                                    </button>
                                </label>
                            </section>
                            <section class="col col-12" id="experience_message_reg_alert">

                            </section>
                        </div>
                    </fieldset>
                    <footer>
                        <button type="button" id="experience_register_page_submit" class="btn btn-primary btn-lg"  style="font-family:微软雅黑;">
                            线上体验 <i class="fa fa-sign-in fa-lg"></i>
                        </button>
                    </footer>

                </form>
            </div>
            <%--<div class="modal-footer">--%>
            <%--<button type="button" class="btn btn-default"--%>
            <%--data-dismiss="modal">关闭--%>
            <%--</button>--%>
            <%--<button type="button" class="btn btn-primary">--%>
            <%--提交--%>
            <%--</button>--%>
            <%--</div>--%>
        </div>
        <!-- /.modal-content -->
    </div>

</div>
    <!-- /.modal -->

    <!--================================================== -->

    <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
    <script src="../resource/com/js/plugin/pace/pace.min.js"></script>

    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
    <script> document.write('<script src="../resource/com/js/libs/jquery-2.0.2.min.js"><\/script>'); </script>
    <!--
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>-->
    <script> document.write('<script src="../resource/com/js/libs/jquery-ui-1.10.3.min.js"><\/script>'); </script>

    <!-- JS TOUCH : include this plugin for mobile drag / drop touch events
    <script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

    <!-- BOOTSTRAP JS -->
    <script src="../resource/com/js/bootstrap/bootstrap.min.js"></script>

    <!-- CUSTOM NOTIFICATION -->
    <script src="../resource/com/js/notification/SmartNotification.min.js"></script>

    <!-- JARVIS WIDGETS -->
    <script src="../resource/com/js/smartwidgets/jarvis.widget.min.js"></script>

    <!-- EASY PIE CHARTS -->
    <script src="../resource/com/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

    <!-- SPARKLINES -->
    <script src="../resource/com/js/plugin/sparkline/jquery.sparkline.min.js"></script>

    <!-- JQUERY VALIDATE -->
    <script src="../resource/com/js/plugin/jquery-validate/jquery.validate.min.js"></script>

    <!-- JQUERY MASKED INPUT -->
    <script src="../resource/com/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

    <!-- JQUERY SELECT2 INPUT -->
    <script src="../resource/com/js/plugin/select2/select2.min.js"></script>

    <!-- JQUERY UI + Bootstrap Slider -->
    <script src="../resource/com/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

    <!-- browser msie issue fix -->
    <script src="../resource/com/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

    <!-- SmartClick: For mobile devices -->
    <script src="../resource/com/js/plugin/smartclick/smartclick.js"></script>

    <!--[if IE 7]>

    <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

    <![endif]-->

    <!-- MAIN APP JS FILE -->


    <script type="text/javascript">
        //        $(function(){
        //            var height = $(document.body).height();
        //            var width = $(document.body).width();
        //            $("#computer").width(width*0.35);
        //
        //            $("#computer").height(height*0.35);
        //        });
function login_load(){
	var width = $('#main').width();
	var width2 = $('.osLoginWrap').width();

    var height = $(document).height();
    var height2 = $('.osLoginWrap').height();
    var str  = (height-height2)/3;
    $('.osLoginWrap').css('top', str);

    var width3 = $('div.bottom').width();
    var width4 = $(document.body).width();
    var width5 = $('p.last').width();
    var logo_with = $('.logo img').width();
    var logo_height = $('.logo img').height();

   // alert(width4);
    /*
    if(width4 <=1360){
        if(width4 > 1110){
                $('.osLoginWrap').css('left', (width-width2)/3);
         }else{
                if(width4>840){
                    $('.osLoginWrap').css('left', (width-width2)/6);
                }else{
                    $('.osLoginWrap').css('left',10);
                }
         }
   }else{
        $('.osLoginWrap').css('left', (width-width2)/2);
    }*/

    $('.osLoginWrap').css('left', (width-width2)/10*9);
    $('div.bottom').css('left',(width - width3)/2);
    $('p.last').css('left',(width - width5)/2);
    if(height>= 768){
         $('div.bottom').css('bottom', str*0.6);
    }else{
         $('div.bottom').css('bottom', str*0.9-70);
    }

    $('div.logo').css('left', (width - logo_with)/2);
    $('div.logo').css('top', (height - logo_height- height2)/2-100);
}
window.onresize= login_load;
        var countdown = 120;
        function settime(btn) {
            if (countdown == 0) {
                btn.attr("value", "获取验证码");
                btn.text("获取验证码");
                btn.attr("disabled", false);
                countdown = 120;
            } else {
                btn.attr("value", "重新发送(" + countdown + ")");
                btn.text("重新发送(" + countdown + ")");
                btn.attr("disabled", true);
                countdown--;

                setTimeout(function () {
                    if (countdown >= 0) {
                        settime(btn)
                    }
                }, 1000)
            }
        }

        function login() {
          // alert($("#username").val() + $("#password").val());
            var username = $("#smart-form-register input[name=username]").val();
            var password = $("#smart-form-register input[name=password]").val();
            $('#myModal').modal('hide');

            $.ajax({
                type: "POST",
                url: "login.action",
                cache: false, dataType: "text", async: true,
                data: {"username": encodeURIComponent(username), "password": password},
                success: function (data) {
                    location.replace("index.action");
                }
            })
        }

        function experiencelogin() {
            // alert($("#username").val() + $("#password").val());
            var username = $("#cst-form-register #experience_username").val();
            $('#Modal').modal('hide');

            $.ajax({
                type: "POST",
                url: "login.action",
                cache: false, dataType: "text", async: true,
                data: {"username":"ty"+username, "password": "pass"},
                success: function (data) {
                    location.replace("index.action");
                }
            })
        }

        $(function () {
            login_load();
            // Validation
            $("#login-form").validate({
                // Rules for form validation
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 1,
                        maxlength: 20
                    }
                },

                // Messages for form validation
                messages: {
                    username: {
                        required: '请输入您的手机号/邮件地址'
                    },
                    password: {
                        required: '请输入您的密码'
                    }
                },

                // Do not change code below
                errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                },
                submitHandler: function(form){   //表单提交句柄,为一回调函数，带一个参数：form
                    //alert($("#login-form input[name=username]").val())
                    form.submit();   //提交表单
                }
            });

            $("#btn-Register").unbind("click").bind("click", function () {
                $("#smart-form-register input[name=username]").val("");
                $("#smart-form-register input[name=password]").val("");
                $("#smart-form-register input[name=passwordConfirm]").val("");
                $("#smart-form-register input[name=authcode]").val("");
                $('#myModal').modal("show").css({"margin-top": "100px"});
            });

            $("#experience_btn-Register").unbind("click").bind("click", function () {
                $("#cst-form-register #experience_username").val("");
                $("#cst-form-register #experience_authcode").val("");
                $('#Modal').modal("show").css({"margin-top": "100px"});
            });


            // Validation
            $("#smart-form-register").validate({
                // Rules for form validation
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 3,
                        maxlength: 20
                    },
                    passwordConfirm: {
                        required: true,
                        minlength: 3,
                        maxlength: 20,
                        equalTo: '#password'
                    }
                },

                // Messages for form validation
                messages: {
                    username: {
                        required: '请输入唯一用户名'
                    },
                    password: {
                        required: '请输入您的密码',
                        minlength:'密码长度至少要3位',
                        maxlength:'密码长度最多要20位'
                    },
                    passwordConfirm: {
                        required: '请再输入一次密码',
                        equalTo: '两次密码输入应一致',
                        minlength:'密码长度至少要3位',
                        maxlength:'密码长度最多要20位'
                    }
                },

                // Ajax form submition
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        success: function () {
                            $("#smart-form-register").addClass('submited');
                        }
                    });
                },

                // Do not change code below
                errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                }
            });
            $("#cst-form-register").validate({
                // Rules for form validation
                rules: {
                    username: {
                        required: true
                    },
                    authcode: {
                        required: true
                    }
                },

                // Messages for form validation
                messages: {
                    username: {
                        required: '请输入唯一用户名'
                    },
                    authcode: {
                        required: '请输入验证码'
                    }
                },

                // Ajax form submition
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        success: function () {
                            $("#cst-form-register").addClass('submited');
                        }
                    });
                },

                // Do not change code below
                errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                }
            });
            $('.okBtn').click(function(){
                $('#login-form').submit();

            });
            $('body').bind('keyup', function(event) {

                if (event.keyCode == "13") {
                    //回车执行查询
                    $('#login-form').submit();
                }
             });
            $("#register_page_send").click(function (e) {
                $("#message_reg_alert").empty();
                $("#message_reg_alert").html("正在发送...");
                $.ajax({
                    type: "POST",
                    url: "register!sendAuthCode.action",
                    cache: false, dataType: "json", async: true,
                    data: $("form#smart-form-register").serialize(),
                    success: function (data) {

                        $("#message_reg_alert").empty();

                        if (data.result.errorCode == 1 || data.result.errorCode == "1") {
                            //alert(data.result.errorMessage);
                            settime($("#register_page_send"));
                            $('<div class="alert alert-warning fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-warning"></i><strong>提醒！</strong> ' + data.result.errorMessage + '</div>').appendTo($("#message_reg_alert"));

                        } else {
                            $('<div class="alert alert-danger fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-times"></i><strong>错误！</strong> ' + data.result.errorMessage + '</div>').appendTo($("#message_reg_alert"));
                        }
                    }
                });
            });
            //体验短信
            $("#experience_register_page_send").click(function (e) {
                $("#experience_message_reg_alert").empty();
                $("#experience_message_reg_alert").html("正在发送...");
                $.ajax({
                    type: "POST",
                    url: "register!sendexperienceAuthCode.action",
                    cache: false, dataType: "json", async: true,
                    data: $("form#cst-form-register").serialize(),
                    success: function (data) {

                        $("#experience_message_reg_alert").empty();

                        if (data.result.errorCode == 1 || data.result.errorCode == "1") {
                            //alert(data.result.errorMessage);
                            settime($("#experience_register_page_send"));
                            $('<div class="alert alert-warning fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-warning"></i><strong>提醒！</strong> ' + data.result.errorMessage + '</div>').appendTo($("#experience_message_reg_alert"));

                        } else {
                            $('<div class="alert alert-danger fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-times"></i><strong>错误！</strong> ' + data.result.errorMessage + '</div>').appendTo($("#experience_message_reg_alert"));
                        }
                    }
                });
            });

            $("#register_page_submit").click(function (e) {
                var $valid = $("#smart-form-register").valid();
                if (!$valid) {
                    return false;
                }
                $("#message_reg_alert").empty();
                $("#message_reg_alert").html("正在注册...");
                $.ajax({
                    type: "POST",
                    url: "register!ajaxRegister.action",
                    cache: false, dataType: "json", async: true,
                    data: $("form#smart-form-register").serialize(),
                    success: function (data) {
                        $("#message_reg_alert").empty();

                        if (data.result.errorCode == 1 || data.result.errorCode == "1") {
                            $('<div class="alert alert-warning fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-warning"></i><strong>提醒！</strong> ' + '正在登录，请稍等...' + '</div>').appendTo($("#message_reg_alert"));
                            login();
                        } else {
                            $('<div class="alert alert-danger fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-times"></i><strong>错误！</strong> ' + data.result.errorMessage + '</div>').appendTo($("#message_reg_alert"));
                        }
                    }
                });
            });

            $("#experience_register_page_submit").click(function (e) {
                var $valid = $("#cst-form-register").valid();
                if (!$valid) {
                    return false;
                }
                $("#experience_message_reg_alert").empty();
                $("#experience_message_reg_alert").html("正在注册...");
                $.ajax({
                    type: "POST",
                    url: "register!experienceRegister.action",
                    cache: false, dataType: "json", async: true,
                    data: $("form#cst-form-register").serialize(),
                    success: function (data) {
                        $("#experience_message_reg_alert").empty();

                        if (data.result.errorCode == 1 || data.result.errorCode == "1") {
                            $('<div class="alert alert-warning fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-warning"></i><strong>提醒！</strong> ' + '正在登录，请稍等...' + '</div>').appendTo($("#experience_message_reg_alert"));
                            experiencelogin();
                        } else {
                            $('<div class="alert alert-danger fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-times"></i><strong>错误！</strong> ' + data.result.errorMessage + '</div>').appendTo($("#experience_message_reg_alert"));
                        }
                    }
                });
            });


        });
    </script>
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "//hm.baidu.com/hm.js?036b704c8a212107220e412a13d760a2";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    </body>
    </html>
