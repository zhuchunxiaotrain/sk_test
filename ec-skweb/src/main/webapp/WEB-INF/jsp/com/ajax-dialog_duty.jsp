<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12" id="ajax_duty_list_row">
            <table id="ajax_duty_list_table">
            </table>
            <div id="ajax_duty_list_page">
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <span class="btn btn-primary" id="dialog-ok">确定</span>
    <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>
    function run_jqgrid_function(){
        jQuery("#ajax_duty_list_table").jqGrid({
            url:'../com/ajax-duty!list.action',
            mtype:"POST",
            datatype: 'json',
            page : 1,
            colNames:['职员名称','职员岗位','职员部门','departId','Id'],
            colModel : [
                {name:'user',index:'user', width:150,sortable:false,search:true,sorttype:'string'},
                {name:'post',index:'post', width:150,sortable:false,search:true,sorttype:'string'},
                {name:'depart',index:'depart', width:150,sortable:false,search:true,sorttype:'string'},
                {name:'departId',index:'departId',width:10,key:false,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true},
                {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
            ],
            rowNum : 10,
            rowList:[10,20,30],
            pager : '#ajax_duty_list_page',
            sortname : 'createDate',
            sortorder : "desc",
            gridComplete:function(){
                    var ids=$("#ajax_duty_list_table").jqGrid('getDataIDs');
                    var param="<s:property value="param"/>"
                    if(param=="moneymanager"){
                        var dutysId=$("#dutyId").val();
                    }
                    for(var i=0;i<ids.length;i++) {
                        var cl = ids[i];
                        if (dutysId != "" && dutysId != null) {
                            var idsArr = dutysId.split(",");
                            for (var s = 0; s < idsArr.length; s++) {
                                if (idsArr[s] == cl) {
                                    $("#ajax_duty_list_table").jqGrid("setSelection", cl);
                                }
                            }
                        }
                    }
                jqGridStyle();
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
            },
            onSelectRow: function (rowId, status, e) {
                /*var rowId = $("#ajax_duty_list_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#ajax_duty_list_table").jqGrid('getRowData', rowId);
                $("#dutyId").val(rowId);
                $("#departmentName").val(rowDatas["depart"]);
                var departId=rowDatas["departId"];
                var actionUrl="<%=path %>/com/ajax-dict!count.action";
                var data={departId : departId};
               ajax_action(actionUrl,data,null,function (pdata) {
                   
               })*/
/*
                console.log("departId="+rowDatas["departId"]);
                var departIdArr=$("#ajax_duty_list_table").jqGrid("getCol",'departId',false);
                var allDatas=$("#ajax_duty_list_table").jqGrid('getRowData');
                console.log("allLength"+allDatas.length);
               
                var count=0;
                console.log("departIdLength="+departIdArr.length);
                for(var i=0;i<departIdArr.length;i++){
                        console.log("departIdAr_"+i+"="+departIdArr[i]);
                        console.log(typeof departIdArr[i]);
                        console.log(typeof departId);
                        if(departIdArr[i] == departId){
                            count++;
                                alert(true);
                        }
                }*/
            },
            /*
            onSelectAll:function(aRowids, status, e){
                $(this).gridselect('morecheck',{
                    table:"ajax_duty_list_table",
                    rowId:aRowids,
                    status:status,
                    id:"dutyId",
                    name:"dutyName",
                });
            },*/
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },

            multiselect: true,
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            forceFit:true,
            viewrecords: true,
            autowidth: true,
            height : 'auto',
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_duty_list_table").jqGrid('setGridWidth', $("#ajax_duty_list_row").width()-10);
        })

        jQuery("#ajax_duty_list_table").jqGrid('navGrid', "#ajax_duty_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });

        jQuery("#ajax_duty_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
    }

    $(function(){
        run_jqgrid_function();
    });
    $("#dialog-ok").unbind("click").bind("click",function(){
        gDialog.fClose();
    });
    /*function getCol() {
        var depart=$("#ajax_duty_list_table");
        var departIdArr=depart.getCo
    }*/
</script>
