<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<input type="hidden" id="dictName" value="<s:property value="dictName"/>"/>
<input type="hidden" id="index" value="<s:property value="index"/>"/>
<div class="modal-body">
    <div class="row">
        <input type="hidden" id="dictName" value="<s:property value="dictName"/>"/>
        <div class="col-md-12" id="ajax_dict_list_row">
            <table id="ajax_dict_list_table">
            </table>
            <div id="ajax_dict_list_page">
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <span class="btn btn-primary" id="dialog-ok">确定</span>
    <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>
    function run_jqgrid_function(){
        jQuery("#ajax_dict_list_table").jqGrid({
            url:'../com/ajax-dict!listtreecopy.action?dictName=<s:property value="dictName"/>',
            mtype:"POST",
            datatype: 'json',
            page : 1,
            colNames:['活动类型','Id'],
            colModel : [
                {name:'name',index:'name', width:240,sortable:false,search:true,sorttype:'string'},
                {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
            ],
            rowNum : 10,
            rowList:[10,20,30],
            pager : '#ajax_dict_list_page',
            sortname : 'createDate',
            sortorder : "desc",
            gridComplete:function(){

                var ids=$("#ajax_dict_list_table").jqGrid('getDataIDs');
                var dictsId;
                var param="<s:property value="dictName"/>"
                var index=$("#index").val();
                //alert("index_1 = "+index);
                if(param=="ActivityType"){
                    dictsId=$("#activityTypeId").val();
                }else if(param=="ActivityBranch"){
                    dictsId=$("#activityBranchId").val();
                }else if(param=="AnnualBudgetItem"){
                    dictsId=$("#annualBudgetItemId_"+index).val();
                }else if(param=="LetterType"){
                    dictsId=$("#letterTypeId").val();
                }else if(param=="PostCategory"){
                    dictsId=$("#postCategoryId").val();
                }
                    for(var i=0;i<ids.length;i++) {
                        var cl = ids[i];
                        if (dictsId != "" && dictsId != null) {
                            var idsArr = dictsId.split(",");
                            for (var s = 0; s < idsArr.length; s++) {
                                if (idsArr[s] == cl) {
                                    $("#ajax_dict_list_table").jqGrid("setSelection", cl);
                                }
                            }
                        }
                    }
                jqGridStyle();
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
            },
            onSelectRow: function (rowId, status, e) {
                var dictId="";/*两个页面传递id的中间值*/
                var dictName="";/*请求页面和返回页面之间的传递参数的中间值*/
                var flag=false;
                var param="<s:property value="dictName"/>";
                var index="<s:property value="index"/>";
                //alert("index_2 = "+index);
                if(param=="ActivityType"){
                    dictId="activityTypeId";
                    dictName="activityTypeName";
                }else if(param=="ActivityBranch"){
                    dictId="activityBranchId";
                    dictName="activityBranchName";
                    flag=true;
                }else if(param=="AnnualBudgetItem"){
                    dictId="annualBudgetItemId_"+index;
                    dictName="annualBudgetItemName_"+index;
                    flag=true;
                }else if(param=="LetterType"){
                    dictId="letterTypeId";
                    dictName="letterTypeName";
                    flag=true;
                }else if(param=="PostCategory"){
                    dictId="postCategoryId";
                    dictName="postCategoryName";
                    flag=true;
                }

                if(flag==true){
                    $(this).gridselect('onecheck',{
                        table:"ajax_dict_list_table",
                        rowId:rowId,
                        status:status,
                        id:dictId,/*dictId是中间值*/
                        name:dictName,
                        type:"radio"/*单选按钮*/
                    });
                }else {
                    $(this).gridselect('onecheck',{
                        table:"ajax_dict_list_table",
                        rowId:rowId,
                        status:status,
                        id:dictId,
                        name:dictName,
                        type:"checkbox"/*多选按钮*/
                    });
                }

            },
            /*onSelectAll:function(aRowids, status, e){
                alert("只能选择一个活动类型");
                return false;
                /!*$(this).gridselect('morecheck',{
                    table:"ajax_dict_list_table",
                    rowId:aRowids,
                    status:status,
                    id:"dictId",
                    name:"dictName",
                });*!/
            },*/
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },

           multiselect: true,
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            forceFit:true,
            viewrecords: true,
            autowidth: true,
            height : 'auto',
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_dict_list_table").jqGrid('setGridWidth', $("#ajax_dict_list_row").width()-10);
        })

        jQuery("#ajax_dict_list_table").jqGrid('navGrid', "#ajax_dict_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });

        jQuery("#ajax_dict_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
    }

    $(function(){
        run_jqgrid_function();
    });
    $("#dialog-ok").unbind("click").bind("click",function(){
        gDialog.fClose();
    });
</script>
