<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="/struts-tags" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String jsessionid=session.getId();
%>
<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-sm-12 col-md-12 col-lg-12">
        <div class="jarviswidget well" id="wid-id-3"
             data-widget-colorbutton="false"
             data-widget-editbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-custombutton="false"
             data-widget-sortable="false">
            <!-- widget div-->
            <div>
                <div class="widget-body">
                    <a class="btn btn-default" id="btn-interview-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>面试登记</a>
                    <s:if test="interview==null || interview.getProcessState().name()=='Draft'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
                    </s:if>
                    <s:if test="interview!=null && interview.getProcessState().name()=='Backed'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
                    </s:if>
                    <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
                    <hr class="simple">
                    <form id="interview" class="smart-form" novalidate="novalidate" action="" method="post">
                        <input type="hidden" name="keyId" id="keyId" value="<s:property value="interview.id" />"/>
                        <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                        <input type="hidden"  name="parentId" id="parentId" value="<s:property value="parentId"/>" />
                        <header  style="display: block;">
                            面试登记&nbsp;&nbsp;<span id="title"></span>
                        </header>
                        <fieldset>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-depart"> 应聘部门</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="interviewDepart" name="interviewDepart" placeholder="请选择招聘部门"
                                                   value="<s:property value="interview.department.name"/>"/>
                                            <input type="hidden" id="interviewDepartId" name="interviewDepartId"
                                                   value="<s:property value="interview.department.id"/>"/>
                                        </label>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    应聘岗位
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="postName" id="postName" value="<s:property value="post.name"/>" >
                                        <input type="hidden"  name="postId" id="postId" value="<s:property value="post.id"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    姓名
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="name" id="name" placeholder="请输入应聘者姓名" value="<s:property value="interview.name"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    身份证
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="cardID" id="cardID" placeholder="请输入身份证" value="<s:property value="interview.cardID"/>" >
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    面试日期
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input  placeholder="请选择面试日期" id="interviewDate" name="interviewDate"
                                                type="text" value="<s:date name="interview.interviewDate" format="yyyy-MM-dd"/>">
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-interviewer"> 面试人</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">

                                            <s:if test="interview ==null">
                                                <input disabled type="text"  id="interviewer" placeholder="请选择面试人" value="<s:property value="loginUser.name"/>" >
                                                <input type="hidden" id="interviewerId" name="interviewerId" value="<s:property value="loginUser.id"/>"/></label>
                                             </s:if>

                                            <s:if test="interview!=null">
                                                <input disabled type="text" id="interviewer" value="<s:iterator id="list" value="interview.interviewer"><s:property value="#list.name"/>,</s:iterator>"/>
                                                <input type="hidden" id="interviewerId" name="interviewerId" value="<s:iterator id="list" value="interview.interviewer"><s:property value="#list.id"/>,</s:iterator>"/>
                                            </s:if>

                                           <%--<label class="input state-disabled">
                                                <input disabled type="text" name="interviewer" id="interviewer" placeholder="请选择面试人" value="<s:property value="interview.interviewer.name"/>" >
                                                <input type="hidden" id="interviewerId" name="interviewerId" value="<s:property value="interview.interviewer.id"/>"/>
                                            </label>--%>
                                        </label>

                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    面试记录
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input  name="uploadify" id="recordfilename" placeholder="" type="file" >
                                        <input name="recordfileId" id="recordfileId" style="display: none" value="<s:property value="recordfileId"/>">
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    面试结果
                                </label>
                                <section class="col col-5">
                                    <div class="inline-group" name="result">
                                        <label class="radio">
                                            <input type="radio" checked="checked" name="result" value="0" <s:property value="interview.result==0?'checked':''"/>>
                                            <i></i>推荐录用</label>
                                        <label class="radio">
                                            <input type="radio"  name="result" value="1" <s:property value="interview.result==1?'checked':''"/>>
                                            <i></i>进入下一轮</label>
                                        <label class="radio">
                                            <input type="radio"  name="result" value="2" <s:property value="interview.result==2?'checked':''"/>>
                                            <i></i>淘汰</label>
                                    </div>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    备注
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="interview.remark"/>" >
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    操作人
                                </label>
                                <section class="col col-4">
                                    <label class="input state-disabled">
                                        <input  disabled type="text" placeholder="" value="<s:property value="creater.name"/>" >
                                    </label>
                                </section>
                                <label class="label col col-2">
                                    操作日期
                                </label>
                                <section class="col col-4 ">
                                    <label class="input state-disabled">
                                        <input  disabled type="text" name="createDate" id=createDate"  value="<s:property value="createDate" />">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </article>
</div>

<script>
    var draft = "<s:property value="draft" />";
    //上传
    inputLoad({
        objId:"recordfilename",
        entityName:"recordId",
        sourceId:"recordfileId",
        jsessionid:"<%=jsessionid%>"
    });

    $(function(){
        var pdata= {
            keyId:$("#keyId").val(),
            flowName:"interview",
            todo:"1"
        };
        multiDuty(pdata);
    });

    $("a[key=btn-choose-interviewer]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择人员",
            url:"ajax-dialog!manageUser.action?key=btn-choose-interviewer",
            width:340
        }).show();
    });

    // 身份证号码验证
    jQuery.validator.addMethod("isIdCardNo", function(value, element) {
        return this.optional(element) || idCardNoUtil.checkIdCardNo(value);
    }, "请正确输入您的身份证号码");

    //校验
    $("#interview").validate({
        rules : {
            departId:{
                required : true
            },
            postId:{
                required : true
            },
            name:{
                required : true
            },
            interviewDate:{
                required:true
            },
            recordId:{
                required:true
            },
            cardID:{
                required:true,
                isIdCardNo:true
            },
        },
        messages : {
            departId: {
                required : '请输入应聘部门'
            },
            postId: {
                required : '请输入应聘岗位'
            },
            name : {
                required : '请输入姓名'
            },
            interviewDate:{
                required:"请输入面试时间"
            },
            recordId:{
                required:"请输入面试记录"
            },

            cardID:{
                required:"请输入身份证号",
                isIdCardNo:"请输入正确的身份证号"
            },
        },
        ignore: "",
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });

    var idCardNoUtil = {
        provinceAndCitys: {11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江",
            31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北",43:"湖南",44:"广东",
            45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",
            65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"},

        powers: ["7","9","10","5","8","4","2","1","6","3","7","9","10","5","8","4","2"],

        parityBit: ["1","0","X","9","8","7","6","5","4","3","2"],

        genders: {male:"男",female:"女"},

        checkAddressCode: function(addressCode){
            var check = /^[1-9]\d{5}$/.test(addressCode);
            if(!check) return false;
            if(idCardNoUtil.provinceAndCitys[parseInt(addressCode.substring(0,2))]){
                return true;
            }else{
                return false;
            }
        },

        checkBirthDayCode: function(birDayCode){
            var check = /^[1-9]\d{3}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))$/.test(birDayCode);
            if(!check) return false;
            var yyyy = parseInt(birDayCode.substring(0,4),10);
            var mm = parseInt(birDayCode.substring(4,6),10);
            var dd = parseInt(birDayCode.substring(6),10);
            var xdata = new Date(yyyy,mm-1,dd);
            if(xdata > new Date()){
                return false;//生日不能大于当前日期
            }else if ( ( xdata.getFullYear() == yyyy ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == dd ) ){
                return true;
            }else{
                return false;
            }
        },

        getParityBit: function(idCardNo){
            var id17 = idCardNo.substring(0,17);
            var power = 0;
            for(var i=0;i<17;i++){
                power += parseInt(id17.charAt(i),10) * parseInt(idCardNoUtil.powers[i]);
            }
            var mod = power % 11;
            return idCardNoUtil.parityBit[mod];
        },

        checkParityBit: function(idCardNo){
            var parityBit = idCardNo.charAt(17).toUpperCase();
            if(idCardNoUtil.getParityBit(idCardNo) == parityBit){
                return true;
            }else{
                return false;
            }
        },

        checkIdCardNo: function(idCardNo){
            //15位和18位身份证号码的基本校验
            var check = /^\d{15}|(\d{17}(\d|x|X))$/.test(idCardNo);
            if(!check) return false;

            //判断长度为15位或18位
            if(idCardNo.length==15){
                return idCardNoUtil.check15IdCardNo(idCardNo);
            }else if(idCardNo.length==18){
                return idCardNoUtil.check18IdCardNo(idCardNo);
            }else{
                return false;
            }
        },

        //校验15位的身份证号码
        check15IdCardNo: function(idCardNo){
            //15位身份证号码的基本校验
            var check = /^[1-9]\d{7}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}$/.test(idCardNo);
            if(!check) return false;
            //校验地址码
            var addressCode = idCardNo.substring(0,6);
            check = idCardNoUtil.checkAddressCode(addressCode);
            if(!check) return false;
            var birDayCode = '19' + idCardNo.substring(6,12);
            //校验日期码
            return idCardNoUtil.checkBirthDayCode(birDayCode);
        },

        //校验18位的身份证号码
        check18IdCardNo: function(idCardNo){
            //18位身份证号码的基本格式校验
            var check = /^[1-9]\d{5}[1-9]\d{3}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}(\d|x|X)$/.test(idCardNo);
            if(!check) return false;

            //校验地址码
            var addressCode = idCardNo.substring(0,6);
            check = idCardNoUtil.checkAddressCode(addressCode);
            if(!check) return false;

            //校验日期码
            var birDayCode = idCardNo.substring(6,14);
            check = idCardNoUtil.checkBirthDayCode(birDayCode);
            if(!check) return false;

            //验证校检码
            return idCardNoUtil.checkParityBit(idCardNo);
        },
        formateDateCN: function(day){
            var yyyy =day.substring(0,4);
            var mm = day.substring(4,6);
            var dd = day.substring(6);
            return yyyy + '-' + mm +'-' + dd;
        },
        //获取信息
        getIdCardInfo: function(idCardNo){
            var idCardInfo = {
                gender:"", //性别
                birthday:"" // 出生日期(yyyy-mm-dd)
            };
            if(idCardNo.length==15){
                var aday = '19' + idCardNo.substring(6,12);

                idCardInfo.birthday=idCardNoUtil.formateDateCN(aday);

                if(parseInt(idCardNo.charAt(14))%2==0){
                    idCardInfo.gender=idCardNoUtil.genders.female;
                }else{
                    idCardInfo.gender=idCardNoUtil.genders.male;
                }
            }else if(idCardNo.length==18){
                var aday = idCardNo.substring(6,14);

                idCardInfo.birthday = idCardNoUtil.formateDateCN(aday);

                if(parseInt(idCardNo.charAt(16))%2==0){
                    idCardInfo.gender = idCardNoUtil.genders.female;
                }else{
                    idCardInfo.gender = idCardNoUtil.genders.male;
                }
            }
            return idCardInfo;
        },

        getId15:function(idCardNo){
            if(idCardNo.length==15){
                return idCardNo;
            }else if(idCardNo.length==18){
                return idCardNo.substring(0,6) + idCardNo.substring(8,17);
            }else{
                return null;
            }
        },

        getId18: function(idCardNo){
            if(idCardNo.length==15){
                var id17 = idCardNo.substring(0,6) + '19' + idCardNo.substring(6);
                var parityBit = idCardNoUtil.getParityBit(id17);
                return id17 + parityBit;
            }else if(idCardNo.length==18){
                return idCardNo;
            }else{
                return null;
            }
        }
    };

    //招聘部门
    $("a[key=btn-choose-depart]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择招聘部门",
            url:"ajax-dialog!dept.action?key=interviewDepart",
            width:560
        }).show();
    });

    $('#interviewDate').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        startDate:new Date(),
        todayBtn: 'linked',
        language: 'zh-CN',
        minView:2
    });

    //保存
    $("#btn-save-common").click(

        function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("interview","ajax-interview!save.action",null,function (data) {

                if(data.state == "200" || data.state ==200) {
                    if(draft == 1){
                        location.href='index.action';
                    }else{
                        loadURL("ajax-interview.action?parentId="+$("input#parentId").val(),$('div#s2'));
                    }
                }
            });
        }

    );

    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#interview").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#interview").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("interview","ajax-interview!commit.action",null,function (data) {

                        if(data.state == "200" || data.state ==200) {
                            if(draft== 1){
                                location.href='index.action';
                            }else {
                                loadURL("ajax-interview.action?parentId=" + $("input#parentId").val(), $('div#s2'));
                            }
                        }

                    });
                }
            });
        }
    );

    //返回视图
    $("#btn-interview-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-interview.action?parentId="+$("#parentId").val(),$('div#s2'));
        }

    });

</script>