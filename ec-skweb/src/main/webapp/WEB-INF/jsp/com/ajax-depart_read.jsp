<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <form id="depart_read" class="smart-form" novalidate="novalidate" action="" method="post">
                <input type="hidden" name="keyId" id="keyId" value="<s:property value="depart.id" />">
                <input type="hidden" name="companyId" id="companyId" value="<s:property value="company.id" />">
                <s:if test="depart == null">
                    <header>
                        [新增部门]
                    </header>
                </s:if>
                <s:else>
                        [编辑部门]
                </s:else>
                <fieldset>
                    <div class="row">
                        <label class="label col col-3" >部门名称</label>
                        <section class="col col-9">
                            <label class="input">
                                <input name="name" id="name" placeholder="请输入部门名称"
                                type="text" value="<s:property value="depart.name" />">
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <label class="label col col-3" >上级部门</label>
                        <section class="col col-9">
                            <label class="input">
                                <input id="fid" name="fid"  placeholder="请选择部门"
                                       type="text" onfocus="showzTree()" value="<s:if test="depart.parent.state.toString() == 'Enable' "><s:property value="depart.parent.name" /></s:if>">
                            </label>
                            <input type="hidden" name="departmentid" id="parentId"/>
                            <div id="tree_menu" style="display:none;position:absolute;overflow:auto;background: #fff ;z-index: 99;height: 150px;border: 1px solid #ccc">
                                <ul id="pTree" class="ztree" style="margin-top:0; width:298px; height: 100px;"></ul>
                            </div>
                        </section>
                    </div>
                    <s:if test="depart==null">
                        <div class="row">
                            <label class="label col col-3" >部门标识</label>
                            <section class="col col-9">
                                <label class="input">
                                    <input name="pname" id="pname" placeholder="请输入部门标识"
                                           type="text" value="">
                                </label>
                            </section>
                        </div>
                    </s:if>
                    <s:else>
                        <div class="row">
                            <label class="label col col-3" >部门标识</label>
                            <section class="col col-9">
                                <label class="input state-disabled">
                                    <input disabled name="pname" id="pname" placeholder="请输入部门标识"
                                           type="text" value="<s:property value="depart.pname" />">
                                </label>
                            </section>
                        </div>
                    </s:else>
                    <div class="row">
                        <label class="label col col-3" >部门简介</label>
                        <section class="col col-9">
                            <label class="input">
                              <input name="description" id="description" placeholder="请输入部门描述信息"
                                type="text" value="<s:property value="depart.description" />">
                            </label>
                        </section>
                    </div>


                    <div class="row">
                        <label class="label col col-3" >下级部门</label>
                        <section class="col col-9">
                            <label class="select">
                                  <ul class="list-group">
                                    <s:iterator value="depart.children" id="chdList">
                                        <s:if test="#chdList.state.toString()  == 'Enable'">
                                            <li class="list-group-item">
                                                <s:property value="#chdList.name"/>
                                            </li>
                                        </s:if>
                                    </s:iterator>
                                  </ul>
                            </label>
                        </section>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
	<a href="#../com/ajax!depart.action" class="btn btn-primary" id="dialog-ok">确定</a>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>
    //校验
    $("#depart_read").validate({
        rules : {

            name:{
                required : true
            },
            pname:{
                required : true,
                remote:{                                          //验证标识是否存在
                    type:"POST",
                    url:"../com/ajax-depart!isUnique.action",             //servlet
                    data:{
                        keyId:"",
                        validatePro:"pname",
                        newValue:function(){return $("#pname").val();}
                    }
                }
            }
        },
        messages : {
            name:{
                required : '请输入部门名称'
            },
            pname:{
                required:'请输入部门标识',
                remote:jQuery.format("该部门已经存在")
            }
        },
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });

    $("#dialog-ok").unbind("click").bind("click",function(){
        var $valid = $("#depart_read").valid();
        if(!$valid) return false;
        var actionUrl = "<%=path%>/com/ajax-depart!save.action";
        var opt={dialogId:'dialog-depart_message'};
        form_save("depart_read",actionUrl,opt);
        jQuery("#ajax_depart_table").trigger("reloadGrid");
        gDialog.fClose();
    });

    $(function(){
        $("input#fid").treeSelect('init',{
            actionUrl:"../com/ajax-depart!zTree.action"
        });
    });
    function showzTree(){
        $("#tree_menu").slideDown();
        $("input#fid").treeSelect('show');
    }
</script>