<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/"; 
%>

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>项目信息</a>
          <s:if test="manageProInfo==null || manageProInfo.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="manageProInfo!=null && manageProInfo.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="manageProInfo" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="manageProInfo.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              项目基本信息&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  项目名称
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="name" id="name" placeholder="请输入项目名称" value="<s:property value="manageProInfo.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  项目编号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="projectNo" id="projectNo" placeholder="项目编号自动生成" value="<s:property value="manageProInfo.projectNo"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-client"> 建设单位</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="clientName"
                           value="<s:property value="manageProInfo.client.name"/>"/>
                    <input type="hidden" id="clientId" name="clientId"
                           value="<s:property value="manageProInfo.client.id"/>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  建设单位地址
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="buildAddress" id="buildAddress" placeholder="调用客户地址" value="<s:property value="manageProInfo.buildAddress"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-linkman"> 联系人</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="linkmanName"
                             value="<s:property value="manageProInfo.linkman.name"/>"/>
                      <input type="hidden" id="linkmanId" name="linkmanId"
                             value="<s:property value="manageProInfo.linkman.id"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  项目概况
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="projectInfo" id="projectInfo" placeholder="请输入项目信息描述" value="<s:property value="manageProInfo.projectInfo"/>" >
                  </label>
                </section>
              </div>
                <div class="row">
                    <label class="label col col-2">
                        <i class="fa fa-asterisk txt-color-red"></i>
                        计划投资额（万元）
                    </label>
                    <section class="col col-5">
                        <label class="input">
                            <input type="text" name="numInventMoney" id="numInventMoney" placeholder="请输入计划投资额（万元）" value="<s:property value="manageProInfo.numInventMoney"/>" >
                        </label>
                    </section>
                </div>
              <div class="row">
                <label class="label col col-2">
                  资金来源
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="moneyFrom" id="moneyFrom" placeholder="请输入资金来源信息" value="<s:property value="manageProInfo.moneyFrom"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  招标信息来源
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="bidFrom" id="bidFrom" placeholder="请输入招标来源信息" value="<s:property value="manageProInfo.bidFrom"/>" >
                  </label>
                </section>
              </div>
                <div class="row">
                    <label class="label col col-2">
                        <i class="fa fa-asterisk txt-color-red"></i>
                        <a  href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                    </label>
                    <section class="col col-5">
                        <label class="input">
                            <label class="input state-disabled">
                                <input disabled type="text" id="usersName" name="nextStepApproversName"
                                       value="<s:property value="manageProInfo.nextStepApproversName"/>"/>
                                <input type="hidden" id="usersId" name="nextStepApproversId"
                                       value="<s:property value="manageProInfo.nextStepApproversId"/>"/>
                            </label>
                        </label>
                    </section>
                </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="remark" id="remark" placeholder="请输入备注信息" value="<s:property value="manageProInfo.remark"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>
  $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"manageproinfo"
    };
    multiDuty(pdata);
  });

  //选择客户对话框
  $("a[key='btn-choose-client']").unbind("click").bind("click",function(){
    ajax_action("ajax-dialog!clientDlg.action",{keyIds:$("div.widget-body #clientId").val()},{},function(pdata){
      oDialog.open({
        type :"radio",
        title:"请选择客户",
        data:pdata.data.data,
        callback:function(){
          var obj =$("#group input:checked");
          if(obj.size()!=1){
            alert("请选择一位客户！");
            return false;
          }
          var name = getArrProp(obj,"key");
          var str = getArrProp(obj,"id").split("&");
          var id = str[0];
          var location = "";
          if (str.length > 1)
            location = str[1];
          $("div.widget-body #clientName").val(name);
          $("div.widget-body #clientId").val(id);
          $("div.widget-body #buildAddress").val(location);
        }
      });
    });
  });
  //选择对应客户的联系人
  $("a[key='btn-choose-linkman']").unbind("click").bind("click",function(){
    var clientId = $("#clientId").val();
    if(clientId.length<1){
      alert("请先选择建设单位！");
      return false;
    }
    ajax_action("ajax-dialog!linkmanDlg.action?clientId="+clientId,{keyIds:$("div.widget-body #linkmanId").val()},{},function(pdata){
      oDialog.open({
        type :"radio",
        title:"请选择联系人",
        data:pdata.data.data,
        callback:function(){
          var obj =$("#group input:checked");
          if(obj.size()!=1){
            alert("请选择一位联系人！");
            return false;
          }
          var key = getArrProp(obj,"key");
          var arr = key.split("*");
          var name = arr[0];
          var mobile = arr[1];
          var id = getArrProp(obj,"id");
          $("div.widget-body #linkmanName").val(name);
          $("div.widget-body #linkmanId").val(id);
          $("div.widget-body #linkmanMobile").val(mobile);
        }
      });
    });
  });

  $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
      gDialog.fCreate({
          title:"请选择人员",
          url:"ajax-dialog!user.action",
          width:340
      }).show();
  });

  //返回视图
  $("#btn-re-common").click(function(){
    loadURL("ajax-manageproinfo.action?viewtype=1",$('#content'));
  });
  //校验
  $("#manageProInfo").validate({
    rules : {
      name : {
        required : true
      },
      clientId:{
       // required : true
      },
      linkmanId:{
      //  required : true
      },
      numInventMoney:{
        required : true,
          number : true,
          min:0
      },
        nextStepApproversId:{
        required : true
      }
    },
    messages : {
      name : {
        required : '请输入项目名称'
      },
      clientId : {
        required : '请选择建设单位'
      },
      linkmanId : {
        required : '请选择联系人'
      },
        numInventMoney : {
            required : '请输入计划投资额（万元）',
            number :  '请输入数字',
            min : '请输入一个大于0的数字'
        },
        nextStepApproversId : {
            required : '请选择会签人'
        }
    },
    ignore: "",
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  $('.hasDatepicker').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  });

  //保存
  $("#btn-save-common").click(

        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("manageProInfo","ajax-manageproinfo!save.action");
          loadURL("ajax-manageproinfo.action?viewtype=1",$('#content'));
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#manageProInfo").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#manageProInfo").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("manageProInfo","ajax-manageproinfo!commit.action");
                loadURL("ajax-manageproinfo.action?viewtype=1",$('#content'));
              }
            });
          }
  );
</script>
