<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />

<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      员工生日一览
    </h1>
  </div>
</div>
<div id="business_data">
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_employees_list_row">
                    <table id="ajax_employees_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_employees_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function(){
    load_employees_jqGrid();
  });
  function load_employees_jqGrid(){
      jQuery("#ajax_employees_table").jqGrid({ 
          url:'ajax-birthday!list.action', 
          datatype: "json", 
          colNames:["月","工号","姓名","年龄","性别","岗位","部门","id"], 
          colModel:[ 
              {name:'mon',index:'mon', search:false,width:80}, 
              {name:'staffid',index:'staffid', search:false,width:80}, 
              {name:'name',index:'name', search:false,width:80}, 
              {name:'age',index:'age', search:false,width:80}, 
              {name:'sex',index:'sex', search:false,width:80}, 
              {name:'post',index:'post', search:false,width:80}, 
              {name:'dep',index:'dep', search:false,width:80}, 
              {name:'id',index:'id',search:false,hidden:true}, 
          ], 
          rowNum : 10, 
          rowList:[10,20,30], 
          pager : '#ajax_employees_list_page', 
          sortname : 'mon', 
          sortorder : "desc", 
          gridComplete:function(){ 
              $(".ui-jqgrid-bdiv").css("overflow-x","hidden"); 
              jqGridStyle(); 
          }, 
          jsonReader: { 
              root: "dataRows", 
              page: "page", 
              total: "total", 
              records: "records", 
              repeatitems : false 
          }, 
          caption : "<i class='fa fa-arrow-circle-right'></i> 员工生日一览", 
          multiselect : false, 
          rownumbers:true, 
          gridview:true, 
          shrinkToFit:true, 
          viewrecords: true, 
          autowidth: true, 
          height:'auto', 
          forceFit:true, 
          loadComplete: function() { 
          } });

      $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_employees_table").jqGrid('setGridWidth', $("#ajax_employees_list_row").width());
       })
       jQuery("#ajax_employees_table").jqGrid('filterToolbar',{
        searchOperators:false,
        stringResult:true});

    jQuery("#ajax_employees_table").jqGrid('navGrid', "#ajax_employees_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });
  };


</script>





















