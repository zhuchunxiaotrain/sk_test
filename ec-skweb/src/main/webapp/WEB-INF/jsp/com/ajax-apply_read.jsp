<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<input  type="hidden" name="numStatus" id="numStatus"  value="<s:property value="numStatus" />"/>
<div class="row" >
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i>员工转正
          </a>
          <!--
                <a href="javascript:void(0);" key="btn-comment">
                      <span style="margin-top: -13px !important;" class="btn btn-default pull-right">
                          <i class="fa-fw fa fa-comment-o"></i> 审批意见<span></span>
                      </span>
                </a>
                -->
          
          <div id="myTabContent1" class="tab-content padding-10 ">
            <div class="tab-pane fade in active " id="s1" style="margin: 10px;">
              <div class="row">
                <shiro:hasAnyRoles name="wechat">
                    <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(apply.id)"/> key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 编辑</a>
                </shiro:hasAnyRoles>
              </div>
          <hr class="simple">
          <form id="apply" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="apply.id" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId"/>"/>
            <header  style="display: block;">
              续约申请&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <section class="col col-5">
                  <label class="input">
                    <input type="hidden"  name="nature" id="nature"  value="<s:property value="apply.nature"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled id="name" name="name" type="text" value="<s:property value="apply.name"/>">

                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  部门
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="depName" id="depName"  value="<s:property value="apply.department.name"/>" >
                    <input type="hidden"  name="depId" id="depId" value="<s:property value="apply.department.id"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  岗位
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" name="postName" disabled id="postName"  value="<s:property value="apply.post.name"/>" >
                    <input type="hidden"  name="postId" id="postId" value="<s:property value="apply.post.id"/>" >

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同开始日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="startDate" name="startDate" disabled
                           type="text" value="<s:date name="apply.startDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>

                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同结束日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="endDate" name="endDate" disabled
                           type="text" value="<s:date name="apply.endDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  试用期开始日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  id="applyalStart" name="applyalStart" disabled
                            type="text" value="<s:date name="apply.trialStart" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  试用期结束日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  id="applyalEnd" name="applyalEnd" disabled
                            type="text" value="<s:date name="apply.trialEnd" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="usersName" name="nextStepApproversName"
                             value="<s:property value="apply.nextStepApproversName"/>"/>
                      <input type="hidden" id="usersId" name="nextStepApproversId"
                             value="<s:property value="apply.nextStepApproversId"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text" id="creater" placeholder="" value="<s:property value="apply.creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input disabled type="text" placeholder="操作时间" value="<s:date name="apply.createDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
             <div class="flow">
                <s:if test="apply.getProcessState().name()=='Running'">
                  <div class="f_title">审批意见</div>
                  <div class="chat-footer"style="margin-top:5px">
                    <div class="textarea-div">
                      <div class="typearea">
                        <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                      </div>
                    </div>
                    <span class="textarea-controls"></span>
                  </div>
                </s:if>
                <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
                <div class="f_content" style="display:none">
                  <div id="showFlow"></div>
                </div>
                <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
                <div class="f_content" style="display:none">
                  <div id="showNext"></div>
                </div>
              </div>

            </div>
      </div>
    </div>
 </div>
      </div>
    </article>
</div>

<script type="text/javascript">

 /* $('li.disabled').off("click").on("click",function(){
    return false;
  })*/

  /*$(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"apply"
    };
    multiDuty(pdata);
  });*/

  $('#startDate,#endDate,#applyalEnd,#applyalStart').datetimepicker({
      format: 'yyyy-mm-dd',
      weekStart: 1,
      autoclose: true,
      todayBtn: 'linked',
      language: 'zh-CN',
      minView:2
  });

 //返回视图
 $("#btn-re-common").click(function(){
     var index = "<s:property value="index" />";
     var todo = "<s:property value="todo" />";
     var remind = "<s:property value="remind" />";
     var record = "<s:property value="record" />";
     var draft = "<s:property value="draft" />";
     if(index==1){
         loadURL("ajax-index!page.action",$('#content'));
     }else if(todo==1){
         loadURL("ajax!toDoList.action",$('#content'));
     }else if(remind==1){
         loadURL("ajax!remindList.action",$('#content'));
     }else if(record==1){
         loadURL("ajax!taskRecordList.action?type=1",$('#content'));
     }else if(record==2){
         loadURL("ajax!taskRecordList.action?type=2",$('#content'));
     }else if(draft==1){
         loadURL("ajax!draftList.action",$('#content'));
     }else{
         loadURL("ajax-apply.action?viewtype=<s:property value="viewtype"/>",$('#content'));
     }

 });

 //编辑
 $("a[key=ajax_edit]").click(function(){
     var draft = "<s:property value="draft" />";
     loadURL("ajax-apply!input.action?keyId="+$("input#keyId").val()+"&draft="+draft+"&parentId="+$("#parentId").val(),$('#content'));
 });

  //左侧意见栏
 $(function(){
     loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=flow",$('#showFlow'));
     loadURL("ajax-running!workflow.action?bussinessId="+$("input#keyId").val()+"&type=next",$('#showNext'));
     ajax_action("ajax-config!operateType.action",{keyId: $("input#keyId").val()},null,function(pdata){
         var showDuty = false;
         var area = $("span.textarea-controls");
         $(pdata.data.datarows).each(function(i,v){
             var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
             $(area).append(str);
             if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                 showDuty = true;
             }
         });
         if(showDuty == true){
             var pdata = {
                 keyId: $("input#keyId").val(),
                 flowName: "apply"
             };
             multiDuty(pdata);
         }
     });
     var valueObj=$("input#numStatus").val();
     switch(valueObj){
         case "0":
             //保存后再提交
             $("#left_foot_btn_approve").off("click").on("click",function(){
                 form_save("apply","ajax-apply!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                     $("#btn-re-common").trigger("click");
                 })
             });
             break;
         case "1":
             //第一步审批
             $("#left_foot_btn_approve").off("click").on("click",function(){
                 form_save("apply","ajax-apply!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                     $("#btn-re-common").trigger("click");
                 });
             });
             break;
         case "2":
             //第二步审批
             $("#left_foot_btn_approve").off("click").on("click",function(){
                 form_save("apply","ajax-apply!approve2.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                     $("#btn-re-common").trigger("click");
                 })
             });
             break;
         case "3":
             //第三步审批
             $("#left_foot_btn_approve").off("click").on("click",function(){
                 form_save("apply","ajax-apply!approve3.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                     $("#btn-re-common").trigger("click");
                 })
             });
             break;
     }
     //退回
     $("#left_foot_btn_sendback").off("click").on("click",function(){
         var vActionUrl="ajax-apply!reject.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
         var data={keyId:$("input#keyId").val()};
         ajax_action(vActionUrl,data,{},function(pdata){
             _show(pdata);
             $("#btn-re-common").trigger("click");
         });
     });
     //否决
     $("#left_foot_btn_deny").off("click").on("click",function(){
         var vActionUrl="ajax-apply!deny.action?comment="+$("textarea#chat_textarea-expand").val()+"&curDutyId="+$('#curDutyId').val();
         var data={keyId:$("input#keyId").val()};
         ajax_action(vActionUrl,data,{},function(pdata){
             _show(pdata);
             $("#btn-re-common").trigger("click");
         });
     });
     //流程信息展开
     $('#flow,#next').click(function(){
         if($(this).hasClass("right")){
             $(this).removeClass("right").addClass("down");
             $(this).parent(".f_title").next("div.f_content").show();
         }else{
             $(this).removeClass("down").addClass("right");
             $(this).parent(".f_title").next("div.f_content").hide();
         }
     });
 });

</script>