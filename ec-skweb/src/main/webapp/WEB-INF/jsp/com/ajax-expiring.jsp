<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />

<input type="hidden" name="keyId" id="keyId" value="keyId"/>
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      即将到期员工信息
    </h1>
  </div>
</div>
<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <shiro:hasAnyRoles name="wechat">
            <a id="ajax_renew_btn_add"  class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 续约申请表</a>
            <a id="ajax_apply_btn_add"  class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 员工转正</a>
          </shiro:hasAnyRoles>
        </form>
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_expiring_list_row">
                    <table id="ajax_expiring_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_expiring_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script >
    function run_jqgrid_function(){
        jQuery("#ajax_expiring_table").jqGrid({
            url:'ajax-expiring!list.action',
            mtype:"POST",
            datatype: 'json',
            page : 1,
            colNames:["姓名","年龄","性别","岗位","部门","用工性质","id"],
            colModel:[
                {name:'name',index:'name', search:false,width:80},
                {name:'age',index:'age', search:false,width:80},
                {name:'gender',index:'gender', search:false,width:80},
                {name:'post',index:'post', search:false,width:80},
                {name:'dep',index:'dep', search:false,width:80},
                {name:'nature',index:'nature', search:false,width:80},
                {name:'id',index:'id',search:false,hidden:true},
            ],
            rowNum : 10,
            rowList:[10,20,30],
            pager : '#ajax_expiring_list_page',
            sortname : 'createDate',
            sortorder : "desc",
            gridComplete:function(){
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                jqGridStyle();
            },
            onSelectRow: function (rowId, status, e) {
                /*$(this).gridselect('onecheck',{
                    table:"ajax_expiring_table",
                    rowId:rowId,
                    status:status,
                    id:"usersId",
                    name:"usersName"
                });*/
                var slt =   $("#ajax_expiring_table").jqGrid('getGridParam','selarrrow');
                if(slt.length > 1){
                    alert("请选择一条数据");
                    return false;
                }

                var rowId = $("#ajax_expiring_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#ajax_expiring_table").jqGrid('getRowData', rowId);
                $("#keyId").val(rowId);

            },
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },

            multiselect: true,
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            forceFit:true,
            viewrecords: true,
            autowidth: true,
            height : 'auto',
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_expiring_table").jqGrid('setGridWidth', $("#ajax_expiring_list_row").width());
        })

        jQuery("#ajax_expiring_table").jqGrid('navGrid', "#ajax_expiring_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });

        jQuery("#ajax_expiring_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
    }

    $(function(){
        run_jqgrid_function();
    });
    $("#dialog-ok").unbind("click").bind("click",function(){

        gDialog.fClose();
    });

    function fn_expiring_read(id){
        loadURL("ajax-renew!read.action?keyId="+id,$("#content"));
    }

    //续签
    $("#ajax_renew_btn_add").off("click").on("click",function(){
        if($('#keyId').val()=="keyId") {
            alert("请先选择即将到期员工!")
        }else {

            $.ajax({
                type: "get",
                url: "ajax-renew!checkUser.action?parentId=" + $('#keyId').val(),
                cache: false,
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.ifPersonnel == "1") {
                        loadURL("ajax-renew!input.action?parentId=" + $('#keyId').val(), $('#content'));
                    } else {
                        alert("请对应部门的人事专员操作或只针对用工性质为试用!")
                    }

                }
            });
        }
    });

    //员工转正
    $("#ajax_apply_btn_add").off("click").on("click",function(){
        $.ajax({
                type: "get",
                url: "ajax-apply!checkUser.action?parentId="+$('#keyId').val(),
                cache: false,
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.ifPersonnel == "1") {
                        //form_save("manage","ajax-manage!update.action?keyId="+$("#keyId").val());
                        //loadURL("ajax-manage.action?viewtype=2",$('#content'));

                        form_save("expiring","ajax-apply!update.action?keyId="+$('#keyId').val(), $('#content'));
                        // loadURL("ajax-apply!input.action?parentId="+$('#keyId').val(), $('#content'));
                        loadURL("ajax-expiring.action",$('#content'));

                    } else {
                        alert("请对应部门的人事专员操作!")
                    }

                }
        });
    });
</script>





















