<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="selectedIds" value=""/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_levelUser_list_row">
      <table id="ajax_levelUser_list_table">
      </table>
      <div id="ajax_levelUser_list_page">
      </div>
        <table id="ajax_user_list_table">
        </table>
        <div id="ajax_user_list_page">
        </div>
    </div>
  </div>
</div>

<div class="modal-footer">
    <%--  <span style="float:left;">用户名:</span>
      <input name="userName" id="userName" style="float:left;margin-left:5px"/>
      <span class="btn btn-primary" id="dialog-serach" style="float:left;cursor:pointer;margin-left:10px">搜索</span>
      <span class="btn btn-primary" id="dialog-back" style="float:left;cursor:pointer;margin-left:10px">返回</span>  --%>
    <span class="btn btn-primary" id="dialog-ok">确定</span>
    <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>
  var key = "<s:property value="key" />";
  var more="<s:property value="more"/>";
  function run_jqgrid_function(){
    jQuery("#ajax_levelUser_list_table").jqGrid({
      url:'ajax-depart!list.action',
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['上级部门','部门名称','Id'],
      colModel : [
            {name:'parent',index:'parent_name', width:100,sortable:false,search:false},
            {name:'name',index:'name', width:500,sortable:true,search:false},
            {name:'id',index:'id',width:10,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_levelUser_list_page',
      sortname : 'createDate',
      sortorder : "asc",
      gridComplete:function(){
          /*
          var ids=$("#ajax_levelUser_list_table").jqGrid('getDataIDs');
          var levelUsersId=$("#levelUsersId").val();
          //levelUsersId = levelUsersId.substring(0,levelUsersId.length-1);
          for(var i=0;i<ids.length;i++) {
              var cl = ids[i];
              if (levelUsersId != "" && levelUsersId != null) {
                  var idsArr = levelUsersId.split(",");
                  for (var s = 0; s < idsArr.length; s++) {
                      if (idsArr[s] == cl) {
                          $("#ajax_levelUser_list_table").jqGrid("setSelection", cl);
                      }
                  }
              }
          }*/
          jqGridStyle();
          $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: false,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
        grouping:true,
        groupingView : {
            groupField : ['parent'],
            groupColumnShow : [false]
        },
      subGrid: true,
      subGridOptions: {
        "plusicon"  : "ui-icon-triangle-1-e",
        "minusicon" : "ui-icon-triangle-1-s",
        "openicon"  : "ui-icon-arrowreturn-1-e",
        //expand all rows on load
        "expandOnLoad" : false
      },
        subGridRowExpanded: function(subgrid_id, row_id) {
            var subgrid_table_id, pager_id;
            subgrid_table_id = subgrid_id+"_t";
            pager_id = "p_"+subgrid_table_id;
            $("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
            jQuery("#"+subgrid_table_id).jqGrid({
                url:"ajax-leveluser!subGrid.action?pid="+row_id,
                datatype: "json",
                colNames: ['用户名','岗位','id'],
                colModel: [
                    {name:"name",index:"name",width:200,search:true},
                    {name:"post",index:"post",width:260,search:true},
                    {name:'id',index:'id',key:true,width:10,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
                ],
                sortname : 'createDate',
                sortorder : "desc",
                height: '100%',
                multiselect: true,
                jsonReader: {
                    root: "dataRows"
                },
                gridComplete:function(){
                    var ids=$("#"+subgrid_table_id).jqGrid('getDataIDs');
                    var usersId=$("#"+key+"Id").val();
                    for(var i=0;i<ids.length;i++) {
                        var cl = ids[i];
                        if (usersId != "" && usersId != null) {
                            var idsArr = usersId.split(",");
                            for (var s = 0; s < idsArr.length; s++) {
                                if (idsArr[s] == cl) {
                                    $("#"+subgrid_table_id).jqGrid("setSelection", cl);
                                }
                            }
                        }
                    }
                    jqGridStyle();
                    $(".ui-jqgrid-bdiv").css("overflow-x","hidden");

                },
                onSelectRow: function (rowId, status, e) {
                   // var rowId = $("#"+subgrid_table_id).jqGrid('getGridParam','selrow');
                    if(more !="1"){
                        $(this).gridselect('onecheck',{
                            table: subgrid_table_id,
                            rowId:rowId,
                            status:status,
                            id:key+"Id",
                            name:key,
                            type:"radio"
                        });
                    }else {
                        $(this).gridselect('onecheck', {
                            table: subgrid_table_id,
                            rowId: rowId,
                            status: status,
                            id: key + "Id",
                            name: key,
                            type: "checkbox"
                        });
                    }
                 },
                onSelectAll:function(aRowids, status, e){
                    if(more =="1") {
                        $(this).gridselect('morecheck', {
                            table: subgrid_table_id,
                            rowId: aRowids,
                            status: status,
                            id: key + "Id",
                            name: key
                        });
                    }
                }

            });
            jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{edit:false,add:false,del:false,
                search:false});
            jQuery("#"+subgrid_table_id).jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
        },
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_levelUser_list_table").jqGrid('setGridWidth', $("#ajax_levelUser_list_row").width()-10);
    })

    jQuery("#ajax_levelUser_list_table").jqGrid('navGrid', "#ajax_levelUser_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_levelUser_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
      $('#dialog-serach').unbind("click").bind("click",function(){
          if($('#userName').val() == ""){
              alert("请输入用户名");
              return false;
          }
          jQuery("#ajax_user_list_table").GridUnload();
          jQuery("#ajax_levelUser_list_table").GridUnload();
          run_jqgrid_search();
      });
      $('#dialog-back').unbind("click").bind("click",function(){
          jQuery("#ajax_levelUser_list_table").GridUnload();
          jQuery("#ajax_user_list_table").GridUnload();
          run_jqgrid_function();
      });

  });
  $("#dialog-ok").unbind("click").bind("click",function(){
//      var slt =   $("#ajax_levelUser_list_table").jqGrid('getGridParam','selarrrow');
//      var levelUsersName = "";
//      for(var i=0; i<slt.length; i++){
//            var rowDatas = $("#ajax_levelUser_list_table").jqGrid('getRowData', slt[i]);
//            levelUsersName = levelUsersName + rowDatas['name'] + ",";
//      }
//       levelUsersName = levelUsersName.substring(0,levelUsersName.length-1);
//       $("#levelUsersName").val(levelUsersName);
       gDialog.fClose();
  });

  function run_jqgrid_search(){
      var userName = $('#userName').val();
      jQuery("#ajax_user_list_table").jqGrid({
          url:'ajax-users!list.action?userName='+userName,
          mtype:"POST",
          datatype: 'json',
          page : 1,
          colNames:['用户名称','Id'],
          colModel : [
              {name:'name',index:'name', width:240,sortable:false,search:true,sorttype:'string'},
              {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
          ],
          rowNum : 10,
          rowList:[10,20,30],
          pager : '#ajax_user_list_page',
          sortname : 'createDate',
          sortorder : "desc",
          gridComplete:function(){
              var ids=$("#ajax_user_list_table").jqGrid('getDataIDs');
              var usersId=$("#"+key+"Id").val();
              //usersId = usersId.substring(0,usersId.length-1);
              for(var i=0;i<ids.length;i++) {
                  var cl = ids[i];
                  if (usersId != "" && usersId != null) {
                      var idsArr = usersId.split(",");
                      for (var s = 0; s < idsArr.length; s++) {
                          if (idsArr[s] == cl) {
                              $("#ajax_user_list_table").jqGrid("setSelection", cl);
                          }
                      }
                  }
              }

              jqGridStyle();
              $(".ui-jqgrid-bdiv").css("overflow-x","hidden");


          },
          onSelectRow: function (rowId, status, e) {
              $(this).gridselect('onecheck',{
                  table:"ajax_user_list_table",
                  rowId:rowId,
                  status:status,
                  id:key+"Id",
                  name:key,
                  type:"checkbox"
              });
          },
          onSelectAll:function(aRowids, status, e){
              $(this).gridselect('morecheck',{
                  table:"ajax_user_list_table",
                  rowId:aRowids,
                  status:status,
                  id:key+"Id",
                  name:key
              });
          },
          jsonReader: {
              root: "dataRows",
              page: "page",
              total: "total",
              records: "records",
              repeatitems : false
          },
          multiselect: true,
          rownumbers:true,
          gridview:true,
          shrinkToFit:true,
          forceFit:true,
          viewrecords: true,
          autowidth: true,
          height : 'auto',
          loadComplete: function() {

          }
      });
      $(window).on('resize.jqGrid', function() {
          jQuery("#ajax_user_list_table").jqGrid('setGridWidth', $("#ajax_user_list_row").width()-10);
      })

      jQuery("#ajax_user_list_table").jqGrid('navGrid', "#ajax_user_list_page", {
          edit : false,
          add : false,
          del : false,
          search:false
      });

      jQuery("#ajax_user_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

</script>
