<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12" id="ajax_field_list_row">
            <table id="ajax_field_list_table">
            </table>
            <div id="ajax_field_list_page">
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <span class="btn btn-primary" id="dialog-ok">确定</span>
    <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>
    function run_jqgrid_function(){
        jQuery("#ajax_field_list_table").jqGrid({
            url:'ajax-dialog!fieldlist.action?keyId='+"<s:property value="keyId"/>",
            mtype:"POST",
            datatype: 'json',
            page : 1,
            colNames:["",'域名'],
            colModel : [
                {name:'id',index:'id',width:100,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true},
                {name:'name',index:'name', width:250,sortable:false,search:true,sorttype:'string'}
            ],
            rowNum : 20,
            rowList:[10,20,30],
            pager : '#ajax_field_list_page',
            sortname : 'createDate',
            sortorder : "desc",
            gridComplete:function(){
                    var fieldsId=$("#fields").val();
                    var ids=$("#ajax_field_list_table").jqGrid('getDataIDs');
                    for(var i=0;i<ids.length;i++) {
                        var cl = ids[i];
                        if (fieldsId != "" && fieldsId != null) {
                            var idsArr = fieldsId.split(",");
                            for (var s = 0; s < idsArr.length; s++) {
                                if (idsArr[s] == cl) {
                                    $("#ajax_field_list_table").jqGrid("setSelection", cl);
                                }
                            }
                        }
                    }
                jqGridStyle();
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
            },
            onSelectRow: function (rowId, status, e) {
                $(this).gridselect('onecheck',{
                    table:"ajax_field_list_table",
                    rowId:rowId,
                    status:status,
                    id:"fields",
                    name:"fieldName",
                });
            },
            onSelectAll:function(aRowids, status, e){
                $(this).gridselect('morecheck',{
                    table:"ajax_field_list_table",
                    rowId:aRowids,
                    status:status,
                    id:"fields",
                    name:"fieldName",
                });
            },
            jsonReader: {
                root: "data.data",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },

            multiselect: true,
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            forceFit:true,
            viewrecords: true,
            autowidth: true,
            height : 'auto',
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_field_list_table").jqGrid('setGridWidth', $("#ajax_field_list_row").width()-10);
        })

        jQuery("#ajax_field_list_table").jqGrid('navGrid', "#ajax_field_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });

        jQuery("#ajax_field_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
    }

    $(function(){
        run_jqgrid_function();
    });
    $("#dialog-ok").unbind("click").bind("click",function(){

//        var slt =   $("#ajax_field_list_table").jqGrid('getGridParam','selarrrow');
//        var fieldsName = "";
//        var fieldsId = "";
//        for(var i=0; i<slt.length; i++){
//            var rowDatas = $("#ajax_field_list_table").jqGrid('getRowData', slt[i]);
//            fieldsName += rowDatas['fieldName'] + ",";
//            fieldsId += slt[i] + ",";
//        }
//        fieldsName = fieldsName.substring(0,fieldsName.length-1);
//        fieldsId = fieldsId.substring(0,fieldsId.length-1);
//        if(isModal){
//            $("div.modal-body #fieldName").val(fieldsName);
//            $("div.modal-body #fieldId").val(fieldsId);
//        }else{
//              $("#fieldsName").val(fieldsName);
//              $("#fieldsId").val(fieldsId);
//              $("#fieldName").val(fieldsName);
//              $("#fieldId").val(fieldsId);
//        }

        gDialog.fClose();
    });
</script>
