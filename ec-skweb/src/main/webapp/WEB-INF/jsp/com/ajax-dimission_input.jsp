<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid=session.getId();
%>
<jsp:include page="ajax-top.jsp" />

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all">离职员工</i></a>

          <s:if test="dimission==null || dimission.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="dimission!=null && dimission.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="dimission" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="dimission.id" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              离职员工&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  用人性质
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="hidden" disabled name="nature" id="nature"  value="<s:property value="employees.nature"/>" >
                    <input type="text"  name="natureName" id="natureName"  value="<s:property value="natureName"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled id="name" name="name" type="text" value="<s:property value="employees.name"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  身份证号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input id="cardID" disabled name="cardID" type="text" value="<s:property value="employees.cardID"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  部门
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="depName" id="depName"  value="<s:property value="employees.dep.name"/>" >
                    <input type="hidden"  name="depId" id="depId" value="<s:property value="employees.dep.id"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  政治面貌
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="hidden" disabled name="politicalStatus" id="politicalStatus"  value="<s:property value="politicalStatus"/>" >
                    <input type="text" name="politicalStatusName" id="politicalStatusName"  value="<s:property value="politicalStatusName"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同开始日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="startDate" disabled name="startDate"
                           type="text" value="<s:date name="employees.startDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>

                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同结束日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="endDate"  name="endDate" disabled
                           type="text" value="<s:date name="employees.endDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  离职性质
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="dimissionNature">
                    <label class="radio">
                      <input type="radio" checked="checked" name="dimissionNature" value="0" <s:property value="dimission.dimissionNature==0?'checked':''"/>>
                      <i></i>辞职</label>
                    <label class="radio">
                      <input type="radio"  name="dimissionNature" value="1" <s:property value="dimission.dimissionNature==1?'checked':''"/>>
                      <i></i>辞退</label>
                    <label class="radio">
                      <input type="radio"  name="dimissionNature" value="2" <s:property value="dimission.dimissionNature==2?'checked':''"/>>
                      <i></i>退休</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  离职原因
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input id="reasons" name="reasons" type="text"  placeholder="请输入离职原因" value="<s:property value="dimission.reasons"/>">

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">辞职报告</label>
                <section class="col col-5">
                  <%--<label class="input">
                    <input name="reportText" id="reportText" placeholder="请输入辞职报告"
                           type="text" value="<s:property value="dimission.reportText"/>">
                  </label>--%>
                  <label class="input">
                    <input  name="uploadify" id="reportfilename" placeholder="" type="file" >
                    <input name="reportfileId" id="reportfileId" style="display: none" value="<s:property value="reportfileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">

              <label class="label col col-2">
                <i class="fa fa-asterisk txt-color-red"></i>
                预计离职日期
              </label>
              <section class="col col-4">
                <label class="input">
                  <input id="leaveDate" name="leaveDate" placeholder="请选择预计离职日期"
                         type="text" value="<s:date name="dimission.leaveDate" format="yyyy-MM-dd"/>">
                </label>
              </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input  type="text" id="usersName" name="nextStepApproversName" placeholder="请选择会签人"
                              value="<s:property value="dimission.nextStepApproversName"/>"/>
                      <input type="hidden" id="usersId" name="nextStepApproversId"
                             value="<s:property value="dimission.nextStepApproversId"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input id="remark" name="remark" type="text" placeholder="请输入备注" value="<s:property value="dimission.remark"/>">

                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text"  id="applyUserName" placeholder="" value="<s:property value="creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  申请日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input  disabled type="text"  id=applyDate" placeholder="" value="<s:property value="createDate"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">
/*
       $('li.disabled').off("click").on("click", function () {
           return false;
       })


*/
    $(function(){
        var pdata= {
            keyId:$("#keyId").val(),
            flowName:"dimission",
            todo:"1"
        };
        multiDuty(pdata);
    });


    //上传
    inputLoad({
        objId:"reportfilename",
        entityName:"reportId",
        sourceId:"reportfileId",
        jsessionid:"<%=jsessionid%>"
    });
    //校验
    $("#manage").validate({
        rules : {
            reportId : {
                required : true
            },
            reasons: {
                required : true
            },
            leaveDate:{
                required : true
            },

        },
        messages : {
            reportId : {
                required : '请选择辞职报告'
            },
            reasons: {
                required : '请输入离职原因'
            },
            leaveDate:{
                required : '请选择离职原因'
            },

        },
        ignore: "",

    });

    $('#startDate,#endDate,#leaveDate').datetimepicker({
       format: 'yyyy-mm-dd',
       weekStart: 1,
       autoclose: true,
       startDate:new Date(),
       todayBtn: 'linked',
       language: 'zh-CN',
       minView:2
    });

    //会签
    $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择人员",
            url:"ajax-dialog!user.action",
            width:340
        }).show();
    });

  //保存
  $("#btn-save-common").click(

      function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("dimission","ajax-dimission!save.action",null,function (data) {

              if(data.state == "200" || data.state ==200) {
                  if(draft == 1){
                      location.href='index.action';
                  }else{
                      loadURL("ajax-dimission.action",$('#content'));
                  }
              }
          });
      }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
      function(e) {
          if(!$("#dimission").valid()){
              $("#areaselect_span").hide();
              return false;
          }
          $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
          }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                  e.preventDefault();
                  e.stopPropagation();
                  return;
              }
              if (ButtonPressed === "确认") {
                  var $validForm = $("#dimission").valid();
                  if(!$validForm) return false;
                  $("#btn-confirm-common").attr("disabled", "disabled");
                  $("#btn-recommit-common").attr("disabled", "disabled");
                  form_save("dimission","ajax-dimission!commit.action",null,function (data) {

                      if(data.state == "200" || data.state ==200) {
                          if(draft == 1){
                              location.href='index.action';
                          }else{
                              loadURL("ajax-dimission.action",$('#content'));
                          }
                      }
                  });
              }
          });
      }
  );

    //返回视图
    $("#btn-re-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-dimission1.action",$('#content'));
        }

    });

</script>