<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid=session.getId();
%>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-summary-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"> 培训总结</i></a>

          <s:if test="summary==null || summary.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="summary!=null && summary.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="summary" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="summary.id" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              培训总结&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  所属培训
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="hidden" disabled name="typeDictId" id="typeDictId"  value="<s:property value="type.id"/>" >
                    <input type="text"  name="typeDictName" id="typeDictName"  value="<s:property value="type.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  课程名称
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled id="name" name="name" type="text" value="<s:property value="name"/>">

                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  培训讲师
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled  type="text" name="teacherName" value="<s:iterator id="list" value="teacherList"><s:property value="#list.name"/>,</s:iterator>"/>
                  </label>
                  <label class="input state-disabled">
                    <input   type="hidden" id="teacherId" name="teacherId" value="<s:iterator id="list" value="teacherList"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  培训评估
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  name="uploadify" id="assessmentfilename" placeholder="" type="file" >
                    <input name="assessmentfileId" id="assessmentfileId" style="display: none" value="<s:property value="assessmentfileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  提交人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text"  id="creater" placeholder="" value="<s:property value="creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  提交日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input  disabled type="text" name="createDate" id=createDate"  value="<s:property value="createDate"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">
    var draft = "<s:property value="draft" />";

    //上传
    inputLoad({
        objId:"assessmentfilename",
        entityName:"assessmentfileIds",
        sourceId:"assessmentfileId",
        jsessionid:"<%=jsessionid%>"
    });

    $(function(){
        var pdata= {
            keyId:$("#keyId").val(),
            flowName:"summary"
        };
        multiDuty(pdata);
    });

    //校验
    $("#summary").validate({
        rules : {
            assessmentfileIds:{
                required : true
            },
        },
        messages : {
            assessmentfileIds: {
                required : '请选择培训评估'
            },
        },
        ignore: "",
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });

    //保存
    $("#btn-save-common").click(
        function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("summary","ajax-summary!save.action",null,function (data) {

                if(data.state == "200" || data.state ==200) {
                    if(draft == 1){
                        location.href='index.action';
                    }else{
                        loadURL("ajax-summary.action?parentId="+$("input#parentId").val(),$('div#s2'));
                    }
                }
            });
        }
    );

    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#summary").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#summary").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("summary","ajax-summary!commit.action",null,function (data) {

                        if(data.state == "200" || data.state ==200) {
                            if(draft == 1){
                                location.href='index.action';
                            }else{
                                loadURL("ajax-summary.action?parentId="+$("input#parentId").val(),$('div#s2'));
                            }
                        }
                    });
                }
            });
        }
    );

    //返回视图
    $("#btn-summary-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-summary.action?parentId="+$("#parentId").val(),$('div#s2'));
        }

    });


</script>