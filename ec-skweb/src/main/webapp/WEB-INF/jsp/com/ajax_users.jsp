<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="row" style="padding:2px">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<div >
			<form id="searchForm" method="post" action="">
				<a id="ajax_users_btn_add" class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 添加新成员</a>
				<%--<a id="ajax_export_users_info" class="btn btn-default btn-primary pull-right" href="javascript:void(0);"> 成员个人信息导出  <i class="fa fa-lg fa-external-link"></i></a>--%>
				<%--<a id="ajax_import_btn_upload" class="btn btn-default btn-primary pull-right" href="javascript:void(0);"> 成员个人信息导入 <i class="fa fa-lg fa-cloud-upload"></i></a>--%>
			</form>
		</div>
	</div>
</div>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">
		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
				data-widget-colorbutton="false" 
				data-widget-togglebutton="false" 
				data-widget-deletebutton="false" 
				data-widget-fullscreenbutton="true" 
				data-widget-custombutton="false" 
				data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<div style="margin-left: 30px;margin-top: -5px;position: absolute;">成员信息列表</div>
				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class=" ">
                                <div class="row" id="ajax_users_list_row">

						            <table id="ajax_users_table" class="table table-striped table-bordered table-hover">
						            </table>
                                    <div id="ajax_users_list_page">
						            </div>
                                </div>

						    </div>
                        </div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<div id="dialog-users_message">

</div><!-- #dialog-message -->

<div id="dialog-message">

</div><!-- #dialog-message -->

<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	
	function reload(){
		loadURL("../com/ajax!users.action",$('#content'));
	}
	
	//添加新成员
	$('#ajax_users_btn_add').click(function() {
	    var this_tr = $(this).parent().parent();
        gDialog.fCreate({
		    title:'添加成员信息',
		    url:"../com/ajax-users!input.action?keyId=" + $(this_tr).attr("id"),
		    width:500
		}).show();
	});

	$("#dialog-users_message").dialog({
		autoOpen : false,
		modal : true,
		title : "成员信息框",
		width:500,
		buttons : [{
			html : "取消",
			"class" : "btn btn-default",
			click : function() {
				$(this).dialog("close");
			}
		}, {
			html : "<i class='fa fa-check'></i>&nbsp; 提交",
			"class" : "btn btn-primary",
			click : function() {
			    var actionUrl = "<%=path%>/com/ajax-users!save.action";
			    var opt={dialogId:'dialog-users_message'};
			    form_save("users_read",actionUrl,opt);
			    loadInbox();

			}
		}]

	});


    //删除成员账户
    function fn_delete(id){
	$.SmartMessageBox({
		title : "<i class='fa fa-minus-square txt-color-orangeDark'></i> <span class='txt-color-orangeDark'><strong>删除后该成员账号将不可登陆</strong></span>",
		content : "确定删除该成员账号吗？",
		buttons : '[取消][确定]'
	}, function(ButtonPressed) {
		if (ButtonPressed === "确定") {

			var vActionUrl = "<%=path%>/com/ajax-users!delete.action";
			data={keyId:id};
			ajax_action(vActionUrl,data,{},function(pdata){
				_show(pdata);
			});
			jQuery("#ajax_users_table").trigger("reloadGrid");
		}
	});
}

</script>
<script type="text/javascript">
    $(function(){
        load_users_jqGrid();
    });
    function imgFormat(cellvalue, options, cell){

          if(cellvalue != ""){
              var imgUrl = '<img style="border-radius: 50%;" height="30" width="30" alt="" src="'+cellvalue+'">';
          }else{
              var imgUrl = '<img style="border-radius: 50%;" height="30" width="30" alt="" src="../resource/com/img/avatars/male.png">';
          }
          return imgUrl;
    };
    function btnPassFormat(cellvalue, options, cell){
         if(cellvalue!= ""){
              var cl=cell["id"];
              var btn=cellvalue+"  "+"<button class='btn btn-danger btn-sm' data-original-title='修改密码' onclick=\"ajax_users_reset_pass('"+cl+"');\"><i class='fa fa-lg fa-lock'></i>修改密码</button>";
              return btn;
         }
         return false;
    }
	function btnDutyInfo(cellvalue, options, cell){
		if(cellvalue!=""){
			var cl=cell["dutyDefault"];
            var id=cell["id"];
			if(cl==""){
				var btn="<button class='btn bg-color-blueLight txt-color-white' data-original-title='职责信息'  onclick=\"fn_users_actions_query('"+id+"');\"><i class='fa fa-eye'></i> 职责信息</button>";
			}
			else{
				var btn="<button class='btn btn-warning' data-original-title='职责信息'  onclick=\"fn_users_actions_query('"+id+"');\"><i class='fa fa-eye'></i> 职责信息</button>";
			}
			return btn;
		}
		return false;
	}
	function btnSetDutyInfo(cellvalue, options, cell){
		if(cellvalue!=""){
			var cl=cell["dutyId"];
			var id=cell["id"];
			if(cl!=""){
				var btn="<button class='btn bg-color-blueLight txt-color-white' data-original-title='分配职权'  onclick=\"fn_users_actions_config('"+id+"');\"><i class='fa fa-gear'></i>分配职权</button>";
			}else{
				var btn="<button class='btn btn-default bg-color-orange txt-color-white' data-original-title='分配职权'  onclick=\"fn_users_actions_config('"+id+"');\"><i class='fa fa-gear'></i>分配职权</button>";
			}
			return btn;
		}
		return false;
	}
	function btnSetUser(cellvalue, options, cell){
		if(cellvalue!=""){
			var cl=cell["state"];
			var id=cell["id"];
			var btn="<button class='btn btn-default' data-original-title='编辑' onclick=\"fn_users_actions_edit('"+id+"');\"><i class='fa fa-edit'></i>编辑</button>"+" "+"<button class='btn btn-default' data-original-title='删除' onclick=\"fn_users_actions_delete('"+id+"');\"><i class='fa fa-times'></i>删除</button>";
			/*
			if(cl!=""){
				btn=btn+" "+"<button class='btn btn-default' data-original-title='冻结' onclick=\"fn_users_actions_setstate('"+id+"');\"><i class='fa fa-lock'></i>冻结</button>";
			}else{
				btn=btn+" "+"<button class='btn btn-danger btn-sm' data-original-title='激活' onclick=\"fn_users_actions_setstate('"+id+"');\"><i class='fa fa-unlock'></i>激活</button>";
			}*/
			return btn;
		}
		return false;
	}
    function load_users_jqGrid(){
        jQuery("#ajax_users_table").jqGrid({
            	url:'../com/ajax-users!list.action',
        	    datatype: "json",
            	colNames:['编号',"图像",'用户名称',"邮箱/手机号","创建日期","职责信息","职权信息","操作","id"],
            	colModel:[
				   {name:'no',index:'no', width:15,sortable:false,search:false},
				   {name:'userImg',index:'userImg', width:20,sortable:false,search:false,formatter:imgFormat},
				   {name:'name',index:'name', width:50,formatter:btnPassFormat},
				   {name:'mobile',index:'mobile', width:40},
				   {name:'createDate',index:'createDate', width:42},
				   {name:'roleBtn',index:'roleBtn', width:30,sortable:false,search:false,formatter:btnDutyInfo},
				   {name:'setBtn',index:'setBtn', width:110,sortable:false,search:false,fixed:true,formatter:btnSetDutyInfo},
				   {name:'act',index:'act', width:210,sortable:false,search:false,fixed:true,formatter:btnSetUser},
				   {name:'id',index:'id',hidden:true}
             	],
                rowNum : 10,
                rowList:[10,20,30],
                pager : '#ajax_users_list_page',
                sortname : 'id',
                sortorder : "asc",
                gridComplete:function(){
                    $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                    jqGridStyle();
                },
    	        jsonReader: {
    			    root: "dataRows",
    			    page: "page",
    			    total: "total",
    			    records: "records",
    			    repeatitems : false
    			},
    			multiselect : false,
                rownumbers:true,
                gridview:true,
                shrinkToFit:true,
                viewrecords: true,
                autowidth: true,
                height:'auto',
                forceFit:true,
                loadComplete: function() {
                }
            });
            $(window).on('resize.jqGrid', function() {
                jQuery("#ajax_users_table").jqGrid('setGridWidth', $("#ajax_users_list_row").width());
            })
			jQuery("#ajax_users_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});
            jQuery("#ajax_users_table").jqGrid('navGrid', "#ajax_users_list_page", {
                edit : false,
            	add : false,
            	del : false,
            	search:false
            });
	};
     //编辑
    function fn_users_actions_edit(id){
         loadURL("../com/ajax-users!read.action?keyId="+id,$('#content'));
    };
    //删除
    function fn_users_actions_delete(id){
         fn_delete(id);
    };
    //分配部门岗位
    function fn_users_actions_config(id){
        gDialog.fCreate({
		    title:'分配职权【左部门右岗位】',
		    url:"../com/ajax-users!configView.action?keyId=" + id,
		    width:500
		}).show();
    };
    //查看职责信息
    function fn_users_actions_query(id){
        var actionUrl = "../com/ajax-users!leftView.action?keyId="+id;
		$(this).leftview('init',{
			title:"查看职责【可以设置主要职责】",
			actionUrl:actionUrl
		});
    };
</script>
<script type="text/javascript">
$("#ajax_export_users_info").click(function(){
        var content = "";
		var title = "确定导出业务数据吗？";

		$.SmartMessageBox({
			title : title,
			content : content,
			buttons : '[取消][确定]'
		}, function(ButtonPressed) {
			if (ButtonPressed === "确定") {
				$("#searchForm").attr("action","../com/ajax-import!exportFile.action").submit();
			}
		});
});
$("#ajax_import_btn_upload").click(function(){
        gDialog.fCreate({
		    title:'请上传excel文件',
		    url:"../com/ajax-import!dialog.action",
		    width:500
		}).show();
});
//重置密码
function ajax_users_reset_pass(id){
		$.SmartMessageBox({
			title : "<i class='fa fa-plus-square txt-color-orangeDark'></i>  重置  <span class='txt-color-orangeDark'><strong> 密码，请谨慎操作！ </strong></span> ",
			content : "请输入一个新密码",
			buttons : "[取消][确定]",
			input : "text",
			placeholder : "请输入新密码..."
		}, function(ButtonPress, Value) {
			if(ButtonPress=="确定"){
				if(Value == ""){
					$.smallBox({
						title : "操作失败",
						content : "<i class='fa fa-clock-o'></i> <i>"
								+ "密码不能为空！" + "</i>",
						color : "#C46A69",
						iconSmall : "fa fa-times fa-2x fadeInRight animated",
						timeout : 3000
					});
				}else{
                    var vActionUrl = "<%=path%>/com/ajax-users!reset.action";
			        data={keyId:id,password:Value};
			        ajax_action(vActionUrl,data,{},function(pdata){
						_show(pdata);
					});
				}
			}
		});
};
//冻结，激活
	function fn_users_actions_setstate(id){
		var vActionUrl = "<%=path%>/com/ajax-users!setUserState.action";
		data={keyId:id};
		ajax_action(vActionUrl,data,{},function(pdata){
			_show(pdata);
		});
		jQuery("#ajax_users_table").trigger("reloadGrid");
	}
</script>