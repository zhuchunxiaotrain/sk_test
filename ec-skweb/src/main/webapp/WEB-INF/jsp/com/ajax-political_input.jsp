<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-sm-12 col-md-12 col-lg-12">
        <div class="jarviswidget well" id="wid-id-3"
             data-widget-colorbutton="false"
             data-widget-editbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-custombutton="false"
             data-widget-sortable="false">
            <!-- widget div-->
            <div>
                <div class="widget-body">
                    <div class="row">
                        <s:if test="employees!=null || employees.getProcessState().name()=='Draft'">
                            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
                        </s:if>
                        <s:if test="employees!=null && employees.getProcessState().name()=='Backed'">
                            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
                        </s:if>
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-save-political" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>

                    </div>
                    <hr class="simple">
                    <form id="political" class="smart-form" novalidate="novalidate" action="" method="post">
                        <input  type="hidden" name="keyId" id="keyId" value="<s:property value="keyId" />"/>
                        <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                        <header  style="display: block;">
                            政治面貌信息&nbsp;&nbsp;<span id="title"></span>
                        </header>
                        <fieldset>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    是否党员
                                </label>
                                <section class="col col-5">
                                    <div class="inline-group" name="partyMember">
                                        <label class="radio">
                                            <input type="radio" checked="checked" name="partyMember" value="0" <s:property value="employees.partyMember==0?'checked':''"/>>
                                            <i></i>是</label>
                                        <label class="radio">
                                            <input type="radio" name="partyMember" value="1" <s:property value="employees.partyMember==1?'checked':''"/>>
                                            <i></i>否</label>
                                    </div>
                                </section>
                            </div>

                            <div class="row" <s:if test="employees.partyMember==1 ">style="display:none"</s:if> id="showDiv">
                                <div class="row">
                                    <label class="label col col-2">
                                        <i class="fa fa-asterisk txt-color-red"></i>
                                        入党时间
                                    </label>
                                    <section class="col col-5">
                                        <label class="input">
                                            <input  placeholder="请选择入党时间" id="partyDate" name="partyDate"
                                                    type="text" value="<s:date name="employees.partyDate" format="yyyy-MM-dd"/>">
                                        </label>
                                    </section>
                                </div>

                                <div class="row">
                                    <label class="label col col-2">
                                        介绍人
                                    </label>
                                    <section class="col col-5">
                                        <label class="input">
                                            <input type="text" name="introducer" id="introducer" placeholder="请输入介绍人" value="<s:property value="employees.introducer"/>" >
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <label class="label col col-2">
                                        党费缴纳基数(元)
                                    </label>
                                    <section class="col col-5">
                                        <label class="input">
                                            <input type="text" name="baseMoney" id="baseMoney" placeholder="请输入当费缴纳基数" value="<s:property value="employees.baseMoney"/>" >
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <label class="label col col-2">
                                        工会费(元)
                                    </label>
                                    <section class="col col-5">
                                        <label class="input">
                                            <input type="text" name="dues" id="dues" placeholder="请输入工会费" value="<s:property value="employees.dues"/>" >
                                        </label>
                                    </section>
                                </div>
                           </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </article>
</div>

<script>
    
    $('#partyDate').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN',
        minView:2
    });


    $(':radio[name="partyMember"]').click(function(){
        if($(this).val()=="0") {
            $('#showDiv').show();
        }else{
            $('#showDiv').hide();
        }
    });

    //保存
    $("#btn-save-political").click(

        function(){
            $("#btn-save-political").attr("disabled", "disabled");
            form_save("political","ajax-political!save.action");
           // loadURL("ajax-employees.action",$('#content'));
        }

    );

    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#political").valid()){
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#political").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("political","ajax-political!commit.action");
                    loadURL("ajax-employees.action",$('#content'));
                }
            });
        }
    );


</script>