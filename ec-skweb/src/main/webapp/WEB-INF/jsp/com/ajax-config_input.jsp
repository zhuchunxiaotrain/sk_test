<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath(); 
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<input type="hidden" id="createType" value="<s:property value="cselectType"/>"/>
<input type="hidden" id="readType" value="<s:property value="rselectType"/>"/>
<input type="hidden" id="editType" value="<s:property value="eselectType"/>"/>
<input type="hidden" id="docReadType" value="<s:property value="dselectType"/>"/>
<!-- rows -->
<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-sm-12 col-md-12 col-lg-12">
        <div class="jarviswidget well" id="wid-id-3"
            data-widget-colorbutton="false"
            data-widget-editbutton="false"
            data-widget-togglebutton="false"
            data-widget-deletebutton="false"
            data-widget-fullscreenbutton="false"
            data-widget-custombutton="false"
            data-widget-sortable="false">
            <!-- widget div-->
            <div>
                <div class="widget-body">
                    <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i> 流程明细</a>
                    <a class="btn btn-default pull-right pull-right-fix" id="btn-config-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>

                    <hr class="simple">
                    <form id="formConfig" class="smart-form" novalidate="novalidate" action="" method="post">
                        <input type="hidden" name="keyId" id="keyId" value="<s:property value="keyId" />">
                        <input type="hidden"  id="createId"     name="createId" value="<s:property value="ccreateConfig.id" />">
                        <input type="hidden"  id="archReaderId" name="archReaderId" value="<s:property value="creadConfig.id" />">
                        <input type="hidden"  id="archEditorId" name="archEditorId" value="<s:property value="ceditConfig.id" />">
                        <input type="hidden"  id="docReaderId"  name="docReaderId" value="<s:property value="cdocConfig.id" />">
                        <header id="title" style="display: block;">
                            基本配置
                            <a href="javascript:void(0);" key="btn_process-setting">
                                        <span class="txt-color-blueDark pull-right">
                                            <i class="fa-fw fa fa-wrench"></i> 流程配置<span></span>
                                        </span>
                            </a>
                        </header>
                        <fieldset >
                            <div class="row">
                                <label class="label col col-2">流程名称：</label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="processName" id="processName"
                                               value="<s:property value="processDefinition.name!=null?processDefinition.name:name"/>">
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">关键字：</label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input  type="text" name="processKey" id="processKey" disabled value="<s:property value="processDefinition.key"/>">
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">流程版本：</label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input  type="text" name="processVersion" id="processVersion" disabled value="<s:property value="processDefinition.version"/>">
                                    </label>
                                </section>
                            </div>

                            <%--<div class="row">--%>
                                <%--<label class="label col col-2">表：</label>--%>
                                <%--<section class="col col-5">--%>
                                    <%--<select class="form-control" name="tablekey">--%>
                                        <%--<option value="">请选择</option>--%>
                                            <%--<option  value="ec_comproinfo">项目信息表</option>--%>
                                            <%--<option  value="ec_proBack">反馈表</option>--%>
                                    <%--</select>--%>
                                <%--</section>--%>
                            <%--</div>--%>
                            <%--
                            <div class="row">
                                <label class="label col col-2">更新控制存取：</label>
                                <section class="col col-5">
                                    <label class="checkbox">
                                        <input type="checkbox" name="ptype" id="ptype" <s:property value="businessConfig.ptype==1?'checked':''"/> value="1" >
                                        <i></i>已归档文档按原有权限执行
                                    </label>
                                </section>
                            </div>
                            --%>
                            <%--<div class="row">--%>
                                <%--<label class="label col col-2">更新时间</label>--%>
                                <%--<section class="col col-2">--%>
                                    <%--<label class="input">--%>
                                        <%--<i class="icon-append fa fa-calendar"></i>--%>
                                        <%--<input  class="hasDatepicker form-control" name="triggleTime" id="triggleTime" placeholder="请选择更新时间"--%>
                                                <%--type="text" value="<s:date name="businessConfig.triggleTime" format="yyyy-MM-dd"/>">--%>
                                    <%--</label>--%>
                                <%--</section>--%>
                            <%--</div>--%>
                            <div class="row" <s:property value="businessConfig.ptype==1?'':'hidden'"/> id="bversion">
                                <label class="label col col-2">业务配置版本：</label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input disabled type="text" name="version" id="version" value="<s:property value="version"/>">
                                    </label>
                                </section>
                            </div>

                            <header style="display: block;">
                                文档归档后配置

                                <a key="archReader_href" href="javascript:void(0);">
                                    <span class="txt-color-blueDark pull-right">
                                            <i class="fa-fw fa fa-wrench"></i> 读者类型<span></span>
                                    </span>
                                </a>
                                <a key="archEditor_href" href="javascript:void(0);">
                                    <span class="txt-color-blueDark pull-right">
                                            <i class="fa-fw fa fa-wrench"></i> 编辑者类型<span></span>
                                    </span>
                                </a>
                            </header>
                            <%--
                            <div class="row">
                                <label class="label col col-2">经办人是否可见：</label>
                                <section class="col col-5">
                                    <label class="checkbox">
                                        <input type="checkbox" name="otype" id="otype" value="1" <s:property value="businessConfig.otype==1?'checked':''"/>>
                                        <i></i>经办人可见
                                    </label>
                                </section>
                            </div>
                            --%>
                            <div class="row">
                                <label class="label col col-2" style="color:#4FA5D0">读者：</label>
                            </div>
                             <div id="archReaderDiv" >
                             </div>
                             <div class="row">
                                <label class="label col col-2" style="color:#4FA5D0">编辑者：</label>
                             </div>
                              <div id="archEditorDiv">
                              </div>
                            <header style="display: block;">
                                其他设置
                                <a key="docReader_href" href="javascript:void(0);">
                                    <span class="txt-color-blueDark pull-right">
                                            <i class="fa-fw fa fa-wrench"></i> 文档阅览者<span></span>
                                    </span>
                                </a>
                            </header>
                            <div id="docReaderDiv">

                            </div>
                        </fieldset>

                    </form>
                </div>
            </div>
        </div>
    </article>
</div>


<script type="text/javascript">
pageSetUp();

$("a[key=btn_process-setting]").unbind("click").bind("click",function(){
   var keyId = $("#formConfig #keyId").val();
   $.ajax({ url: "<%=path%>/com/ajax-config!checkBusinessConfig.action?keyId="+keyId,dataType:"json",type:"post",
        success: function(data){
            if(data.state == "200"){
                loadURL("<%=path%>/com/ajax-config!processInput.action?keyId="+keyId,$("#content"));
            }else{
                $.smallBox({
                        title : data.title,
                        content : "<i class='fa fa-clock-o'></i> <i>"+ data.message + "</i>",
                        color : "#C46A69",
                        iconSmall : "fa fa-times fa-2x fadeInRight animated",
                        timeout : 6000
                    });
            }
        }
   });


});

$("#btn-re-common").click(function(){
    loadURL("../com/ajax!flow.action",$('#content'));
});
$("#btn-config-save-common").click(function(){
    var $valid = $("#formConfig").valid();
    if(!$valid) return false;
    form_save("formConfig","<%=path%>/com/ajax-config!save.action");
    loadURL("../com/ajax!flow.action",$('#content'));
});

</script>
<script>
    $.validator.methods.compareDate = function(value, element) {
        var date1 = new Date();
        var date2 = new Date(Date.parse(value.replace("-", "/")));
        return date1 <= date2;
    };
 //校验
 $("#formConfig").validate({
     rules : {
         projectId : {
             required : true
         },
         triggleTime:{
             required : true,
             compareDate:true
         }
     },
     messages : {
         projectId : {
             required : '请选择项目'
         },
         triggleTime:{
             required : '请选择更新时间',
             compareDate:'更新时间请不要小于当前时间'
         }
     },
     errorPlacement : function(error, element) {
         error.insertAfter(element.parent());
     }
 });
    $("#ptype").unbind("click").bind("click",function(){
        if($(this).prop("checked")){
            $("#ptype").val(1);
            $("#bversion").show();
        }else{
            $("#ptype").val(0);
            $("#bversion").hide();
        }

    });
</script>

<script>

    $("a[key$='_href']").unbind("click").bind("click",function(){
        var key = $(this).attr("key");

        var type = key.split("_")[0];
        console.debug(type);
        var keyId = $("input#"+type+"Id").val();
        var actionUrl = "../com/ajax-commoncfg!input.action";
        $(this).leftview('init',{
            title:"配置权限",
            data:{keyId:keyId,selectType:type},
            actionUrl:actionUrl
        });
    });
    $('.hasDatepicker').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN',
        minView:2
    });

    $(function(){
       // loadURL("ajax-commoncfg!read.action?keyId="+$("#createId").val(),$("#createDiv"));
        loadURL("../com/ajax-commoncfg!read.action?keyId="+$("#archReaderId").val(),$("#archReaderDiv"));
        loadURL("../com/ajax-commoncfg!read.action?keyId="+$("#archEditorId").val(),$("#archEditorDiv"));
        loadURL("../com/ajax-commoncfg!read.action?keyId="+$("#docReaderId").val(),$("#docReaderDiv"));
    });
</script>
