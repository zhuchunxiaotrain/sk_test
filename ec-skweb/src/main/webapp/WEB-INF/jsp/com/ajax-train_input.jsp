<%-- 
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
  String jsessionid=session.getId();
%>
<jsp:include page="ajax-top.jsp" />
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>培训申请</a>
          <s:if test="train==null || train.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="train!=null && train.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="train" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="train.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              培训申请&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  培训主题
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="name" id="name" placeholder="请输入培训主题" value="<s:property value="train.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                   培训类型
                </label>
                <section class="col col-5">
                  <label class="input">
                    <select class="form-control" name="traindictId" id="traindictId" >
                      <option value="">请选择</option>
                      <s:iterator value="trainDict" id="list">
                        <option  value="<s:property value="#list.id"/>" <s:property value="#list.selected"/> ><s:property value="#list.name"/></option>
                      </s:iterator>
                    </select>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  培训内容
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input  name="uploadify" id="contentfilename" placeholder="" type="file" >
                    <input name="contentfileId" id="contentfileId" style="display: none" value="<s:property value="contentfileId"/>">
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-trainUsers"> 培训讲师</a>
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="trainUsers"
                           value="<s:iterator id="list" value="train.trainUsers"><s:property value="#list.name"/>,</s:iterator>"/>
                    <input type="hidden" id="trainUsersId" name="trainUsersId"
                           value="<s:iterator id="list" value="train.trainUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  培训周期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input   id="startDate" name="startDate" type="text" placeholder="请选择培训周期" value="<s:date name="train.startDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <label class="label col col-2">

                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="endDate" name="endDate" type="text" placeholder="请选择培训周期" value="<s:date name="train.endDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">部门</label>
                <label class="label col col-6">参与人</label>
                <section class="col col-3">
                  <label class="label col col-3" >
                    <a id="btn-add-row" href="javascript:void(0)"><i class="fa fa-rocket"></i>增加行</a>
                  </label>
                </section>
              </div>
              <div id="add-participant-row">
                <s:iterator id="list" value="train.participantSet">
                  <div class="row">
                    <label class="label col col-2">部门,参与人</label>
                    <section class="col col-4">
                      <label class="input state-disabled">
                        <input disabled type="text" id="departmentName" value="<s:property value="#list.department.name"/>"/>
                        <input type="hidden" id="departmentId" name="departmentId" value="<s:property value="#list.department.id"/>"/>
                      </label>
                    </section>
                    <label class="label col col-2">
                    </label>
                    <section class="col col-4">

                      <label class="input state-disabled">
                        <input disabled type="text" id="participantName"
                               value="<s:iterator id="list1" value="#list.participantUsers"><s:property value="#list1.name"/>,</s:iterator>"/>
                        <input type="hidden" id="participantId" name="participantId"
                               value="<s:iterator id="list1" value="#list.participantUsers"><s:property value="#list1.id"/>,</s:iterator>"/>
                      </label>
                    </section>
                      <%--
                                      <section class="col col-4">
                                        <label class="input state-disabled">
                                          <input disabled type="text" id="participantName" value="<s:property value="#list.participantUsers.name"/>"/>
                                          <input type="hidden" id="participantId" name="participantId" value="<s:property value="#list.participantUsers.id"/>"/>
                                        </label>
                                      </section>--%>
                  </div>
                </s:iterator>
              </div>

              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="remark" id="remark" placeholder="请输入备注" value="<s:property value="train.remark"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text" placeholder="" value="<s:property value="creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  操作日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input  disabled type="text" name="createDate" id=createDate"  value="<s:property value="createDate" />">
                  </label>
                </section>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script>

    var draft = "<s:property value="draft" />";
    $(function(){
    var pdata= {
      keyId:$("#keyId").val(),
      flowName:"train",
      todo:"1"
    };
    multiDuty(pdata);

  });

    //校验
    $("#train").validate({
        rules : {
            name : {
                required : true
            },
            traindictId: {
                required : true
            },
            contentfileIds:{
                required : true
            },
            trainUsersId:{
                required : true
            },
            startDate:{
                required : true
            },
            endDate:{
                required : true
            },

        },
        messages : {
            name : {
                required : '请输入培训主题'
            },
            traindictId: {
                required : '请选择培训类型'
            },
            contentfileIds:{
                required : '请选择培训内容'
            },
            trainUsersId:{
                required : '请选择培训讲师'
            },
            startDate:{
                required : '请选择培训开始时间'
            },
            endDate:{
                required : '请选择培训结束时间'
            },


        },
        ignore: "",

    });

    //增加行
  $("#btn-add-row").bind("click",function () {
      var myDate=new Date;
      var num= myDate.getMilliseconds();  //获取当前毫秒数(0-999)

      input_id='participantDepartment'+num+"Id";
      input_name='participantDepartment'+num;
       input_usersId='participantUsers'+num+"Id";
       input_usersName='participantUsers'+num;
      var str='<div class="row" id="addDiv"> <label class="label col col-2"><a href="javascript:void(0);" key="btn-choose-department" id="add-dep" > 部门</a></label> <section class="col col-3"> <label class="input state-disabled"> <input disabled type="text" id="'+input_name+'" name="'+input_name+'"  placeholder="请选择部门" /> <input type="hidden" id="'+input_id+'" name="departmentId" /> </label> </section> <label class="label col col-2"> <a  href="javascript:void(0);" key="btn-choose-participantUsers" id="add-users"> 参与人</a> </label> <section class="col col-3"> <label class="input"> <label class="input state-disabled"> <input disabled type="text"  id="'+input_usersName+'" name="'+input_usersName+'"/> <input type="hidden" id="'+input_usersId+'" name="participantId" /> </label> </label> </section> <section class="col col-2"> <label class="input"> <a  href="javascript:void(0);" class="del"><i class="fa fa-rocket"></i>删除行</a> </label></section></div>';
      $("#add-participant-row").append(str);

  });

  $(document).on("click", '.del', function(){

      var div$=$("a.del");
/*
      div$.attr("style","border: 1px solid red")
*/
      /*var acceptMoneys$= $("input[name='acceptMoneys']");
      if(acceptMoneys$.length==1){
          alert("至少有一个收款人")
      }else{
*/        /*$(this).parents('div[data-row="1"]').remove();*/
          $(this).closest("div").remove();
      /*}*/
  });

  //部门
  $(document).on("click", '#add-dep', function(){
      gDialog.fCreate({
          title:"请选择发布部门",
          url:"ajax-dialog!dept.action?key="+input_name,
          width:560
      }).show();
  });

  //参与人
  $(document).on("click", '#add-users', function(){
      /*var dep=$("#"+input_name).val();
      if(dep.length<1){
          alert("请先选择部门！");
          return false;
      }*/
      gDialog.fCreate({
          title:"请选择参与人",
          //url:"ajax-dialog!manageUser.action?key=btn-choose-participantUsers"+"&participantName="+$("input[name='+input_usersName+']").attr("id")+"&participantId="+$("input[name='input_usersName']").attr("id")+"&departmentId="+$("input[name='departmentId']").val(),
          url:"ajax-dialog!usersByDept.action?key="+input_usersName+"&more=1&departmentId="+$("input[name='departmentId']").val(),
          width:340
      }).show();
  });


  //上传
  inputLoad({
    objId:"contentfilename",
    entityName:"contentfileIds",
    sourceId:"contentfileId",
    jsessionid:"<%=jsessionid%>"
  });


  //培训讲师
  $("a[key=btn-choose-trainUsers]").unbind("click").bind("click",function(){
    gDialog.fCreate({
      title:"请选择培训讲师",
      url:"ajax-dialog!levelUser.action?key=trainUsers&more=1",
      width:560
    }).show();
  });

  $('#startDate').datetimepicker({
    format: 'yyyy-mm-dd',
    weekStart: 1,
    autoclose: true,
    todayBtn: 'linked',
    language: 'zh-CN',
    minView:2
  }).on('changeDate',function () {
      var startDate=$('#startDate').val();
      $('#endDate').datetimepicker('setStartDate',startDate);

  });

    $('#endDate').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN',
        minView:2
    });
  //保存
  $("#btn-save-common").click(

        function(){
          $("#btn-save-common").attr("disabled", "disabled");
          form_save("train","ajax-train!save.action",null,function (data) {

              if(data.state == "200" || data.state ==200) {
                  if(draft == 1){
                      location.href='index.action';
                  }else{
                      loadURL("ajax-train.action?viewtype=1",$('#content'));
                  }
              }
          });
        }

  );

  //提交
  $("#btn-confirm-common,#btn-recommit-common").click(
          function(e) {
            if(!$("#train").valid()){
              $("#areaselect_span").hide();
              return false;
            }
            $.SmartMessageBox({
              title : "提示：",
              content : "确定提交申请吗？",
              buttons : '[取消][确认]'
            }, function(ButtonPressed) {
              if (ButtonPressed === "取消") {
                e.preventDefault();
                e.stopPropagation();
                return;
              }
              if (ButtonPressed === "确认") {
                var $validForm = $("#train").valid();
                if(!$validForm) return false;
                $("#btn-confirm-common").attr("disabled", "disabled");
                $("#btn-recommit-common").attr("disabled", "disabled");
                form_save("train","ajax-train!commit.action",null,function (data) {

                    if(data.state == "200" || data.state ==200) {
                        if(draft == 1){
                            location.href='index.action';
                        }else{
                            loadURL("ajax-train.action?viewtype=1",$('#content'));
                        }
                    }
                });

              }
            });
          }
  );
    //返回视图
    $("#btn-re-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-train.action?viewtype=1",$('#content'));
        }

    });
</script>
