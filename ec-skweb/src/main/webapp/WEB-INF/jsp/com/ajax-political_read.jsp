<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <div id="myTabContent1" class="tab-content padding-10 ">
              <div class="row">
                  <a class="btn btn-default pull-right pull-right-fix"  key="ajax_edit_political" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 编辑</a>
              </div>

              <hr class="simple">
              <form id="interview" class="smart-form" novalidate="novalidate" action="" method="post">
                <input  type="hidden" name="keyId" id="politicalKeyId" value="<s:property value="employees.id" />"/>
                <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                <header  style="display: block;">
                  政治面貌信息&nbsp;&nbsp;<span id="title"></span>
                </header>
                <fieldset>
                  <div class="row">
                    <label class="label col col-2">
                      <i class="fa fa-asterisk txt-color-red"></i>
                      是否党员
                    </label>
                    <section class="col col-5">
                      <div class="inline-group" name="partyMember">
                        <label class="radio">
                          <input type="radio"  disabled  checked="checked" name="partyMember" value="0" <s:property value="employees.partyMember==0?'checked':''"/>>
                          <i></i>是</label>
                        <label class="radio">
                          <input type="radio"  disabled  name="partyMember" value="1" <s:property value="employees.partyMember==1?'checked':''"/>>
                          <i></i>否</label>
                      </div>
                    </section>
                  </div>

                  <div class="row" <s:if test="employees.partyMember== 1">style="display:none"</s:if> id="showValid">
                    <div class="row">
                    <label class="label col col-2">
                          <i class="fa fa-asterisk txt-color-red"></i>
                          入党时间
                        </label>
                        <section class="col col-5">
                          <label class="input">
                            <input  disabled placeholder="请选择入党时间" id="partyDate" name="partyDate"
                                    type="text" value="<s:date name="employees.partyDate" format="yyyy-MM-dd"/>">
                          </label>
                        </section>
                      </div>

                      <div class="row">
                        <label class="label col col-2">
                          介绍人
                        </label>
                        <section class="col col-5">
                          <label class="input">
                            <input disabled type="text" name="introducer" id="introducer" placeholder="请输入介绍人" value="<s:property value="employees.introducer"/>" >
                          </label>
                        </section>
                      </div>

                      <div class="row">
                        <label class="label col col-2">
                          党费缴纳基数(元)
                        </label>
                        <section class="col col-5">
                          <label class="input">
                            <input type="text" name="baseMoney" id="baseMoney" placeholder="请输入当费缴纳基数" value="<s:property value="employees.baseMoney"/>" >
                          </label>
                        </section>
                      </div>
                      <div class="row">
                        <label class="label col col-2">
                          工会费(元)
                        </label>
                        <section class="col col-5">
                          <label class="input">
                            <input type="text" name="money" id="money" placeholder="请输入工会费" value="<s:property value="employees.dues"/>" >
                          </label>
                        </section>

                      </div>
                   </div>
                </fieldset>
              </form>
          </div>
        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">
    //上传文件
    readLoad({
        objId:"recordfileId",
        entityName:"recordId",
        sourceId:"recordfileId"
    });

    //返回视图
    $("#btn-political-common").click(function(){
        loadURL("ajax-employees.action",$('div#s2'));
    });

    //编辑
    $("a[key=ajax_edit_political]").click(function(){
        $.ajax({
            type: "get",
            url:"ajax-employees!checkOneself.action?keyId="+$("input#keyId").val(),
            cache:false,
            dataType:"json",
            async:false,
            success: function(data){
                if(data.ifOneself=="1"){
                    loadURL("ajax-political!input.action?keyId="+$("input#politicalKeyId").val(),$('div#s2'));
                }else {
                    alert("需要本人操作!")
                }

            }
        });
    });


</script>
