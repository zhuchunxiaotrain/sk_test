<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common_emp" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>录用审核登记</a>
          <div id="myTabContent1" class="tab-content padding-10 ">
              <div class="row">
                <shiro:hasAnyRoles name="wechat">
                  <a class="btn btn-default pull-right pull-right-fix" <s:property value="isEdit(employment.id)"/> key="ajax_emp_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
                </shiro:hasAnyRoles>
              </div>
          <hr class="simple">
          <form id="employment" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="employmentKeyId" value="<s:property value="employment.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="numStatus" id="emp_numStatus" value="<s:property value="numStatus" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId"/>" />

            <header  style="display: block;">
              录用审核&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="name" id="name" disabled value="<s:property value="employment.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  员工工号
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="staffid" id="staffid" disabled value="<s:property value="employment.staffid"/>" >
                  </label>
                </section>
              </div><div class="row">
                <label class="label col col-2">
                  性别
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="sexValue" id="sexValue" placeholder="请输入性别" value="<s:property value="sexValue"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  用工性质
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="nature">
                    <label class="radio">
                      <input type="radio" disabled checked="checked" name="nature" value="0" <s:property value="employment.nature==0?'checked':''"/>>
                      <i></i>正式</label>
                    <label class="radio">
                      <input type="radio" disabled  name="nature" value="1" <s:property value="employment.nature==1?'checked':''"/>>
                      <i></i>试用</label>
                    <label class="radio">
                      <input type="radio" disabled name="nature" value="2" <s:property value="employment.nature==2?'checked':''"/>>
                      <i></i>聘用</label>
                  </div>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  手机号码
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="phone" id="phone" disabled value="<s:property value="employment.phone"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  录用岗位
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="postName" name="postName"
                           value="<s:property value="employment.post.name"/>"/>
                    <input disabled type="hidden" id="postId" name="postId"
                           value="<s:property value="employment.post.id"/>"/>

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  部门
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" id="departmentName" name="departmentName"
                           value="<s:property value="employment.department.name"/>"/>
                    <input disabled type="hidden" id="departmentId" name="departmentId"
                           value="<s:property value="employment.department.id"/>"/>

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="remark" id="remark" disabled value="<s:property value="employment.remark"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text"  placeholder="" value="<s:property value="employment.creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  操作日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input  disabled type="text" name="createDate" id=createDate"  value="<s:date name="employment.createDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
            <div class="flow">
              <s:if test="employment.getProcessState().name()=='Running'">
                <div class="f_title">审批意见</div>
                <div class="chat-footer"style="margin-top:5px">
                  <div class="textarea-div">
                    <div class="typearea">
                      <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                    </div>
                  </div>
                  <span class="textarea-controls"></span>
                </div>
              </s:if>
              <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
              <div class="f_content" style="display:none">
                <div id="emp_showFlow"></div>
              </div>
              <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
              <div class="f_content" style="display:none">
                <div id="emp_showNext"></div>
              </div>
            </div>
          </div>
    </div>
  </div>
    </div>
  </article>
</div>

<script type="text/javascript">
/*
       $('li.disabled').off("click").on("click", function () {
           return false;
       })
*/
    $(function(){
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#employmentKeyId").val()+"&type=flow",$('#emp_showFlow'));
    loadURL("ajax-running!workflow.action?bussinessId="+$("input#employmentKeyId").val()+"&type=next",$('#emp_showNext'));
    ajax_action("ajax-config!operateType.action",{keyId: $("input#employmentKeyId").val()},null,function(pdata){
        var showDuty = false;
        var area = $("span.textarea-controls");
        $(pdata.data.datarows).each(function(i,v){
            var str = '<button id="emp_left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
            $(area).append(str);
            if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                showDuty = true;
            }
        });
        if(showDuty == true){
            var pdata = {
                keyId: $("input#employmentKeyId").val(),
                flowName: "employment"
            };
            multiDuty(pdata);
        }
    });
    var valueObj=$("input#emp_numStatus").val();
    switch(valueObj){
        case "0":
            //保存后再提交
            $("#emp_left_foot_btn_approve").off("click").on("click",function(){
                form_save("employment","ajax-employment!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                    $("#btn-re-common_emp").trigger("click");
                })
            });
            break;
        case "1":
            //第一步审批
            $("#emp_left_foot_btn_approve").off("click").on("click",function(){
                form_save("employment","ajax-employment!approve1.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                    $("#btn-re-common_emp").trigger("click");
                });


            });
            break;
    }

    //流程信息展开
    $('#flow,#next').click(function(){
        if($(this).hasClass("right")){
            $(this).removeClass("right").addClass("down");
            $(this).parent(".f_title").next("div.f_content").show();
        }else{
            $(this).removeClass("down").addClass("right");
            $(this).parent(".f_title").next("div.f_content").hide();
        }
    });
   });

    //编辑
    $("a[key=ajax_emp_edit]").click(function(){
        var draft = "<s:property value="draft" />";
        loadURL("ajax-employment!input.action?keyId="+$("input#employmentKeyId").val()+"&draft="+draft+"&parentId="+$("#parentId").val(),$('#content'));
    });

    //返回视图
    $("#btn-re-common_emp").click(function(){
        var index = "<s:property value="index" />";
        var todo = "<s:property value="todo" />";
        var remind = "<s:property value="remind" />";
        var record = "<s:property value="record" />";
        var draft = "<s:property value="draft" />";
        if(index==1){
            loadURL("ajax-index!page.action",$('#content'));
        }else if(todo==1){
            loadURL("ajax!toDoList.action",$('#content'));
        }else if(remind==1){
            loadURL("ajax!remindList.action",$('#content'));
        }else if(record==1){
            loadURL("ajax!taskRecordList.action?type=1",$('#content'));
        }else if(record==2){
            loadURL("ajax!taskRecordList.action?type=2",$('#content'));
        }else if(draft==1){
            loadURL("ajax!draftList.action",$('#content'));
        }else{
            loadURL("ajax-employment.action?parentId="+$("#parentId").val(),$('div#s3'));
        }

    });

</script>