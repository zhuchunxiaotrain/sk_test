<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="row" style="padding:2px">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<div>
			<h5><label style="width:100%"> <span class="label  label-success"> 提示：删除岗位之前，先确保岗位下没有员工</span> </label>
			</h5><a id="ajax_post_btn_add" class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 添加岗位</a>
		</div>
	</div>
</div>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
				data-widget-colorbutton="false" 
				data-widget-togglebutton="false" 
				data-widget-deletebutton="false" 
				data-widget-fullscreenbutton="true" 
				data-widget-custombutton="false" 
				data-widget-sortable="false">

				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<div style="margin-left: 30px;margin-top: -5px;position: absolute;">岗位信息 </div>
				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class=" ">
                                <div class="row" id="ajax_post_list_row">
						            <table id="ajax_post_table" class="table table-striped table-bordered table-hover">
						            </table>
                                    <div id="ajax_post_list_page">
						            </div>
                                </div>

						    </div>
                        </div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<div id="dialog-post_message">

</div><!-- #dialog-message -->


<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();
	
	// PAGE RELATED SCRIPTS
	function loadDataTableScripts() {
		loadScript("../resource/com/js/plugin/datatables/jquery.dataTables-cust.min.js", dt_2);

		function dt_2() {
			loadScript("../resource/com/js/plugin/datatables/ColReorder.min.js", dt_3);
		}

		function dt_3() {
			loadScript("../resource/com/js/plugin/datatables/FixedColumns.min.js", dt_4);
		}

		function dt_4() {
			loadScript("../resource/com/js/plugin/datatables/ColVis.min.js", dt_5);
		}

		function dt_5() {
			loadScript("../resource/com/js/plugin/datatables/ZeroClipboard.js", dt_6);
		}

		function dt_6() {
			loadScript("../resource/com/js/plugin/datatables/media/js/TableTools.min.js", dt_7);
		}

		function dt_7() {
			loadScript("../resource/com/js/plugin/datatables/DT_bootstrap.js", runDataTables);
		}

	}
	function runDataTables() {
    /*
    * BASIC
    $('#ajax_members_table').dataTable({
    		"sPaginationType" : "bootstrap_full"
    });*/
    /* END BASIC */
    }
	function reload(){
		loadURL("../post/ajax!post.action",$('#content'));
	}
	
	//add post
	$('#ajax_post_btn_add').click(function() {
        var this_tr = $(this).parent().parent();
        gDialog.fCreate({
		    title:'添加岗位信息',
		    url:"../com/ajax-post!read.action?keyId=" + $(this_tr).attr("id"),
		    width:500
		}).show();
	});

	$("#dialog-post_message").dialog({
		autoOpen : false,
		modal : true,
		title : "成员信息框",
		width:500,
		buttons : [{
			html : "取消",
			"class" : "btn btn-default",
			click : function() {
				$(this).dialog("close");
			}
		}, {
			html : "<i class='fa fa-check'></i>&nbsp; 提交",
			"class" : "btn btn-primary",
			click : function() {
			    var actionUrl = "<%=path%>/com/ajax-post!save.action";
			    var opt={dialogId:'dialog-post_message'};
			    form_save("post_read",actionUrl,opt);
			    loadInbox();

			}
		}]

	});

//删除岗位
function fn_delete_post(id){
	$.SmartMessageBox({
        title : "<i class='fa fa-minus-square txt-color-orangeDark'></i> <span class='txt-color-orangeDark'><strong>删除岗位 【请确保该岗位没有关联职权】</strong></span>",
 		content : "确定删除该岗位吗？",
		buttons : '[取消][确定]'
	}, function(ButtonPressed) {
		if (ButtonPressed === "确定") {
			var vActionUrl = "<%=path%>/com/ajax-post!delete.action";
			data={keyId:id};
			ajax_action(vActionUrl,data,{},function(pdata){
                _show(pdata);
            });
			jQuery("#ajax_post_table").trigger("reloadGrid");
		}
	});
}
</script>
<script type="text/javascript">
    $(function(){
        load_post_jqGrid();
    });
    function load_post_jqGrid(){
        jQuery("#ajax_post_table").jqGrid({
            	url:'../com/ajax-post!list.action',
        	    datatype: "json",
            	colNames:["岗位名称",'岗位描述',"创建日期","操作","id"],
            	colModel:[
           		    {name:'name',index:'name', width:50},
           		    {name:'description',index:'description', width:70},
           		    {name:'createDate',index:'createDate', width:80},
           		    {name:'act',index:'act', width:150,sortable:false,fixed:true,search:false},
           		    {name:'id',index:'id', width:50,hidden:true}
             	],
                rowNum : 10,
                rowList:[10,20,30],
                pager : '#ajax_post_list_page',
                sortname : 'id',
                sortorder : "asc",
                gridComplete:function(){
                    var ids=$("#ajax_post_table").jqGrid('getDataIDs');
                    for(var i=0;i<ids.length;i++){
                        var cl=ids[i];
                        se="<button class='btn btn-default' data-original-title='编辑' onclick=\"fn_post_actions_edit('"+cl+"');\"><i class='fa fa-edit'></i>编辑</button>"+" ";
                        de="<button class='btn btn-default' data-original-title='删除' onclick=\"fn_post_actions_delete('"+cl+"');\"><i class='fa fa-times'></i>删除</button>"+" ";
                        jQuery("#ajax_post_table").jqGrid('setRowData',ids[i],{act:se+de});
                    }
                    $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                    jqGridStyle();
                },
    	        jsonReader: {
    			    root: "dataRows",
    			    page: "page",
    			    total: "total",
    			    records: "records",
    			    repeatitems : false
    			},
    			multiselect : false,
                rownumbers:true,
                gridview:true,
                shrinkToFit:true,
                viewrecords: true,
                autowidth: true,
                height:'auto',
                forceFit:true,
                loadComplete: function() {
                }
            });
            $(window).on('resize.jqGrid', function() {
                jQuery("#ajax_post_table").jqGrid('setGridWidth', $("#ajax_post_list_row").width());
            });
			jQuery("#ajax_post_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});
            jQuery("#ajax_post_table").jqGrid('navGrid', "#ajax_post_list_page", {
                edit : false,
            	add : false,
            	del : false,
            	search:false
            });
	};

	function fn_post_actions_edit(id){
        gDialog.fCreate({
		    title:'岗位信息编辑',
		    url:"../com/ajax-post!read.action?keyId=" +id,
		    width:500
		}).show();
	};

	function fn_post_actions_delete(id){
	    fn_delete_post(id);
	};
</script>
