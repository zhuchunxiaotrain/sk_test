<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<ul class="notification-body">
	<s:if test="taskArrayList.size() > 0">
		<s:iterator value="taskArrayList" id="list">
			<li>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="checkbox style-0">
                                    <span class="col-xs-12">
                                        <img style="border-radius: 50%;" height="30" width="30" alt="" src="<s:property value="getFile(#list.creator.id)" />">
                                            <s:property value="#list.creator.name"/>于<s:property value="#list.createDate"/>发起
                                            <span class="label label-primary"><s:property value="#list.pname"/></span>
                                            ，当前正处于
                                            <span class="label label-success">
                                                <s:property value="#list.taskname"/>
                                            </span>，当前状态为
                                            <span class="label label-warning">
                                                <s:property value="#list.state"/>
                                            </span>


                                    </span>

                    </label>
                </div>
			</li>
		</s:iterator>
	</s:if>
	<s:else>
		<h4> </h4>
	</s:else>
</ul>
<script>
	/*
    $("a[key=ajax_todo_action_edit]").click(function(){
        $("div.ajax-dropdown").hide();
        var url = $(this).attr("mpUrl");
        loadURL(url,$('#content'));
    });
	*/
</script>