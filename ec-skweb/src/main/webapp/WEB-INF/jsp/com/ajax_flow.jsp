<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="row" style="padding:2px">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<div>

				<a href="javascript:void(0);" id="ajax_flow_btn_add"  class="btn btn-default "><i class="fa fa-lg fa-plus"></i> 流程部署</a>

		</div>
	</div>
</div>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">
		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
				data-widget-colorbutton="false" 
				data-widget-togglebutton="false" 
				data-widget-deletebutton="false" 
				data-widget-fullscreenbutton="true" 
				data-widget-custombutton="false" 
				data-widget-sortable="false">

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class=" ">
                                <div class="row" id="ajax_flow_list_row">
                                    <table id="ajax_flow_table" class="table table-striped table-bordered table-hover">
                                    </table>
                                    <div id="ajax_flow_list_page">
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->
</section>
<!-- end widget grid -->


<script type="text/javascript">

	function loadInbox() {
		loadURL("../com/ajax!flow.action?keyId=", $('#content'));
	}
	
	function reload(){
		loadURL("../com/ajax!flow.action",$('#content'));
	}
    $("#ajax_flow_btn_add").click(function(){
        gDialog.fCreate({
		    title:'请上传流程部署文件',
		    url:"../com/ajax-flow!dialog.action?keyId=",
		    width:500
		}).show();
    });

    function hrefFormat(cellvalue, options, cell){
        var keyId = cell["id"];
        var href = "<%=path%>/com/ajax-running!list.action?keyId="+cell["id"];
        return "<a href='javascript:void(0);' onclick=openRunning('"+keyId+"') id="+keyId+" key='running_list'>"+cellvalue+"</a>";
    }
	function xmlFormat(cellvalue, options, cell){
		var keyId = cell["id"];
		var href = "<%=path%>/com/ajax-flow!readXml.action?keyId="+cell["id"];
		return "<a href='"+href+"' target='_blank' id="+keyId+" key='running_list'>"+cellvalue+"</a>";
	}
    $(function(){
            jQuery("#ajax_flow_table").jqGrid({
            	url:'../com/ajax-flow!list.action',
        	    datatype: "json",
            	colNames:['流程定义编号','流程名称',"流程Key","XML","流程版本","部署日期","查看","操作","id"],
            	colModel:[
           		    {name:'pid',index:'pid', width:50,sortable:true,search:true,searchoptions:{sopt:['cn']}},
           		    {name:'name',index:'name', width:50,formatter:hrefFormat,sortable:true,search:true,searchoptions:{sopt:['cn']}},
           		    {name:'key',index:'key', width:50,sortable:true,search:true,searchoptions:{sopt:['cn']}},
					{name:'xml',index:'xml', width:50,formatter:xmlFormat,sortable:true,search:true,searchoptions:{sopt:['cn']}},
           		    {name:'version',index:'version', width:50,sortable:true,search:true,searchoptions:{sopt:['cn']}},
           		    {name:'time',index:'time', width:50,sortable:true,search:true,searchoptions:{sopt:['cn']}},
					{name:'opt',index:'opt', width:200,sortable:false,fixed:true},
           		    {name:'act',index:'act', width:200,sortable:false,fixed:true},
           		    {name:'id',index:'id', width:50,hidden:true},
             	],
				loadonce: true,
                rowNum : 10,
                rowList:[10,20,30],
                pager : '#ajax_flow_list_page',
                sortname : 'time',
                sortorder : "desc",
                gridComplete:function(){
					$('.ui-search-oper').hide();
                    var ids=$("#ajax_flow_table").jqGrid('getDataIDs');
                    for(var i=0;i<ids.length;i++){
                        var cl=ids[i];
                        se="<button class='btn btn-default' data-original-title='流程图' onclick=\"fn_flow_actions_img('"+cl+"');\"><i class='fa fa-eye'></i>流程图</button>";
                        cf="<button class='btn btn-default bg-color-orange txt-color-white' data-original-title='配置' onclick=\"fn_flow_actions_config('"+cl+"');\"><i class='fa fa-gear'></i>配置</button>";
						gq="<button class='btn btn-default bg-color-red txt-color-white' data-original-title='挂起' onclick=\"fn_flow_actions_suspend('"+cl+"');\"><i class='fa fa-exclamation-circle'></i>挂起</button>";
						ck="<button class='btn btn-default bg-color-green txt-color-white' data-original-title='查看历史版本' onclick=\"fn_flow_actions_histroy('"+cl+"');\"><i class='fa fa-eye'></i>查看历史版本</button>";

						jQuery("#ajax_flow_table").jqGrid('setRowData',ids[i],{opt:ck});
						jQuery("#ajax_flow_table").jqGrid('setRowData',ids[i],{act:se+cf+gq});
                    }
                    $(".ui-jqgrid-bdiv").css("overflow-x","hidden");//暂时这样以后再想办法(浏览器问题chrome)
                    jqGridStyle();
                },
    	        jsonReader: {
    			    root: "dataRows",
    			    page: "page",
    			    total: "total",
    			    records: "records",
    			    repeatitems : false
    			},
				caption : "<i class='fa fa-arrow-circle-right'></i> 流程部署信息",
    			multiselect : false,
                rownumbers:true,
                gridview:true,
                shrinkToFit:true,
                viewrecords: true,
                autowidth: true,
                height:'auto',
                forceFit:true,
                loadComplete: function() {
                }
            });
            $(window).on('resize.jqGrid', function() {
                jQuery("#ajax_flow_table").jqGrid('setGridWidth', $("#ajax_flow_list_row").width());
            });
			jQuery("#ajax_flow_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});
            jQuery("#ajax_flow_table").jqGrid('navGrid', "#ajax_flow_list_page", {
                edit : false,
            	add : false,
            	del : false,
            	search:false
            });
    });

    function openRunning(id){
        loadURL("../com/ajax-running!list.action?keyId="+id,$('#content'));
    }
    function fn_flow_actions_img(id){
        gDialog.fCreate({
		    title:'查看流程图',
		    url:"../com/ajax-flow!export.action?keyId="+id,
		    width:1000
		}).show();
    }
    function fn_flow_actions_config(id){
        loadURL("../com/ajax-config!input.action?keyId="+id+"&param=1",$('#content'));
    }
	function fn_flow_actions_suspend(id){
		//loadURL("ajax-flow!suspend.action?keyId="+id,$('#content'));
		$.SmartMessageBox({
			title : "<i class='fa fa-plus-square txt-color-orangeDark'></i>  挂起  <span class='txt-color-orangeDark'><strong> 流程，请谨慎操作！ </strong></span> ",
			content : "请确认是否执行挂起流程操作",
			buttons : "[取消][确定]"
		}, function(ButtonPress, Value) {
			if(ButtonPress=="确定"){
				var vActionUrl = "<%=path%>/com/ajax-flow!suspend.action";
				data={keyId:id};
				ajax_action(vActionUrl,data,{},function(pdata){
					_show(pdata);
				});
				jQuery("#ajax_flow_table").trigger("reloadGrid");
			}
		});
	}

	function fn_flow_actions_histroy(id){
		loadURL("../com/ajax-flow!showhistory.action?keyId="+id,$('#content'));
	}
</script>

