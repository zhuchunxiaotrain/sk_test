<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <form id="role_input" class="form-horizontal" action="" method="post">
                <input type="hidden" name="keyId" id="keyId" value="<s:property value="role.id" />">
                <fieldset>
                    <s:if test="role!=null">
                    <legend>
                        <span class="label  label-warning"> [角色编辑]</span>
                    </legend>
                    </s:if>
                    <s:else>
                    <legend>
                        <span class="label  label-warning"> [角色添加]</span>
                    </legend>
                    </s:else>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">名称</label>
                        <div class="col-md-9">
                            <input class="form-control" name="name" id="name" placeholder="请输入名称"
                                type="text" value="<s:property value="role.name" />">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="pName">别名</label>
                        <div class="col-md-9">
                            <input class="form-control" name="pName" id="pName" placeholder="请输入别名"
                                type="text" value="<s:property value="role.pName" />">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="description">描述</label>
                        <div class="col-md-9">
                            <input class="form-control" name="description" id="description" placeholder="请输入描述"
                                type="text" value="<s:property value="role.description" />">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="usersName">
                            <a  href="javascript:void(0);" key="ajax_user_group_list">用户</a>
                        </label>
                        <div class="col-md-9">
                                <input name="usersId" id="usersId"
                                       type="hidden" value="<s:iterator id="list" value="usersSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>">
                                <input  id="usersName" placeholder="请选择" disabled class="form-control"
                                        type="text" value="<s:iterator id="list" value="usersSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="departName">
                            <a  href="javascript:void(0);" key="ajax_depart_group_list">部门</a>
                        </label>
                        <div class="col-md-9">
                                <input name="departId" id="departId"
                                       type="hidden" value="<s:iterator id="list" value="departmentsSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>">
                                <input  id="departName" placeholder="请选择" disabled class="form-control"
                                        type="text" value="<s:iterator id="list" value="departmentsSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="powerName">
                            <a  href="javascript:void(0);" key="ajax_post_group_list">职权</a>
                        </label>
                        <div class="col-md-9">
                                <input name="powerId" id="powerId"
                                       type="hidden" value="<s:iterator id="list" value="powersSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>">
                                <input  id="powerName" placeholder="请选择" disabled class="form-control"
                                        type="text" value="<s:iterator id="list" value="powersSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.department.name"/>-<s:property value="#list.post.name"/>,</s:if></s:iterator>">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
	<a href="#../com/ajax!role.action" class="btn btn-primary" id="dialog-role-ok">确定</a>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>

    $("#dialog-role-ok").unbind("click").bind("click",function(){
        var actionUrl = "<%=path%>/com/ajax-role!save.action";
        form_save("role_input",actionUrl);
        gDialog.fClose();
        jQuery("#ajax_role_table").trigger("reloadGrid");
    });


    $("a[key=ajax_user_group_list]").unbind("click").bind("click",function(){

        gDialog.fCreate({
           title:"请选择人员",
           url:"../com/ajax-dialog!user.action",
           width:340
        }).show();
    });
    $("a[key=ajax_depart_group_list]").unbind("click").bind("click",function(){
        gDialog.fCreate({
           title:"请选择部门",
           url:"../com/ajax-dialog!mdepart.action",
           width:500
        }).show();

    });
    $("a[key=ajax_post_group_list]").unbind("click").bind("click",function(){
        gDialog.fCreate({
           title:"请选择职权",
           url:"../com/ajax-dialog!power.action",
           width:340
        }).show();
    });
</script>