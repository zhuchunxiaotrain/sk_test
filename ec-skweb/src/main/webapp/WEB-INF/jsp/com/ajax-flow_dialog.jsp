<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">

            <h5><label style="width:100%"> 提示：请选择流程部署文件
             </label>
            </h5>
            </div>
        </div>
        <div class="col-md-12">
            <form class="form-horizontal" action="" method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-md-3 control-label" >请上传流程部署文件</label>
                        <div class="col-md-9">
                            <input class="form-control" name="uploadify" id="file_upload_1" placeholder=""
                                type="file" >
                        </div>
                    </div>
                </fieldset>
            </form>
            <div id="import_message_alert">

            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
	<button class="btn" class="btn btn-primary" id="dialog-ok">确定</button>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>
$(function() {
	$("#file_upload_1").uploadify({
		height        : 30,
		multi : false,
		fileSizeLimit : '5MB',
		fileDataName : 'uploadify',
		progressData : 'percentage',
		'fileTypeDesc' : '格式:zip',     //描述
		fileTypeExts : '*.zip',
		buttonText : '选择zip文件上传',
		swf           : '../resource/com/js/plugin/uploadify/uploadify.swf',
		uploader      : '../com/ajax-flow!deploy.action',
		width         : 140,
		onUploadStart:function(file){
			$("#import_message_alert").empty();
			$('<div class="alert alert-info fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-info"></i><strong>正在努力为您处理！</strong> 导入过程分校验和导入两部分，请耐心等待...</div>').appendTo($("#import_message_alert"));
            
		},
		onUploadSuccess : function(file, data, response) {
            console.debug("data:"+data);
			$("#import_message_alert").empty();
			var json = jQuery.parseJSON(data);
            if(json && json.state == "200"){
            	$('<div class="alert alert-success fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-check"></i><strong>上传完毕</strong> 数据导入成功！</div>').appendTo($("#import_message_alert"));
            	$(json.rows).each(function(index,map){
                	$('<div class="alert alert-info fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-info"></i><strong>提醒！</strong> '+map.id+'</div>').appendTo($("#import_message_alert"));
                });
            }else{
            	$('<div class="alert alert-info fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-info"></i><strong>上传失败！</strong> 请按照下面的错误清单调整CSV文件后重新上传！</div>').appendTo($("#import_message_alert"));

            	$(json.rows).each(function(index,map){
                	$('<div class="alert alert-danger fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-times"></i><strong>错误！</strong> '+map.id+'</div>').appendTo($("#import_message_alert"));
                });

            }
		}
	});
});
$("#dialog-ok").unbind("click").bind("click",function(){
        gDialog.fClose();
        loadURL("../com/ajax!flow.action?keyId=", $('#content'));
    });
</script>