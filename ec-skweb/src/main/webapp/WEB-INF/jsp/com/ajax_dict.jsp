<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="inbox-nav-bar no-content-padding">
	<h1 class="page-title txt-color-blueDark hidden-tablet"><i class="fa fa-fw fa-list"></i> 系统字典 &nbsp;</h1>
	<div class="inbox-checkbox-triggered">
		<div class="btn-group">
			<h5 class=" txt-color-blueDark hidden-tablet">当前字典：<i class="fa fa-fw fa-arrow-right"></i> <span id="ajax_dict_name_selected"> 正在加载...</span>  &nbsp;</h5>
		</div>
	</div>


</div>

<div id="inbox-content" class="inbox-body no-content-padding">
	<div class="inbox-side-bar">
		<a href="javascript:void(0);" id="dict_new_option" class="btn btn-primary btn-block"> <strong>添加新选项</strong> </a>

		<h6> 字典列表 <a href="javascript:loadInbox();" rel="tooltip" title="" data-placement="right" data-original-title="刷新选项" class="pull-right txt-color-darken"><i class="fa fa-refresh"></i></a></h6>

		<ul class="inbox-menu-lg" id="ajax_dict_menulist">
            <li>
                <a key="NoticeType" href="javascript:void(0)">公告类型</a>
            </li>
            <li>
                <a key="Post"  href="javascript:void(0);">招聘岗位</a>
            </li>
            <li>
                <a key="Edu"  href="javascript:void(0);">最高学历</a>
            </li>
            <li>
                <a key="Branch"  href="javascript:void(0);">所在支部</a>
            </li>
            <li>
                <a key="PostCategory"  href="javascript:void(0);">岗位类别</a>
            </li>
            <li>
                <a key="ActivityType"  href="javascript:void(0);">活动类型</a>
            </li>
            <li>
                <a key="ActivityBranch"  href="javascript:void(0);">活动支部</a>
            </li>
            <li>
                <a key="AnnualBudgetItem"  href="javascript:void(0);">年度预算科目</a>
            </li>
            <li>
                <a key="LetterType"  href="javascript:void(0);">信访类别</a>
            </li>
            <li>
                <a key="TrainType"  href="javascript:void(0);">培训类型</a>
            </li>
            <li>
                <a key="DispatchType"  href="javascript:void(0);">发文类别</a>
            </li>
            <li>
                <a key="ReceiveType"  href="javascript:void(0);">收文类别</a>
            </li>

            <li>
                <a key="MeetName"  href="javascript:void(0);">申请会议室</a>
            </li>

            <li>
                <a key="MeetType"  href="javascript:void(0);">会议类型</a>
            </li>
            <li>
                <a key="DataType"  href="javascript:void(0);">资料类别</a>
            </li>
            <li>
                <a key="AuditType"  href="javascript:void(0);">审计类型</a>
            </li>
            <li>
                <a key="AuditOffice"  href="javascript:void(0);">审计事务所</a>
            </li>
            <li>
                <a key="QualificationsName"  href="javascript:void(0);">资质名称</a>
            </li>
            <li>
                <a key="SealName"  href="javascript:void(0);">印章名称</a>
            </li>
            <li>
                <a key="CarBrand"  href="javascript:void(0);">车辆品牌</a>
            </li>
            <li>
                <a key="CarType"  href="javascript:void(0);">车辆型号</a>
            </li>
            <li>
                <a key="CarNo"  href="javascript:void(0);">车辆号牌</a>
            </li>
            <li>
                <a key="ArchivesType"  href="javascript:void(0);">档案类型</a>
            </li>
            <li>
                <a key="ArchivesCategory"  href="javascript:void(0);">档案类目</a>
            </li>
            <li>
                <a key="ScrapReason"  href="javascript:void(0);">报废原因</a>
            </li>
		</ul>
	</div>

	<div class="table-wrap custom-scroll animated fast fadeInRight">

	</div>
</div>

<script type="text/javascript">
    pageSetUp();
    loadScript("../resource/com/js/plugin/delete-table-row/delete-table-row.js");

    tableHeightSize()
    $(window).resize(function() {
        tableHeightSize()
    })
    function tableHeightSize() {
        var tableHeight = $(window).height() - 172;
        $('.table-wrap').css('height', tableHeight + 'px');
    }

    //初始化部门显示信息
    var ajax_dictName = "ClientType";
    $("#ajax_dict_name_selected").html("客户性质");
    /*
     * LOAD INBOX MESSAGES
     */
    loadInbox();
    function loadInbox() {
        loadURL("<%=path %>/com/ajax-dict!tree.action?dictName="+ajax_dictName, $('#inbox-content > .table-wrap'))
    }
    /*
     * Buttons (compose mail and inbox load)
     */
    $(".inbox-load").click(function() {
        loadInbox();
    });

    $("#dict_new_option").click(function(e) {
        var text = $("ul#ajax_dict_menulist li.active").text();
        $.SmartMessageBox({
            title : "<i class='fa fa-plus-square txt-color-orangeDark'></i>  添加  <span class='txt-color-orangeDark'><strong> "+text+"</strong></span>",
            content : "请输入一个新选项名称",
            buttons : "[取消][确定]",
            input : "text",
            placeholder : "输入名称..."
        }, function(ButtonPress, Value) {
            //alert(ButtonPress + " " + Value);
            if(ButtonPress=="确定"){
                save("","save",Value);
            }
        });
        e.preventDefault();
    })
    $("#ajax_dict_menulist li").click(function() {
        $("#ajax_dict_menulist li.active").removeClass("active");
        $(this).addClass("active");
        $("#ajax_dict_name_selected").html($(this).text());
        ajax_dictName = $("a",$(this)).attr("key");
        loadInbox();
    });

    function fn_create(){

    }
    function fn_edit(id,did){
        $.SmartMessageBox({
            title : "编辑该选项",
            content : "请输入一个新名称",
            buttons : "[取消][确定]",
            input : "text",
            placeholder : "输入名称..."
        }, function(ButtonPress, Value) {
            //alert(ButtonPress + " " + Value);
            if(ButtonPress=="确定"){
                save(id,"edit",Value,did);
            }
        });
    }
    function fn_delete(id,did){
        $.SmartMessageBox({
            title : "删除该选项",
            content : "确定删除该选项吗？",
            buttons : '[取消][确定]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "确定") {
                save(id,"delete","",did);

            }
        });
    }
    function fn_disabled(id,did){
        $.SmartMessageBox({
            title : "禁用该选项",
            content : "确定禁用该选项吗？",
            buttons : '[取消][确定]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "确定") {
                save(id,"disabled","",did);
            }
        });
    }
    function fn_enable(id,did){
        $.SmartMessageBox({
            title : "启用该选项",
            content : "确定启用该选项吗？",
            buttons : '[取消][确定]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "确定") {
                save(id,"enable","",did);
            }
        });
    }

    function save(id,action,name,did){
        //ajax执行返回时触发
        //var rowId = $("#tree").jqGrid('getGridParam','selrow');
        var superNode = $.trim($("#superNode").val());
        var vActionUrl = "<%=path %>/com/ajax-dict!method.action";
        var data = {"dictName":ajax_dictName, "keyId":id,"type":action,"name":name,"did":did,"superNode":superNode};
        ajax_action(vActionUrl,data,null,function(data){
            _show(data);
            loadInbox();
        })
    }

</script>
