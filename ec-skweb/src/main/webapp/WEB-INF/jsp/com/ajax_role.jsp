<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="row" style="padding:2px">
	<div class="col-sm-12 col-md-4 col-lg-4">
			<h5><label style="width:100%"> <span class="label  label-success"> 提示：可以添加、删除、修改群组信息</span> </label>
			</h5><a id="ajax_role_btn_add" class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 添加群组</a>
	</div>
</div>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false"
				data-widget-colorbutton="false" 
				data-widget-togglebutton="false" 
				data-widget-deletebutton="false" 
				data-widget-fullscreenbutton="true" 
				data-widget-custombutton="false" 
				data-widget-sortable="false">
				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="col-sm-12 col-md-12 col-lg-12">
                            <div class=" ">
                                <div class="row" id="ajax_role_list_row">
						            <table id="ajax_role_table" class="table table-striped table-bordered table-hover">
						            </table>
						            <div id="ajax_role_list_page">
						            </div>
						         </div>
						    </div>
                        </div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<div id="dialog-role_message">

</div><!-- #dialog-message -->

<div id="dialog-message">

</div><!-- #dialog-message -->

<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();
	

	function reload(){
		loadURL("../com/ajax!role.action",$('#content'));
	}
	
	//添加角色
	$('#ajax_role_btn_add').click(function() {
	    var this_tr = $(this).parent().parent();
        gDialog.fCreate({
		    title:'添加群组信息',
		    url:"../com/ajax-role!input.action?keyId=" + $(this_tr).attr("id"),
		    width:500
		}).show();
	});

	$("#dialog-role_message").dialog({
		autoOpen : false,
		modal : true,
		title : "成员信息框",
		width:500,
		buttons : [{
			html : "取消",
			"class" : "btn btn-default",
			click : function() {
				$(this).dialog("close");
			}
		}, {
			html : "<i class='fa fa-check'></i>&nbsp; 提交",
			"class" : "btn btn-primary",
			click : function() {
			    var actionUrl = "<%=path%>/com/ajax-role!save.action";
			    var opt={dialogId:'dialog-role_message'};
			    form_save("role_read",actionUrl,opt);
			    loadInbox();

			}
		}]

	});

</script>
<script type="text/javascript">
	pageSetUp();
	function load_jqGrid(){
        jQuery("#ajax_role_table").jqGrid({
            	url:'../com/ajax-role!list.action',
        	    datatype: "json",
            	colNames:["别名",'名称',"描述","操作","id"],
            	colModel:[
           		    {name:'pName',index:'pName', width:50,sortable:false,search:false},
           		    {name:'name',index:'name', width:50},
           		    {name:'description',index:'description', width:50},
           		    {name:'act',index:'act', width:200,sortable:false,search:false,fixed:true},
           		    {name:'id',index:'id', width:50,hidden:true}
             	],
                rowNum : 10,
                rowList:[10,20,30],
                pager : '#ajax_role_list_page',
                sortname : 'id',
                sortorder : "desc",
                gridComplete:function(){
                    var ids=$("#ajax_role_table").jqGrid('getDataIDs');
                    for(var i=0;i<ids.length;i++){
                        var cl=ids[i];
                        se="<button class='btn btn-default' data-original-title='编辑' onclick=\"fn_role_actions_edit('"+cl+"');\"><i class='fa fa-eye'></i>编辑</button>"+" ";
                        de="<button class='btn btn-default' data-original-title='删除' onclick=\"fn_role_actions_delete('"+cl+"');\"><i class='fa fa-times'></i>删除</button>"+" ";
                        jQuery("#ajax_role_table").jqGrid('setRowData',ids[i],{act:se+de});
                    }
                    $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                    jqGridStyle();
                },
    	        jsonReader: {
    			    root: "dataRows",
    			    page: "page",
    			    total: "total",
    			    records: "records",
    			    repeatitems : false
    			},
                caption : '群组信息	',
    			multiselect : false,
                rownumbers:true,
                gridview:true,
                shrinkToFit:true,
                viewrecords: true,
                autowidth: true,
                height:'auto',
                forceFit:true,
                loadComplete: function() {
                }
            });
		jQuery("#ajax_role_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});
		jQuery("#ajax_role_table").jqGrid('navGrid', "#ajax_role_list_page", {
			edit : false,
			add : false,
			del : false,
			search:false
		});
	}
    $(function(){
        load_jqGrid();
    });
    $(window).on('resize.jqGrid', function() {
        jQuery("#ajax_role_table").jqGrid('setGridWidth', $("#ajax_role_list_row").width());
    })


    //编辑
    function fn_role_actions_edit(id){
        gDialog.fCreate({
		    title:'编辑群组信息',
		    url:"../com/ajax-role!input.action?keyId=" +id,
		    width:500
		}).show();
    }
    function fn_role_actions_delete(id){
        //删除
        $.SmartMessageBox({
    		title : "<i class='fa fa-minus-square txt-color-orangeDark'></i> <span class='txt-color-orangeDark'><strong>删除群组</strong></span>",
    		content : "确定删除该群组吗？",
    		buttons : '[取消][确定]'
    	}, function(ButtonPressed) {
    		if (ButtonPressed === "确定") {
    			var vActionUrl = "<%=path%>/com/ajax-role!delete.action";
    			data={keyId:id};
    			ajax_action(vActionUrl,data,{},function(pdata){
    				_show(pdata);
    			});
    			jQuery("#ajax_role_table").trigger("reloadGrid");
    		}
    	});
    }
    function fn_role_config(){
        $("#content").hide("blind",function(){
            loadURL("../com/ajax-role!config.action",$('#content'));
            $("#content").show();
        })

    }
</script>