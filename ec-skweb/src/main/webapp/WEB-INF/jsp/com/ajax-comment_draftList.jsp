<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<ul class="notification-body ">
	<s:if test="taskArrayList.size() > 0">
		<s:iterator value="taskArrayList" id="list">
			<input type="hidden" id="keyId" value="<s:property value="#list.docId"/>"/>
			<li>
				<span class="">
					<a key="ajax_draft_action_edit" href="javascript:void(0);" mpUrl="<s:property value="#list.url"/>?keyId=<s:property value="#list.docId"/>" class="msg">
						<img src="../resource/com/img/avatars/male.png" alt="" class="air air-top-left margin-top-5" width="40" height="40" />
						<span class="from"> <i class="fa fa-user txt-color-green"></i>  <s:property value="#list.creater.name"/> </span>
						<time><s:date name="#list.time" format="yyyy-MM-dd HH:mm" /></time>
						<span class="subject">
						    <i class="fa fa-volume-down"></i>
						    表单名称：<s:property value="#list.pName"/><a key="ajax_draft_delete" data="<s:property value="#list.key"/>" style="margin-top:-19px;" class="pull-right" data-toggle="modal"><i class="fa fa-trash-o"></i></a>
						</span>
					</a>
				</span>
			</li>
		</s:iterator>
	</s:if>
	<s:else>
		<h4> <i class="fa fa-warning"></i> 我还没有发表任何草稿信息！</h4>
	</s:else>
</ul>
<script>
    $("a[key=ajax_draft_action_edit]").click(function(){
        $("div.ajax-dropdown").hide();
        var url = $(this).attr("mpUrl");
        loadURL(url,$('#content'));
    });

	$("a[key=ajax_draft_delete]").click(function(){
		var vActionUrl = "<%=path%>/com/ajax-flow!draftDelete.action";
		data={keyId:$("#keyId").val(),key:$(this).attr("data")};
		ajax_action(vActionUrl,data,{},function(pdata){
			_show(pdata);
		});
		loadURL("ajax-flow!draftList.action",$(".ajax-notifications.custom-scroll"));
	});
</script>

