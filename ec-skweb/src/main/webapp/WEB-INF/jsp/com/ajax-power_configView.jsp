<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<div class="modal-body">
<fieldset>
	<legend>
	  <span class="label  label-warning "> [请选择部门岗位信息]</span>
	</legend>
	<table>
	    <tr>
	        <td>
	            <ul class="list-group td-ul" id="ajax_power_config_depart">
                        <s:iterator value="dArrayList" id="list">
                           <li id="<s:property value="#list.id"/>" class="list-group-item td-li">
                                <div class="checkbox">
                                    <label>
                                      <input id="<s:property value="#list.id"/>" type="checkbox" class="checkbox style-0" <s:property value="#list.checked"/>>
                                      <span><s:property value="#list.name" /></span>
                                    </label>
                                </div>

                            </li>
                        </s:iterator>
                </ul>
	        </td>
	        <td>
                <ul class="list-group td-ul" id="ajax_power_config_post">

                </ul>

	        </td>
	    </tr>
	</table>
</fieldset>
</div>
<div class="modal-footer">
	<a href="#ajax!power.action" class="btn btn-primary" id="dialog-power-ok">确定</a>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>
	var actionUrl = "../com/ajax-users!configPost.action";
	var keyId = $("input#powerId").val();
	multList("ajax_power_config_depart","ajax_power_config_post",actionUrl,{keyId:keyId});
    $("#dialog-power-ok").unbind("click").bind("click",function(){
        var rowId= getRowIds("ajax_power_config_post");
        var rowNames=getRowNames("ajax_power_config_post");
        $("#dp_name").val(rowNames);
        $("#dp_id").val(rowId);
        console.debug($("#dp_name").val());
        console.debug($("#dp_id").val());
        gDialog.fClose();
        jQuery("#ajax_power_table").trigger("reloadGrid");
    });

</script>
