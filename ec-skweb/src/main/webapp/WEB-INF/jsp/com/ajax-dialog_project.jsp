<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_project_list_row">
      <table id="ajax_project_list_table">
      </table>
      <div id="ajax_project_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script tex>
  function run_jqgrid_function(){
    jQuery("#ajax_project_list_table").jqGrid({
      url:'ajax-manageproinfo!listfordialog.action',
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['项目信息名称','Id','no','buildCompany','buildCompanyAddr','constructUnit','buildAddress','clientId'],
      colModel : [
        {name:'name',index:'name', width:410,sortable:false,search:true,sorttype:'string'},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true},
        {name:'no',index:'no',hidden:true},
        {name:'buildCompany',index:'buildCompany',hidden:true},
        {name:'buildCompanyAddr',index:'buildCompanyAddr',hidden:true},
        {name:'constructUnit',index:'constructUnit',hidden:true},
        {name:'buildAddress',index:'buildAddress',hidden:true},
        {name:'clientId',index:'clientId',hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_project_list_page',
      sortname : 'createDate',
      sortorder : "desc",
      gridComplete:function(){
        var param=$("#param").val();
        var proInfoId=$("#proInfoId").val();
        $("#ajax_project_list_table").jqGrid('setSelection',proInfoId);
        jqGridStyle();
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      },
      onSelectRow: function (rowId, status, e) {
          var rowId = $("#ajax_project_list_table").jqGrid('getGridParam','selrow');
          var rowDatas = $("#ajax_project_list_table").jqGrid('getRowData', rowId);
          $("#contractName").val(rowDatas["name"]);
          $("#proInfoId").val(rowId);
          $("#projectNo").val(rowDatas["no"]);
          $("#projectId").val(rowId);
          $("#projectName").val(rowDatas["name"]);
          $("#buildCompany").val(rowDatas["buildCompany"]);
          $("#buildCompanyAddr").val(rowDatas["buildCompanyAddr"]);
          $("#clientId").val(rowDatas["clientId"]);
          $("#constructUnit").val(rowDatas["constructUnit"]);
          $("#constructUnitAddress").val(rowDatas["buildAddress"]);
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_project_list_table").jqGrid('setGridWidth', $("#ajax_project_list_row").width()-10);
    })

    jQuery("#ajax_project_list_table").jqGrid('navGrid', "#ajax_project_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_project_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
    gDialog.fClose();
  });
</script>
