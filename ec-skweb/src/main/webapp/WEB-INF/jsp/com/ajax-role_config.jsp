<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<input type="hidden" name="keyId" id="keyId" value="">
<div class="row" style="padding:2px">
    <div class="col-sm-12 col-md-4 col-lg-4">
        <div class="form-group">
            <h5><label style="width:100%"> <span class="label  label-success"> 提示：可以添加、删除、修改员工信息</span> </label>
            </h5><a id="ajax_role_back" class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-reply-all"></i> 角色管理</a>
        </div>
    </div>
</div>
<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-togglebutton="false"
                 data-widget-deletebutton="false"
                 data-widget-fullscreenbutton="true"
                 data-widget-custombutton="false"
                 data-widget-sortable="false">

                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>权限分配 </h2>
                    <div class="widget-toolbar">
                    </div>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <ol class="dd-list">
                                    <s:iterator value="roleList" id="list">
                                        <li class="dd-item dd3-item" data-id="<s:property value="#list.id"/>" data-name="<s:property value="#list.name"/>">
                                            <div class="dd-handle dd3-handle">Drag</div>
                                            <div class="dd3-content" style="cursor:pointer">
                                                <s:property value="#list.name"/>
                                                <div id="pdiv" class="pull-right">
                                                    <a id="<s:property value="#list.id"/>" key="ajax_permission_config" title="关联资源" class="btn btn-danger  btn-xs" href="javascript:void(0);"><i class="fa fa-lg fa-cloud"></i>关联资源</a>
                                                </div>
                                            </div>
                                        </li>
                                    </s:iterator>

                                </ol>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-8" >
                                <div class=" " id="ajax_user_grid">
                                    <table id="ajax_user_table" class="table table-striped table-bordered table-hover">
                                    </table>
                                    <div id="ajax_user_page">
                                    </div>
                                </div>
                                <div id="ajax_permission_tree" style="display:none">
                                    <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false"
                                         data-widget-colorbutton="false"
                                         data-widget-togglebutton="false"
                                         data-widget-deletebutton="false"
                                         data-widget-fullscreenbutton="false"
                                         data-widget-custombutton="false"
                                         data-widget-sortable="false">
                                        <header>
                                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                            <h2>个人权限 </h2>
                                            <div class="widget-toolbar"></div>
                                        </header>
                                        <div>
                                            <div class="widget-body no-padding">
                                                <div class="row" >
                                                    <div class="col-sm-12">
                                                        <ul id="tree" class="ztree" style="max-height:500px;min-width:340px; overflow:auto;"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->

<div id="dialog-users_message">

</div><!-- #dialog-message -->

<div id="dialog-message">

</div><!-- #dialog-message -->

<script type="text/javascript">
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    pageSetUp();
    function reload(){
        loadURL("../com/ajax!role.action",$('#content'));
    }


</script>
<script type="text/javascript">
    function imgFormat(cellvalue, options, cell){
        if(cellvalue != ""){
            var imgUrl = '<img style="border-radius: 50%;" height="30" width="30" alt="" src="'+cellvalue+'">';
        }else{
            var imgUrl = '<img style="border-radius: 50%;" height="30" width="30" alt="" src="../resource/com/img/avatars/male.png">';
        }
        return imgUrl;
    }
    function zTreeOnCheck(event, treeId, treeNode){
        var data = {'keyId':treeNode.roleId,'permissionId':treeNode.id};
        if(treeNode.checked){
            ajax_action("../com/ajax-role!permissionAddConfig.action",data,{},function(pdata){
                _show(pdata);
            });
        }else{
            ajax_action("../com/ajax-role!permissionRemoveConfig.action",data,{},function(pdata){
                _show(pdata);
            });
        }

    }

    function load_jqGrid(){
        jQuery("#ajax_user_table").jqGrid({
            url:'../com/ajax-role!userGrid.action',
            mtype:"POST",
            datatype: 'json',
            postData:{keyId :$("ol.dd-list li:first").attr("data-id")},
            page : 1,
            colNames:['头像','用户名 ', '帐号','操作','Id','roleId','selected'],
            colModel : [
                {name:'userImg',index:'userImg', width:50,sortable:false,formatter:imgFormat},
                {name:'name',index:'name', width:50,sortable:false},
                {name:'email',index:'email', width:100,sortable:false},
                {name:'act',index:'act', width:250,sortable:false,fixed:true},
                {name:'id',index:'id',width:100,sortable:false,fixed:true,hidden:true},
                {name:'roleId',index:'roleId',width:100,sortable:false,fixed:true,hidden:true},
                {name:'selected',index:'selected',width:100,sortable:false,fixed:true,hidden:true},
            ],
            rowNum : 10,
            rowList:[10,20,30],
            pager : '#ajax_user_page',
            sortname : 'id',
            sortorder : "asc",
            gridComplete:function(){
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                var ids=$("#ajax_user_table").jqGrid('getDataIDs');
                for(var i=0;i<ids.length;i++){
                    var cl=ids[i];
                    var rowData =$("#ajax_user_table").jqGrid("getRowData",cl);
                    var roleId = rowData['roleId'];
                    var selected = rowData['selected'];
                    var de = "";
                    var se = "";
                    if(selected != ""){
                        de="<button class='btn btn-warning' data-original-title='移出该角色'  onclick=\"fn_user_add('"+cl+"','"+roleId+"');\"><i class='fa fa-lg fa-minus'></i> 移出该角色</button>";
                    }else{
                        se="<button class='btn btn-success' data-original-title='加入该角色' onclick=\"fn_user_add('"+cl+"','"+roleId+"');\"><i class='fa fa-lg fa-plus'></i> 加入该角色</button>"+" ";
                    }
                    jQuery("#ajax_user_table").jqGrid('setRowData',ids[i],{act:se+de});
                }


                jqGridStyle();
            },
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },
            caption : "用户信息 <i class='fa fa-arrow-circle-right'></i> "+$(".dd-list li:first").attr("data-name"),
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            forceFit:true,
            viewrecords: true,
            autowidth: true,
            height : 'auto',
            loadComplete: function() {
            }
        });
    }
    function loadCategoryTree(roleId){
        var setting = {
            view: {
                dblClickExpand: false,
                showLine: true,
                selectedMulti: true,
                nameIsHTML: true
            },
            data: {
                simpleData: {
                    enable:true,
                    idKey: "id",
                    pIdKey: "pId",
                    rootPId: ""
                }
            },
            check:{
                chkStyle:"checkbox",
                autoCheckTrigger:true,
                enable:true,
                chkboxType: { "Y": "ps", "N": "s" }
            },
            callback: {
                //onClick: onNodeClick1
                onCheck:zTreeOnCheck
            }
        };
        $.ajax({
            type: "get",
            url: 'ajax-role!zTree.action?keyId='+roleId,
            success: function(data){
                if(data.result.errorCode == "1"){
                    var t = $("#tree");
                    t = $.fn.zTree.init(t, setting, data.data.data);
                    t.expandAll(true);
                }
            }
        });
    }
</script>
<script type="text/javascript">
    $(function(){
        loadCategoryTree($("div#pdiv a:first").attr("id"));
        $("div#ajax_permission_tree").show('bounce');
    });
    $("a#ajax_role_back").unbind("click").bind("click",function(){
        loadURL("../com/ajax!role.action",$('#content'));
    })
    $("a[key='ajax_permission_config']").unbind("click").bind("click",function(){
        var keyId = $(this).attr("id");
        $("div#ajax_user_grid").hide('blind',function(){
            loadCategoryTree(keyId);
            $("div#ajax_permission_tree").show('bounce');
        });
    })
    $("a[key='ajax_user_config']").unbind("click").bind("click",function(){
        var keyId = $(this).attr("id");
        var name=$(this).attr("name");
        $("div#ajax_permission_tree").hide('fold',function(){
            $("#ajax_user_table").jqGrid("setCaption","用户列表 <i class='fa fa-arrow-circle-right'></i>"+name);
            $("#ajax_user_table").jqGrid('setGridParam',{postData:{keyId :keyId}});
            $("#ajax_user_table").trigger("reloadGrid");
            $("div#ajax_user_grid").show('blind');
        });
    });
</script>
