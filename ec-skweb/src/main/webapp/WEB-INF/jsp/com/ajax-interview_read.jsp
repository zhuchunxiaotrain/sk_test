<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<input type="text" name="numStatus" id="numStatus" hidden value="<s:property value="numStatus" />"/>
<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-interview" href="javascript:void(0)">
            <i class="fa fa-lg fa-mail-reply-all"></i> 面试登记
          </a>
          <div class="row">
            <shiro:hasAnyRoles name="wechat">
              <a style="margin-top: -13px !important;" <s:property value="isEdit(interview.id)"/> class="btn btn-default pull-right pull-right-fix" key="ajax_edit" href="javascript:void(0);"><i class="fa fa-lg fa-edit"></i> 编辑</a>
            </shiro:hasAnyRoles>
          </div>
          <hr class="simple">
          <form id="interview" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="InterviewKeyId" value="<s:property value="interview.id" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <input type="hidden" name="todo" id="todo"  value="<s:property value="todo" />"/>
            <input type="hidden" name="viewtype" id="viewtype"  value="<s:property value="viewtype" />"/>
            <input type="hidden" name="parentId" id="parentId"  value="<s:property value="parentId" />"/>
            <header  style="display: block;">
              面试登记记录&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                   应聘部门
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="interviewDepartName" name="interviewDepartName" value="<s:property value="interview.department.name"/>"/>
                      <input type="hidden" id="interviewDepartId" name="interviewDepartId" value="<s:property value="interview.department.id"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  应聘岗位
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="postName" id="postName"  value="<s:property value="interview.post.name"/>" >
                    <input type="hidden"  name="postId" id="postId" value="<s:property value="interview.post.id"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="name" id="name" value="<s:property value="interview.name"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  身份证
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input  type="text" name="cardID" id="cardID" disabled value="<s:property value="interview.cardID"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  面试日期
                </label>
                <section class="col col-5">
                  <label class="input state-disabled" >
                    <input  disabled id="interviewDate" name="interviewDate"
                            type="text" value="<s:date name="interview.interviewDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">面试记录</label>
                <section class="col col-5">
                  <label class="input state-disabled">
<%--
                    <input  name="uploadify" id="recordfileName" placeholder="" style="display: none"  type="file"/>
--%>
                    <input  name="uploadify" id="recordfileId" placeholder="" style="display: none" value="<s:property value="recordfileId"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同类型
                </label>
                <section class="col col-5">
                  <div class="inline-group" name="result">
                    <label class="radio">
                      <input type="radio" disabled checked="checked" name="interview.result" value="0" <s:property value="interview.result==0?'checked':''"/>>
                      <i></i>推荐录用</label>
                    <label class="radio">
                      <input type="radio" disabled name="interview.result" value="1" <s:property value="interview.result==1?'checked':''"/>>
                      <i></i>进入下一轮</label>
                    <label class="radio">
                      <input type="radio" disabled name="interview.result" value="2" <s:property value="interview.result==2?'checked':''"/>>
                      <i></i>淘汰</label>
                  </div>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                   面试人
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="usersName" name="interviewer"
                             value="<s:iterator id="list" value="interview.interviewer"><s:property value="#list.name"/>,</s:iterator>"/>
                      <input type="hidden" id="usersId" name="interviewer"
                             value="<s:iterator id="list" value="interview.interviewer"><s:property value="#list.id"/>,</s:iterator>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  备注
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="text" name="remark" id="remark" disabled value="<s:property value="interview.remark"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  操作人
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled type="text" name="creater" id="creater" value="<s:property value="interview.creater.name"/>" />
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  操作日期
                </label>
                <section class="col col-5">
                  <label class="input state-disabled" >
                    <input disabled type="text" placeholder="操作时间" value="<s:date name="interview.createDate" format="yyyy-MM-dd"/>">

                  </label>
                </section>
              </div>
            </fieldset>
          </form>
          <div class="flow">
            <s:if test="interview.getProcessState().name()=='Running'">
              <div class="f_title">审批意见</div>
              <div class="chat-footer"style="margin-top:5px">
                <div class="textarea-div">
                  <div class="typearea">
                    <textarea class="inputorr" placeholder="请填写审批意见..." name="comment" id="chat_textarea-expand"></textarea>
                  </div>
                </div>
                <span class="textarea-controls"></span>
              </div>
            </s:if>
            <div class="f_title"><i class="right" id="flow"></i>流程信息</div>
            <div class="f_content" style="display:none">
              <div id="interview_showFlow"></div>
            </div>
            <div class="f_title"><i class="right" id="next"></i>下一步骤提示</div>
            <div class="f_content" style="display:none">
              <div id="interview_showNext"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </article>
</div>

<script>
    //上传文件
    readLoad({
        objId:"recordfileId",
        entityName:"recordId",
        sourceId:"recordfileId"
    });

    //返回视图
    $("#btn-re-interview").click(function(){
        var index = "<s:property value="index" />";
        var todo = "<s:property value="todo" />";
        var remind = "<s:property value="remind" />";
        var record = "<s:property value="record" />";
        var draft = "<s:property value="draft" />";
        if(index==1){
            loadURL("ajax-index!page.action",$('#content'));
        }else if(todo==1){
            loadURL("ajax!toDoList.action",$('#content'));
        }else if(remind==1){
            loadURL("ajax!remindList.action",$('#content'));
        }else if(record==1){
            loadURL("ajax!taskRecordList.action?type=1",$('#content'));
        }else if(record==2){
            loadURL("ajax!taskRecordList.action?type=2",$('#content'));
        }else if(draft==1){
            loadURL("ajax!draftList.action",$('#content'));
        }else{
            loadURL("ajax-interview.action?parentId="+$("#parentId").val(),$('div#s2'));
        }

    });

    //编辑
    $("a[key=ajax_edit]").click(function(){
        var draft = "<s:property value="draft" />";
        loadURL("ajax-interview!input.action?keyId="+$("input#InterviewKeyId").val()+"&draft="+draft+"&parentId="+$("#parentId").val(),$('#content'));
    });

    //审批
    $(function(){
        loadURL("ajax-running!workflow.action?bussinessId="+$("input#InterviewKeyId").val()+"&type=flow",$('#interview_showFlow'));
        loadURL("ajax-running!workflow.action?bussinessId="+$("input#InterviewKeyId").val()+"&type=next",$('#interview_showNext'));
        ajax_action("ajax-config!operateType.action",{keyId: $("input#InterviewKeyId").val()},null,function(pdata){
            var showDuty = false;
            var area = $("span.textarea-controls");
            $(pdata.data.datarows).each(function(i,v){
                var str = '<button id="left_foot_btn_'+ v.action+'" data="" class="btn btn-sm btn-primary pull-right">'+ v.name+'</button>';
                $(area).append(str);
                if(v.action == "approve" || v.action=="sendback"  || v.action=="deny"){
                    showDuty = true;
                }
            });
            if(showDuty == true){
                var pdata = {
                    keyId: $("input#InterviewKeyId").val(),
                    flowName: "interview"
                };
                multiDuty(pdata);
            }
        });
        var valueObj=$("input#numStatus").val();
        //alert(valueObj)
        switch(valueObj){
            case "0":
                //保存后再提交
                $("#left_foot_btn_approve").off("click").on("click",function(){
                    form_save("interview","ajax-interview!commit.action?comment="+$("textarea#chat_textarea-expand").val(),null,function(){
                        $("#btn-re-interview").trigger("click");
                    })
                });
                break;
            
        }
        //流程信息展开
        $('#flow,#next').click(function(){
            if($(this).hasClass("right")){
                $(this).removeClass("right").addClass("down");
                $(this).parent(".f_title").next("div.f_content").show();
            }else{
                $(this).removeClass("down").addClass("right");
                $(this).parent(".f_title").next("div.f_content").hide();
            }
        });
    });

</script>
