<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="ajax-top.jsp" />
<input type="hidden" name="keyId" id="keyId" value="keyId"/>

<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
      <i class="fa fa-table fa-fw "></i>
      离职申请
    </h1>
  </div>
</div>
<div id="business_data">
  <div class="row" style="padding:2px;padding-bottom: 5px">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div >
        <form id="searchForm" method="post" action="">
          <shiro:hasAnyRoles name="wechat">
            <a id="ajax_dimission_btn_add" <s:property value="isCreate('dimission')"/> class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 离职申请</a>
          </shiro:hasAnyRoles>
        </form>
      </div>
    </div>
  </div>
  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_dimission1_list_row">
                    <table id="ajax_dimission1_table" class="table table-striped table-bordered table-hover">
                    </table>
                    <div id="ajax_dimission1_list_page">
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script >
    function run_jqgrid_function(){
        jQuery("#ajax_dimission1_table").jqGrid({
            url:'ajax-dimission1!list.action',
            mtype:"POST",
            datatype: 'json',
            page : 1,
            colNames:["姓名","年龄","性别","岗位","部门","用工性质","id"],
            colModel:[
                {name:'name',index:'name', search:false,width:80},
                {name:'age',index:'age', search:false,width:80},
                {name:'gender',index:'gender', search:false,width:80},
                {name:'post',index:'post', search:false,width:80},
                {name:'dep',index:'dep', search:false,width:80},
                {name:'nature',index:'nature', search:false,width:80},
                {name:'id',index:'id',search:false,hidden:true},
            ],
            rowNum : 10,
            rowList:[10,20,30],
            pager : '#ajax_dimission1_list_page',
            sortname : 'createDate',
            sortorder : "desc",
            gridComplete:function(){

                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
                jqGridStyle();
            },
            onSelectRow: function (rowId, status, e) {
              /*$(this).gridselect('onecheck',{
               table:"ajax_dimission1_table",
               rowId:rowId,
               status:status,
               id:"usersId",
               name:"usersName"
               });*/
                var slt =   $("#ajax_dimission1_table").jqGrid('getGridParam','selarrrow');
                if(slt.length > 1){
                    alert("请选择一条数据");
                    return false;
                }

                var rowId = $("#ajax_dimission1_table").jqGrid('getGridParam','selrow');
                var rowDatas = $("#ajax_dimission1_table").jqGrid('getRowData', rowId);
                $("#keyId").val(rowId);

            },
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },

            multiselect: true,
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            forceFit:true,
            viewrecords: true,
            autowidth: true,
            height : 'auto',
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_dimission1_table").jqGrid('setGridWidth', $("#ajax_dimission1_list_row").width());
        })

        jQuery("#ajax_dimission1_table").jqGrid('navGrid', "#ajax_dimission1_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });

        jQuery("#ajax_dimission1_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
    }

    $(function(){
        run_jqgrid_function();
    });

    $("#dialog-ok").unbind("click").bind("click",function(){

        gDialog.fClose();
    });

    function fn_dimission1_read(id){
        loadURL("ajax-renew!read.action?keyId="+id,$("#content"));
    }

    //新建
    $("#ajax_dimission_btn_add").off("click").on("click",function(){
            if($('#keyId').val()=="keyId"){
                alert("请选择离职的员工!");
            }else {
                $.ajax({
                    type: "get",
                    url:"ajax-dimission!checkUser.action?parentId="+$('#keyId').val(),
                    cache:false,
                    dataType:"json",
                    async:false,
                    success: function(data){
                        if(data.ifPersonnel=="1"){
                            loadURL("ajax-dimission!input.action?parentId="+$('#keyId').val(),$('#content'));
                        }else {
                            alert("请对应部门的人事专员操作!")
                        }

                    }
                });
            }
        });


</script>





















