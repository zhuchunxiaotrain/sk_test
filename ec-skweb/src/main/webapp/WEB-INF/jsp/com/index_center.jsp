<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/27
  Time: 9:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>上海市卫生产业开发中心-协同运营平台</title>
	<style>
		.tool_opcity1{
			opacity: 0.3;
			filter:alpha(opacity=30);
			-moz-opacity:0.3;
		}
		.tool_opcity2{
			opacity: 0.8;
			filter:alpha(opacity=80);
			-moz-opacity:0.8;
		}
		body{
			font-family: 微软雅黑 !important;
			overflow-x: hidden;
		}

		.data_layout {
			display: none;
			width: 100%;
			height: 100%;
			z-index: 1001;
			text-align:center;
			background:#fff;
			position:absolute;
			top:0;
			left:0;
			opacity:0.6;
			filter:alpha(opacity=60);
		}

		.data_layout img{
			margin-top: 25%;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="../resource/com/css/newframe/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../resource/com/css/newframe/nanoscroller.css">
	<link rel="stylesheet" type="text/css" href="../resource/com/css/newframe/form.css">
	<link rel="stylesheet" type="text/css" href="../resource/jquery/Jcrop/jquery.Jcrop.css">
	<link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/demo.css?v=20150511">
	<link href="../resource/com/umeditor/themes/default/css/umeditor.min.css" type="text/css" rel="stylesheet">
	<link href="../resource/list.css" type="text/css" rel="stylesheet">
	<link href="../resource/jshow/css/bootstrap-modal-bs3patch.css" type="text/css" rel="stylesheet">
	<link href="../resource/jshow/css/bootstrap-modal.css" type="text/css" rel="stylesheet">
	<link href="../resource/multiselect/css/bootstrap-multiselect.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="../resource/com/js/plugin/ztree/css/zTreeStyle/metro.css">
	<link rel="stylesheet" href="../resource/activiti/ipicture/iPicture.css">
	<link rel="stylesheet" href="../resource/activiti/activitiSmart.css">
	<link rel="stylesheet" href="../resource/activiti/orgChart/css/jquery.jOrgChart.css">

	<link rel="stylesheet" type="text/css" href="../resource/com/css/newframe/select2.css">
	<link rel="stylesheet" type="text/css" href="../resource/com/css/newframe/theme_styles.css">

	<link rel="stylesheet" href="../resource/com/css/newframe/daterangepicker.css" type="text/css">
	<link rel="stylesheet" href="../resource/com/css/newframe/jquery-jvectormap-1.2.2.css" type="text/css">
	<link rel="stylesheet" href="../resource/com/css/newframe/weather-icons.css" type="text/css">
	<!-- jqGrid CSS -->
	<%--<link rel="stylesheet" type="text/css" media="screen" href="../resource/css/jqgrid-ui.css">--%>
	<link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/jqgrid-ui.css">


	<!-- calendar CSS -->
	<link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/newframe/fullcalendar.css">
	<link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/newframe/calendar.css">
	<link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/newframe/fullcalendar.print.css">
	<!-- datetimepicker -->
	<link href="../resource/activiti/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet">
	<link type="image/x-icon" href="favicon.png" rel="shortcut icon">
	<!-- uploadify CSS -->
	<link rel="stylesheet" type="text/css" media="screen" href="../resource/com/js/plugin/uploadify/uploadify.css">
	<link rel="stylesheet" type="text/css" media="screen" href="../resource/com/css/jquery.atwho.min.css">
	<!--[if lt IE 9]>
	<script src="../resource/newframejs/html5shiv.js"></script>
	<script src="../resource/newframejs/respond.min.js"></script>
	<![endif]-->
</head>
<body style="overflow-y: auto !important;">
<div id="data_loading" class="data_layout"><img src="../resource/com/img/loadingDQF.gif" /></div>
<div id="theme-wrapper">
	<header class="navbar index_header" id="header-navbar">
		<div class="container">
			<div id="logo" class="navbar-brand">
			<%--	<img src="../resource/com/img/logo-cst-white.png" alt="上海市卫生产业开发中心-协同运营平台" class="normal-logo logo-white" style="width:160px;height: 35px;">--%>
				<img src="../resource/com/img/logo-black.png" alt="" class="normal-logo logo-black">
				<img src="../resource/com/img/logo-small.png" alt="" class="small-logo hidden-xs hidden-sm hidden">
			</div>

             <span id="activity" class="activity-dropdown pull-left" style="margin-top:18px !important;">
				    <i class="fa fa-bell swing animated"></i>
				    <b class="badge" id="numAll"><s:property value="numToDo()+numDraft()"/>
						<!--<i class="fa fa-bell swing animated"></i>-->
					</b>
			</span>
			<div class="ajax-dropdown">

				<!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
				<div class="btn-group btn-group-justified" data-toggle="buttons">
					<label class="btn btn-default">
						<input type="radio" name="activity" id="ajax-flow!toDoList.action">
						<i class="fa fa-hand-o-right"></i> 我的待办 <span id="todo"> ( <s:property value="numToDo()"/>) </span> </label>
					<label class="btn btn-default">
						<input type="radio" name="activity" id="ajax-flow!draftList.action">
						<i class="fa fa-hand-o-right"></i> 我的草稿 <span id="draft">( <s:property value="numDraft()"/>) </span> </label>

				</div>

				<!-- notification content -->
				<div class="ajax-notifications custom-scroll">

					<div class="alert alert-transparent">
						<h4><i class="fa fa-upload"></i> 请点击上面的按钮显示内容</h4>

					</div>

					<i class="fa fa-lock fa-4x fa-border"></i>

				</div>
				<!-- end notification content -->

				<!-- footer: refresh area -->
				<%--<span> 最新个人信息一览--%>
				<%--<button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> 加载中..." class="btn btn-xs btn-default pull-right">--%>
				<%--<i class="fa fa-refresh"></i>--%>
				<%--</button> </span>--%>
				<!-- end footer -->

			</div>
			<div class="clearfix">
				<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="fa fa-bars"></span>
				</button>
				<div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
					<%--
					<ul class="nav navbar-nav pull-left">
						<li class="dropdown hidden-xs">
							<a class="btn dropdown-toggle" data-toggle="dropdown">
								<i class="<s:if test="_t != 'system' ">fa fa-tag</s:if><s:elseif test="_t=='system'">fa fa-university</s:elseif>"></i>
							</a>
							<ul class="dropdown-menu">
								<li class="item">
									<a href="index.action?_t=web" title="人事管理" class="btn">
										<i class="fa fa-tag"></i>
										人事管理
									</a>
								</li>
								<li class="item">
									<a href="index.action?_t=manage"  title="行政管理" class="btn">
										<i class="fa fa-tag"></i>
										行政管理
									</a>
								</li>
								<li class="item">
									<a href="index.action?_t=party"  title="党政管理" class="btn">
										<i class="fa fa-tag"></i>
										党政管理
									</a>
								</li>
								<li class="item">
									<a href="index.action?_t=finance"  title="财务管理" class="btn">
										<i class="fa fa-tag"></i>
										财务管理
									</a>
								</li>

								<shiro:hasAnyRoles name="company">

										<li class="item">
											<a href="index.action?_t=system">
												<i class="fa fa-university"></i>
												组织结构
											</a>
										</li>

								</shiro:hasAnyRoles>
							</ul>
							--%>
						</li>
					</ul>
				</div>

				<div class="nav-no-collapse pull-right" id="header-nav">
					<ul class="nav navbar-nav pull-right">

						<li>
							<a class="btn" id="make-small-nav" title="隐藏菜单">
								<i class="fa fa-bars"></i>
							</a>
						</li>
						<li class="hidden-xxs">
							<a class="btn" href="index!logout.action" title="登出">
								<i class="fa fa-power-off"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>
	<div id="page-wrapper" class="container">
		<div class="row" style="overflow-x: hidden;">
			<div id="nav-col">
				<section id="col-left" class="col-left-nano">
					<div id="col-left-inner" class="col-left-nano-content">
						<div id="user-left-box" class="clearfix hidden-sm hidden-xs dropdown profile2-dropdown" style="border-bottom: 1px solid #dee4e8;">
							<div style="float:left;width:40%;padding-left:5px">
								<s:if test="user.id == null">
									<img id="btn-img" src="" alt="" class="online" />
								</s:if>
								<s:else>
									<img id="btn-img" height="70" width="70" alt="" src="file.action?keyId=<s:property value="user.id" />" alt="" class="online" />
								</s:else>
							</div>
							<div class="user-box" style="float:left;width:60%;padding:20px 0 0 10px">
                        <span class="name" style="word-break:break-all">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<s:property value="user.name" />
						</a>
                             <ul class="dropdown-menu">
								 <li><a href="#ajax-users!profile.action?keyId=<s:property value="user.id" />">
									 <i class="fa fa-user"></i>个人信息</a></li>
								 <li><a href="index!logout.action"><i class="fa fa-power-off"></i>注销</a></li>
							 </ul>

                        </span>

                             <span class="status" style="font-size:14px">
								 <i class="fa fa-users"></i>
								 <a href="javascript:void(0)" class="remind" style="font-weight:700">提醒事项</a>

                                <%-- <i class="fa fa-circle"></i> 在线 --%>
								 <%--
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-angle-down"  ></i>
								</a>
                                <ul class="dropdown-menu">
									<li><a href="ajax-users!profile.action?keyId=<s:property value="user.id" />">
										<i class="fa fa-user"></i>个人信息</a></li>
									<li><a href="index!logout.action"><i class="fa fa-power-off"></i>注销</a></li>
								</ul>
								--%>
                              </span>

							</div>
						</div>
						<div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">
							<nav>
								<ul id="treeMenu" class="nav nav-pills nav-stacked">
									<s:if test="_t == null">
										<li>
											<a href="ajax-index!page.action" title="主页">
												<i class="fa fa-suitcase"></i>
												<span class="menu-item-parent">主页</span>
											</a>
										</li>
										<li>
											<a href="ajax!remindList.action" title="提醒事项">
												<i class="fa fa-users"></i>
												<span class="menu-item-parent">提醒事项</span>
											</a>
										</li>
										<li>
											<a href="ajax!toDoList.action" title="个人待办事项">
												<i class="fa fa-user-plus"></i>
												<span class="menu-item-parent">个人待办事项</span>
											</a>
										</li>
										<li>
											<a href="ajax!taskRecordList.action?type=1" title="已办未结">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">已办未结</span>
											</a>
										</li>
										<li>
											<a href="ajax!taskRecordList.action?type=2" title="已办已结">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">已办已结</span>
											</a>
										</li>
										<li>
											<a href="ajax!draftList.action" title="草稿">
												<i class="fa fa-trash"></i>
												<span class="menu-item-parent">草稿</span>
											</a>
										</li>
										<shiro:hasAnyRoles name="company">
											<li>
												<a href="###" onclick="location.href='index.action?_t=system'">
													<i class="fa fa-university"></i>
													<span class="menu-item-parent" >组织结构</span>
												</a>
											</li>
										</shiro:hasAnyRoles>

										<s:if test="manager == true">
											<li>
												<a href="###"  onclick="location.href='index.action?_t=system'">
													<i class="fa fa-university"></i>
													<span class="menu-item-parent" >组织结构</span>
												</a>
											</li>
										</s:if>
									</s:if>
									<s:elseif test="_t == 'web'">
										<li>
											<a href="ajax-manage.action?viewtype=1" title="人事管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">招聘管理</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-manage.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
												<li>
													<a href="ajax-manage.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														招聘中
													</a>
												</li>
												<li>
													<a href="ajax-manage.action?viewtype=3">
														<span class="sub-menu-buobao">●</span>
														招聘完成
													</a>
												</li>
												<li>
													<a href="ajax-manage.action?viewtype=4">
														<span class="sub-menu-buobao">●</span>
														已否决
													</a>
												</li>

											</ul>
										</li>
										<li>
											<a href="ajax-employees.action?viewtype=1" title="人事管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">员工信息管理</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-employees.action">
														<span class="sub-menu-buobao">●</span>
														员工信息登记
													</a>
												</li>

												<li>
													<a href="ajax-regular.action">
														<span class="sub-menu-buobao">●</span>
														正式员工
													</a>
												</li>
												<li>
													<a href="ajax-probation.action">
														<span class="sub-menu-buobao">●</span>
														试用期员工
													</a>
												</li>
												<li>
													<a href="ajax-expiring.action">
														<span class="sub-expiring-buobao">●</span>
														即将到期员工
													</a>
												</li>
												<li>
													<a href="ajax-dimission1.action">
														<span class="sub-menu-buobao">●</span>
														离职员工管理
													</a>
												</li>
												<li>
													<a href="ajax-retire.action">
														<span class="sub-menu-buobao">●</span>
														退休员工管理
													</a>
												</li>

											</ul>
										</li>

										<li>
											<a href="ajax-apply.action?viewtype=1" title="人事管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">员工转正管理</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-apply.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="ajax-change.action?viewtype=1" title="人事管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">员工异动管理</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-change.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
												<li>
													<a href="ajax-change.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														已归档
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="ajax-renew.action?viewtype=1" title="人事管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">员工续签管理</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-renew.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="ajax-dimission.action?viewtype=1" title="人事管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">员工离职管理</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-dimission.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="ajax-birthday.action?viewtype=1" title="人事管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">员工生日一览</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-birthday.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														员工生日
													</a>
												</li>

											</ul>
										</li>
										<li>
											<a href="ajax-employeeInfo.action?viewtype=1" title="人事管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">员工信息统计</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-employeeinfo.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														员工信息统计
													</a>
												</li>

											</ul>
										</li>

										<li>
											<a href="ajax-train.action?viewtype=1" title="人事管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">培训管理</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-train.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														申请中
													</a>
												</li>
												<li>
													<a href="ajax-train.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														培训中
													</a>
												</li>
												<li>
													<a href="ajax-train.action?viewtype=3">
														<span class="sub-menu-buobao">●</span>
														已完成
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="ajax-leave.action?viewtype=1" title="请假管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">请假管理</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-leave.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														申请中
													</a>
												</li>
												<li>
													<a href="ajax-leave.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														已完成
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="ajax-calendar.action?viewtype=1" title="考勤日历">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">考勤日历</span>
											</a>
											<ul class="submenu" style="display: block;">
												<li>
													<a href="ajax-calendar!page.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														考勤日历
													</a>
												</li>
											</ul>
										</li>


									</s:elseif>
									<s:elseif test="_t=='system'">
										<li>
											<a href="ajax!organization.action" title="组织结构">
												<i class="fa fa-sitemap"></i>
												<span class="menu-item-parent">组织结构</span>
											</a>
										</li>
										<li>
											<a href="ajax!users.action" title="人员管理">
												<i class="fa fa-users"></i>
												<span class="menu-item-parent">人员管理</span>
											</a>
										</li>
										<li>
											<a href="ajax!depart.action" title="部门管理">
												<i class="fa fa-sitemap"></i>
												<span class="menu-item-parent">部门管理</span>
											</a>
										</li>
										<li>
											<a href="ajax!post.action" title="岗位管理">
												<i class="fa fa-university"></i>
												<span class="menu-item-parent">岗位管理</span>
											</a>
										</li>
										<li>
											<a href="ajax!power.action" title="职权管理">
												<i class="fa fa-university"></i>
												<span class="menu-item-parent">职权管理</span>
											</a>
										</li>
										<li>
											<a href="ajax!role.action" title="群组管理">
												<i class="fa fa-users"></i>
												<span class="menu-item-parent">群组管理</span>
											</a>
										</li>
										<shiro:hasAnyRoles name="company">
										<li>
											<a href="ajax!flow.action" title="流程管理">
												<i class="fa fa-users"></i>
												<span class="menu-item-parent">流程管理</span>
											</a>
										</li>
										<li>
											<a href="ajax!modeler.action" title="模型管理">
												<i class="fa fa-users"></i>
												<span class="menu-item-parent">模型管理</span>
											</a>
										</li>
										</shiro:hasAnyRoles>
										<li>
											<a href="<%=path%>/com/ajax!dict.action" title="数据维护" class="dropdown-toggle">
												<i class="fa fa-lg fa-fw fa-cog"></i>
												<span class="menu-item-parent">数据维护</span>
											</a>
											<ul class="submenu">
												<li>
													<a href="<%=path%>/com/ajax!dict.action">
														<span class="sub-menu-buobao">●</span>
														数据字典
													</a>
												</li>
											</ul>
										</li>
									</s:elseif>

									<s:elseif test="_t=='party'">
										<li>
											<a href="../party/ajax-strongbig.action?viewtype=1" title="三重一大">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">三重一大</span>
											</a>
											<ul class="submenu" style="display: none;">
												<li>
													<a href="../party/ajax-strongbig.action?viewtype=1" >
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
												<li>
													<a href="../party/ajax-strongbig.action?viewtype=2" >
														<span class="sub-menu-buobao">●</span>
														已归档
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../party/ajax-selectionnotice.action?viewtype=1&flag=0" title="干部选拔">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">干部选拔</span>
											</a>
											<ul class="submenu" style="display: none">
													<%--	<s:if test="backIndex==0">--%>
												<li>
													<a href="../party/ajax-selectionnotice.action?viewtype=1&flag=0">
														<span class="sub-menu-buobao">●</span>
														实施中
													</a>
												</li>
												<li>
													<a href="../party/ajax-selectionnotice.action?viewtype=2&flag=1">
														<span class="sub-menu-buobao">●</span>
														已完成
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../party/ajax-activitymanager.action?viewtype=1" title=">活动管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">活动管理</span>
											</a>
											<ul class="submenu" style="display: none;">
												<li>
													<a href="../party/ajax-activitymanager.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
												<li>
													<a href="../party/ajax-activitymanager.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														已通过
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../party/ajax-moneymanager.action?viewtype=1" title=">党费登记">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">党费管理</span>
											</a>
											<ul class="submenu" style="display: none;">
												<li>
													<a href="../party/ajax-moneymanager.action?viewtype=1" title="党费缴纳">
														<span class="menu-item-parent">党费缴纳</span>
													</a>
													<ul class="submenu" style="display: none;">
														<li>
															<a href="../party/ajax-moneymanager.action?viewtype=1">
																<span class="sub-menu-buobao">●</span>
																流转中
															</a>
														</li>
														<li>
															<a href="../party/ajax-moneymanager.action?viewtype=2">
																<span class="sub-menu-buobao">●</span>
																已通过
															</a>
														</li>

													</ul>
												</li>
												<li>
													<a href="../party/ajax-moneyavg.action?viewtype=1&detail=0" title=">党费分摊">
														<span class="menu-item-parent">党费分摊</span>
													</a>
													<ul class="submenu" style="display: none;">
														<li>
															<a href="../party/ajax-moneyavg.action?viewtype=1&detail=0">
																<span class="sub-menu-buobao">●</span>
																流转中
															</a>
														</li>
														<li>
															<a href="../party/ajax-moneyavg.action?viewtype=2&detail=0">
																<span class="sub-menu-buobao">●</span>
																已通过
															</a>
														</li>
													</ul>
												</li>
												<li>
													<a href="../party/ajax-moneyavg.action?viewtype=1&detail=1" title=">党费分摊">
														<span class="menu-item-parent">党费可支配情况</span>
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../party/ajax-annualbudget.action?viewtype=1"  title=">退管会管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">退管会管理</span>
											</a>
											<ul class="submenu" style="display: none;">
												<li>
													<a href="../party/ajax-annualbudget.action?viewtype=1" title=">年度预算申请">
														<span class="menu-item-parent">年度预算申请</span>
													</a>
													<ul class="submenu" style="display: none;">
														<li>
															<a href="../party/ajax-annualbudget.action?viewtype=1">
																<span class="sub-menu-buobao">●</span>
																流转中
															</a>
														</li>
														<li>
															<a href="../party/ajax-annualbudget.action?viewtype=2">
																<span class="sub-menu-buobao">●</span>
																已通过
															</a>
														</li>
													</ul>
												</li>
												<li>
													<a href="../party/ajax-rebateactivity.action?viewtype=1" title=">退管会活动">
														<span class="menu-item-parent">退管会活动</span>
													</a>
													<ul class="submenu" style="display: none;">
														<li>
															<a href="../party/ajax-rebateactivity.action?viewtype=1">
																<span class="sub-menu-buobao">●</span>
																流转中
															</a>
														</li>
														<li>
															<a href="../party/ajax-rebateactivity.action?viewtype=2">
																<span class="sub-menu-buobao">●</span>
																已通过
															</a>
														</li>
													</ul>
												</li>
											</ul>

										</li>
										<li>
											<a href="../party/ajax-letterregister.action?viewtype=1" title=">信访登记">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">信访登记</span>
											</a>
											<ul class="submenu" style="display: none;">
												<li>
													<a href="../party/ajax-letterregister.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
												<li>
													<a href="../party/ajax-letterregister.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														已通过
													</a>
												</li>
											</ul>
										</li>
										<%--<li>
											<a href="../party/ajax-partymoney.action?viewtype=1" title=">测试党费缴纳">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">测试党费缴纳</span>
											</a>
											<ul class="submenu" style="display: none;">
												<li>
													<a href="../party/ajax-partymoney.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
												<li>
													<a href="../party/ajax-partymoney.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														已通过
													</a>
												</li>
											</ul>
										</li>--%>

									</s:elseif>
									<s:elseif test="_t=='manage'">
										<li>
											<a href="../manage/ajax!notice.action?viewtype=1" title="公告通知">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">公告通知 </span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../manage/ajax!notice.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
												<li>
													<a href="../manage/ajax!notice.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														已通过
													</a>
												</li>
												<li>
													<a href="../manage/ajax!notice.action?viewtype=3">
														<span class="sub-menu-buobao">●</span>
														已失效
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../manage/ajax!schedule.action" title="日程安排">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">日程安排 </span>
											</a>
										</li>
										<li>
											<a href="../manage/ajax!dispatch.action?viewtype=1" title="文件管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">文件管理 </span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../manage/ajax!dispatch.action?viewtype=1" title="发文管理">
														<span class="menu-item-parent">发文管理</span>
													</a>
													<ul class="submenu" style="display: none">
														<li>
															<a href="../manage/ajax!dispatch.action?viewtype=1">
																<span class="sub-menu-buobao">●</span>
																流转中
															</a>
														</li>
														<li>
															<a href="../manage/ajax!dispatch.action?viewtype=2">
																<span class="sub-menu-buobao">●</span>
																已通过
															</a>
														</li>
														<li>
															<a href="../manage/ajax!dispatch.action?viewtype=3">
																<span class="sub-menu-buobao">●</span>
																已失效
															</a>
														</li>
													</ul>
												</li>
												<li>
													<a href="../manage/ajax!receive.action?viewtype=1" title="收文管理">
														<span class="menu-item-parent">收文管理</span>
													</a>
													<ul class="submenu" style="display: none">
														<li>
															<a href="../manage/ajax!receive.action?viewtype=1">
																<span class="sub-menu-buobao">●</span>
																流转中
															</a>
														</li>
														<li>
															<a href="../manage/ajax!receive.action?viewtype=2">
																<span class="sub-menu-buobao">●</span>
																已通过
															</a>
														</li>
														<li>
															<a href="../manage/ajax!receive.action?viewtype=3">
																<span class="sub-menu-buobao">●</span>
																已失效
															</a>
														</li>
													</ul>
												</li>
											</ul>
										</li>
										<li>
											<a href="../manage/ajax!meetinguse.action?viewtype=1" title="会议室使用管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">会议室使用管理</span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../manage/ajax!meetinguse.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														待使用
													</a>
												</li>
												<li>
													<a href="../manage/ajax!meetinguse.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														已使用
													</a>
												</li>
												<li>
													<a href="../manage/ajax!meetinguse.action?viewtype=3">
														<span class="sub-menu-buobao">●</span>
														已取消
													</a>
												</li>
												<li>
													<a href="../manage/ajax!meetingusecalandar.action">
														<span class="sub-menu-buobao">●</span>
														会议日历
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../manage/ajax!registe.action?viewtype=1" title="资料管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">资料管理</span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../manage/ajax!registe.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
												<li>
													<a href="../manage/ajax!registe.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														已使用
													</a>
												</li>
												<li>
													<a href="../manage/ajax!registe.action?viewtype=3">
														<span class="sub-menu-buobao">●</span>
														已失效
													</a>
												</li>

											</ul>
										</li>
										<li>
											<a href="../manage/ajax!qualifications.action?step=1" title="公司资质管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">公司资质管理 </span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../manage/ajax!qualifications.action?step=1">
														<span class="sub-menu-buobao">●</span>
														使用中
													</a>
												</li>
												<li>
													<a href="../manage/ajax!qualifications.action?step=2">
														<span class="sub-menu-buobao">●</span>
														到期提醒
													</a>
												</li>
												<li>
													<a href="../manage/ajax!qualifications.action?step=3">
														<span class="sub-menu-buobao">●</span>
														已失效
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../manage/ajax!seal.action?step=1" title="印章使用管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">印章使用管理 </span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../manage/ajax!seal.action?step=1">
														<span class="sub-menu-buobao">●</span>
														申请中
													</a>
												</li>
												<li>
													<a href="../manage/ajax!seal.action?step=2">
														<span class="sub-menu-buobao">●</span>
														外借未归还
													</a>
												</li>
												<li>
													<a href="../manage/ajax!seal.action?step=3">
														<span class="sub-menu-buobao">●</span>
														已归还
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../manage/ajax!contract.action?viewtype=1" title="合同管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">合同管理 </span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../manage/ajax!contract.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														申请中
													</a>
												</li>
												<li>
													<a href="../manage/ajax!contract.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														已归档
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../manage/ajax!worklog.action?viewtype=1" title="工作日志">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">工作日志 </span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../manage/ajax!worklog.action?viewtype=1">
														<span class="sub-menu-buobao">●</span>
														综合视图
													</a>
												</li>
												<li>
													<a href="../manage/ajax!worklog.action?viewtype=2">
														<span class="sub-menu-buobao">●</span>
														人员工作日志汇总视图
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../manage/ajax!car.action?step=1" title="车辆管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">车辆管理 </span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../manage/ajax!car.action?step=1" title="车辆使用管理">
														<span class="sub-menu-buobao">●</span>
														车辆使用管理
													</a>
													<ul class="submenu" style="display: none">
														<li>
															<a href="../manage/ajax!car.action?step=1">
																<span class="sub-menu-buobao">●</span>
																申请中
															</a>
														</li>
														<li>
															<a href="../manage/ajax!car.action?step=2">
																<span class="sub-menu-buobao">●</span>
																待使用
															</a>
														</li>
														<li>
															<a href="../manage/ajax!car.action?step=3">
																<span class="sub-menu-buobao">●</span>
																已使用
															</a>
														</li>
													</ul>
												</li>
												<li>
													<a href="../manage/ajax!oil.action">
														<span class="sub-menu-buobao">●</span>
														车辆加油登记
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../manage/ajax!purchase.action?step=1" title="采购管理">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">采购管理 </span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../manage/ajax!purchase.action?step=1">
														<span class="sub-menu-buobao">●</span>
														流转中
													</a>
												</li>
												<li>
													<a href="../manage/ajax!purchase.action?step=2">
														<span class="sub-menu-buobao">●</span>
														待采购
													</a>
												</li>
												<li>
													<a href="../manage/ajax!purchase.action?step=3">
														<span class="sub-menu-buobao">●</span>
														采购完成
													</a>
												</li>
											</ul>
										</li>
									</s:elseif>
									<s:elseif test="_t=='finance'">
										<li>
											<a href="../finance/ajax!newsmonth.action" title=">财务报表">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">财务报表</span>
											</a>
											<ul class="submenu" style="display: none;">
												<li>
													<a href="../finance/ajax!newsmonth.action" title="月度快报">
														<span class="menu-item-parent">月度快报</span>
													</a>
												</li>
												<li>
													<a href="../finance/ajax!finance.action" title="财务月报">
														<span class="menu-item-parent">财务月报</span>
													</a>
												</li>
												<li>
													<a href="../finance/ajax!financeyear.action" title="财务年报">
														<span class="menu-item-parent">财务年报</span>
													</a>
												</li>
												<li>
													<a href="../finance/ajax!financialanalysis.action" title="财务分析报表">
														<span class="menu-item-parent">财务分析报表</span>
													</a>
												</li>
												<li>
													<a href="../finance/ajax!calculationyear.action" title="全年预决算报表">
														<span class="menu-item-parent">全年预决算报表</span>
													</a>
												</li>
												<li>
													<a href="../finance/ajax!modifyyear.action" title="预算调整报表">
														<span class="menu-item-parent">预算调整报表</span>
													</a>
												</li>
												<li>
													<a href="../finance/ajax!workplan.action" title="财务年终工作总结及计划">
														<span class="menu-item-parent">财务年终工作总结及计划</span>
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="../finance/ajax!auditnotice.action" title="财务审计">
												<i class="fa fa-bar-chart"></i>
												<span class="menu-item-parent">财务审计 </span>
											</a>
											<ul class="submenu" style="display: none">
												<li>
													<a href="../finance/ajax!auditnotice.action?step=1">
														<span class="sub-menu-buobao">●</span>
														实施中
													</a>
												</li>
												<li>
													<a href="../finance/ajax!auditnotice.action?step=2">
														<span class="sub-menu-buobao">●</span>
														已完成
													</a>
												</li>
											</ul>
										</li>
									</s:elseif>
								</ul>
							</nav>
						</div>
					</div>
				</section>
				<div id="nav-col-submenu"></div>
			</div>
			<div id="content-wrapper" style="min-height: 909px;background-color: #f3f5f6;">
				<div class="row">
					<div class="col-lg-12">
						<div id="ribbon" class="clearfix" style="border-bottom: 1px solid #dee4e8;margin-left: -10px;margin-right: -10px;">
							<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> 提示! 刷新会重新设置您的排版。" data-html="true"><i class="fa fa-refresh"></i></span> </span>
							<ol class="breadcrumb" style="display: inline-block">

							</ol>
						</div>
						<div class="row" id="content">

						</div>
					</div>
				</div>
			</div>
			<div id="config-tool" class="closed" style="top:50px">
				<a id="config-tool-cog" class="tool_opcity1">
					<i class="fa fa-cog"></i>
				</a>

				<div id="config-tool-options" class="tool_opcity2">
					<h4>布局</h4>
					<ul>
						<li>
							<div class="checkbox-nice">
								<input type="checkbox" id="config-fixed-header">
								<label for="config-fixed-header">
									固定抬头
								</label>
							</div>
						</li>
						<li>
							<div class="checkbox-nice">
								<input type="checkbox" id="config-fixed-sidebar">
								<label for="config-fixed-sidebar">
									固定左侧菜单
								</label>
							</div>
						</li>
					</ul>
					<br>
					<h4>主题</h4>
					<ul id="skin-colors" class="clearfix">
						<li>
							<a class="skin-changer" data-skin="" data-toggle="tooltip" title="" style="background-color: #34495e;" data-original-title="Default">
							</a>
						</li>
						<li>
							<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="" style="background-color: #2ecc71;" data-original-title="White/Green">
							</a>
						</li>
						<li>
							<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="" data-original-title="Gradient">
							</a>
						</li>
						<li>
							<a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="" style="background-color: #1abc9c;" data-original-title="Green Sea">
							</a>
						</li>
						<li>
							<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="" style="background-color: #9b59b6;" data-original-title="Amethyst">
							</a>
						</li>
						<li>
							<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="" style="background-color: #2980b9;" data-original-title="Blue">
							</a>
						</li>
						<li>
							<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="" style="background-color: #e74c3c;" data-original-title="Red">
							</a>
						</li>
						<li>
							<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="" style="background-color: #3498db;" data-original-title="White/Blue">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	//if (!window.jQuery) {
	document.write('<script src="../resource/com/js/libs/jquery-2.0.2.min.js"><\/script>');
	//}
</script>
<script>
	//if (!window.jQuery.ui) {
	document.write('<script src="../resource/com/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
	//}
</script>

<script src="../resource/com/js/bootstrap/bootstrap.min.js"></script>
<script src="../resource/jshow/js/bootstrap-modalmanager.js"></script>
<script src="../resource/jshow/js/bootstrap-modal.js"></script>
<script src="../resource/jshow/js/jquery.ui.draggable.js"></script>
<script src="../resource/jshow/js/jshow.utils.js"></script>
<script src="../resource/jshow/js/modal.manager.plugin1.0.js"></script>


<script src="../resource/newframejs/demo-skin-changer.js"></script>

<%--<script src="../resource/newframejs/bootstrap.js"></script>--%>
<script src="../resource/newframejs/jquery.nanoscroller.min.js"></script>
<script src="../resource/newframejs/demo.js"></script>


<script src="../resource/newframejs/notification/SmartNotification.min.js"></script>
<script src="../resource/newframejs/plugin/flot/jquery.flot.cust.js"></script>
<script src="../resource/newframejs/plugin/flot/jquery.flot.tooltip.js"></script>
<script src="../resource/newframejs/app.js"></script>
<%--<script src="../resource/newframejs/scripts.js"></script>--%>


<script src="../resource/newframejs/moment.min.js"></script>
<script src="../resource/newframejs/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../resource/newframejs/jquery-jvectormap-world-merc-en.js"></script>
<script src="../resource/newframejs/gdp-data.js"></script>
<script src="../resource/newframejs/flot/jquery.flot.min.js"></script>
<script src="../resource/newframejs/flot/jquery.flot.resize.min.js"></script>
<script src="../resource/newframejs/flot/jquery.flot.time.min.js"></script>
<script src="../resource/newframejs/flot/jquery.flot.threshold.js"></script>
<script src="../resource/newframejs/flot/jquery.flot.axislabels.js"></script>
<script src="../resource/newframejs/jquery.sparkline.min.js"></script>
<script src="../resource/newframejs/skycons.js"></script>

<script src="../resource/newframejs/jqMultiSelect.js"></script>
<%--<script src="../resource/newframejs/pace.min.js"></script>--%>

<!-- JQUERY VALIDATE -->
<script src="../resource/com/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- 下拉列表框-->
<script src="../resource/newframejs/select2.min.js"></script>

<!-- umeditor -->
<script type="text/javascript" src="../resource/com/umeditor/umeditor.js"></script>
<script type="text/javascript" src="../resource/com/umeditor/umeditor.config.js"></script>
<script type="text/javascript" src="../resource/com/js/plugin/uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="../resource/com/common.js"></script>

<!--在app.js之前加载init，确保左侧树形结构加载成功-->
<script src="../resource/activiti/init.js"></script>
<!--文件上传插件 -->
<script src="../resource/activiti/upload.js"></script>
<!--文件上传插件 -->
<script src="../resource/activiti/ipicture/jquery.ipicture.js"></script>
<!--左侧滑动操作-->
<script src="../resource/activiti/leftview/leftview.js"></script>
<!--grid多选操作-->
<script src="../resource/activiti/gridselect/gridselect.js"></script>
<!-- datetimepicker -->
<script src="../resource/activiti/datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="../resource/activiti/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<!-- timeLinr -->
<script src="../resource/activiti/timeLinr/js/jquery.mousewheel.js"></script>
<script src="../resource/activiti/timeLinr/js/jquery.timelinr.js"></script>
<%--activiti定制appjs--%>
<script src="../resource/activiti/app.js"></script>
<!-- jqgrid -->
<script type="text/javascript" src="../resource/newframejs/plugin/jqgrid/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../resource/newframejs/plugin/jqgrid/jquery.jqGrid.min.js"></script>

<!-- ztree -->
<script type="text/javascript" src="../resource/newframejs/plugin/ztree/js/jquery.ztree.all-3.5.min.js"></script>
<!-- echarts -->
<%--<script type="text/javascript" src="../resource/newframejs/plugin/echarts/echarts-plain.js"></script>--%>

<!-- multiselect-->
<script src="../resource/multiselect/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="../resource/newframejs/moment.min.js"></script>
<script type="text/javascript" src="../resource/newframejs/fullcalendar.min.js"></script>
<script type="text/javascript" src="../resource/newframejs/lang-all.js"></script>
<!--金额格式化插件-->
<script src="../resource/activiti/accounting.min.js"></script>
<!--组织结构-->
<script src="../resource/activiti/orgChart/jquery.jOrgChart.js"></script>
<!--tree select -->
<script src="../resource/activiti/treeSelect.js"></script>
<%--activiti定制通用dialog弹出对话框--%>
<script src="../resource/activiti/dialog/oDialog.js"></script>
<script type="text/javascript" src="../resource/activiti/leftview/leftview.js"></script>

<script type="text/javascript" src="../resource/com/js/plugin/at/jquery.caret.js"></script>
<script type="text/javascript" src="../resource/com/js/plugin/at/jquery.atwho.min.js"></script>

<%--tz_dialog插件--%>
<link rel="stylesheet" type="text/css" media="screen" href="../resource/com/js/plugin/tz_dialog/css/tz.css">
<link rel="stylesheet" type="text/css" media="screen" href="../resource/com/js/plugin/tz_dialog/css/tz_dialog_v2.0.css">
<script type="text/javascript" src="../resource/com/js/plugin/tz_dialog/js/tz_dialog_v2.0.js"></script>
<!-- echarts 插件 -->
<script type="text/javascript" src="../resource/echart/echarts.js"></script>
<script type="text/javascript" src="../resource/newframejs/plugin/summernote/summernote.js"></script>
<script>
	$(function(){
		$('#make-small-nav').bind('click',function(){
			$('#page-wrapper').toggleClass('nav-small');
		});
		$("body").css("min-height",$(window).height()-50);
	});
</script>

<script>
	var _hmt = _hmt || [];
	var umIndex = 0;
	$(function() {
		/*
		var hm = document.createElement("script");
		hm.src = "//hm.baidu.com/hm.js?036b704c8a212107220e412a13d760a2";
		var s = document.getElementsByTagName("script")[0];
		s.parentNode.insertBefore(hm, s);*/
	});

</script>
<script>
	/*$(function () {
	    var t="<s:property value="_t"/>";
	    alert("_t="+t);
	    if(t=="party"){
	        alert("length="+$("#treeMenu a").length);
            $("#treeMenu a").each(function (i,v) {
				$(this).attr("aa",$(this).attr("href"));//a标记同时又href属性和onclick时间，先执行onclick事件，在执行href属相下的动作（页面跳转，或javaScript伪链接），如果不想执行href属性下的动作，onclick需要返回false，一般是这样写onclick="xxx();return false;"
				$(this).attr("href",$(this).attr("aa"));
            });

            $("#treeMenu a").each(function (i,v) {

            })
            function partyClick(url) {
                alert(1111);
                loadURL(url,$("#content"));
            }
		}
    });
*/

	$("#treeMenu li a:first").click(function(){
		loadURL($("#treeMenu li a:first").attr("href"),$("#content"));
	});
	$(function(){
		loadURL($("#treeMenu li a:first").attr("href"),$("#content"));
		setTimeout(function(){
			$('body').addClass('fixed-header')
		},700);
	});
	$('a.remind').click(function(){
		loadURL('ajax!remindList.action' ,$("#content"));
		return false;
	});


	//    $('#activity').click(function(){
	//        $.ajax({
	//            type: "post",
	//            url: "index!numToDo.action",
	//            data: "ajax=1",
	//            dataType: "text",
	//            success: function(data){
	//                $('#todo').html(data);
	//                $.ajax({
	//                    type: "post",
	//                    url: "index!numDraft.action",
	//                    data: "ajax=1",
	//                    dataType: "text",
	//                    success: function(data){
	//                        $('#draft').html(data);
	//                        $('#numAll').html(Number(data)+ Number($('#todo').text()));
	//                    }
	//                });
	//            }
	//        });
	//    })
</script>
<!--
<script type="text/javascript" src="http://api.map.baidu.com/api?ak=wSznDI1OGu0S1kvVU9GQLlZL&v=2.0"></script>
-->
</body>
</html>
