<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<input  type="hidden" id="participantName" name="participantName" value="<s:property value="participantName"/>"/>
<input  type="hidden" id="participantId" name="participantId" value="<s:property value="participantId"/>"/>
<input type="hidden" id="participantDepartmentId" name="participantDepartmentId" value="<s:property value="participantDepartmentId"/>"/>

<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_user_list_row">
      <table id="ajax_user_list_table">
      </table>
      <div id="ajax_user_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>
    function run_jqgrid_function(){
        var key="<s:property value="key"/>";
        jQuery("#ajax_user_list_table").jqGrid({
            url:'ajax-users!list.action',
            mtype:"POST",
            datatype: 'json',
            page : 1,
            colNames:['用户名称','Id'],
            colModel : [
                {name:'name',index:'name', width:240,sortable:false,search:true,sorttype:'string'},
                {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
            ],
            rowNum : 10,
            rowList:[10,20,30],
            pager : '#ajax_user_list_page',
            sortname : 'createDate',
            sortorder : "desc",
            gridComplete:function(){
                var ids=$("#ajax_user_list_table").jqGrid('getDataIDs');
                var usersId;
                var usersId1=$("#participantId").val();
                if(key=="btn-choose-interviewer"){
                    usersId=$("#interviewerId").val();
                }else if(key=="btn-choose-trainUsers"){
                    usersId=$("#trainUsersId").val();
                }else if(key=="btn-choose-participantUsers"){
                    usersId=$("#"+usersId1).val();
                }
                for(var i=0;i<ids.length;i++) {
                    var cl = ids[i];
                    if (usersId != "" && usersId != null) {
                        var idsArr = usersId.split(",");
                        for (var s = 0; s < idsArr.length; s++) {
                            if (idsArr[s] == cl) {
                                $("#ajax_user_list_table").jqGrid("setSelection", cl);
                            }
                        }
                    }
                }
                jqGridStyle();
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
            },
            onSelectRow: function (rowId, status, e) {
                var usersName1=$("#participantName").val();
                var usersId1=$("#participantId").val();
                var usersId="";
                var usersName="";
                var flag=false;
                if(key=="btn-choose-interviewer"){
                    usersId="interviewerId";
                    usersName="interviewer";
                    flag=true;
                }else if(key=="btn-choose-trainUsers") {
                    usersId="trainUsersId";
                    usersName="trainUsersName";
                    flag=true;
                }else if(key=="btn-choose-participantUsers") {
                    usersId=usersId1;
                    usersName=usersName1;
                    flag=true;
                }
                if(flag==true){
                    $(this).gridselect('onecheck',{
                        table:"ajax_user_list_table",
                        rowId:rowId,
                        status:status,
                        id:usersId,
                        name:usersName,
                        type:"checkbox"
                    });
                }else {
                    $(this).gridselect('onecheck', {
                        table: "ajax_user_list_table",
                        rowId: rowId,
                        status: status,
                        id: usersId,
                        name: usersName,
                        type: "radio"
                    });
                }


            },
            onSelectAll:function(aRowids, status, e){
                var usersId="";
                var usersName="";
                if(key=="btn-choose-interviewer"){
                    usersId="interviewerId";
                    usersName="interviewer";

                }else if(key=="btn-choose-trainUsers") {
                    usersId="trainUsersId";
                    usersName="trainUsersName";
                }else if(key=="btn-choose-participantUsers") {
                    usersId="participantId";
                    usersName="participantName";
                }
                $(this).gridselect('morecheck',{
                    table:"ajax_user_list_table",
                    rowId:aRowids,
                    status:status,
                    id:usersId,
                    name:usersName,
                    type:"checkbox"
                });
            },

            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },

            multiselect: true,
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            forceFit:true,
            viewrecords: true,
            autowidth: true,
            height : 'auto',
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_user_list_table").jqGrid('setGridWidth', $("#ajax_user_list_row").width()-10);
        })

        jQuery("#ajax_user_list_table").jqGrid('navGrid', "#ajax_user_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });

        jQuery("#ajax_user_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
    }

    $(function(){
        run_jqgrid_function();
    });
    $("#dialog-ok").unbind("click").bind("click",function(){
      /*var slt = $("#ajax_user_list_table").jqGrid('getGridParam','selarrrow');
      var usersName = "";
      for(var i=0; i<slt.length; i++){
          var rowDatas = $("#ajax_user_list_table").jqGrid('getRowData', slt[i]);
            usersName = usersName + rowDatas['name'] + ",";
      }
      usersName = usersName.substring(0,usersName.length-1);
      $("#interviewer").val(usersName);*/
/*
        var rowId = $("#ajax_user_list_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_user_list_table").jqGrid('getRowData', rowId);
        $("#interviewer").val(rowDatas['name']);
        $("#interviewerId").val(rowId)*/;
        gDialog.fClose();
    });
</script>
