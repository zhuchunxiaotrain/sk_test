<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

    <fieldset>
        <ul class="list-group">
               <li style="list-style-type: none;">
                  <div id="commonDiv">
                       <div class="row"  id="type-0001">
                           <label class="label col col-2">
                               群组：
                           </label>
                           <section class="col col-5">
                               <label class="input state-disabled">
                                   <input disabled type="text" id="roleNames"
                                          value="<s:iterator id="list" value="commonConfig.roleSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>"/>
                                   <input type="hidden" id="roleIds" name="roleId"
                                          value="<s:iterator id="list" value="commonConfig.roleSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                               </label>
                           </section>
                       </div>
                       <div class="row"  id="type-0010">
                           <label class="label col col-2">
                               部门：
                           </label>
                           <section class="col col-5">
                               <label class="input state-disabled">
                                   <input  disabled type="text" id="departNames"
                                           value="<s:iterator id="list" value="commonConfig.departmentSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>"/>
                                   <input type="hidden" id="departIds" name="departId"
                                          value="<s:iterator id="list" value="commonConfig.departmentSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                               </label>
                           </section>
                       </div>
                       <div class="row"  id="type-0100" >
                           <label class="label col col-2">
                              职权：
                           </label>
                           <section class="col col-5">
                               <label class="input state-disabled">
                                   <input disabled  type="text" id="powerNames"
                                          value="<s:iterator id="list" value="commonConfig.powerSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.department.name"/>-<s:property value="#list.post.name"/>,</s:if></s:iterator>"/>
                                   <input type="hidden"  id="powerIds" name="powerId"
                                          value="<s:iterator id="list" value="commonConfig.powerSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                               </label>
                           </section>
                       </div>
                       <div class="row"  id="type-1000" >
                           <label class="label col col-2">
                               岗位：
                           </label>
                           <section class="col col-5">
                               <label class="input state-disabled">
                                   <input disabled  type="text" id="postNames"
                                          value="<s:iterator id="list" value="commonConfig.postSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>"/>
                                   <input type="hidden" id="postIds" name="postId"
                                          value="<s:iterator id="list" value="commonConfig.postSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                               </label>
                           </section>
                       </div>


                  </div>
               </li>
        </ul>
    </fieldset>

