<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<jsp:include page="ajax-top.jsp" />
<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-sm-12 col-md-12 col-lg-12">
        <div class="jarviswidget well" id="wid-id-3"
             data-widget-colorbutton="false"
             data-widget-editbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-custombutton="false"
             data-widget-sortable="false">
            <!-- widget div-->
            <div>
                <div class="widget-body">
                    <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>招聘信息</a>
                    <s:if test="manage==null || manage.getProcessState().name()=='Draft'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
                    </s:if>
                    <s:if test="manage!=null && manage.getProcessState().name()=='Backed'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
                    </s:if>
                    <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
                    <hr class="simple">
                    <form id="manage" class="smart-form" novalidate="novalidate" action="" method="post">
                        <input type="hidden" name="keyId" id="keyId" value="<s:property value="manage.id" />"/>
                        <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                        <header  style="display: block;">
                            招聘申请&nbsp;&nbsp;<span id="title"></span>
                        </header>
                        <fieldset>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-depart"> 招聘部门</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="depart" name="depart" placeholder="请选择招聘部门"
                                                   value="<s:property value="manage.recruitDepart.name"/>"/>
                                            <input type="hidden" id="departId" name="departId"
                                                   value="<s:property value="manage.recruitDepart.id"/>"/>
                                        </label>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-post"> 招聘岗位</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="postName" name="postName" placeholder="请选择招聘岗位"
                                                   value="<s:property value="manage.post.name"/>"/>
                                            <input type="hidden" id="postId" name="postId"
                                                   value="<s:property value="manage.post.id"/>"/>
                                        </label>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    招聘人数
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="recruitNum" id="recruitNum" placeholder="请输入招聘人数" value="<s:property value="manage.recruitNum"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    年龄
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="age" id="age" placeholder="请输入年龄" value="<s:property value="manage.age"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    性别
                                </label>
                                <section class="col col-5">
                                    <select name="sex" id="sex" data-native-menu="true" class="form-control">
                                    <option value="male">男</option>
                                    <option value="female">女</option>
                                    </select>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    学历
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="edu" id="edu" placeholder="请输入学历" value="<s:property value="manage.edu"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    专业
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="profession" id="profession" placeholder="请输入专业" value="<s:property value="manage.profession"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    工作经验
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="experience" id="experience" placeholder="请输入工作经验" value="<s:property value="manage.experience"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-chargeUsers"> 面试负责人</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="usersName1" name="chargeUsers"
                                            value="<s:iterator id="list" value="manage.chargeUsers"><s:property value="#list.name"/>,</s:iterator>"/>

                                            <input type="hidden" id="usersId1" name="chargeUsersId"
                                                   value="<s:iterator id="list" value="manage.chargeUsers"><s:property value="#list.id"/>,</s:iterator>"/>
                                        </label>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                    <label class="label col col-2">
                                        其他要求
                                    </label>
                                    <section class="col col-5">
                                        <label class="input">
                                            <input type="text" name="others" id="others" placeholder="请输入其他要求" value="<s:property value="manage.others"/>" >
                                        </label>
                                    </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="usersName" name="nextStepApproversName"
                                                   value="<s:property value="manage.nextStepApproversName"/>"/>
                                            <input type="hidden" id="usersId" name="nextStepApproversId"
                                                   value="<s:property value="manage.nextStepApproversId"/>"/>
                                        </label>
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    申请人
                                </label>
                                <section class="col col-4">
                                    <label class="input state-disabled">
                                        <input  disabled type="text" name="applyUserName" id="applyUserName" placeholder="" value="<s:property value="applyUserName"/>" >
                                        <input disabled type="hidden" name="applyUserId" id="applyUserId" placeholder="" value="<s:property value="applyUserId"/>" >
                                    </label>
                                </section>
                                <label class="label col col-2">
                                    操作日期
                                </label>
                                <section class="col col-4 ">
                                    <label class="input state-disabled">
                                        <input  disabled type="text" name="applyDate" id=applyDate" placeholder="" value="<s:property value="applyDate"/>" >
                                    </label>
                                </section>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </article>
</div>

<script>

    var draft = "<s:property value="draft" />";
    $(function(){
        var pdata= {
            keyId:$("#keyId").val(),
            flowName:"manage",
            todo:"1"
        };
        multiDuty(pdata);
    });

    //校验
    $("#manage").validate({
        rules : {
            departId : {
                required : true
            },
            postId: {
                required : true
            },
            recruitNum:{
                digits:true,
                required : true
            },
            age:{
                required : true
            },
            profession:{
                required : true
            },
            chargeUsersId:{
                required : true
            },
        },
        messages : {
            departId : {
                required : "请选择招聘部门"
            },
            postId : {
                required : "请选择招聘岗位"
            },
            age : {
                required : "请输入年龄",
                digits:"请输入整数",
            },
            recruitNum:{
                required : "请输入招聘人数",
                digits:"请输入整数",

            },
            profession:{
                required : "请输入专业"
            },
            chargeUsersId:{
                required : "请选择面试负责人"
            },

        },
        ignore: "",

    });

    //招聘部门
    $("a[key=btn-choose-depart]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择招聘部门",
            url:"ajax-dialog!dept.action?key=depart",
            width:560
        }).show();
    });

    //招聘岗位
    $("a[key=btn-choose-post]").unbind("click").bind("click",function(){
        var dep=$("#departId").val();
         if(dep.length<1){
         alert("请先选择部门！");
         return false;
         }
        gDialog.fCreate({
            title:"请选择招聘部门",
            url:"ajax-dialog!managePost.action?depId="+$("#departId").val(),
            width:340
        }).show();
    });


    $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择人员",
            url:"ajax-dialog!user.action",
            width:340
        }).show();
    });

    //责任人
    $("a[key=btn-choose-chargeUsers]").unbind("click").bind("click",function(){
        gDialog.fCreate({
           title:"请选择责任人",
           url:"ajax-dialog!users.action",
           width:340
        }).show();
    });

    //保存
    $("#btn-save-common").click(

        function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("manage","ajax-manage!save.action",null,function (data) {

                if(data.state == "200" || data.state ==200) {
                    if(draft == 1){
                        location.href='index.action';
                    }else{
                        loadURL("ajax-manage.action?viewtype=1",$('#content'));
                    }
                }
            });
        }

    );

    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#manage").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#manage").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("manage","ajax-manage!commit.action",null,function (data) {

                        if(data.state == "200" || data.state ==200) {
                            if(draft == 1){
                                location.href='index.action';
                            }else{
                                loadURL("ajax-manage.action?viewtype=1",$('#content'));
                            }
                        }

                    });
                }
            });
        }
    );

    //返回视图
    $("#btn-re-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-manage.action?viewtype=1",$('#content'));
        }

    });

</script>