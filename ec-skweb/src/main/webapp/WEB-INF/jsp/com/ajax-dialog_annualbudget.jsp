<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" value="<s:property value="param"/>"/>
<input type="hidden" id="index" value="<s:property value="index"/>"/>
<input type="hidden" id="departId" value="<s:property value="departId"/>"/>
<input type="hidden" id="str" value="<s:property value="str"/>"/>
<%--index=<s:property value="index"/>
param=<s:property value="param"/>--%>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12" id="ajax_annualbudget_list_row">
            <table id="ajax_annualbudget_list_table">
            </table>
            <div id="ajax_annualbudget_list_page">
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <span class="btn btn-primary" id="dialog-ok">确定</span>
    <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>
    function run_jqgrid_function(){
        jQuery("#ajax_annualbudget_list_table").jqGrid({
            url:'../party/ajax-annualbudget!listfordialog.action?departId='+$("#departId").val()+"&str="+$("#str").val(),
            mtype:"POST",
            datatype: 'json',
            page : 1,
            colNames:['年度预算科目','预算金额','Id'],
            colModel : [
                {name:'dictName',index:'dictName', width:200,sortable:false,search:true,sorttype:'string'},
                {name:'annualBudgetMoney',index:'annualBudgetMoney', width:200,sortable:false,search:true,sorttype:'string'},
                //{name:'annualBudgetId',index:'annualBudgetId',width:10,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true},
                {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
            ],
            rowNum : 10,
            rowList:[10,20,30],
            pager : '#ajax_annualbudget_list_page',
            sortname : 'createDate',
            sortorder : "desc",
            gridComplete:function(){

                var ids=$("#ajax_annualbudget_list_table").jqGrid('getDataIDs');
                var annualBudgetsId;
                var param="<s:property value="param"/>"
                var index=$("#index").val();
               // alert("index_1 = "+index);
                if(param=="rebateActivity"){
                    annualBudgetsId=$("#annualBudgetId_"+index).val();
                }
                    for(var i=0;i<ids.length;i++) {
                        var cl = ids[i];
                        if (annualBudgetsId != "" && annualBudgetsId != null) {
                            var idsArr = annualBudgetsId.split(",");
                            for (var s = 0; s < idsArr.length; s++) {
                                if (idsArr[s] == cl) {
                                    $("#ajax_annualbudget_list_table").jqGrid("setSelection", cl);
                                }
                            }
                        }
                    }
                jqGridStyle();
                $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
            },
            onSelectRow: function (rowId, status, e) {
                var param="<s:property value="param"/>";
                var index="<s:property value="index"/>";
                var rowId=$("#ajax_annualbudget_list_table").jqGrid("getGridParam",'selrow');
                var rowDatas=$("#ajax_annualbudget_list_table").jqGrid('getRowData',rowId);
                if(param=="rebateActivity"){
                   var actionUrl="<%=path%>/party/ajax-rebateactivity!checkUsedAmount.action";
                    var data={departId:$("#departId").val(),annualBudgetDetailId:rowId};
                    ajax_action(actionUrl,data,null,function (pdata) {
                        if(pdata.dataRows==""){
                            $("#usedAmount_"+index).val(0);
                            $("#remainAmount_"+index).val(rowDatas["annualBudgetMoney"] - 0);
                        }else{
                            $(pdata.dataRows).each(function (i,v) {
                                $("#usedAmount_"+index).val(v.usedAmount);
                                $("#remainAmount_"+index).val(rowDatas["annualBudgetMoney"] - v.usedAmount);
                            })
                        }

                    });
                    $("#annualBudgetName_"+index).val(rowDatas["dictName"]);
                    //$("#annualBudgetId_"+index).val(rowDatas["annualBudgetId"]);
                    $("#annualBudgetMoney_"+index).val(rowDatas["annualBudgetMoney"]);
                    $("#annualBudgetDetailId_"+index).val(rowDatas["id"]);
                  /*  annualBudgetId="annualBudgetId_"+index;
                    annualBudgetName="annualBudgetName_"+index;
                    annualBudgetMoney="annualBudgetMoney_"+index;
*/
                }
                /*
                if(flag==true){
                    $(this).gridselect('onecheck',{
                        table:"ajax_annualbudget_list_table",
                        rowId:rowId,
                        status:status,
                        id:annualBudgetId,/!*annualBudgetId是中间值*!/
                        name:annualBudgetName,
                        //annualBudgetMoney:annualBudgetMoney,
                        type:"radio"/!*单选按钮*!/
                    });
                    //alert("annualBudgetId = "+annualBudgetId);
                    //alert("annualBudgetName = "+annualBudgetName);
                    //alert("annualBudgetMoney = "+annualBudgetMoney);
                }else {
                    $(this).gridselect('onecheck',{
                        table:"ajax_annualbudget_list_table",
                        rowId:rowId,
                        status:status,
                        id:annualBudgetId,
                        name:annualBudgetName,
                        //annualBudgetMoney:annualBudgetMoney,
                        type:"checkbox"/!*多选按钮*!/
                    });
                }*/

            },
            /*onSelectAll:function(aRowids, status, e){
                alert("只能选择一个活动类型");
                return false;
                /!*$(this).gridselect('morecheck',{
                    table:"ajax_annualbudget_list_table",
                    rowId:aRowids,
                    status:status,
                    id:"annualBudgetId",
                    name:"annualBudgetMoney",
                });*!/
            },*/
            jsonReader: {
                root: "dataRows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems : false
            },

           multiselect: true,
            rownumbers:true,
            gridview:true,
            shrinkToFit:true,
            forceFit:true,
            viewrecords: true,
            autowidth: true,
            height : 'auto',
            loadComplete: function() {
            }
        });
        $(window).on('resize.jqGrid', function() {
            jQuery("#ajax_annualbudget_list_table").jqGrid('setGridWidth', $("#ajax_annualbudget_list_row").width()-10);
        })

        jQuery("#ajax_annualbudget_list_table").jqGrid('navGrid', "#ajax_annualbudget_list_page", {
            edit : false,
            add : false,
            del : false,
            search:false
        });

        jQuery("#ajax_annualbudget_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
    }

    $(function(){
        run_jqgrid_function();
    });
    $("#dialog-ok").unbind("click").bind("click",function(){

        gDialog.fClose();
    });
</script>
