<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<input type="hidden" name="processDefinitionId" id="processDefinitionId" value="<s:property value="processDefinition.id" />">
<table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>流程历史ID</th>
                                            <th>流程定义ID</th>
                                            <th>流程定义名称</th>
                                            <th>发起人</th>
                                            <th>开始时间</th>
                                            <th>结束时间</th>
                                            <th>耗时</th>
                                            <th>操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <s:iterator value="dataFinish" id="list">
                                            <tr id="<s:property value="#list.id"/>">
                                                <td>
                                                    <s:property value="#list.id" />
                                                </td>
                                                <td>
                                                    <s:property value="#list.pid" />
                                                </td>
                                                <td>
                                                    <s:property value="#list.pdname" />
                                                </td>
                                                <td>
                                                    <s:property value="#list.starter" />
                                                </td>
                                                <td>
                                                    <s:date name="#list.startTime" format="yyyy-MM-dd HH:mm" />
                                                </td>
                                                <td>
                                                    <s:date name="#list.endTime" format="yyyy-MM-dd HH:mm" />
                                                </td>
                                                <td>
                                                    <s:property value="#list.time" />
                                                </td>
                                                <td>
                                                    <%--<a title="流程图" key="ajax_flow_actions_img" class="btn btn-default "--%>
                                                       <%--href="javascript:void(0);">--%>
                                                       <%--<i class="fa fa-eye"></i>流程图</a>--%>
                                                    <a title="审批明细" key="ajax_comment_actions_read" class="btn btn-default " id="<s:property value="#list.id"/>"
                                                       href="javascript:void(0);">
                                                       <i class="fa fa-eye"></i>审批明细</a>
                                                </td>
                                            </tr>
                                        </s:iterator>
                                    </tbody>
                                </table>


<script>
    pageSetUp();
   // loadDataTableScripts();
    $("a[key=ajax_comment_actions_read]").unbind("click").bind("click",function(){
        var this_tr = $(this).parent().parent();
    	var actionUrl = "../com/ajax-running!comment.action?keyId="+$(this_tr).attr("id");
        $(this).leftview('init',{
            title:"查看评论",
            actionUrl:actionUrl
        });
	})
</script>
