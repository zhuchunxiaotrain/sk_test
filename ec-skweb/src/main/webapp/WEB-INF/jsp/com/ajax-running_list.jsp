<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<input type="hidden" name="processDefinitionId" id="processDefinitionId" value="<s:property value="keyId" />">
<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget well" id="wid-id-3"
			    data-widget-colorbutton="false"
			    data-widget-editbutton="false"
			    data-widget-togglebutton="false"
			    data-widget-deletebutton="false"
			    data-widget-fullscreenbutton="false"
			    data-widget-custombutton="false"
			    data-widget-sortable="false">


				<header>
                        <a class="btn btn-default ">
					        <i class="fa fa-lg fa-mail-reply-all"></i>
					        返回流程列表
					    </a>
					    <hr class="simple">

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body ">
					    <a class="bt btn-default "  href="javascript:void(0);" key="ajax_running_actions_back">
                    	    <i class="fa fa-lg fa-mail-reply-all"></i>
                    			返回流程列表
                    	</a>
                    	<hr class="simple">
                        <ul id="myTab1" class="nav nav-tabs bordered">
							<li class="active">
								<a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-credit-card"></i> 流转中 </a>
							</li>
							<li>
								<a href="#s3" data-toggle="tab" key="ajax_history_list"><i class="fa fa-fw fa-lg fa-comments"></i> 已归档 <span class="badge bg-color-blue txt-color-white"></span></a>
							</li>
					    </ul>

						<div id="myTabContent1" class="tab-content padding-10">
                            <div class="tab-pane fade in active" id="s1">
                                <table id="ajax_config_table" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>流程ID</th>
                                            <th>流程定义ID</th>
                                            <th>流程定义名称</th>
                                            <th>流程关键字</th>
                                            <th>发起人</th>
                                            <th>发起时间</th>
                                            <th>当前处理人</th>
                                            <th>当前节点</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <s:iterator value="dataRows" id="list">
                                            <tr id="<s:property value="#list.id"/>">
                                                <td>
                                                    <s:property value="#list.id" />
                                                </td>
                                                <td>
                                                    <s:property value="#list.proId" />
                                                </td>
                                                <td>
                                                    <s:property value="#list.proName" />
                                                </td>
                                                <td>
                                                    <s:property value="#list.key" />
                                                </td>
                                                <td>
                                                    <s:property value="#list.starter" />
                                                </td>
                                                <td>
                                                    <s:date name="#list.startTime" format="yyyy-MM-dd HH:mm" />
                                                </td>
                                                <td>
                                                    <s:property value="#list.assigner" />
                                                </td>
                                                <td>
                                                    <a title="当期节点" key="ajax_flow_actions_img" class="btn btn-default "
                                                       href="javascript:void(0);">
                                                       <i class="fa fa-eye"></i>当前节点</a>
                                                    <a title="审批明细" key="ajax_comment_actions_read" class="btn btn-default " id="<s:property value="#list.id"/>"
                                                       href="javascript:void(0);">
                                                       <i class="fa fa-eye"></i>审批明细</a>
                                                </td>
                                            </tr>
                                        </s:iterator>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="s3">

                            </div>

                        </div><!-- end tap content -->
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->


<script>
    pageSetUp();
    	function loadDataTableScripts() {
    		loadScript("../resource/com/js/plugin/datatables/jquery.dataTables-cust.min.js", dt_2);

    		function dt_2() {
    			loadScript("../resource/com/js/plugin/datatables/ColReorder.min.js", dt_3);
    		}

    		function dt_3() {
    			loadScript("../resource/com/js/plugin/datatables/FixedColumns.min.js", dt_4);
    		}

    		function dt_4() {
    			loadScript("../resource/com/js/plugin/datatables/ColVis.min.js", dt_5);
    		}

    		function dt_5() {
    			loadScript("../resource/com/js/plugin/datatables/ZeroClipboard.js", dt_6);
    		}

    		function dt_6() {
    			loadScript("../resource/com/js/plugin/datatables/media/js/TableTools.min.js", dt_7);
    		}

//    		function dt_7() {
//    			loadScript("../resource/com/js/plugin/datatables/DT_bootstrap.js", runDataTables);
//    		}

    	}
    $("a[key=ajax_flow_actions_img]").click(function(){
        var this_tr = $(this).parent().parent();
    	/*loadURL("ajax-running!export.action?keyId="+$(this_tr).attr("id"),$('#dialog-img_message'));
    	$('#dialog-img_message').dialog('open');*/
    	gDialog.fCreate({
            title:'查看当前节点',
        	url:'<%=path%>/resource/flow/bpmniframe.jsp?keyId='+$(this_tr).attr("id"),
        	width:1000
        }).show();

    });
    $("a[key=ajax_running_actions_back]").unbind("click").bind("click",function(){
	    loadURL("../com/ajax!flow.action?keyId=",$('#content'));
	})
	$("a[key=ajax_comment_actions_read]").unbind("click").bind("click",function(){
    	var actionUrl = "../com/ajax-running!comment.action?keyId="+$(this).attr("id");
        $(this).leftview('init',{
            title:"审批明细",
            actionUrl:actionUrl
        });
	})
	$('a[key="ajax_history_list"]').on('shown.bs.tab', function (e) {
	    var keyId = $("#processDefinitionId").val();
        loadURL("../com/ajax-running!history.action?keyId="+keyId,$('#s3'));
    })
</script>
