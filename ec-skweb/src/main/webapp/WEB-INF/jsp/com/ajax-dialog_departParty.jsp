<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>


<input type="hidden" name="selectedid" id="selectedid"/>
<input type="hidden" name="selectedname" id="selectedname"/>
<input type="hidden" id="param" name="param" value="<s:property value="param"/>"/>
<input type="hidden" id="payDate" name="payDate" value="<s:property value="payDate"/>"/>
<input type="hidden" id="departId" name="departId" value="<s:property value="departId"/>"/>
<input type="hidden" id="keyId" name="keyId" value="<s:property value="keyId"/>"/>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12" id="ajax_depart_list_row">
      <table id="ajax_depart_list_table">
      </table>
      <div id="ajax_depart_list_page">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <span class="btn btn-primary" id="dialog-ok">确定</span>
  <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>

<script>
  function run_jqgrid_function(){
    jQuery("#ajax_depart_list_table").jqGrid({
      url:'ajax-depart!listParty.action?param='+$("#param").val()+"&departId="+$("#departId").val()+"&payDate="+$("#payDate").val()+"&keyId="+$("#keyId").val(),
      mtype:"POST",
      datatype: 'json',
      page : 1,
      colNames:['部门名称','Id'],
      colModel : [
        {name:'name',index:'name', width:400,sortable:false,search:true,sorttype:'string'},
        {name:'id',index:'id',width:10,key:true,hidedlg:true,sortable:false,fixed:true,search:false,hidden:true}
      ],
      rowNum : 10,
      rowList:[10,20,30],
      pager : '#ajax_depart_list_page',
      sortname : 'createDate',
      sortorder : "desc",
      gridComplete:function(){
          var ids=$("#ajax_depart_list_table").jqGrid('getDataIDs');
          var param=$("#param").val();
          var departsId;
          if(param=="annualbudget") {
              departsId=$("#annualBudgetDepartId").val();
             // alert("deppartsId = "+departsId);
          }else if(param=="rebateactivity"){
              departsId=$("#rebateActivityDepartId").val();
          }else if(param="moneymanager"){
              departsId=$("#moneyManagerDepartId").val();
          }else if(param="activitymanager"){
              departsId=$("#activityManagerDepartId").val();
          }
        for (var i=0;i<ids.length;i++){
             // alert("start_"+i);
            var cl=ids[i];
            if(departsId!=null && departsId != ""){
                var idsArr= departsId.split(",");
                for(var s=0;s<idsArr.length;s++){
                    //alert("idsArrLength = "+idsArr.length);
                    //alert("center_"+s);
                    if(idsArr[s] == cl){
                        //alert("equerl_"+s);
                        $("#ajax_depart_list_table").jqGrid("setSelection",cl);
                    }
                }
            }
          }
        jqGridStyle();
        $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
      },
      onSelectRow: function (rowId, status, e) {
              var param=$("#param").val();
              var departId="";
              var departName="";
              var flag=false;
              if(param=="annualbudget"){
                  departId="annualBudgetDepartId";
                  departName="annualBudgetDepartName";
                  flag=true;
              }else if(param=="rebateactivity"){
                  departId="rebateActivityDepartId";
                  departName="rebateActivityDepartName";
                  flag=true;
              }else if(param=="moneymanager"){
                  departId="moneyManagerDepartId";
                  departName="moneyManagerDepartName";
                  flag=true;
              }else if(param=="activitymanager"){
                  departId="activityManagerDepartId";
                  departName="activityManagerDepartName";
                  flag=true;
              }


              if(flag==true){
                  $(this).gridselect('onecheck',{
                      table:"ajax_depart_list_table",
                      rowId:rowId,
                      status:status,
                      id:departId,
                      name:departName,
                      type:"radio"
                  });
              }else {
                  $(this).gridselect('onecheck',{
                      table:"ajax_depart_list_table",
                      rowId:rowId,
                      status:status,
                      id:departId,
                      name:departName,
                      type:"checkbox"
                  });
              }
      },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },

      multiselect: true,
      rownumbers:true,
      gridview:true,
      shrinkToFit:true,
      forceFit:true,
      viewrecords: true,
      autowidth: true,
      height : 'auto',
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_depart_list_table").jqGrid('setGridWidth', $("#ajax_depart_list_row").width()-10);
    })


    jQuery("#ajax_depart_list_table").jqGrid('navGrid', "#ajax_depart_list_page", {
      edit : false,
      add : false,
      del : false,
      search:false
    });

    jQuery("#ajax_depart_list_table").jqGrid('filterToolbar',{searchOperators : false,stringResult:true});
  }

  $(function(){
    run_jqgrid_function();
  });
  $("#dialog-ok").unbind("click").bind("click",function(){
      var rowId = $("#ajax_depart_list_table").jqGrid('getGridParam','selrow');
      var rowDatas = $("#ajax_depart_list_table").jqGrid('getRowData', rowId);
      var param="<s:property value="param"/>"
      gDialog.fClose();

      if(param=="moneymanager"){
          var recordDate=$("#recordDate").val();
          var data={recordDate:recordDate,departId:rowId};
          var actionUrl="<%=path%>/party/ajax-moneymanager!checkMonth.action";
          ajax_action(actionUrl,data,num,function (pdata) {
                if(pdata.count>0){
                    alert("选择的部门在月份已经有登记记录，无需再次提交，请查看登记记录");
                    $("#moneyManagerDepartName").val("");
                    $("#moneyManagerDepartId").val("");
                    $("#moneyManagerDiv div[name='moneyDiv']").remove();
                }else{
                    $("#moneyManagerDiv div[name='moneyDiv']").remove();

                    var actionUrl="<%=path%>/com/ajax-employees!count.action";
                    var data={departId: rowId,payDateString:$("#payDate").val()};
                    var div="";
                    ajax_action(actionUrl, data,null,function (pdata) {
                            if($(pdata.dataRows).length==0){
                                div='<div class="row" name="moneyDiv" ><label class="label col col-2"> </label> <section class="col col-5"> <label class="input"> <label class="input state-disabled"> <input style="border: hidden" disabled type="text"  name="departName"value="选定的月份没有要交党费的人"/> </label> </label> </section><div>';
                            }else{
                                $(pdata.dataRows).each(function (i,v) {
                                    var str1='<div class="row" name="moneyDiv"> <label class="label col col-2"> </label><section class="col col-2"><label class="input"> <label class="input state-disabled"> <input disabled type="text"  name="userName" value="'+v.userName+'"/> <input disabled type="hidden"  name="employessId" value="'+v.id+'"/>';
                                    var str2='</label> </label> </section> <section class="col col-2"> <label class="input"> <label class="input state-disabled"> <input disabled type="text" id="baseMoney_'+i+'" name="baseMoney" value="'+v.baseMoney+'"/>';
                                    var str3='</label> </label> </section> <section class="col col-1"> <label class="input"> <label class="input"> <input  type="text" name="monthPayDues" value=""/> </label> </label> </section> </div>';
                                    div+=str1+str2+str3;
                                });
                            }

                        })

                    $("#moneyManagerDiv").append(div);
                }
          })





      }else if(param=="rebateactivity"){
          $("#add-annoualbudget-row div[name='delDiv']").remove();



         /* $("#add-annoualbudget-row div[name='delDiv']").remove();//不需要判断单位是否相等，
          var actionUrl1="<%=path%>/party/ajax-rebateactivity!checkUsedAmount.action";
          var data1={annualBudgetId:rowDatas["annualBudgetId"],departId:$("#rebateActivityDepartId").val(),annualBudgetDetailId:rowDatas["Id"]};
          var arr=new Array();
          ajax_action(actionUrl1,data1,null,function (pdata) {
              if($(pdata.dataRows).length==0){
                  for(var i=0;i<20;i++){
                      arr[i]=0;
                  }
              }else{
                  $(pdata.dataRows).each(function (i, v) {
                      arr[i]=v.usedAmount;
                  })
              }

              /!*$("#usedAmount_"+index).val(pdata.usedAmount);
              $("#remainAmount_"+index).val(rowDatas["annualBudgetMoney"] - pdata.usedAmount);*!/
          })

          var actionUrl="<%=path%>/party/ajax-annualbudget!count.action";
          var data={departId: rowId};
          var div="";
          ajax_action(actionUrl, data,null,function (pdata) {
              if($(pdata.dataRows).length==0){
                  div='<div class="row" name="delDiv" ><label class="label col col-2"> </label> <section class="col col-5"> <label class="input"> <label class="input state-disabled"> <input style="border: hidden" disabled type="text"  name="departName"value="没有年度预算申请信息"/> </label> </label> </section><div>';
              }else{
                  $(pdata.dataRows).each(function (i,v) {
                      var str1='<div class="row" name="delDiv"> <label class="label col col-2"> </label><section class="col col-2"><label class="input"> <label class="input state-disabled"> <input disabled type="text"  name="dictName" value="'+v.dictName+'"/> <input disabled type="hidden"  name="annualBudgetId" value="'+v.annualBudgetId+'"/><input disabled type="hidden"  name="annualBudgetDetailId" value="'+v.id+'"/>';
                      var str2='</label> </label> </section> <section class="col col-1"> <label class="input"> <label class="input state-disabled"> <input disabled type="text" id="annualBudgetMoney_'+i+'" name="annualBudgetMoney" value="'+v.annualBudgetMoney+'"/>';
                      var str3='</label> </label> </section> <section class="col col-1"> <label class="input"> <label class="input state-disabled"> <input disabled type="text" id="usedAmount_'+i+'" name="usedAmount" value="'+arr[i]+'"/>';
                      var str4='</label> </label> </section> <section class="col col-2"> <label class="input"> <label class="input"> <input type="text" id="actualAmount_'+i+'" name="actualAmount" value=""/>';
                      var str5='</label> </label> </section> <section class="col col-1"> <label class="input"> <label class="input state-disabled"> <input disabled  type="text" id="remainAmount_'+i+'" name="remainAmount" value="'+(v.annualBudgetMoney-arr[i])+'"/> </label> </label> </section> </div>';
                      div+=str1+str2+str3+str4+str5;
                  })
              }
          });
          $("#add-annoualbudget-row").append(div);*/

      }else if(param=="activitymanager"){
          var actionUrl="<%=path%>/party/ajax-moneyavg!getEnableMoney.action";
          var data={departId:rowId};
          ajax_action(actionUrl,data,null,function (pdata) {
              if($(pdata.dataRows).length>0){
                  $(pdata.dataRows).each(function (i,v) {
                      $("#enableMoney").val(v.departResidueEnableMoney);
                  })
              }else{
                  $("#enableMoney").val(0);
              }
          });
      }


      /*if($("#recordDate").val()==""){
          $("#moneyManagerDepartId").val("");
          $("#moneyManagerDepartName").val("");
          $("#moneyManagerDiv").val("");
      }*/
  });
</script>