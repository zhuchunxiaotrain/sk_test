<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<form id="commonconfig_form" action="" method="post" class="smart-form">
    <input type="hidden" name="selectType" id="selectType" value="<s:property value="selectType"/>"/>
    <input type="hidden" id="sType" value="<s:property value="sType"/>"/>
    <input type="hidden" name="keyId" id="keyId" value="<s:property value="keyId"/>"/>
    <input type="hidden" name="allMans" id="allMans" value="<s:if test='commonConfig.allmans==true'>1</s:if><s:else>0</s:else>" />
    <fieldset>

        <div class="row">
            <section class="col col-9">
                <div class="inline-group" >
                    <label class="checkbox">
                        <input type="checkbox" name="type" value="000001" />
                        <i></i>群组
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="type" value="000010" />
                        <i></i>部门
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="type" value="000100" />
                        <i></i>岗位
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="type" value="001000" />
                        <i></i>职权
                    </label>
                    <s:if test='selectType=="docReader" || selectType=="archReader"'>
                        <label class="checkbox">
                            <input type="checkbox" name="type" value="010000" />
                            <i></i>域名
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="type" value="100000" />
                            <i></i>所有人
                        </label>
                    </s:if>
                </div>
            </section>
        </div>
        <ul class="list-group">
               <li class="list-group-item">
                  <div id="commonDiv">
                       <div class="row"  id="type-000001" style="display:none">
                           <label class="label col col-2">
                               <a  href="javascript:void(0);" key="btn-choose-role">群组：</a>
                           </label>
                           <section class="col col-5">
                               <label class="input state-disabled">
                                   <input disabled type="text" id="roleName"
                                          value="<s:iterator id="list" value="commonConfig.roleSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>"/>
                                   <input type="hidden" id="roleId" name="roleId"
                                          value="<s:iterator id="list" value="commonConfig.roleSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                               </label>
                           </section>
                       </div>
                       <div class="row"  id="type-000010" style="display:none">
                           <label class="label col col-2">
                               <a  href="javascript:void(0);" key="btn-choose-depart">部门：</a>
                           </label>
                           <section class="col col-5">
                               <label class="input state-disabled">
                                   <input  disabled type="text" id="departName"
                                           value="<s:iterator id="list" value="commonConfig.departmentSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>"/>
                                   <input type="hidden" id="departId" name="departId"
                                          value="<s:iterator id="list" value="commonConfig.departmentSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                               </label>
                           </section>
                       </div>
                       <div class="row"  id="type-001000" style="display:none">
                           <label class="label col col-2">
                               <a  href="javascript:void(0);" key="btn-choose-power">职权：</a>
                           </label>
                           <section class="col col-5">
                               <label class="input state-disabled">
                                   <input disabled  type="text" id="powerName"
                                          value="<s:iterator id="list" value="commonConfig.powerSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.department.name"/>-<s:property value="#list.post.name"/>,</s:if></s:iterator>"/>
                                   <input type="hidden"  id="powerId" name="powerId"
                                          value="<s:iterator id="list" value="commonConfig.powerSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                               </label>
                           </section>
                       </div>
                       <div class="row"  id="type-000100" style="display:none">
                           <label class="label col col-2">
                               <a  href="javascript:void(0);" key="btn-choose-post">岗位：</a>
                           </label>
                           <section class="col col-5">
                               <label class="input state-disabled">
                                   <input disabled  type="text" id="postName"
                                          value="<s:iterator id="list" value="commonConfig.postSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.name"/>,</s:if></s:iterator>"/>
                                   <input type="hidden" id="postId" name="postId"
                                          value="<s:iterator id="list" value="commonConfig.postSet"><s:if test="#list.state.toString() == 'Enable'"><s:property value="#list.id"/>,</s:if></s:iterator>"/>
                               </label>
                           </section>
                       </div>
                      <div class="row"  id="type-010000" style="display:none">
                          <label class="label col col-2">
                             域名：
                          </label>
                          <section class="col col-5">
                              <label class="input">
                                  <input type="text" id="variable" name="variable" value="<s:property value="commonConfig.variable"/>" placeholder="请输入自定义域名"/>
                              </label>
                          </section>
                       </div>
                  </div>
               </li>
        </ul>
    </fieldset>
</form>
<script>
    $(function(){
        $('.chat-footer .textarea-controls').append('<button id="left_foot_btn_save" data="" class="btn btn-sm btn-primary pull-right">确认</button>');
    });
    $("div.modal-body input[name='type']").click(function(){
        var id = $(this).val();
        if($(this).prop("checked")){
            $("div.modal-body #type-"+id).show();
            if(id == "100000"){
                $('#allMans').val(1);
            }
        }else{
            $("div.modal-body #type-"+id).hide();
            if(id == "100000"){
                $('#allMans').val(0);
            }
        }
    });




    $("a[key='btn-choose-role']").unbind("click").bind("click",function(){
         gDialog.fCreate({
                   title:"请选择群组",
                   url:"../com/ajax-dialog!role.action",
                   width:340
                }).show();
    });
    $("a[key='btn-choose-depart']").unbind("click").bind("click",function(){
        gDialog.fCreate({
                       title:"请选择部门",
                       url:"../com/ajax-dialog!mdepart.action",
                       width:500
                    }).show();
    });
    $("a[key='btn-choose-post']").unbind("click").bind("click",function(){
         gDialog.fCreate({
                   title:"请选择岗位",
                   url:"../com/ajax-dialog!mpost.action",
                   width:340
                }).show();
    });
    $("a[key='btn-choose-power']").unbind("click").bind("click",function(){
        gDialog.fCreate({
                   title:"请选择职权",
                   url:"../com/ajax-dialog!power.action",
                   width:340
                }).show();
    });

    $("#left_foot_btn_save").off("click").on("click",function(){
        var actionUrl="../com/ajax-commoncfg!save.action";
        var clone = "";
        form_save("commonconfig_form",actionUrl,{msg:false},function(pdata){
            var data = pdata.data.data[0];
            _show(data);
            clone = $("div.modal-body #commonDiv").clone(true);
            $("#"+data.selectType+"Div").empty();


            var keyId=data.keyId;
            $("#"+data.selectType+"Id").val(data.keyId);

            loadURL("../com/ajax-commoncfg!read.action?keyId="+keyId,$("#"+data.selectType+"Div"));
        });

        $(this).leftview('leftClose');
    });

    $(function(){
        var cType=$("input#sType").val();
        $("input[name='type']").each(function(i,v){
            var optVal = $(v).val();
            var idx = optVal.indexOf("1");
            if(cType!=null && cType.charAt(idx) == "1"){
                $(v).prop("checked",true);
                $("div.modal-body #type-"+optVal).show();
            }
        });
    });
</script>
