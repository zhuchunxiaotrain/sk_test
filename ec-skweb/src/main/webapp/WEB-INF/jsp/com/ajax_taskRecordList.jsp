<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<style>
    .ui-search-toolbar div{padding: 0px !important;}
</style>
<jsp:include page="../com/ajax-top.jsp" />
<div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark" style="margin-top:6px;margin-bottom:10px">
        <i class="fa fa-table fa-fw "></i>
        <s:if test='type=="1"'>已办未结</s:if><s:else>已办已结</s:else>
    </h1>
  </div>
</div>
<input type="hidden" name="type" value='<s:property value="type" />' id="type" />
<div id="taskrecord_data">

  <!-- rows -->
  <!-- widget grid -->
  <section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget " id="wid-id-0" data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="true"
             data-widget-custombutton="false"
             data-widget-sortable="false">
          <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
              <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->
            <!-- widget content -->
            <div class="widget-body no-padding">
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class=" ">
                  <div class="row" id="ajax_notice_list_row">
                    <table id="ajax_taskrecord_table" class="table table-striped table-bordered table-hover">
                    </table>
                      <div id="ajax_taskrecord_list_page">
                      </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- end widget content -->
          </div>
          <!-- end widget div -->
        </div>
        <!-- end widget -->
      </article>
      <!-- WIDGET END -->
    </div>
  </section>
  <!-- end widget grid -->
</div>

<script type="text/javascript">
  $(function() {
        if($('#type').val() == "1"){
            load_notaskrecord_jqGrid();
        }else{
            load_taskrecord_jqGrid();
        }
  });

  function load_notaskrecord_jqGrid(){
    jQuery("#ajax_taskrecord_table").jqGrid({
        url:'../com/ajax-taskrecord!list.action?type=1',
        datatype: "json",
        colNames:['审核内容','所属模块','当前操作人','url',"id"],
        colModel:[
            {name:'pname',index:'pname', width:200,search:false,sortable:false},
            {name:'module',index:'module', width:100,search:true,sortable:true},
            {name:'usernames',index:'usernames', width:100,search:false,sortable:false},
            {name:'url',index:'url', width:0,search:false,sortable:false,hidden:true},
            {name:'id',index:'id',search:false,hidden:true,key:true}
        ],
        rowNum : 10,
        rowList:[10,20,30],
      pager : '#ajax_taskrecord_list_page',
      sortname : '',
      sortorder : "",
      gridComplete:function(){

          $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
          jqGridStyle();
      },
      onSelectRow: function (rowId, status, e) {
        var rowId = $("#ajax_taskrecord_table").jqGrid('getGridParam','selrow');
        var rowDatas = $("#ajax_taskrecord_table").jqGrid('getRowData', rowId);
      },
        ondblClickRow:function(rowid, iRow, iCol, e){
            var rowId = $("#ajax_taskrecord_table").jqGrid('getGridParam','selrow');
            var rowDatas = $("#ajax_taskrecord_table").jqGrid('getRowData', rowId);
            loadURL(rowDatas.url,$('#content'));
        },
      jsonReader: {
        root: "dataRows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems : false
      },
        caption : "已办未结",
        multiselect : false,
        rownumbers:false,
        gridview:true,
        shrinkToFit:true,
        viewrecords: false,
        autowidth: true,
        height:'auto',
        forceFit:true,
      loadComplete: function() {
      }
    });
    $(window).on('resize.jqGrid', function() {
      jQuery("#ajax_taskrecord_table").jqGrid('setGridWidth', $("#ajax_taskrecord_list_row").width());
    })
    jQuery("#ajax_taskrecord_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

      jQuery("#ajax_taskrecord_table").jqGrid('navGrid', "#ajax_taskrecord_list_page", {
          edit : false,
          add : false,
          del : false,
          search:false
      });

  };


  function load_taskrecord_jqGrid(){
      jQuery("#ajax_taskrecord_table").jqGrid({
          url:'../com/ajax-taskrecord!list.action?type=2',
          datatype: "json",
          colNames:['审核内容','所属模块','url',"id"],
          colModel:[
              {name:'pname',index:'pname', width:200,search:false,sortable:false},
              {name:'module',index:'module', width:100,search:true,sortable:true},
              {name:'url',index:'url', width:0,search:false,sortable:false,hidden:true},
              {name:'id',index:'id',search:false,hidden:true,key:true}
          ],
          rowNum : 10,
          rowList:[10,20,30],
          pager : '#ajax_taskrecord_list_page',
          sortname : '',
          sortorder : "",
          gridComplete:function(){
              $(".ui-jqgrid-bdiv").css("overflow-x","hidden");
              jqGridStyle();
          },
          onSelectRow: function (rowId, status, e) {
              var rowId = $("#ajax_taskrecord_table").jqGrid('getGridParam','selrow');
              var rowDatas = $("#ajax_taskrecord_table").jqGrid('getRowData', rowId);
          },
          ondblClickRow:function(rowid, iRow, iCol, e){
              var rowId = $("#ajax_taskrecord_table").jqGrid('getGridParam','selrow');
              var rowDatas = $("#ajax_taskrecord_table").jqGrid('getRowData', rowId);
              loadURL(rowDatas.url,$('#content'));
          },
          jsonReader: {
              root: "dataRows",
              page: "page",
              total: "total",
              records: "records",
              repeatitems : false
          },
          caption : "已办已结",
          multiselect : false,
          rownumbers:false,
          gridview:true,
          shrinkToFit:true,
          viewrecords: false,
          autowidth: true,
          height:'auto',
          forceFit:true,
          loadComplete: function() {
          }
      });
      $(window).on('resize.jqGrid', function() {
          jQuery("#ajax_taskrecord_table").jqGrid('setGridWidth', $("#ajax_taskrecord_list_row").width());
      })
      jQuery("#ajax_taskrecord_table").jqGrid('filterToolbar',{searchOperators:false,stringResult:true});

      jQuery("#ajax_taskrecord_table").jqGrid('navGrid', "#ajax_taskrecord_list_page", {
          edit : false,
          add : false,
          del : false,
          search:false
      });

  };


</script>





















