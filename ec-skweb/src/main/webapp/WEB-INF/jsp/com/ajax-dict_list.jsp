<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<style>
th{font-weight:500}
</style>
<table id="ajax_dict_list_table" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>系统默认名称</th>
        <th>自定义显示名称</th>
        <th>自定义排序</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <s:iterator value="list" id="list">
        <tr id="<s:property value="#list.id"/>" did="<s:property value="#list.sid"/>"
            sname="<s:property value="#list.sname"/>" sortNo="<s:property value="#list.sortNo"/>"
            <s:if test="#list.companyId == null">class="unread"</s:if>
                >
            <!--
            <td  class="inbox-table-icon">
                <div class="checkbox">
                <label>
                    <input type="checkbox" class="checkbox style-2">
                    <span></span> </label>
                </div>
            </td> -->
            <td class="inbox-data-from  ">
						<span>
						<s:if test="#list.sstate == null || #list.sstate.toString() == 'Enable'">
                            <i class="fa fa-lg fa-check"></i>
                        </s:if>
						<s:elseif test="#list.sstate.toString() == 'Disenable'">
                            <i class="fa fa-lg fa-times"></i>
                        </s:elseif>
						<s:property value="#list.name"/> <s:if test="#list.companyId  == null"><span
                                class="label bg-color-teal">系统设定 </span></s:if>  &nbsp;

						</span>
                <s:if test="#list.remark != null && #list.remark != ''">
                    <span>【<i class="fa fa-question"></i>】<s:property value="#list.remark"/></span>
                </s:if>
            </td>
            <td>
                <div>
                    <i class="fa fa-lg fa-hand-o-right"></i>
                    <s:if test="#list.sname != null">
                        <s:property value="#list.sname"/>
                    </s:if>
                    <s:else>
                        <s:property value="#list.name"/>
                    </s:else>
                </div>
            </td>

            <td class=" ">
                <div>
                    <s:if test="#list.company_id == null">
                        <span class="label bg-color-teal">系统设定不允许排序</span>
                    </s:if>
                    <s:else>
                        <a key="ajax_dict_actions_up" class="btn btn-default " href="javascript:void(0);"><i
                                class="fa fa-lg fa-arrow-up"></i> 上移</a>
                        <a key="ajax_dict_actions_down" class="btn  btn-default" href="javascript:void(0);"><i
                                class="fa fa-lg fa-arrow-down"></i> 下移</a>
                    </s:else>
                </div>
            </td>
            <td class=" ">
                <div>
                    <!-- <s:date name="#list.createDate" format="yyyy-MM-dd HH:mm" /> -->
                    <a key="ajax_dict_actions_edit" class="btn btn-default " href="javascript:void(0);"><i
                            class="fa fa-lg fa-edit"></i> 自定义名称</a>
                    <!-- 是否系统关键字典，不允许禁用 -->
                    <s:if test="#list.ifKey == 0">
                        <s:if test="#list.sstate == null || #list.sstate.toString() == 'Enable'">
                            <a key="ajax_dict_actions_disabled" class="btn btn-warning " href="javascript:void(0);"><i
                                    class="fa fa-lg fa-ban"></i> 禁用</a>
                        </s:if>
                        <s:elseif test="#list.sstate.toString() == 'Disenable'">
                            <a key="ajax_dict_actions_enable" class="btn  btn-success" href="javascript:void(0);"><i
                                    class="fa fa-lg fa-check-circle-o"></i> 启用</a>
                        </s:elseif>
                    </s:if>
                    <s:else>
                        <span class="label bg-color-green"><i class="fa fa-lg fa-key"></i> 关键字典不允许禁用</span>
                    </s:else>
                </div>
            </td>
        </tr>
    </s:iterator>
    </tbody>
</table>

<script>
    //dict delete
    $("a[key=ajax_dict_actions_disabled]").click(function () {
        var this_tr = $(this).parent().parent().parent();
        fn_disabled($(this_tr).attr("id"), $(this_tr).attr("did"));
    });
    $("a[key=ajax_dict_actions_enable]").click(function () {
        var this_tr = $(this).parent().parent().parent();
        fn_enable($(this_tr).attr("id"), $(this_tr).attr("did"));
    });

    //dict edit
    $("a[key=ajax_dict_actions_edit]").click(function () {
        var this_tr = $(this).parent().parent().parent();
        fn_edit($(this_tr).attr("id"), $(this_tr).attr("did"));
    });


    //交换位置：交换sortNo，(下移到第一个置1，其他全部加1)，(或者上移到最后，其他全部减1)，通过Ajax一并JSON {id:sortNo},{id:sortNo}
    $("a[key=ajax_dict_actions_up]").click(function () {
        var this_tr = $(this).parent().parent().parent();
        if ($(this_tr).prev().length > 0) {
            //alert($(this_tr).attr("id") + " " + $(this_tr).prev().attr("id"))
            $(this_tr).prev().before($(this_tr));
            //判断，上移判断交换对方是否是系统自带字典，原位置的自定义的邻居从1开始，后续减1，否则交换sortNo
            fn_dictSort($(this_tr), $(this_tr).next(), "up");
        }
    });
    $("a[key=ajax_dict_actions_down]").click(function () {
        var this_tr = $(this).parent().parent().parent();
        if ($(this_tr).next().length > 0) {
            //alert($(this_tr).attr("id") + " " + $(this_tr).next().attr("id"))
            $(this_tr).next().after($(this_tr));

            //判断，下移判断交换对方是否是系统自带字典，现位置的自定义的邻居从1开始，后续加1，否则交换sortNo
            fn_dictSort($(this_tr).prev(), $(this_tr), "down");
        } else {
            //alert($(this_tr).attr("id") )
        }
    });

    //Gets tooltips activated
    $("#inbox-table [rel=tooltip]").tooltip();

    $("#inbox-table input[type='checkbox']").change(function () {
        $(this).closest('tr').toggleClass("highlight", this.checked);
    });

    $("#inbox-table .inbox-data-message").click(function () {
        $this = $(this);

    })
    $("#inbox-table .inbox-data-from").click(function () {
        $this = $(this);

    })

    $('.inbox-table-icon input:checkbox').click(function () {
        enableDeleteButton();
    })

    $(".deletebutton").click(function () {
        $('#inbox-table td input:checkbox:checked').parents("tr").rowslide();
        //$(".inbox-checkbox-triggered").removeClass('visible');
        //$("#compose-mail").show();
    });

    function enableDeleteButton() {
        var isChecked = $('.inbox-table-icon input:checkbox').is(':checked');

        if (isChecked) {
            $(".inbox-checkbox-triggered").addClass('visible');
            //$("#compose-mail").hide();
        } else {
            $(".inbox-checkbox-triggered").removeClass('visible');
            //$("#compose-mail").show();
        }
    }

</script>
