<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<style>
th{font-weight:500}
</style>
<div class="row" style="padding:2px">
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="form-group">
										
										<h5><label style="width:100%" for="h-input"> <span class="label  label-danger"> 提示：支持增量数据批量导入，如果数据已经存在，则该条记录不导入。</span> </label>
										</h5>
										<!-- <a id="ajax_members_btn_add" class="btn btn-default " data-toggle="modal"><i class="fa fa-lg fa-plus"></i> 新增成员账号</a>
										 --> 
									</div>
								</div>
							</div>
							<!-- rows -->
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-100" data-widget-editbutton="false" 
				data-widget-colorbutton="false" 
				data-widget-togglebutton="false" 
				data-widget-deletebutton="false" 
				data-widget-fullscreenbutton="true" 
				data-widget-custombutton="false" 
				data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2 style="margin-top: -42px;font-family: 微软雅黑;">导入模板清单一览信息 </h2>
				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
						<table id="ajax_members_table" class="table table-striped table-bordered table-hover">
							
						
						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->


		<div class="col-sm-12">
			<div class="alert alert-block alert-info">
				<h4 class="alert-heading">【说明】</h4>
				CSV文件可以通过Excel另存为csv文件得到，通过Excel表格制作好对应的数据后，另存为csv文件即可，【警告】多个Sheet页只能另存为一个Sheet页内容，请多次另存为。
			</div>
			<div class="alert alert-block alert-warning">
				【小提示】多个Sheet页只能另存为一个Sheet页内容，请多次另存为。
			</div>
		</div>

	</div>

</section>



<!-- end widget grid -->
<div id="dialog-import_message" >

</div><!-- #dialog-message -->

<!-- Modal 
<div class="modal fade" id="dialog-message" tabindex="-1" role="dialog" aria-labelledby="dialog-messageLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="dialog-messageLabel">成员信息</h4>
			</div>
			<div class="modal-body">
				


			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					取消
				</button>
				<button type="button" class="btn btn-primary">
					提交
				</button>
			</div>
		</div>
	</div>
</div>
-->
<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();
	
	loadInbox();
	function loadInbox() {
		loadURL("<%=path%>/com/ajax-import!list.action?keyId=", $('#ajax_members_table'));
	}
		
	//添加新成员
	$('#ajax_import_btn_upload').click(function() {
		//loadURL("ajax-import!read.action?keyId=", $('#dialog-members_message'));
		$('#dialog-members_message').dialog('open');
		return false;
	});

	$("#dialog-import_message").dialog({
		autoOpen : false,
		modal : true,
		title : "初始化数据导入框",
		width:700,
		height:500,
		//position: [200,300],
		buttons : [{
			html : "关闭",
			"class" : "btn btn-default",
			click : function() {
				$(this).dialog("close");
			}
		}]

	});
	$(function(){

		$("#company_businessdata_reset").click(function(e){
			//ajax执行返回时触发
			$.SmartMessageBox({
				title : "<i class='fa fa-trash-o txt-color-orangeDark'></i> 【谨慎使用该操作】删除  <span class='txt-color-orangeDark'><strong> 所有销售业务数据吗？ </strong></span>",
				content : "",
				buttons : '[取消][确定]'
			}, function(ButtonPressed) {
				//[仅删除本项]
				if (ButtonPressed === "确定") {
					if(!confirm("请再次确认您的操作无误，确定删除【预约、洽谈、业务、成交、履约信息】吗？")){
						e.preventDefault();
						e.stopPropagation();
						return ;
					}
					var vActionUrl = "<%=path%>/com/company-data!salesReset.action";
					$.ajax({
						url : vActionUrl,
						cache : false,
						dataType : "json",
						async : false,
						data : {
							"keyId" : ""
						},
						success : function(data) {
							if (data.state == "200") {
								//前台显示出来
								$.smallBox({
									title : "提示：",
									content : "<i class='fa fa-clock-o'></i> <i>"
									+ data.message + "</i>",
									color : "#659265",
									iconSmall : "fa fa-thumbs-up bounce animated",
									timeout : 4000
								});
							} else {
								$.smallBox({
									title : "操作失败",
									content : "<i class='fa fa-clock-o'></i> <i>"
									+ data.message + "</i>",
									color : "#C46A69",
									iconSmall : "fa fa-times fa-2x fadeInRight animated",
									timeout : 6000
								});
							}
						}
					});
				}
			});
		})

		//company_clientdata_reset
		$("#company_clientdata_reset").click(function(e){
			//ajax执行返回时触发
			$.SmartMessageBox({
				title : "<i class='fa fa-trash-o txt-color-orangeDark'></i> 【谨慎使用该操作】删除  <span class='txt-color-orangeDark'><strong> 所有客户数据吗？ </strong></span>",
				content : "",
				buttons : '[取消][确定]'
			}, function(ButtonPressed) {
				//[仅删除本项]
				if (ButtonPressed === "确定") {
					if(!confirm("请再次确认您的操作无误，确定删除【客户、联系人信息】吗？")){
						e.preventDefault();
						e.stopPropagation();
						return ;
					}
					var vActionUrl = "<%=path%>/com/company-data!clientReset.action";
					$.ajax({
						url : vActionUrl,
						cache : false,
						dataType : "json",
						async : false,
						data : {
							"keyId" : ""
						},
						success : function(data) {
							if (data.state == "200") {
								//前台显示出来
								$.smallBox({
									title : "提示：",
									content : "<i class='fa fa-clock-o'></i> <i>"
									+ data.message + "</i>",
									color : "#659265",
									iconSmall : "fa fa-thumbs-up bounce animated",
									timeout : 4000
								});
							} else {
								$.smallBox({
									title : "操作失败",
									content : "<i class='fa fa-clock-o'></i> <i>"
									+ data.message + "</i>",
									color : "#C46A69",
									iconSmall : "fa fa-times fa-2x fadeInRight animated",
									timeout : 6000
								});
							}
						}
					});
				}
			});
		})
	})
</script>
