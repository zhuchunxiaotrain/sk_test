<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
          + request.getServerName() + ":" + request.getServerPort()
          + path + "/";
%>
<jsp:include page="ajax-top.jsp" />

<div class="row">
  <!-- NEW WIDGET START -->
  <article class="col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget well" id="wid-id-3"
         data-widget-colorbutton="false"
         data-widget-editbutton="false"
         data-widget-togglebutton="false"
         data-widget-deletebutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-custombutton="false"
         data-widget-sortable="false">
      <!-- widget div-->
      <div>
        <div class="widget-body">
          <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all">员工续签</i></a>

          <s:if test="renew==null || renew.getProcessState().name()=='Draft'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
          </s:if>
          <s:if test="renew!=null && renew.getProcessState().name()=='Backed'">
            <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
          </s:if>
          <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-download"></i> 保存</a>
          <hr class="simple">
          <form id="renew" class="smart-form" novalidate="novalidate" action="" method="post">
            <input type="hidden" name="keyId" id="keyId" value="<s:property value="renew.id" />"/>
            <input type="hidden" name="parentId" id="parentId" value="<s:property value="parentId" />"/>
            <input type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
            <header  style="display: block;">
              员工续签&nbsp;&nbsp;<span id="title"></span>
            </header>
            <fieldset>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  用人性质
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input type="hidden" disabled name="nature" id="nature"  value="<s:property value="employees.nature"/>" >
                    <input type="text"  name="natureName" id="natureName"  value="<s:property value="natureName"/>" >
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  姓名
                </label>
                <section class="col col-5">
                  <label class="input state-disabled">
                    <input disabled id="name" name="name" type="text" value="<s:property value="employees.name"/>">

                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  部门
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="oldDepName" id="oldDepName"  value="<s:property value="employees.dep.name"/>" >
                    <input type="hidden"  name="oldDepId" id="oldDepId" value="<s:property value="employees.dep.id"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  岗位
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input type="text" disabled name="oldPostName" id="oldPostName"  value="<s:property value="employees.empPost.name"/>" >
                    <input type="hidden"  name="oldPostId" id="oldPostId" value="<s:property value="employees.empPost.id"/>" >

                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同开始日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="startDate" disabled name="startDate"
                           type="text" value="<s:date name="employees.startDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>

                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  合同结束日期
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input id="endDate" disabled name="endDate"
                           type="text" value="<s:date name="employees.endDate" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  续签合同开始日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="renewalStart" name="renewalStart" placeholder="请选择续签合同开始日期"
                            type="text" value="<s:date name="renew.renewalStart" format="yyyy-MM-dd"/>">
                  </label>
                </section>
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  续签合同结束日期
                </label>
                <section class="col col-4">
                  <label class="input">
                    <input  id="renewalEnd" name="renewalEnd" placeholder="请选择续签合同结束日期"
                            type="text" value="<s:date name="renew.renewalEnd" format="yyyy-MM-dd"/>">
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-post">续签职位</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input disabled type="text" id="p_name"
                           value="<s:property value="renew.renewPost.name"/>"/>
                    <input type="hidden" id="p_id" name="postId"
                           value="<s:property value="renew.renewPost.id"/>"/>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  续签薪资(元)
                </label>
                <section class="col col-5">
                  <label class="input">
                    <input type="text" name="renewMoney" id="renewMoney" placeholder="请输入续签薪资(元)" value="<s:property value="renew.renewMoney"/>" >
                  </label>
                </section>
              </div>

              <div class="row">
                <label class="label col col-2">
                  <i class="fa fa-asterisk txt-color-red"></i>
                  <a  href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                </label>
                <section class="col col-5">
                  <label class="input">
                    <label class="input state-disabled">
                      <input disabled type="text" id="usersName" name="nextStepApproversName"
                             value="<s:property value="renew.nextStepApproversName"/>"/>
                      <input type="hidden" id="usersId" name="nextStepApproversId"
                             value="<s:property value="renew.nextStepApproversId"/>"/>
                    </label>
                  </label>
                </section>
              </div>
              <div class="row">
                <label class="label col col-2">
                  提交人
                </label>
                <section class="col col-4">
                  <label class="input state-disabled">
                    <input  disabled type="text" id="creater" placeholder="" value="<s:property value="creater.name"/>" >
                  </label>
                </section>
                <label class="label col col-2">
                  提交日期
                </label>
                <section class="col col-4 ">
                  <label class="input state-disabled">
                    <input  disabled type="text" name="createDate" id=createDate"  value="<s:property value="createDate"/>" >
                  </label>
                </section>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </article>
</div>

<script type="text/javascript">
    var draft = "<s:property value="draft" />";

    /*
           $('li.disabled').off("click").on("click", function () {
               return false;
           })

    */
    $(function(){
        var pdata= {
            keyId:$("#keyId").val(),
            flowName:"interview",
            todo:"1"
        };
        multiDuty(pdata);
    });

    $('#startDate,#endDate,#renewalEnd,#renewalStart').datetimepicker({
           format: 'yyyy-mm-dd',
           weekStart: 1,
           autoclose: true,
           todayBtn: 'linked',
           language: 'zh-CN',
           minView:2
        });

    //续签职位
    $("a[key=btn-choose-post]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择续签职位",
            url:"ajax-dialog!post.action",
            width:340
        }).show();
    });

    //会签
    $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择人员",
            url:"ajax-dialog!user.action",
            width:340
        }).show();
    });

    //保存
    $("#btn-save-common").click(

        function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("renew","ajax-renew!save.action",null,function (data) {
                if(data.state == "200" || data.state ==200) {
                    if(draft == 1){
                        location.href='index.action';
                    }else{
                        loadURL("ajax-expiring.action",$('#content'));
                    }
                }
            });
        }

    );

    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#renew").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#renew").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("renew","ajax-renew!commit.action",null,function (data) {

                        if(data.state == "200" || data.state ==200) {
                            if(draft == 1){
                                location.href='index.action';
                            }else{
                                loadURL("ajax-expiring.action",$('#content'));
                            }
                        }

                    });
                }
            });
        }
    );

    //返回视图
    $("#btn-re-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-expiring.action",$('#content'));
        }

    });

</script>