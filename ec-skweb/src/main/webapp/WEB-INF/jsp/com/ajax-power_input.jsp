<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<div class="modal-body">
    <div class="row">
            <div class="col-md-12">

            <form id="power_input" class="smart-form" novalidate="novalidate" action="" method="post">
                <input type="hidden" name="keyId" id="keyId"  value="<s:property value="keyId"/>">
                <header>
                   [职权添加]
                </header>
                <fieldset>

                    <div class="row">
                        <label class="label col col-3">描述</label>
                        <section class="col col-9">
                            <label class="input">
                                <input name="description" id="description" placeholder="请输入描述"
                                       type="text" value="<s:property value="power.description"/>">
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <label class="label col col-3">上级职权</label>
                        <section class="col col-9">
                            <label class="input">
                                <input id="fid"  placeholder="请选择职权"
                                       type="text" onfocus="showzTree()" value="<s:if test="power.parent.state.toString() == 'Enable' "><s:property value="power.parent.department.name" />-<s:property value="power.parent.post.name" /></s:if>">
                            </label>
                            <input type="hidden" name="parentId" id="parentId"/>
                            <div id="tree_menu" style="display:none;position:absolute;overflow:auto;background: #fff ;z-index: 99;height: 150px;border: 1px solid #ccc">
                                <ul id="pTree" class="ztree" style="margin-top:0; width:298px; height: 100px;"></ul>
                            </div>
                        </section>
                    </div>
                    <div class="row">
                        <label class="label col col-3">
                            <a title="分配部门" href="javascript:void(0);" key="ajax_depart_action_list">分配部门</a>
                        </label>
                        <section class="col col-9">
                            <label class="input">
                                <input type="text" id="d_name" name="d_name" placeholder="请选择职权" value="<s:property value="power.department.name"/>">
                                <input type="hidden" name="d_id" id="d_id" placeholder="请输入编号" value="">
                            </label>
                        </section>
                    </div>

                    <div class="row">
                        <label class="label col col-3">
                            <a title="分配岗位" href="javascript:void(0);" key="ajax_post_action_list">分配岗位</a>
                        </label>
                        <section class="col col-9">
                            <label class="input">
                                <input type="text" id="p_name" name="p_name" placeholder="请选择职权" value="<s:property value="power.post.name"/>">
                                <input type="hidden" name="p_id" id="p_id" placeholder="请输入编号" value="">
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <label class="label col col-3">下级职权</label>
                        <section class="col col-9">
                            <label class="select">
                                <ul class="list-group">
                                    <s:iterator value="power.children" id="chdList">
                                        <s:if test="#chdList.state.toString()  == 'Enable'">
                                            <li class="list-group-item">
                                                <s:property value="#chdList.department.name"/>-<s:property value="#chdList.post.name"/>
                                            </li>
                                        </s:if>
                                    </s:iterator>
                                </ul>
                            </label>
                        </section>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
	<a href="#../com/ajax!power.action" class="btn btn-primary" id="dialog-power-ok">确定</a>
	<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
</div>
<script>
    //校验
    $("#power_input").validate({
        rules : {
            d_name : {
                required : true
            },
            p_name:{
                required : true
            }
        },
        messages : {
            d_name : {
                required : '请选择部门'
            },
            p_name:{
                required : '请输入岗位'
            }

        },
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });
    $("#dialog-power-ok").unbind("click").bind("click",function(){
        var $valid = $("#power_input").valid();
        if(!$valid) return false;
        var actionUrl = "<%=path%>/com/ajax-power!save.action";
        form_save("power_input",actionUrl);
        gDialog.fClose();
        jQuery("#ajax_power_table").trigger("reloadGrid");
    });
    $(function(){
        $("input[name=type]:checked").each(function(){
           if($(this).val()=="gpuser"){
               $("#userDiv").show();
               $("#departDiv").hide();
               $("#postDiv").hide();
           }
           if($(this).val()=="gpdepart"){
               $("#userDiv").hide();
               $("#departDiv").show();
               $("#postDiv").hide();
           }
           if($(this).val()=="gppost"){
               $("#userDiv").hide();
               $("#departDiv").hide();
               $("#postDiv").show();
           }
        });
    });
    $("input[name=type]").unbind().bind("click",function(e){
        if($(this).prop("checked") && $(this).val() == "gpuser"){
            $("#userDiv").show();
            $("#departDiv").hide();
            $("#postDiv").hide();
        }
        if($(this).prop("checked") && $(this).val() == "gpdepart"){
            $("#userDiv").hide();
            $("#departDiv").show();
            $("#postDiv").hide();
        }
        if($(this).prop("checked") && $(this).val() == "gppost"){
            $("#userDiv").hide();
            $("#departDiv").hide();
            $("#postDiv").show();
        }
    });

    $("a[key=ajax_depart_action_list]").unbind("click").bind("click",function(e){
         gDialog.fCreate({
               title:"请选择部门",
               url:"../com/ajax-dialog!depart.action",
               width:500
           }).show();

        /*
        ajax_action("ajax-dialog!departDlg.action",{keyIds:$("#d_id").val()},{},function(pdata){
            oDialog.open({
                title:"请选择部门",
                data:pdata.data.data,
                type:"radio",
                callback:function(){
                    var obj =$("#group input:checked");
                    var name = getArrProp(obj,"key");
                    var id = getArrProp(obj,"id");
                    $("#d_name").val(name);
                    $("#d_id").val(id);
                }
            });
        });*/
    });
    $("a[key=ajax_post_action_list]").unbind("click").bind("click",function(e){
        gDialog.fCreate({
               title:"请选择岗位",
               url:"../com/ajax-dialog!post.action",
               width:340
           }).show();
        /*
        ajax_action("ajax-dialog!postDlg.action",{keyIds:$("#p_id").val()},{},function(pdata){
            oDialog.open({
                title:"请选择岗位",
                data:pdata.data.data,
                type:"radio",
                callback:function(){
                    var obj =$("#group input:checked");
                    var name = getArrProp(obj,"key");
                    var id = getArrProp(obj,"id");
                    $("#p_name").val(name);
                    $("#p_id").val(id);
                }
            });
        });*/
    });
    $(function(){
        $("input#fid").treeSelect('init',{
            actionUrl:"../com/ajax-power!zTree.action"
        });
    });

    function showzTree(){
        $("#tree_menu").slideDown();
        $("input#fid").treeSelect('show');
    }

</script>