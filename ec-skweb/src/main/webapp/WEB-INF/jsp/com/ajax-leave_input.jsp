<%--
  Created by IntelliJ IDEA.
  User: dqf
  Date: 2015/8/26
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String jsessionid=session.getId();

%>
<jsp:include page="ajax-top.jsp" />
<div class="row">
    <!-- NEW WIDGET START -->
    <article class="col-sm-12 col-md-12 col-lg-12">
        <div class="jarviswidget well" id="wid-id-3"
             data-widget-colorbutton="false"
             data-widget-editbutton="false"
             data-widget-togglebutton="false"
             data-widget-deletebutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-custombutton="false"
             data-widget-sortable="false">
            <!-- widget div-->
            <div>
                <div class="widget-body">
                    <a class="btn btn-default" id="btn-re-common" href="javascript:void(0)"><i class="fa fa-lg fa-mail-reply-all"></i>招聘信息</a>
                    <s:if test="leave==null || leave.getProcessState().name()=='Draft'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-confirm-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 提交</a>
                    </s:if>
                    <s:if test="leave!=null && leave.getProcessState().name()=='Backed'">
                        <a class="btn btn-default pull-right pull-right-fix" id="btn-recommit-common" href="javascript:void(0);"><i class="fa fa-rocket"></i> 再提交</a>
                    </s:if>
                    <a class="btn btn-default pull-right pull-right-fix" id="btn-save-common" href="javascript:void(0);"><i class="fa fa-lg fa-download"></i> 保存</a>
                    <hr class="simple">
                    <form id="leave" class="smart-form" novalidate="novalidate" action="" method="post">
                        <input type="hidden" name="keyId" id="keyId" value="<s:property value="leave.id" />"/>
                        <input  type="hidden" name="curDutyId" id="curDutyId" value="<s:property value="curDutyId" />"/>
                        <header  style="display: block;">
                            请假申请&nbsp;&nbsp;<span id="title"></span>
                        </header>
                        <fieldset>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    请假类型
                                </label>
                                <section class="col col-5">
                                    <div class="inline-group" name="result">
                                        <label class="radio">
                                            <input type="radio" checked="checked" name="type" value="0" <s:property value="leave.type==0?'checked':''"/>>
                                            <i></i>年假</label>
                                        <label class="radio">
                                            <input type="radio"  name="type" value="1" <s:property value="leave.type==1?'checked':''"/>>
                                            <i></i>病假</label>
                                        <label class="radio">
                                            <input type="radio"  name="type" value="2" <s:property value="leave.type==2?'checked':''"/>>
                                            <i></i>事假</label>
                                        <label class="radio">
                                            <input type="radio"  name="type" value="3" <s:property value="leave.type==3?'checked':''"/>>
                                            <i></i>其他</label>
                                    </div>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    职员姓名
                                </label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="name" id="name" value="<s:property value="creater.name"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    所属年份
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input  placeholder="请选择所属年份" id="time" name="time" type="text" value="<s:property value="leave.time"/>">
                                    </label>
                                </section>

                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    请假天数
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input type="text" name="dayNum" id="dayNum" placeholder="请输入请假天数" value="<s:property value="dayNum"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    工作交接安排
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <input name="fileText" id="fileText" placeholder="请输入工作交接安排"
                                               type="text" value="<s:property value="leave.fileText"/>">
                                    </label>
                                    <label class="input">
                                        <input name="uploadify" id="filename" placeholder="" type="file" >
                                        <input name="fileId" id="fileId" style="display: none" value="<s:property value="fileId"/>">
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    今年年假
                                </label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="annuaLeave" id="annuaLeave" value="<s:property value="leaveDetail.annuaLeave"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    今年年假(已用)
                                </label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="usedAnnuaLeave" id="usedAnnuaLeave" value="<s:property value="leaveDetail.usedAnnuaLeave"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    今年年假(可用)
                                </label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="unusedAnnuaLeave" id="unusedAnnuaLeave" value="<s:property value="leaveDetail.unusedAnnuaLeave"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    已申请病假
                                </label>
                                <section class="col col-5 state-disabled">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="sickLeave" id="sickLeave" value="<s:property value="leaveDetail.sickLeave"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2 state-disabled">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    已申请事假
                                </label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="thingLeave" id="thingLeave" value="<s:property value="leaveDetail.thingLeave"/>" >
                                    </label>
                                </section>
                            </div><div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    已申请其他假期
                                </label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="othersLeave" id="othersLeave" value="<s:property value="leaveDetail.othersLeave"/>" >
                                    </label>
                                </section>
                            </div><div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    当年已申请天数
                                </label>
                                <section class="col col-5">
                                    <label class="input state-disabled">
                                        <input type="text" disabled name="usedDayNum" id="usedDayNum" value="<s:property value="leaveDetail.usedDayNum"/>" >
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <label class="label col col-2">
                                    <i class="fa fa-asterisk txt-color-red"></i>
                                    <a  href="javascript:void(0);" key="btn-choose-users"> 会签人</a>
                                </label>
                                <section class="col col-5">
                                    <label class="input">
                                        <label class="input state-disabled">
                                            <input disabled type="text" id="usersName" name="nextStepApproversName"
                                                   value="<s:property value="leave.nextStepApproversName"/>"/>
                                            <input type="hidden" id="usersId" name="nextStepApproversId"
                                                   value="<s:property value="leave.nextStepApproversId"/>"/>
                                        </label>
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <label class="label col col-2">
                                    申请人
                                </label>
                                <section class="col col-4">
                                    <label class="input state-disabled">
                                        <input  disabled type="text"  placeholder="" value="<s:property value="creater.name"/>" >
                                    </label>
                                </section>
                                <label class="label col col-2">
                                    操作日期
                                </label>
                                <section class="col col-4 ">
                                    <label class="input state-disabled">
                                        <input  disabled type="text" name="createDate" id=createDate" placeholder="" value="<s:property value="createDate"/>" >
                                    </label>
                                </section>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </article>
</div>

<script>
    var draft = "<s:property value="draft" />";

    $(function(){
        var pdata= {
            keyId:$("#keyId").val(),
            flowName:"leave"
        };
        multiDuty(pdata);

    });

    $('#time').datetimepicker({
        format: 'yyyy',
        weekStart: 1,
        autoclose: true,
        startView: 4,
        minView: 4,
        forceParse: false,
        language: 'zh-CN'
    }).on('changeDate', function(){
        $.ajax({
            type: "get",
            url:"ajax-leave!loadData.action?time="+$('#time').val(),
            cache:false,
            dataType:"json",
            async:false,
            success: function(data){
                $("#annuaLeave").val(data.annuaLeave);
                $("#usedAnnuaLeave").val(data.usedAnnuaLeave);
                $("#unusedAnnuaLeave").val(data.unusedAnnuaLeave);
                $("#sickLeave").val(data.sickLeave);
                $("#thingLeave").val(data.thingLeave);
                $("#othersLeave").val(data.othersLeave);
                $("#usedDayNum").val(data.usedDayNum);

            }
        });

    });


    //上传
    inputLoad({
        objId:"filename",
        entityName:"fileIds",
        sourceId:"fileId",
        jsessionid:"<%=jsessionid%>"
    });


    //校验
    $("#leave").validate({
        rules : {
            time : {
                required : true
            },
            dayNum: {
                required : true,
                number:true,
                min:0

            },
            fileIds:{
                required : true
            },

        },
        messages : {
            time : {
                required : "请选择年份"
            },
            dayNum : {
                required : "请输入请假天数",
                number:"请输入合法数字",
                min:"不能小于0"

            },
            fileIds : {
                required : "请输入工作交接安排",
            },


        },
        ignore: "",
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }

    });


    //会签
    $("a[key=btn-choose-users]").unbind("click").bind("click",function(){
        gDialog.fCreate({
            title:"请选择人员",
            url:"ajax-dialog!user.action",
            width:340
        }).show();
    });
    //保存
    $("#btn-save-common").click(

        function(){
            $("#btn-save-common").attr("disabled", "disabled");
            form_save("leave","ajax-leave!save.action",null,function (data) {
                if(data.state == "200" || data.state ==200) {
                    if(draft == 1){
                        location.href='index.action';
                    }else{
                        loadURL("ajax-leave.action?viewtype=1",$('#content'));
                    }
                }
            });
        }

    );

    //提交
    $("#btn-confirm-common,#btn-recommit-common").click(
        function(e) {
            if(!$("#leave").valid()){
                $("#areaselect_span").hide();
                return false;
            }
            $.SmartMessageBox({
                title : "提示：",
                content : "确定提交申请吗？",
                buttons : '[取消][确认]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "取消") {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                if (ButtonPressed === "确认") {
                    var $validForm = $("#leave").valid();
                    if(!$validForm) return false;
                    $("#btn-confirm-common").attr("disabled", "disabled");
                    $("#btn-recommit-common").attr("disabled", "disabled");
                    form_save("leave","ajax-leave!commit.action",null,function (data) {

                        if(data.state == "200" || data.state ==200) {
                            if(draft == 1){
                                location.href='index.action';
                            }else{
                                loadURL("ajax-leave.action?viewtype=1",$('#content'));
                            }
                        }
                    });

                }
            });
        }
    );

    //返回视图
    $("#btn-re-common").click(function(){
        if(draft == 1){
            location.href='index.action';
        }else{
            loadURL("ajax-leave.action?viewtype=1",$('#content'));
        }
    });

</script>