package com.fz.us.dict.bean;

/**
 * Created with us-parent -> com.fz.us.dict.
 * User: min_xu
 * Date: 2014/9/10
 * Time: 21:45
 * 说明：
 * 1、定义系统字典的类型
 * 2、定义每一个系统字典中存在的关键key的信息
 * 3、系统字典增加一项是否可选项
 */
public class DictBean {
    /**
     * 系统字典
     */
    public static enum DictEnum {
        NoticeType("公告类型",1),
        ProInfoCategory("项目类别", 2),
        PostCategory("岗位类别",3),
        ActivityType("活动类型",4),
        ActivityBranch("活动支部",5),
        AnnualBudgetItem("年度预算科目",6),
        LetterType("信访类别",7),
        DispatchType("发文类别",8),
        ReceiveType("收文类别",9),
        MeetName("申请会议室",10),
        MeetType("会议类型",11),
        DataType("资料类别",12),
        Expert("评标专家库",36),
        Post("招聘岗位",37),
        Edu("最高学历",38),
        Branch("所在支部",39),
        TrainType("培训类型",40),
        AuditType("审计类型",41),
        AuditOffice("审计事务所",42),
        QualificationsName("资质名称",43),
        SealName("印章名称",44),
        CarBrand("车辆品牌",45),
        CarType("车辆型号",46),
        CarNo("车辆号牌",47),
        ArchivesType("档案类型",48),
        ArchivesCategory("档案类目",49),
        ScrapReason("报废原因",50);
        private final String value;
        private final int key;

        private DictEnum(final String value, final int key) {
            this.value = value;
            this.key = key;
        }
        public String value() {
            return this.value;
        }
        public int key() {
            return this.key;
        }
    }



}
